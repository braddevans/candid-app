package com.devbrackets.android.exomedia.p006ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.devbrackets.android.exomedia.core.exoplayer.EMExoPlayer;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.google.android.exoplayer.MediaFormat;
import java.util.List;
import java.util.Map;

/* renamed from: com.devbrackets.android.exomedia.ui.widget.EMVideoView */
public class EMVideoView extends RelativeLayout {

    /* renamed from: p */
    private static final String f7870p = EMVideoView.class.getSimpleName();

    /* renamed from: a */
    protected VideoControls f7871a;

    /* renamed from: b */
    protected ImageView f7872b;

    /* renamed from: c */
    protected Uri f7873c;

    /* renamed from: d */
    protected VideoViewApi f7874d;

    /* renamed from: e */
    protected DeviceUtil f7875e = new DeviceUtil();

    /* renamed from: f */
    protected AudioManager f7876f;

    /* renamed from: g */
    protected C1974b f7877g = new C1974b();

    /* renamed from: h */
    protected int f7878h = 0;

    /* renamed from: i */
    protected int f7879i = -1;

    /* renamed from: j */
    protected boolean f7880j = false;

    /* renamed from: k */
    protected StopWatch f7881k = new StopWatch();

    /* renamed from: l */
    protected C1975c f7882l = new C1975c();

    /* renamed from: m */
    protected EMListenerMux f7883m;

    /* renamed from: n */
    protected boolean f7884n = true;

    /* renamed from: o */
    protected boolean f7885o = true;

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.EMVideoView$a */
    public class C1973a {
        /* access modifiers changed from: private */

        /* renamed from: b */
        public boolean f7887b = false;

        /* renamed from: c */
        private boolean f7888c = false;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public int f7889d = C3079d.exomedia_default_exo_texture_video_view;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public int f7890e = C3079d.exomedia_default_native_texture_video_view;

        public C1973a(Context context, AttributeSet attributeSet) {
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C3080e.EMVideoView);
                if (obtainStyledAttributes != null) {
                    this.f7887b = obtainStyledAttributes.getBoolean(C3080e.EMVideoView_useDefaultControls, this.f7887b);
                    this.f7888c = obtainStyledAttributes.getBoolean(C3080e.EMVideoView_useSurfaceViewBacking, this.f7888c);
                    this.f7889d = this.f7888c ? C3079d.exomedia_default_exo_surface_video_view : C3079d.exomedia_default_exo_texture_video_view;
                    this.f7890e = this.f7888c ? C3079d.exomedia_default_native_surface_video_view : C3079d.exomedia_default_native_texture_video_view;
                    this.f7889d = obtainStyledAttributes.getResourceId(C3080e.EMVideoView_videoViewApiImplLegacy, this.f7889d);
                    this.f7890e = obtainStyledAttributes.getResourceId(C3080e.EMVideoView_videoViewApiImplLegacy, this.f7890e);
                    obtainStyledAttributes.recycle();
                }
            }
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.EMVideoView$b */
    public class C1974b implements OnAudioFocusChangeListener {

        /* renamed from: a */
        protected boolean f7891a = false;

        /* renamed from: b */
        protected boolean f7892b = false;

        /* renamed from: c */
        protected int f7893c = 0;

        protected C1974b() {
        }

        /* renamed from: a */
        public boolean mo11369a() {
            if (!EMVideoView.this.f7885o || this.f7893c == 1) {
                return true;
            }
            if (EMVideoView.this.f7876f == null) {
                return false;
            }
            if (1 == EMVideoView.this.f7876f.requestAudioFocus(this, 3, 1)) {
                this.f7893c = 1;
                return true;
            }
            this.f7891a = true;
            return false;
        }

        /* renamed from: b */
        public boolean mo11370b() {
            if (!EMVideoView.this.f7885o) {
                return true;
            }
            if (EMVideoView.this.f7876f == null) {
                return false;
            }
            this.f7891a = false;
            return 1 == EMVideoView.this.f7876f.abandonAudioFocus(this);
        }

        public void onAudioFocusChange(int i) {
            if (EMVideoView.this.f7885o && this.f7893c != i) {
                this.f7893c = i;
                switch (i) {
                    case -3:
                    case -2:
                        if (EMVideoView.this.mo11331b()) {
                            this.f7892b = true;
                            EMVideoView.this.mo11334d();
                            return;
                        }
                        return;
                    case -1:
                        if (EMVideoView.this.mo11331b()) {
                            this.f7892b = true;
                            EMVideoView.this.mo11334d();
                            return;
                        }
                        return;
                    case 1:
                    case 2:
                        if (this.f7891a || this.f7892b) {
                            EMVideoView.this.mo11333c();
                            this.f7891a = false;
                            this.f7892b = false;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.EMVideoView$c */
    public class C1975c extends C3083a {
        protected C1975c() {
        }

        /* renamed from: a */
        public void mo11372a() {
            if (EMVideoView.this.f7871a != null) {
                EMVideoView.this.f7871a.mo11388b();
            }
        }

        /* renamed from: a */
        public void mo11373a(int i, int i2, int i3, float f) {
            EMVideoView.this.f7874d.setVideoRotation(i3, false);
            EMVideoView.this.f7874d.mo11221a(i, i2);
        }

        /* renamed from: a */
        public void mo11374a(EMExoPlayer eMExoPlayer, Exception exc) {
            EMVideoView.this.mo11335e();
            if (eMExoPlayer != null) {
                eMExoPlayer.mo11171c();
            }
        }

        /* renamed from: a */
        public void mo11375a(boolean z) {
            if (EMVideoView.this.f7872b != null) {
                EMVideoView.this.f7872b.setVisibility(z ? 0 : 8);
            }
        }

        /* renamed from: a */
        public boolean mo11376a(long j) {
            return ((long) EMVideoView.this.getCurrentPosition()) + j >= ((long) EMVideoView.this.getDuration());
        }

        /* renamed from: b */
        public void mo11377b() {
            if (EMVideoView.this.f7871a != null) {
                EMVideoView.this.f7871a.setDuration((long) EMVideoView.this.getDuration());
                EMVideoView.this.f7871a.mo11388b();
            }
        }

        /* renamed from: c */
        public void mo11378c() {
            EMVideoView.this.setKeepScreenOn(false);
            EMVideoView.this.mo11337g();
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.EMVideoView$d */
    public class C1976d extends SimpleOnGestureListener implements OnTouchListener {

        /* renamed from: a */
        protected GestureDetector f7896a;

        public C1976d(Context context) {
            this.f7896a = new GestureDetector(context, this);
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            if (EMVideoView.this.f7871a != null) {
                EMVideoView.this.f7871a.mo11390c();
                if (EMVideoView.this.mo11331b()) {
                    EMVideoView.this.f7871a.mo11385a(2000);
                }
            }
            return true;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            this.f7896a.onTouchEvent(motionEvent);
            return true;
        }
    }

    public EMVideoView(Context context) {
        super(context);
        mo11327a(context, (AttributeSet) null);
    }

    public EMVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo11327a(context, attributeSet);
    }

    @TargetApi(11)
    public EMVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo11327a(context, attributeSet);
    }

    @TargetApi(21)
    public EMVideoView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        mo11327a(context, attributeSet);
    }

    /* renamed from: a */
    public void mo11325a() {
        this.f7871a = null;
        mo11335e();
        this.f7881k.mo16920a();
        this.f7874d.mo11223c();
    }

    /* renamed from: a */
    public void mo11326a(int i) {
        if (this.f7871a != null) {
            this.f7871a.mo11389b(false);
        }
        this.f7874d.seekTo(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11327a(Context context, AttributeSet attributeSet) {
        if (!isInEditMode()) {
            this.f7876f = (AudioManager) context.getSystemService("audio");
            C1973a aVar = new C1973a(context, attributeSet);
            mo11328a(context, aVar);
            mo11329a(aVar);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11328a(Context context, C1973a aVar) {
        mo11330b(context, aVar);
        this.f7872b = (ImageView) findViewById(C3078c.exomedia_video_preview_image);
        this.f7874d = (VideoViewApi) findViewById(C3078c.exomedia_video_view);
        this.f7882l = new C1975c();
        this.f7883m = new EMListenerMux(this.f7882l);
        this.f7874d.setListenerMux(this.f7883m);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11329a(C1973a aVar) {
        if (aVar.f7887b) {
            setControls(this.f7875e.mo16898b(getContext()) ? new VideoControlsLeanback(getContext()) : new VideoControlsMobile(getContext()));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo11330b(Context context, C1973a aVar) {
        View.inflate(context, C3079d.exomedia_video_view_layout, this);
        ViewStub viewStub = (ViewStub) findViewById(C3078c.video_view_api_impl_stub);
        viewStub.setLayoutResource(mo11332c(context, aVar));
        viewStub.inflate();
    }

    /* renamed from: b */
    public boolean mo11331b() {
        return this.f7874d.isPlaying();
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public int mo11332c(Context context, C1973a aVar) {
        return !this.f7875e.mo16896a(context) ? aVar.f7890e : aVar.f7889d;
    }

    /* renamed from: c */
    public void mo11333c() {
        if (this.f7877g.mo11369a()) {
            this.f7874d.start();
            setKeepScreenOn(true);
            if (this.f7871a != null) {
                this.f7871a.mo11391c(true);
            }
        }
    }

    /* renamed from: d */
    public void mo11334d() {
        this.f7877g.mo11370b();
        this.f7874d.pause();
        setKeepScreenOn(false);
        if (this.f7871a != null) {
            this.f7871a.mo11391c(false);
        }
    }

    /* renamed from: e */
    public void mo11335e() {
        this.f7877g.mo11370b();
        this.f7874d.mo11220a();
        setKeepScreenOn(false);
        if (this.f7871a != null) {
            this.f7871a.mo11391c(false);
        }
    }

    /* renamed from: f */
    public boolean mo11336f() {
        if (this.f7873c == null || !this.f7874d.mo11222b()) {
            return false;
        }
        if (this.f7871a != null) {
            this.f7871a.mo11389b(true);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo11337g() {
        mo11335e();
    }

    public Map<Integer, List<MediaFormat>> getAvailableTracks() {
        return this.f7874d.getAvailableTracks();
    }

    public int getBufferPercentage() {
        return this.f7874d.getBufferedPercent();
    }

    public int getCurrentPosition() {
        return this.f7880j ? this.f7878h + this.f7881k.mo16922c() : this.f7878h + this.f7874d.getCurrentPosition();
    }

    public int getDuration() {
        return this.f7879i >= 0 ? this.f7879i : this.f7874d.getDuration();
    }

    public ImageView getPreviewImageView() {
        return this.f7872b;
    }

    public VideoControls getVideoControls() {
        return this.f7871a;
    }

    public Uri getVideoUri() {
        return this.f7873c;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode() && this.f7884n) {
            mo11325a();
        }
    }

    public void setControls(VideoControls videoControls) {
        if (!(this.f7871a == null || this.f7871a == videoControls)) {
            removeView(this.f7871a);
        }
        if (videoControls != null) {
            this.f7871a = videoControls;
            videoControls.setVideoView(this);
            addView(videoControls);
        }
        C1976d dVar = new C1976d(getContext());
        if (this.f7871a == null) {
            dVar = null;
        }
        setOnTouchListener(dVar);
    }

    public void setDrmProvider(DrmProvider ywVar) {
        this.f7874d.setDrmProvider(ywVar);
    }

    public void setHandleAudioFocus(boolean z) {
        this.f7877g.mo11370b();
        this.f7885o = z;
    }

    public void setId3MetadataListener(Id3MetadataListener yeVar) {
        this.f7883m.mo16786a(yeVar);
    }

    public void setMeasureBasedOnAspectRatioEnabled(boolean z) {
        this.f7874d.setMeasureBasedOnAspectRatioEnabled(z);
    }

    public void setOnBufferUpdateListener(OnBufferUpdateListener ylVar) {
        this.f7883m.mo16788a(ylVar);
    }

    public void setOnCompletionListener(OnCompletionListener ymVar) {
        this.f7883m.mo16789a(ymVar);
    }

    public void setOnErrorListener(OnErrorListener ynVar) {
        this.f7883m.mo16790a(ynVar);
    }

    public void setOnPreparedListener(OnPreparedListener yoVar) {
        this.f7883m.mo16791a(yoVar);
    }

    public void setOnSeekCompletionListener(OnSeekCompletionListener ypVar) {
        this.f7883m.mo16792a(ypVar);
    }

    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.f7874d.setOnTouchListener(onTouchListener);
        super.setOnTouchListener(onTouchListener);
    }

    public void setPositionOffset(int i) {
        this.f7878h = i;
    }

    public void setPreviewImage(int i) {
        if (this.f7872b != null) {
            this.f7872b.setImageResource(i);
        }
    }

    public void setPreviewImage(Bitmap bitmap) {
        if (this.f7872b != null) {
            this.f7872b.setImageBitmap(bitmap);
        }
    }

    public void setPreviewImage(Drawable drawable) {
        if (this.f7872b != null) {
            this.f7872b.setImageDrawable(drawable);
        }
    }

    public void setPreviewImage(Uri uri) {
        if (this.f7872b != null) {
            this.f7872b.setImageURI(uri);
        }
    }

    public void setReleaseOnDetachFromWindow(boolean z) {
        this.f7884n = z;
    }

    public void setScaleType(ScaleType scaleType) {
        this.f7874d.setScaleType(scaleType);
    }

    public void setTrack(int i, int i2) {
        this.f7874d.setTrack(i, i2);
    }

    public void setVideoPath(String str) {
        setVideoURI(Uri.parse(str));
    }

    public void setVideoRotation(int i) {
        this.f7874d.setVideoRotation(i, true);
    }

    public void setVideoURI(Uri uri) {
        this.f7873c = uri;
        this.f7874d.setVideoUri(uri);
        if (this.f7871a != null) {
            this.f7871a.mo11389b(true);
        }
    }

    public void setVideoURI(Uri uri, RenderBuilder yaVar) {
        this.f7873c = uri;
        this.f7874d.setVideoUri(uri, yaVar);
        if (this.f7871a != null) {
            this.f7871a.mo11389b(true);
        }
    }
}
