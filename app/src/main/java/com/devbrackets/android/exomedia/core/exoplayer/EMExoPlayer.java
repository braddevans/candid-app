package com.devbrackets.android.exomedia.core.exoplayer;

import android.media.MediaCodec.CryptoException;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager.WakeLock;
import android.view.Surface;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.MediaCodecTrackRenderer.DecoderInitializationException;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.audio.AudioTrack.InitializationException;
import com.google.android.exoplayer.audio.AudioTrack.WriteException;
import com.google.android.exoplayer.dash.DashChunkSource.C2111a;
import com.google.android.exoplayer.extractor.ExtractorSampleSource.C2117a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class EMExoPlayer implements C0146c, C0151a, C0156a, C0165b, C0173a, C0206a, C0270a, C0272a<List<ahy>>, aio, C0282a, C2111a, C2117a {

    /* renamed from: a */
    private RenderBuilder f7758a;

    /* renamed from: b */
    private final ack f7759b;

    /* renamed from: c */
    private final Handler f7760c;

    /* renamed from: d */
    private final CopyOnWriteArrayList<ExoPlayerListener> f7761d;

    /* renamed from: e */
    private final AtomicBoolean f7762e;

    /* renamed from: f */
    private RenderBuildingState f7763f;

    /* renamed from: g */
    private C1958b f7764g;

    /* renamed from: h */
    private Repeater f7765h;

    /* renamed from: i */
    private boolean f7766i;

    /* renamed from: j */
    private Surface f7767j;

    /* renamed from: k */
    private acy f7768k;

    /* renamed from: l */
    private acy f7769l;

    /* renamed from: m */
    private ada f7770m;

    /* renamed from: n */
    private adb f7771n;

    /* renamed from: o */
    private CaptionListener f7772o;

    /* renamed from: p */
    private Id3MetadataListener f7773p;

    /* renamed from: q */
    private InternalErrorListener f7774q;

    /* renamed from: r */
    private InfoListener f7775r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public OnBufferUpdateListener f7776s;

    /* renamed from: t */
    private WakeLock f7777t;

    public enum RenderBuildingState {
        IDLE,
        BUILDING,
        BUILT
    }

    /* renamed from: com.devbrackets.android.exomedia.core.exoplayer.EMExoPlayer$a */
    class C1957a implements C3103b {
        private C1957a() {
        }

        /* renamed from: a */
        public void mo11183a() {
            if (EMExoPlayer.this.f7776s != null) {
                EMExoPlayer.this.f7776s.mo16782a(EMExoPlayer.this.mo11179k());
            }
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.core.exoplayer.EMExoPlayer$b */
    static class C1958b {

        /* renamed from: a */
        private int[] f7783a;

        private C1958b() {
            this.f7783a = new int[]{1, 1, 1, 1};
        }

        /* renamed from: a */
        public int mo11184a() {
            return this.f7783a[3];
        }

        /* renamed from: a */
        public void mo11185a(boolean z, int i) {
            if (this.f7783a[3] != mo11187b(z, i)) {
                this.f7783a[0] = this.f7783a[1];
                this.f7783a[1] = this.f7783a[2];
                this.f7783a[2] = this.f7783a[3];
                this.f7783a[3] = i;
            }
        }

        /* renamed from: a */
        public boolean mo11186a(int[] iArr, boolean z) {
            boolean z2 = true;
            int i = z ? 268435455 : -1;
            int length = this.f7783a.length - iArr.length;
            for (int i2 = length; i2 < this.f7783a.length; i2++) {
                z2 &= (this.f7783a[i2] & i) == (iArr[i2 - length] & i);
            }
            return z2;
        }

        /* renamed from: b */
        public int mo11187b(boolean z, int i) {
            return (z ? -268435456 : 0) | i;
        }

        /* renamed from: b */
        public boolean mo11188b() {
            return (this.f7783a[3] & -268435456) != 0;
        }
    }

    public EMExoPlayer() {
        this(null);
    }

    public EMExoPlayer(RenderBuilder yaVar) {
        this.f7762e = new AtomicBoolean();
        this.f7764g = new C1958b();
        this.f7765h = new Repeater();
        this.f7766i = false;
        this.f7777t = null;
        this.f7765h.mo16915a(1000);
        this.f7765h.mo16916a((C3103b) new C1957a());
        this.f7759b = C0145b.m865a(4, 1000, 5000);
        this.f7759b.mo353a((C0146c) this);
        this.f7760c = new Handler();
        this.f7761d = new CopyOnWriteArrayList<>();
        this.f7763f = RenderBuildingState.IDLE;
        this.f7759b.mo358b(2, -1);
        mo11160a(yaVar);
    }

    /* renamed from: c */
    private void m10210c(boolean z) {
        if (this.f7768k != null) {
            if (z) {
                this.f7759b.mo359b(this.f7768k, 1, this.f7767j);
            } else {
                this.f7759b.mo352a(this.f7768k, 1, this.f7767j);
            }
        }
    }

    /* renamed from: d */
    private void m10211d(boolean z) {
        if (!z || this.f7776s == null) {
            this.f7765h.mo16917b();
        } else {
            this.f7765h.mo16914a();
        }
    }

    /* renamed from: q */
    private void m10212q() {
        boolean c = this.f7759b.mo360c();
        int h = mo11176h();
        int b = this.f7764g.mo11187b(c, h);
        if (b != this.f7764g.mo11184a()) {
            this.f7764g.mo11185a(c, h);
            if (b == 4) {
                m10211d(true);
            } else if (b == 1 || b == 5 || b == 2) {
                m10211d(false);
            }
            boolean a = this.f7764g.mo11186a(new int[]{100, 4}, true) | this.f7764g.mo11186a(new int[]{100, 3, 4}, true) | this.f7764g.mo11186a(new int[]{100, 4, 3, 4}, true);
            Iterator it = this.f7761d.iterator();
            while (it.hasNext()) {
                ExoPlayerListener ydVar = (ExoPlayerListener) it.next();
                ydVar.mo16794a(c, h);
                if (a) {
                    ydVar.mo16781a();
                }
            }
        }
    }

    /* renamed from: a */
    public int mo11150a(int i) {
        return this.f7759b.mo348a(i);
    }

    /* renamed from: a */
    public MediaFormat mo11151a(int i, int i2) {
        return this.f7759b.mo350a(i, i2);
    }

    /* renamed from: a */
    public void mo11152a() {
        if (this.f7767j != null) {
            this.f7767j.release();
        }
        this.f7767j = null;
        m10210c(true);
    }

    /* renamed from: a */
    public void mo420a(int i, int i2, int i3, float f) {
        Iterator it = this.f7761d.iterator();
        while (it.hasNext()) {
            ((ExoPlayerListener) it.next()).mo16783a(i, i2, i3, f);
        }
    }

    /* renamed from: a */
    public void mo421a(int i, long j) {
        if (this.f7775r != null) {
            this.f7775r.mo16838a(i, j);
        }
    }

    /* renamed from: a */
    public void mo481a(int i, long j, int i2, int i3, adl adl, long j2, long j3) {
        if (this.f7775r != null) {
            this.f7775r.mo16839a(i, j, i2, i3, adl, j2, j3);
        }
    }

    /* renamed from: a */
    public void mo482a(int i, long j, int i2, int i3, adl adl, long j2, long j3, long j4, long j5) {
        if (this.f7775r != null) {
            this.f7775r.mo16840a(i, j, i2, i3, adl, j2, j3, j4, j5);
        }
    }

    /* renamed from: a */
    public void mo984a(int i, long j, long j2) {
        if (this.f7775r != null) {
            this.f7775r.mo16841a(i, j, j2);
        }
    }

    /* renamed from: a */
    public void mo11153a(int i, acx acx) {
        if (this.f7775r != null) {
            this.f7775r.mo16842a(i, acx);
        }
    }

    /* renamed from: a */
    public void mo483a(int i, adl adl, int i2, long j) {
        if (this.f7775r != null) {
            if (i == 0) {
                this.f7775r.mo16843a(adl, i2, j);
            } else if (i == 1) {
                this.f7775r.mo16845b(adl, i2, j);
            }
        }
    }

    /* renamed from: a */
    public void mo484a(int i, IOException iOException) {
        if (this.f7774q != null) {
            this.f7774q.mo16847a(i, iOException);
        }
    }

    /* renamed from: a */
    public void mo11154a(long j) {
        this.f7759b.mo351a(j);
        this.f7764g.mo11185a(this.f7764g.mo11188b(), 100);
    }

    /* renamed from: a */
    public void mo480a(ada ada) {
        if (!ada.equals(this.f7770m)) {
            this.f7770m = ada;
            if (this.f7758a != null) {
                boolean l = mo11180l();
                long i = mo11177i();
                mo11160a(this.f7758a);
                this.f7759b.mo351a(i);
                this.f7759b.mo354a(l);
            }
        }
    }

    /* renamed from: a */
    public void mo11155a(CryptoException cryptoException) {
        if (this.f7774q != null) {
            this.f7774q.mo16848a(cryptoException);
        }
    }

    /* renamed from: a */
    public void mo11156a(Surface surface) {
        this.f7767j = surface;
        m10210c(false);
    }

    /* renamed from: a */
    public void mo367a(ExoPlaybackException exoPlaybackException) {
        this.f7763f = RenderBuildingState.IDLE;
        Iterator it = this.f7761d.iterator();
        while (it.hasNext()) {
            ((ExoPlayerListener) it.next()).mo16784a(this, (Exception) exoPlaybackException);
        }
    }

    /* renamed from: a */
    public void mo11157a(DecoderInitializationException decoderInitializationException) {
        if (this.f7774q != null) {
            this.f7774q.mo16849a(decoderInitializationException);
        }
    }

    /* renamed from: a */
    public void mo405a(InitializationException initializationException) {
        if (this.f7774q != null) {
            this.f7774q.mo16850a(initializationException);
        }
    }

    /* renamed from: a */
    public void mo406a(WriteException writeException) {
        if (this.f7774q != null) {
            this.f7774q.mo16851a(writeException);
        }
    }

    /* renamed from: a */
    public void mo11158a(Exception exc) {
        if (this.f7774q != null) {
            this.f7774q.mo16852a(exc);
        }
        Iterator it = this.f7761d.iterator();
        while (it.hasNext()) {
            ((ExoPlayerListener) it.next()).mo16784a(this, exc);
        }
        this.f7763f = RenderBuildingState.IDLE;
        m10212q();
    }

    /* renamed from: a */
    public void mo11159a(String str, long j, long j2) {
        if (this.f7775r != null) {
            this.f7775r.mo16844a(str, j, j2);
        }
    }

    /* renamed from: a */
    public void mo917a(List<aii> list) {
        if (this.f7772o != null && mo11166b(2) != -1) {
            this.f7772o.mo16837a(list);
        }
    }

    /* renamed from: a */
    public void mo11160a(RenderBuilder yaVar) {
        this.f7758a = yaVar;
        if (this.f7758a != null && this.f7770m == null) {
            this.f7771n = new adb(this.f7758a.mo16829a(), this);
            this.f7771n.mo477a();
        }
        this.f7766i = false;
        mo11172d();
    }

    /* renamed from: a */
    public void mo11161a(ExoPlayerListener ydVar) {
        if (ydVar != null) {
            this.f7761d.add(ydVar);
        }
    }

    /* renamed from: a */
    public void mo11162a(Id3MetadataListener yeVar) {
        this.f7773p = yeVar;
    }

    /* renamed from: a */
    public void mo11163a(OnBufferUpdateListener ylVar) {
        this.f7776s = ylVar;
        m10211d(ylVar != null);
    }

    /* renamed from: a */
    public void mo11164a(boolean z) {
        this.f7759b.mo354a(z);
        mo11170b(z);
    }

    /* renamed from: a */
    public void mo368a(boolean z, int i) {
        m10212q();
    }

    /* renamed from: a */
    public void mo11165a(acy[] acyArr, ajq ajq) {
        for (int i = 0; i < 4; i++) {
            if (acyArr[i] == null) {
                acyArr[i] = new acj();
            }
        }
        this.f7768k = acyArr[0];
        this.f7769l = acyArr[1];
        m10210c(false);
        this.f7759b.mo355a(acyArr);
        this.f7763f = RenderBuildingState.BUILT;
    }

    /* renamed from: b */
    public int mo11166b(int i) {
        return this.f7759b.mo357b(i);
    }

    /* renamed from: b */
    public Map<Integer, List<MediaFormat>> mo11167b() {
        int[] iArr;
        if (mo11176h() == 1) {
            return null;
        }
        ArrayMap dkVar = new ArrayMap();
        for (int i : new int[]{1, 0, 2, 3}) {
            int a = mo11150a(i);
            ArrayList arrayList = new ArrayList(a);
            dkVar.put(Integer.valueOf(i), arrayList);
            for (int i2 = 0; i2 < a; i2++) {
                arrayList.add(mo11151a(i, i2));
            }
        }
        return dkVar;
    }

    /* renamed from: b */
    public void mo11168b(int i, int i2) {
        this.f7759b.mo358b(i, i2);
        if (i == 2 && i2 == -1 && this.f7772o != null) {
            this.f7772o.mo16837a(Collections.emptyList());
        }
    }

    /* renamed from: b */
    public void mo485b(int i, long j) {
    }

    /* renamed from: b */
    public void mo407b(int i, long j, long j2) {
        if (this.f7774q != null) {
            this.f7774q.mo16846a(i, j, j2);
        }
    }

    /* renamed from: b */
    public void mo422b(Surface surface) {
    }

    /* renamed from: b */
    public void mo661b(Exception exc) {
        if (this.f7774q != null) {
            this.f7774q.mo16853b(exc);
        }
    }

    /* renamed from: b */
    public void mo893a(List<ahy> list) {
        if (this.f7773p != null && mo11166b(3) != -1) {
            this.f7773p.mo16785a(list);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo11170b(boolean z) {
        if (this.f7777t != null) {
            if (z && !this.f7777t.isHeld()) {
                this.f7777t.acquire();
            } else if (!z && this.f7777t.isHeld()) {
                this.f7777t.release();
            }
        }
    }

    /* renamed from: c */
    public void mo11171c() {
        this.f7766i = false;
    }

    /* renamed from: c */
    public void mo486c(int i, long j, long j2) {
    }

    /* renamed from: d */
    public void mo11172d() {
        if (!this.f7766i && this.f7758a != null) {
            if (this.f7763f == RenderBuildingState.BUILT) {
                this.f7759b.mo361d();
            }
            this.f7768k = null;
            this.f7763f = RenderBuildingState.BUILDING;
            m10212q();
            this.f7758a.mo16831b(this);
            this.f7766i = true;
            this.f7762e.set(false);
        }
    }

    /* renamed from: e */
    public void mo11173e() {
        if (!this.f7762e.getAndSet(true)) {
            this.f7759b.mo354a(false);
            this.f7759b.mo361d();
        }
    }

    /* renamed from: f */
    public boolean mo11174f() {
        int h = mo11176h();
        if (h != 1 && h != 5) {
            return false;
        }
        mo11154a(0);
        mo11164a(true);
        mo11171c();
        mo11172d();
        return true;
    }

    /* renamed from: g */
    public void mo11175g() {
        if (this.f7758a != null) {
            this.f7758a.mo16830b();
        }
        if (this.f7771n != null) {
            this.f7771n.mo478b();
            this.f7771n = null;
        }
        m10211d(false);
        this.f7763f = RenderBuildingState.IDLE;
        this.f7761d.clear();
        this.f7767j = null;
        this.f7759b.mo362e();
        mo11170b(false);
    }

    /* renamed from: h */
    public int mo11176h() {
        if (this.f7763f == RenderBuildingState.BUILDING) {
            return 2;
        }
        return this.f7759b.mo356b();
    }

    /* renamed from: i */
    public long mo11177i() {
        return this.f7759b.mo364g();
    }

    /* renamed from: j */
    public long mo11178j() {
        return this.f7759b.mo363f();
    }

    /* renamed from: k */
    public int mo11179k() {
        return this.f7759b.mo365h();
    }

    /* renamed from: l */
    public boolean mo11180l() {
        return this.f7759b.mo360c();
    }

    /* renamed from: m */
    public Looper mo11181m() {
        return this.f7759b.mo349a();
    }

    /* renamed from: n */
    public Handler mo11182n() {
        return this.f7760c;
    }

    /* renamed from: o */
    public void mo662o() {
    }

    /* renamed from: p */
    public void mo369p() {
    }
}
