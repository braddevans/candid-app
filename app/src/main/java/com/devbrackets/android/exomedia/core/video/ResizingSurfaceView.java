package com.devbrackets.android.exomedia.core.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import java.util.concurrent.locks.ReentrantLock;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

public class ResizingSurfaceView extends SurfaceView implements ClearableSurface {

    /* renamed from: k */
    private static final int[] f7784k = {12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, 4, 12344, 0, 12344};

    /* renamed from: l */
    private static final int[] f7785l = {12440, 2, 12344};

    /* renamed from: a */
    protected C1962c f7786a;

    /* renamed from: b */
    protected Point f7787b = new Point(0, 0);

    /* renamed from: c */
    protected Point f7788c = new Point(0, 0);

    /* renamed from: d */
    protected MatrixManager f7789d = new MatrixManager();

    /* renamed from: e */
    protected C1960a f7790e = new C1960a();

    /* renamed from: f */
    protected C1961b f7791f = new C1961b();

    /* renamed from: g */
    protected final ReentrantLock f7792g = new ReentrantLock(true);

    /* renamed from: h */
    protected int f7793h = 0;

    /* renamed from: i */
    protected int f7794i = 0;

    /* renamed from: j */
    protected boolean f7795j = true;

    /* renamed from: com.devbrackets.android.exomedia.core.video.ResizingSurfaceView$a */
    class C1960a implements OnAttachStateChangeListener {
        private C1960a() {
        }

        public void onViewAttachedToWindow(View view) {
            ResizingSurfaceView.this.f7792g.lock();
            ResizingSurfaceView.this.getViewTreeObserver().addOnGlobalLayoutListener(ResizingSurfaceView.this.f7791f);
            ResizingSurfaceView.this.removeOnAttachStateChangeListener(this);
            ResizingSurfaceView.this.f7792g.unlock();
        }

        public void onViewDetachedFromWindow(View view) {
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.core.video.ResizingSurfaceView$b */
    class C1961b implements OnGlobalLayoutListener {
        private C1961b() {
        }

        public void onGlobalLayout() {
            ResizingSurfaceView.this.setScaleType(ResizingSurfaceView.this.f7789d.mo16882b());
            if (VERSION.SDK_INT >= 16) {
                ResizingSurfaceView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            } else {
                ResizingSurfaceView.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.core.video.ResizingSurfaceView$c */
    public interface C1962c {
        /* renamed from: a */
        void mo11204a(int i, int i2);
    }

    public ResizingSurfaceView(Context context) {
        super(context);
    }

    public ResizingSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ResizingSurfaceView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @TargetApi(21)
    public ResizingSurfaceView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo11189b(int i, int i2) {
        this.f7789d.mo16876a(i, i2);
        mo11192e();
        this.f7788c.x = i;
        this.f7788c.y = i2;
        if (i == 0 || i2 == 0) {
            return false;
        }
        SurfaceHolder holder = getHolder();
        if (holder != null) {
            holder.setFixedSize(i, i2);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11190c(int i, int i2) {
        if (this.f7787b.x != i || this.f7787b.y != i2) {
            this.f7787b.x = i;
            this.f7787b.y = i2;
            if (this.f7786a != null) {
                this.f7786a.mo11204a(i, i2);
            }
        }
    }

    /* renamed from: d */
    public void mo11191d() {
        try {
            EGL10 egl10 = (EGL10) EGLContext.getEGL();
            EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            egl10.eglInitialize(eglGetDisplay, null);
            EGLConfig[] eGLConfigArr = new EGLConfig[1];
            egl10.eglChooseConfig(eglGetDisplay, f7784k, eGLConfigArr, eGLConfigArr.length, new int[1]);
            EGLContext eglCreateContext = egl10.eglCreateContext(eglGetDisplay, eGLConfigArr[0], EGL10.EGL_NO_CONTEXT, f7785l);
            EGLSurface eglCreateWindowSurface = egl10.eglCreateWindowSurface(eglGetDisplay, eGLConfigArr[0], this, new int[]{12344});
            egl10.eglMakeCurrent(eglGetDisplay, eglCreateWindowSurface, eglCreateWindowSurface, eglCreateContext);
            egl10.eglSwapBuffers(eglGetDisplay, eglCreateWindowSurface);
            egl10.eglDestroySurface(eglGetDisplay, eglCreateWindowSurface);
            egl10.eglMakeCurrent(eglGetDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            egl10.eglDestroyContext(eglGetDisplay, eglCreateContext);
            egl10.eglTerminate(eglGetDisplay);
        } catch (Exception e) {
            Log.e("ResizingSurfaceView", "Error clearing surface", e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11192e() {
        this.f7792g.lock();
        if (getWindowToken() == null) {
            addOnAttachStateChangeListener(this.f7790e);
        } else {
            getViewTreeObserver().addOnGlobalLayoutListener(this.f7791f);
        }
        this.f7792g.unlock();
    }

    public ScaleType getScaleType() {
        return this.f7789d.mo16882b();
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        mo11192e();
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        if (!this.f7795j) {
            super.onMeasure(i, i2);
            mo11190c(getMeasuredWidth(), getMeasuredHeight());
            return;
        }
        int defaultSize = getDefaultSize(this.f7788c.x, i);
        int defaultSize2 = getDefaultSize(this.f7788c.y, i2);
        if (this.f7788c.x <= 0 || this.f7788c.y <= 0) {
            setMeasuredDimension(defaultSize, defaultSize2);
            mo11190c(defaultSize, defaultSize2);
            return;
        }
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            i3 = size;
            i4 = size2;
            if (this.f7788c.x * i4 < this.f7788c.y * i3) {
                i3 = (this.f7788c.x * i4) / this.f7788c.y;
            } else if (this.f7788c.x * i4 > this.f7788c.y * i3) {
                i4 = (this.f7788c.y * i3) / this.f7788c.x;
            }
        } else if (mode == 1073741824) {
            i3 = size;
            i4 = (this.f7788c.y * i3) / this.f7788c.x;
            if (mode2 == Integer.MIN_VALUE && i4 > size2) {
                i4 = size2;
            }
        } else if (mode2 == 1073741824) {
            i4 = size2;
            i3 = (this.f7788c.x * i4) / this.f7788c.y;
            if (mode == Integer.MIN_VALUE && i3 > size) {
                i3 = size;
            }
        } else {
            i3 = this.f7788c.x;
            i4 = this.f7788c.y;
            if (mode2 == Integer.MIN_VALUE && i4 > size2) {
                i4 = size2;
                i3 = (this.f7788c.x * i4) / this.f7788c.y;
            }
            if (mode == Integer.MIN_VALUE && i3 > size) {
                i3 = size;
                i4 = (this.f7788c.y * i3) / this.f7788c.x;
            }
        }
        setMeasuredDimension(i3, i4);
        mo11190c(i3, i4);
    }

    public void setMeasureBasedOnAspectRatioEnabled(boolean z) {
        this.f7795j = z;
        requestLayout();
    }

    public void setOnSizeChangeListener(C1962c cVar) {
        this.f7786a = cVar;
    }

    public void setScaleType(ScaleType scaleType) {
        this.f7789d.mo16881a((View) this, scaleType);
    }

    public void setVideoRotation(int i, int i2) {
        this.f7793h = i;
        this.f7794i = i2;
        this.f7789d.mo16879a((View) this, (i + i2) % 360);
    }

    public void setVideoRotation(int i, boolean z) {
        int i2 = z ? i : this.f7793h;
        if (z) {
            i = this.f7794i;
        }
        setVideoRotation(i2, i);
    }
}
