package com.devbrackets.android.exomedia.core.video.p005mp;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View.OnTouchListener;
import android.widget.MediaController.MediaPlayerControl;
import com.devbrackets.android.exomedia.core.video.ResizingSurfaceView;
import com.devbrackets.android.exomedia.core.video.p005mp.NativeVideoDelegate.C1971a;
import com.google.android.exoplayer.MediaFormat;
import java.util.List;
import java.util.Map;

/* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeSurfaceVideoView */
public class NativeSurfaceVideoView extends ResizingSurfaceView implements MediaPlayerControl, C1971a, VideoViewApi {

    /* renamed from: k */
    protected OnTouchListener f7816k;

    /* renamed from: l */
    protected NativeVideoDelegate f7817l;

    /* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeSurfaceVideoView$a */
    public class C1969a implements Callback {
        protected C1969a() {
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            NativeSurfaceVideoView.this.f7817l.mo11290a(i2, i3);
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            NativeSurfaceVideoView.this.f7817l.mo11299a(surfaceHolder.getSurface());
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            surfaceHolder.getSurface().release();
            NativeSurfaceVideoView.this.mo11253f();
        }
    }

    public NativeSurfaceVideoView(Context context) {
        super(context);
        mo11248a(context, (AttributeSet) null);
    }

    public NativeSurfaceVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo11248a(context, attributeSet);
    }

    public NativeSurfaceVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo11248a(context, attributeSet);
    }

    @TargetApi(21)
    public NativeSurfaceVideoView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        mo11248a(context, attributeSet);
    }

    /* renamed from: a */
    public void mo11220a() {
        this.f7817l.mo11289a();
    }

    /* renamed from: a */
    public void mo11221a(int i, int i2) {
        if (mo11189b(i, i2)) {
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11248a(Context context, AttributeSet attributeSet) {
        this.f7817l = new NativeVideoDelegate(context, this, this);
        getHolder().addCallback(new C1969a());
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        mo11189b(0, 0);
    }

    /* renamed from: b */
    public boolean mo11222b() {
        return this.f7817l.mo11302c();
    }

    /* renamed from: c */
    public void mo11223c() {
    }

    public boolean canPause() {
        return this.f7817l.canPause();
    }

    public boolean canSeekBackward() {
        return this.f7817l.canSeekBackward();
    }

    public boolean canSeekForward() {
        return this.f7817l.canSeekForward();
    }

    /* renamed from: d */
    public void mo11252d(int i, int i2) {
        if (mo11189b(i, i2)) {
            requestLayout();
        }
    }

    /* renamed from: f */
    public void mo11253f() {
        this.f7817l.mo11301b();
    }

    public int getAudioSessionId() {
        return this.f7817l.getAudioSessionId();
    }

    public Map<Integer, List<MediaFormat>> getAvailableTracks() {
        return null;
    }

    public int getBufferPercentage() {
        return this.f7817l.getBufferPercentage();
    }

    public int getBufferedPercent() {
        return getBufferPercentage();
    }

    public int getCurrentPosition() {
        return this.f7817l.getCurrentPosition();
    }

    public int getDuration() {
        return this.f7817l.getDuration();
    }

    public boolean isPlaying() {
        return this.f7817l.isPlaying();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (this.f7816k != null) {
            z = this.f7816k.onTouch(this, motionEvent);
        }
        return z || super.onTouchEvent(motionEvent);
    }

    public void pause() {
        this.f7817l.pause();
    }

    public void seekTo(int i) {
        this.f7817l.seekTo(i);
    }

    public void setDrmProvider(DrmProvider ywVar) {
    }

    public void setListenerMux(EMListenerMux xwVar) {
        this.f7817l.mo11300a(xwVar);
    }

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener onBufferingUpdateListener) {
        this.f7817l.mo11291a(onBufferingUpdateListener);
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        this.f7817l.mo11292a(onCompletionListener);
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.f7817l.mo11293a(onErrorListener);
    }

    public void setOnInfoListener(OnInfoListener onInfoListener) {
        this.f7817l.mo11294a(onInfoListener);
    }

    public void setOnPreparedListener(OnPreparedListener onPreparedListener) {
        this.f7817l.mo11295a(onPreparedListener);
    }

    public void setOnSeekCompleteListener(OnSeekCompleteListener onSeekCompleteListener) {
        this.f7817l.mo11296a(onSeekCompleteListener);
    }

    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.f7816k = onTouchListener;
        super.setOnTouchListener(onTouchListener);
    }

    public void setTrack(int i, int i2) {
    }

    public void setVideoURI(Uri uri) {
        setVideoURI(uri, null);
    }

    public void setVideoURI(Uri uri, Map<String, String> map) {
        this.f7817l.mo11298a(uri, map);
        requestLayout();
        invalidate();
    }

    public void setVideoUri(Uri uri) {
        setVideoUri(uri, null);
    }

    public void setVideoUri(Uri uri, RenderBuilder yaVar) {
        setVideoURI(uri);
    }

    public void start() {
        this.f7817l.start();
        requestFocus();
    }
}
