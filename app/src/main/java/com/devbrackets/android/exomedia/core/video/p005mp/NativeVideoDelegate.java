package com.devbrackets.android.exomedia.core.video.p005mp;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.util.Log;
import android.view.Surface;
import android.widget.MediaController.MediaPlayerControl;
import java.io.IOException;
import java.util.Map;

/* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeVideoDelegate */
public class NativeVideoDelegate implements MediaPlayerControl {

    /* renamed from: a */
    protected Map<String, String> f7822a;

    /* renamed from: b */
    protected State f7823b = State.IDLE;

    /* renamed from: c */
    protected Context f7824c;

    /* renamed from: d */
    protected C1971a f7825d;

    /* renamed from: e */
    protected ClearableSurface f7826e;

    /* renamed from: f */
    protected MediaPlayer f7827f;

    /* renamed from: g */
    protected boolean f7828g = false;

    /* renamed from: h */
    protected int f7829h;

    /* renamed from: i */
    protected int f7830i;

    /* renamed from: j */
    protected EMListenerMux f7831j;

    /* renamed from: k */
    protected C1972b f7832k = new C1972b();

    /* renamed from: l */
    protected OnCompletionListener f7833l;

    /* renamed from: m */
    protected OnPreparedListener f7834m;

    /* renamed from: n */
    protected OnBufferingUpdateListener f7835n;

    /* renamed from: o */
    protected OnSeekCompleteListener f7836o;

    /* renamed from: p */
    protected OnErrorListener f7837p;

    /* renamed from: q */
    protected OnInfoListener f7838q;

    /* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeVideoDelegate$State */
    public enum State {
        ERROR,
        IDLE,
        PREPARING,
        PREPARED,
        PLAYING,
        PAUSED,
        COMPLETED
    }

    /* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeVideoDelegate$a */
    public interface C1971a {
        /* renamed from: d */
        void mo11252d(int i, int i2);
    }

    /* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeVideoDelegate$b */
    public class C1972b implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener, OnSeekCompleteListener, OnVideoSizeChangedListener {
        public C1972b() {
        }

        public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
            NativeVideoDelegate.this.f7830i = i;
            if (NativeVideoDelegate.this.f7835n != null) {
                NativeVideoDelegate.this.f7835n.onBufferingUpdate(mediaPlayer, i);
            }
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            NativeVideoDelegate.this.f7823b = State.COMPLETED;
            if (NativeVideoDelegate.this.f7833l != null) {
                NativeVideoDelegate.this.f7833l.onCompletion(NativeVideoDelegate.this.f7827f);
            }
        }

        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            Log.d("ContentValues", "Error: " + i + "," + i2);
            NativeVideoDelegate.this.f7823b = State.ERROR;
            return NativeVideoDelegate.this.f7837p == null || NativeVideoDelegate.this.f7837p.onError(NativeVideoDelegate.this.f7827f, i, i2);
        }

        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
            return NativeVideoDelegate.this.f7838q == null || NativeVideoDelegate.this.f7838q.onInfo(mediaPlayer, i, i2);
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            NativeVideoDelegate.this.f7823b = State.PREPARED;
            if (NativeVideoDelegate.this.f7834m != null) {
                NativeVideoDelegate.this.f7834m.onPrepared(NativeVideoDelegate.this.f7827f);
            }
            NativeVideoDelegate.this.f7825d.mo11252d(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
            if (NativeVideoDelegate.this.f7829h != 0) {
                NativeVideoDelegate.this.seekTo(NativeVideoDelegate.this.f7829h);
            }
            if (NativeVideoDelegate.this.f7828g) {
                NativeVideoDelegate.this.start();
            }
        }

        public void onSeekComplete(MediaPlayer mediaPlayer) {
            if (NativeVideoDelegate.this.f7836o != null) {
                NativeVideoDelegate.this.f7836o.onSeekComplete(mediaPlayer);
            }
        }

        public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
            NativeVideoDelegate.this.f7825d.mo11252d(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
        }
    }

    public NativeVideoDelegate(Context context, C1971a aVar, ClearableSurface yiVar) {
        this.f7824c = context;
        this.f7825d = aVar;
        this.f7826e = yiVar;
        mo11306d();
        this.f7823b = State.IDLE;
    }

    /* renamed from: a */
    public void mo11289a() {
        this.f7823b = State.IDLE;
        if (mo11307e()) {
            try {
                this.f7827f.stop();
            } catch (Exception e) {
                Log.d("ContentValues", "stopPlayback: error calling mediaPlayer.stop()", e);
            }
        }
        this.f7828g = false;
        this.f7831j.mo16787a(this.f7826e);
    }

    /* renamed from: a */
    public void mo11290a(int i, int i2) {
        if (this.f7827f != null && i > 0 && i2 > 0) {
            if (this.f7829h != 0) {
                seekTo(this.f7829h);
            }
            if (this.f7828g) {
                start();
            }
        }
    }

    /* renamed from: a */
    public void mo11291a(OnBufferingUpdateListener onBufferingUpdateListener) {
        this.f7835n = onBufferingUpdateListener;
    }

    /* renamed from: a */
    public void mo11292a(OnCompletionListener onCompletionListener) {
        this.f7833l = onCompletionListener;
    }

    /* renamed from: a */
    public void mo11293a(OnErrorListener onErrorListener) {
        this.f7837p = onErrorListener;
    }

    /* renamed from: a */
    public void mo11294a(OnInfoListener onInfoListener) {
        this.f7838q = onInfoListener;
    }

    /* renamed from: a */
    public void mo11295a(OnPreparedListener onPreparedListener) {
        this.f7834m = onPreparedListener;
    }

    /* renamed from: a */
    public void mo11296a(OnSeekCompleteListener onSeekCompleteListener) {
        this.f7836o = onSeekCompleteListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11297a(Uri uri) {
        if (uri != null) {
            this.f7830i = 0;
            try {
                this.f7827f.setDataSource(this.f7824c.getApplicationContext(), uri, this.f7822a);
                this.f7827f.prepareAsync();
                this.f7823b = State.PREPARING;
            } catch (IOException | IllegalArgumentException e) {
                Log.w("ContentValues", "Unable to open content: " + uri, e);
                this.f7823b = State.ERROR;
                this.f7832k.onError(this.f7827f, 1, 0);
            }
        }
    }

    /* renamed from: a */
    public void mo11298a(Uri uri, Map<String, String> map) {
        this.f7822a = map;
        this.f7829h = 0;
        this.f7828g = false;
        mo11297a(uri);
    }

    /* renamed from: a */
    public void mo11299a(Surface surface) {
        this.f7827f.setSurface(surface);
        if (this.f7828g) {
            start();
        }
    }

    /* renamed from: a */
    public void mo11300a(EMListenerMux xwVar) {
        this.f7831j = xwVar;
        mo11292a((OnCompletionListener) xwVar);
        mo11295a((OnPreparedListener) xwVar);
        mo11291a((OnBufferingUpdateListener) xwVar);
        mo11296a((OnSeekCompleteListener) xwVar);
        mo11293a((OnErrorListener) xwVar);
    }

    /* renamed from: b */
    public void mo11301b() {
        this.f7823b = State.IDLE;
        try {
            this.f7827f.reset();
            this.f7827f.release();
        } catch (Exception e) {
            Log.d("ContentValues", "stopPlayback: error calling mediaPlayer.reset() or mediaPlayer.release()", e);
        }
        this.f7828g = false;
    }

    /* renamed from: c */
    public boolean mo11302c() {
        if (this.f7823b != State.COMPLETED) {
            return false;
        }
        seekTo(0);
        start();
        this.f7831j.mo16793a(false);
        this.f7831j.mo16795b(false);
        return true;
    }

    public boolean canPause() {
        return this.f7823b == State.PREPARED || this.f7823b == State.PLAYING;
    }

    public boolean canSeekBackward() {
        return this.f7823b == State.PREPARED || this.f7823b == State.PLAYING || this.f7823b == State.PAUSED;
    }

    public boolean canSeekForward() {
        return this.f7823b == State.PREPARED || this.f7823b == State.PLAYING || this.f7823b == State.PAUSED;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11306d() {
        this.f7827f = new MediaPlayer();
        this.f7827f.setOnInfoListener(this.f7832k);
        this.f7827f.setOnErrorListener(this.f7832k);
        this.f7827f.setOnPreparedListener(this.f7832k);
        this.f7827f.setOnCompletionListener(this.f7832k);
        this.f7827f.setOnSeekCompleteListener(this.f7832k);
        this.f7827f.setOnBufferingUpdateListener(this.f7832k);
        this.f7827f.setOnVideoSizeChangedListener(this.f7832k);
        this.f7827f.setAudioStreamType(3);
        this.f7827f.setScreenOnWhilePlaying(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public boolean mo11307e() {
        return (this.f7823b == State.ERROR || this.f7823b == State.IDLE || this.f7823b == State.PREPARING) ? false : true;
    }

    public int getAudioSessionId() {
        return this.f7827f.getAudioSessionId();
    }

    public int getBufferPercentage() {
        if (this.f7827f != null) {
            return this.f7830i;
        }
        return 0;
    }

    public int getCurrentPosition() {
        if (!this.f7831j.mo16796b() || !mo11307e()) {
            return 0;
        }
        return this.f7827f.getCurrentPosition();
    }

    public int getDuration() {
        if (!this.f7831j.mo16796b() || !mo11307e()) {
            return 0;
        }
        return this.f7827f.getDuration();
    }

    public boolean isPlaying() {
        return mo11307e() && this.f7827f.isPlaying();
    }

    public void pause() {
        if (mo11307e() && this.f7827f.isPlaying()) {
            this.f7827f.pause();
            this.f7823b = State.PAUSED;
        }
        this.f7828g = false;
    }

    public void seekTo(int i) {
        if (mo11307e()) {
            this.f7827f.seekTo(i);
            this.f7829h = 0;
            return;
        }
        this.f7829h = i;
    }

    public void start() {
        if (mo11307e()) {
            this.f7827f.start();
            this.f7823b = State.PLAYING;
        }
        this.f7828g = true;
        this.f7831j.mo16795b(false);
    }
}
