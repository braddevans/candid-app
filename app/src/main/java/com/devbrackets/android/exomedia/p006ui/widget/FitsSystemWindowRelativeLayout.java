package com.devbrackets.android.exomedia.p006ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.WindowInsets;
import android.widget.RelativeLayout;

/* renamed from: com.devbrackets.android.exomedia.ui.widget.FitsSystemWindowRelativeLayout */
public class FitsSystemWindowRelativeLayout extends RelativeLayout {

    /* renamed from: a */
    private Rect f7898a;

    public FitsSystemWindowRelativeLayout(Context context) {
        super(context);
        m10353a();
    }

    public FitsSystemWindowRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m10353a();
    }

    public FitsSystemWindowRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m10353a();
    }

    @TargetApi(21)
    public FitsSystemWindowRelativeLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m10353a();
    }

    /* renamed from: a */
    private void m10353a() {
        if (VERSION.SDK_INT >= 14) {
            setFitsSystemWindows(true);
        }
        if (this.f7898a == null) {
            this.f7898a = new Rect(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        m10354a(new Rect());
    }

    /* renamed from: a */
    private void m10354a(Rect rect) {
        int i = this.f7898a.top + rect.top;
        setPadding(this.f7898a.left, i, this.f7898a.right + rect.right, this.f7898a.bottom + rect.bottom);
    }

    /* renamed from: b */
    private boolean m10355b() {
        return VERSION.SDK_INT >= 13 && getResources().getConfiguration().smallestScreenWidthDp <= 600;
    }

    /* access modifiers changed from: protected */
    public boolean fitSystemWindows(Rect rect) {
        m10354a(rect);
        return false;
    }

    @TargetApi(20)
    public WindowInsets onApplyWindowInsets(WindowInsets windowInsets) {
        fitSystemWindows(new Rect(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom()));
        return windowInsets;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (m10355b()) {
            m10353a();
        }
    }
}
