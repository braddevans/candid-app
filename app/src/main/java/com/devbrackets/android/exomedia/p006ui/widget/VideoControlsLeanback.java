package com.devbrackets.android.exomedia.p006ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.devbrackets.android.exomedia.p006ui.widget.VideoControls.C1982a;

@TargetApi(21)
/* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControlsLeanback */
public class VideoControlsLeanback extends VideoControls {

    /* renamed from: D */
    protected ProgressBar f7935D;

    /* renamed from: E */
    protected ImageView f7936E;

    /* renamed from: F */
    protected ViewGroup f7937F;

    /* renamed from: G */
    protected ImageButton f7938G;

    /* renamed from: H */
    protected ImageButton f7939H;

    /* renamed from: I */
    protected Drawable f7940I;

    /* renamed from: J */
    protected Drawable f7941J;

    /* renamed from: K */
    protected View f7942K;

    /* renamed from: L */
    protected C1985a f7943L = new C1985a();

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControlsLeanback$a */
    public class C1985a implements OnFocusChangeListener {
        protected C1985a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo11456a(View view) {
            int[] iArr = new int[2];
            view.getLocationOnScreen(iArr);
            int i = iArr[0];
            VideoControlsLeanback.this.f7936E.getLocationOnScreen(iArr);
            return (i - ((VideoControlsLeanback.this.f7936E.getWidth() - view.getWidth()) / 2)) - iArr[0];
        }

        public void onFocusChange(View view, boolean z) {
            if (z) {
                VideoControlsLeanback.this.f7936E.startAnimation(new C1988d(mo11456a(view)));
            }
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControlsLeanback$b */
    public class C1986b extends C1982a {
        protected C1986b() {
            super();
        }

        /* renamed from: d */
        public boolean mo11443d() {
            if (VideoControlsLeanback.this.f7919r == null) {
                return false;
            }
            int currentPosition = VideoControlsLeanback.this.f7919r.getCurrentPosition() - 10000;
            if (currentPosition < 0) {
                currentPosition = 0;
            }
            VideoControlsLeanback.this.mo11446a(currentPosition);
            return true;
        }

        /* renamed from: e */
        public boolean mo11444e() {
            if (VideoControlsLeanback.this.f7919r == null) {
                return false;
            }
            int currentPosition = VideoControlsLeanback.this.f7919r.getCurrentPosition() + 10000;
            if (currentPosition > VideoControlsLeanback.this.f7935D.getMax()) {
                currentPosition = VideoControlsLeanback.this.f7935D.getMax();
            }
            VideoControlsLeanback.this.mo11446a(currentPosition);
            return true;
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControlsLeanback$c */
    public class C1987c implements OnKeyListener {
        protected C1987c() {
        }

        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() != 0) {
                return false;
            }
            switch (i) {
                case 4:
                    if (!VideoControlsLeanback.this.f7899A || !VideoControlsLeanback.this.f7900B || VideoControlsLeanback.this.f7927z) {
                        return VideoControlsLeanback.this.f7937F.getAnimation() != null;
                    }
                    VideoControlsLeanback.this.mo11385a(0);
                    return true;
                case 19:
                    VideoControlsLeanback.this.mo11451o();
                    return true;
                case 20:
                    VideoControlsLeanback.this.mo11385a(0);
                    return true;
                case 21:
                    VideoControlsLeanback.this.mo11451o();
                    VideoControlsLeanback.this.mo11448b(VideoControlsLeanback.this.f7942K);
                    return true;
                case 22:
                    VideoControlsLeanback.this.mo11451o();
                    VideoControlsLeanback.this.mo11447a(VideoControlsLeanback.this.f7942K);
                    return true;
                case 23:
                    VideoControlsLeanback.this.mo11451o();
                    VideoControlsLeanback.this.f7942K.callOnClick();
                    return true;
                case 85:
                    VideoControlsLeanback.this.mo11396g();
                    return true;
                case 87:
                    VideoControlsLeanback.this.mo11400i();
                    return true;
                case 88:
                    VideoControlsLeanback.this.mo11399h();
                    return true;
                case 89:
                    VideoControlsLeanback.this.mo11449m();
                    return true;
                case 90:
                    VideoControlsLeanback.this.mo11450n();
                    return true;
                case 126:
                    if (VideoControlsLeanback.this.f7919r == null || VideoControlsLeanback.this.f7919r.mo11331b()) {
                        return false;
                    }
                    VideoControlsLeanback.this.f7919r.mo11333c();
                    return true;
                case 127:
                    if (VideoControlsLeanback.this.f7919r == null || !VideoControlsLeanback.this.f7919r.mo11331b()) {
                        return false;
                    }
                    VideoControlsLeanback.this.f7919r.mo11334d();
                    return true;
                default:
                    return false;
            }
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControlsLeanback$d */
    public class C1988d extends TranslateAnimation implements AnimationListener {

        /* renamed from: a */
        protected int f7949a;

        public C1988d(int i) {
            super(0.0f, (float) i, 0.0f, 0.0f);
            this.f7949a = i;
            setDuration(250);
            setAnimationListener(this);
        }

        public void onAnimationEnd(Animation animation) {
            VideoControlsLeanback.this.f7936E.setX(VideoControlsLeanback.this.f7936E.getX() + ((float) this.f7949a));
            VideoControlsLeanback.this.f7936E.clearAnimation();
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    public VideoControlsLeanback(Context context) {
        super(context);
    }

    public VideoControlsLeanback(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public VideoControlsLeanback(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11384a() {
        if (this.f7899A) {
            boolean j = mo11401j();
            if (this.f7901C && j && this.f7912k.getVisibility() == 0) {
                this.f7912k.clearAnimation();
                this.f7912k.startAnimation(new BottomViewHideShowAnimation(this.f7912k, false, 300));
            } else if ((!this.f7901C || !j) && this.f7912k.getVisibility() != 0) {
                this.f7912k.clearAnimation();
                this.f7912k.startAnimation(new BottomViewHideShowAnimation(this.f7912k, true, 300));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11446a(int i) {
        if (this.f7920s == null || !this.f7920s.mo11440a(i)) {
            this.f7923v.mo11440a(i);
        }
    }

    /* renamed from: a */
    public void mo11386a(long j, long j2, int i) {
        this.f7935D.setSecondaryProgress((int) (((float) this.f7935D.getMax()) * (((float) i) / 100.0f)));
        this.f7935D.setProgress((int) j);
        this.f7902a.setText(TimeFormatUtil.m17258a(j));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11447a(View view) {
        int nextFocusRightId = view.getNextFocusRightId();
        if (nextFocusRightId != -1) {
            View findViewById = findViewById(nextFocusRightId);
            if (findViewById.getVisibility() != 0) {
                mo11447a(findViewById);
                return;
            }
            findViewById.requestFocus();
            this.f7942K = findViewById;
            this.f7943L.onFocusChange(findViewById, true);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11387a(boolean z) {
        if (this.f7899A != z) {
            if (!this.f7927z) {
                this.f7937F.startAnimation(new BottomViewHideShowAnimation(this.f7937F, z, 300));
            }
            this.f7899A = z;
            mo11402k();
        }
    }

    /* renamed from: b */
    public void mo11388b() {
        boolean z = false;
        if (this.f7927z) {
            this.f7927z = false;
            this.f7911j.setVisibility(0);
            this.f7936E.setVisibility(0);
            this.f7910i.setVisibility(8);
            if (this.f7919r != null && this.f7919r.mo11331b()) {
                z = true;
            }
            mo11391c(z);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo11448b(View view) {
        int nextFocusLeftId = view.getNextFocusLeftId();
        if (nextFocusLeftId != -1) {
            View findViewById = findViewById(nextFocusLeftId);
            if (findViewById.getVisibility() != 0) {
                mo11448b(findViewById);
                return;
            }
            findViewById.requestFocus();
            this.f7942K = findViewById;
            this.f7943L.onFocusChange(findViewById, true);
        }
    }

    /* renamed from: b */
    public void mo11389b(boolean z) {
        if (!this.f7927z) {
            this.f7927z = true;
            this.f7911j.setVisibility(8);
            this.f7936E.setVisibility(8);
            this.f7910i.setVisibility(0);
            mo11390c();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11392d() {
        super.mo11392d();
        this.f7935D = (ProgressBar) findViewById(C3078c.exomedia_controls_video_progress);
        this.f7939H = (ImageButton) findViewById(C3078c.exomedia_controls_rewind_btn);
        this.f7938G = (ImageButton) findViewById(C3078c.exomedia_controls_fast_forward_btn);
        this.f7936E = (ImageView) findViewById(C3078c.exomedia_controls_leanback_ripple);
        this.f7937F = (ViewGroup) findViewById(C3078c.exomedia_controls_parent);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11394e() {
        super.mo11394e();
        this.f7939H.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VideoControlsLeanback.this.mo11449m();
            }
        });
        this.f7938G.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VideoControlsLeanback.this.mo11450n();
            }
        });
        this.f7908g.setOnFocusChangeListener(this.f7943L);
        this.f7939H.setOnFocusChangeListener(this.f7943L);
        this.f7907f.setOnFocusChangeListener(this.f7943L);
        this.f7938G.setOnFocusChangeListener(this.f7943L);
        this.f7909h.setOnFocusChangeListener(this.f7943L);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11395f() {
        super.mo11395f();
        this.f7940I = EMResourceUtil.m17217a(getContext(), C3077b.exomedia_ic_rewind_white, C3076a.exomedia_default_controls_button_selector);
        this.f7939H.setImageDrawable(this.f7940I);
        this.f7941J = EMResourceUtil.m17217a(getContext(), C3077b.exomedia_ic_fast_forward_white, C3076a.exomedia_default_controls_button_selector);
        this.f7938G.setImageDrawable(this.f7941J);
    }

    /* access modifiers changed from: protected */
    public int getLayoutResource() {
        return C3079d.exomedia_default_controls_leanback;
    }

    /* access modifiers changed from: protected */
    /* renamed from: m */
    public void mo11449m() {
        if (this.f7921t == null || !this.f7921t.mo11443d()) {
            this.f7923v.mo11443d();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo11450n() {
        if (this.f7921t == null || !this.f7921t.mo11444e()) {
            this.f7923v.mo11444e();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public void mo11451o() {
        mo11390c();
        if (this.f7919r != null && this.f7919r.mo11331b()) {
            mo11385a(2000);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f7907f.requestFocus();
        this.f7942K = this.f7907f;
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public void mo11453p() {
        C1987c cVar = new C1987c();
        setOnKeyListener(cVar);
        this.f7907f.setOnKeyListener(cVar);
        this.f7908g.setOnKeyListener(cVar);
        this.f7909h.setOnKeyListener(cVar);
        this.f7939H.setOnKeyListener(cVar);
        this.f7938G.setOnKeyListener(cVar);
    }

    public void setDuration(long j) {
        if (j != ((long) this.f7935D.getMax())) {
            this.f7903b.setText(TimeFormatUtil.m17258a(j));
            this.f7935D.setMax((int) j);
        }
    }

    public void setFastForwardButtonEnabled(boolean z) {
        if (this.f7938G != null) {
            this.f7938G.setEnabled(z);
        }
    }

    public void setFastForwardButtonRemoved(boolean z) {
        if (this.f7938G != null) {
            this.f7938G.setVisibility(z ? 8 : 0);
        }
    }

    public void setFastForwardDrawable(Drawable drawable) {
        if (this.f7938G != null) {
            this.f7938G.setImageDrawable(drawable);
        }
    }

    public void setFastForwardImageResource(int i) {
        if (this.f7938G != null) {
            if (i != 0) {
                this.f7938G.setImageResource(i);
            } else {
                this.f7938G.setImageDrawable(this.f7941J);
            }
        }
    }

    public void setPosition(long j) {
        this.f7902a.setText(TimeFormatUtil.m17258a(j));
        this.f7935D.setProgress((int) j);
    }

    public void setRewindButtonEnabled(boolean z) {
        if (this.f7939H != null) {
            this.f7939H.setEnabled(z);
        }
    }

    public void setRewindButtonRemoved(boolean z) {
        if (this.f7939H != null) {
            this.f7939H.setVisibility(z ? 8 : 0);
        }
    }

    public void setRewindDrawable(Drawable drawable) {
        if (this.f7939H != null) {
            this.f7939H.setImageDrawable(drawable);
        }
    }

    public void setRewindImageResource(int i) {
        if (this.f7939H != null) {
            if (i != 0) {
                this.f7939H.setImageResource(i);
            } else {
                this.f7939H.setImageDrawable(this.f7940I);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setup(Context context) {
        super.setup(context);
        this.f7923v = new C1986b();
        mo11453p();
        setFocusable(true);
    }
}
