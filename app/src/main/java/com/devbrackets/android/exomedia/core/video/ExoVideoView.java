package com.devbrackets.android.exomedia.core.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import com.devbrackets.android.exomedia.core.video.exo.ExoTextureVideoView;

@TargetApi(16)
@Deprecated
public class ExoVideoView extends ExoTextureVideoView {
    public ExoVideoView(Context context) {
        super(context);
    }

    public ExoVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ExoVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
