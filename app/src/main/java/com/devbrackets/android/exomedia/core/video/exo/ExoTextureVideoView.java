package com.devbrackets.android.exomedia.core.video.exo;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView.SurfaceTextureListener;
import com.devbrackets.android.exomedia.core.video.ResizingTextureView;
import com.google.android.exoplayer.MediaFormat;
import java.util.List;
import java.util.Map;

@TargetApi(16)
public class ExoTextureVideoView extends ResizingTextureView implements VideoViewApi {

    /* renamed from: k */
    protected ExoVideoDelegate f7814k;

    /* renamed from: com.devbrackets.android.exomedia.core.video.exo.ExoTextureVideoView$a */
    public class C1968a implements SurfaceTextureListener {
        protected C1968a() {
        }

        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
            ExoTextureVideoView.this.f7814k.mo16859a(new Surface(surfaceTexture));
        }

        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            ExoTextureVideoView.this.f7814k.mo16872k();
            surfaceTexture.release();
            return true;
        }

        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        }

        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }
    }

    public ExoTextureVideoView(Context context) {
        super(context);
        mo11242f();
    }

    public ExoTextureVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo11242f();
    }

    public ExoTextureVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo11242f();
    }

    public ExoTextureVideoView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        mo11242f();
    }

    /* renamed from: a */
    public void mo11220a() {
        this.f7814k.mo16866e();
    }

    /* renamed from: a */
    public void mo11221a(int i, int i2) {
        if (mo11205b(i, i2)) {
            requestLayout();
        }
    }

    /* renamed from: b */
    public boolean mo11222b() {
        return this.f7814k.mo16862a();
    }

    /* renamed from: c */
    public void mo11223c() {
        this.f7814k.mo16871j();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11242f() {
        this.f7814k = new ExoVideoDelegate(getContext(), this);
        setSurfaceTextureListener(new C1968a());
        mo11205b(0, 0);
    }

    public Map<Integer, List<MediaFormat>> getAvailableTracks() {
        return this.f7814k.mo16870i();
    }

    public int getBufferedPercent() {
        return this.f7814k.mo16869h();
    }

    public int getCurrentPosition() {
        return this.f7814k.mo16868g();
    }

    public int getDuration() {
        return this.f7814k.mo16867f();
    }

    public String getUserAgent() {
        return this.f7814k.mo16873l();
    }

    public boolean isPlaying() {
        return this.f7814k.mo16863b();
    }

    public void pause() {
        this.f7814k.mo16865d();
    }

    public void seekTo(int i) {
        this.f7814k.mo16855a(i);
    }

    public void setDrmProvider(DrmProvider ywVar) {
        this.f7814k.mo16861a(ywVar);
    }

    public void setListenerMux(EMListenerMux xwVar) {
        this.f7814k.mo16860a(xwVar);
    }

    public void setTrack(int i, int i2) {
        this.f7814k.mo16856a(i, i2);
    }

    public void setVideoUri(Uri uri) {
        this.f7814k.mo16857a(uri);
    }

    public void setVideoUri(Uri uri, RenderBuilder yaVar) {
        this.f7814k.mo16858a(uri, yaVar);
    }

    public void start() {
        this.f7814k.mo16864c();
    }
}
