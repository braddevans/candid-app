package com.devbrackets.android.exomedia.core.video.exo;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import com.devbrackets.android.exomedia.core.video.ResizingSurfaceView;
import com.google.android.exoplayer.MediaFormat;
import java.util.List;
import java.util.Map;

@TargetApi(16)
public class ExoSurfaceVideoView extends ResizingSurfaceView implements VideoViewApi {

    /* renamed from: k */
    protected ExoVideoDelegate f7812k;

    /* renamed from: com.devbrackets.android.exomedia.core.video.exo.ExoSurfaceVideoView$a */
    public class C1967a implements Callback {
        protected C1967a() {
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            ExoSurfaceVideoView.this.f7812k.mo16859a(surfaceHolder.getSurface());
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            ExoSurfaceVideoView.this.f7812k.mo16872k();
            surfaceHolder.getSurface().release();
        }
    }

    public ExoSurfaceVideoView(Context context) {
        super(context);
        mo11224f();
    }

    public ExoSurfaceVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo11224f();
    }

    public ExoSurfaceVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo11224f();
    }

    public ExoSurfaceVideoView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        mo11224f();
    }

    /* renamed from: a */
    public void mo11220a() {
        this.f7812k.mo16866e();
    }

    /* renamed from: a */
    public void mo11221a(int i, int i2) {
        if (mo11189b(i, i2)) {
            requestLayout();
        }
    }

    /* renamed from: b */
    public boolean mo11222b() {
        return this.f7812k.mo16862a();
    }

    /* renamed from: c */
    public void mo11223c() {
        this.f7812k.mo16871j();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11224f() {
        this.f7812k = new ExoVideoDelegate(getContext(), this);
        getHolder().addCallback(new C1967a());
        mo11189b(0, 0);
    }

    public Map<Integer, List<MediaFormat>> getAvailableTracks() {
        return this.f7812k.mo16870i();
    }

    public int getBufferedPercent() {
        return this.f7812k.mo16869h();
    }

    public int getCurrentPosition() {
        return this.f7812k.mo16868g();
    }

    public int getDuration() {
        return this.f7812k.mo16867f();
    }

    public String getUserAgent() {
        return this.f7812k.mo16873l();
    }

    public boolean isPlaying() {
        return this.f7812k.mo16863b();
    }

    public void pause() {
        this.f7812k.mo16865d();
    }

    public void seekTo(int i) {
        this.f7812k.mo16855a(i);
    }

    public void setDrmProvider(DrmProvider ywVar) {
        this.f7812k.mo16861a(ywVar);
    }

    public void setListenerMux(EMListenerMux xwVar) {
        this.f7812k.mo16860a(xwVar);
    }

    public void setTrack(int i, int i2) {
        this.f7812k.mo16856a(i, i2);
    }

    public void setVideoUri(Uri uri) {
        this.f7812k.mo16857a(uri);
    }

    public void setVideoUri(Uri uri, RenderBuilder yaVar) {
        this.f7812k.mo16858a(uri, yaVar);
    }

    public void start() {
        this.f7812k.mo16864c();
    }
}
