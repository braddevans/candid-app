package com.devbrackets.android.exomedia.core.video;

import android.content.Context;
import android.util.AttributeSet;
import com.devbrackets.android.exomedia.core.video.p005mp.NativeTextureVideoView;

@Deprecated
public class NativeVideoView extends NativeTextureVideoView {
    public NativeVideoView(Context context) {
        super(context);
    }

    public NativeVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
