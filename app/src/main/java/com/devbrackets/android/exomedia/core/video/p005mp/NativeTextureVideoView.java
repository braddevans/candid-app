package com.devbrackets.android.exomedia.core.video.p005mp;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View.OnTouchListener;
import android.widget.MediaController.MediaPlayerControl;
import com.devbrackets.android.exomedia.core.video.ResizingTextureView;
import com.devbrackets.android.exomedia.core.video.p005mp.NativeVideoDelegate.C1971a;
import com.google.android.exoplayer.MediaFormat;
import java.util.List;
import java.util.Map;

/* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeTextureVideoView */
public class NativeTextureVideoView extends ResizingTextureView implements MediaPlayerControl, C1971a, VideoViewApi {

    /* renamed from: k */
    protected OnTouchListener f7819k;

    /* renamed from: l */
    protected NativeVideoDelegate f7820l;

    /* renamed from: com.devbrackets.android.exomedia.core.video.mp.NativeTextureVideoView$a */
    public class C1970a implements SurfaceTextureListener {
        protected C1970a() {
        }

        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
            NativeTextureVideoView.this.f7820l.mo11299a(new Surface(surfaceTexture));
        }

        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            surfaceTexture.release();
            NativeTextureVideoView.this.mo11273f();
            return true;
        }

        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
            NativeTextureVideoView.this.f7820l.mo11290a(i, i2);
        }

        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }
    }

    public NativeTextureVideoView(Context context) {
        super(context);
        mo11269a(context, (AttributeSet) null);
    }

    public NativeTextureVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo11269a(context, attributeSet);
    }

    public NativeTextureVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo11269a(context, attributeSet);
    }

    @TargetApi(21)
    public NativeTextureVideoView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        mo11269a(context, attributeSet);
    }

    /* renamed from: a */
    public void mo11220a() {
        this.f7820l.mo11289a();
    }

    /* renamed from: a */
    public void mo11221a(int i, int i2) {
        if (mo11205b(i, i2)) {
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11269a(Context context, AttributeSet attributeSet) {
        this.f7820l = new NativeVideoDelegate(context, this, this);
        setSurfaceTextureListener(new C1970a());
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        mo11205b(0, 0);
    }

    /* renamed from: b */
    public boolean mo11222b() {
        return this.f7820l.mo11302c();
    }

    /* renamed from: c */
    public void mo11223c() {
    }

    public boolean canPause() {
        return this.f7820l.canPause();
    }

    public boolean canSeekBackward() {
        return this.f7820l.canSeekBackward();
    }

    public boolean canSeekForward() {
        return this.f7820l.canSeekForward();
    }

    /* renamed from: d */
    public void mo11252d(int i, int i2) {
        if (mo11205b(i, i2)) {
            requestLayout();
        }
    }

    /* renamed from: f */
    public void mo11273f() {
        this.f7820l.mo11301b();
    }

    public int getAudioSessionId() {
        return this.f7820l.getAudioSessionId();
    }

    public Map<Integer, List<MediaFormat>> getAvailableTracks() {
        return null;
    }

    public int getBufferPercentage() {
        return this.f7820l.getBufferPercentage();
    }

    public int getBufferedPercent() {
        return getBufferPercentage();
    }

    public int getCurrentPosition() {
        return this.f7820l.getCurrentPosition();
    }

    public int getDuration() {
        return this.f7820l.getDuration();
    }

    public boolean isPlaying() {
        return this.f7820l.isPlaying();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (this.f7819k != null) {
            z = this.f7819k.onTouch(this, motionEvent);
        }
        return z || super.onTouchEvent(motionEvent);
    }

    public void pause() {
        this.f7820l.pause();
    }

    public void seekTo(int i) {
        this.f7820l.seekTo(i);
    }

    public void setDrmProvider(DrmProvider ywVar) {
    }

    public void setListenerMux(EMListenerMux xwVar) {
        this.f7820l.mo11300a(xwVar);
    }

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener onBufferingUpdateListener) {
        this.f7820l.mo11291a(onBufferingUpdateListener);
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        this.f7820l.mo11292a(onCompletionListener);
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.f7820l.mo11293a(onErrorListener);
    }

    public void setOnInfoListener(OnInfoListener onInfoListener) {
        this.f7820l.mo11294a(onInfoListener);
    }

    public void setOnPreparedListener(OnPreparedListener onPreparedListener) {
        this.f7820l.mo11295a(onPreparedListener);
    }

    public void setOnSeekCompleteListener(OnSeekCompleteListener onSeekCompleteListener) {
        this.f7820l.mo11296a(onSeekCompleteListener);
    }

    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.f7819k = onTouchListener;
        super.setOnTouchListener(onTouchListener);
    }

    public void setTrack(int i, int i2) {
    }

    public void setVideoURI(Uri uri) {
        setVideoURI(uri, null);
    }

    public void setVideoURI(Uri uri, Map<String, String> map) {
        this.f7820l.mo11298a(uri, map);
        requestLayout();
        invalidate();
    }

    public void setVideoUri(Uri uri) {
        setVideoUri(uri, null);
    }

    public void setVideoUri(Uri uri, RenderBuilder yaVar) {
        setVideoURI(uri);
    }

    public void start() {
        this.f7820l.start();
        requestFocus();
    }
}
