package com.devbrackets.android.exomedia.p006ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import java.util.LinkedList;
import java.util.List;

/* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControlsMobile */
public class VideoControlsMobile extends VideoControls {

    /* renamed from: D */
    protected SeekBar f7951D;

    /* renamed from: E */
    protected LinearLayout f7952E;

    /* renamed from: F */
    protected boolean f7953F = false;

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControlsMobile$a */
    public class C1990a implements OnSeekBarChangeListener {

        /* renamed from: b */
        private int f7956b;

        protected C1990a() {
        }

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            if (z) {
                this.f7956b = i;
                if (VideoControlsMobile.this.f7902a != null) {
                    VideoControlsMobile.this.f7902a.setText(TimeFormatUtil.m17258a((long) this.f7956b));
                }
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            VideoControlsMobile.this.f7953F = true;
            if (VideoControlsMobile.this.f7920s == null || !VideoControlsMobile.this.f7920s.mo11445f()) {
                VideoControlsMobile.this.f7923v.mo11445f();
            }
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            VideoControlsMobile.this.f7953F = false;
            if (VideoControlsMobile.this.f7920s == null || !VideoControlsMobile.this.f7920s.mo11440a(this.f7956b)) {
                VideoControlsMobile.this.f7923v.mo11440a(this.f7956b);
            }
        }
    }

    public VideoControlsMobile(Context context) {
        super(context);
    }

    public VideoControlsMobile(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public VideoControlsMobile(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11384a() {
        if (this.f7899A) {
            boolean j = mo11401j();
            if (this.f7901C && j && this.f7912k.getVisibility() == 0) {
                this.f7912k.clearAnimation();
                this.f7912k.startAnimation(new TopViewHideShowAnimation(this.f7912k, false, 300));
            } else if ((!this.f7901C || !j) && this.f7912k.getVisibility() != 0) {
                this.f7912k.clearAnimation();
                this.f7912k.startAnimation(new TopViewHideShowAnimation(this.f7912k, true, 300));
            }
        }
    }

    /* renamed from: a */
    public void mo11385a(long j) {
        this.f7926y = j;
        if (j >= 0 && this.f7900B && !this.f7927z && !this.f7953F) {
            this.f7917p.postDelayed(new Runnable() {
                public void run() {
                    VideoControlsMobile.this.mo11387a(false);
                }
            }, j);
        }
    }

    /* renamed from: a */
    public void mo11386a(long j, long j2, int i) {
        if (!this.f7953F) {
            this.f7951D.setSecondaryProgress((int) (((float) this.f7951D.getMax()) * (((float) i) / 100.0f)));
            this.f7951D.setProgress((int) j);
            this.f7902a.setText(TimeFormatUtil.m17258a(j));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11387a(boolean z) {
        if (this.f7899A != z) {
            if (!this.f7901C || !mo11401j()) {
                this.f7912k.startAnimation(new TopViewHideShowAnimation(this.f7912k, z, 300));
            }
            if (!this.f7927z) {
                this.f7911j.startAnimation(new BottomViewHideShowAnimation(this.f7911j, z, 300));
            }
            this.f7899A = z;
            mo11402k();
        }
    }

    /* renamed from: b */
    public void mo11388b() {
        boolean z = false;
        if (this.f7927z) {
            this.f7927z = false;
            this.f7911j.setVisibility(0);
            this.f7910i.setVisibility(8);
            if (this.f7919r != null && this.f7919r.mo11331b()) {
                z = true;
            }
            mo11391c(z);
        }
    }

    /* renamed from: b */
    public void mo11389b(boolean z) {
        if (!this.f7927z) {
            this.f7927z = true;
            this.f7911j.setVisibility(8);
            this.f7910i.setVisibility(0);
            mo11390c();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11392d() {
        super.mo11392d();
        this.f7951D = (SeekBar) findViewById(C3078c.exomedia_controls_video_seek);
        this.f7952E = (LinearLayout) findViewById(C3078c.exomedia_controls_extra_container);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11394e() {
        super.mo11394e();
        this.f7951D.setOnSeekBarChangeListener(new C1990a());
    }

    public List<View> getExtraViews() {
        int childCount = this.f7952E.getChildCount();
        if (childCount <= 0) {
            return super.getExtraViews();
        }
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < childCount; i++) {
            linkedList.add(this.f7952E.getChildAt(i));
        }
        return linkedList;
    }

    /* access modifiers changed from: protected */
    public int getLayoutResource() {
        return C3079d.exomedia_default_controls_mobile;
    }

    public void setDuration(long j) {
        if (j != ((long) this.f7951D.getMax())) {
            this.f7903b.setText(TimeFormatUtil.m17258a(j));
            this.f7951D.setMax((int) j);
        }
    }

    public void setPosition(long j) {
        this.f7902a.setText(TimeFormatUtil.m17258a(j));
        this.f7951D.setProgress((int) j);
    }
}
