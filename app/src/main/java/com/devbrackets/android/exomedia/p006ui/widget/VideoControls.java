package com.devbrackets.android.exomedia.p006ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.LinkedList;
import java.util.List;

/* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControls */
public abstract class VideoControls extends RelativeLayout {

    /* renamed from: A */
    protected boolean f7899A = true;

    /* renamed from: B */
    protected boolean f7900B = true;

    /* renamed from: C */
    protected boolean f7901C = true;

    /* renamed from: a */
    protected TextView f7902a;

    /* renamed from: b */
    protected TextView f7903b;

    /* renamed from: c */
    protected TextView f7904c;

    /* renamed from: d */
    protected TextView f7905d;

    /* renamed from: e */
    protected TextView f7906e;

    /* renamed from: f */
    protected ImageButton f7907f;

    /* renamed from: g */
    protected ImageButton f7908g;

    /* renamed from: h */
    protected ImageButton f7909h;

    /* renamed from: i */
    protected ProgressBar f7910i;

    /* renamed from: j */
    protected ViewGroup f7911j;

    /* renamed from: k */
    protected ViewGroup f7912k;

    /* renamed from: l */
    protected Drawable f7913l;

    /* renamed from: m */
    protected Drawable f7914m;

    /* renamed from: n */
    protected Drawable f7915n;

    /* renamed from: o */
    protected Drawable f7916o;

    /* renamed from: p */
    protected Handler f7917p = new Handler();

    /* renamed from: q */
    protected Repeater f7918q = new Repeater();

    /* renamed from: r */
    protected EMVideoView f7919r;

    /* renamed from: s */
    protected VideoControlsSeekListener f7920s;

    /* renamed from: t */
    protected VideoControlsButtonListener f7921t;

    /* renamed from: u */
    protected VideoControlsVisibilityListener f7922u;

    /* renamed from: v */
    protected C1982a f7923v = new C1982a();

    /* renamed from: w */
    protected int f7924w = 0;

    /* renamed from: x */
    protected int f7925x = 0;

    /* renamed from: y */
    protected long f7926y = -1;

    /* renamed from: z */
    protected boolean f7927z = false;

    /* renamed from: com.devbrackets.android.exomedia.ui.widget.VideoControls$a */
    public class C1982a implements VideoControlsButtonListener, VideoControlsSeekListener {

        /* renamed from: a */
        protected boolean f7933a = false;

        protected C1982a() {
        }

        /* renamed from: a */
        public boolean mo11439a() {
            if (VideoControls.this.f7919r == null) {
                return false;
            }
            if (VideoControls.this.f7919r.mo11331b()) {
                VideoControls.this.f7919r.mo11334d();
            } else {
                VideoControls.this.f7919r.mo11333c();
            }
            return true;
        }

        /* renamed from: a */
        public boolean mo11440a(int i) {
            if (VideoControls.this.f7919r == null) {
                return false;
            }
            VideoControls.this.f7919r.mo11326a(i);
            if (this.f7933a) {
                this.f7933a = false;
                VideoControls.this.f7919r.mo11333c();
                VideoControls.this.mo11385a(VideoControls.this.f7926y);
            }
            return true;
        }

        /* renamed from: b */
        public boolean mo11441b() {
            return false;
        }

        /* renamed from: c */
        public boolean mo11442c() {
            return false;
        }

        /* renamed from: d */
        public boolean mo11443d() {
            return false;
        }

        /* renamed from: e */
        public boolean mo11444e() {
            return false;
        }

        /* renamed from: f */
        public boolean mo11445f() {
            if (VideoControls.this.f7919r == null) {
                return false;
            }
            if (VideoControls.this.f7919r.mo11331b()) {
                this.f7933a = true;
                VideoControls.this.f7919r.mo11334d();
            }
            VideoControls.this.mo11390c();
            return true;
        }
    }

    public VideoControls(Context context) {
        super(context);
        setup(context);
    }

    public VideoControls(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setup(context);
    }

    public VideoControls(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setup(context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo11384a();

    /* renamed from: a */
    public void mo11385a(long j) {
        this.f7926y = j;
        if (j >= 0 && this.f7900B && !this.f7927z) {
            this.f7917p.postDelayed(new Runnable() {
                public void run() {
                    VideoControls.this.mo11387a(false);
                }
            }, j);
        }
    }

    /* renamed from: a */
    public abstract void mo11386a(long j, long j2, int i);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo11387a(boolean z);

    /* renamed from: b */
    public abstract void mo11388b();

    /* renamed from: b */
    public abstract void mo11389b(boolean z);

    /* renamed from: c */
    public void mo11390c() {
        this.f7917p.removeCallbacksAndMessages(null);
        clearAnimation();
        mo11387a(true);
    }

    /* renamed from: c */
    public void mo11391c(boolean z) {
        mo11393d(z);
        this.f7918q.mo16914a();
        if (z) {
            mo11385a(2000);
        } else {
            mo11390c();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11392d() {
        this.f7902a = (TextView) findViewById(C3078c.exomedia_controls_current_time);
        this.f7903b = (TextView) findViewById(C3078c.exomedia_controls_end_time);
        this.f7904c = (TextView) findViewById(C3078c.exomedia_controls_title);
        this.f7905d = (TextView) findViewById(C3078c.exomedia_controls_sub_title);
        this.f7906e = (TextView) findViewById(C3078c.exomedia_controls_description);
        this.f7907f = (ImageButton) findViewById(C3078c.exomedia_controls_play_pause_btn);
        this.f7908g = (ImageButton) findViewById(C3078c.exomedia_controls_previous_btn);
        this.f7909h = (ImageButton) findViewById(C3078c.exomedia_controls_next_btn);
        this.f7910i = (ProgressBar) findViewById(C3078c.exomedia_controls_video_loading);
        this.f7911j = (ViewGroup) findViewById(C3078c.exomedia_controls_interactive_container);
        this.f7912k = (ViewGroup) findViewById(C3078c.exomedia_controls_text_container);
    }

    /* renamed from: d */
    public void mo11393d(boolean z) {
        if (z) {
            if (this.f7925x != 0) {
                this.f7907f.setImageResource(this.f7925x);
            } else {
                this.f7907f.setImageDrawable(this.f7914m);
            }
        } else if (this.f7924w != 0) {
            this.f7907f.setImageResource(this.f7924w);
        } else {
            this.f7907f.setImageDrawable(this.f7913l);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11394e() {
        this.f7907f.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VideoControls.this.mo11396g();
            }
        });
        this.f7908g.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VideoControls.this.mo11399h();
            }
        });
        this.f7909h.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                VideoControls.this.mo11400i();
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11395f() {
        this.f7913l = EMResourceUtil.m17217a(getContext(), C3077b.exomedia_ic_play_arrow_white, C3076a.exomedia_default_controls_button_selector);
        this.f7914m = EMResourceUtil.m17217a(getContext(), C3077b.exomedia_ic_pause_white, C3076a.exomedia_default_controls_button_selector);
        this.f7907f.setImageDrawable(this.f7913l);
        this.f7915n = EMResourceUtil.m17217a(getContext(), C3077b.exomedia_ic_skip_previous_white, C3076a.exomedia_default_controls_button_selector);
        this.f7908g.setImageDrawable(this.f7915n);
        this.f7916o = EMResourceUtil.m17217a(getContext(), C3077b.exomedia_ic_skip_next_white, C3076a.exomedia_default_controls_button_selector);
        this.f7909h.setImageDrawable(this.f7916o);
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo11396g() {
        if (this.f7921t == null || !this.f7921t.mo11439a()) {
            this.f7923v.mo11439a();
        }
    }

    public List<View> getExtraViews() {
        return new LinkedList();
    }

    /* access modifiers changed from: protected */
    public abstract int getLayoutResource();

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo11399h() {
        if (this.f7921t == null || !this.f7921t.mo11441b()) {
            this.f7923v.mo11441b();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo11400i() {
        if (this.f7921t == null || !this.f7921t.mo11442c()) {
            this.f7923v.mo11442c();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public boolean mo11401j() {
        if (this.f7904c.getText() != null && this.f7904c.getText().length() > 0) {
            return false;
        }
        if (this.f7905d.getText() == null || this.f7905d.getText().length() <= 0) {
            return this.f7906e.getText() == null || this.f7906e.getText().length() <= 0;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public void mo11402k() {
        if (this.f7922u != null) {
            if (this.f7899A) {
                this.f7922u.mo16888a();
            } else {
                this.f7922u.mo16889b();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public void mo11403l() {
        if (this.f7919r != null) {
            mo11386a((long) this.f7919r.getCurrentPosition(), (long) this.f7919r.getDuration(), this.f7919r.getBufferPercentage());
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f7918q.mo16917b();
        this.f7918q.mo16916a((C3103b) null);
    }

    public void setButtonListener(VideoControlsButtonListener yqVar) {
        this.f7921t = yqVar;
    }

    public void setCanHide(boolean z) {
        this.f7900B = z;
    }

    public void setDescription(CharSequence charSequence) {
        this.f7906e.setText(charSequence);
        mo11384a();
    }

    public abstract void setDuration(long j);

    public void setFastForwardButtonEnabled(boolean z) {
    }

    public void setFastForwardButtonRemoved(boolean z) {
    }

    public void setFastForwardDrawable(Drawable drawable) {
    }

    public void setFastForwardImageResource(int i) {
    }

    public void setHideEmptyTextContainer(boolean z) {
        this.f7901C = z;
        mo11384a();
    }

    public void setNextButtonEnabled(boolean z) {
        this.f7909h.setEnabled(z);
    }

    public void setNextButtonRemoved(boolean z) {
        this.f7909h.setVisibility(z ? 8 : 0);
    }

    public void setNextDrawable(Drawable drawable) {
        this.f7909h.setImageDrawable(drawable);
    }

    public void setNextImageResource(int i) {
        if (i != 0) {
            this.f7909h.setImageResource(i);
        } else {
            this.f7909h.setImageDrawable(this.f7916o);
        }
    }

    public void setPlayPauseDrawables(Drawable drawable, Drawable drawable2) {
        this.f7913l = drawable;
        this.f7914m = drawable2;
        mo11393d(this.f7919r != null && this.f7919r.mo11331b());
    }

    public void setPlayPauseImages(int i, int i2) {
        this.f7924w = i;
        this.f7925x = i2;
        mo11393d(this.f7919r != null && this.f7919r.mo11331b());
    }

    public abstract void setPosition(long j);

    public void setPreviousButtonEnabled(boolean z) {
        this.f7908g.setEnabled(z);
    }

    public void setPreviousButtonRemoved(boolean z) {
        this.f7908g.setVisibility(z ? 8 : 0);
    }

    public void setPreviousDrawable(Drawable drawable) {
        this.f7908g.setImageDrawable(drawable);
    }

    public void setPreviousImageResource(int i) {
        if (i != 0) {
            this.f7908g.setImageResource(i);
        } else {
            this.f7908g.setImageDrawable(this.f7915n);
        }
    }

    public void setRewindButtonEnabled(boolean z) {
    }

    public void setRewindButtonRemoved(boolean z) {
    }

    public void setRewindDrawable(Drawable drawable) {
    }

    public void setRewindImageResource(int i) {
    }

    public void setSeekListener(VideoControlsSeekListener yrVar) {
        this.f7920s = yrVar;
    }

    public void setSubTitle(CharSequence charSequence) {
        this.f7905d.setText(charSequence);
        mo11384a();
    }

    public void setTitle(CharSequence charSequence) {
        this.f7904c.setText(charSequence);
        mo11384a();
    }

    public void setVideoView(EMVideoView eMVideoView) {
        this.f7919r = eMVideoView;
    }

    public void setVisibilityListener(VideoControlsVisibilityListener ysVar) {
        this.f7922u = ysVar;
    }

    /* access modifiers changed from: protected */
    public void setup(Context context) {
        View.inflate(context, getLayoutResource(), this);
        mo11392d();
        mo11394e();
        mo11395f();
        this.f7918q.mo16916a((C3103b) new C3103b() {
            /* renamed from: a */
            public void mo11183a() {
                VideoControls.this.mo11403l();
            }
        });
    }
}
