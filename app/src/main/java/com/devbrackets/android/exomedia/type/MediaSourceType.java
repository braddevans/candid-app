package com.devbrackets.android.exomedia.type;

import android.net.Uri;

public enum MediaSourceType {
    AAC(".aac", null),
    MP4(".mp4", null),
    MP3(".mp3", null),
    M4A(".m4a", null),
    FMP4(".fmp4", null),
    TS(".ts", null),
    WEBM(".webm", null),
    MKV(".mkv", null),
    _3GP(".3gp", null),
    HLS(".m3u8", ".*m3u8.*"),
    DASH(".mpd", ".*mpd.*"),
    SMOOTH_STREAM(".ism", ".*ism.*"),
    UNKNOWN(null, null);
    

    /* renamed from: n */
    private String f7868n;

    /* renamed from: o */
    private String f7869o;

    private MediaSourceType(String str, String str2) {
        this.f7868n = str;
        this.f7869o = str2;
    }

    /* renamed from: a */
    public static MediaSourceType m10324a(Uri uri) {
        MediaSourceType[] values;
        for (MediaSourceType mediaSourceType : values()) {
            if (mediaSourceType.mo11324b() != null && uri.toString().matches(mediaSourceType.mo11324b())) {
                return mediaSourceType;
            }
        }
        return UNKNOWN;
    }

    /* renamed from: a */
    public static MediaSourceType m10325a(String str) {
        MediaSourceType[] values;
        for (MediaSourceType mediaSourceType : values()) {
            if (mediaSourceType.mo11323a() != null && mediaSourceType.mo11323a().equalsIgnoreCase(str)) {
                return mediaSourceType;
            }
        }
        return UNKNOWN;
    }

    /* renamed from: a */
    public String mo11323a() {
        return this.f7868n;
    }

    /* renamed from: b */
    public String mo11324b() {
        return this.f7869o;
    }
}
