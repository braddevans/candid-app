package com.devbrackets.android.exomedia.core.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import java.util.concurrent.locks.ReentrantLock;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

public class ResizingTextureView extends TextureView implements ClearableSurface {

    /* renamed from: k */
    private static final int[] f7798k = {12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, 4, 12344, 0, 12344};

    /* renamed from: l */
    private static final int[] f7799l = {12440, 2, 12344};

    /* renamed from: a */
    protected C1966c f7800a;

    /* renamed from: b */
    protected Point f7801b = new Point(0, 0);

    /* renamed from: c */
    protected Point f7802c = new Point(0, 0);

    /* renamed from: d */
    protected MatrixManager f7803d = new MatrixManager();

    /* renamed from: e */
    protected C1964a f7804e = new C1964a();

    /* renamed from: f */
    protected C1965b f7805f = new C1965b();

    /* renamed from: g */
    protected final ReentrantLock f7806g = new ReentrantLock(true);

    /* renamed from: h */
    protected int f7807h = 0;

    /* renamed from: i */
    protected int f7808i = 0;

    /* renamed from: j */
    protected boolean f7809j;

    /* renamed from: com.devbrackets.android.exomedia.core.video.ResizingTextureView$a */
    class C1964a implements OnAttachStateChangeListener {
        private C1964a() {
        }

        public void onViewAttachedToWindow(View view) {
            ResizingTextureView.this.f7806g.lock();
            ResizingTextureView.this.getViewTreeObserver().addOnGlobalLayoutListener(ResizingTextureView.this.f7805f);
            ResizingTextureView.this.removeOnAttachStateChangeListener(this);
            ResizingTextureView.this.f7806g.unlock();
        }

        public void onViewDetachedFromWindow(View view) {
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.core.video.ResizingTextureView$b */
    class C1965b implements OnGlobalLayoutListener {
        private C1965b() {
        }

        public void onGlobalLayout() {
            ResizingTextureView.this.setScaleType(ResizingTextureView.this.f7803d.mo16882b());
            if (VERSION.SDK_INT >= 16) {
                ResizingTextureView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            } else {
                ResizingTextureView.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        }
    }

    /* renamed from: com.devbrackets.android.exomedia.core.video.ResizingTextureView$c */
    public interface C1966c {
        /* renamed from: a */
        void mo11219a(int i, int i2);
    }

    public ResizingTextureView(Context context) {
        super(context);
    }

    public ResizingTextureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ResizingTextureView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @TargetApi(21)
    public ResizingTextureView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo11205b(int i, int i2) {
        this.f7803d.mo16876a(i, i2);
        mo11207e();
        this.f7802c.x = i;
        this.f7802c.y = i2;
        if (i == 0 || i2 == 0) {
            return false;
        }
        if (VERSION.SDK_INT >= 15) {
            SurfaceTexture surfaceTexture = getSurfaceTexture();
            if (surfaceTexture == null) {
                return false;
            }
            surfaceTexture.setDefaultBufferSize(i, i2);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11206c(int i, int i2) {
        if (this.f7801b.x != i || this.f7801b.y != i2) {
            this.f7801b.x = i;
            this.f7801b.y = i2;
            if (this.f7800a != null) {
                this.f7800a.mo11219a(i, i2);
            }
        }
    }

    /* renamed from: d */
    public void mo11191d() {
        if (getSurfaceTexture() != null) {
            try {
                EGL10 egl10 = (EGL10) EGLContext.getEGL();
                EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
                egl10.eglInitialize(eglGetDisplay, null);
                EGLConfig[] eGLConfigArr = new EGLConfig[1];
                egl10.eglChooseConfig(eglGetDisplay, f7798k, eGLConfigArr, eGLConfigArr.length, new int[1]);
                EGLContext eglCreateContext = egl10.eglCreateContext(eglGetDisplay, eGLConfigArr[0], EGL10.EGL_NO_CONTEXT, f7799l);
                EGLSurface eglCreateWindowSurface = egl10.eglCreateWindowSurface(eglGetDisplay, eGLConfigArr[0], getSurfaceTexture(), new int[]{12344});
                egl10.eglMakeCurrent(eglGetDisplay, eglCreateWindowSurface, eglCreateWindowSurface, eglCreateContext);
                egl10.eglSwapBuffers(eglGetDisplay, eglCreateWindowSurface);
                egl10.eglDestroySurface(eglGetDisplay, eglCreateWindowSurface);
                egl10.eglMakeCurrent(eglGetDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                egl10.eglDestroyContext(eglGetDisplay, eglCreateContext);
                egl10.eglTerminate(eglGetDisplay);
            } catch (Exception e) {
                Log.e("ResizingTextureView", "Error clearing surface", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11207e() {
        this.f7806g.lock();
        if (getWindowToken() == null) {
            addOnAttachStateChangeListener(this.f7804e);
        } else {
            getViewTreeObserver().addOnGlobalLayoutListener(this.f7805f);
        }
        this.f7806g.unlock();
    }

    public ScaleType getScaleType() {
        return this.f7803d.mo16882b();
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        mo11207e();
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        if (!this.f7809j) {
            super.onMeasure(i, i2);
            mo11206c(getMeasuredWidth(), getMeasuredHeight());
            return;
        }
        int defaultSize = getDefaultSize(this.f7802c.x, i);
        int defaultSize2 = getDefaultSize(this.f7802c.y, i2);
        if (this.f7802c.x <= 0 || this.f7802c.y <= 0) {
            setMeasuredDimension(defaultSize, defaultSize2);
            mo11206c(defaultSize, defaultSize2);
            return;
        }
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            i3 = size;
            i4 = size2;
            if (this.f7802c.x * i4 < this.f7802c.y * i3) {
                i3 = (this.f7802c.x * i4) / this.f7802c.y;
            } else if (this.f7802c.x * i4 > this.f7802c.y * i3) {
                i4 = (this.f7802c.y * i3) / this.f7802c.x;
            }
        } else if (mode == 1073741824) {
            i3 = size;
            i4 = (this.f7802c.y * i3) / this.f7802c.x;
            if (mode2 == Integer.MIN_VALUE && i4 > size2) {
                i4 = size2;
            }
        } else if (mode2 == 1073741824) {
            i4 = size2;
            i3 = (this.f7802c.x * i4) / this.f7802c.y;
            if (mode == Integer.MIN_VALUE && i3 > size) {
                i3 = size;
            }
        } else {
            i3 = this.f7802c.x;
            i4 = this.f7802c.y;
            if (mode2 == Integer.MIN_VALUE && i4 > size2) {
                i4 = size2;
                i3 = (this.f7802c.x * i4) / this.f7802c.y;
            }
            if (mode == Integer.MIN_VALUE && i3 > size) {
                i3 = size;
                i4 = (this.f7802c.y * i3) / this.f7802c.x;
            }
        }
        setMeasuredDimension(i3, i4);
        mo11206c(i3, i4);
    }

    public void setMeasureBasedOnAspectRatioEnabled(boolean z) {
        this.f7809j = z;
        requestLayout();
    }

    public void setOnSizeChangeListener(C1966c cVar) {
        this.f7800a = cVar;
    }

    public void setScaleType(ScaleType scaleType) {
        this.f7803d.mo16881a((View) this, scaleType);
    }

    public void setVideoRotation(int i, int i2) {
        this.f7807h = i;
        this.f7808i = i2;
        this.f7803d.mo16879a((View) this, (i + i2) % 360);
    }

    public void setVideoRotation(int i, boolean z) {
        int i2 = z ? i : this.f7807h;
        if (z) {
            i = this.f7808i;
        }
        setVideoRotation(i2, i);
    }
}
