package com.bumptech.glide.request;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.Queue;

public final class GenericRequest<A, T, Z, R> implements Request, ResourceCallback, SizeReadyCallback {

    /* renamed from: a */
    private static final Queue<GenericRequest<?, ?, ?, ?>> f7690a = C2994sn.m16221a(0);

    /* renamed from: A */
    private Resource<?> f7691A;

    /* renamed from: B */
    private C2791c f7692B;

    /* renamed from: C */
    private long f7693C;

    /* renamed from: D */
    private Status f7694D;

    /* renamed from: b */
    private final String f7695b = String.valueOf(hashCode());

    /* renamed from: c */
    private Key f7696c;

    /* renamed from: d */
    private Drawable f7697d;

    /* renamed from: e */
    private int f7698e;

    /* renamed from: f */
    private int f7699f;

    /* renamed from: g */
    private int f7700g;

    /* renamed from: h */
    private Context f7701h;

    /* renamed from: i */
    private Transformation<Z> f7702i;

    /* renamed from: j */
    private LoadProvider<A, T, Z, R> f7703j;

    /* renamed from: k */
    private RequestCoordinator f7704k;

    /* renamed from: l */
    private A f7705l;

    /* renamed from: m */
    private Class<R> f7706m;

    /* renamed from: n */
    private boolean f7707n;

    /* renamed from: o */
    private Priority f7708o;

    /* renamed from: p */
    private Target<R> f7709p;

    /* renamed from: q */
    private RequestListener<? super A, R> f7710q;

    /* renamed from: r */
    private float f7711r;

    /* renamed from: s */
    private Engine f7712s;

    /* renamed from: t */
    private GlideAnimationFactory<R> f7713t;

    /* renamed from: u */
    private int f7714u;

    /* renamed from: v */
    private int f7715v;

    /* renamed from: w */
    private DiskCacheStrategy f7716w;

    /* renamed from: x */
    private Drawable f7717x;

    /* renamed from: y */
    private Drawable f7718y;

    /* renamed from: z */
    private boolean f7719z;

    enum Status {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CANCELLED,
        CLEARED,
        PAUSED
    }

    private GenericRequest() {
    }

    /* renamed from: a */
    public static <A, T, Z, R> GenericRequest<A, T, Z, R> m10122a(LoadProvider<A, T, Z, R> rhVar, A a, Key lsVar, Context context, Priority priority, Target<R> sdVar, float f, Drawable drawable, int i, Drawable drawable2, int i2, Drawable drawable3, int i3, RequestListener<? super A, R> rkVar, RequestCoordinator rjVar, Engine mhVar, Transformation<Z> lwVar, Class<R> cls, boolean z, GlideAnimationFactory<R> rqVar, int i4, int i5, DiskCacheStrategy diskCacheStrategy) {
        GenericRequest<A, T, Z, R> genericRequest = (GenericRequest) f7690a.poll();
        if (genericRequest == null) {
            genericRequest = new GenericRequest<>();
        }
        genericRequest.m10128b(rhVar, a, lsVar, context, priority, sdVar, f, drawable, i, drawable2, i2, drawable3, i3, rkVar, rjVar, mhVar, lwVar, cls, z, rqVar, i4, i5, diskCacheStrategy);
        return genericRequest;
    }

    /* renamed from: a */
    private void m10123a(String str) {
        Log.v("GenericRequest", str + " this: " + this.f7695b);
    }

    /* renamed from: a */
    private static void m10124a(String str, Object obj, String str2) {
        if (obj == null) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(" must not be null");
            if (str2 != null) {
                sb.append(", ");
                sb.append(str2);
            }
            throw new NullPointerException(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001b, code lost:
        if (r10.f7710q.onResourceReady(r12, r10.f7705l, r10.f7709p, r10.f7719z, r5) == false) goto L_0x001d;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m10125a(p000.Resource<?> r11, R r12) {
        /*
            r10 = this;
            boolean r5 = r10.m10134p()
            com.bumptech.glide.request.GenericRequest$Status r0 = com.bumptech.glide.request.GenericRequest.Status.COMPLETE
            r10.f7694D = r0
            r10.f7691A = r11
            rk<? super A, R> r0 = r10.f7710q
            if (r0 == 0) goto L_0x001d
            rk<? super A, R> r0 = r10.f7710q
            A r2 = r10.f7705l
            sd<R> r3 = r10.f7709p
            boolean r4 = r10.f7719z
            r1 = r12
            boolean r0 = r0.onResourceReady(r1, r2, r3, r4, r5)
            if (r0 != 0) goto L_0x002a
        L_0x001d:
            rq<R> r0 = r10.f7713t
            boolean r1 = r10.f7719z
            rp r6 = r0.mo16135a(r1, r5)
            sd<R> r0 = r10.f7709p
            r0.onResourceReady(r12, r6)
        L_0x002a:
            r10.m10135q()
            java.lang.String r0 = "GenericRequest"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0070
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Resource ready in "
            java.lang.StringBuilder r0 = r0.append(r1)
            long r2 = r10.f7693C
            double r2 = p000.LogTime.m16201a(r2)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = " size: "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r11.mo14375b()
            double r2 = (double) r1
            r8 = 4517110426252607488(0x3eb0000000000000, double:9.5367431640625E-7)
            double r2 = r2 * r8
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = " fromCache: "
            java.lang.StringBuilder r0 = r0.append(r1)
            boolean r1 = r10.f7719z
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.m10123a(r0)
        L_0x0070:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.GenericRequest.m10125a(mo, java.lang.Object):void");
    }

    /* renamed from: b */
    private void m10126b(Exception exc) {
        if (m10133o()) {
            Drawable drawable = this.f7705l == null ? m10129k() : null;
            if (drawable == null) {
                drawable = m10130l();
            }
            if (drawable == null) {
                drawable = m10131m();
            }
            this.f7709p.onLoadFailed(exc, drawable);
        }
    }

    /* renamed from: b */
    private void m10127b(Resource moVar) {
        this.f7712s.mo14761a(moVar);
        this.f7691A = null;
    }

    /* renamed from: b */
    private void m10128b(LoadProvider<A, T, Z, R> rhVar, A a, Key lsVar, Context context, Priority priority, Target<R> sdVar, float f, Drawable drawable, int i, Drawable drawable2, int i2, Drawable drawable3, int i3, RequestListener<? super A, R> rkVar, RequestCoordinator rjVar, Engine mhVar, Transformation<Z> lwVar, Class<R> cls, boolean z, GlideAnimationFactory<R> rqVar, int i4, int i5, DiskCacheStrategy diskCacheStrategy) {
        this.f7703j = rhVar;
        this.f7705l = a;
        this.f7696c = lsVar;
        this.f7697d = drawable3;
        this.f7698e = i3;
        this.f7701h = context.getApplicationContext();
        this.f7708o = priority;
        this.f7709p = sdVar;
        this.f7711r = f;
        this.f7717x = drawable;
        this.f7699f = i;
        this.f7718y = drawable2;
        this.f7700g = i2;
        this.f7710q = rkVar;
        this.f7704k = rjVar;
        this.f7712s = mhVar;
        this.f7702i = lwVar;
        this.f7706m = cls;
        this.f7707n = z;
        this.f7713t = rqVar;
        this.f7714u = i4;
        this.f7715v = i5;
        this.f7716w = diskCacheStrategy;
        this.f7694D = Status.PENDING;
        if (a != null) {
            m10124a("ModelLoader", rhVar.mo15987e(), "try .using(ModelLoader)");
            m10124a("Transcoder", rhVar.mo15988f(), "try .as*(Class).transcode(ResourceTranscoder)");
            m10124a("Transformation", lwVar, "try .transform(UnitTransformation.get())");
            if (diskCacheStrategy.mo11056a()) {
                m10124a("SourceEncoder", rhVar.mo15854c(), "try .sourceEncoder(Encoder) or .diskCacheStrategy(NONE/RESULT)");
            } else {
                m10124a("SourceDecoder", rhVar.mo15853b(), "try .decoder/.imageDecoder/.videoDecoder(ResourceDecoder) or .diskCacheStrategy(ALL/SOURCE)");
            }
            if (diskCacheStrategy.mo11056a() || diskCacheStrategy.mo11057b()) {
                m10124a("CacheDecoder", rhVar.mo15852a(), "try .cacheDecoder(ResouceDecoder) or .diskCacheStrategy(NONE)");
            }
            if (diskCacheStrategy.mo11057b()) {
                m10124a("Encoder", rhVar.mo15855d(), "try .encode(ResourceEncoder) or .diskCacheStrategy(NONE/SOURCE)");
            }
        }
    }

    /* renamed from: k */
    private Drawable m10129k() {
        if (this.f7697d == null && this.f7698e > 0) {
            this.f7697d = this.f7701h.getResources().getDrawable(this.f7698e);
        }
        return this.f7697d;
    }

    /* renamed from: l */
    private Drawable m10130l() {
        if (this.f7718y == null && this.f7700g > 0) {
            this.f7718y = this.f7701h.getResources().getDrawable(this.f7700g);
        }
        return this.f7718y;
    }

    /* renamed from: m */
    private Drawable m10131m() {
        if (this.f7717x == null && this.f7699f > 0) {
            this.f7717x = this.f7701h.getResources().getDrawable(this.f7699f);
        }
        return this.f7717x;
    }

    /* renamed from: n */
    private boolean m10132n() {
        return this.f7704k == null || this.f7704k.mo16130a(this);
    }

    /* renamed from: o */
    private boolean m10133o() {
        return this.f7704k == null || this.f7704k.mo16131b(this);
    }

    /* renamed from: p */
    private boolean m10134p() {
        return this.f7704k == null || !this.f7704k.mo16133c();
    }

    /* renamed from: q */
    private void m10135q() {
        if (this.f7704k != null) {
            this.f7704k.mo16132c(this);
        }
    }

    /* renamed from: a */
    public void mo11104a() {
        this.f7703j = null;
        this.f7705l = null;
        this.f7701h = null;
        this.f7709p = null;
        this.f7717x = null;
        this.f7718y = null;
        this.f7697d = null;
        this.f7710q = null;
        this.f7704k = null;
        this.f7702i = null;
        this.f7713t = null;
        this.f7719z = false;
        this.f7692B = null;
        f7690a.offer(this);
    }

    /* renamed from: a */
    public void mo11105a(int i, int i2) {
        if (Log.isLoggable("GenericRequest", 2)) {
            m10123a("Got onSizeReady in " + LogTime.m16201a(this.f7693C));
        }
        if (this.f7694D == Status.WAITING_FOR_SIZE) {
            this.f7694D = Status.RUNNING;
            int round = Math.round(this.f7711r * ((float) i));
            int round2 = Math.round(this.f7711r * ((float) i2));
            DataFetcher a = this.f7703j.mo15987e().mo14316a(this.f7705l, round, round2);
            if (a == null) {
                mo11106a(new Exception("Failed to load model: '" + this.f7705l + "'"));
                return;
            }
            ResourceTranscoder f = this.f7703j.mo15988f();
            if (Log.isLoggable("GenericRequest", 2)) {
                m10123a("finished setup for calling load in " + LogTime.m16201a(this.f7693C));
            }
            this.f7719z = true;
            this.f7692B = this.f7712s.mo14758a(this.f7696c, round, round2, a, this.f7703j, this.f7702i, f, this.f7708o, this.f7707n, this.f7716w, this);
            this.f7719z = this.f7691A != null;
            if (Log.isLoggable("GenericRequest", 2)) {
                m10123a("finished onSizeReady in " + LogTime.m16201a(this.f7693C));
            }
        }
    }

    /* renamed from: a */
    public void mo11106a(Exception exc) {
        if (Log.isLoggable("GenericRequest", 3)) {
            Log.d("GenericRequest", "load failed", exc);
        }
        this.f7694D = Status.FAILED;
        if (this.f7710q == null || !this.f7710q.onException(exc, this.f7705l, this.f7709p, m10134p())) {
            m10126b(exc);
        }
    }

    /* renamed from: a */
    public void mo11107a(Resource<?> moVar) {
        if (moVar == null) {
            mo11106a(new Exception("Expected to receive a Resource<R> with an object of " + this.f7706m + " inside, but instead got null."));
            return;
        }
        Object d = moVar.mo14377d();
        if (d == null || !this.f7706m.isAssignableFrom(d.getClass())) {
            m10127b((Resource) moVar);
            mo11106a(new Exception("Expected to receive an object of " + this.f7706m + " but instead got " + (d != null ? d.getClass() : "") + "{" + d + "}" + " inside Resource{" + moVar + "}." + (d != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.")));
        } else if (!m10132n()) {
            m10127b((Resource) moVar);
            this.f7694D = Status.COMPLETE;
        } else {
            m10125a(moVar, (R) d);
        }
    }

    /* renamed from: b */
    public void mo11108b() {
        this.f7693C = LogTime.m16202a();
        if (this.f7705l == null) {
            mo11106a((Exception) null);
            return;
        }
        this.f7694D = Status.WAITING_FOR_SIZE;
        if (C2994sn.m16223a(this.f7714u, this.f7715v)) {
            mo11105a(this.f7714u, this.f7715v);
        } else {
            this.f7709p.getSize(this);
        }
        if (!mo11113g() && !mo11116j() && m10133o()) {
            this.f7709p.onLoadStarted(m10131m());
        }
        if (Log.isLoggable("GenericRequest", 2)) {
            m10123a("finished run method in " + LogTime.m16201a(this.f7693C));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo11109c() {
        this.f7694D = Status.CANCELLED;
        if (this.f7692B != null) {
            this.f7692B.mo14765a();
            this.f7692B = null;
        }
    }

    /* renamed from: d */
    public void mo11110d() {
        C2994sn.m16222a();
        if (this.f7694D != Status.CLEARED) {
            mo11109c();
            if (this.f7691A != null) {
                m10127b((Resource) this.f7691A);
            }
            if (m10133o()) {
                this.f7709p.onLoadCleared(m10131m());
            }
            this.f7694D = Status.CLEARED;
        }
    }

    /* renamed from: e */
    public void mo11111e() {
        mo11110d();
        this.f7694D = Status.PAUSED;
    }

    /* renamed from: f */
    public boolean mo11112f() {
        return this.f7694D == Status.RUNNING || this.f7694D == Status.WAITING_FOR_SIZE;
    }

    /* renamed from: g */
    public boolean mo11113g() {
        return this.f7694D == Status.COMPLETE;
    }

    /* renamed from: h */
    public boolean mo11114h() {
        return mo11113g();
    }

    /* renamed from: i */
    public boolean mo11115i() {
        return this.f7694D == Status.CANCELLED || this.f7694D == Status.CLEARED;
    }

    /* renamed from: j */
    public boolean mo11116j() {
        return this.f7694D == Status.FAILED;
    }
}
