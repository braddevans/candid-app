package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import java.util.HashSet;

@TargetApi(11)
public class RequestManagerFragment extends Fragment {

    /* renamed from: a */
    private final ActivityFragmentLifecycle f7684a;

    /* renamed from: b */
    private final RequestManagerTreeNode f7685b;

    /* renamed from: c */
    private RequestManager f7686c;

    /* renamed from: d */
    private final HashSet<RequestManagerFragment> f7687d;

    /* renamed from: e */
    private RequestManagerFragment f7688e;

    /* renamed from: com.bumptech.glide.manager.RequestManagerFragment$a */
    class C1953a implements RequestManagerTreeNode {
        private C1953a() {
        }
    }

    public RequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }

    @SuppressLint({"ValidFragment"})
    RequestManagerFragment(ActivityFragmentLifecycle qnVar) {
        this.f7685b = new C1953a();
        this.f7687d = new HashSet<>();
        this.f7684a = qnVar;
    }

    /* renamed from: a */
    private void m10116a(RequestManagerFragment requestManagerFragment) {
        this.f7687d.add(requestManagerFragment);
    }

    /* renamed from: b */
    private void m10117b(RequestManagerFragment requestManagerFragment) {
        this.f7687d.remove(requestManagerFragment);
    }

    /* renamed from: a */
    public ActivityFragmentLifecycle mo11093a() {
        return this.f7684a;
    }

    /* renamed from: a */
    public void mo11094a(RequestManager leVar) {
        this.f7686c = leVar;
    }

    /* renamed from: b */
    public RequestManager mo11095b() {
        return this.f7686c;
    }

    /* renamed from: c */
    public RequestManagerTreeNode mo11096c() {
        return this.f7685b;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f7688e = RequestManagerRetriever.m15922a().mo15949a(getActivity().getFragmentManager());
        if (this.f7688e != this) {
            this.f7688e.m10116a(this);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f7684a.mo15946c();
    }

    public void onDetach() {
        super.onDetach();
        if (this.f7688e != null) {
            this.f7688e.m10117b(this);
            this.f7688e = null;
        }
    }

    public void onLowMemory() {
        if (this.f7686c != null) {
            this.f7686c.mo14653a();
        }
    }

    public void onStart() {
        super.onStart();
        this.f7684a.mo15943a();
    }

    public void onStop() {
        super.onStop();
        this.f7684a.mo15945b();
    }

    public void onTrimMemory(int i) {
        if (this.f7686c != null) {
            this.f7686c.mo14654a(i);
        }
    }
}
