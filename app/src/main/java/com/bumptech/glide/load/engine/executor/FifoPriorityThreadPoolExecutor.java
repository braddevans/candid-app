package com.bumptech.glide.load.engine.executor;

import android.os.Process;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class FifoPriorityThreadPoolExecutor extends ThreadPoolExecutor {

    /* renamed from: a */
    private final AtomicInteger f7663a;

    /* renamed from: b */
    private final UncaughtThrowableStrategy f7664b;

    public enum UncaughtThrowableStrategy {
        IGNORE,
        LOG {
            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void mo11064a(Throwable th) {
                if (Log.isLoggable("PriorityExecutor", 6)) {
                    Log.e("PriorityExecutor", "Request threw uncaught throwable", th);
                }
            }
        },
        THROW {
            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void mo11064a(Throwable th) {
                super.mo11064a(th);
                throw new RuntimeException(th);
            }
        };

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo11064a(Throwable th) {
        }
    }

    /* renamed from: com.bumptech.glide.load.engine.executor.FifoPriorityThreadPoolExecutor$a */
    public static class C1947a implements ThreadFactory {

        /* renamed from: a */
        int f7669a = 0;

        public Thread newThread(Runnable runnable) {
            C19481 r0 = new Thread(runnable, "fifo-pool-thread-" + this.f7669a) {
                public void run() {
                    Process.setThreadPriority(10);
                    super.run();
                }
            };
            this.f7669a++;
            return r0;
        }
    }

    /* renamed from: com.bumptech.glide.load.engine.executor.FifoPriorityThreadPoolExecutor$b */
    static class C1949b<T> extends FutureTask<T> implements Comparable<C1949b<?>> {

        /* renamed from: a */
        private final int f7671a;

        /* renamed from: b */
        private final int f7672b;

        public C1949b(Runnable runnable, T t, int i) {
            super(runnable, t);
            if (!(runnable instanceof Prioritized)) {
                throw new IllegalArgumentException("FifoPriorityThreadPoolExecutor must be given Runnables that implement Prioritized");
            }
            this.f7671a = ((Prioritized) runnable).mo11059b();
            this.f7672b = i;
        }

        /* renamed from: a */
        public int compareTo(C1949b<?> bVar) {
            int i = this.f7671a - bVar.f7671a;
            return i == 0 ? this.f7672b - bVar.f7672b : i;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1949b)) {
                return false;
            }
            C1949b bVar = (C1949b) obj;
            return this.f7672b == bVar.f7672b && this.f7671a == bVar.f7671a;
        }

        public int hashCode() {
            return (this.f7671a * 31) + this.f7672b;
        }
    }

    public FifoPriorityThreadPoolExecutor(int i) {
        this(i, UncaughtThrowableStrategy.LOG);
    }

    public FifoPriorityThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, ThreadFactory threadFactory, UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        super(i, i2, j, timeUnit, new PriorityBlockingQueue(), threadFactory);
        this.f7663a = new AtomicInteger();
        this.f7664b = uncaughtThrowableStrategy;
    }

    public FifoPriorityThreadPoolExecutor(int i, UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        this(i, i, 0, TimeUnit.MILLISECONDS, new C1947a(), uncaughtThrowableStrategy);
    }

    /* access modifiers changed from: protected */
    public void afterExecute(Runnable runnable, Throwable th) {
        super.afterExecute(runnable, th);
        if (th == null && (runnable instanceof Future)) {
            Future future = (Future) runnable;
            if (future.isDone() && !future.isCancelled()) {
                try {
                    future.get();
                } catch (InterruptedException e) {
                    this.f7664b.mo11064a(e);
                } catch (ExecutionException e2) {
                    this.f7664b.mo11064a(e2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new C1949b(runnable, t, this.f7663a.getAndIncrement());
    }
}
