package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.Priority;

public class EngineRunnable implements Runnable, Prioritized {

    /* renamed from: a */
    private final Priority f7655a;

    /* renamed from: b */
    private final C1943a f7656b;

    /* renamed from: c */
    private final DecodeJob<?, ?, ?> f7657c;

    /* renamed from: d */
    private Stage f7658d = Stage.CACHE;

    /* renamed from: e */
    private volatile boolean f7659e;

    enum Stage {
        CACHE,
        SOURCE
    }

    /* renamed from: com.bumptech.glide.load.engine.EngineRunnable$a */
    public interface C1943a extends ResourceCallback {
        /* renamed from: b */
        void mo11061b(EngineRunnable engineRunnable);
    }

    public EngineRunnable(C1943a aVar, DecodeJob<?, ?, ?> mgVar, Priority priority) {
        this.f7656b = aVar;
        this.f7657c = mgVar;
        this.f7655a = priority;
    }

    /* renamed from: a */
    private void m10084a(Exception exc) {
        if (m10086c()) {
            this.f7658d = Stage.SOURCE;
            this.f7656b.mo11061b(this);
            return;
        }
        this.f7656b.mo11106a(exc);
    }

    /* renamed from: a */
    private void m10085a(Resource moVar) {
        this.f7656b.mo11107a(moVar);
    }

    /* renamed from: c */
    private boolean m10086c() {
        return this.f7658d == Stage.CACHE;
    }

    /* renamed from: d */
    private Resource<?> m10087d() throws Exception {
        return m10086c() ? m10088e() : m10089f();
    }

    /* renamed from: e */
    private Resource<?> m10088e() throws Exception {
        Resource<?> moVar = null;
        try {
            moVar = this.f7657c.mo14751a();
        } catch (Exception e) {
            if (Log.isLoggable("EngineRunnable", 3)) {
                Log.d("EngineRunnable", "Exception decoding result from cache: " + e);
            }
        }
        return moVar == null ? this.f7657c.mo14752b() : moVar;
    }

    /* renamed from: f */
    private Resource<?> m10089f() throws Exception {
        return this.f7657c.mo14753c();
    }

    /* renamed from: a */
    public void mo11058a() {
        this.f7659e = true;
        this.f7657c.mo14754d();
    }

    /* renamed from: b */
    public int mo11059b() {
        return this.f7655a.ordinal();
    }

    public void run() {
        if (!this.f7659e) {
            Exception exc = null;
            Resource moVar = null;
            try {
                moVar = m10087d();
            } catch (Exception e) {
                if (Log.isLoggable("EngineRunnable", 2)) {
                    Log.v("EngineRunnable", "Exception decoding", e);
                }
                exc = e;
            }
            if (this.f7659e) {
                if (moVar != null) {
                    moVar.mo14376c();
                }
            } else if (moVar == null) {
                m10084a(exc);
            } else {
                m10085a(moVar);
            }
        }
    }
}
