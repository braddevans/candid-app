package com.bumptech.glide.load.engine;

public enum DiskCacheStrategy {
    ALL(true, true),
    NONE(false, false),
    SOURCE(true, false),
    RESULT(false, true);
    

    /* renamed from: e */
    private final boolean f7653e;

    /* renamed from: f */
    private final boolean f7654f;

    private DiskCacheStrategy(boolean z, boolean z2) {
        this.f7653e = z;
        this.f7654f = z2;
    }

    /* renamed from: a */
    public boolean mo11056a() {
        return this.f7653e;
    }

    /* renamed from: b */
    public boolean mo11057b() {
        return this.f7654f;
    }
}
