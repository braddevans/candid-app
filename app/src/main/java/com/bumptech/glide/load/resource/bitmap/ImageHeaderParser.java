package com.bumptech.glide.load.resource.bitmap;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ImageHeaderParser {

    /* renamed from: a */
    private static final byte[] f7673a;

    /* renamed from: b */
    private static final int[] f7674b = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    /* renamed from: c */
    private final C1951b f7675c;

    public enum ImageType {
        GIF(true),
        JPEG(false),
        PNG_A(true),
        PNG(false),
        UNKNOWN(false);
        

        /* renamed from: a */
        private final boolean f7676a;

        private ImageType(boolean z) {
            this.f7676a = z;
        }

        public boolean hasAlpha() {
            return this.f7676a;
        }
    }

    /* renamed from: com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$a */
    static class C1950a {

        /* renamed from: a */
        private final ByteBuffer f7677a;

        public C1950a(byte[] bArr) {
            this.f7677a = ByteBuffer.wrap(bArr);
            this.f7677a.order(ByteOrder.BIG_ENDIAN);
        }

        /* renamed from: a */
        public int mo11075a() {
            return this.f7677a.array().length;
        }

        /* renamed from: a */
        public int mo11076a(int i) {
            return this.f7677a.getInt(i);
        }

        /* renamed from: a */
        public void mo11077a(ByteOrder byteOrder) {
            this.f7677a.order(byteOrder);
        }

        /* renamed from: b */
        public short mo11078b(int i) {
            return this.f7677a.getShort(i);
        }
    }

    /* renamed from: com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$b */
    static class C1951b {

        /* renamed from: a */
        private final InputStream f7678a;

        public C1951b(InputStream inputStream) {
            this.f7678a = inputStream;
        }

        /* renamed from: a */
        public int mo11079a() throws IOException {
            return ((this.f7678a.read() << 8) & 65280) | (this.f7678a.read() & 255);
        }

        /* renamed from: a */
        public int mo11080a(byte[] bArr) throws IOException {
            int length = bArr.length;
            while (length > 0) {
                int read = this.f7678a.read(bArr, bArr.length - length, length);
                if (read == -1) {
                    break;
                }
                length -= read;
            }
            return bArr.length - length;
        }

        /* renamed from: a */
        public long mo11081a(long j) throws IOException {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.f7678a.skip(j2);
                if (skip > 0) {
                    j2 -= skip;
                } else if (this.f7678a.read() == -1) {
                    break;
                } else {
                    j2--;
                }
            }
            return j - j2;
        }

        /* renamed from: b */
        public short mo11082b() throws IOException {
            return (short) (this.f7678a.read() & 255);
        }

        /* renamed from: c */
        public int mo11083c() throws IOException {
            return this.f7678a.read();
        }
    }

    static {
        byte[] bArr = new byte[0];
        try {
            bArr = "Exif\u0000\u0000".getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        f7673a = bArr;
    }

    public ImageHeaderParser(InputStream inputStream) {
        this.f7675c = new C1951b(inputStream);
    }

    /* renamed from: a */
    private static int m10097a(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    /* renamed from: a */
    private static int m10098a(C1950a aVar) {
        ByteOrder byteOrder;
        int length = "Exif\u0000\u0000".length();
        short b = aVar.mo11078b(length);
        if (b == 19789) {
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else if (b == 18761) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else {
            if (Log.isLoggable("ImageHeaderParser", 3)) {
                Log.d("ImageHeaderParser", "Unknown endianness = " + b);
            }
            byteOrder = ByteOrder.BIG_ENDIAN;
        }
        aVar.mo11077a(byteOrder);
        int a = aVar.mo11076a(length + 4) + length;
        short b2 = aVar.mo11078b(a);
        for (int i = 0; i < b2; i++) {
            int a2 = m10097a(a, i);
            short b3 = aVar.mo11078b(a2);
            if (b3 == 274) {
                short b4 = aVar.mo11078b(a2 + 2);
                if (b4 >= 1 && b4 <= 12) {
                    int a3 = aVar.mo11076a(a2 + 4);
                    if (a3 >= 0) {
                        if (Log.isLoggable("ImageHeaderParser", 3)) {
                            Log.d("ImageHeaderParser", "Got tagIndex=" + i + " tagType=" + b3 + " formatCode=" + b4 + " componentCount=" + a3);
                        }
                        int i2 = a3 + f7674b[b4];
                        if (i2 <= 4) {
                            int i3 = a2 + 8;
                            if (i3 < 0 || i3 > aVar.mo11075a()) {
                                if (Log.isLoggable("ImageHeaderParser", 3)) {
                                    Log.d("ImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + b3);
                                }
                            } else if (i2 >= 0 && i3 + i2 <= aVar.mo11075a()) {
                                return aVar.mo11078b(i3);
                            } else {
                                if (Log.isLoggable("ImageHeaderParser", 3)) {
                                    Log.d("ImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + b3);
                                }
                            }
                        } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                            Log.d("ImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + b4);
                        }
                    } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                        Log.d("ImageHeaderParser", "Negative tiff component count");
                    }
                } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Got invalid format code=" + b4);
                }
            }
        }
        return -1;
    }

    /* renamed from: a */
    private static boolean m10099a(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    /* renamed from: d */
    private byte[] m10100d() throws IOException {
        short b;
        int a;
        long a2;
        do {
            short b2 = this.f7675c.mo11082b();
            if (b2 != 255) {
                if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Unknown segmentId=" + b2);
                }
                return null;
            }
            b = this.f7675c.mo11082b();
            if (b == 218) {
                return null;
            }
            if (b == 217) {
                if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Found MARKER_EOI in exif segment");
                }
                return null;
            }
            a = this.f7675c.mo11079a() - 2;
            if (b != 225) {
                a2 = this.f7675c.mo11081a((long) a);
            } else {
                byte[] bArr = new byte[a];
                int a3 = this.f7675c.mo11080a(bArr);
                if (a3 == a) {
                    return bArr;
                }
                if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Unable to read segment data, type: " + b + ", length: " + a + ", actually read: " + a3);
                }
                return null;
            }
        } while (a2 == ((long) a));
        if (Log.isLoggable("ImageHeaderParser", 3)) {
            Log.d("ImageHeaderParser", "Unable to skip enough data, type: " + b + ", wanted to skip: " + a + ", but actually skipped: " + a2);
        }
        return null;
    }

    /* renamed from: a */
    public boolean mo11071a() throws IOException {
        return mo11072b().hasAlpha();
    }

    /* renamed from: b */
    public ImageType mo11072b() throws IOException {
        int a = this.f7675c.mo11079a();
        if (a == 65496) {
            return ImageType.JPEG;
        }
        int a2 = ((a << 16) & -65536) | (this.f7675c.mo11079a() & 65535);
        if (a2 != -1991225785) {
            return (a2 >> 8) == 4671814 ? ImageType.GIF : ImageType.UNKNOWN;
        }
        this.f7675c.mo11081a(21);
        return this.f7675c.mo11083c() >= 3 ? ImageType.PNG_A : ImageType.PNG;
    }

    /* renamed from: c */
    public int mo11073c() throws IOException {
        if (!m10099a(this.f7675c.mo11079a())) {
            return -1;
        }
        byte[] d = m10100d();
        boolean z = d != null && d.length > f7673a.length;
        if (z) {
            int i = 0;
            while (true) {
                if (i >= f7673a.length) {
                    break;
                } else if (d[i] != f7673a[i]) {
                    z = false;
                    break;
                } else {
                    i++;
                }
            }
        }
        if (z) {
            return m10098a(new C1950a(d));
        }
        return -1;
    }
}
