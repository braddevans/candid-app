package com.bumptech.glide.load.resource.bitmap;

import android.util.Log;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RecyclableBufferedInputStream extends FilterInputStream {

    /* renamed from: a */
    private volatile byte[] f7679a;

    /* renamed from: b */
    private int f7680b;

    /* renamed from: c */
    private int f7681c;

    /* renamed from: d */
    private int f7682d = -1;

    /* renamed from: e */
    private int f7683e;

    public static class InvalidMarkException extends RuntimeException {
        public InvalidMarkException(String str) {
            super(str);
        }
    }

    public RecyclableBufferedInputStream(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("buffer is null or empty");
        }
        this.f7679a = bArr;
    }

    /* renamed from: a */
    private int m10113a(InputStream inputStream, byte[] bArr) throws IOException {
        if (this.f7682d == -1 || this.f7683e - this.f7682d >= this.f7681c) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                this.f7682d = -1;
                this.f7683e = 0;
                this.f7680b = read;
            }
            return read;
        }
        if (this.f7682d == 0 && this.f7681c > bArr.length && this.f7680b == bArr.length) {
            int length = bArr.length * 2;
            if (length > this.f7681c) {
                length = this.f7681c;
            }
            if (Log.isLoggable("BufferedIs", 3)) {
                Log.d("BufferedIs", "allocate buffer of length: " + length);
            }
            byte[] bArr2 = new byte[length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            this.f7679a = bArr2;
            bArr = bArr2;
        } else if (this.f7682d > 0) {
            System.arraycopy(bArr, this.f7682d, bArr, 0, bArr.length - this.f7682d);
        }
        this.f7683e -= this.f7682d;
        this.f7682d = 0;
        this.f7680b = 0;
        int read2 = inputStream.read(bArr, this.f7683e, bArr.length - this.f7683e);
        this.f7680b = read2 <= 0 ? this.f7683e : this.f7683e + read2;
        return read2;
    }

    /* renamed from: b */
    private static IOException m10114b() throws IOException {
        throw new IOException("BufferedInputStream is closed");
    }

    /* renamed from: a */
    public synchronized void mo11084a() {
        this.f7681c = this.f7679a.length;
    }

    public synchronized int available() throws IOException {
        InputStream inputStream;
        inputStream = this.in;
        if (this.f7679a == null || inputStream == null) {
            throw m10114b();
        }
        return (this.f7680b - this.f7683e) + inputStream.available();
    }

    public void close() throws IOException {
        this.f7679a = null;
        InputStream inputStream = this.in;
        this.in = null;
        if (inputStream != null) {
            inputStream.close();
        }
    }

    public synchronized void mark(int i) {
        this.f7681c = Math.max(this.f7681c, i);
        this.f7682d = this.f7683e;
    }

    public boolean markSupported() {
        return true;
    }

    public synchronized int read() throws IOException {
        byte b = -1;
        synchronized (this) {
            byte[] bArr = this.f7679a;
            InputStream inputStream = this.in;
            if (bArr == null || inputStream == null) {
                throw m10114b();
            } else if (this.f7683e < this.f7680b || m10113a(inputStream, bArr) != -1) {
                if (bArr != this.f7679a) {
                    bArr = this.f7679a;
                    if (bArr == null) {
                        throw m10114b();
                    }
                }
                if (this.f7680b - this.f7683e > 0) {
                    int i = this.f7683e;
                    this.f7683e = i + 1;
                    b = bArr[i] & 255;
                }
            }
        }
        return b;
    }

    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        int i5 = -1;
        synchronized (this) {
            byte[] bArr2 = this.f7679a;
            if (bArr2 == null) {
                throw m10114b();
            } else if (i2 == 0) {
                i5 = 0;
            } else {
                InputStream inputStream = this.in;
                if (inputStream == null) {
                    throw m10114b();
                }
                if (this.f7683e < this.f7680b) {
                    int i6 = this.f7680b - this.f7683e >= i2 ? i2 : this.f7680b - this.f7683e;
                    System.arraycopy(bArr2, this.f7683e, bArr, i, i6);
                    this.f7683e += i6;
                    if (i6 == i2 || inputStream.available() == 0) {
                        i5 = i6;
                    } else {
                        i += i6;
                        i3 = i2 - i6;
                    }
                } else {
                    i3 = i2;
                }
                while (true) {
                    if (this.f7682d == -1 && i3 >= bArr2.length) {
                        i4 = inputStream.read(bArr, i, i3);
                        if (i4 == -1) {
                            if (i3 != i2) {
                                i5 = i2 - i3;
                            }
                        }
                    } else if (m10113a(inputStream, bArr2) != -1) {
                        if (bArr2 != this.f7679a) {
                            bArr2 = this.f7679a;
                            if (bArr2 == null) {
                                throw m10114b();
                            }
                        }
                        i4 = this.f7680b - this.f7683e >= i3 ? i3 : this.f7680b - this.f7683e;
                        System.arraycopy(bArr2, this.f7683e, bArr, i, i4);
                        this.f7683e += i4;
                    } else if (i3 != i2) {
                        i5 = i2 - i3;
                    }
                    i3 -= i4;
                    if (i3 == 0) {
                        i5 = i2;
                        break;
                    } else if (inputStream.available() == 0) {
                        i5 = i2 - i3;
                        break;
                    } else {
                        i += i4;
                    }
                }
            }
        }
        return i5;
    }

    public synchronized void reset() throws IOException {
        if (this.f7679a == null) {
            throw new IOException("Stream is closed");
        } else if (-1 == this.f7682d) {
            throw new InvalidMarkException("Mark has been invalidated");
        } else {
            this.f7683e = this.f7682d;
        }
    }

    public synchronized long skip(long j) throws IOException {
        byte[] bArr = this.f7679a;
        InputStream inputStream = this.in;
        if (bArr == null) {
            throw m10114b();
        } else if (j < 1) {
            j = 0;
        } else if (inputStream == null) {
            throw m10114b();
        } else if (((long) (this.f7680b - this.f7683e)) >= j) {
            this.f7683e = (int) (((long) this.f7683e) + j);
        } else {
            long j2 = (long) (this.f7680b - this.f7683e);
            this.f7683e = this.f7680b;
            if (this.f7682d == -1 || j > ((long) this.f7681c)) {
                j = j2 + inputStream.skip(j - j2);
            } else if (m10113a(inputStream, bArr) == -1) {
                j = j2;
            } else if (((long) (this.f7680b - this.f7683e)) >= j - j2) {
                this.f7683e = (int) (((long) this.f7683e) + (j - j2));
            } else {
                long j3 = (((long) this.f7680b) + j2) - ((long) this.f7683e);
                this.f7683e = this.f7680b;
                j = j3;
            }
        }
        return j;
    }
}
