package com.bumptech.glide.load;

public enum DecodeFormat {
    ALWAYS_ARGB_8888,
    PREFER_ARGB_8888,
    PREFER_RGB_565;
    

    /* renamed from: d */
    public static final DecodeFormat f7646d = null;

    static {
        f7646d = PREFER_RGB_565;
    }
}
