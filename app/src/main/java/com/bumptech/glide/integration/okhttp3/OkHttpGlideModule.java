package com.bumptech.glide.integration.okhttp3;

import android.content.Context;
import java.io.InputStream;

public class OkHttpGlideModule implements GlideModule {
    /* renamed from: a */
    public void mo11054a(Context context, Glide lcVar) {
        lcVar.mo14636a(GlideUrl.class, InputStream.class, (ModelLoaderFactory<T, Y>) new C2781a<T,Y>());
    }

    /* renamed from: a */
    public void mo11055a(Context context, GlideBuilder ldVar) {
    }
}
