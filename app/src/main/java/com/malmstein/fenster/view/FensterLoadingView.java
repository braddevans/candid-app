package com.malmstein.fenster.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

public class FensterLoadingView extends FrameLayout {
    public FensterLoadingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FensterLoadingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* renamed from: a */
    public void mo13009a() {
        setVisibility(0);
    }

    /* renamed from: b */
    public void mo13010b() {
        setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater.from(getContext()).inflate(C1086c.fen__view_loading, this);
    }
}
