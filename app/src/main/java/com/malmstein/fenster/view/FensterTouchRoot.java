package com.malmstein.fenster.view;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public final class FensterTouchRoot extends FrameLayout {

    /* renamed from: a */
    private long f9691a;

    /* renamed from: b */
    private C2233a f9692b;

    /* renamed from: com.malmstein.fenster.view.FensterTouchRoot$a */
    public interface C2233a {
        /* renamed from: f */
        void mo12974f();
    }

    public FensterTouchRoot(Context context) {
        super(context);
    }

    public FensterTouchRoot(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FensterTouchRoot(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.f9692b != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime - this.f9691a > 1000) {
                this.f9691a = elapsedRealtime;
                this.f9692b.mo12974f();
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void setOnTouchReceiver(C2233a aVar) {
        this.f9692b = aVar;
    }
}
