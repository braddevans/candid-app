package com.malmstein.fenster.view;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.TypedArray;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.MediaController.MediaPlayerControl;
import java.io.IOException;
import java.util.Map;

public class FensterVideoView extends TextureView implements MediaPlayerControl, bca {

    /* renamed from: A */
    private OnErrorListener f9693A;

    /* renamed from: B */
    private OnBufferingUpdateListener f9694B;

    /* renamed from: C */
    private SurfaceTextureListener f9695C;

    /* renamed from: D */
    private final OnInfoListener f9696D;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final bcc f9697a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f9698b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f9699c;

    /* renamed from: d */
    private ScaleType f9700d;

    /* renamed from: e */
    private Uri f9701e;

    /* renamed from: f */
    private AssetFileDescriptor f9702f;

    /* renamed from: g */
    private Map<String, String> f9703g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public SurfaceTexture f9704h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public MediaPlayer f9705i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public bbv f9706j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public OnCompletionListener f9707k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public OnPreparedListener f9708l;

    /* renamed from: m */
    private OnErrorListener f9709m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public OnInfoListener f9710n;

    /* renamed from: o */
    private int f9711o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public int f9712p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public int f9713q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public boolean f9714r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public boolean f9715s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public boolean f9716t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public bcb f9717u;

    /* renamed from: v */
    private AlertDialog f9718v;

    /* renamed from: w */
    private OnVideoSizeChangedListener f9719w;

    /* renamed from: x */
    private OnPreparedListener f9720x;

    /* renamed from: y */
    private OnCompletionListener f9721y;

    /* renamed from: z */
    private OnInfoListener f9722z;

    public enum ScaleType {
        SCALE_TO_FIT,
        CROP
    }

    public FensterVideoView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FensterVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9698b = 0;
        this.f9699c = 0;
        this.f9705i = null;
        this.f9719w = new OnVideoSizeChangedListener() {
            public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
                FensterVideoView.this.f9697a.mo8754a(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
                if (FensterVideoView.this.f9697a.mo8755a()) {
                    FensterVideoView.this.requestLayout();
                }
            }
        };
        this.f9720x = new OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                FensterVideoView.this.f9698b = 2;
                FensterVideoView.this.f9714r = true;
                FensterVideoView.this.f9715s = true;
                FensterVideoView.this.f9716t = true;
                if (FensterVideoView.this.f9708l != null) {
                    FensterVideoView.this.f9708l.onPrepared(FensterVideoView.this.f9705i);
                }
                if (FensterVideoView.this.f9706j != null) {
                    FensterVideoView.this.f9706j.setEnabled(true);
                }
                FensterVideoView.this.f9697a.mo8754a(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
                int e = FensterVideoView.this.f9712p;
                if (e != 0) {
                    FensterVideoView.this.seekTo(e);
                }
                if (FensterVideoView.this.f9699c == 3) {
                    FensterVideoView.this.start();
                    FensterVideoView.this.m12396j();
                } else if (FensterVideoView.this.m12364a(e)) {
                    FensterVideoView.this.m12391h();
                }
            }
        };
        this.f9721y = new OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                FensterVideoView.this.setKeepScreenOn(false);
                FensterVideoView.this.f9698b = 5;
                FensterVideoView.this.f9699c = 5;
                FensterVideoView.this.m12393i();
                if (FensterVideoView.this.f9707k != null) {
                    FensterVideoView.this.f9707k.onCompletion(FensterVideoView.this.f9705i);
                }
            }
        };
        this.f9722z = new OnInfoListener() {
            public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                if (FensterVideoView.this.f9710n != null) {
                    FensterVideoView.this.f9710n.onInfo(mediaPlayer, i, i2);
                }
                return true;
            }
        };
        this.f9693A = new OnErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                Log.d("TextureVideoView", "Error: " + i + "," + i2);
                if (FensterVideoView.this.f9698b != -1) {
                    FensterVideoView.this.f9698b = -1;
                    FensterVideoView.this.f9699c = -1;
                    FensterVideoView.this.m12393i();
                    if (!FensterVideoView.this.m12370b(i) && !FensterVideoView.this.m12365a(i, i2)) {
                        FensterVideoView.this.m12376c(i);
                    }
                }
                return true;
            }
        };
        this.f9694B = new OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                FensterVideoView.this.f9713q = i;
            }
        };
        this.f9695C = new SurfaceTextureListener() {
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
                FensterVideoView.this.f9704h = surfaceTexture;
                FensterVideoView.this.m12375c();
            }

            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                FensterVideoView.this.f9704h = null;
                FensterVideoView.this.m12393i();
                FensterVideoView.this.m12363a(true);
                return false;
            }

            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
                boolean z = FensterVideoView.this.f9699c == 3;
                boolean c = FensterVideoView.this.f9697a.mo8757c(i, i2);
                if (FensterVideoView.this.f9705i != null && z && c) {
                    if (FensterVideoView.this.f9712p != 0) {
                        FensterVideoView.this.seekTo(FensterVideoView.this.f9712p);
                    }
                    FensterVideoView.this.start();
                }
            }

            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
                FensterVideoView.this.f9704h = surfaceTexture;
            }
        };
        this.f9696D = new OnInfoListener() {
            public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                if (!FensterVideoView.this.m12400l()) {
                    if (3 == i) {
                        FensterVideoView.this.f9717u.mo8751c();
                        FensterVideoView.this.f9717u.mo8752d();
                    }
                    if (701 == i) {
                        FensterVideoView.this.f9717u.mo8753e();
                    }
                    if (702 == i) {
                        FensterVideoView.this.f9717u.mo8752d();
                    }
                }
                return false;
            }
        };
        m12360a(context, attributeSet);
        this.f9697a = new bcc();
        m12359a();
    }

    /* renamed from: a */
    private static AlertDialog m12356a(Context context, final OnCompletionListener onCompletionListener, final MediaPlayer mediaPlayer, int i) {
        return new Builder(context).setMessage(i).setPositiveButton(17039370, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (onCompletionListener != null) {
                    onCompletionListener.onCompletion(mediaPlayer);
                }
            }
        }).setCancelable(false).create();
    }

    /* renamed from: a */
    private void m12359a() {
        this.f9697a.mo8754a(0, 0);
        setSurfaceTextureListener(this.f9695C);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.f9698b = 0;
        this.f9699c = 0;
        setOnInfoListener(this.f9696D);
    }

    /* renamed from: a */
    private void m12360a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C1088e.FensterVideoView);
        if (obtainStyledAttributes != null) {
            if (context.obtainStyledAttributes(attributeSet, new int[]{C1084a.scaleType}) != null) {
                try {
                    this.f9700d = ScaleType.values()[obtainStyledAttributes.getInt(0, 0)];
                } finally {
                    obtainStyledAttributes.recycle();
                }
            } else {
                this.f9700d = ScaleType.SCALE_TO_FIT;
            }
        }
    }

    /* renamed from: a */
    private void m12361a(Uri uri, Map<String, String> map, int i) {
        Log.d("TextureVideoView", "start playing: " + uri);
        this.f9701e = uri;
        this.f9703g = map;
        this.f9712p = i * 1000;
        m12375c();
        requestLayout();
        invalidate();
    }

    /* renamed from: a */
    private void m12362a(Exception exc) {
        Log.w("Unable to open content:" + this.f9701e, exc);
        this.f9698b = -1;
        this.f9699c = -1;
        this.f9693A.onError(this.f9705i, 1, 0);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m12363a(boolean z) {
        if (this.f9705i != null) {
            this.f9705i.reset();
            this.f9705i.release();
            this.f9705i = null;
            this.f9698b = 0;
            if (z) {
                this.f9699c = 0;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m12364a(int i) {
        return !isPlaying() && (i != 0 || getCurrentPosition() > 0);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m12365a(int i, int i2) {
        if (this.f9709m != null) {
            return this.f9709m.onError(this.f9705i, i, i2);
        }
        return false;
    }

    /* renamed from: b */
    private void m12369b() {
        this.f9702f = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m12370b(int i) {
        if (i == 1 || i == -1004) {
            Log.e("TextureVideoView", "TextureVideoView error. File or network related operation errors.");
            if (m12402m()) {
                return this.f9717u.mo8750b(this.f9705i.getCurrentPosition() / 1000);
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m12375c() {
        if (!m12385e()) {
            m12388f();
            m12363a(false);
            try {
                this.f9705i = new MediaPlayer();
                if (this.f9711o != 0) {
                    this.f9705i.setAudioSessionId(this.f9711o);
                } else {
                    this.f9711o = this.f9705i.getAudioSessionId();
                }
                this.f9705i.setOnPreparedListener(this.f9720x);
                this.f9705i.setOnVideoSizeChangedListener(this.f9719w);
                this.f9705i.setOnCompletionListener(this.f9721y);
                this.f9705i.setOnErrorListener(this.f9693A);
                this.f9705i.setOnInfoListener(this.f9722z);
                this.f9705i.setOnBufferingUpdateListener(this.f9694B);
                this.f9713q = 0;
                m12380d();
                setScaleType(this.f9700d);
                this.f9705i.setSurface(new Surface(this.f9704h));
                this.f9705i.setAudioStreamType(3);
                this.f9705i.setScreenOnWhilePlaying(true);
                this.f9705i.prepareAsync();
                this.f9698b = 1;
                m12389g();
            } catch (IOException | IllegalArgumentException e) {
                m12362a(e);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m12376c(int i) {
        if (getWindowToken() != null) {
            if (this.f9718v != null && this.f9718v.isShowing()) {
                Log.d("TextureVideoView", "Dismissing last error dialog for a new one");
                this.f9718v.dismiss();
            }
            this.f9718v = m12356a(getContext(), this.f9707k, this.f9705i, m12378d(i));
            this.f9718v.show();
        }
    }

    /* renamed from: d */
    private static int m12378d(int i) {
        int i2 = C1087d.fen__play_error_message;
        if (i == -1004) {
            Log.e("TextureVideoView", "TextureVideoView error. File or network related operation errors.");
            return i2;
        } else if (i == -1007) {
            Log.e("TextureVideoView", "TextureVideoView error. Bitstream is not conforming to the related coding standard or file spec.");
            return i2;
        } else if (i == 100) {
            Log.e("TextureVideoView", "TextureVideoView error. Media server died. In this case, the application must release the MediaPlayer object and instantiate a new one.");
            return i2;
        } else if (i == -110) {
            Log.e("TextureVideoView", "TextureVideoView error. Some operation takes too long to complete, usually more than 3-5 seconds.");
            return i2;
        } else if (i == 1) {
            Log.e("TextureVideoView", "TextureVideoView error. Unspecified media player error.");
            return i2;
        } else if (i == -1010) {
            Log.e("TextureVideoView", "TextureVideoView error. Bitstream is conforming to the related coding standard or file spec, but the media framework does not support the feature.");
            return i2;
        } else if (i != 200) {
            return i2;
        } else {
            Log.e("TextureVideoView", "TextureVideoView error. The video is streamed and its container is not valid for progressive playback i.e the video's index (e.g moov atom) is not at the start of the file.");
            return C1087d.fen__play_progressive_error_message;
        }
    }

    /* renamed from: d */
    private void m12380d() throws IOException {
        if (this.f9702f != null) {
            this.f9705i.setDataSource(this.f9702f.getFileDescriptor(), this.f9702f.getStartOffset(), this.f9702f.getLength());
        } else {
            this.f9705i.setDataSource(getContext(), this.f9701e, this.f9703g);
        }
    }

    /* renamed from: e */
    private boolean m12385e() {
        return this.f9704h == null;
    }

    /* renamed from: f */
    private void m12388f() {
        Intent intent = new Intent("com.android.music.musicservicecommand");
        intent.putExtra("command", "pause");
        getContext().sendBroadcast(intent);
    }

    /* renamed from: g */
    private void m12389g() {
        if (this.f9705i != null && this.f9706j != null) {
            this.f9706j.setMediaPlayer(this);
            this.f9706j.setEnabled(m12398k());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public void m12391h() {
        if (this.f9706j != null) {
            this.f9706j.mo8725a(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m12393i() {
        if (this.f9706j != null) {
            this.f9706j.mo8726b();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m12396j() {
        if (this.f9706j != null) {
            this.f9706j.mo8724a();
        }
    }

    /* renamed from: k */
    private boolean m12398k() {
        return (this.f9705i == null || this.f9698b == -1 || this.f9698b == 0 || this.f9698b == 1) ? false : true;
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public boolean m12400l() {
        return !m12402m();
    }

    /* renamed from: m */
    private boolean m12402m() {
        return this.f9717u != null;
    }

    private void setOnInfoListener(OnInfoListener onInfoListener) {
        this.f9710n = onInfoListener;
    }

    private void setScaleType(ScaleType scaleType) {
        switch (scaleType) {
            case SCALE_TO_FIT:
                this.f9705i.setVideoScalingMode(1);
                return;
            case CROP:
                this.f9705i.setVideoScalingMode(2);
                return;
            default:
                return;
        }
    }

    public boolean canPause() {
        return this.f9714r;
    }

    public boolean canSeekBackward() {
        return this.f9715s;
    }

    public boolean canSeekForward() {
        return this.f9716t;
    }

    public int getAudioSessionId() {
        if (this.f9711o == 0) {
            MediaPlayer mediaPlayer = new MediaPlayer();
            this.f9711o = mediaPlayer.getAudioSessionId();
            mediaPlayer.release();
        }
        return this.f9711o;
    }

    public int getBufferPercentage() {
        if (this.f9705i != null) {
            return this.f9713q;
        }
        return 0;
    }

    public int getCurrentPosition() {
        if (m12398k()) {
            return this.f9705i.getCurrentPosition();
        }
        return 0;
    }

    public int getCurrentPositionInSeconds() {
        return getCurrentPosition() / 1000;
    }

    public int getDuration() {
        if (m12398k()) {
            return this.f9705i.getDuration();
        }
        return -1;
    }

    public boolean isPlaying() {
        return m12398k() && this.f9705i.isPlaying();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(FensterVideoView.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(FensterVideoView.class.getName());
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        boolean z = (i == 4 || i == 24 || i == 25 || i == 164 || i == 82 || i == 5 || i == 6) ? false : true;
        if (m12398k() && z && this.f9706j != null) {
            if (i == 79 || i == 85) {
                if (this.f9705i.isPlaying()) {
                    pause();
                    m12396j();
                    return true;
                }
                start();
                m12393i();
                return true;
            } else if (i == 126) {
                if (this.f9705i.isPlaying()) {
                    return true;
                }
                start();
                m12393i();
                return true;
            } else if (i != 86 && i != 127) {
                this.f9706j.mo8724a();
            } else if (!this.f9705i.isPlaying()) {
                return true;
            } else {
                pause();
                m12396j();
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        C1089a b = this.f9697a.mo8756b(i, i2);
        setMeasuredDimension(b.mo8758a(), b.mo8759b());
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (m12398k() && this.f9706j != null) {
            this.f9706j.mo8724a();
        }
        return false;
    }

    public void pause() {
        if (m12398k() && this.f9705i.isPlaying()) {
            this.f9705i.pause();
            this.f9698b = 4;
            setKeepScreenOn(false);
        }
        this.f9699c = 4;
    }

    public void seekTo(int i) {
        if (m12398k()) {
            this.f9705i.seekTo(i);
            this.f9712p = 0;
            return;
        }
        this.f9712p = i;
    }

    public void setMediaController(bbv bbv) {
        m12393i();
        this.f9706j = bbv;
        m12389g();
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        this.f9707k = onCompletionListener;
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.f9709m = onErrorListener;
    }

    public void setOnPlayStateListener(bcb bcb) {
        this.f9717u = bcb;
    }

    public void setOnPreparedListener(OnPreparedListener onPreparedListener) {
        this.f9708l = onPreparedListener;
    }

    public void setVideo(AssetFileDescriptor assetFileDescriptor) {
        this.f9702f = assetFileDescriptor;
        m12361a((Uri) null, null, 0);
    }

    public void setVideo(AssetFileDescriptor assetFileDescriptor, int i) {
        this.f9702f = assetFileDescriptor;
        m12361a((Uri) null, null, i);
    }

    public void setVideo(Uri uri, int i) {
        m12369b();
        m12361a(uri, null, i);
    }

    public void setVideo(String str) {
        m12369b();
        setVideo(Uri.parse(str), 0);
    }

    public void setVideo(String str, int i) {
        m12369b();
        setVideo(Uri.parse(str), i);
    }

    public void start() {
        if (m12398k()) {
            this.f9705i.start();
            setKeepScreenOn(true);
            this.f9698b = 3;
        }
        this.f9699c = 3;
    }
}
