package com.malmstein.fenster.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.malmstein.fenster.gestures.FensterGestureControllerView;
import com.malmstein.fenster.seekbar.BrightnessSeekBar;
import com.malmstein.fenster.seekbar.BrightnessSeekBar.C2229a;
import com.malmstein.fenster.seekbar.VolumeSeekBar;
import com.malmstein.fenster.seekbar.VolumeSeekBar.C2232a;
import java.util.Formatter;
import java.util.Locale;

public final class MediaFensterPlayerController extends RelativeLayout implements bbv, bbx, C2229a, C2232a {

    /* renamed from: a */
    private final OnClickListener f9628a;

    /* renamed from: b */
    private bbw f9629b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public bca f9630c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f9631d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f9632e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final Handler f9633f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f9634g;

    /* renamed from: h */
    private boolean f9635h;

    /* renamed from: i */
    private StringBuilder f9636i;

    /* renamed from: j */
    private Formatter f9637j;

    /* renamed from: k */
    private FensterGestureControllerView f9638k;

    /* renamed from: l */
    private View f9639l;

    /* renamed from: m */
    private SeekBar f9640m;

    /* renamed from: n */
    private BrightnessSeekBar f9641n;

    /* renamed from: o */
    private VolumeSeekBar f9642o;

    /* renamed from: p */
    private TextView f9643p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public TextView f9644q;

    /* renamed from: r */
    private final OnSeekBarChangeListener f9645r;

    /* renamed from: s */
    private ImageButton f9646s;

    /* renamed from: t */
    private ImageButton f9647t;

    /* renamed from: u */
    private ImageButton f9648u;

    /* renamed from: v */
    private int f9649v;

    public MediaFensterPlayerController(Context context) {
        this(context, null);
    }

    public MediaFensterPlayerController(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MediaFensterPlayerController(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9628a = new OnClickListener() {
            public void onClick(View view) {
                MediaFensterPlayerController.this.m12285p();
                MediaFensterPlayerController.this.mo8725a(5000);
            }
        };
        this.f9633f = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        if (MediaFensterPlayerController.this.f9630c.isPlaying()) {
                            MediaFensterPlayerController.this.mo8726b();
                            return;
                        }
                        Message obtainMessage = obtainMessage(1);
                        removeMessages(1);
                        sendMessageDelayed(obtainMessage, 5000);
                        return;
                    case 2:
                        int c = MediaFensterPlayerController.this.m12283n();
                        if (!MediaFensterPlayerController.this.f9632e && MediaFensterPlayerController.this.f9631d && MediaFensterPlayerController.this.f9630c.isPlaying()) {
                            sendMessageDelayed(obtainMessage(2), (long) (1000 - (c % 1000)));
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        this.f9635h = true;
        this.f9645r = new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                if (z || MediaFensterPlayerController.this.f9634g) {
                    long duration = (((long) i) * ((long) MediaFensterPlayerController.this.f9630c.getDuration())) / 1000;
                    MediaFensterPlayerController.this.f9630c.seekTo((int) duration);
                    if (MediaFensterPlayerController.this.f9644q != null) {
                        MediaFensterPlayerController.this.f9644q.setText(MediaFensterPlayerController.this.m12271b((int) duration));
                    }
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                MediaFensterPlayerController.this.mo8725a(3600000);
                MediaFensterPlayerController.this.f9632e = true;
                MediaFensterPlayerController.this.f9633f.removeMessages(2);
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                MediaFensterPlayerController.this.f9632e = false;
                MediaFensterPlayerController.this.m12283n();
                MediaFensterPlayerController.this.m12284o();
                MediaFensterPlayerController.this.mo8725a(5000);
                MediaFensterPlayerController.this.f9633f.sendEmptyMessage(2);
            }
        };
        this.f9649v = -1;
    }

    /* renamed from: a */
    private int m12263a(float f, SeekBar seekBar) {
        return m12264a(getWidth(), f, seekBar);
    }

    /* renamed from: a */
    private int m12264a(int i, float f, SeekBar seekBar) {
        float f2;
        int i2 = (int) f;
        float progress = (float) seekBar.getProgress();
        int max = seekBar.getMax();
        if (i2 < 0) {
            f2 = progress - ((((float) i2) / ((float) (max - i))) * progress);
        } else {
            f2 = progress + (((float) max) * (((float) i2) / ((float) i)));
        }
        return (int) f2;
    }

    /* renamed from: a */
    private void m12266a(float f) {
        this.f9642o.mo12999a(m12269b(f, (SeekBar) this.f9642o));
    }

    /* renamed from: b */
    private int m12269b(float f, SeekBar seekBar) {
        return m12264a(getHeight(), f, seekBar);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public String m12271b(int i) {
        int i2 = i / 1000;
        int i3 = i2 % 60;
        int i4 = (i2 / 60) % 60;
        int i5 = i2 / 3600;
        this.f9636i.setLength(0);
        if (i5 > 0) {
            return this.f9637j.format("%d:%02d:%02d", new Object[]{Integer.valueOf(i5), Integer.valueOf(i4), Integer.valueOf(i3)}).toString();
        }
        return this.f9637j.format("%02d:%02d", new Object[]{Integer.valueOf(i4), Integer.valueOf(i3)}).toString();
    }

    /* renamed from: b */
    private void m12272b(float f) {
        this.f9641n.mo12991a((int) f);
    }

    /* renamed from: c */
    private void m12274c(float f) {
        this.f9645r.onProgressChanged(this.f9640m, m12263a(f, this.f9640m), true);
    }

    /* renamed from: l */
    private void m12281l() {
        this.f9639l = findViewById(C1085b.media_controller_bottom_root);
        this.f9638k = (FensterGestureControllerView) findViewById(C1085b.media_controller_gestures_area);
        this.f9638k.setFensterEventsListener(this);
        this.f9646s = (ImageButton) findViewById(C1085b.fen__media_controller_pause);
        this.f9646s.requestFocus();
        this.f9646s.setOnClickListener(this.f9628a);
        this.f9647t = (ImageButton) findViewById(C1085b.fen__media_controller_next);
        this.f9648u = (ImageButton) findViewById(C1085b.fen__media_controller_previous);
        this.f9640m = (SeekBar) findViewById(C1085b.fen__media_controller_progress);
        this.f9640m.setOnSeekBarChangeListener(this.f9645r);
        this.f9640m.setMax(1000);
        this.f9642o = (VolumeSeekBar) findViewById(C1085b.fen__media_controller_volume);
        this.f9642o.mo13000a((C2232a) this);
        this.f9641n = (BrightnessSeekBar) findViewById(C1085b.fen__media_controller_brightness);
        this.f9641n.mo12992a((C2229a) this);
        this.f9643p = (TextView) findViewById(C1085b.fen__media_controller_time);
        this.f9644q = (TextView) findViewById(C1085b.fen__media_controller_time_current);
        this.f9636i = new StringBuilder();
        this.f9637j = new Formatter(this.f9636i, Locale.getDefault());
    }

    /* renamed from: m */
    private void m12282m() {
        this.f9639l.setVisibility(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: n */
    public int m12283n() {
        if (this.f9630c == null || this.f9632e) {
            return 0;
        }
        int currentPosition = this.f9630c.getCurrentPosition();
        int duration = this.f9630c.getDuration();
        if (this.f9640m != null) {
            if (duration > 0) {
                this.f9640m.setProgress((int) ((1000 * ((long) currentPosition)) / ((long) duration)));
            }
            this.f9640m.setSecondaryProgress(this.f9630c.getBufferPercentage() * 10);
        }
        if (this.f9643p != null) {
            this.f9643p.setText(m12271b(duration));
        }
        if (this.f9644q != null) {
            this.f9644q.setText(m12271b(currentPosition));
        }
        int i = currentPosition / 1000;
        if (this.f9649v == i) {
            return currentPosition;
        }
        this.f9649v = i;
        return currentPosition;
    }

    /* access modifiers changed from: private */
    /* renamed from: o */
    public void m12284o() {
        if (this.f9646s != null) {
            if (this.f9630c.isPlaying()) {
                this.f9646s.setImageResource(17301539);
            } else {
                this.f9646s.setImageResource(17301540);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: p */
    public void m12285p() {
        if (this.f9630c.isPlaying()) {
            this.f9630c.pause();
        } else {
            this.f9630c.start();
        }
        m12284o();
    }

    /* renamed from: q */
    private void m12286q() {
        this.f9645r.onProgressChanged(this.f9640m, m12288s(), true);
    }

    /* renamed from: r */
    private void m12287r() {
        this.f9645r.onProgressChanged(this.f9640m, m12289t(), true);
    }

    /* renamed from: s */
    private int m12288s() {
        return this.f9640m.getProgress() + 100;
    }

    /* renamed from: t */
    private int m12289t() {
        return this.f9640m.getProgress() - 100;
    }

    /* renamed from: a */
    public void mo8724a() {
        mo8725a(5000);
    }

    /* renamed from: a */
    public void mo8725a(int i) {
        if (!this.f9631d) {
            m12282m();
            m12283n();
            if (this.f9646s != null) {
                this.f9646s.requestFocus();
            }
            this.f9631d = true;
            setVisibility(0);
        }
        m12284o();
        this.f9633f.sendEmptyMessage(2);
        Message obtainMessage = this.f9633f.obtainMessage(1);
        if (i != 0) {
            this.f9633f.removeMessages(1);
            this.f9633f.sendMessageDelayed(obtainMessage, (long) i);
        }
        if (this.f9629b != null) {
            this.f9629b.mo8729a(true);
        }
    }

    /* renamed from: a */
    public void mo8730a(MotionEvent motionEvent, float f) {
        if (motionEvent.getPointerCount() == 1) {
            m12274c(f);
        } else if (f > 0.0f) {
            m12286q();
        } else {
            m12287r();
        }
    }

    /* renamed from: b */
    public void mo8726b() {
        if (!this.f9632e) {
            if (this.f9631d) {
                try {
                    this.f9633f.removeMessages(2);
                    setVisibility(4);
                } catch (IllegalArgumentException e) {
                    Log.w("MediaController", "already removed");
                }
                this.f9631d = false;
            }
            if (this.f9629b != null) {
                this.f9629b.mo8729a(false);
            }
        }
    }

    /* renamed from: b */
    public void mo8731b(MotionEvent motionEvent, float f) {
        if (motionEvent.getPointerCount() == 1) {
            m12266a(-f);
        } else {
            m12272b(-f);
        }
    }

    /* renamed from: c */
    public void mo8732c() {
        Log.i("PlayerController", "Single Tap Up");
    }

    /* renamed from: d */
    public void mo8733d() {
        m12286q();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        boolean z = keyEvent.getRepeatCount() == 0 && keyEvent.getAction() == 0;
        if (keyCode == 79 || keyCode == 85 || keyCode == 62) {
            if (!z) {
                return true;
            }
            m12285p();
            mo8725a(5000);
            if (this.f9646s == null) {
                return true;
            }
            this.f9646s.requestFocus();
            return true;
        } else if (keyCode == 126) {
            if (!z || this.f9630c.isPlaying()) {
                return true;
            }
            this.f9630c.start();
            m12284o();
            mo8725a(5000);
            return true;
        } else if (keyCode == 86 || keyCode == 127) {
            if (!z || !this.f9630c.isPlaying()) {
                return true;
            }
            this.f9630c.pause();
            m12284o();
            mo8725a(5000);
            return true;
        } else if (keyCode == 25 || keyCode == 24 || keyCode == 164 || keyCode == 27) {
            return super.dispatchKeyEvent(keyEvent);
        } else {
            if (keyCode != 4 && keyCode != 82) {
                mo8725a(5000);
                return super.dispatchKeyEvent(keyEvent);
            } else if (!z) {
                return true;
            } else {
                mo8726b();
                return true;
            }
        }
    }

    /* renamed from: e */
    public void mo8734e() {
        m12287r();
    }

    /* renamed from: f */
    public void mo8735f() {
    }

    /* renamed from: g */
    public void mo8736g() {
    }

    /* renamed from: h */
    public void mo12959h() {
        this.f9632e = true;
    }

    /* renamed from: i */
    public void mo12960i() {
        this.f9632e = false;
    }

    /* renamed from: j */
    public void mo12961j() {
        this.f9632e = true;
    }

    /* renamed from: k */
    public void mo12962k() {
        this.f9632e = false;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        LayoutInflater.from(getContext()).inflate(C1086c.fen__view_media_controller, this);
        m12281l();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(MediaFensterPlayerController.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(MediaFensterPlayerController.class.getName());
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        mo8725a(5000);
        return false;
    }

    public void setEnabled(boolean z) {
        if (this.f9646s != null) {
            this.f9646s.setEnabled(z);
        }
        if (this.f9647t != null) {
            this.f9647t.setEnabled(z);
        }
        if (this.f9648u != null) {
            this.f9648u.setEnabled(z);
        }
        if (this.f9640m != null) {
            this.f9640m.setEnabled(z);
        }
        if (this.f9642o != null) {
            this.f9642o.setEnabled(z);
        }
        if (this.f9641n != null) {
            this.f9641n.setEnabled(z);
        }
        super.setEnabled(z);
    }

    public void setMediaPlayer(bca bca) {
        this.f9630c = bca;
        m12284o();
    }

    public void setVisibilityListener(bbw bbw) {
        this.f9629b = bbw;
    }
}
