package com.malmstein.fenster.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.malmstein.fenster.view.FensterTouchRoot;
import com.malmstein.fenster.view.FensterTouchRoot.C2233a;
import java.util.Formatter;
import java.util.Locale;

public final class SimpleMediaFensterPlayerController extends FrameLayout implements bbv, bcb, C2233a {

    /* renamed from: a */
    private bbw f9653a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public bca f9654b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public boolean f9655c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f9656d;

    /* renamed from: e */
    private boolean f9657e;

    /* renamed from: f */
    private boolean f9658f;

    /* renamed from: g */
    private StringBuilder f9659g;

    /* renamed from: h */
    private Formatter f9660h;

    /* renamed from: i */
    private View f9661i;

    /* renamed from: j */
    private View f9662j;

    /* renamed from: k */
    private ProgressBar f9663k;

    /* renamed from: l */
    private TextView f9664l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public TextView f9665m;

    /* renamed from: n */
    private ImageButton f9666n;

    /* renamed from: o */
    private ImageButton f9667o;

    /* renamed from: p */
    private ImageButton f9668p;

    /* renamed from: q */
    private ProgressBar f9669q;

    /* renamed from: r */
    private int f9670r;

    /* renamed from: s */
    private final OnSeekBarChangeListener f9671s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public final Handler f9672t;

    /* renamed from: u */
    private final OnClickListener f9673u;

    public SimpleMediaFensterPlayerController(Context context) {
        this(context, null);
    }

    public SimpleMediaFensterPlayerController(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SimpleMediaFensterPlayerController(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9658f = true;
        this.f9670r = -1;
        this.f9671s = new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                if (z) {
                    long duration = (((long) i) * ((long) SimpleMediaFensterPlayerController.this.f9654b.getDuration())) / 1000;
                    SimpleMediaFensterPlayerController.this.f9654b.seekTo((int) duration);
                    if (SimpleMediaFensterPlayerController.this.f9665m != null) {
                        SimpleMediaFensterPlayerController.this.f9665m.setText(SimpleMediaFensterPlayerController.this.m12309c((int) duration));
                    }
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                SimpleMediaFensterPlayerController.this.mo8725a(3600000);
                SimpleMediaFensterPlayerController.this.f9656d = true;
                SimpleMediaFensterPlayerController.this.f9672t.removeMessages(2);
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                SimpleMediaFensterPlayerController.this.f9656d = false;
                SimpleMediaFensterPlayerController.this.m12315h();
                SimpleMediaFensterPlayerController.this.m12317i();
                SimpleMediaFensterPlayerController.this.mo8725a(5000);
                SimpleMediaFensterPlayerController.this.f9672t.sendEmptyMessage(2);
            }
        };
        this.f9672t = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        if (SimpleMediaFensterPlayerController.this.f9654b.isPlaying()) {
                            SimpleMediaFensterPlayerController.this.mo8726b();
                            return;
                        }
                        Message obtainMessage = obtainMessage(1);
                        removeMessages(1);
                        sendMessageDelayed(obtainMessage, 5000);
                        return;
                    case 2:
                        int d = SimpleMediaFensterPlayerController.this.m12315h();
                        if (!SimpleMediaFensterPlayerController.this.f9656d && SimpleMediaFensterPlayerController.this.f9655c && SimpleMediaFensterPlayerController.this.f9654b.isPlaying()) {
                            sendMessageDelayed(obtainMessage(2), (long) (1000 - (d % 1000)));
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        this.f9673u = new OnClickListener() {
            public void onClick(View view) {
                SimpleMediaFensterPlayerController.this.m12318j();
                SimpleMediaFensterPlayerController.this.mo8725a(5000);
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public String m12309c(int i) {
        int i2 = i / 1000;
        int i3 = i2 % 60;
        int i4 = (i2 / 60) % 60;
        int i5 = i2 / 3600;
        this.f9659g.setLength(0);
        if (i5 > 0) {
            return this.f9660h.format("%d:%02d:%02d", new Object[]{Integer.valueOf(i5), Integer.valueOf(i4), Integer.valueOf(i3)}).toString();
        }
        return this.f9660h.format("%02d:%02d", new Object[]{Integer.valueOf(i4), Integer.valueOf(i3)}).toString();
    }

    /* renamed from: g */
    private void m12313g() {
        this.f9666n = (ImageButton) findViewById(C1085b.fen__media_controller_pause);
        this.f9666n.requestFocus();
        this.f9666n.setOnClickListener(this.f9673u);
        this.f9667o = (ImageButton) findViewById(C1085b.fen__media_controller_next);
        this.f9668p = (ImageButton) findViewById(C1085b.fen__media_controller_previous);
        this.f9663k = (SeekBar) findViewById(C1085b.fen__media_controller_progress);
        ((SeekBar) this.f9663k).setOnSeekBarChangeListener(this.f9671s);
        this.f9663k.setMax(1000);
        this.f9664l = (TextView) findViewById(C1085b.fen__media_controller_time);
        this.f9665m = (TextView) findViewById(C1085b.fen__media_controller_time_current);
        this.f9659g = new StringBuilder();
        this.f9660h = new Formatter(this.f9659g, Locale.getDefault());
        ((FensterTouchRoot) findViewById(C1085b.media_controller_touch_root)).setOnTouchReceiver(this);
        this.f9661i = findViewById(C1085b.fen__media_controller_bottom_area);
        this.f9661i.setVisibility(4);
        this.f9662j = findViewById(C1085b.media_controller_controls_root);
        this.f9662j.setVisibility(4);
        this.f9669q = (ProgressBar) findViewById(C1085b.fen__media_controller_loading_view);
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public int m12315h() {
        if (this.f9654b == null || this.f9656d) {
            return 0;
        }
        int currentPosition = this.f9654b.getCurrentPosition();
        int duration = this.f9654b.getDuration();
        if (this.f9663k != null) {
            if (duration > 0) {
                this.f9663k.setProgress((int) ((1000 * ((long) currentPosition)) / ((long) duration)));
            }
            this.f9663k.setSecondaryProgress(this.f9654b.getBufferPercentage() * 10);
        }
        if (this.f9664l != null) {
            this.f9664l.setText(m12309c(duration));
        }
        if (this.f9665m != null) {
            this.f9665m.setText(m12309c(currentPosition));
        }
        int i = currentPosition / 1000;
        if (this.f9670r == i) {
            return currentPosition;
        }
        this.f9670r = i;
        return currentPosition;
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m12317i() {
        if (this.f9666n != null) {
            if (this.f9654b.isPlaying()) {
                this.f9666n.setImageResource(17301539);
            } else {
                this.f9666n.setImageResource(17301540);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m12318j() {
        if (this.f9654b.isPlaying()) {
            this.f9654b.pause();
        } else {
            this.f9654b.start();
        }
        m12317i();
    }

    /* renamed from: k */
    private void m12319k() {
        mo8726b();
        this.f9669q.setVisibility(8);
        this.f9657e = false;
    }

    /* renamed from: l */
    private void m12320l() {
        this.f9657e = true;
        this.f9669q.setVisibility(0);
    }

    /* renamed from: a */
    public void mo8724a() {
        mo8725a(5000);
    }

    /* renamed from: a */
    public void mo8725a(int i) {
        if (!this.f9655c) {
            m12315h();
            if (this.f9666n != null) {
                this.f9666n.requestFocus();
            }
            this.f9655c = true;
            setVisibility(0);
        }
        m12317i();
        this.f9672t.sendEmptyMessage(2);
        Message obtainMessage = this.f9672t.obtainMessage(1);
        if (i != 0) {
            this.f9672t.removeMessages(1);
            this.f9672t.sendMessageDelayed(obtainMessage, (long) i);
        }
        if (this.f9653a != null) {
            this.f9653a.mo8729a(true);
        }
    }

    /* renamed from: b */
    public void mo8726b() {
        if (this.f9655c) {
            try {
                this.f9672t.removeMessages(2);
                setVisibility(4);
            } catch (IllegalArgumentException e) {
                Log.w("MediaController", "already removed");
            }
            this.f9655c = false;
        }
        if (this.f9653a != null) {
            this.f9653a.mo8729a(false);
        }
    }

    /* renamed from: b */
    public boolean mo8750b(int i) {
        return false;
    }

    /* renamed from: c */
    public void mo8751c() {
        this.f9662j.setVisibility(0);
        this.f9661i.setVisibility(0);
        this.f9658f = false;
    }

    /* renamed from: d */
    public void mo8752d() {
        m12319k();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        boolean z = keyEvent.getRepeatCount() == 0 && keyEvent.getAction() == 0;
        if (keyCode == 79 || keyCode == 85 || keyCode == 62) {
            if (!z) {
                return true;
            }
            m12318j();
            mo8725a(5000);
            if (this.f9666n == null) {
                return true;
            }
            this.f9666n.requestFocus();
            return true;
        } else if (keyCode == 126) {
            if (!z || this.f9654b.isPlaying()) {
                return true;
            }
            this.f9654b.start();
            m12317i();
            mo8725a(5000);
            return true;
        } else if (keyCode == 86 || keyCode == 127) {
            if (!z || !this.f9654b.isPlaying()) {
                return true;
            }
            this.f9654b.pause();
            m12317i();
            mo8725a(5000);
            return true;
        } else if (keyCode == 25 || keyCode == 24 || keyCode == 164 || keyCode == 27) {
            return super.dispatchKeyEvent(keyEvent);
        } else {
            if (keyCode != 4 && keyCode != 82) {
                mo8725a(5000);
                return super.dispatchKeyEvent(keyEvent);
            } else if (!z) {
                return true;
            } else {
                mo8726b();
                return true;
            }
        }
    }

    /* renamed from: e */
    public void mo8753e() {
        m12320l();
    }

    /* renamed from: f */
    public void mo12974f() {
        if (this.f9655c) {
            Log.d("PlayerController", "controller ui touch received!");
            mo8724a();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        LayoutInflater.from(getContext()).inflate(C1086c.fen__view_simple_media_controller, this);
        m12313g();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(SimpleMediaFensterPlayerController.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(SimpleMediaFensterPlayerController.class.getName());
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        mo8725a(5000);
        return false;
    }

    public void setEnabled(boolean z) {
        if (this.f9666n != null) {
            this.f9666n.setEnabled(z);
        }
        if (this.f9667o != null) {
            this.f9667o.setEnabled(z);
        }
        if (this.f9668p != null) {
            this.f9668p.setEnabled(z);
        }
        if (this.f9663k != null) {
            this.f9663k.setEnabled(z);
        }
        super.setEnabled(z);
    }

    public void setMediaPlayer(bca bca) {
        this.f9654b = bca;
        m12317i();
    }

    public void setVisibilityListener(bbw bbw) {
        this.f9653a = bbw;
    }
}
