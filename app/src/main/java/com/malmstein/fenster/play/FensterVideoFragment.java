package com.malmstein.fenster.play;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.malmstein.fenster.view.FensterLoadingView;
import com.malmstein.fenster.view.FensterVideoView;

public class FensterVideoFragment extends Fragment implements bcb {

    /* renamed from: a */
    private View f9678a;

    /* renamed from: b */
    private FensterVideoView f9679b;

    /* renamed from: c */
    private bbv f9680c;

    /* renamed from: d */
    private FensterLoadingView f9681d;

    /* renamed from: b */
    private void m12330b() {
        this.f9679b.setMediaController(this.f9680c);
        this.f9679b.setOnPlayStateListener(this);
    }

    /* renamed from: f */
    private void m12331f() {
        this.f9681d.mo13009a();
        this.f9680c.mo8726b();
    }

    /* renamed from: a */
    public void mo12988a() {
        this.f9681d.mo13010b();
        this.f9680c.mo8724a();
    }

    /* renamed from: b */
    public boolean mo8750b(int i) {
        return false;
    }

    /* renamed from: c */
    public void mo8751c() {
        this.f9680c.mo8724a();
    }

    /* renamed from: d */
    public void mo8752d() {
        mo12988a();
    }

    /* renamed from: e */
    public void mo8753e() {
        m12331f();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f9678a = layoutInflater.inflate(C1086c.fen__fragment_fenster_video, viewGroup);
        this.f9679b = (FensterVideoView) this.f9678a.findViewById(C1085b.fen__play_video_texture);
        this.f9680c = (bbv) this.f9678a.findViewById(C1085b.fen__play_video_controller);
        this.f9681d = (FensterLoadingView) this.f9678a.findViewById(C1085b.fen__play_video_loading);
        return this.f9678a;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        m12330b();
    }
}
