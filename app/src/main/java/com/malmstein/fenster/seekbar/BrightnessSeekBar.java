package com.malmstein.fenster.seekbar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class BrightnessSeekBar extends SeekBar {

    /* renamed from: a */
    public final OnSeekBarChangeListener f9682a = new OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            BrightnessSeekBar.this.setBrightness(i);
            BrightnessSeekBar.this.setProgress(i);
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            BrightnessSeekBar.this.f9683b.mo12961j();
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            BrightnessSeekBar.this.f9683b.mo12962k();
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C2229a f9683b;

    /* renamed from: com.malmstein.fenster.seekbar.BrightnessSeekBar$a */
    public interface C2229a {
        /* renamed from: j */
        void mo12961j();

        /* renamed from: k */
        void mo12962k();
    }

    public BrightnessSeekBar(Context context) {
        super(context);
    }

    public BrightnessSeekBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public BrightnessSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* renamed from: a */
    public void mo12991a(int i) {
        this.f9682a.onProgressChanged(this, i, true);
    }

    /* renamed from: a */
    public void mo12992a(C2229a aVar) {
        setMax(255);
        setOnSeekBarChangeListener(this.f9682a);
        this.f9683b = aVar;
        mo12991a(bbz.m7593a(getContext()));
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(BrightnessSeekBar.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(BrightnessSeekBar.class.getName());
    }

    public void setBrightness(int i) {
        if (i < 0) {
            i = 0;
        } else if (i > 255) {
            i = 255;
        }
        bbz.m7594a(getContext(), i);
    }
}
