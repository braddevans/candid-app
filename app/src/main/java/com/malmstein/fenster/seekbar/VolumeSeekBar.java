package com.malmstein.fenster.seekbar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class VolumeSeekBar extends SeekBar {

    /* renamed from: a */
    public final OnSeekBarChangeListener f9685a = new OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            VolumeSeekBar.this.f9686b.setStreamVolume(3, i, 0);
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            VolumeSeekBar.this.f9687c.mo12959h();
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            VolumeSeekBar.this.f9687c.mo12960i();
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: b */
    public AudioManager f9686b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C2232a f9687c;

    /* renamed from: d */
    private BroadcastReceiver f9688d = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            VolumeSeekBar.this.m12343a();
        }
    };

    /* renamed from: com.malmstein.fenster.seekbar.VolumeSeekBar$a */
    public interface C2232a {
        /* renamed from: h */
        void mo12959h();

        /* renamed from: i */
        void mo12960i();
    }

    public VolumeSeekBar(Context context) {
        super(context);
    }

    public VolumeSeekBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public VolumeSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m12343a() {
        setProgress(this.f9686b.getStreamVolume(3));
    }

    /* renamed from: b */
    private void m12345b() {
        getContext().registerReceiver(this.f9688d, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
    }

    /* renamed from: c */
    private void m12346c() {
        getContext().unregisterReceiver(this.f9688d);
    }

    /* renamed from: a */
    public void mo12999a(int i) {
        this.f9685a.onProgressChanged(this, i, true);
    }

    /* renamed from: a */
    public void mo13000a(C2232a aVar) {
        this.f9686b = (AudioManager) getContext().getSystemService("audio");
        this.f9687c = aVar;
        setMax(this.f9686b.getStreamMaxVolume(3));
        setProgress(this.f9686b.getStreamVolume(3));
        setOnSeekBarChangeListener(this.f9685a);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        m12345b();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        m12346c();
        super.onDetachedFromWindow();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(VolumeSeekBar.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(VolumeSeekBar.class.getName());
    }
}
