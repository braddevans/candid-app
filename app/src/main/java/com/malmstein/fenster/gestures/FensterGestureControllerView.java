package com.malmstein.fenster.gestures;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class FensterGestureControllerView extends View {

    /* renamed from: a */
    private GestureDetector f9677a;

    public FensterGestureControllerView(Context context) {
        super(context);
    }

    public FensterGestureControllerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FensterGestureControllerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* renamed from: a */
    private void m12329a(MotionEvent motionEvent) {
        this.f9677a.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        setClickable(true);
        setFocusable(true);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        m12329a(motionEvent);
        return true;
    }

    public void setFensterEventsListener(bbx bbx) {
        this.f9677a = new GestureDetector(getContext(), new bby(bbx, ViewConfiguration.get(getContext())));
    }
}
