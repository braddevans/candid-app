package com.volokh.danylo.video_player_manager.p008ui;

import android.content.Context;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.TextureView;

/* renamed from: com.volokh.danylo.video_player_manager.ui.ScalableTextureView */
public abstract class ScalableTextureView extends TextureView {

    /* renamed from: a */
    private static final String f9802a = ScalableTextureView.class.getSimpleName();

    /* renamed from: b */
    private Integer f9803b;

    /* renamed from: c */
    private Integer f9804c;

    /* renamed from: d */
    private float f9805d = 0.0f;

    /* renamed from: e */
    private float f9806e = 0.0f;

    /* renamed from: f */
    private float f9807f = 1.0f;

    /* renamed from: g */
    private float f9808g = 1.0f;

    /* renamed from: h */
    private float f9809h = 0.0f;

    /* renamed from: i */
    private float f9810i = 1.0f;

    /* renamed from: j */
    private int f9811j = 0;

    /* renamed from: k */
    private int f9812k = 0;

    /* renamed from: l */
    private final Matrix f9813l = new Matrix();

    /* renamed from: m */
    private ScaleType f9814m;

    /* renamed from: com.volokh.danylo.video_player_manager.ui.ScalableTextureView$ScaleType */
    public enum ScaleType {
        CENTER_CROP,
        TOP,
        BOTTOM,
        FILL
    }

    public ScalableTextureView(Context context) {
        super(context);
    }

    public ScalableTextureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ScalableTextureView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public ScalableTextureView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* renamed from: a */
    private void mo8812a() {
        this.f9813l.reset();
        this.f9813l.setScale(this.f9807f * this.f9810i, this.f9808g * this.f9810i, this.f9805d, this.f9806e);
        this.f9813l.postRotate(this.f9809h, this.f9805d, this.f9806e);
        setTransform(this.f9813l);
    }

    /* renamed from: b */
    private void mo8818b() {
        float f = this.f9807f * this.f9810i;
        float f2 = this.f9808g * this.f9810i;
        this.f9813l.reset();
        this.f9813l.setScale(f, f2, this.f9805d, this.f9806e);
        this.f9813l.postTranslate((float) this.f9811j, (float) this.f9812k);
        setTransform(this.f9813l);
    }

    /* renamed from: d */
    public void mo13100d() {
        float f;
        float f2;
        if (this.f9803b == null || this.f9804c == null) {
            throw new RuntimeException("null content size");
        }
        float measuredWidth = (float) getMeasuredWidth();
        float measuredHeight = (float) getMeasuredHeight();
        float intValue = (float) this.f9803b.intValue();
        float intValue2 = (float) this.f9804c.intValue();
        float f3 = 1.0f;
        float f4 = 1.0f;
        switch (this.f9814m) {
            case FILL:
                if (measuredWidth <= measuredHeight) {
                    f4 = (measuredWidth * intValue2) / (measuredHeight * intValue);
                    break;
                } else {
                    f3 = (measuredHeight * intValue) / (measuredWidth * intValue2);
                    break;
                }
            case BOTTOM:
            case CENTER_CROP:
            case TOP:
                if (intValue <= measuredWidth || intValue2 <= measuredHeight) {
                    if (intValue >= measuredWidth || intValue2 >= measuredHeight) {
                        if (measuredWidth <= intValue) {
                            if (measuredHeight > intValue2) {
                                f3 = (measuredHeight / intValue2) / (measuredWidth / intValue);
                                break;
                            }
                        } else {
                            f4 = (measuredWidth / intValue) / (measuredHeight / intValue2);
                            break;
                        }
                    } else {
                        f4 = measuredWidth / intValue;
                        f3 = measuredHeight / intValue2;
                        break;
                    }
                } else {
                    f3 = intValue / measuredWidth;
                    f4 = intValue2 / measuredHeight;
                    break;
                }
                break;
        }
        switch (this.f9814m) {
            case FILL:
                f = this.f9805d;
                f2 = this.f9806e;
                break;
            case BOTTOM:
                f = measuredWidth;
                f2 = measuredHeight;
                break;
            case CENTER_CROP:
                f = measuredWidth / 2.0f;
                f2 = measuredHeight / 2.0f;
                break;
            case TOP:
                f = 0.0f;
                f2 = 0.0f;
                break;
            default:
                throw new IllegalStateException("pivotPointX, pivotPointY for ScaleType " + this.f9814m + " are not defined");
        }
        float f5 = 1.0f;
        switch (this.f9814m) {
            case BOTTOM:
            case CENTER_CROP:
            case TOP:
                if (this.f9804c.intValue() <= this.f9803b.intValue()) {
                    f5 = measuredHeight / (measuredHeight * f4);
                    break;
                } else {
                    f5 = measuredWidth / (measuredWidth * f3);
                    break;
                }
        }
        this.f9807f = f5 * f3;
        this.f9808g = f5 * f4;
        this.f9805d = f;
        this.f9806e = f2;
        mo8812a();
    }

    public float getContentAspectRatio() {
        if (this.f9803b == null || this.f9804c == null) {
            return 0.0f;
        }
        return ((float) this.f9803b.intValue()) / ((float) this.f9804c.intValue());
    }

    /* access modifiers changed from: protected */
    public final Integer getContentHeight() {
        return this.f9804c;
    }

    public float getContentScale() {
        return this.f9810i;
    }

    /* access modifiers changed from: protected */
    public final Integer getContentWidth() {
        return this.f9803b;
    }

    /* access modifiers changed from: protected */
    public final float getContentX() {
        return (float) this.f9811j;
    }

    /* access modifiers changed from: protected */
    public final float getContentY() {
        return (float) this.f9812k;
    }

    public float getPivotX() {
        return this.f9805d;
    }

    public float getPivotY() {
        return this.f9806e;
    }

    public float getRotation() {
        return this.f9809h;
    }

    public Integer getScaledContentHeight() {
        return Integer.valueOf((int) (this.f9808g * this.f9810i * ((float) getMeasuredHeight())));
    }

    public Integer getScaledContentWidth() {
        return Integer.valueOf((int) (this.f9807f * this.f9810i * ((float) getMeasuredWidth())));
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f9803b != null && this.f9804c != null) {
            mo13100d();
        }
    }

    /* access modifiers changed from: protected */
    public final void setContentHeight(int i) {
        this.f9804c = Integer.valueOf(i);
    }

    public void setContentScale(float f) {
        this.f9810i = f;
        mo8812a();
    }

    /* access modifiers changed from: protected */
    public final void setContentWidth(int i) {
        this.f9803b = Integer.valueOf(i);
    }

    public final void setContentX(float f) {
        this.f9811j = ((int) f) - ((getMeasuredWidth() - getScaledContentWidth().intValue()) / 2);
        mo8818b();
    }

    public final void setContentY(float f) {
        this.f9812k = ((int) f) - ((getMeasuredHeight() - getScaledContentHeight().intValue()) / 2);
        mo8818b();
    }

    public void setPivotX(float f) {
        this.f9805d = f;
    }

    public void setPivotY(float f) {
        this.f9806e = f;
    }

    public void setRotation(float f) {
        this.f9809h = f;
        mo8812a();
    }

    public void setScaleType(ScaleType scaleType) {
        this.f9814m = scaleType;
    }
}
