package com.volokh.danylo.video_player_manager.p008ui;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.volokh.danylo.video_player_manager.ui.MediaPlayerWrapper */
public abstract class MediaPlayerWrapper implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener, OnVideoSizeChangedListener {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f9779a = ("" + this);

    /* renamed from: b */
    private Surface f9780b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public final Handler f9781c = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: d */
    public final MediaPlayer f9782d;

    /* renamed from: e */
    private final AtomicReference<State> f9783e = new AtomicReference<>();
    /* access modifiers changed from: private */

    /* renamed from: f */
    public C2248a f9784f;

    /* renamed from: g */
    private Context f9785g;

    /* renamed from: h */
    private final Runnable f9786h = new Runnable() {
        public void run() {
            bdn.m7747e(MediaPlayerWrapper.this.f9779a, ">> run, onVideoPreparedMainThread");
            MediaPlayerWrapper.this.f9784f.mo8812a();
            bdn.m7747e(MediaPlayerWrapper.this.f9779a, "<< run, onVideoPreparedMainThread");
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: i */
    public final Runnable f9787i = new Runnable() {
        public void run() {
            bdn.m7747e(MediaPlayerWrapper.this.f9779a, ">> run, onVideoStoppedMainThread");
            MediaPlayerWrapper.this.f9784f.mo8821c();
            bdn.m7747e(MediaPlayerWrapper.this.f9779a, "<< run, onVideoStoppedMainThread");
        }
    };

    /* renamed from: com.volokh.danylo.video_player_manager.ui.MediaPlayerWrapper$State */
    public enum State {
        IDLE,
        INITIALIZED,
        PREPARING,
        PREPARED,
        STARTED,
        PAUSED,
        STOPPED,
        PLAYBACK_COMPLETED,
        END,
        ERROR
    }

    /* renamed from: com.volokh.danylo.video_player_manager.ui.MediaPlayerWrapper$a */
    public interface C2248a {
        /* renamed from: a */
        void mo8812a();

        /* renamed from: a */
        void mo8813a(int i);

        /* renamed from: a */
        void mo8814a(int i, int i2);

        /* renamed from: b */
        void mo8818b();

        /* renamed from: b */
        void mo8819b(int i);

        /* renamed from: b */
        void mo8820b(int i, int i2);

        /* renamed from: c */
        void mo8821c();
    }

    protected MediaPlayerWrapper(MediaPlayer mediaPlayer, Context context) {
        this.f9785g = context;
        bdn.m7747e(this.f9779a, "constructor of MediaPlayerWrapper");
        bdn.m7747e(this.f9779a, "constructor of MediaPlayerWrapper, main Looper " + Looper.getMainLooper());
        bdn.m7747e(this.f9779a, "constructor of MediaPlayerWrapper, my Looper " + Looper.myLooper());
        if (Looper.myLooper() != null) {
            throw new RuntimeException("myLooper not null, a bug in some MediaPlayer implementation cause that listeners are not called at all. Please use a thread without Looper");
        }
        this.f9782d = mediaPlayer;
        this.f9783e.set(State.IDLE);
        this.f9782d.setOnVideoSizeChangedListener(this);
        this.f9782d.setOnCompletionListener(this);
        this.f9782d.setOnErrorListener(this);
        this.f9782d.setOnBufferingUpdateListener(this);
        this.f9782d.setOnInfoListener(this);
        this.f9782d.setOnPreparedListener(this);
        this.f9782d.setScreenOnWhilePlaying(true);
    }

    /* renamed from: a */
    private void m12410a(int i) {
        switch (i) {
            case 1:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_UNKNOWN");
                return;
            case 3:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_VIDEO_RENDERING_START");
                return;
            case 700:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_VIDEO_TRACK_LAGGING");
                return;
            case 701:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_BUFFERING_START");
                return;
            case 702:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_BUFFERING_END");
                return;
            case 800:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_BAD_INTERLEAVING");
                return;
            case 801:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_NOT_SEEKABLE");
                return;
            case 802:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_METADATA_UPDATE");
                return;
            case 901:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_UNSUPPORTED_SUBTITLE");
                return;
            case 902:
                bdn.m7745c(this.f9779a, "onInfoMainThread, MEDIA_INFO_SUBTITLE_TIMED_OUT");
                return;
            default:
                return;
        }
    }

    /* renamed from: j */
    private boolean m12415j() {
        return Thread.currentThread().getId() == 1;
    }

    /* renamed from: a */
    public void mo13075a() {
        bdn.m7747e(this.f9779a, ">> execute prepare, mState " + this.f9783e);
        synchronized (this.f9783e) {
            this.f9781c.post(new Runnable() {
                public void run() {
                    try {
                        MediaPlayerWrapper.this.f9782d.prepareAsync();
                    } catch (IllegalStateException e) {
                        MediaPlayerWrapper.this.f9781c.post(MediaPlayerWrapper.this.f9787i);
                    }
                }
            });
        }
    }

    /* renamed from: a */
    public void mo13076a(float f, float f2) {
        try {
            this.f9782d.setVolume(f, f2);
        } catch (IllegalStateException e) {
        }
    }

    /* renamed from: a */
    public void mo13077a(AssetFileDescriptor assetFileDescriptor) throws IOException {
        synchronized (this.f9783e) {
            this.f9782d.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            this.f9783e.set(State.INITIALIZED);
        }
    }

    /* renamed from: a */
    public void mo13078a(SurfaceTexture surfaceTexture) {
        bdn.m7747e(this.f9779a, ">> setSurfaceTexture " + surfaceTexture);
        bdn.m7747e(this.f9779a, "setSurfaceTexture mSurface " + this.f9780b);
        if (surfaceTexture != null) {
            this.f9780b = new Surface(surfaceTexture);
            try {
                this.f9782d.setSurface(this.f9780b);
            } catch (IllegalStateException e) {
                Log.i("TAG", "MediaPlayer setSurface failed.");
            }
        } else {
            this.f9782d.setSurface(null);
        }
        bdn.m7747e(this.f9779a, "<< setSurfaceTexture " + surfaceTexture);
    }

    /* renamed from: a */
    public void mo13079a(C2248a aVar) {
        this.f9784f = aVar;
    }

    /* renamed from: a */
    public void mo13080a(String str) throws IOException {
        synchronized (this.f9783e) {
            bdn.m7747e(this.f9779a, "setDataSource, filePath " + str + ", mState " + this.f9783e);
            this.f9782d.setDataSource(this.f9785g, Uri.parse(str));
            this.f9783e.set(State.INITIALIZED);
        }
    }

    /* renamed from: a */
    public void mo13081a(boolean z) {
        bdn.m7747e(this.f9779a, "setLooping " + z);
        try {
            this.f9782d.setLooping(z);
        } catch (IllegalStateException e) {
            bdn.m7742a(this.f9779a, e.toString());
        }
    }

    /* renamed from: b */
    public void mo13082b() {
        bdn.m7747e(this.f9779a, ">> start");
        synchronized (this.f9783e) {
            bdn.m7747e(this.f9779a, "start, mState " + this.f9783e);
            try {
                this.f9782d.start();
                this.f9783e.set(State.STARTED);
            } catch (IllegalStateException e) {
                this.f9781c.post(this.f9787i);
            }
        }
        bdn.m7747e(this.f9779a, "<< start");
    }

    /* renamed from: c */
    public void mo13083c() {
        bdn.m7747e(this.f9779a, ">> stop");
        synchronized (this.f9783e) {
            bdn.m7747e(this.f9779a, "stop, mState " + this.f9783e);
            this.f9782d.stop();
            this.f9783e.set(State.STOPPED);
            if (this.f9784f != null) {
                this.f9781c.post(this.f9787i);
            }
        }
        bdn.m7747e(this.f9779a, "<< stop");
    }

    /* renamed from: d */
    public void mo13084d() {
        bdn.m7747e(this.f9779a, ">> reset , mState " + this.f9783e);
        synchronized (this.f9783e) {
            this.f9782d.reset();
            this.f9783e.set(State.IDLE);
        }
        bdn.m7747e(this.f9779a, "<< reset , mState " + this.f9783e);
    }

    /* renamed from: e */
    public void mo13085e() {
        bdn.m7747e(this.f9779a, ">> release, mState " + this.f9783e);
        synchronized (this.f9783e) {
            this.f9782d.release();
            this.f9783e.set(State.END);
        }
        bdn.m7747e(this.f9779a, "<< release, mState " + this.f9783e);
    }

    /* renamed from: f */
    public void mo13086f() {
        bdn.m7747e(this.f9779a, ">> clearAll, mState " + this.f9783e);
        synchronized (this.f9783e) {
            this.f9782d.setOnVideoSizeChangedListener(null);
            this.f9782d.setOnCompletionListener(null);
            this.f9782d.setOnErrorListener(null);
            this.f9782d.setOnBufferingUpdateListener(null);
            this.f9782d.setOnInfoListener(null);
            this.f9782d.setOnPreparedListener(null);
        }
        bdn.m7747e(this.f9779a, "<< clearAll, mState " + this.f9783e);
    }

    /* renamed from: g */
    public int mo13087g() {
        try {
            return this.f9782d.getCurrentPosition();
        } catch (IllegalStateException e) {
            return 0;
        }
    }

    /* renamed from: h */
    public int mo13088h() {
        int duration;
        synchronized (this.f9783e) {
            duration = this.f9782d.getDuration();
        }
        return duration;
    }

    /* renamed from: i */
    public State mo13089i() {
        State state;
        synchronized (this.f9783e) {
            state = (State) this.f9783e.get();
        }
        return state;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        bdn.m7747e("VIDEO_TAG", "onBufferingUpdate percent : " + i);
        if (this.f9784f != null) {
            this.f9784f.mo8813a(i);
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        bdn.m7747e(this.f9779a, "onVideoCompletion, mState " + this.f9783e);
        synchronized (this.f9783e) {
            this.f9783e.set(State.PLAYBACK_COMPLETED);
        }
        if (this.f9784f != null) {
            this.f9784f.mo8818b();
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        bdn.m7747e(this.f9779a, "onErrorMainThread, what " + i + ", extra " + i2);
        synchronized (this.f9783e) {
            this.f9783e.set(State.ERROR);
        }
        if ((i == 1 || i == 100) && this.f9784f != null) {
            this.f9784f.mo8820b(i, i2);
        }
        return true;
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
        bdn.m7747e(this.f9779a, "onInfoMainThread");
        m12410a(i);
        if (this.f9784f != null) {
            this.f9784f.mo8819b(i);
        }
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        bdn.m7747e(this.f9779a, ">> onPrepared");
        this.f9783e.set(State.PREPARED);
        if (this.f9784f != null) {
            this.f9781c.post(this.f9786h);
        }
        mo13082b();
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        bdn.m7747e(this.f9779a, "onVideoSizeChanged, width " + i + ", height " + i2);
        if (!m12415j()) {
            throw new RuntimeException("this should be called in Main Thread");
        } else if (this.f9784f != null) {
            this.f9784f.mo8814a(i, i2);
        }
    }

    public String toString() {
        return getClass().getSimpleName() + "@" + hashCode();
    }
}
