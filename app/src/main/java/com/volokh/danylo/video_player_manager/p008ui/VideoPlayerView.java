package com.volokh.danylo.video_player_manager.p008ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import com.volokh.danylo.video_player_manager.p008ui.MediaPlayerWrapper.C2248a;
import com.volokh.danylo.video_player_manager.p008ui.MediaPlayerWrapper.State;
import com.volokh.danylo.video_player_manager.p008ui.ScalableTextureView.ScaleType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.volokh.danylo.video_player_manager.ui.VideoPlayerView */
public class VideoPlayerView extends ScalableTextureView implements SurfaceTextureListener, C2248a {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f9821a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public MediaPlayerWrapper f9822b;

    /* renamed from: c */
    private bdm f9823c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C2256a f9824d;

    /* renamed from: e */
    private SurfaceTextureListener f9825e;

    /* renamed from: f */
    private AssetFileDescriptor f9826f;

    /* renamed from: g */
    private String f9827g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final bdl f9828h = new bdl();

    /* renamed from: i */
    private final Set<C2248a> f9829i = new HashSet();

    /* renamed from: j */
    private final Runnable f9830j = new Runnable() {
        public void run() {
            VideoPlayerView.this.f9824d.mo13155a(VideoPlayerView.this.getContentHeight().intValue(), VideoPlayerView.this.getContentWidth().intValue());
        }
    };

    /* renamed from: k */
    private final Runnable f9831k = new Runnable() {
        public void run() {
            VideoPlayerView.this.f9824d.mo13154a();
        }
    };

    /* renamed from: l */
    private final Runnable f9832l = new Runnable() {
        public void run() {
            bdn.m7747e(VideoPlayerView.this.f9821a, ">> run, onVideoSizeAvailable");
            synchronized (VideoPlayerView.this.f9828h) {
                bdn.m7747e(VideoPlayerView.this.f9821a, "onVideoSizeAvailable, mReadyForPlaybackIndicator " + VideoPlayerView.this.f9828h);
                VideoPlayerView.this.f9828h.mo8829a(VideoPlayerView.this.getContentHeight(), VideoPlayerView.this.getContentWidth());
                if (VideoPlayerView.this.f9828h.mo8834c()) {
                    bdn.m7747e(VideoPlayerView.this.f9821a, "run, onVideoSizeAvailable, notifyAll");
                    VideoPlayerView.this.f9828h.notifyAll();
                }
                bdn.m7747e(VideoPlayerView.this.f9821a, "<< run, onVideoSizeAvailable");
            }
            if (VideoPlayerView.this.f9824d != null) {
                VideoPlayerView.this.f9824d.mo13155a(VideoPlayerView.this.getContentHeight().intValue(), VideoPlayerView.this.getContentWidth().intValue());
            }
        }
    };

    /* renamed from: com.volokh.danylo.video_player_manager.ui.VideoPlayerView$a */
    public interface C2256a {
        /* renamed from: a */
        void mo13154a();

        /* renamed from: a */
        void mo13155a(int i, int i2);

        /* renamed from: b */
        void mo13156b(int i, int i2);
    }

    public VideoPlayerView(Context context) {
        super(context);
        m12452n();
    }

    public VideoPlayerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m12452n();
    }

    public VideoPlayerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m12452n();
    }

    @TargetApi(21)
    public VideoPlayerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m12452n();
    }

    /* renamed from: c */
    private void m12444c(int i) {
        ArrayList<C2248a> arrayList;
        bdn.m7747e(this.f9821a, "notifyOnInfo");
        synchronized (this.f9829i) {
            arrayList = new ArrayList<>(this.f9829i);
        }
        for (C2248a b : arrayList) {
            b.mo8819b(i);
        }
    }

    /* renamed from: c */
    private void m12445c(int i, int i2) {
        ArrayList<C2248a> arrayList;
        bdn.m7747e(this.f9821a, "notifyOnVideoSizeChangedMainThread, width " + i + ", height " + i2);
        synchronized (this.f9829i) {
            arrayList = new ArrayList<>(this.f9829i);
        }
        for (C2248a a : arrayList) {
            a.mo8814a(i, i2);
        }
    }

    /* renamed from: d */
    private void m12447d(int i) {
        switch (i) {
            case -1010:
                bdn.m7747e(this.f9821a, "error extra MEDIA_ERROR_UNSUPPORTED");
                return;
            case -1007:
                bdn.m7747e(this.f9821a, "error extra MEDIA_ERROR_MALFORMED");
                return;
            case -1004:
                bdn.m7747e(this.f9821a, "error extra MEDIA_ERROR_IO");
                return;
            case -110:
                bdn.m7747e(this.f9821a, "error extra MEDIA_ERROR_TIMED_OUT");
                return;
            default:
                return;
        }
    }

    /* renamed from: d */
    private void m12448d(int i, int i2) {
        ArrayList<C2248a> arrayList;
        bdn.m7747e(this.f9821a, "notifyOnErrorMainThread");
        synchronized (this.f9829i) {
            arrayList = new ArrayList<>(this.f9829i);
        }
        for (C2248a b : arrayList) {
            b.mo8820b(i, i2);
        }
    }

    /* renamed from: e */
    private static String m12449e(int i) {
        switch (i) {
            case 0:
                return "VISIBLE";
            case 4:
                return "INVISIBLE";
            case 8:
                return "GONE";
            default:
                throw new RuntimeException("unexpected");
        }
    }

    /* renamed from: l */
    private void m12450l() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new RuntimeException("cannot be in main thread");
        }
    }

    /* renamed from: m */
    private void m12451m() {
        ArrayList<C2248a> arrayList;
        bdn.m7747e(this.f9821a, "notifyOnVideoStopped");
        synchronized (this.f9829i) {
            arrayList = new ArrayList<>(this.f9829i);
        }
        for (C2248a c : arrayList) {
            c.mo8821c();
        }
    }

    /* renamed from: n */
    private void m12452n() {
        if (!isInEditMode()) {
            this.f9821a = "" + this;
            bdn.m7747e(this.f9821a, "initView");
            setScaleType(ScaleType.CENTER_CROP);
            super.setSurfaceTextureListener(this);
        }
    }

    /* renamed from: o */
    private void m12453o() {
        ArrayList<C2248a> arrayList;
        bdn.m7747e(this.f9821a, "notifyVideoCompletionMainThread");
        synchronized (this.f9829i) {
            arrayList = new ArrayList<>(this.f9829i);
        }
        for (C2248a b : arrayList) {
            b.mo8818b();
        }
    }

    /* renamed from: p */
    private void m12454p() {
        ArrayList<C2248a> arrayList;
        bdn.m7747e(this.f9821a, "notifyOnVideoPreparedMainThread");
        synchronized (this.f9829i) {
            arrayList = new ArrayList<>(this.f9829i);
        }
        for (C2248a a : arrayList) {
            a.mo8812a();
        }
    }

    /* renamed from: q */
    private void m12455q() {
        bdn.m7747e(this.f9821a, ">> onVideoSizeAvailable");
        mo13100d();
        if (isAttachedToWindow()) {
            this.f9823c.mo8837a(this.f9832l);
        }
        bdn.m7747e(this.f9821a, "<< onVideoSizeAvailable");
    }

    /* renamed from: r */
    private void m12456r() {
        bdn.m7747e(this.f9821a, ">> notifyTextureAvailable");
        this.f9823c.mo8837a((Runnable) new Runnable() {
            public void run() {
                bdn.m7747e(VideoPlayerView.this.f9821a, ">> run notifyTextureAvailable");
                synchronized (VideoPlayerView.this.f9828h) {
                    if (VideoPlayerView.this.f9822b != null) {
                        VideoPlayerView.this.f9822b.mo13078a(VideoPlayerView.this.getSurfaceTexture());
                    } else {
                        VideoPlayerView.this.f9828h.mo8829a(null, null);
                        bdn.m7747e(VideoPlayerView.this.f9821a, "mMediaPlayer null, cannot set surface texture");
                    }
                    VideoPlayerView.this.f9828h.mo8830a(true);
                    if (VideoPlayerView.this.f9828h.mo8834c()) {
                        bdn.m7747e(VideoPlayerView.this.f9821a, "notify ready for playback");
                        VideoPlayerView.this.f9828h.notifyAll();
                    }
                }
                bdn.m7747e(VideoPlayerView.this.f9821a, "<< run notifyTextureAvailable");
            }
        });
        bdn.m7747e(this.f9821a, "<< notifyTextureAvailable");
    }

    /* renamed from: a */
    public void mo8812a() {
        m12454p();
        if (this.f9824d != null) {
            this.f9823c.mo8837a(this.f9831k);
        }
    }

    /* renamed from: a */
    public void mo8813a(int i) {
    }

    /* renamed from: a */
    public void mo8814a(int i, int i2) {
        bdn.m7747e(this.f9821a, ">> onVideoSizeChangedMainThread, width " + i + ", height " + i2);
        if (i == 0 || i2 == 0) {
            bdn.m7744b(this.f9821a, "onVideoSizeChangedMainThread, size 0. Probably will be unable to start video");
            synchronized (this.f9828h) {
                this.f9828h.mo8832b(true);
                this.f9828h.notifyAll();
            }
        } else {
            setContentWidth(i);
            setContentHeight(i2);
            m12455q();
        }
        m12445c(i, i2);
        bdn.m7747e(this.f9821a, "<< onVideoSizeChangedMainThread, width " + i + ", height " + i2);
    }

    /* renamed from: a */
    public void mo13122a(C2248a aVar) {
        synchronized (this.f9829i) {
            this.f9829i.add(aVar);
        }
    }

    /* renamed from: b */
    public void mo8818b() {
        m12453o();
        if (this.f9824d != null) {
            this.f9823c.mo8837a(this.f9830j);
        }
    }

    /* renamed from: b */
    public void mo8819b(int i) {
        m12444c(i);
    }

    /* renamed from: b */
    public void mo8820b(final int i, final int i2) {
        bdn.m7747e(this.f9821a, "onErrorMainThread, this " + this);
        switch (i) {
            case 1:
                bdn.m7747e(this.f9821a, "onErrorMainThread, what MEDIA_ERROR_UNKNOWN");
                m12447d(i2);
                break;
            case 100:
                bdn.m7747e(this.f9821a, "onErrorMainThread, what MEDIA_ERROR_SERVER_DIED");
                m12447d(i2);
                break;
        }
        m12448d(i, i2);
        if (this.f9824d != null) {
            this.f9823c.mo8837a((Runnable) new Runnable() {
                public void run() {
                    VideoPlayerView.this.f9824d.mo13156b(i, i2);
                }
            });
        }
    }

    /* renamed from: c */
    public void mo8821c() {
        m12451m();
    }

    /* renamed from: e */
    public void mo13123e() {
        m12450l();
        synchronized (this.f9828h) {
            if (this.f9822b != null) {
                this.f9822b.mo13084d();
            }
        }
    }

    /* renamed from: f */
    public void mo13124f() {
        m12450l();
        synchronized (this.f9828h) {
            this.f9822b.mo13085e();
        }
    }

    /* renamed from: g */
    public void mo13125g() {
        bdn.m7747e(this.f9821a, ">> clearPlayerInstance");
        m12450l();
        synchronized (this.f9828h) {
            this.f9828h.mo8829a(null, null);
            if (this.f9822b != null) {
                this.f9822b.mo13086f();
                this.f9822b = null;
            }
        }
        bdn.m7747e(this.f9821a, "<< clearPlayerInstance");
    }

    public AssetFileDescriptor getAssetFileDescriptorDataSource() {
        return this.f9826f;
    }

    public State getCurrentState() {
        State i;
        synchronized (this.f9828h) {
            i = this.f9822b.mo13089i();
        }
        return i;
    }

    public int getDuration() {
        int h;
        synchronized (this.f9828h) {
            h = this.f9822b.mo13088h();
        }
        return h;
    }

    public MediaPlayerWrapper getMediaPlayer() {
        return this.f9822b;
    }

    public String getVideoUrlDataSource() {
        return this.f9827g;
    }

    /* renamed from: h */
    public void mo13131h() {
        bdn.m7747e(this.f9821a, ">> createNewPlayerInstance");
        bdn.m7747e(this.f9821a, "createNewPlayerInstance main Looper " + Looper.getMainLooper());
        bdn.m7747e(this.f9821a, "createNewPlayerInstance my Looper " + Looper.myLooper());
        m12450l();
        synchronized (this.f9828h) {
            this.f9822b = new bdk(getContext());
            this.f9828h.mo8829a(null, null);
            this.f9828h.mo8832b(false);
            if (this.f9828h.mo8833b()) {
                SurfaceTexture surfaceTexture = getSurfaceTexture();
                bdn.m7747e(this.f9821a, "texture " + surfaceTexture);
                this.f9822b.mo13078a(surfaceTexture);
            } else {
                bdn.m7747e(this.f9821a, "texture not available");
            }
            this.f9822b.mo13079a((C2248a) this);
        }
        bdn.m7747e(this.f9821a, "<< createNewPlayerInstance");
    }

    /* renamed from: i */
    public void mo13132i() {
        m12450l();
        synchronized (this.f9828h) {
            this.f9822b.mo13075a();
        }
    }

    public boolean isAttachedToWindow() {
        return this.f9823c != null;
    }

    /* renamed from: j */
    public void mo13134j() {
        m12450l();
        synchronized (this.f9828h) {
            this.f9822b.mo13083c();
        }
    }

    /* renamed from: k */
    public void mo13135k() {
        synchronized (this.f9828h) {
            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putBoolean("IS_VIDEO_MUTED", true).commit();
            if (this.f9822b != null) {
                this.f9822b.mo13076a(0.0f, 0.0f);
                this.f9822b.mo13081a(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        boolean isInEditMode = isInEditMode();
        bdn.m7747e(this.f9821a, ">> onAttachedToWindow " + isInEditMode);
        if (!isInEditMode) {
            this.f9823c = new bdm(this.f9821a, false);
            this.f9823c.mo8836a();
        }
        bdn.m7747e(this.f9821a, "<< onAttachedToWindow");
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        boolean isInEditMode = isInEditMode();
        bdn.m7747e(this.f9821a, ">> onDetachedFromWindow, isInEditMode " + isInEditMode);
        if (!isInEditMode) {
            this.f9823c.mo8838b();
            this.f9823c = null;
        }
        bdn.m7747e(this.f9821a, "<< onDetachedFromWindow");
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        bdn.m7747e(this.f9821a, "onSurfaceTextureAvailable, width " + i + ", height " + i2 + ", this " + this);
        if (this.f9825e != null) {
            this.f9825e.onSurfaceTextureAvailable(surfaceTexture, i, i2);
        }
        m12456r();
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        bdn.m7747e(this.f9821a, "onSurfaceTextureDestroyed, surface " + surfaceTexture);
        if (this.f9825e != null) {
            this.f9825e.onSurfaceTextureDestroyed(surfaceTexture);
        }
        if (isAttachedToWindow()) {
            this.f9823c.mo8837a((Runnable) new Runnable() {
                public void run() {
                    synchronized (VideoPlayerView.this.f9828h) {
                        VideoPlayerView.this.f9828h.mo8830a(false);
                        VideoPlayerView.this.f9828h.notifyAll();
                    }
                }
            });
        }
        surfaceTexture.release();
        return false;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        if (this.f9825e != null) {
            this.f9825e.onSurfaceTextureSizeChanged(surfaceTexture, i, i2);
        }
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        if (this.f9825e != null) {
            this.f9825e.onSurfaceTextureUpdated(surfaceTexture);
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        boolean isInEditMode = isInEditMode();
        bdn.m7747e(this.f9821a, ">> onVisibilityChanged " + m12449e(i) + ", isInEditMode " + isInEditMode);
        if (!isInEditMode) {
            switch (i) {
                case 4:
                case 8:
                    synchronized (this.f9828h) {
                        this.f9828h.notifyAll();
                    }
                    break;
            }
        }
        bdn.m7747e(this.f9821a, "<< onVisibilityChanged");
    }

    public void setBackgroundThreadMediaPlayerListener(C2256a aVar) {
        this.f9824d = aVar;
    }

    public void setDataSource(AssetFileDescriptor assetFileDescriptor) {
        m12450l();
        synchronized (this.f9828h) {
            bdn.m7747e(this.f9821a, "setDataSource, assetFileDescriptor " + assetFileDescriptor + ", this " + this);
            try {
                this.f9822b.mo13077a(assetFileDescriptor);
                this.f9826f = assetFileDescriptor;
            } catch (IOException e) {
                bdn.m7746d(this.f9821a, e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setDataSource(java.lang.String r6) {
        /*
            r5 = this;
            r5.m12450l()
            bdl r2 = r5.f9828h
            monitor-enter(r2)
            java.lang.String r1 = r5.f9821a     // Catch:{ all -> 0x003d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x003d }
            r3.<init>()     // Catch:{ all -> 0x003d }
            java.lang.String r4 = "setDataSource, path "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x003d }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x003d }
            java.lang.String r4 = ", this "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x003d }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ all -> 0x003d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x003d }
            p000.bdn.m7747e(r1, r3)     // Catch:{ all -> 0x003d }
            com.volokh.danylo.video_player_manager.ui.MediaPlayerWrapper r1 = r5.f9822b     // Catch:{ IOException -> 0x0031 }
            r1.mo13080a(r6)     // Catch:{ IOException -> 0x0031 }
            r5.f9827g = r6     // Catch:{ all -> 0x003d }
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
        L_0x0030:
            return
        L_0x0031:
            r0 = move-exception
            java.lang.String r1 = r5.f9821a     // Catch:{ all -> 0x003d }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x003d }
            p000.bdn.m7746d(r1, r3)     // Catch:{ all -> 0x003d }
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            goto L_0x0030
        L_0x003d:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.volokh.danylo.video_player_manager.p008ui.VideoPlayerView.setDataSource(java.lang.String):void");
    }

    public final void setSurfaceTextureListener(SurfaceTextureListener surfaceTextureListener) {
        this.f9825e = surfaceTextureListener;
    }

    public String toString() {
        return getClass().getSimpleName() + "@" + hashCode();
    }
}
