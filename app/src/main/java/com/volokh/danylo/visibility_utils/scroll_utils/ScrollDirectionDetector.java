package com.volokh.danylo.visibility_utils.scroll_utils;

public class ScrollDirectionDetector {

    /* renamed from: a */
    private static final String f9841a = ScrollDirectionDetector.class.getSimpleName();

    /* renamed from: b */
    private final C2257a f9842b;

    /* renamed from: c */
    private ScrollDirection f9843c = null;

    public enum ScrollDirection {
        UP,
        DOWN
    }

    /* renamed from: com.volokh.danylo.visibility_utils.scroll_utils.ScrollDirectionDetector$a */
    public interface C2257a {
    }

    public ScrollDirectionDetector(C2257a aVar) {
        this.f9842b = aVar;
    }
}
