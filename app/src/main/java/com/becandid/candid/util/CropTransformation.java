package com.becandid.candid.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.RectF;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;

public class CropTransformation implements Transformation<Bitmap> {

    /* renamed from: a */
    private BitmapPool f7157a;

    /* renamed from: b */
    private int f7158b;

    /* renamed from: c */
    private int f7159c;

    /* renamed from: d */
    private CropType f7160d;

    /* renamed from: e */
    private Context f7161e;

    public enum CropType {
        TOP,
        CENTER,
        BOTTOM
    }

    public CropTransformation(Context context, int i, int i2, CropType cropType) {
        this(Glide.m15039a(context).mo14632a(), i, i2, cropType);
        this.f7161e = context;
    }

    public CropTransformation(BitmapPool msVar, int i, int i2, CropType cropType) {
        this.f7160d = CropType.CENTER;
        this.f7157a = msVar;
        this.f7158b = i;
        this.f7159c = i2;
        this.f7160d = cropType;
    }

    /* renamed from: a */
    private float m9695a(float f) {
        switch (this.f7160d) {
            case CENTER:
                return (((float) this.f7159c) - f) / 2.0f;
            case BOTTOM:
                return ((float) this.f7159c) - f;
            default:
                return 0.0f;
        }
    }

    /* renamed from: a */
    public String mo10701a() {
        return "CropTransformation(width=" + this.f7158b + ", height=" + this.f7159c + ", cropType=" + this.f7160d + ")";
    }

    /* renamed from: a */
    public Resource<Bitmap> mo10702a(Resource<Bitmap> moVar, int i, int i2) {
        Bitmap bitmap = (Bitmap) moVar.mo14377d();
        this.f7158b = this.f7158b == 0 ? bitmap.getWidth() : this.f7158b;
        this.f7159c = this.f7159c == 0 ? bitmap.getHeight() : this.f7159c;
        Config config = bitmap.getConfig() != null ? bitmap.getConfig() : Config.ARGB_8888;
        Bitmap a = this.f7157a.mo14803a(this.f7158b, this.f7159c, config);
        if (a == null) {
            a = Bitmap.createBitmap(this.f7158b, this.f7159c, config);
        }
        float max = Math.max(((float) this.f7158b) / ((float) bitmap.getWidth()), ((float) this.f7159c) / ((float) bitmap.getHeight()));
        float width = max * ((float) bitmap.getWidth());
        float height = max * ((float) bitmap.getHeight());
        float f = (((float) this.f7158b) - width) / 2.0f;
        float a2 = m9695a(height);
        RectF rectF = new RectF(f, a2, f + width, a2 + height);
        new Canvas(a).drawBitmap(bitmap, null, rectF, null);
        return new RoundedCornersTransformation(this.f7161e, (int) (10.0f * this.f7161e.getResources().getDisplayMetrics().density), 0, CornerType.TOP).mo10703a(a, i, i2);
    }
}
