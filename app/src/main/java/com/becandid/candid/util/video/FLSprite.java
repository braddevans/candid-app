package com.becandid.candid.util.video;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class FLSprite {

    /* renamed from: a */
    final float[] f7202a = {-0.15f, 0.15f, 0.0f, -0.15f, -0.15f, 0.0f, 0.15f, 0.15f, 0.0f, -0.15f, -0.15f, 0.0f, 0.15f, -0.15f, 0.0f, 0.15f, 0.15f, 0.0f};

    /* renamed from: b */
    final float[] f7203b = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f};

    /* renamed from: c */
    final float[] f7204c = {-1.0f, 1.5f, 0.0f, -1.0f, -1.5f, 0.0f, 1.0f, 1.5f, 0.0f, -1.0f, -1.5f, 0.0f, 1.0f, -1.5f, 0.0f, 1.0f, 1.5f, 0.0f};

    /* renamed from: d */
    private int f7205d = -1;

    /* renamed from: e */
    private float[] f7206e = new float[16];

    /* renamed from: f */
    private float[] f7207f = new float[16];

    /* renamed from: g */
    private float[] f7208g = new float[16];

    /* renamed from: h */
    private FloatBuffer f7209h;

    /* renamed from: i */
    private FloatBuffer f7210i;

    /* renamed from: j */
    private FloatBuffer f7211j;

    /* renamed from: k */
    private int f7212k;

    /* renamed from: l */
    private int f7213l;

    /* renamed from: m */
    private int f7214m;

    /* renamed from: n */
    private int f7215n;

    /* renamed from: o */
    private int f7216o;

    /* renamed from: p */
    private int f7217p;

    /* renamed from: q */
    private final int f7218q = 4;

    /* renamed from: r */
    private final int f7219r = 12;

    /* renamed from: s */
    private final int f7220s = 0;

    /* renamed from: t */
    private final int f7221t = 3;

    /* renamed from: u */
    private final int f7222u = 3;

    /* renamed from: v */
    private final int f7223v = 4;

    /* renamed from: w */
    private float[] f7224w = new float[16];

    /* renamed from: x */
    private SpecialEffect f7225x = SpecialEffect.NONE;

    /* renamed from: y */
    private int f7226y = 0;

    public enum SpecialEffect {
        NONE("NONE"),
        FLASH("FLASHING"),
        ROTATE("ROTATING"),
        FULL("FULLSCREEN");
        

        /* renamed from: e */
        private final String f7233e;

        private SpecialEffect(String str) {
            this.f7233e = str;
        }
    }

    public FLSprite(int i, SpecialEffect specialEffect) {
        if (i > -1) {
            this.f7205d = i;
        }
        if (specialEffect != null) {
            this.f7225x = specialEffect;
        }
    }

    /* renamed from: a */
    public static int m9728a(Bitmap bitmap) {
        int[] iArr = new int[1];
        GLES20.glGenTextures(1, iArr, 0);
        m9729a("loadTextureFromBitmap after gen texture");
        if (iArr[0] != 0) {
            GLES20.glBindTexture(3553, iArr[0]);
            m9729a("loadTextureFromBitmap after gen bind to 2D");
            GLES20.glTexParameteri(3553, 10241, 9728);
            GLES20.glTexParameteri(3553, 10240, 9728);
            GLUtils.texImage2D(3553, 0, bitmap, 0);
            m9729a("loadTextureFromBitmap after texImage2D");
        }
        if (iArr[0] != 0) {
            return iArr[0];
        }
        throw new RuntimeException("Error loading texture.");
    }

    /* renamed from: a */
    public static void m9729a(String str) {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            throw new RuntimeException(str + ": glError 0x" + Integer.toHexString(glGetError));
        }
    }

    /* renamed from: a */
    private void m9730a(FloatBuffer floatBuffer, float[] fArr) {
        floatBuffer.position(0);
        GLES20.glVertexAttribPointer(this.f7216o, 3, 5126, false, 12, floatBuffer);
        GLES20.glEnableVertexAttribArray(this.f7216o);
        Matrix.multiplyMM(this.f7208g, 0, this.f7207f, 0, this.f7206e, 0);
        GLES20.glUniformMatrix4fv(this.f7214m, 1, false, this.f7208g, 0);
        Matrix.multiplyMM(this.f7224w, 0, fArr, 0, this.f7208g, 0);
        System.arraycopy(this.f7224w, 0, this.f7208g, 0, 16);
        GLES20.glUniformMatrix4fv(this.f7213l, 1, false, this.f7208g, 0);
        GLES20.glDrawArrays(4, 0, 6);
    }

    /* renamed from: b */
    private String m9731b() {
        return "precision mediump float;\nuniform sampler2D u_Texture;\nvarying vec3 v_Position;\nvarying vec2 v_TexCoordinate;\nvoid main() {\n    gl_FragColor = texture2D(u_Texture, v_TexCoordinate);\n}";
    }

    /* renamed from: b */
    private void m9732b(float[] fArr, int i) {
        if ((i & 8) == 0) {
            m9737e(fArr, i);
        }
    }

    /* renamed from: c */
    private String m9733c() {
        return "uniform mat4 u_MVPMatrix;\nuniform mat4 u_MVMatrix;\nattribute vec4 a_Position;\nattribute vec2 a_TexCoordinate;\nvarying vec3 v_Position;\nvarying vec2 v_TexCoordinate;\nvoid main() {\nv_Position = vec3(u_MVMatrix * a_Position);\nv_TexCoordinate = a_TexCoordinate;\ngl_Position = u_MVPMatrix * a_Position;\n}";
    }

    /* renamed from: c */
    private void m9734c(float[] fArr, int i) {
        GLES20.glUseProgram(this.f7212k);
        float uptimeMillis = 0.036f * ((float) ((int) (SystemClock.uptimeMillis() % 10000)));
        Matrix.setIdentityM(this.f7206e, 0);
        Matrix.translateM(this.f7206e, 0, 0.6f, -0.5f + (0.35f * ((float) this.f7226y)), 0.0f);
        Matrix.rotateM(this.f7206e, 0, uptimeMillis, 0.0f, 0.0f, 1.0f);
        m9735d();
        m9730a(this.f7209h, fArr);
        GLES20.glBindTexture(3553, 0);
        GLES20.glUseProgram(0);
    }

    /* renamed from: d */
    private void m9735d() {
        m9729a("drawCustomSpriteNoEffect3");
        GLES20.glActiveTexture(33984);
        m9729a("drawCustomSpriteNoEffect4");
        GLES20.glBindTexture(3553, this.f7205d);
        m9729a("drawCustomSpriteNoEffect5");
        GLES20.glUniform1i(this.f7205d, 0);
        m9729a("drawCustomSpriteNoEffect6");
        this.f7210i.position(0);
        GLES20.glVertexAttribPointer(this.f7217p, 2, 5126, false, 0, this.f7210i);
        GLES20.glEnableVertexAttribArray(this.f7217p);
        m9729a("drawCustomSpriteNoEffect7");
    }

    /* renamed from: d */
    private void m9736d(float[] fArr, int i) {
        GLES20.glUseProgram(this.f7212k);
        Matrix.setIdentityM(this.f7206e, 0);
        m9735d();
        m9730a(this.f7211j, fArr);
        GLES20.glDisableVertexAttribArray(this.f7216o);
        GLES20.glDisableVertexAttribArray(this.f7217p);
        GLES20.glBindTexture(3553, 0);
        GLES20.glUseProgram(0);
    }

    /* renamed from: e */
    private void m9737e(float[] fArr, int i) {
        m9729a("drawCustomSpriteNoEffect1");
        GLES20.glUseProgram(this.f7212k);
        m9729a("drawCustomSpriteNoEffect2");
        Matrix.setIdentityM(this.f7206e, 0);
        Matrix.translateM(this.f7206e, 0, 0.6f, -0.5f + (0.15f * ((float) this.f7226y)), 0.0f);
        m9735d();
        m9730a(this.f7209h, fArr);
        m9729a("drawCustomSpriteNoEffect8");
        GLES20.glDisableVertexAttribArray(this.f7216o);
        GLES20.glDisableVertexAttribArray(this.f7217p);
        GLES20.glBindTexture(3553, 0);
        GLES20.glUseProgram(0);
        m9729a("drawCustomSpriteNoEffect9");
    }

    /* renamed from: a */
    public void mo10713a() {
        GLES20.glEnable(3042);
        GLES20.glBlendFunc(1, 771);
        this.f7209h = ByteBuffer.allocateDirect(this.f7202a.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        this.f7209h.put(this.f7202a).position(0);
        this.f7210i = ByteBuffer.allocateDirect(this.f7203b.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        this.f7210i.put(this.f7203b).position(0);
        this.f7211j = ByteBuffer.allocateDirect(this.f7204c.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        this.f7211j.put(this.f7204c).position(0);
        Matrix.setLookAtM(this.f7207f, 0, 0.0f, 0.0f, 1.5f, 0.0f, 0.0f, -5.0f, 0.0f, 1.0f, 0.0f);
        String c = m9733c();
        String b = m9731b();
        int glCreateShader = GLES20.glCreateShader(35633);
        if (glCreateShader != 0) {
            GLES20.glShaderSource(glCreateShader, c);
            GLES20.glCompileShader(glCreateShader);
            int[] iArr = new int[1];
            GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
            if (iArr[0] == 0) {
                GLES20.glDeleteShader(glCreateShader);
                glCreateShader = 0;
            }
        }
        if (glCreateShader == 0) {
            throw new RuntimeException("Error creating vertex shader.");
        }
        int glCreateShader2 = GLES20.glCreateShader(35632);
        if (glCreateShader2 != 0) {
            GLES20.glShaderSource(glCreateShader2, b);
            GLES20.glCompileShader(glCreateShader2);
            int[] iArr2 = new int[1];
            GLES20.glGetShaderiv(glCreateShader2, 35713, iArr2, 0);
            if (iArr2[0] == 0) {
                GLES20.glDeleteShader(glCreateShader2);
                glCreateShader2 = 0;
            }
        }
        if (glCreateShader2 == 0) {
            throw new RuntimeException("Error creating fragment shader.");
        }
        int glCreateProgram = GLES20.glCreateProgram();
        if (glCreateProgram != 0) {
            GLES20.glAttachShader(glCreateProgram, glCreateShader);
            GLES20.glAttachShader(glCreateProgram, glCreateShader2);
            GLES20.glBindAttribLocation(glCreateProgram, 0, "a_Position");
            GLES20.glBindAttribLocation(glCreateProgram, 1, "a_TexCoordinate");
            GLES20.glLinkProgram(glCreateProgram);
            int[] iArr3 = new int[1];
            GLES20.glGetProgramiv(glCreateProgram, 35714, iArr3, 0);
            if (iArr3[0] == 0) {
                GLES20.glDeleteProgram(glCreateProgram);
                glCreateProgram = 0;
            }
        }
        if (glCreateProgram == 0) {
            throw new RuntimeException("Error creating program.");
        }
        this.f7213l = GLES20.glGetUniformLocation(glCreateProgram, "u_MVPMatrix");
        this.f7216o = GLES20.glGetAttribLocation(glCreateProgram, "a_Position");
        this.f7217p = GLES20.glGetAttribLocation(glCreateProgram, "a_TexCoordinate");
        this.f7214m = GLES20.glGetUniformLocation(glCreateProgram, "u_MVMatrix");
        this.f7215n = GLES20.glGetUniformLocation(glCreateProgram, "u_Texture");
        this.f7212k = glCreateProgram;
    }

    /* renamed from: a */
    public void mo10714a(float[] fArr, int i) {
        switch (this.f7225x) {
            case FLASH:
                m9732b(fArr, i);
                return;
            case ROTATE:
                m9734c(fArr, i);
                return;
            case FULL:
                m9736d(fArr, i);
                return;
            default:
                m9737e(fArr, i);
                return;
        }
    }
}
