package com.becandid.candid.util.video;

import android.annotation.TargetApi;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.os.Build.VERSION;
import android.util.Log;

@TargetApi(18)
public final class EglBase {

    /* renamed from: a */
    private static final int f7189a = VERSION.SDK_INT;

    /* renamed from: b */
    private EGLContext f7190b;

    /* renamed from: c */
    private ConfigType f7191c;

    /* renamed from: d */
    private EGLConfig f7192d;

    /* renamed from: e */
    private EGLDisplay f7193e;

    /* renamed from: f */
    private EGLSurface f7194f;

    /* renamed from: g */
    private int f7195g;

    /* renamed from: h */
    private int f7196h;

    public enum ConfigType {
        PLAIN,
        PIXEL_BUFFER,
        RECORDABLE
    }

    public EglBase() {
        this(EGL14.EGL_NO_CONTEXT, ConfigType.PLAIN);
    }

    public EglBase(EGLContext eGLContext, ConfigType configType) {
        this.f7194f = EGL14.EGL_NO_SURFACE;
        this.f7191c = configType;
        this.f7193e = m9719h();
        this.f7192d = m9716a(this.f7193e, configType);
        this.f7190b = m9717a(eGLContext, this.f7193e, this.f7192d);
    }

    /* renamed from: a */
    private static EGLConfig m9716a(EGLDisplay eGLDisplay, ConfigType configType) {
        int[] iArr = {12324, 8, 12323, 8, 12322, 8, 12352, 4, 12344, 0, 12344};
        switch (configType) {
            case PLAIN:
                break;
            case PIXEL_BUFFER:
                iArr[iArr.length - 3] = 12339;
                iArr[iArr.length - 2] = 1;
                break;
            case RECORDABLE:
                iArr[iArr.length - 3] = 12610;
                iArr[iArr.length - 2] = 1;
                break;
            default:
                throw new IllegalArgumentException();
        }
        EGLConfig[] eGLConfigArr = new EGLConfig[1];
        if (EGL14.eglChooseConfig(eGLDisplay, iArr, 0, eGLConfigArr, 0, eGLConfigArr.length, new int[1], 0)) {
            return eGLConfigArr[0];
        }
        throw new RuntimeException("Unable to find RGB888 " + configType + " EGL config");
    }

    /* renamed from: a */
    private static EGLContext m9717a(EGLContext eGLContext, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
        EGLContext eglCreateContext = EGL14.eglCreateContext(eGLDisplay, eGLConfig, eGLContext, new int[]{12440, 2, 12344}, 0);
        if (eglCreateContext != EGL14.EGL_NO_CONTEXT) {
            return eglCreateContext;
        }
        throw new RuntimeException("Failed to create EGL context");
    }

    /* renamed from: g */
    private void m9718g() {
        if (this.f7193e == EGL14.EGL_NO_DISPLAY || this.f7190b == EGL14.EGL_NO_CONTEXT || this.f7192d == null) {
            throw new RuntimeException("This object has been released");
        }
    }

    /* renamed from: h */
    private static EGLDisplay m9719h() {
        EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
        if (eglGetDisplay == EGL14.EGL_NO_DISPLAY) {
            throw new RuntimeException("Unable to get EGL14 display");
        }
        int[] iArr = new int[2];
        if (EGL14.eglInitialize(eglGetDisplay, iArr, 0, iArr, 1)) {
            return eglGetDisplay;
        }
        throw new RuntimeException("Unable to initialize EGL14");
    }

    /* renamed from: a */
    public int mo10705a() {
        return this.f7195g;
    }

    /* renamed from: a */
    public int mo10706a(EGLSurface eGLSurface, int i) {
        int[] iArr = new int[1];
        EGL14.eglQuerySurface(this.f7193e, eGLSurface, i, iArr, 0);
        return iArr[0];
    }

    /* renamed from: a */
    public void mo10707a(Object obj) {
        m9718g();
        if (this.f7191c == ConfigType.PIXEL_BUFFER) {
            Log.w("EglBase", "This EGL context is configured for PIXEL_BUFFER, but uses regular Surface");
        }
        if (this.f7194f != EGL14.EGL_NO_SURFACE) {
            throw new RuntimeException("Already has an EGLSurface");
        }
        this.f7194f = EGL14.eglCreateWindowSurface(this.f7193e, this.f7192d, obj, new int[]{12344}, 0);
        if (this.f7194f == EGL14.EGL_NO_SURFACE) {
            throw new RuntimeException("Failed to create window surface");
        }
        this.f7195g = mo10706a(this.f7194f, 12375);
        this.f7196h = mo10706a(this.f7194f, 12374);
    }

    /* renamed from: b */
    public int mo10708b() {
        return this.f7196h;
    }

    /* renamed from: c */
    public void mo10709c() {
        if (this.f7194f != EGL14.EGL_NO_SURFACE) {
            EGL14.eglDestroySurface(this.f7193e, this.f7194f);
            this.f7194f = EGL14.EGL_NO_SURFACE;
        }
    }

    /* renamed from: d */
    public void mo10710d() {
        m9718g();
        mo10709c();
        EGL14.eglMakeCurrent(this.f7193e, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
        EGL14.eglDestroyContext(this.f7193e, this.f7190b);
        EGL14.eglReleaseThread();
        EGL14.eglTerminate(this.f7193e);
        this.f7190b = EGL14.EGL_NO_CONTEXT;
        this.f7193e = EGL14.EGL_NO_DISPLAY;
        this.f7192d = null;
    }

    /* renamed from: e */
    public void mo10711e() {
        m9718g();
        if (this.f7194f == EGL14.EGL_NO_SURFACE) {
            throw new RuntimeException("No EGLSurface - can't make current");
        } else if (!EGL14.eglMakeCurrent(this.f7193e, this.f7194f, this.f7194f, this.f7190b)) {
            throw new RuntimeException("eglMakeCurrent failed");
        }
    }

    /* renamed from: f */
    public void mo10712f() {
        m9718g();
        if (this.f7194f == EGL14.EGL_NO_SURFACE) {
            throw new RuntimeException("No EGLSurface - can't swap buffers");
        }
        EGL14.eglSwapBuffers(this.f7193e, this.f7194f);
    }
}
