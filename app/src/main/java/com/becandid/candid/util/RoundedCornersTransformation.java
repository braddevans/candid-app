package com.becandid.candid.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.util.Log;

public class RoundedCornersTransformation implements Transformation<Bitmap> {

    /* renamed from: a */
    private BitmapPool f7167a;

    /* renamed from: b */
    private int f7168b;

    /* renamed from: c */
    private int f7169c;

    /* renamed from: d */
    private int f7170d;

    /* renamed from: e */
    private CornerType f7171e;

    public enum CornerType {
        ALL,
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        TOP,
        BOTTOM,
        LEFT,
        RIGHT,
        OTHER_TOP_LEFT,
        OTHER_TOP_RIGHT,
        OTHER_BOTTOM_LEFT,
        OTHER_BOTTOM_RIGHT,
        DIAGONAL_FROM_TOP_LEFT,
        DIAGONAL_FROM_TOP_RIGHT
    }

    public RoundedCornersTransformation(Context context, int i, int i2) {
        this(context, i, i2, CornerType.ALL);
    }

    public RoundedCornersTransformation(Context context, int i, int i2, CornerType cornerType) {
        this(Glide.m15039a(context).mo14632a(), i, i2, cornerType);
    }

    public RoundedCornersTransformation(BitmapPool msVar, int i, int i2, CornerType cornerType) {
        this.f7167a = msVar;
        this.f7168b = i;
        this.f7169c = this.f7168b * 2;
        this.f7170d = i2;
        this.f7171e = cornerType;
    }

    /* renamed from: a */
    private void m9698a(Canvas canvas, Paint paint, float f, float f2) {
        Log.d("RCT", "Draw round bitmap with size: " + f + "x" + f2);
        float f3 = f - ((float) this.f7170d);
        float f4 = f2 - ((float) this.f7170d);
        switch (this.f7171e) {
            case ALL:
                canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, f3, f4), (float) this.f7168b, (float) this.f7168b, paint);
                return;
            case TOP_LEFT:
                m9699b(canvas, paint, f3, f4);
                return;
            case TOP_RIGHT:
                m9700c(canvas, paint, f3, f4);
                return;
            case BOTTOM_LEFT:
                m9701d(canvas, paint, f3, f4);
                return;
            case BOTTOM_RIGHT:
                m9702e(canvas, paint, f3, f4);
                return;
            case TOP:
                m9703f(canvas, paint, f3, f4);
                return;
            case BOTTOM:
                m9704g(canvas, paint, f3, f4);
                return;
            case LEFT:
                m9705h(canvas, paint, f3, f4);
                return;
            case RIGHT:
                m9706i(canvas, paint, f3, f4);
                return;
            case OTHER_TOP_LEFT:
                m9707j(canvas, paint, f3, f4);
                return;
            case OTHER_TOP_RIGHT:
                m9708k(canvas, paint, f3, f4);
                return;
            case OTHER_BOTTOM_LEFT:
                m9709l(canvas, paint, f3, f4);
                return;
            case OTHER_BOTTOM_RIGHT:
                m9710m(canvas, paint, f3, f4);
                return;
            case DIAGONAL_FROM_TOP_LEFT:
                m9711n(canvas, paint, f3, f4);
                return;
            case DIAGONAL_FROM_TOP_RIGHT:
                m9712o(canvas, paint, f3, f4);
                return;
            default:
                canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, f3, f4), (float) this.f7168b, (float) this.f7168b, paint);
                return;
        }
    }

    /* renamed from: b */
    private void m9699b(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, (float) (this.f7170d + this.f7169c), (float) (this.f7170d + this.f7169c)), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) (this.f7170d + this.f7168b), (float) (this.f7170d + this.f7168b), f2), paint);
        canvas.drawRect(new RectF((float) (this.f7170d + this.f7168b), (float) this.f7170d, f, f2), paint);
    }

    /* renamed from: c */
    private void m9700c(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF(f - ((float) this.f7169c), (float) this.f7170d, f, (float) (this.f7170d + this.f7169c)), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) this.f7170d, f - ((float) this.f7168b), f2), paint);
        canvas.drawRect(new RectF(f - ((float) this.f7168b), (float) (this.f7170d + this.f7168b), f, f2), paint);
    }

    /* renamed from: d */
    private void m9701d(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, f2 - ((float) this.f7169c), (float) (this.f7170d + this.f7169c), f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) this.f7170d, (float) (this.f7170d + this.f7169c), f2 - ((float) this.f7168b)), paint);
        canvas.drawRect(new RectF((float) (this.f7170d + this.f7168b), (float) this.f7170d, f, f2), paint);
    }

    /* renamed from: e */
    private void m9702e(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF(f - ((float) this.f7169c), f2 - ((float) this.f7169c), f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) this.f7170d, f - ((float) this.f7168b), f2), paint);
        canvas.drawRect(new RectF(f - ((float) this.f7168b), (float) this.f7170d, f, f2 - ((float) this.f7168b)), paint);
    }

    /* renamed from: f */
    private void m9703f(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, f, (float) (this.f7170d + this.f7169c)), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) (this.f7170d + this.f7168b), f, f2), paint);
    }

    /* renamed from: g */
    private void m9704g(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, f2 - ((float) this.f7169c), f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) this.f7170d, f, f2 - ((float) this.f7168b)), paint);
    }

    /* renamed from: h */
    private void m9705h(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, (float) (this.f7170d + this.f7169c), f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) (this.f7170d + this.f7168b), (float) this.f7170d, f, f2), paint);
    }

    /* renamed from: i */
    private void m9706i(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF(f - ((float) this.f7169c), (float) this.f7170d, f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) this.f7170d, f - ((float) this.f7168b), f2), paint);
    }

    /* renamed from: j */
    private void m9707j(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, f2 - ((float) this.f7169c), f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRoundRect(new RectF(f - ((float) this.f7169c), (float) this.f7170d, f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) this.f7170d, f - ((float) this.f7168b), f2 - ((float) this.f7168b)), paint);
    }

    /* renamed from: k */
    private void m9708k(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, (float) (this.f7170d + this.f7169c), f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRoundRect(new RectF((float) this.f7170d, f2 - ((float) this.f7169c), f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) (this.f7170d + this.f7168b), (float) this.f7170d, f, f2 - ((float) this.f7168b)), paint);
    }

    /* renamed from: l */
    private void m9709l(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, f, (float) (this.f7170d + this.f7169c)), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRoundRect(new RectF(f - ((float) this.f7169c), (float) this.f7170d, f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) (this.f7170d + this.f7168b), f - ((float) this.f7168b), f2), paint);
    }

    /* renamed from: m */
    private void m9710m(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, f, (float) (this.f7170d + this.f7169c)), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, (float) (this.f7170d + this.f7169c), f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) (this.f7170d + this.f7168b), (float) (this.f7170d + this.f7168b), f, f2), paint);
    }

    /* renamed from: n */
    private void m9711n(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF((float) this.f7170d, (float) this.f7170d, (float) (this.f7170d + this.f7169c), (float) (this.f7170d + this.f7169c)), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRoundRect(new RectF(f - ((float) this.f7169c), f2 - ((float) this.f7169c), f, f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) (this.f7170d + this.f7168b), f - ((float) this.f7169c), f2), paint);
        canvas.drawRect(new RectF((float) (this.f7170d + this.f7169c), (float) this.f7170d, f, f2 - ((float) this.f7168b)), paint);
    }

    /* renamed from: o */
    private void m9712o(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRoundRect(new RectF(f - ((float) this.f7169c), (float) this.f7170d, f, (float) (this.f7170d + this.f7169c)), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRoundRect(new RectF((float) this.f7170d, f2 - ((float) this.f7169c), (float) (this.f7170d + this.f7169c), f2), (float) this.f7168b, (float) this.f7168b, paint);
        canvas.drawRect(new RectF((float) this.f7170d, (float) this.f7170d, f - ((float) this.f7168b), f2 - ((float) this.f7168b)), paint);
        canvas.drawRect(new RectF((float) (this.f7170d + this.f7168b), (float) (this.f7170d + this.f7168b), f, f2), paint);
    }

    /* renamed from: a */
    public String mo10701a() {
        return "RoundedTransformation(radius=" + this.f7168b + ", margin=" + this.f7170d + ", diameter=" + this.f7169c + ", cornerType=" + this.f7171e.name() + ")";
    }

    /* renamed from: a */
    public Resource<Bitmap> mo10703a(Bitmap bitmap, int i, int i2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap a = this.f7167a.mo14803a(i, i2, Config.ARGB_8888);
        if (a != null) {
            a.eraseColor(0);
        } else if (i2 > 0 && i > 0) {
            a = Bitmap.createBitmap(i, i2, Config.ARGB_8888);
        } else if (width > 0 && height > 0) {
            a = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(a);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        BitmapShader bitmapShader = new BitmapShader(bitmap, TileMode.CLAMP, TileMode.CLAMP);
        Matrix matrix = new Matrix();
        float f = ((float) i) / ((float) width);
        float f2 = ((float) i2) / ((float) height);
        float max = Math.max(f, f2);
        matrix.setScale(max, max);
        matrix.postTranslate(f > f2 ? 0.0f : (((float) i) - (((float) width) * f2)) * 0.5f, 0.0f);
        bitmapShader.setLocalMatrix(matrix);
        paint.setShader(bitmapShader);
        m9698a(canvas, paint, (float) i, (float) i2);
        return BitmapResource.m15713a(a, this.f7167a);
    }

    /* renamed from: a */
    public Resource<Bitmap> mo10702a(Resource<Bitmap> moVar, int i, int i2) {
        return mo10703a((Bitmap) moVar.mo14377d(), i, i2);
    }
}
