package com.becandid.candid.util.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.opengl.EGL14;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import com.becandid.candid.util.video.FLSprite.SpecialEffect;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

@TargetApi(18)
public final class VideoGLView extends GLSurfaceView {

    /* renamed from: a */
    public static List<Integer> f7234a;

    /* renamed from: b */
    public static List<Integer> f7235b;

    /* renamed from: c */
    public static C1780b f7236c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static int f7237d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static int f7238e = 360;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static int f7239f = 640;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final C1781c f7240g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Bitmap f7241h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public SpecialEffect f7242i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public boolean f7243j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public C1779a f7244k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public int f7245l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public int f7246m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public int f7247n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public int f7248o;

    /* renamed from: com.becandid.candid.util.video.VideoGLView$a */
    static final class C1779a extends Handler {

        /* renamed from: a */
        private C1782d f7255a;

        public C1779a(C1782d dVar) {
            this.f7255a = dVar;
        }

        /* renamed from: a */
        public void mo10734a() {
            synchronized (this) {
                sendEmptyMessage(3);
            }
        }

        /* renamed from: a */
        public void mo10735a(int i, int i2) {
            sendMessage(obtainMessage(1, i, i2));
        }

        /* renamed from: a */
        public void mo10736a(boolean z) {
            synchronized (this) {
                sendEmptyMessage(2);
                if (z && this.f7255a.f7273d) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    this.f7255a.m9772a(message.arg1, message.arg2);
                    return;
                case 2:
                    this.f7255a.m9776b();
                    synchronized (this) {
                        notifyAll();
                    }
                    Looper.myLooper().quit();
                    this.f7255a = null;
                    return;
                case 3:
                    this.f7255a.m9778c();
                    return;
                default:
                    throw new RuntimeException("unknown message:what=" + message.what);
            }
        }
    }

    /* renamed from: com.becandid.candid.util.video.VideoGLView$b */
    public interface C1780b {
        /* renamed from: a */
        void mo10463a();
    }

    /* renamed from: com.becandid.candid.util.video.VideoGLView$c */
    static final class C1781c implements OnFrameAvailableListener, Renderer {

        /* renamed from: a */
        private final WeakReference<VideoGLView> f7256a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public SurfaceTexture f7257b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public int f7258c;

        /* renamed from: d */
        private GLDrawer2D f7259d;

        /* renamed from: e */
        private FLSprite f7260e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public int f7261f = -1;

        /* renamed from: g */
        private Bitmap f7262g;
        /* access modifiers changed from: private */

        /* renamed from: h */
        public SpecialEffect f7263h;

        /* renamed from: i */
        private final float[] f7264i = new float[16];

        /* renamed from: j */
        private final float[] f7265j = new float[16];

        /* renamed from: k */
        private final float[] f7266k = new float[16];

        /* renamed from: l */
        private MediaVideoEncoder f7267l;

        /* renamed from: m */
        private volatile boolean f7268m = false;

        /* renamed from: n */
        private boolean f7269n = true;

        public C1781c(VideoGLView videoGLView) {
            this.f7256a = new WeakReference<>(videoGLView);
            Matrix.setIdentityM(this.f7266k, 0);
        }

        /* renamed from: a */
        public static void m9761a(String str) {
            int glGetError = GLES20.glGetError();
            if (glGetError != 0) {
                String str2 = str + ": glError 0x" + Integer.toHexString(glGetError);
                Log.d("VideoGLView", str2);
                throw new RuntimeException(str2);
            }
        }

        /* renamed from: b */
        private void m9763b() {
            m9761a("initMovableSprites start");
            if (this.f7262g != null) {
                GLES20.glBindTexture(3553, 0);
                m9761a("initMovableSprites after clear bind");
                this.f7261f = FLSprite.m9728a(this.f7262g);
                this.f7260e = new FLSprite(this.f7261f, this.f7263h);
                this.f7260e.mo10713a();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public final void m9765c() {
            int i;
            int i2;
            int i3;
            int i4;
            VideoGLView videoGLView = (VideoGLView) this.f7256a.get();
            if (videoGLView != null) {
                int width = videoGLView.getWidth();
                int height = videoGLView.getHeight();
                GLES20.glViewport(0, 0, width, height);
                GLES20.glClear(16384);
                double d = (double) videoGLView.f7245l;
                double e = (double) videoGLView.f7246m;
                if (d != 0.0d && e != 0.0d) {
                    Matrix.setIdentityM(this.f7266k, 0);
                    double d2 = ((double) width) / ((double) height);
                    Log.d("VideoGLView", String.format("view(%d,%d)%f,video(%1.0f,%1.0f)", new Object[]{Integer.valueOf(width), Integer.valueOf(height), Double.valueOf(d2), Double.valueOf(d), Double.valueOf(e)}));
                    switch (videoGLView.f7248o) {
                        case 1:
                            double d3 = d / e;
                            if (d2 > d3) {
                                i4 = 0;
                                i3 = height;
                                i2 = (int) (((double) height) * d3);
                                i = (width - i2) / 2;
                            } else {
                                i = 0;
                                i2 = width;
                                i3 = (int) (((double) width) / d3);
                                i4 = (height - i3) / 2;
                            }
                            GLES20.glViewport(i, i4, i2, i3);
                            break;
                        case 2:
                            Log.d("VideoGLView", String.format("view_width/height(%1.0f, %1.0f), video_width/height(%1.0f, %1.0f)", new Object[]{Float.valueOf((float) width), Float.valueOf((float) height), Float.valueOf((float) d), Float.valueOf((float) e)}));
                            float f = ((float) width) / ((float) d);
                            float f2 = ((float) height) / ((float) e);
                            float max = Math.max(f, f2);
                            double d4 = ((double) f) * d;
                            double d5 = ((double) f2) * e;
                            Log.d("VideoGLView", String.format("size(%1.0f,%1.0f),scale(%f,%f),mat(%f,%f)", new Object[]{Double.valueOf(d4), Double.valueOf(d5), Float.valueOf(f), Float.valueOf(f2), Double.valueOf(d4 / ((double) width)), Double.valueOf(d5 / ((double) height))}));
                            Matrix.scaleM(this.f7266k, 0, (float) (d4 / ((double) width)), (float) (d5 / ((double) height)), 1.0f);
                            int i5 = (int) ((((double) width) - d4) / 2.0d);
                            int i6 = (int) ((((double) height) - d5) / 2.0d);
                            break;
                        case 3:
                            double d6 = ((double) width) / d;
                            double d7 = ((double) height) / e;
                            double min = videoGLView.f7248o == 3 ? Math.max(d6, d7) : Math.min(d6, d7);
                            double d8 = min * d;
                            double d9 = min * e;
                            Log.d("VideoGLView", String.format("size(%1.0f,%1.0f),scale(%f,%f),mat(%f,%f)", new Object[]{Double.valueOf(d8), Double.valueOf(d9), Double.valueOf(d6), Double.valueOf(d7), Double.valueOf(d8 / ((double) width)), Double.valueOf(d9 / ((double) height))}));
                            Matrix.scaleM(this.f7266k, 0, (float) (d8 / ((double) width)), (float) (d9 / ((double) height)), 1.0f);
                            break;
                        case 4:
                            double max2 = Math.max(((double) width) / d, ((double) height) / e);
                            double d10 = max2 * d;
                            double d11 = max2 * e;
                            GLES20.glViewport((int) ((((double) width) - d10) / 2.0d), (int) ((((double) height) - d11) / 2.0d), (int) d10, (int) d11);
                            GLES20.glClear(16384);
                            break;
                    }
                    if (this.f7259d != null) {
                        this.f7259d.mo14382a(this.f7266k, 0);
                    }
                }
            }
        }

        /* renamed from: a */
        public void mo10738a() {
            if (this.f7259d != null) {
                this.f7259d.mo14380a();
                this.f7259d = null;
            }
            if (this.f7257b != null) {
                this.f7257b.release();
                this.f7257b = null;
            }
            GLDrawer2D.m14639a(this.f7258c);
        }

        /* renamed from: a */
        public void mo10739a(Bitmap bitmap, SpecialEffect specialEffect) {
            if (bitmap != null) {
                this.f7262g = bitmap;
                this.f7263h = specialEffect;
                m9763b();
            }
        }

        /* renamed from: a */
        public void mo10740a(MediaVideoEncoder jrVar) {
            this.f7267l = jrVar;
        }

        public void onDrawFrame(GL10 gl10) {
            boolean z = true;
            GLES20.glClear(16384);
            if (this.f7268m) {
                this.f7268m = false;
                this.f7257b.updateTexImage();
                this.f7257b.getTransformMatrix(this.f7265j);
            }
            this.f7259d.mo14381a(this.f7258c, this.f7265j);
            if (this.f7260e != null) {
                this.f7260e.mo10714a(this.f7264i, 1);
            }
            if (this.f7269n) {
                z = false;
            }
            this.f7269n = z;
            if (this.f7269n) {
                synchronized (this) {
                    if (this.f7267l != null) {
                        this.f7267l.mo14405a(this.f7265j, this.f7266k);
                    }
                }
            }
        }

        public void onFrameAvailable(SurfaceTexture surfaceTexture) {
            this.f7268m = true;
        }

        public void onSurfaceChanged(GL10 gl10, int i, int i2) {
            if (i != 0 && i2 != 0) {
                m9765c();
                float f = ((float) i) / ((float) i2);
                Matrix.frustumM(this.f7264i, 0, -f, f, -1.0f, 1.0f, 1.0f, 10.0f);
                VideoGLView videoGLView = (VideoGLView) this.f7256a.get();
                if (videoGLView != null) {
                    Log.d("VideoGLView", String.format("Starting surface preview (%d, %d)", new Object[]{Integer.valueOf(i), Integer.valueOf(i2)}));
                    videoGLView.m9744a(i, i2);
                }
            }
        }

        public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
            if (!GLES20.glGetString(7939).contains("OES_EGL_image_external")) {
                throw new RuntimeException("This system does not support OES_EGL_image_external.");
            }
            this.f7258c = GLDrawer2D.m14641b();
            this.f7257b = new SurfaceTexture(this.f7258c);
            this.f7257b.setOnFrameAvailableListener(this);
            VideoGLView videoGLView = (VideoGLView) this.f7256a.get();
            if (videoGLView != null) {
                videoGLView.f7243j = true;
            }
            this.f7259d = new GLDrawer2D();
            this.f7259d.mo14382a(this.f7266k, 0);
        }
    }

    /* renamed from: com.becandid.candid.util.video.VideoGLView$d */
    static final class C1782d extends Thread {

        /* renamed from: a */
        private final Object f7270a = new Object();

        /* renamed from: b */
        private final WeakReference<VideoGLView> f7271b;

        /* renamed from: c */
        private C1779a f7272c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public volatile boolean f7273d = false;

        /* renamed from: e */
        private Camera f7274e;

        /* renamed from: f */
        private boolean f7275f = true;

        public C1782d(VideoGLView videoGLView) {
            super("Camera thread");
            this.f7271b = new WeakReference<>(videoGLView);
        }

        /* renamed from: a */
        private static Size m9771a(List<Size> list, final int i, final int i2) {
            return (Size) Collections.min(list, new Comparator<Size>() {
                /* renamed from: a */
                private int m9781a(Size size) {
                    return Math.abs(i - size.width) + Math.abs(i2 - size.height);
                }

                /* renamed from: a */
                public int compare(Size size, Size size2) {
                    return m9781a(size) - m9781a(size2);
                }
            });
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public final void m9772a(int i, int i2) {
            Log.d("VideoGLView", String.format("Starting preview at width/height(%d, %d)", new Object[]{Integer.valueOf(i), Integer.valueOf(i2)}));
            final VideoGLView videoGLView = (VideoGLView) this.f7271b.get();
            if (videoGLView != null && this.f7274e == null) {
                try {
                    if (VideoGLView.f7234a == null) {
                        VideoGLView.f7234a = new ArrayList();
                        VideoGLView.f7235b = new ArrayList();
                        CameraInfo cameraInfo = new CameraInfo();
                        int numberOfCameras = Camera.getNumberOfCameras();
                        for (int i3 = 0; i3 < numberOfCameras; i3++) {
                            Camera.getCameraInfo(i3, cameraInfo);
                            if (cameraInfo.facing == 1) {
                                VideoGLView.f7234a.add(Integer.valueOf(i3));
                            } else {
                                VideoGLView.f7235b.add(Integer.valueOf(i3));
                            }
                        }
                    }
                    if (this.f7275f) {
                        VideoGLView.f7237d = (VideoGLView.f7234a == null || VideoGLView.f7234a.size() <= 0) ? 0 : ((Integer) VideoGLView.f7234a.get(0)).intValue();
                    } else {
                        VideoGLView.f7237d = (VideoGLView.f7235b == null || VideoGLView.f7235b.size() <= 0) ? 0 : ((Integer) VideoGLView.f7235b.get(0)).intValue();
                    }
                    this.f7274e = Camera.open(VideoGLView.f7237d);
                    Parameters parameters = this.f7274e.getParameters();
                    List supportedFocusModes = parameters.getSupportedFocusModes();
                    if (supportedFocusModes.contains("continuous-video")) {
                        parameters.setFocusMode("continuous-video");
                    } else if (supportedFocusModes.contains("auto")) {
                        parameters.setFocusMode("auto");
                    } else {
                        Log.d("VideoGLView", "Camera does not support autofocus");
                    }
                    List supportedPreviewFpsRange = parameters.getSupportedPreviewFpsRange();
                    int[] iArr = (int[]) supportedPreviewFpsRange.get(supportedPreviewFpsRange.size() - 1);
                    Log.d("VideoGLView", String.format("fps:%d-%d", new Object[]{Integer.valueOf(iArr[0]), Integer.valueOf(iArr[1])}));
                    parameters.setPreviewFpsRange(iArr[0], iArr[1]);
                    parameters.setRecordingHint(true);
                    Size a = m9771a(parameters.getSupportedPreviewSizes(), i, i2);
                    parameters.setPreviewSize(a.width, a.height);
                    final Size a2 = m9771a(parameters.getSupportedPictureSizes(), i, i2);
                    Log.d("VideoGLView", String.format("Picture size set to %1.0f, %1.0f", new Object[]{Float.valueOf((float) a2.width), Float.valueOf((float) a2.height)}));
                    parameters.setPictureSize(a2.width, a2.height);
                    m9773a(parameters);
                    this.f7274e.setParameters(parameters);
                    Size previewSize = this.f7274e.getParameters().getPreviewSize();
                    Log.d("VideoGLView", String.format("previewSize(%d, %d)", new Object[]{Integer.valueOf(previewSize.width), Integer.valueOf(previewSize.height)}));
                    videoGLView.post(new Runnable() {
                        public void run() {
                            videoGLView.setVideoSize(a2.width, a2.height);
                        }
                    });
                    SurfaceTexture surfaceTexture = videoGLView.getSurfaceTexture();
                    surfaceTexture.setDefaultBufferSize(a2.width, a2.height);
                    this.f7274e.setPreviewTexture(surfaceTexture);
                } catch (IOException e) {
                    Log.d("VideoGLView", "error camera startPreview:", e);
                    if (this.f7274e != null) {
                        this.f7274e.release();
                        this.f7274e = null;
                    }
                } catch (RuntimeException e2) {
                    Log.d("VideoGLView", "error camera startPreview:", e2);
                    if (this.f7274e != null) {
                        this.f7274e.release();
                        this.f7274e = null;
                    }
                }
                if (this.f7274e != null) {
                    this.f7274e.startPreview();
                    if (VideoGLView.f7236c != null) {
                        VideoGLView.f7236c.mo10463a();
                    }
                }
            }
        }

        /* renamed from: a */
        private final void m9773a(Parameters parameters) {
            VideoGLView videoGLView = (VideoGLView) this.f7271b.get();
            if (videoGLView != null) {
                int i = 0;
                switch (((WindowManager) videoGLView.getContext().getSystemService("window")).getDefaultDisplay().getRotation()) {
                    case 0:
                        i = 0;
                        break;
                    case 1:
                        i = 90;
                        break;
                    case 2:
                        i = 180;
                        break;
                    case 3:
                        i = 270;
                        break;
                }
                CameraInfo cameraInfo = new CameraInfo();
                Camera.getCameraInfo(VideoGLView.f7237d, cameraInfo);
                this.f7275f = cameraInfo.facing == 1;
                int i2 = this.f7275f ? (360 - ((cameraInfo.orientation + i) % 360)) % 360 : ((cameraInfo.orientation - i) + 360) % 360;
                this.f7274e.setDisplayOrientation(i2);
                videoGLView.f7247n = i2;
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m9776b() {
            if (this.f7274e != null) {
                this.f7274e.stopPreview();
                this.f7274e.release();
                this.f7274e = null;
            }
            VideoGLView videoGLView = (VideoGLView) this.f7271b.get();
            if (videoGLView != null) {
                videoGLView.f7244k = null;
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public void m9778c() {
            if (VideoGLView.f7234a != null && VideoGLView.f7234a.size() > 0 && VideoGLView.f7235b != null && VideoGLView.f7235b.size() > 0) {
                if (this.f7274e != null) {
                    this.f7274e.stopPreview();
                    this.f7274e.release();
                    this.f7274e = null;
                }
                this.f7275f = !this.f7275f;
                m9772a(VideoGLView.f7238e, VideoGLView.f7239f);
            }
        }

        /* renamed from: a */
        public C1779a mo10745a() {
            synchronized (this.f7270a) {
                try {
                    this.f7270a.wait();
                } catch (InterruptedException e) {
                }
            }
            return this.f7272c;
        }

        public void run() {
            Looper.prepare();
            synchronized (this.f7270a) {
                this.f7272c = new C1779a(this);
                this.f7273d = true;
                this.f7270a.notify();
            }
            Looper.loop();
            synchronized (this.f7270a) {
                this.f7272c = null;
                this.f7273d = false;
            }
        }
    }

    public VideoGLView(Context context) {
        this(context, null, 0);
    }

    public VideoGLView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VideoGLView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.f7244k = null;
        this.f7248o = 4;
        this.f7240g = new C1781c(this);
        setEGLContextClientVersion(2);
        setRenderer(this.f7240g);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized void m9744a(int i, int i2) {
        if (this.f7244k == null) {
            C1782d dVar = new C1782d(this);
            dVar.start();
            this.f7244k = dVar.mo10745a();
        }
        this.f7244k.mo10735a(f7238e, f7239f);
    }

    /* renamed from: a */
    public void mo10715a() {
        if (this.f7244k != null) {
            this.f7244k.mo10734a();
        }
    }

    public int getScaleMode() {
        return this.f7248o;
    }

    public SurfaceTexture getSurfaceTexture() {
        if (this.f7240g != null) {
            return this.f7240g.f7257b;
        }
        return null;
    }

    public int getVideoHeight() {
        return this.f7246m;
    }

    public int getVideoWidth() {
        return this.f7245l;
    }

    public void onPause() {
        if (this.f7244k != null) {
            this.f7244k.mo10736a(false);
        }
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (this.f7243j && this.f7244k == null) {
            m9744a(getWidth(), getHeight());
        }
    }

    public void setCameraStatusListener(C1780b bVar) {
        f7236c = bVar;
    }

    public void setDefaultWidthAndHeight(int i, int i2) {
        f7238e = i;
        f7239f = i2;
    }

    public void setScaleMode(int i) {
        if (this.f7248o != i) {
            this.f7248o = i;
            queueEvent(new Runnable() {
                public void run() {
                    VideoGLView.this.f7240g.m9765c();
                }
            });
        }
    }

    public void setSpriteBitmaps(Bitmap[] bitmapArr, SpecialEffect[] specialEffectArr) {
        if (bitmapArr != null) {
            this.f7241h = bitmapArr[0];
            this.f7242i = specialEffectArr[0];
            if (this.f7240g != null) {
                queueEvent(new Runnable() {
                    public void run() {
                        VideoGLView.this.f7240g.mo10739a(VideoGLView.this.f7241h, VideoGLView.this.f7242i);
                    }
                });
            }
        }
    }

    public void setVideoEncoder(final MediaVideoEncoder jrVar) {
        queueEvent(new Runnable() {
            public void run() {
                synchronized (VideoGLView.this.f7240g) {
                    if (jrVar != null) {
                        jrVar.mo14404a(EGL14.eglGetCurrentContext(), VideoGLView.this.f7240g.f7258c, VideoGLView.this.f7240g.f7261f, VideoGLView.this.f7240g.f7263h);
                    }
                    VideoGLView.this.f7240g.mo10740a(jrVar);
                }
            }
        });
    }

    public void setVideoSize(int i, int i2) {
        if (this.f7247n % 180 == 0) {
            this.f7245l = i;
            this.f7246m = i2;
        } else {
            this.f7245l = i2;
            this.f7246m = i;
        }
        queueEvent(new Runnable() {
            public void run() {
                VideoGLView.this.f7240g.m9765c();
            }
        });
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (this.f7244k != null) {
            this.f7244k.mo10736a(true);
        }
        this.f7244k = null;
        this.f7243j = false;
        if (this.f7240g != null) {
            queueEvent(new Runnable() {
                public void run() {
                    VideoGLView.this.f7240g.mo10738a();
                }
            });
        }
        super.surfaceDestroyed(surfaceHolder);
    }
}
