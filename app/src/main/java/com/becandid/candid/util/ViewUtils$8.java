package com.becandid.candid.util;

import android.text.TextPaint;
import android.text.style.StyleSpan;

public class ViewUtils$8 extends StyleSpan {
    public ViewUtils$8(int i) {
        super(i);
    }

    public void updateDrawState(TextPaint textPaint) {
        super.updateDrawState(textPaint);
        textPaint.setUnderlineText(true);
    }
}
