package com.becandid.candid.adapters;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.adapters.ContactsAdapter.LetterHolder;

public class ContactsAdapter$LetterHolder$$ViewBinder<T extends LetterHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.adapters.ContactsAdapter$LetterHolder$$ViewBinder$a */
    /* compiled from: ContactsAdapter$LetterHolder$$ViewBinder */
    public static class C1551a<T extends LetterHolder> implements Unbinder {

        /* renamed from: a */
        private T f6723a;

        protected C1551a(T t) {
            this.f6723a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10300a(T t) {
            t.letter = null;
        }

        public final void unbind() {
            if (this.f6723a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10300a(this.f6723a);
            this.f6723a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1551a a = mo10299a(t);
        t.letter = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.letter, "field 'letter'"), R.id.letter, "field 'letter'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1551a<T> mo10299a(T t) {
        return new C1551a<>(t);
    }
}
