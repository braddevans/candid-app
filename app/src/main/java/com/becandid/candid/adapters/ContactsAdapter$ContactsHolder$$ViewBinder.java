package com.becandid.candid.adapters;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.adapters.ContactsAdapter.ContactsHolder;

public class ContactsAdapter$ContactsHolder$$ViewBinder<T extends ContactsHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.adapters.ContactsAdapter$ContactsHolder$$ViewBinder$a */
    /* compiled from: ContactsAdapter$ContactsHolder$$ViewBinder */
    public static class C1550a<T extends ContactsHolder> implements Unbinder {

        /* renamed from: a */
        private T f6722a;

        protected C1550a(T t) {
            this.f6722a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10297a(T t) {
            t.contactName = null;
            t.contactPhoto = null;
            t.animalBgView = null;
            t.invite = null;
        }

        public final void unbind() {
            if (this.f6722a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10297a(this.f6722a);
            this.f6722a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1550a a = mo10296a(t);
        t.contactName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.contact_name, "field 'contactName'"), R.id.contact_name, "field 'contactName'");
        t.contactPhoto = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.contact_photo, "field 'contactPhoto'"), R.id.contact_photo, "field 'contactPhoto'");
        t.animalBgView = (View) finder.findRequiredView(obj, R.id.animal_bg, "field 'animalBgView'");
        t.invite = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.contact_button, "field 'invite'"), R.id.contact_button, "field 'invite'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1550a<T> mo10296a(T t) {
        return new C1550a<>(t);
    }
}
