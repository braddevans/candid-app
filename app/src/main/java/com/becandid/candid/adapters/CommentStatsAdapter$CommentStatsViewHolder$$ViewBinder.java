package com.becandid.candid.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.adapters.CommentStatsAdapter.CommentStatsViewHolder;

public class CommentStatsAdapter$CommentStatsViewHolder$$ViewBinder<T extends CommentStatsViewHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.adapters.CommentStatsAdapter$CommentStatsViewHolder$$ViewBinder$a */
    /* compiled from: CommentStatsAdapter$CommentStatsViewHolder$$ViewBinder */
    public static class C1545a<T extends CommentStatsViewHolder> implements Unbinder {

        /* renamed from: a */
        private T f6703a;

        protected C1545a(T t) {
            this.f6703a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10284a(T t) {
            t.mUserIcon = null;
            t.mUserName = null;
            t.mUserAction = null;
            t.mMessageIcon = null;
        }

        public final void unbind() {
            if (this.f6703a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10284a(this.f6703a);
            this.f6703a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1545a a = mo10283a(t);
        t.mUserIcon = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.user_icon, "field 'mUserIcon'"), R.id.user_icon, "field 'mUserIcon'");
        t.mUserName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.user_name, "field 'mUserName'"), R.id.user_name, "field 'mUserName'");
        t.mUserAction = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.user_action, "field 'mUserAction'"), R.id.user_action, "field 'mUserAction'");
        t.mMessageIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_icon, "field 'mMessageIcon'"), R.id.message_icon, "field 'mMessageIcon'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1545a<T> mo10283a(T t) {
        return new C1545a<>(t);
    }
}
