package com.becandid.candid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.data.Group;

public class CommunityFilterAdapter extends BaseAdapter<Group> {

    /* renamed from: d */
    Context f6704d;

    public class CommunityFilterViewHolder extends BaseViewHolder {
        @BindView(2131624472)
        LinearLayout body;
        @BindView(2131624475)
        ImageView filter;
        @BindView(2131624473)
        TextView name;
        @BindView(2131624476)
        ImageView selected;

        public CommunityFilterViewHolder(View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
        }
    }

    public CommunityFilterAdapter(Context context) {
        this.f6704d = context;
    }

    /* renamed from: a */
    private void m9309a(boolean z, CommunityFilterViewHolder communityFilterViewHolder) {
        if (z) {
            communityFilterViewHolder.filter.setVisibility(8);
            communityFilterViewHolder.selected.setVisibility(0);
            communityFilterViewHolder.body.setBackground(this.f6704d.getResources().getDrawable(R.drawable.community_info_tag_box_orange));
            return;
        }
        communityFilterViewHolder.filter.setVisibility(0);
        communityFilterViewHolder.selected.setVisibility(8);
        communityFilterViewHolder.body.setBackground(this.f6704d.getResources().getDrawable(R.drawable.community_info_tag_box_grey));
    }

    /* renamed from: a */
    public void mo10285a(Group group) {
        this.f10096a.add(group);
        notifyItemInserted(this.f10096a.size() - 1);
    }

    /* renamed from: a */
    public void onBindViewHolder(BaseViewHolder juVar, int i) {
        Group group = (Group) this.f10096a.get(i);
        ((CommunityFilterViewHolder) juVar).name.setText(group.group_name);
        m9309a(group.filterEnabled, (CommunityFilterViewHolder) juVar);
    }

    /* renamed from: b */
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CommunityFilterViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.community_info_filter, viewGroup, false));
    }
}
