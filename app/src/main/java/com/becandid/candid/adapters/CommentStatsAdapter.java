package com.becandid.candid.adapters;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.R;
import com.becandid.candid.activities.MessageActivity;
import com.becandid.candid.data.User;
import java.util.List;

public class CommentStatsAdapter extends BaseAdapter<User> {

    /* renamed from: d */
    int f6697d;

    /* renamed from: e */
    int f6698e;

    class CommentStatsViewHolder extends BaseViewHolder {
        @BindView(2131624451)
        ImageView mMessageIcon;
        @BindView(2131624453)
        TextView mUserAction;
        @BindView(2131624450)
        TextView mUserIcon;
        @BindView(2131624452)
        TextView mUserName;

        public CommentStatsViewHolder(View view) {
            super(view);
        }
    }

    public CommentStatsAdapter(List<User> list, int i, int i2) {
        this.f6697d = i;
        this.f6698e = i2;
        mo13764a(list);
    }

    /* renamed from: a */
    public void onBindViewHolder(final BaseViewHolder juVar, int i) {
        if (juVar instanceof CommentStatsViewHolder) {
            final User user = (User) this.f10096a.get(i);
            int parseColor = Color.parseColor(user.icon_color);
            C2699jj.m14621a(user.icon_name, parseColor, ((CommentStatsViewHolder) juVar).mUserIcon, 40);
            ((CommentStatsViewHolder) juVar).mUserName.setTextColor(parseColor);
            if (user.liked == 1) {
                ((CommentStatsViewHolder) juVar).mUserAction.setText("liked this comment");
            } else {
                ((CommentStatsViewHolder) juVar).mUserAction.setText("disliked this comment");
            }
            if (user.thats_me == 1) {
                ((CommentStatsViewHolder) juVar).mUserName.setText("You");
                ((CommentStatsViewHolder) juVar).mMessageIcon.setVisibility(8);
                return;
            }
            ((CommentStatsViewHolder) juVar).mUserName.setText(user.user_name);
            ((CommentStatsViewHolder) juVar).mMessageIcon.setVisibility(0);
            ((CommentStatsViewHolder) juVar).mMessageIcon.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(juVar.f10906f, MessageActivity.class);
                    intent.putExtra("post_id", Integer.toString(CommentStatsAdapter.this.f6697d));
                    intent.putExtra("user_name", user.user_name);
                    intent.putExtra("comment_id", Integer.toString(CommentStatsAdapter.this.f6698e));
                    juVar.f10906f.startActivity(intent);
                }
            });
        }
    }

    /* renamed from: b */
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CommentStatsViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_stats_item, viewGroup, false));
    }
}
