package com.becandid.candid.adapters;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.p003v7.widget.RecyclerView.Adapter;
import android.support.p003v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import java.util.List;

public class ContactsAdapter extends Adapter<C1549c> {

    /* renamed from: a */
    private List<C1547a> f6707a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C1548b f6708b;

    public class ContactsHolder extends C1549c implements OnClickListener {

        /* renamed from: a */
        C1547a f6709a;
        @BindView(2131624484)
        View animalBgView;

        /* renamed from: b */
        GradientDrawable f6710b = new GradientDrawable();

        /* renamed from: c */
        int f6711c;
        @BindView(2131624487)
        TextView contactName;
        @BindView(2131624485)
        TextView contactPhoto;
        @BindView(2131624486)
        Button invite;

        ContactsHolder(View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
            this.invite.setOnClickListener(this);
            this.contactPhoto.setTypeface(CandidAnimals.m14511b());
            this.f6710b.setShape(1);
            this.animalBgView.setBackground(this.f6710b);
        }

        /* renamed from: a */
        public void mo10293a(C1547a aVar, int i) {
            this.f6709a = aVar;
            this.f6711c = i;
            this.contactName.setText(Html.fromHtml("<b>" + aVar.f6716c + "</b> " + aVar.f6717d));
            this.contactPhoto.setText(CandidAnimals.m14510a(aVar.f6714a));
            int parseColor = Color.parseColor(aVar.f6715b);
            this.contactPhoto.setTextColor(parseColor);
            this.f6710b.setColor((16777215 & parseColor) | 1040187392);
            if (aVar.f6721h) {
                this.invite.setBackground(this.invite.getContext().getResources().getDrawable(R.drawable.grey_button_bg));
                this.invite.setText("Invited");
                return;
            }
            this.invite.setBackground(this.invite.getContext().getResources().getDrawable(R.drawable.orange_button_bg));
            this.invite.setText("Invite");
        }

        public void onClick(View view) {
            if (ContactsAdapter.this.f6708b != null && this.f6709a != null) {
                ContactsAdapter.this.f6708b.invite(this.f6709a, this.f6711c);
            }
        }
    }

    public class LetterHolder extends C1549c {
        @BindView(2131624488)
        TextView letter;

        LetterHolder(View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
        }

        /* renamed from: a */
        public void mo10293a(C1547a aVar, int i) {
            this.letter.setText(aVar.f6720g);
        }
    }

    /* renamed from: com.becandid.candid.adapters.ContactsAdapter$a */
    public static class C1547a {

        /* renamed from: a */
        public String f6714a;

        /* renamed from: b */
        public String f6715b;

        /* renamed from: c */
        public String f6716c;

        /* renamed from: d */
        public String f6717d;

        /* renamed from: e */
        public String f6718e;

        /* renamed from: f */
        public String f6719f;

        /* renamed from: g */
        public String f6720g;

        /* renamed from: h */
        public boolean f6721h;
    }

    /* renamed from: com.becandid.candid.adapters.ContactsAdapter$b */
    public interface C1548b {
        void invite(C1547a aVar, int i);
    }

    /* renamed from: com.becandid.candid.adapters.ContactsAdapter$c */
    public static class C1549c extends ViewHolder {
        public C1549c(View view) {
            super(view);
        }

        /* renamed from: a */
        public void mo10293a(C1547a aVar, int i) {
        }
    }

    /* renamed from: a */
    public C1549c onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 2) {
            return new ContactsHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contacts_friend_row, viewGroup, false));
        }
        if (i == 1) {
            return new LetterHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contacts_letter_row, viewGroup, false));
        }
        return null;
    }

    /* renamed from: a */
    public void mo10290a(C1548b bVar) {
        this.f6708b = bVar;
    }

    /* renamed from: a */
    public void onBindViewHolder(C1549c cVar, int i) {
        cVar.mo10293a((C1547a) this.f6707a.get(i), i);
    }

    /* renamed from: a */
    public void mo10292a(List<C1547a> list) {
        this.f6707a = list;
        notifyDataSetChanged();
    }

    public int getItemCount() {
        if (this.f6707a != null) {
            return this.f6707a.size();
        }
        return 0;
    }

    public int getItemViewType(int i) {
        return ((C1547a) this.f6707a.get(i)).f6720g == null ? 2 : 1;
    }
}
