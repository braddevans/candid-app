package com.becandid.candid.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.adapters.CommunityFilterAdapter.CommunityFilterViewHolder;

public class CommunityFilterAdapter$CommunityFilterViewHolder$$ViewBinder<T extends CommunityFilterViewHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.adapters.CommunityFilterAdapter$CommunityFilterViewHolder$$ViewBinder$a */
    /* compiled from: CommunityFilterAdapter$CommunityFilterViewHolder$$ViewBinder */
    public static class C1546a<T extends CommunityFilterViewHolder> implements Unbinder {

        /* renamed from: a */
        private T f6706a;

        protected C1546a(T t) {
            this.f6706a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10288a(T t) {
            t.body = null;
            t.name = null;
            t.filter = null;
            t.selected = null;
        }

        public final void unbind() {
            if (this.f6706a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10288a(this.f6706a);
            this.f6706a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1546a a = mo10287a(t);
        t.body = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_info_tag_body, "field 'body'"), R.id.community_info_tag_body, "field 'body'");
        t.name = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_info_tag_name, "field 'name'"), R.id.community_info_tag_name, "field 'name'");
        t.filter = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.symbol_filter, "field 'filter'"), R.id.symbol_filter, "field 'filter'");
        t.selected = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.symbol_selected, "field 'selected'"), R.id.symbol_selected, "field 'selected'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1546a<T> mo10287a(T t) {
        return new C1546a<>(t);
    }
}
