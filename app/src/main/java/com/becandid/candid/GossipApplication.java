package com.becandid.candid;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.Settings.Secure;
import android.support.multidex.MultiDexApplication;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.ContactsInfo;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.squareup.leakcanary.LeakCanary;
import java.util.HashSet;

public class GossipApplication extends MultiDexApplication {

    /* renamed from: a */
    public static volatile Context f6093a;

    /* renamed from: b */
    public static volatile Handler f6094b;

    /* renamed from: c */
    public static RequestManager f6095c;

    /* renamed from: d */
    public static String f6096d = "http://192.168.1.37:5554/";

    /* renamed from: e */
    public static String f6097e;

    /* renamed from: f */
    private static volatile GossipApplication f6098f;

    /* renamed from: g */
    private C1268a f6099g;

    /* renamed from: h */
    private HttpProxyCacheServer f6100h;

    /* renamed from: com.becandid.candid.GossipApplication$a */
    class C1268a implements ActivityLifecycleCallbacks {

        /* renamed from: a */
        public int f6101a;

        /* renamed from: b */
        public int f6102b;

        /* renamed from: c */
        public boolean f6103c;

        C1268a() {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
            this.f6102b++;
            if (this.f6101a == this.f6102b) {
                AppState.saveState(GossipApplication.this, false);
            }
        }

        public void onActivityResumed(Activity activity) {
            this.f6101a++;
            if (this.f6103c) {
                AppState.loadState(GossipApplication.this);
            }
            this.f6103c = false;
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    /* renamed from: a */
    public static GossipApplication m9012a() {
        return f6098f;
    }

    /* renamed from: a */
    public static HttpProxyCacheServer m9013a(Context context) {
        GossipApplication gossipApplication = (GossipApplication) context.getApplicationContext();
        if (gossipApplication.f6100h != null) {
            return gossipApplication.f6100h;
        }
        HttpProxyCacheServer d = gossipApplication.m9014d();
        gossipApplication.f6100h = d;
        return d;
    }

    /* renamed from: d */
    private HttpProxyCacheServer m9014d() {
        return new HttpProxyCacheServer((Context) this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo9698b() {
        FacebookSdk.m17291a(getApplicationContext());
        bdz.m7802a((Context) this, new Crashlytics());
        f6095c = Glide.m15045b(f6093a);
    }

    /* renamed from: c */
    public void mo9699c() {
        if (AppState.contactsInfo == null) {
            AppState.contactsInfo = new ContactsInfo();
        }
        Cursor query = getContentResolver().query(Phone.CONTENT_URI, null, null, null, null);
        while (query != null && query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("data1"));
            try {
                PhoneNumberUtil a = PhoneNumberUtil.m12186a();
                AppState.contactsInfo.contacts.add(a.mo12922a(a.mo12920a(string, "US"), PhoneNumberFormat.E164));
            } catch (NumberParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        if (!LeakCanary.isInAnalyzerProcess(this)) {
            LeakCanary.install(this);
            f6093a = getApplicationContext();
            f6094b = new Handler(Looper.getMainLooper());
            f6098f = this;
            this.f6099g = new C1268a();
            AppState.expandedPostIds = new HashSet<>();
            registerActivityLifecycleCallbacks(this.f6099g);
            mo9698b();
            f6097e = Secure.getString(getContentResolver(), "android_id");
            AppState.loadState(this);
        }
    }
}
