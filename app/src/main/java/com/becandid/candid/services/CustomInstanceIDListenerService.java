package com.becandid.candid.services;

import android.content.Intent;
import com.google.android.gms.iid.InstanceIDListenerService;

public class CustomInstanceIDListenerService extends InstanceIDListenerService {
    /* renamed from: a */
    public void mo10691a() {
        startService(new Intent(this, RegistrationIntentService.class));
    }
}
