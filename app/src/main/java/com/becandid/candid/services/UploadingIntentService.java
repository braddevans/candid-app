package com.becandid.candid.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.p003v7.app.NotificationCompat.Builder;
import android.widget.Toast;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.MessageActivity;
import com.becandid.candid.models.JoinedVideoData;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.models.NetworkData.UploadResponse;
import com.becandid.candid.models.UploadMediaResponse;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class UploadingIntentService extends IntentService {

    /* renamed from: a */
    private Uri f7145a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Uri f7146b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f7147c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f7148d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f7149e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f7150f;

    public UploadingIntentService() {
        super("UploadingIntentService");
    }

    /* renamed from: a */
    private int[] m9687a(Uri uri) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        return new int[]{options.outWidth, options.outHeight};
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        if (intent != null) {
            RxBus.m14573a().mo14349a(new C2645bg());
            final NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
            final Builder builder = new Builder(this);
            builder.setContentTitle("Video Uploading").setContentText("Upload in progress").setProgress(0, 0, true).setSmallIcon(R.drawable.ic_candid_notification).setPriority(1);
            notificationManager.notify(Integer.parseInt("999"), builder.build());
            this.f7145a = (Uri) intent.getParcelableExtra("uri");
            this.f7146b = (Uri) intent.getParcelableExtra("cover_uri");
            this.f7147c = intent.getIntExtra("uploaded", -1);
            this.f7148d = intent.getStringExtra("post_id");
            this.f7149e = intent.getStringExtra("username");
            this.f7150f = intent.getStringExtra("comment_id");
            HashMap hashMap = new HashMap();
            final int[] a = m9687a(this.f7146b);
            hashMap.put("image_width", Integer.toString(a[0]));
            hashMap.put("image_height", Integer.toString(a[1]));
            bjs.m8432a(ApiService.m14297a().mo14118b(this.f7146b, this.f7146b.getPath()).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()), ApiService.m14297a().mo14099a(this.f7145a, this.f7145a.getPath()).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()), (bkk<? super T1, ? super T2, ? extends R>) new bkk<UploadResponse, UploadMediaResponse, JoinedVideoData>() {
                /* renamed from: a */
                public JoinedVideoData call(UploadResponse uploadResponse, UploadMediaResponse uploadMediaResponse) {
                    return new JoinedVideoData(uploadResponse, uploadMediaResponse);
                }
            }).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<JoinedVideoData>() {
                /* renamed from: a */
                public void onNext(JoinedVideoData joinedVideoData) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("video_url", joinedVideoData.mVideoUrl);
                    hashMap.put("uploaded", Integer.toString(UploadingIntentService.this.f7147c));
                    hashMap.put("source_url", joinedVideoData.mCoverUrl);
                    if (UploadingIntentService.this.f7148d != null) {
                        hashMap.put("post_id", UploadingIntentService.this.f7148d);
                        if (UploadingIntentService.this.f7149e != null) {
                            hashMap.put("post_name", UploadingIntentService.this.f7149e);
                        }
                        if (UploadingIntentService.this.f7150f != null) {
                            hashMap.put("thread_comment_id", UploadingIntentService.this.f7150f);
                        }
                        hashMap.put("send", Boolean.toString(true));
                        ApiService.m14297a().mo14170o((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                            /* renamed from: a */
                            public void onNext(NetworkData networkData) {
                                RxBus.m14573a().mo14349a(new C2644bf(networkData.message));
                            }

                            public void onCompleted() {
                            }

                            public void onError(Throwable th) {
                                Toast.makeText(GossipApplication.f6093a, th.toString(), 0).show();
                            }
                        });
                        return;
                    }
                    RxBus.m14573a().mo14349a(new C2653j(joinedVideoData.mVideoUrl, UploadingIntentService.this.f7147c, joinedVideoData.mCoverUrl, a[0], a[1], UploadingIntentService.this.f7146b));
                }

                public void onCompleted() {
                    Intent intent = new Intent(GossipApplication.f6093a, MessageActivity.class);
                    intent.putExtra("post_id", UploadingIntentService.this.f7148d);
                    intent.putExtra("user_name", UploadingIntentService.this.f7149e);
                    builder.setContentText("Upload completed!").setProgress(0, 0, false).setAutoCancel(true).setContentIntent(PendingIntent.getActivity(GossipApplication.f6093a, 999, intent, 134217728));
                    if (VERSION.SDK_INT >= 21) {
                        builder.setColor(GossipApplication.f6093a.getResources().getColor(R.color.gossip));
                    }
                    notificationManager.notify(Integer.parseInt("999"), builder.build());
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    RxBus.m14573a().mo14349a(new C2643be());
                }
            });
        }
    }
}
