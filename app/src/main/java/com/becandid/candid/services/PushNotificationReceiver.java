package com.becandid.candid.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.p001v4.app.NotificationCompat.BigTextStyle;
import android.support.p001v4.app.NotificationCompat.Builder;
import android.util.Log;
import com.becandid.candid.R;
import com.becandid.candid.activities.GroupDetailsActivity;
import com.becandid.candid.activities.MainTabsActivity;
import com.becandid.candid.activities.MessageActivity;
import com.becandid.candid.activities.PostDetailsActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;

public class PushNotificationReceiver extends BroadcastReceiver {

    /* renamed from: a */
    private Context f7143a;

    /* renamed from: a */
    private PendingIntent m9681a(Bundle bundle, String str) {
        Intent intent;
        if (!bundle.containsKey("notification_type")) {
            return null;
        }
        String string = bundle.getString("notification_type", "");
        char c = 65535;
        switch (string.hashCode()) {
            case -2059463938:
                if (string.equals("new_post_friend")) {
                    c = 0;
                    break;
                }
                break;
            case -1659251239:
                if (string.equals("friend_signup")) {
                    c = 8;
                    break;
                }
                break;
            case -1348026953:
                if (string.equals("like_comment")) {
                    c = 6;
                    break;
                }
                break;
            case -877257620:
                if (string.equals("new_post_small_groups")) {
                    c = 9;
                    break;
                }
                break;
            case -641874555:
                if (string.equals("new_post_trending")) {
                    c = 1;
                    break;
                }
                break;
            case -242382124:
                if (string.equals("group_invite_friend")) {
                    c = 7;
                    break;
                }
                break;
            case -163723192:
                if (string.equals("like_post")) {
                    c = 5;
                    break;
                }
                break;
            case -72322863:
                if (string.equals("new_comment_other")) {
                    c = 3;
                    break;
                }
                break;
            case 205758144:
                if (string.equals("new_comment")) {
                    c = 2;
                    break;
                }
                break;
            case 492030777:
                if (string.equals("new_comment_subscribed")) {
                    c = 4;
                    break;
                }
                break;
            case 954925063:
                if (string.equals("message")) {
                    c = 10;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                break;
            case 1:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                break;
            case 2:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                intent.putExtra("scrollToBottom", true);
                intent.putExtra("comment_id", Integer.valueOf(bundle.getString("comment_id")));
                break;
            case 3:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                intent.putExtra("scrollToBottom", true);
                intent.putExtra("comment_id", Integer.valueOf(bundle.getString("comment_id")));
                break;
            case 4:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                intent.putExtra("comment_id", Integer.valueOf(bundle.getString("comment_id")));
                break;
            case 5:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                break;
            case 6:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                intent.putExtra("scrollToBottom", true);
                intent.putExtra("comment_id", Integer.valueOf(bundle.getString("comment_id")));
                break;
            case 7:
                intent = new Intent(this.f7143a, GroupDetailsActivity.class);
                ays m = new ayt().mo8417a(bundle.getString("group")).mo8398m();
                intent.putExtra("group_id", m.mo8410b("group_id").mo8390g());
                intent.putExtra("group_name", m.mo8410b("group_name").mo8385c());
                break;
            case 8:
                intent = new Intent(this.f7143a, MainTabsActivity.class);
                intent.putExtra("selected_tab", "me");
                break;
            case 9:
                intent = new Intent(this.f7143a, PostDetailsActivity.class);
                intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                break;
            case 10:
                intent = new Intent(this.f7143a, MessageActivity.class);
                intent.putExtra("post_id", bundle.getString("post_id"));
                if (bundle.getString("user_info") != null) {
                    intent.putExtra("user_name", (String) DataUtil.toMap(bundle.getString("user_info")).get("post_name"));
                    break;
                }
                break;
            default:
                if (!bundle.containsKey("post_id")) {
                    intent = new Intent(this.f7143a, MainTabsActivity.class);
                    intent.putExtra("selected_tab", "feed");
                    break;
                } else {
                    intent = new Intent(this.f7143a, PostDetailsActivity.class);
                    intent.putExtra("post_id", Integer.valueOf(bundle.getString("post_id")));
                    break;
                }
        }
        if (bundle.containsKey("orig_notification_id")) {
            intent.putExtra("click_notification", bundle.getString("orig_notification_id"));
        }
        return PendingIntent.getActivity(this.f7143a, Integer.parseInt(str), intent, 134217728);
    }

    /* renamed from: a */
    private void m9682a(String str, String str2, String str3, String str4, Bundle bundle) {
        try {
            Integer.parseInt(str);
            PendingIntent pendingIntent = str4 != null ? m9681a(bundle, str) : null;
            Builder contentText = new Builder(this.f7143a).setSmallIcon(R.drawable.ic_candid_notification).setContentTitle(str2).setPriority(1).setStyle(new BigTextStyle().bigText(str3)).setAutoCancel(true).setContentText(str3);
            String string = bundle.getString("notification_sound_on");
            String string2 = bundle.getString("notification_light_on");
            String string3 = bundle.getString("notification_vibration_on");
            int i = -1;
            if (string != null && Integer.parseInt(string) == 1) {
                i = 1;
            }
            if (string2 != null && Integer.parseInt(string2) == 1) {
                i = i == -1 ? 4 : i | 4;
            }
            if (string3 != null && Integer.parseInt(string3) == 1) {
                i = i == -1 ? 2 : i | 2;
            }
            if (i != -1) {
                contentText.setDefaults(i);
            }
            if (pendingIntent != null) {
                contentText.setContentIntent(pendingIntent);
            }
            NotificationManager notificationManager = (NotificationManager) this.f7143a.getSystemService("notification");
            if (VERSION.SDK_INT >= 21) {
                contentText.setColor(this.f7143a.getResources().getColor(R.color.gossip));
            }
            notificationManager.notify(Integer.parseInt(str), contentText.build());
        } catch (NumberFormatException e) {
            Crashlytics.m16437a((Throwable) e);
        }
    }

    /* renamed from: a */
    public void mo10693a(Bundle bundle) {
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (obj != null) {
                    Log.d("PushNotificationPayload", String.format("%s %s (%s)", new Object[]{str, obj.toString(), obj.getClass().getName()}));
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String string;
        this.f7143a = context;
        Bundle extras = intent.getExtras();
        mo10693a(extras);
        if (!extras.containsKey("push_notification_disabled") || !extras.getString("push_notification_disabled", "0").equals("1")) {
            try {
                String string2 = extras.getString("title");
                if (!extras.containsKey("message_id")) {
                    string = extras.getString("message");
                } else if (!AppState.config.getBoolean("messaging_turned_on", true)) {
                    return;
                } else {
                    if (!extras.containsKey("is_request") || !extras.getString("is_request", "false").equals("true")) {
                        string = extras.getString("subject");
                    } else {
                        string = ((String) DataUtil.toMap(extras.getString("user_info")).get("post_name")) + " sent you a message request!";
                    }
                }
                m9682a(extras.getString("notification_id"), string2, string, extras.getString("url"), extras);
            } catch (NullPointerException e) {
                Crashlytics.m16437a((Throwable) e);
            }
        }
    }
}
