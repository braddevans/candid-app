package com.becandid.candid.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.becandid.candid.R;
import com.becandid.candid.data.EmptyClass;
import java.io.IOException;
import p012rx.schedulers.Schedulers;

public class RegistrationIntentService extends IntentService {
    public RegistrationIntentService() {
        super("RegIntentService");
    }

    /* renamed from: a */
    private void m9684a(String str) {
        ApiService.m14297a().mo14137d(str).mo9249a(bkc.m8469a()).mo9256b(Schedulers.m16145io()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
            /* renamed from: a */
            public void onNext(EmptyClass emptyClass) {
            }

            public void onCompleted() {
                PreferenceManager.getDefaultSharedPreferences(RegistrationIntentService.this.getApplicationContext()).edit().putBoolean("sentTokenToServer", true).apply();
            }

            public void onError(Throwable th) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        try {
            String a = aoc.m4198c(this).mo6578a(getResources().getString(R.string.gcm_defaultSenderId), "GCM");
            Log.d("RegIntentService", "GCM Registration Token: " + a);
            m9684a(a);
            defaultSharedPreferences.edit().putString("gcmToken", a).apply();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
