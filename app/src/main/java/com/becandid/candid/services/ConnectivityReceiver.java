package com.becandid.candid.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        RxBus.m14573a().mo14349a(new C2617af(activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()));
    }
}
