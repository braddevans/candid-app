package com.becandid.candid.services;

import com.google.android.gms.gcm.GcmListenerService;

public class GcmMessageHandler extends GcmListenerService {
    /* JADX WARNING: Removed duplicated region for block: B:33:0x016f  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10692a(java.lang.String r16, android.os.Bundle r17) {
        /*
            r15 = this;
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            if (r11 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            android.content.Intent r9 = new android.content.Intent
            java.lang.String r11 = "com.becandid.candid.GOT_PUSH"
            r9.<init>(r11)
            r0 = r17
            r9.putExtras(r0)
            java.lang.String r11 = "unread_activity_count"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x0057
            java.lang.String r11 = "like_post"
            java.lang.String r12 = "notification_type"
            r0 = r17
            java.lang.String r12 = r0.getString(r12)
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x01b7
            java.lang.String r11 = "0"
            java.lang.String r12 = "unread_activity_count"
            r0 = r17
            java.lang.String r12 = r0.getString(r12)
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x01b7
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            int r11 = r11.unread_activity_count
            if (r11 != 0) goto L_0x01ad
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            r12 = 1
            r11.unread_activity_count = r12
        L_0x0046:
            jf r11 = p000.RxBus.m14573a()
            iu$az r12 = new iu$az
            r13 = 3
            com.becandid.candid.data.User r14 = com.becandid.candid.data.AppState.account
            int r14 = r14.unread_activity_count
            r12.<init>(r13, r14)
            r11.mo14349a(r12)
        L_0x0057:
            java.lang.String r11 = "unread_feed_count"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x0082
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            java.lang.String r12 = "unread_feed_count"
            r0 = r17
            java.lang.String r12 = r0.getString(r12)
            int r12 = java.lang.Integer.parseInt(r12)
            r11.unread_feed_count = r12
            jf r11 = p000.RxBus.m14573a()
            iu$az r12 = new iu$az
            r13 = 0
            com.becandid.candid.data.User r14 = com.becandid.candid.data.AppState.account
            int r14 = r14.unread_feed_count
            r12.<init>(r13, r14)
            r11.mo14349a(r12)
        L_0x0082:
            java.lang.String r11 = "unread_groups_count"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x00ad
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            java.lang.String r12 = "unread_groups_count"
            r0 = r17
            java.lang.String r12 = r0.getString(r12)
            int r12 = java.lang.Integer.parseInt(r12)
            r11.unread_groups_count = r12
            jf r11 = p000.RxBus.m14573a()
            iu$az r12 = new iu$az
            r13 = 1
            com.becandid.candid.data.User r14 = com.becandid.candid.data.AppState.account
            int r14 = r14.unread_groups_count
            r12.<init>(r13, r14)
            r11.mo14349a(r12)
        L_0x00ad:
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            int r11 = r11.unread_activity_count
            com.becandid.candid.data.User r12 = com.becandid.candid.data.AppState.account
            int r12 = r12.unread_feed_count
            int r7 = r11 + r12
            p000.bhw.m8185a(r15, r7)
            java.lang.String r11 = "message_id"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            if (r11 == 0) goto L_0x01a7
            java.lang.String r11 = "post_id"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            if (r11 == 0) goto L_0x01a7
            com.becandid.candid.data.Config r11 = com.becandid.candid.data.AppState.config
            java.lang.String r12 = "messaging_turned_on"
            r13 = 1
            boolean r11 = r11.getBoolean(r12, r13)
            if (r11 == 0) goto L_0x01a7
            java.lang.String r11 = "message_id"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            int r2 = java.lang.Integer.parseInt(r11)
            java.lang.String r11 = "user_info"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            java.util.Map r10 = com.becandid.candid.data.DataUtil.toMap(r11)
            java.lang.String r11 = "post_name"
            java.lang.Object r5 = r10.get(r11)
            java.lang.String r5 = (java.lang.String) r5
            java.lang.String r11 = "post_id"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            int r3 = java.lang.Integer.parseInt(r11)
            java.lang.String r11 = "message"
            r0 = r17
            java.lang.String r4 = r0.getString(r11)
            java.lang.String r11 = "subject"
            r0 = r17
            java.lang.String r6 = r0.getString(r11)
            iu$aj r1 = new iu$aj
            r1.<init>(r2, r3, r4, r5, r6)
            java.lang.String r11 = "image_width"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x01c9
            java.lang.String r11 = "image_height"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x01c9
            java.lang.String r11 = "source_url"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x01c9
            java.lang.String r11 = "image_width"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            int r11 = java.lang.Integer.parseInt(r11)
            r1.f10596f = r11
            java.lang.String r11 = "image_height"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            int r11 = java.lang.Integer.parseInt(r11)
            r1.f10597g = r11
            java.lang.String r11 = "source_url"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            r1.f10598h = r11
        L_0x015e:
            jf r11 = p000.RxBus.m14573a()
            r11.mo14349a(r1)
            java.lang.String r11 = "unread_message_count"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x019b
            java.lang.String r11 = "unread_message_count"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            int r8 = java.lang.Integer.parseInt(r11)
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            r11.unread_message_count = r8
            jf r11 = p000.RxBus.m14573a()
            iu$az r12 = new iu$az
            r13 = 2
            r14 = 1
            r12.<init>(r13, r8, r14)
            r11.mo14349a(r12)
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            int r11 = r11.unread_activity_count
            com.becandid.candid.data.User r12 = com.becandid.candid.data.AppState.account
            int r12 = r12.unread_feed_count
            int r11 = r11 + r12
            int r7 = r11 + r8
            p000.bhw.m8185a(r15, r7)
        L_0x019b:
            android.content.Intent r9 = new android.content.Intent
            java.lang.String r11 = "com.becandid.candid.MESSAGING"
            r9.<init>(r11)
            r0 = r17
            r9.putExtras(r0)
        L_0x01a7:
            r11 = 0
            r15.sendOrderedBroadcast(r9, r11)
            goto L_0x0004
        L_0x01ad:
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            int r12 = r11.unread_activity_count
            int r12 = r12 + 1
            r11.unread_activity_count = r12
            goto L_0x0046
        L_0x01b7:
            com.becandid.candid.data.User r11 = com.becandid.candid.data.AppState.account
            java.lang.String r12 = "unread_activity_count"
            r0 = r17
            java.lang.String r12 = r0.getString(r12)
            int r12 = java.lang.Integer.parseInt(r12)
            r11.unread_activity_count = r12
            goto L_0x0046
        L_0x01c9:
            java.lang.String r11 = "sticker_name"
            r0 = r17
            boolean r11 = r0.containsKey(r11)
            if (r11 == 0) goto L_0x015e
            java.lang.String r11 = "sticker_name"
            r0 = r17
            java.lang.String r11 = r0.getString(r11)
            r1.f10599i = r11
            goto L_0x015e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.becandid.candid.services.GcmMessageHandler.mo10692a(java.lang.String, android.os.Bundle):void");
    }
}
