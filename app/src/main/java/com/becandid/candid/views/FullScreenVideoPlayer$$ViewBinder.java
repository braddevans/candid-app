package com.becandid.candid.views;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.VideoView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.FullScreenVideoPlayer;

public class FullScreenVideoPlayer$$ViewBinder<T extends FullScreenVideoPlayer> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.FullScreenVideoPlayer$$ViewBinder$a */
    /* compiled from: FullScreenVideoPlayer$$ViewBinder */
    public static class C1813a<T extends FullScreenVideoPlayer> implements Unbinder {

        /* renamed from: a */
        private T f7334a;

        protected C1813a(T t) {
            this.f7334a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10807a(T t) {
            t.mVideoView = null;
            t.mVideoPlaceholder = null;
            t.mVideoProgressBar = null;
            t.mFullScreenExit = null;
        }

        public final void unbind() {
            if (this.f7334a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10807a(this.f7334a);
            this.f7334a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1813a a = mo10806a(t);
        t.mVideoView = (VideoView) finder.castView((View) finder.findRequiredView(obj, R.id.video_view, "field 'mVideoView'"), R.id.video_view, "field 'mVideoView'");
        t.mVideoPlaceholder = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.video_player_placeholder, "field 'mVideoPlaceholder'"), R.id.video_player_placeholder, "field 'mVideoPlaceholder'");
        t.mVideoProgressBar = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.video_player_progress_bar, "field 'mVideoProgressBar'"), R.id.video_player_progress_bar, "field 'mVideoProgressBar'");
        t.mFullScreenExit = (View) finder.findRequiredView(obj, R.id.fullscreen_exit, "field 'mFullScreenExit'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1813a<T> mo10806a(T t) {
        return new C1813a<>(t);
    }
}
