package com.becandid.candid.views;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.EmptyClass;
import com.becandid.candid.data.Group;
import com.becandid.candid.models.NetworkData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import p012rx.schedulers.Schedulers;

public class GroupStackView extends RelativeLayout implements OnClickListener, OnTouchListener {

    /* renamed from: a */
    RelativeLayout f7342a;

    /* renamed from: b */
    RelativeLayout f7343b;

    /* renamed from: c */
    ImageView f7344c;

    /* renamed from: d */
    private int f7345d;

    /* renamed from: e */
    private int f7346e;

    /* renamed from: f */
    private long f7347f;

    /* renamed from: g */
    private float f7348g;

    /* renamed from: h */
    private float f7349h = 0.0f;

    /* renamed from: i */
    private ConcurrentLinkedQueue<View> f7350i;

    /* renamed from: j */
    private ConcurrentLinkedQueue<View> f7351j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public View f7352k;

    /* renamed from: l */
    private ArrayList<Group> f7353l;

    /* renamed from: m */
    private int f7354m = 0;

    /* renamed from: n */
    private int f7355n = 0;

    /* renamed from: o */
    private boolean f7356o;

    /* renamed from: p */
    private float f7357p;

    /* renamed from: q */
    private float f7358q;

    /* renamed from: r */
    private C1821b[] f7359r = {new C1821b(-13.0f, 5.0f, -0.12f), new C1821b(15.0f, -10.0f, 0.11f), new C1821b(12.0f, 9.0f, -0.16f), new C1821b(-12.0f, -8.0f, 0.17f), new C1821b(-4.0f, -1.0f, -0.19f), new C1821b(9.0f, 3.0f, 0.14f)};

    /* renamed from: s */
    private Button f7360s;

    /* renamed from: t */
    private Button f7361t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public C1822c f7362u;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public boolean f7363v = true;

    /* renamed from: w */
    private boolean f7364w;

    /* renamed from: x */
    private boolean f7365x = false;

    /* renamed from: com.becandid.candid.views.GroupStackView$a */
    public class C1820a {

        /* renamed from: a */
        TextView f7372a;

        /* renamed from: b */
        TextView f7373b;

        /* renamed from: c */
        TextView f7374c;

        /* renamed from: d */
        TextView[] f7375d;

        /* renamed from: e */
        ImageView f7376e;

        /* renamed from: f */
        TextView f7377f;

        /* renamed from: g */
        ImageView f7378g;

        /* renamed from: h */
        ImageView f7379h;

        public C1820a(View view) {
            this.f7372a = (TextView) view.findViewById(R.id.group_name);
            this.f7373b = (TextView) view.findViewById(R.id.group_description);
            this.f7374c = (TextView) view.findViewById(R.id.group_stats);
            this.f7375d = new TextView[]{(TextView) view.findViewById(R.id.group_tag_0), (TextView) view.findViewById(R.id.group_tag_1), (TextView) view.findViewById(R.id.group_tag_2)};
            this.f7376e = (ImageView) view.findViewById(R.id.group_image);
            this.f7377f = (TextView) view.findViewById(R.id.group_created_by_friend);
            this.f7378g = (ImageView) view.findViewById(R.id.group_join_stamp);
            this.f7379h = (ImageView) view.findViewById(R.id.group_skip_stamp);
        }

        /* renamed from: a */
        public void mo10837a(Group group) {
            StringBuilder sb = new StringBuilder(50);
            if (group.num_friends > 1) {
                sb.append(group.num_friends + " friends   ");
            }
            sb.append(group.num_members + " member");
            if (group.num_members != 1) {
                sb.append("s");
            }
            sb.append("   " + group.num_posts + " post");
            if (group.num_posts != 1) {
                sb.append("s");
            }
            this.f7374c.setText(sb.toString());
        }

        /* renamed from: a */
        public void mo10838a(List<String> list) {
            for (int i = 0; i < this.f7375d.length; i++) {
                if (i < list.size()) {
                    this.f7375d[i].setText((CharSequence) list.get(i));
                    this.f7375d[i].setVisibility(0);
                } else {
                    this.f7375d[i].setVisibility(8);
                }
            }
        }
    }

    /* renamed from: com.becandid.candid.views.GroupStackView$b */
    class C1821b {

        /* renamed from: a */
        float f7381a;

        /* renamed from: b */
        float f7382b;

        /* renamed from: c */
        float f7383c;

        public C1821b(float f, float f2, float f3) {
            this.f7381a = f;
            this.f7382b = f2;
            this.f7383c = (60.0f * f3) / 3.1415927f;
        }
    }

    /* renamed from: com.becandid.candid.views.GroupStackView$c */
    public interface C1822c {
        void onEmptyGroups();

        void onJoinGroup(Group group);

        void onSkipGroup(Group group);

        void onTouchEvent(Group group);
    }

    public GroupStackView(Context context) {
        super(context);
        m9848e();
    }

    public GroupStackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m9848e();
    }

    public GroupStackView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m9848e();
    }

    @TargetApi(21)
    public GroupStackView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m9848e();
    }

    /* renamed from: a */
    private void m9844a(View view, float f, int i) {
        float max = Math.max(0.0f, Math.min(1.0f, ((3.0f * f) / ((float) this.f7345d)) - 0.2f));
        float max2 = Math.max(0.0f, Math.min(1.0f, (((-f) * 3.0f) / ((float) this.f7345d)) - 0.2f));
        C1820a aVar = (C1820a) view.getTag(R.id.group_name);
        if (i == 0) {
            aVar.f7378g.setAlpha(max);
            aVar.f7379h.setAlpha(max2);
            return;
        }
        aVar.f7378g.animate().alpha(max).setDuration((long) i).start();
        aVar.f7379h.animate().alpha(max2).setDuration((long) i).start();
    }

    /* renamed from: a */
    private void m9845a(final View view, int i, int i2) {
        if (i > 0) {
            mo10816a((Group) view.getTag(R.id.group_stats));
        } else {
            mo10820b((Group) view.getTag(R.id.group_stats));
        }
        m9844a(view, (float) i, i2 / 4);
        int i3 = i + (i / 5);
        final ViewPropertyAnimator rotation = view.animate().x((float) i3).rotation(i3 > 0 ? 25.0f : -25.0f);
        if (i2 >= 250 || i2 < 0) {
            rotation.setInterpolator(new AccelerateDecelerateInterpolator());
        } else {
            rotation.setInterpolator(new LinearInterpolator());
        }
        rotation.setDuration((long) i2);
        rotation.setListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                Log.d("GSV", "animation end");
                view.setX(0.0f);
                GroupStackView.this.f7343b.removeView(view);
                GroupStackView.this.mo10815a(view);
                GroupStackView.this.mo10821c();
                rotation.setListener(null);
                if (GroupStackView.this.f7342a.getChildCount() <= 0 && GroupStackView.this.f7352k == null) {
                    GroupStackView.this.f7362u.onEmptyGroups();
                }
                GroupStackView.this.f7363v = true;
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
                Log.d("GSV", "animation start");
                GroupStackView.this.mo10822d();
                GroupStackView.this.f7363v = false;
            }
        });
        rotation.start();
    }

    /* renamed from: e */
    private void m9848e() {
        if (getChildCount() <= 0) {
            this.f7364w = true;
            this.f7358q = getResources().getDisplayMetrics().density;
            this.f7351j = new ConcurrentLinkedQueue<>();
            this.f7350i = new ConcurrentLinkedQueue<>();
            this.f7353l = new ArrayList<>();
            this.f7342a = new RelativeLayout(getContext());
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.setMargins(0, 0, 0, (int) (50.0f * this.f7358q));
            this.f7342a.setLayoutParams(layoutParams);
            addView(this.f7342a);
            this.f7344c = new ImageView(getContext());
            setLayoutParams(layoutParams);
            addView(this.f7344c);
            this.f7343b = new RelativeLayout(getContext());
            LayoutParams layoutParams2 = new LayoutParams(-1, -1);
            layoutParams2.setMargins(0, 0, 0, (int) (50.0f * this.f7358q));
            this.f7343b.setLayoutParams(layoutParams2);
            addView(this.f7343b);
            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            relativeLayout.setLayoutParams(new LayoutParams(-1, -1));
            addView(relativeLayout);
            relativeLayout.setOnTouchListener(this);
            LinearLayout linearLayout = new LinearLayout(getContext());
            LayoutParams layoutParams3 = new LayoutParams(-1, (int) (40.0f * this.f7358q));
            layoutParams3.addRule(12);
            layoutParams3.setMargins((int) (15.0f * this.f7358q), 0, (int) (15.0f * this.f7358q), (int) (15.0f * this.f7358q));
            linearLayout.setLayoutParams(layoutParams3);
            addView(linearLayout);
            this.f7361t = new Button(getContext());
            this.f7361t.setText("Skip");
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -1);
            layoutParams4.weight = 1.0f;
            this.f7361t.setLayoutParams(layoutParams4);
            this.f7361t.setBackground(getResources().getDrawable(R.drawable.grey_button));
            linearLayout.addView(this.f7361t);
            this.f7361t.setOnClickListener(this);
            this.f7360s = new Button(getContext());
            this.f7360s.setText("Join");
            LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, -1);
            layoutParams5.weight = 1.0f;
            layoutParams5.setMargins((int) (20.0f * this.f7358q), 0, 0, 0);
            this.f7360s.setLayoutParams(layoutParams5);
            this.f7360s.setBackground(getResources().getDrawable(R.drawable.orange_button));
            this.f7360s.setTextColor(Color.parseColor("#ffffff"));
            linearLayout.addView(this.f7360s);
            this.f7360s.setOnClickListener(this);
            for (int i = 0; i < 6; i++) {
                View f = m9849f();
                f.setAlpha(0.0f);
                this.f7342a.addView(f, 0);
            }
        }
    }

    /* renamed from: f */
    private View m9849f() {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.group_card, this.f7342a, false);
        inflate.setTag(R.id.group_name, new C1820a(inflate));
        return inflate;
    }

    /* renamed from: a */
    public View mo10814a(Group group, int i) {
        View view = this.f7351j.isEmpty() ? m9849f() : (View) this.f7351j.poll();
        if (view.getAlpha() == 0.0f) {
            view.setAlpha(1.0f);
        }
        view.setTag(Integer.valueOf(i));
        view.setTag(R.id.group_stats, group);
        C1820a aVar = (C1820a) view.getTag(R.id.group_name);
        aVar.f7378g.setAlpha(0.0f);
        aVar.f7379h.setAlpha(0.0f);
        aVar.f7378g.setVisibility(8);
        aVar.f7379h.setVisibility(8);
        RoundAndTintBitmap jeVar = new RoundAndTintBitmap(getContext());
        if (this.f7364w) {
            GossipApplication.f6095c.mo14650a(group.imageUrl()).mo14576h().mo14572d(17301613).mo14591a(jeVar).mo14554a(aVar.f7376e);
        }
        aVar.f7372a.setText(group.group_name);
        aVar.f7373b.setText(group.about);
        aVar.mo10838a(group.tags);
        aVar.mo10837a(group);
        if (group.isFriendCreated()) {
            aVar.f7377f.setVisibility(0);
            aVar.f7377f.setTypeface(Typeface.defaultFromStyle(1));
        } else {
            aVar.f7377f.setVisibility(8);
        }
        this.f7342a.addView(view, 0);
        return view;
    }

    /* renamed from: a */
    public void mo10815a(View view) {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        this.f7351j.add(view);
    }

    /* renamed from: a */
    public void mo10816a(Group group) {
        ApiService.m14297a().mo14128c(group.group_id).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                if (networkData.success && GroupStackView.this.f7362u != null) {
                    GroupStackView.this.f7362u.onJoinGroup(networkData.group);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                Log.d("GroupStackView", th.toString());
            }
        });
    }

    /* renamed from: a */
    public void mo10817a(List<Group> list) {
        this.f7353l.addAll(list);
        mo10821c();
        this.f7342a.requestLayout();
        for (int i = 0; i < this.f7342a.getChildCount(); i++) {
            this.f7342a.getChildAt(i).requestLayout();
        }
        this.f7343b.requestLayout();
    }

    /* renamed from: a */
    public boolean mo10818a() {
        return this.f7365x;
    }

    /* renamed from: b */
    public int mo10819b() {
        return this.f7353l.size() - this.f7355n;
    }

    /* renamed from: b */
    public void mo10820b(final Group group) {
        ApiService.m14297a().mo14102a(group).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
            /* renamed from: a */
            public void onNext(EmptyClass emptyClass) {
                if (GroupStackView.this.f7362u != null) {
                    GroupStackView.this.f7362u.onSkipGroup(group);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                Log.d("GroupStackView", th.toString());
            }
        });
    }

    /* renamed from: c */
    public void mo10821c() {
        while (this.f7342a.getChildCount() < 3 && this.f7354m < this.f7353l.size()) {
            View a = mo10814a((Group) this.f7353l.get(this.f7354m), this.f7354m);
            C1821b bVar = this.f7359r[this.f7354m % this.f7359r.length];
            a.setTranslationX(bVar.f7381a);
            a.setTranslationY(bVar.f7382b);
            a.setRotation(bVar.f7383c);
            this.f7354m++;
        }
        mo10822d();
    }

    /* renamed from: d */
    public void mo10822d() {
        int childCount = this.f7342a.getChildCount();
        if (childCount > 0 && this.f7352k == null) {
            View childAt = this.f7342a.getChildAt(childCount - 1);
            Log.d("GSV", "promoteNextCard: " + ((TextView) childAt.findViewById(R.id.group_name)).getText());
            this.f7342a.removeView(childAt);
            this.f7343b.addView(childAt, 0);
            childAt.animate().rotation(0.0f).translationX(0.0f).translationY(0.0f).setDuration(100).start();
            this.f7352k = childAt;
            this.f7355n = this.f7354m - childCount;
            C1820a aVar = (C1820a) childAt.getTag(R.id.group_name);
            aVar.f7378g.setVisibility(0);
            aVar.f7379h.setVisibility(0);
        }
    }

    public boolean isAttachedToWindow() {
        this.f7364w = true;
        return super.isAttachedToWindow();
    }

    public void onClick(View view) {
        if (this.f7352k != null && this.f7363v) {
            if (view == this.f7360s) {
                View view2 = this.f7352k;
                this.f7352k = null;
                m9844a(view2, (float) this.f7345d, 0);
                m9845a(view2, this.f7345d, 750);
            } else if (view == this.f7361t) {
                View view3 = this.f7352k;
                this.f7352k = null;
                m9844a(view3, (float) (-this.f7345d), 0);
                m9845a(view3, -this.f7345d, 750);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.f7364w = false;
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f7345d = getMeasuredWidth();
        this.f7346e = getMeasuredHeight();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f7352k == null) {
            return false;
        }
        float x = motionEvent.getX();
        if (motionEvent.getAction() == 0) {
            if (this.f7362u != null) {
                this.f7362u.onTouchEvent((Group) this.f7352k.getTag(R.id.group_stats));
            }
            this.f7356o = true;
            this.f7357p = x;
            this.f7347f = System.currentTimeMillis();
            this.f7348g = this.f7357p;
            this.f7349h = 0.0f;
            this.f7352k.findViewById(R.id.group_join_stamp).setVisibility(0);
            this.f7352k.findViewById(R.id.group_skip_stamp).setVisibility(0);
            return true;
        }
        float f = x - this.f7357p;
        if (motionEvent.getAction() == 2 && this.f7356o) {
            this.f7352k.setX(f);
            this.f7352k.setRotation((25.0f * f) / ((float) this.f7345d));
            m9844a(this.f7352k, f, 0);
            long currentTimeMillis = System.currentTimeMillis();
            if (this.f7347f > 0) {
                this.f7349h = (0.5f * this.f7349h) + ((0.5f * (x - this.f7348g)) / ((float) (currentTimeMillis - this.f7347f)));
                if (this.f7349h > 0.0f) {
                    this.f7349h = Math.max(this.f7349h, 1.0f);
                } else {
                    this.f7349h = Math.min(this.f7349h, -1.0f);
                }
            }
            this.f7348g = x;
            this.f7347f = currentTimeMillis;
            return true;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            Log.d("GSV", "drag end at " + f + " vel: " + this.f7349h + " (sw: " + this.f7345d);
            int i = 0;
            int i2 = 0;
            int i3 = ((int) f) + ((int) (this.f7349h * 200.0f));
            if (i3 > this.f7345d / 2) {
                Log.d("GSV", "fling right");
                i2 = Math.abs((int) (((((float) this.f7345d) - x) + this.f7357p) / Math.abs(this.f7349h)));
                i = this.f7345d;
            } else if (i3 < (-this.f7345d) / 2) {
                Log.d("GSV", "fling left");
                i2 = Math.abs((int) ((((float) this.f7345d) + f) / Math.abs(this.f7349h)));
                i = -this.f7345d;
            }
            if (i != 0) {
                View view2 = this.f7352k;
                this.f7352k = null;
                m9845a(view2, i, i2);
            } else {
                this.f7352k.animate().x(0.0f).rotation(0.0f).setDuration(250).start();
                m9844a(this.f7352k, 0.0f, 150);
            }
            return true;
        }
    }

    public void setGroups(List<Group> list) {
        this.f7353l.clear();
        this.f7354m = 0;
        this.f7355n = 0;
        for (int i = 0; i < this.f7342a.getChildCount(); i++) {
            this.f7351j.add(this.f7342a.getChildAt(i));
        }
        this.f7342a.removeAllViews();
        if (this.f7352k != null) {
            this.f7343b.removeView(this.f7352k);
        }
        this.f7352k = null;
        mo10817a(list);
    }

    public void setListener(C1822c cVar) {
        this.f7362u = cVar;
    }

    public void setLoadMoreGroups(boolean z) {
        this.f7365x = z;
    }
}
