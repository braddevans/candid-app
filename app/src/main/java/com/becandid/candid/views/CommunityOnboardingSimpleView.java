package com.becandid.candid.views;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.CommunitySearchActivity;
import com.becandid.candid.activities.CommunitySettingsActivity;
import com.becandid.candid.data.AppState;

public class CommunityOnboardingSimpleView extends RelativeLayout {

    /* renamed from: a */
    private Context f7311a;

    /* renamed from: b */
    private C1801a f7312b;

    /* renamed from: com.becandid.candid.views.CommunityOnboardingSimpleView$a */
    public interface C1801a {
        /* renamed from: e */
        void mo10353e();
    }

    public CommunityOnboardingSimpleView(Context context) {
        super(context);
        m9820a(context);
    }

    public CommunityOnboardingSimpleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m9820a(context);
    }

    public CommunityOnboardingSimpleView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m9820a(context);
    }

    /* renamed from: a */
    private void m9819a(int i) {
        Intent intent = new Intent(this.f7311a, CommunitySearchActivity.class);
        String str = null;
        String str2 = null;
        switch (i) {
            case CommunitySettingsActivity.REQUEST_HIGHSCHOOL /*201*/:
                str = "school";
                str2 = AppState.config.getString("community_search_high_school_placeholder", "ex. Mill High School");
                break;
            case CommunitySettingsActivity.REQUEST_COLLEGE /*202*/:
                str = "college";
                str2 = AppState.config.getString("community_search_college_placeholder", "ex. University of San Francisco");
                break;
            case 203:
                str = "company";
                str2 = AppState.config.getString("community_search_work_placeholder", "ex. MyLikes");
                break;
        }
        intent.putExtra("queryType", str);
        intent.putExtra("searchViewHint", str2);
        if (this.f7311a instanceof BaseActivity) {
            ((BaseActivity) this.f7311a).startActivityForResult(intent, i);
        }
    }

    /* renamed from: a */
    private void m9820a(Context context) {
        LayoutInflater.from(context).inflate(R.layout.onboarding_community_simple, this, true);
        ButterKnife.bind((View) this);
        this.f7311a = context;
    }

    /* access modifiers changed from: 0000 */
    @OnClick({2131624916})
    public void onCommunityCollegeClick(View view) {
        m9819a((int) CommunitySettingsActivity.REQUEST_COLLEGE);
    }

    /* access modifiers changed from: 0000 */
    @OnClick({2131624917})
    public void onCommunityCompanyClick(View view) {
        m9819a(203);
    }

    /* access modifiers changed from: 0000 */
    @OnClick({2131624915})
    public void onCommunitySchoolClick(View view) {
        m9819a((int) CommunitySettingsActivity.REQUEST_HIGHSCHOOL);
    }

    /* access modifiers changed from: 0000 */
    @OnClick({2131624864})
    public void onCommunitySkipClick(View view) {
        if (this.f7312b != null) {
            this.f7312b.mo10353e();
        }
    }

    public void setOnSkipClick(C1801a aVar) {
        this.f7312b = aVar;
    }
}
