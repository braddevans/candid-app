package com.becandid.candid.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class TabView extends RelativeLayout {

    /* renamed from: a */
    protected ViewGroup f7419a;

    public TabView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void setContentView(int i) {
        setContentView(LayoutInflater.from(getContext()).inflate(i, this, false));
    }

    /* access modifiers changed from: protected */
    public void setContentView(View view) {
        this.f7419a = (ViewGroup) view;
        addView(view);
        view.setLayoutParams(new LayoutParams(-1, -1));
    }
}
