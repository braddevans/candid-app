package com.becandid.candid.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.R;
import com.becandid.candid.activities.CreateGroupActivity;
import com.becandid.candid.activities.GroupSearchActivity;
import com.becandid.candid.activities.MainTabsActivity;

public class GroupHeaderView extends RelativeLayout {
    public GroupHeaderView(Context context) {
        super(context);
        mo10808a(context);
    }

    public GroupHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo10808a(context);
    }

    public GroupHeaderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo10808a(context);
    }

    /* renamed from: a */
    public void mo10808a(Context context) {
        LayoutInflater.from(context).inflate(R.layout.group_header, this, true);
        ButterKnife.bind((View) this);
    }

    @OnClick({2131624701})
    public void onCreateGroupClick(View view) {
        Activity activity = (Activity) view.getContext();
        if (activity instanceof MainTabsActivity) {
            activity.startActivityForResult(new Intent(activity, CreateGroupActivity.class), 1001);
        }
    }

    @OnClick({2131624700})
    public void onGroupSearchClick(View view) {
        getContext().startActivity(new Intent(getContext(), GroupSearchActivity.class));
    }
}
