package com.becandid.candid.views;

import android.content.Context;
import android.support.p003v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;

public class HeaderView extends FrameLayout {
    @BindView(2131624085)
    Toolbar mToolbar;
    @BindView(2131624086)
    TextView mToolbarText;

    public HeaderView(Context context) {
        super(context);
        m9863a(null);
    }

    public HeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m9863a(attributeSet);
    }

    public HeaderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m9863a(attributeSet);
    }

    /* renamed from: a */
    private void m9863a(AttributeSet attributeSet) {
        LayoutInflater.from(getContext()).inflate(R.layout.header_view, this, true);
        ButterKnife.bind((View) this);
        BaseActivity baseActivity = (BaseActivity) getContext();
        baseActivity.setSupportActionBar(this.mToolbar);
        baseActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        baseActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        baseActivity.getSupportActionBar().setHomeAsUpIndicator((int) R.drawable.back_chevron);
        if (attributeSet != null) {
            this.mToolbarText.setText(baseActivity.obtainStyledAttributes(attributeSet, C2434a.HeaderView).getString(0));
        }
    }
}
