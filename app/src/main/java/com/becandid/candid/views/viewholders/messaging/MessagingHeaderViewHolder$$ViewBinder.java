package com.becandid.candid.views.viewholders.messaging;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.messaging.MessagingHeaderViewHolder;

public class MessagingHeaderViewHolder$$ViewBinder<T extends MessagingHeaderViewHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.messaging.MessagingHeaderViewHolder$$ViewBinder$a */
    /* compiled from: MessagingHeaderViewHolder$$ViewBinder */
    public static class C1925a<T extends MessagingHeaderViewHolder> implements Unbinder {

        /* renamed from: a */
        private T f7556a;

        protected C1925a(T t) {
            this.f7556a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10986a(T t) {
            t.messagesHeaderName = null;
            t.messagesHeaderUserIcon = null;
            t.messagesHeaderIconContainer = null;
        }

        public final void unbind() {
            if (this.f7556a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10986a(this.f7556a);
            this.f7556a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1925a a = mo10985a(t);
        t.messagesHeaderName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_header_name, "field 'messagesHeaderName'"), R.id.messages_header_name, "field 'messagesHeaderName'");
        t.messagesHeaderUserIcon = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_header_user_icon, "field 'messagesHeaderUserIcon'"), R.id.messages_header_user_icon, "field 'messagesHeaderUserIcon'");
        t.messagesHeaderIconContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.messages_header_user_icon_container, "field 'messagesHeaderIconContainer'"), R.id.messages_header_user_icon_container, "field 'messagesHeaderIconContainer'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1925a<T> mo10985a(T t) {
        return new C1925a<>(t);
    }
}
