package com.becandid.candid.views.viewholders.messaging;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.messaging.MessagingPostViewHolder;

public class MessagingPostViewHolder$$ViewBinder<T extends MessagingPostViewHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.messaging.MessagingPostViewHolder$$ViewBinder$a */
    /* compiled from: MessagingPostViewHolder$$ViewBinder */
    public static class C1930a<T extends MessagingPostViewHolder> implements Unbinder {

        /* renamed from: a */
        private T f7559a;

        protected C1930a(T t) {
            this.f7559a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10993a(T t) {
            t.messagePostDeleted = null;
            t.messagePostWrapper = null;
            t.mPostCaption = null;
            t.mPostImage = null;
            t.mPostRow = null;
            t.mHeader = null;
            t.mPostPopupAction = null;
            t.mPostHeaderBadge = null;
            t.mPostHeaderBadgeText = null;
            t.mPostHeaderBadgeScore = null;
            t.mPostHeaderBadgeDot = null;
            t.mPostAuthor = null;
            t.mPostIcon = null;
            t.mPostModIcon = null;
            t.mPostGroup = null;
            t.mPostGroupContainer = null;
            t.mPostGroupMenu = null;
            t.mPostGroupMenuContainer = null;
            t.mPostTimestamp = null;
            t.mPostLinkPlacehoder = null;
            t.mPostLinkTitle = null;
            t.mPostLinkImage = null;
            t.mPostLinkDesc = null;
            t.mPostLinkDomain = null;
        }

        public final void unbind() {
            if (this.f7559a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10993a(this.f7559a);
            this.f7559a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1930a a = mo10992a(t);
        t.messagePostDeleted = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.message_post_deleted, "field 'messagePostDeleted'"), R.id.message_post_deleted, "field 'messagePostDeleted'");
        t.messagePostWrapper = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_post_wrapper, "field 'messagePostWrapper'"), R.id.message_post_wrapper, "field 'messagePostWrapper'");
        t.mPostCaption = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.message_post_caption, "field 'mPostCaption'"), R.id.message_post_caption, "field 'mPostCaption'");
        t.mPostImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_post_image, "field 'mPostImage'"), R.id.message_post_image, "field 'mPostImage'");
        t.mPostRow = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_post_row, "field 'mPostRow'"), R.id.message_post_row, "field 'mPostRow'");
        t.mHeader = (View) finder.findRequiredView(obj, R.id.post_header, "field 'mHeader'");
        t.mPostPopupAction = (View) finder.findRequiredView(obj, R.id.post_popup_action, "field 'mPostPopupAction'");
        t.mPostHeaderBadge = (View) finder.findRequiredView(obj, R.id.header_badge, "field 'mPostHeaderBadge'");
        t.mPostHeaderBadgeText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.header_badge_text, "field 'mPostHeaderBadgeText'"), R.id.header_badge_text, "field 'mPostHeaderBadgeText'");
        t.mPostHeaderBadgeScore = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.header_badge_score, "field 'mPostHeaderBadgeScore'"), R.id.header_badge_score, "field 'mPostHeaderBadgeScore'");
        t.mPostHeaderBadgeDot = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.header_badge_dot, "field 'mPostHeaderBadgeDot'"), R.id.header_badge_dot, "field 'mPostHeaderBadgeDot'");
        t.mPostAuthor = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_author, "field 'mPostAuthor'"), R.id.post_author, "field 'mPostAuthor'");
        t.mPostIcon = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_icon, "field 'mPostIcon'"), R.id.post_icon, "field 'mPostIcon'");
        t.mPostModIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_mod_icon, "field 'mPostModIcon'"), R.id.post_mod_icon, "field 'mPostModIcon'");
        t.mPostGroup = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_group, "field 'mPostGroup'"), R.id.post_group, "field 'mPostGroup'");
        t.mPostGroupContainer = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_group_container, "field 'mPostGroupContainer'"), R.id.post_group_container, "field 'mPostGroupContainer'");
        t.mPostGroupMenu = (View) finder.findRequiredView(obj, R.id.post_group_menu, "field 'mPostGroupMenu'");
        t.mPostGroupMenuContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_group_menu_container, "field 'mPostGroupMenuContainer'"), R.id.post_group_menu_container, "field 'mPostGroupMenuContainer'");
        t.mPostTimestamp = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_timestamp, "field 'mPostTimestamp'"), R.id.post_timestamp, "field 'mPostTimestamp'");
        t.mPostLinkPlacehoder = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.link_placeholder, "field 'mPostLinkPlacehoder'"), R.id.link_placeholder, "field 'mPostLinkPlacehoder'");
        t.mPostLinkTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_title, "field 'mPostLinkTitle'"), R.id.post_link_title, "field 'mPostLinkTitle'");
        t.mPostLinkImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_image, "field 'mPostLinkImage'"), R.id.post_link_image, "field 'mPostLinkImage'");
        t.mPostLinkDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_desc, "field 'mPostLinkDesc'"), R.id.post_link_desc, "field 'mPostLinkDesc'");
        t.mPostLinkDomain = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_domain, "field 'mPostLinkDomain'"), R.id.post_link_domain, "field 'mPostLinkDomain'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1930a<T> mo10992a(T t) {
        return new C1930a<>(t);
    }
}
