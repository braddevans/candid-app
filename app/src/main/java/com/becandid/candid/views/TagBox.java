package com.becandid.candid.views;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.StateListDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;

public class TagBox extends LinearLayout {

    /* renamed from: a */
    Context f7422a;

    /* renamed from: b */
    public C1851a f7423b;
    @BindView(2131625116)
    ImageView mTagIcon;
    @BindView(2131625117)
    TextView mTagName;

    /* renamed from: com.becandid.candid.views.TagBox$a */
    public interface C1851a {
        /* renamed from: a */
        void mo10405a(View view, String str);
    }

    public TagBox(Context context, final String str) {
        super(context);
        this.f7422a = context;
        LayoutInflater.from(context).inflate(R.layout.tag_box, this, true);
        ButterKnife.bind((View) this);
        try {
            StateListDrawable stateListDrawable = new StateListDrawable();
            String str2 = "onboarding_tags_" + str.toLowerCase().replace(" ", "_").replace("'", "");
            stateListDrawable.addState(new int[]{16842913}, getResources().getDrawable(getResources().getIdentifier(str2 + "_selected", "drawable", context.getPackageName())));
            stateListDrawable.addState(new int[0], getResources().getDrawable(getResources().getIdentifier(str2, "drawable", context.getPackageName())));
            this.mTagIcon.setImageDrawable(stateListDrawable);
        } catch (NotFoundException e) {
            Log.d("Onboarding", "Resource not found for tag icon");
        }
        this.mTagName.setText(str);
        setTag(str);
        setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (TagBox.this.f7423b != null) {
                    TagBox.this.f7423b.mo10405a(view, str);
                }
            }
        });
    }

    public String getTagName() {
        return this.mTagName.getText().toString();
    }

    public String getTagNameLowerCase() {
        return this.mTagName.getText().toString().toLowerCase();
    }

    public void setTagClickListener(C1851a aVar) {
        this.f7423b = aVar;
    }
}
