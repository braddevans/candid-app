package com.becandid.candid.views;

import android.content.Context;
import android.support.p001v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.becandid.candid.data.AppState;

public class TabViewPager extends ViewPager {

    /* renamed from: d */
    private Context f7420d;

    /* renamed from: e */
    private String f7421e;

    public TabViewPager(Context context) {
        super(context);
        this.f7420d = context;
    }

    public TabViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f7420d = context;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.f7421e != null && this.f7421e.equals("feed") && AppState.config.getInt("feed_swipe_enabled", 0) == 1) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        if (this.f7421e == null || !this.f7421e.equals("me") || AppState.config.getInt("me_swipe_enabled", 0) != 1) {
            return false;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (AppState.config.getInt("feed_swipe_enabled", 0) == 0) {
            return super.onTouchEvent(motionEvent);
        }
        if (this.f7421e == null || !this.f7421e.equals("me") || AppState.config.getInt("me_swipe_enabled", 0) != 1) {
            return false;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void setCurrTab(String str) {
        this.f7421e = str;
    }
}
