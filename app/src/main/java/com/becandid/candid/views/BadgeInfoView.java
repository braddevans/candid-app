package com.becandid.candid.views;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;

public class BadgeInfoView extends BlurLayout {

    /* renamed from: b */
    private String f7287b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C1788a f7288c;
    @BindView(2131624079)
    View mBadgeContainer;
    @BindView(2131624081)
    TextView mBadgeDesc;
    @BindView(2131624083)
    ImageView mBadgeIcon;
    @BindView(2131624080)
    TextView mBadgeTitle;
    @BindView(2131624082)
    View seeAllBadges;

    /* renamed from: com.becandid.candid.views.BadgeInfoView$a */
    public interface C1788a {
        /* renamed from: k */
        void mo10512k();
    }

    public BadgeInfoView(Context context, String str, boolean z, String str2) {
        super(context);
        this.f7287b = str;
        this.f7290a = str2;
        LayoutInflater.from(context).inflate(R.layout.activity_badge_info, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2548a a = BadgeFactory.m14098a().mo13968a(this.f7287b);
        if (a != null) {
            this.mBadgeIcon.setImageResource(a.f10366c);
            this.mBadgeTitle.setText(a.f10365b);
            this.mBadgeDesc.setText(a.f10367d);
            this.mBadgeTitle.setTextColor(Color.parseColor(a.f10368e));
        }
        C2699jj.m14618a(this.mBadgeContainer, "#ffffff");
        if (z) {
            this.seeAllBadges.setVisibility(0);
            this.seeAllBadges.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    BadgeInfoView.this.f7288c.mo10512k();
                }
            });
            return;
        }
        this.seeAllBadges.setVisibility(8);
    }

    public void setSeeAllBadgesListener(C1788a aVar) {
        this.f7288c = aVar;
    }
}
