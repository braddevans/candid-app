package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class GifSearchViewHolder extends BaseViewHolder {
    @BindView(2131624693)
    public ImageView mGifImg;
    @BindView(2131624695)
    public View mGifLoadingImageProgress;
    @BindView(2131624694)
    public View mGifLoadingImageProgressContainer;

    public GifSearchViewHolder(View view) {
        super(view);
        ButterKnife.bind((Object) this, view);
    }
}
