package com.becandid.candid.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.PopupWithBlurBackgroundActivity;
import com.becandid.candid.data.AppState;

public class PostMovedView extends BlurLayout {

    /* renamed from: b */
    PopupWithBlurBackgroundActivity f7405b;
    @BindView(2131625080)
    TextView mBadgeDesc;
    @BindView(2131624083)
    ImageView mBadgeIcn;
    @BindView(2131625079)
    TextView mBadgeTitle;
    @BindView(2131625078)
    View mMessageContainer;
    @BindView(2131624482)
    Button mOkBtn;

    public PostMovedView(Context context, String str, String str2) {
        String format;
        super(context);
        this.f7405b = (PopupWithBlurBackgroundActivity) context;
        this.f7290a = str;
        LayoutInflater.from(context).inflate(R.layout.post_moved_view, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mMessageContainer, "#ffffff");
        this.mOkBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PostMovedView.this.m9898b();
            }
        });
        this.mBadgeTitle.setText(AppState.config.getString("post_moved_header", context.getString(R.string.post_moved_header)));
        if (AppState.config.getString("post_moved_body") == null || AppState.config.getString("post_moved_body").isEmpty()) {
            String string = context.getString(R.string.post_moved_body);
            if (str2 != null) {
                format = String.format(string, new Object[]{str2});
            } else {
                format = String.format(string, new Object[]{""});
            }
            this.mBadgeDesc.setText(format);
            return;
        }
        this.mBadgeDesc.setText(AppState.config.getString("post_moved_body"));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9898b() {
        FrameLayout frameLayout = (FrameLayout) this.f7405b.findViewById(R.id.popup_info_container);
        if (frameLayout != null) {
            this.f7405b.slideOutAnimation(frameLayout);
        }
    }
}
