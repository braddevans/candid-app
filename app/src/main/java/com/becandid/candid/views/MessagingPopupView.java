package com.becandid.candid.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.PopupWithBlurBackgroundActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.EmptySubscriber;
import java.util.HashMap;
import p012rx.schedulers.Schedulers;

public class MessagingPopupView extends BlurLayout {

    /* renamed from: b */
    PopupWithBlurBackgroundActivity f7387b;
    @BindView(2131624777)
    TextView badgeDescEnabled;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public boolean f7388c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f7389d;
    @BindView(2131624787)
    ImageView mAutoCheckmark;
    @BindView(2131624786)
    View mAutoDeleteMsgContainer;
    @BindView(2131624083)
    ImageView mBadgeIcon;
    @BindView(2131624779)
    TextView mDisabledTextView;
    @BindView(2131624789)
    Button mFinishBtn;
    @BindView(2131624778)
    TextView mKeepEnabledTextView;
    @BindView(2131624784)
    ImageView mManualCheckmark;
    @BindView(2131624783)
    View mManualDeleteMsgContainer;
    @BindView(2131624790)
    View mMessageCloseBtn;
    @BindView(2131624775)
    View mMessageContainerEnabled;

    public MessagingPopupView(Context context, String str, boolean z) {
        super(context);
        this.f7387b = (PopupWithBlurBackgroundActivity) context;
        this.f7290a = str;
        LayoutInflater.from(context).inflate(R.layout.messaging_popup_view, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mMessageContainerEnabled, "#ffffff");
        if (z) {
            this.badgeDescEnabled.setText(R.string.you_received_a_message_do_you_want_to_keep_messaging_enabled);
        } else {
            this.badgeDescEnabled.setText(R.string.welcome_to_messages);
        }
        this.mKeepEnabledTextView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                HashMap hashMap = new HashMap();
                hashMap.put("messaging_disabled", "0");
                ApiService.m14297a().mo14173r(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                AppState.account.messaging_disabled = 0;
                RxBus.m14573a().mo14349a(new C2616ae(0));
                MessagingPopupView.this.m9869b();
            }
        });
        this.mDisabledTextView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                HashMap hashMap = new HashMap();
                hashMap.put("messaging_disabled", "1");
                ApiService.m14297a().mo14173r(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                AppState.account.messaging_disabled = 1;
                RxBus.m14573a().mo14349a(new C2616ae(1));
                MessagingPopupView.this.m9869b();
            }
        });
        this.mAutoDeleteMsgContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                boolean z = true;
                if (!MessagingPopupView.this.f7389d) {
                    MessagingPopupView.this.setAutoState(true);
                    MessagingPopupView.this.setManualState(false);
                    MessagingPopupView.this.f7388c = false;
                } else {
                    MessagingPopupView.this.setAutoState(false);
                }
                MessagingPopupView messagingPopupView = MessagingPopupView.this;
                if (MessagingPopupView.this.f7389d) {
                    z = false;
                }
                messagingPopupView.f7389d = z;
                MessagingPopupView.this.m9872c();
            }
        });
        this.mManualDeleteMsgContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                boolean z = true;
                if (!MessagingPopupView.this.f7388c) {
                    MessagingPopupView.this.setAutoState(false);
                    MessagingPopupView.this.setManualState(true);
                    MessagingPopupView.this.f7389d = false;
                } else {
                    MessagingPopupView.this.setManualState(false);
                }
                MessagingPopupView messagingPopupView = MessagingPopupView.this;
                if (MessagingPopupView.this.f7388c) {
                    z = false;
                }
                messagingPopupView.f7388c = z;
                MessagingPopupView.this.m9872c();
            }
        });
        this.mFinishBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                HashMap hashMap = new HashMap();
                int i = 0;
                if (MessagingPopupView.this.f7389d) {
                    i = 1;
                }
                hashMap.put("message_auto_deletion", Integer.toString(i));
                AppState.account.message_auto_deletion = i;
                ApiService.m14297a().mo14173r(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                MessagingPopupView.this.m9869b();
            }
        });
        this.mMessageCloseBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MessagingPopupView.this.m9869b();
            }
        });
        this.mManualDeleteMsgContainer.performClick();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9869b() {
        FrameLayout frameLayout = (FrameLayout) this.f7387b.findViewById(R.id.popup_info_container);
        if (frameLayout != null) {
            this.f7387b.slideOutAnimation(frameLayout);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m9872c() {
        if (this.f7388c || this.f7389d) {
            this.mFinishBtn.setEnabled(true);
        } else {
            this.mFinishBtn.setEnabled(false);
        }
    }

    /* access modifiers changed from: private */
    public void setAutoState(boolean z) {
        if (z) {
            this.mAutoDeleteMsgContainer.setBackground(getResources().getDrawable(R.drawable.green_checkmark_bg));
            this.mAutoCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.green_circle_check));
            return;
        }
        this.mAutoDeleteMsgContainer.setBackground(getResources().getDrawable(R.drawable.empty_checkmark_bg));
        this.mAutoCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.empty_checkmark));
    }

    /* access modifiers changed from: private */
    public void setManualState(boolean z) {
        if (z) {
            this.mManualDeleteMsgContainer.setBackground(getResources().getDrawable(R.drawable.green_checkmark_bg));
            this.mManualCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.green_circle_check));
            return;
        }
        this.mManualDeleteMsgContainer.setBackground(getResources().getDrawable(R.drawable.empty_checkmark_bg));
        this.mManualCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.empty_checkmark));
    }
}
