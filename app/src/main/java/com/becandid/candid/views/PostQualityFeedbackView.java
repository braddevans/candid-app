package com.becandid.candid.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.PopupWithBlurBackgroundActivity;
import com.becandid.candid.data.AppState;

public class PostQualityFeedbackView extends BlurLayout {

    /* renamed from: b */
    PopupWithBlurBackgroundActivity f7407b;
    @BindView(2131625083)
    TextView mBadgeDesc;
    @BindView(2131624083)
    ImageView mBadgeIcn;
    @BindView(2131625082)
    TextView mBadgeTitle;
    @BindView(2131625081)
    View mMessageContainer;
    @BindView(2131624482)
    Button mOkBtn;

    public PostQualityFeedbackView(Context context, String str) {
        super(context);
        this.f7407b = (PopupWithBlurBackgroundActivity) context;
        this.f7290a = str;
        LayoutInflater.from(context).inflate(R.layout.post_quality_feedback_view, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mMessageContainer, "#ffffff");
        this.mOkBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PostQualityFeedbackView.this.m9906b();
            }
        });
        this.mBadgeTitle.setText(AppState.config.getString("low_quality_post_header", context.getString(R.string.low_quality_post_header)));
        this.mBadgeDesc.setText(AppState.config.getString("low_quality_post_body", context.getString(R.string.low_quality_post_body)));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9906b() {
        FrameLayout frameLayout = (FrameLayout) this.f7407b.findViewById(R.id.popup_info_container);
        if (frameLayout != null) {
            this.f7407b.slideOutAnimation(frameLayout);
        }
    }
}
