package com.becandid.candid.views;

import android.content.Context;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.MessageActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.User;

public class NicknamePopupView extends BlurLayout {
    @BindView(2131624809)
    TextView addNicknameDesc;

    /* renamed from: b */
    MessageActivity f7396b;
    @BindView(2131624810)
    EditText editNickname;
    @BindView(2131624083)
    ImageView mBadgeIcon;
    @BindView(2131624811)
    Button mCancelButton;
    @BindView(2131624789)
    Button mFinishBtn;
    @BindView(2131624807)
    View mMessageContainerAddNickname;
    @BindView(2131624802)
    View mMessageContainerSecondNickname;
    @BindView(2131624812)
    View mNicknameCloseButton;
    @BindView(2131624805)
    Button mNoButton;
    @BindView(2131624482)
    Button mOkButton;
    @BindView(2131624806)
    Button mYesButton;
    @BindView(2131624804)
    TextView secondNicknameDesc;

    public NicknamePopupView(Context context, String str, boolean z) {
        super(context);
        this.f7396b = (MessageActivity) context;
        this.f7290a = str;
        LayoutInflater.from(context).inflate(R.layout.nickname_popup_view, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mMessageContainerSecondNickname, "#ffffff");
        C2699jj.m14618a(this.mMessageContainerAddNickname, "#ffffff");
        this.mNicknameCloseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                NicknamePopupView.this.m9890b();
            }
        });
        if (z) {
            this.f7396b.markNicknameAlertShown();
            User targetUserData = this.f7396b.getTargetUserData();
            User userData = this.f7396b.getUserData();
            if (targetUserData == null || targetUserData.post_name == null || targetUserData.nickname == null) {
                m9890b();
            } else {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(targetUserData.post_name);
                spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 33);
                spannableStringBuilder.append(" is now " + targetUserData.nickname);
                spannableStringBuilder.setSpan(new StyleSpan(1), spannableStringBuilder.length() - targetUserData.nickname.length(), spannableStringBuilder.length(), 33);
                spannableStringBuilder.append(".");
                if (userData.nickname == null) {
                    spannableStringBuilder.append(" Want to add your own nickname?");
                } else {
                    this.mOkButton.setVisibility(0);
                    this.mOkButton.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            NicknamePopupView.this.f7396b.markNicknameAlertShown();
                            NicknamePopupView.this.m9890b();
                        }
                    });
                    this.mYesButton.setVisibility(8);
                    this.mNoButton.setVisibility(8);
                    this.mNicknameCloseButton.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            NicknamePopupView.this.m9890b();
                        }
                    });
                }
                this.secondNicknameDesc.setText(spannableStringBuilder);
            }
            this.mMessageContainerSecondNickname.setVisibility(0);
            this.mMessageContainerAddNickname.setVisibility(8);
            this.mYesButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    NicknamePopupView.this.setupNicknamePrompt();
                }
            });
            this.mNoButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    NicknamePopupView.this.f7396b.ignoreNickname();
                    NicknamePopupView.this.m9890b();
                }
            });
            return;
        }
        setupNicknamePrompt();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9890b() {
        FrameLayout frameLayout = (FrameLayout) this.f7396b.findViewById(R.id.popup_info_container);
        if (frameLayout != null) {
            this.f7396b.slideOutAnimation(frameLayout);
        }
        this.f7396b.closingPopup();
    }

    public void setupNicknamePrompt() {
        this.mMessageContainerAddNickname.setVisibility(0);
        this.mMessageContainerSecondNickname.setVisibility(8);
        this.editNickname.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    NicknamePopupView.this.mFinishBtn.setEnabled(true);
                } else {
                    NicknamePopupView.this.mFinishBtn.setEnabled(false);
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        if (AppState.nickname != null) {
            this.editNickname.setText(AppState.nickname);
            this.editNickname.setSelection(this.editNickname.length());
        } else if (!this.f7396b.keyboardOpen) {
            this.f7396b.openKeyboard();
        }
        this.mFinishBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                NicknamePopupView.this.f7396b.updateNickname(NicknamePopupView.this.editNickname.getText().toString());
                NicknamePopupView.this.m9890b();
            }
        });
        this.mCancelButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                NicknamePopupView.this.f7396b.ignoreNickname();
                NicknamePopupView.this.m9890b();
            }
        });
    }
}
