package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.GroupHeaderHolder;

public class GroupHeaderHolder$$ViewBinder<T extends GroupHeaderHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.GroupHeaderHolder$$ViewBinder$a */
    /* compiled from: GroupHeaderHolder$$ViewBinder */
    public static class C1907a<T extends GroupHeaderHolder> implements Unbinder {

        /* renamed from: a */
        private T f7521a;

        protected C1907a(T t) {
            this.f7521a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10956a(T t) {
            t.groupHeader = null;
            t.groupAbout = null;
            t.groupStats = null;
            t.groupImage = null;
            t.groupMenu = null;
            t.groupJoin = null;
            t.groupLeave = null;
        }

        public final void unbind() {
            if (this.f7521a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10956a(this.f7521a);
            this.f7521a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1907a a = mo10955a(t);
        t.groupHeader = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.group_header, "field 'groupHeader'"), R.id.group_header, "field 'groupHeader'");
        t.groupAbout = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_about, "field 'groupAbout'"), R.id.group_about, "field 'groupAbout'");
        t.groupStats = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_stats, "field 'groupStats'"), R.id.group_stats, "field 'groupStats'");
        t.groupImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.group_image, "field 'groupImage'"), R.id.group_image, "field 'groupImage'");
        t.groupMenu = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.group_menu, "field 'groupMenu'"), R.id.group_menu, "field 'groupMenu'");
        t.groupJoin = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_join, "field 'groupJoin'"), R.id.group_join, "field 'groupJoin'");
        t.groupLeave = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_leave, "field 'groupLeave'"), R.id.group_leave, "field 'groupLeave'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1907a<T> mo10955a(T t) {
        return new C1907a<>(t);
    }
}
