package com.becandid.candid.views.viewholders;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.data.Post;

public class TextPostViewHolder extends BasePostViewHolder {
    @BindView(2131624156)
    TextView mPostCaption;
    @BindView(2131624580)
    LinearLayout mPostCaptionPlaceholder;
    @BindView(2131624557)
    TextView mPostImageCaption;
    @BindView(2131625077)
    TextView mPostImageGroup;
    @BindView(2131624554)
    View mPostShareImage;
    @BindView(2131624579)
    View mPostTextRow;

    public TextPostViewHolder(View view, Activity activity) {
        super(view, activity);
    }

    public TextPostViewHolder(View view, Activity activity, String str) {
        super(view, activity, str);
    }

    /* renamed from: a */
    public static void m9995a(TextPostViewHolder textPostViewHolder, Post post) {
        BasePostViewHolder.m9934a((BasePostViewHolder) textPostViewHolder, post);
        C2699jj.m14618a(textPostViewHolder.mPostTextRow, post.icon_color);
        C2699jj.m14615a(textPostViewHolder.mFooter);
        textPostViewHolder.mPostCaption.setTypeface(FontFactory.m14101a().mo13970a("JosefinSans-SemiBold.ttf"));
    }

    /* renamed from: a */
    public void mo10896a(BasePostViewHolder basePostViewHolder, Post post, int i) {
        super.mo10896a(basePostViewHolder, post, i);
        this.mPostShareImage.setVisibility(4);
        GradientDrawable gradientDrawable = new GradientDrawable();
        if (post.icon_color != null) {
            gradientDrawable.setColor(Color.parseColor(post.icon_color));
        }
        this.mPostShareImage.setBackground(gradientDrawable);
        this.mPostImageCaption.setTypeface(FontFactory.m14101a().mo13970a("JosefinSans-SemiBold.ttf"));
    }
}
