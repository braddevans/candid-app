package com.becandid.candid.views;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.BadgeInfoView;
import com.becandid.candid.views.BlurLayout$$ViewBinder.C1791a;

public class BadgeInfoView$$ViewBinder<T extends BadgeInfoView> extends BlurLayout$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.BadgeInfoView$$ViewBinder$a */
    /* compiled from: BadgeInfoView$$ViewBinder */
    public static class C1789a<T extends BadgeInfoView> extends C1791a<T> {
        protected C1789a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            super.mo10764a(t);
            t.mBadgeIcon = null;
            t.mBadgeTitle = null;
            t.mBadgeDesc = null;
            t.mBadgeContainer = null;
            t.seeAllBadges = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1789a aVar = (C1789a) super.bind(finder, t, obj);
        t.mBadgeIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_icon, "field 'mBadgeIcon'"), R.id.badge_icon, "field 'mBadgeIcon'");
        t.mBadgeTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_title, "field 'mBadgeTitle'"), R.id.badge_title, "field 'mBadgeTitle'");
        t.mBadgeDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_desc, "field 'mBadgeDesc'"), R.id.badge_desc, "field 'mBadgeDesc'");
        t.mBadgeContainer = (View) finder.findRequiredView(obj, R.id.badge_container, "field 'mBadgeContainer'");
        t.seeAllBadges = (View) finder.findRequiredView(obj, R.id.see_all_badges, "field 'seeAllBadges'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1789a<T> mo10762a(T t) {
        return new C1789a<>(t);
    }
}
