package com.becandid.candid.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MainTabViewPager extends TabViewPager {

    /* renamed from: d */
    private boolean f7386d;

    public MainTabViewPager(Context context) {
        super(context);
    }

    public MainTabViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.f7386d && super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.f7386d && super.onTouchEvent(motionEvent);
    }

    public void setSwipeEnabled(boolean z) {
        this.f7386d = z;
    }
}
