package com.becandid.candid.views;

import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.BlurLayout$$ViewBinder.C1791a;
import com.becandid.candid.views.CommunityPopupView;

public class CommunityPopupView$$ViewBinder<T extends CommunityPopupView> extends BlurLayout$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.CommunityPopupView$$ViewBinder$a */
    /* compiled from: CommunityPopupView$$ViewBinder */
    public static class C1808a<T extends CommunityPopupView> extends C1791a<T> {
        protected C1808a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            super.mo10764a(t);
            t.mMessageContainer = null;
            t.mOkBtn = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1808a aVar = (C1808a) super.bind(finder, t, obj);
        t.mMessageContainer = (View) finder.findRequiredView(obj, R.id.message_container_community, "field 'mMessageContainer'");
        t.mOkBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.ok, "field 'mOkBtn'"), R.id.ok, "field 'mOkBtn'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1808a<T> mo10762a(T t) {
        return new C1808a<>(t);
    }
}
