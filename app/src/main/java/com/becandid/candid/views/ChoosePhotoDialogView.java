package com.becandid.candid.views;

import android.app.Activity;
import android.content.Context;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.RecyclerView.Adapter;
import android.support.p003v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import p012rx.schedulers.Schedulers;

public class ChoosePhotoDialogView extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public C1794a f7293a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public ayo f7294b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C1795b f7295c = new C1795b();
    @BindView(2131624399)
    ProgressBar spinny;
    @BindView(2131624398)
    RecyclerView suggestedPhotoView;

    public class PhotoSuggestionHolder extends ViewHolder {
        @BindView(2131624403)
        ImageView imageView;

        public PhotoSuggestionHolder(View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
        }

        /* renamed from: a */
        public void mo10775a(ays ays) {
            if (this.imageView != null) {
                if (ays.mo8409a("thumb_width") && ays.mo8409a("thumb_height")) {
                    LayoutParams layoutParams = (LayoutParams) this.imageView.getLayoutParams();
                    layoutParams.width = (int) (((float) layoutParams.height) * (ays.mo8410b("thumb_width").mo8387e() / ays.mo8410b("thumb_height").mo8387e()));
                    this.imageView.setLayoutParams(layoutParams);
                }
                DrawableRequestBuilder b = GossipApplication.f6095c.mo14650a(ays.mo8410b("thumb_url").mo8385c()).mo14592b();
                if (ays.mo8409a("already_loaded")) {
                    b = b.mo14576h();
                }
                ays.mo8407a("already_loaded", Boolean.valueOf(true));
                b.mo14554a(this.imageView);
            }
        }
    }

    /* renamed from: com.becandid.candid.views.ChoosePhotoDialogView$a */
    public interface C1794a {
        /* renamed from: c */
        void mo10776c();

        void choosePhoto(String str, int i, int i2);

        /* renamed from: d */
        void mo10778d();

        /* renamed from: e */
        void mo10779e();
    }

    /* renamed from: com.becandid.candid.views.ChoosePhotoDialogView$b */
    public class C1795b extends Adapter<PhotoSuggestionHolder> {
        public C1795b() {
        }

        /* renamed from: a */
        public PhotoSuggestionHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new PhotoSuggestionHolder(((Activity) ChoosePhotoDialogView.this.getContext()).getLayoutInflater().inflate(R.layout.choose_photo_suggestion, viewGroup, false));
        }

        /* renamed from: a */
        public void onBindViewHolder(PhotoSuggestionHolder photoSuggestionHolder, int i) {
            photoSuggestionHolder.mo10775a(ChoosePhotoDialogView.this.f7294b.mo8382a(i).mo8398m());
        }

        public int getItemCount() {
            if (ChoosePhotoDialogView.this.f7294b != null) {
                return ChoosePhotoDialogView.this.f7294b.mo8381a();
            }
            return 0;
        }
    }

    public ChoosePhotoDialogView(Context context) {
        super(context);
        setOrientation(1);
        ((Activity) context).getLayoutInflater().inflate(R.layout.choose_photo_dialog, this);
        ButterKnife.bind((View) this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(0);
        this.suggestedPhotoView.setLayoutManager(linearLayoutManager);
        this.suggestedPhotoView.setAdapter(this.f7295c);
        this.suggestedPhotoView.addOnItemTouchListener(new RecyclerItemClickListener(context) {
            public void onItemClick(View view, int i) {
                if (ChoosePhotoDialogView.this.f7293a != null && ChoosePhotoDialogView.this.f7294b != null && i < ChoosePhotoDialogView.this.f7294b.mo8381a()) {
                    ays m = ChoosePhotoDialogView.this.f7294b.mo8382a(i).mo8398m();
                    ChoosePhotoDialogView.this.f7293a.choosePhoto(m.mo8410b("source_url").mo8385c(), m.mo8410b("width").mo8390g(), m.mo8410b("height").mo8390g());
                    ChoosePhotoDialogView.this.f7293a.mo10779e();
                }
            }
        });
    }

    @OnClick({2131624402})
    public void cancel(View view) {
        if (this.f7293a != null) {
            this.f7293a.mo10779e();
        }
    }

    @OnClick({2131624401})
    public void library(View view) {
        if (this.f7293a != null) {
            this.f7293a.mo10778d();
            this.f7293a.mo10779e();
        }
    }

    public void setDelegate(C1794a aVar) {
        this.f7293a = aVar;
    }

    public void setQuery(String str) {
        if (str == null || str.trim().length() == 0) {
            this.suggestedPhotoView.setVisibility(8);
            this.spinny.setVisibility(8);
            return;
        }
        this.suggestedPhotoView.setVisibility(8);
        this.spinny.setVisibility(0);
        ApiService.m14297a().mo14160j(str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            /* renamed from: a */
            public void onNext(ays ays) {
                ChoosePhotoDialogView.this.spinny.setVisibility(8);
                if (!ays.mo8410b("success").mo8391h() || !ays.mo8409a("images") || ays.mo8411c("images").mo8381a() <= 0) {
                    String str = "Unable to suggest images based on your group's name";
                    if (ays.mo8409a("error")) {
                        str = ays.mo8410b("error").mo8385c();
                    }
                    if (str.length() > 0) {
                        Toast.makeText(ChoosePhotoDialogView.this.getContext(), str, 1).show();
                        return;
                    }
                    return;
                }
                ChoosePhotoDialogView.this.suggestedPhotoView.setVisibility(0);
                ChoosePhotoDialogView.this.f7294b = ays.mo8411c("images");
                ChoosePhotoDialogView.this.f7295c.notifyDataSetChanged();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                ChoosePhotoDialogView.this.spinny.setVisibility(8);
                Toast.makeText(ChoosePhotoDialogView.this.getContext(), "Unable to suggest images based on your group's name", 1).show();
            }
        });
    }

    @OnClick({2131624400})
    public void takePhoto(View view) {
        if (this.f7293a != null) {
            this.f7293a.mo10776c();
            this.f7293a.mo10779e();
        }
    }
}
