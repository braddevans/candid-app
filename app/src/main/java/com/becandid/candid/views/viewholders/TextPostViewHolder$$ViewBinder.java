package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.BasePostViewHolder$$ViewBinder.C1898a;
import com.becandid.candid.views.viewholders.TextPostViewHolder;

public class TextPostViewHolder$$ViewBinder<T extends TextPostViewHolder> extends BasePostViewHolder$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.TextPostViewHolder$$ViewBinder$a */
    /* compiled from: TextPostViewHolder$$ViewBinder */
    public static class C1918a<T extends TextPostViewHolder> extends C1898a<T> {
        protected C1918a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10942a(T t) {
            super.mo10942a(t);
            t.mPostCaption = null;
            t.mPostTextRow = null;
            t.mPostCaptionPlaceholder = null;
            t.mPostShareImage = null;
            t.mPostImageGroup = null;
            t.mPostImageCaption = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1918a aVar = (C1918a) super.bind(finder, t, obj);
        t.mPostCaption = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_caption, "field 'mPostCaption'"), R.id.post_caption, "field 'mPostCaption'");
        t.mPostTextRow = (View) finder.findRequiredView(obj, R.id.post_text_row, "field 'mPostTextRow'");
        t.mPostCaptionPlaceholder = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_caption_placeholder, "field 'mPostCaptionPlaceholder'"), R.id.post_caption_placeholder, "field 'mPostCaptionPlaceholder'");
        t.mPostShareImage = (View) finder.findRequiredView(obj, R.id.post_share_image, "field 'mPostShareImage'");
        t.mPostImageGroup = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_image_group, "field 'mPostImageGroup'"), R.id.post_image_group, "field 'mPostImageGroup'");
        t.mPostImageCaption = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_caption_image, "field 'mPostImageCaption'"), R.id.post_caption_image, "field 'mPostImageCaption'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1918a<T> mo10941a(T t) {
        return new C1918a<>(t);
    }
}
