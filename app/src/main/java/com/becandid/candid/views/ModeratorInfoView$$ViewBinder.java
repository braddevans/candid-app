package com.becandid.candid.views;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.BlurLayout$$ViewBinder.C1791a;
import com.becandid.candid.views.ModeratorInfoView;

public class ModeratorInfoView$$ViewBinder<T extends ModeratorInfoView> extends BlurLayout$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.ModeratorInfoView$$ViewBinder$a */
    /* compiled from: ModeratorInfoView$$ViewBinder */
    public static class C1831a<T extends ModeratorInfoView> extends C1791a<T> {
        protected C1831a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            super.mo10764a(t);
            t.mBadgeContainer = null;
            t.mModTitle = null;
            t.mModDesc = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1831a aVar = (C1831a) super.bind(finder, t, obj);
        t.mBadgeContainer = (View) finder.findRequiredView(obj, R.id.badge_container, "field 'mBadgeContainer'");
        t.mModTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_title, "field 'mModTitle'"), R.id.badge_title, "field 'mModTitle'");
        t.mModDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_desc, "field 'mModDesc'"), R.id.badge_desc, "field 'mModDesc'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1831a<T> mo10762a(T t) {
        return new C1831a<>(t);
    }
}
