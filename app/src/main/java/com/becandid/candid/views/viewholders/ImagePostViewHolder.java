package com.becandid.candid.views.viewholders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.FullScreenImageActivity;
import com.becandid.candid.activities.PostDetailsActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Post;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class ImagePostViewHolder extends BasePostViewHolder {

    /* renamed from: a */
    public boolean f7522a;
    @BindView(2131624566)
    ImageView mGifImgPlaceholder;
    @BindView(2131624156)
    TextView mPostCaption;
    @BindView(2131624569)
    View mPostGifPlay;
    @BindView(2131624564)
    ImageView mPostImage;
    @BindView(2131624563)
    View mPostImageContainer;
    @BindView(2131624558)
    ImageView mPostImageImage;
    @BindView(2131624562)
    View mPostImageRow;
    @BindView(2131624565)
    TextView mPostImageUploaded;
    @BindView(2131624568)
    View mPostLoadingImageProgress;
    @BindView(2131624567)
    View mPostLoadingImageProgressContainer;

    public ImagePostViewHolder(View view, Activity activity) {
        super(view, activity);
    }

    public ImagePostViewHolder(View view, Activity activity, String str) {
        super(view, activity, str);
    }

    public ImagePostViewHolder(View view, Activity activity, boolean z) {
        super(view, activity);
        this.f7522a = z;
    }

    /* renamed from: a */
    public static void m9971a(ImagePostViewHolder imagePostViewHolder, Post post) {
        BasePostViewHolder.m9934a((BasePostViewHolder) imagePostViewHolder, post);
        if (post.theme_color == 0) {
            post.theme_color = R.color.gossip_grey;
        }
        imagePostViewHolder.mPostHeaderBadgeScore.setTextColor(post.theme_color);
        imagePostViewHolder.mPostIcon.setTextColor(post.theme_color);
        imagePostViewHolder.mPostGroup.setTextColor(post.theme_color);
        imagePostViewHolder.mFooterCommentIcon.setTextColor(post.theme_color);
        Triangle klVar = new Triangle();
        klVar.mo14475a(post.theme_color);
        klVar.setBounds(0, C2699jj.m14599a(1, imagePostViewHolder.f10906f), C2699jj.m14599a(10, imagePostViewHolder.f10906f), C2699jj.m14599a(6, imagePostViewHolder.f10906f));
        imagePostViewHolder.mPostGroupMenu.setBackground(klVar);
        C2699jj.m14616a(imagePostViewHolder.mPostImageRow, post.theme_color);
        imagePostViewHolder.mPostHeaderBadgeText.setTextColor(post.theme_color);
        Drawable drawable = imagePostViewHolder.f10906f.getResources().getDrawable(R.drawable.dot_circle);
        drawable.setColorFilter(post.theme_color, Mode.SRC_IN);
        imagePostViewHolder.mPostHeaderBadgeDot.setBackground(drawable);
        String str = post.thumb_url != null ? post.thumb_url : post.source_url;
        int i = post.width;
        int i2 = post.height;
        if (str == null || i == 0 || i2 == 0) {
            Crashlytics.m16437a((Throwable) new Exception("thumb_url and source_url are both null"));
            return;
        }
        if (post.actual_height == -1 && post.actual_width == -1) {
            Display defaultDisplay = ((WindowManager) imagePostViewHolder.f10906f.getSystemService("window")).getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            int i3 = point.x - (((int) (5.0f * (((float) imagePostViewHolder.f10906f.getResources().getDisplayMetrics().densityDpi) / 160.0f))) * 2);
            post.actual_height = (int) ((((float) i3) * ((float) i2)) / ((float) i));
            post.actual_width = i3;
        }
        imagePostViewHolder.mPostImage.setLayoutParams(new LayoutParams(post.actual_width, post.actual_height));
        if (post.from_camera == 1) {
            imagePostViewHolder.mPostImageUploaded.setVisibility(0);
        } else {
            imagePostViewHolder.mPostImageUploaded.setVisibility(8);
        }
        if (post.localBitmapPath != null && imagePostViewHolder.f7522a) {
            Options options = new Options();
            options.inPreferredConfig = Config.ARGB_8888;
            imagePostViewHolder.mPostImage.setImageBitmap(BitmapFactory.decodeFile(post.localBitmapPath, options));
        } else if (AppState.config.getInt("android_enable_image_progress") == 1) {
            ImageLoadingUtil.m14521a(imagePostViewHolder.mPostLoadingImageProgress, imagePostViewHolder.mPostLoadingImageProgressContainer, imagePostViewHolder.f10906f, post.actual_width, post.actual_height, Math.min(120, post.actual_height), str, imagePostViewHolder.mPostImage, imagePostViewHolder.mPostGifPlay, post.wait_for_play, false, imagePostViewHolder.mGifImgPlaceholder, post.animated, imagePostViewHolder.mPostImageRow, imagePostViewHolder.mPostIcon, imagePostViewHolder.mPostHeaderBadgeScore, imagePostViewHolder.mPostGroup, imagePostViewHolder.mPostGroupMenu, imagePostViewHolder.mFooterCommentIcon, imagePostViewHolder.mPostHeaderBadgeText, imagePostViewHolder.mPostHeaderBadgeDot, post);
        } else {
            imagePostViewHolder.mPostImage.setBackgroundResource(17301613);
            GossipApplication.f6095c.mo14650a(str).mo14576h().mo14559b(DiskCacheStrategy.SOURCE).mo14554a(imagePostViewHolder.mPostImage);
        }
        final Post post2 = post;
        imagePostViewHolder.mPostImageRow.setOnClickListener(new OnClickListener(imagePostViewHolder) {

            /* renamed from: a */
            final /* synthetic */ ImagePostViewHolder f7523a;

            {
                this.f7523a = r1;
            }

            public void onClick(View view) {
                if (!(this.f7523a.f10906f instanceof PostDetailsActivity)) {
                    PostDetailsActivity.startPostDetailsActivity(post2.post_id, this.f7523a.f10906f, post2.icon_color);
                    return;
                }
                Intent intent = new Intent(view.getContext(), FullScreenImageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("post_id", post2.post_id);
                bundle.putString("source_url", post2.source_url);
                bundle.putInt("num_likes", post2.num_likes);
                bundle.putInt("num_dislikes", post2.num_dislikes);
                bundle.putString("icon_name", post2.icon_name);
                bundle.putString("icon_color", post2.icon_color);
                bundle.putString("user_name", post2.user_name);
                bundle.putInt("like_value", post2.like_value);
                bundle.putInt("num_comments", post2.num_comments);
                bundle.putBoolean("fromDetails", true);
                bundle.putInt("is_rumor", post2.rumor);
                bundle.putInt("num_true", post2.num_true);
                bundle.putInt("num_false", post2.num_false);
                bundle.putInt("opinion_value", post2.opinion_value);
                bundle.putString("share_info_url", post2.share_info.url);
                bundle.putString("share_info_title", post2.share_info.title);
                bundle.putString("share_info_image", post2.share_info.image);
                bundle.putInt("from_camera", post2.from_camera);
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });
        final Post post3 = post;
        imagePostViewHolder.mPostImageRow.setOnLongClickListener(new OnLongClickListener(imagePostViewHolder) {

            /* renamed from: a */
            final /* synthetic */ ImagePostViewHolder f7525a;

            {
                this.f7525a = r1;
            }

            public boolean onLongClick(View view) {
                if (!(this.f7525a.f10906f instanceof PostDetailsActivity)) {
                    Intent intent = new Intent(view.getContext(), FullScreenImageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("post_id", post3.post_id);
                    bundle.putString("source_url", post3.source_url);
                    bundle.putInt("num_likes", post3.num_likes);
                    bundle.putInt("num_dislikes", post3.num_dislikes);
                    bundle.putString("icon_name", post3.icon_name);
                    bundle.putString("icon_color", post3.icon_color);
                    bundle.putString("user_name", post3.user_name);
                    bundle.putInt("like_value", post3.like_value);
                    bundle.putInt("num_comments", post3.num_comments);
                    bundle.putInt("is_rumor", post3.rumor);
                    bundle.putInt("num_true", post3.num_true);
                    bundle.putInt("num_false", post3.num_false);
                    bundle.putInt("opinion_value", post3.opinion_value);
                    bundle.putString("share_info_url", post3.share_info.url);
                    bundle.putString("share_info_title", post3.share_info.title);
                    bundle.putString("share_info_image", post3.share_info.image);
                    bundle.putInt("from_camera", post3.from_camera);
                    intent.putExtras(bundle);
                    view.getContext().startActivity(intent);
                }
                return false;
            }
        });
    }

    /* renamed from: a */
    public void mo10896a(BasePostViewHolder basePostViewHolder, Post post, int i) {
        super.mo10896a(basePostViewHolder, post, i);
        Display defaultDisplay = ((WindowManager) basePostViewHolder.f10906f.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        int i2 = point.x;
        ImagePostViewHolder imagePostViewHolder = (ImagePostViewHolder) basePostViewHolder;
        imagePostViewHolder.mPostImageImage.setImageDrawable(imagePostViewHolder.mPostImage.getDrawable().getCurrent());
        imagePostViewHolder.mPostImageImage.setLayoutParams(new LinearLayout.LayoutParams(i2, post.actual_height));
        imagePostViewHolder.mPostImageImage.setScaleType(ScaleType.FIT_XY);
    }
}
