package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.BasePostViewHolder$$ViewBinder.C1898a;
import com.becandid.candid.views.viewholders.LinkPostViewHolder;

public class LinkPostViewHolder$$ViewBinder<T extends LinkPostViewHolder> extends BasePostViewHolder$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.LinkPostViewHolder$$ViewBinder$a */
    /* compiled from: LinkPostViewHolder$$ViewBinder */
    public static class C1916a<T extends LinkPostViewHolder> extends C1898a<T> {
        protected C1916a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10942a(T t) {
            super.mo10942a(t);
            t.mPostLinkTitle = null;
            t.mPostLinkImage = null;
            t.mPostLinkDesc = null;
            t.mPostLinkDomain = null;
            t.mPostImageRow = null;
            t.mPostCaption = null;
            t.mPostLinkPlacehoder = null;
            t.mPostYoutubePlaceholder = null;
            t.mPostLinkRow = null;
            t.mYoutubeThumb = null;
            t.mYoutubeTitle = null;
            t.mYoutubeDesc = null;
            t.mPostImageImage = null;
            t.mPostLinkTitleImage = null;
            t.mPostLinkDescImage = null;
            t.mPostLinkDomainImage = null;
            t.mPostInfoImage = null;
            t.mPostYoutubeIcon = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1916a aVar = (C1916a) super.bind(finder, t, obj);
        t.mPostLinkTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_title, "field 'mPostLinkTitle'"), R.id.post_link_title, "field 'mPostLinkTitle'");
        t.mPostLinkImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_image, "field 'mPostLinkImage'"), R.id.post_link_image, "field 'mPostLinkImage'");
        t.mPostLinkDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_desc, "field 'mPostLinkDesc'"), R.id.post_link_desc, "field 'mPostLinkDesc'");
        t.mPostLinkDomain = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_domain, "field 'mPostLinkDomain'"), R.id.post_link_domain, "field 'mPostLinkDomain'");
        t.mPostImageRow = (View) finder.findRequiredView(obj, R.id.post_image_row, "field 'mPostImageRow'");
        t.mPostCaption = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_caption, "field 'mPostCaption'"), R.id.post_caption, "field 'mPostCaption'");
        t.mPostLinkPlacehoder = (View) finder.findRequiredView(obj, R.id.link_placeholder, "field 'mPostLinkPlacehoder'");
        t.mPostYoutubePlaceholder = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.youtube_placeholder, "field 'mPostYoutubePlaceholder'"), R.id.youtube_placeholder, "field 'mPostYoutubePlaceholder'");
        t.mPostLinkRow = (View) finder.findRequiredView(obj, R.id.post_link_row, "field 'mPostLinkRow'");
        t.mYoutubeThumb = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.youtube_thumb, "field 'mYoutubeThumb'"), R.id.youtube_thumb, "field 'mYoutubeThumb'");
        t.mYoutubeTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.youtube_title, "field 'mYoutubeTitle'"), R.id.youtube_title, "field 'mYoutubeTitle'");
        t.mYoutubeDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.youtube_desc, "field 'mYoutubeDesc'"), R.id.youtube_desc, "field 'mYoutubeDesc'");
        t.mPostImageImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_image_image, "field 'mPostImageImage'"), R.id.post_image_image, "field 'mPostImageImage'");
        t.mPostLinkTitleImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_title_image, "field 'mPostLinkTitleImage'"), R.id.post_link_title_image, "field 'mPostLinkTitleImage'");
        t.mPostLinkDescImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_desc_image, "field 'mPostLinkDescImage'"), R.id.post_link_desc_image, "field 'mPostLinkDescImage'");
        t.mPostLinkDomainImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_domain_image, "field 'mPostLinkDomainImage'"), R.id.post_link_domain_image, "field 'mPostLinkDomainImage'");
        t.mPostInfoImage = (View) finder.findRequiredView(obj, R.id.post_info_image, "field 'mPostInfoImage'");
        t.mPostYoutubeIcon = (View) finder.findRequiredView(obj, R.id.post_youtube_icon, "field 'mPostYoutubeIcon'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1916a<T> mo10941a(T t) {
        return new C1916a<>(t);
    }
}
