package com.becandid.candid.views;

import android.content.Context;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.util.AttributeSet;
import com.becandid.candid.data.AppState;

public class AutoplayFeedRecyclerView extends RecyclerView {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public LinearLayoutManager f7281a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public FeedAdapter f7282b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public bdq f7283c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public bdu f7284d;

    public AutoplayFeedRecyclerView(Context context) {
        super(context);
        m9784a();
    }

    public AutoplayFeedRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m9784a();
    }

    public AutoplayFeedRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m9784a();
    }

    /* renamed from: a */
    private void m9784a() {
        this.f7281a = new LinearLayoutManager(getContext());
        setLayoutManager(this.f7281a);
    }

    public bdu getItemsPositionGetter() {
        return this.f7284d;
    }

    public LinearLayoutManager getLayoutManager() {
        return this.f7281a;
    }

    public bdq getVideoVisibilityCalculator() {
        return this.f7283c;
    }

    public void setupFeedRecyclerView(FeedAdapter hiVar, LinearLayoutManager linearLayoutManager) {
        this.f7282b = hiVar;
        this.f7281a = linearLayoutManager;
        if (AppState.config.getInt("android_video_messaging_enabled", 1) == 1 && AppState.config.getInt("android_autoplay_video_enabled", 1) == 1) {
            this.f7284d = new bdv(this.f7281a, this);
            this.f7283c = new bdr(new bdp(), this.f7282b.mo13771e());
        }
        addOnScrollListener(new PagingScrollListener(this.f7281a, this.f7282b, 0) {
            public void onLoadMore(String str) {
            }

            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                if (i == 0 && !AutoplayFeedRecyclerView.this.f7282b.mo13771e().isEmpty() && AutoplayFeedRecyclerView.this.f7283c != null && AutoplayFeedRecyclerView.this.f7284d != null) {
                    AutoplayFeedRecyclerView.this.f7283c.mo8844a(AutoplayFeedRecyclerView.this.f7284d, AutoplayFeedRecyclerView.this.f7281a.findFirstVisibleItemPosition(), AutoplayFeedRecyclerView.this.f7281a.findLastVisibleItemPosition());
                }
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                super.onScrolled(recyclerView, i, i2);
                AutoplayFeedRecyclerView.this.post(new Runnable() {
                    public void run() {
                        if (AutoplayFeedRecyclerView.this.f7283c != null && AutoplayFeedRecyclerView.this.f7284d != null) {
                            AutoplayFeedRecyclerView.this.f7283c.mo8844a(AutoplayFeedRecyclerView.this.f7284d, AutoplayFeedRecyclerView.this.f7281a.findFirstVisibleItemPosition(), AutoplayFeedRecyclerView.this.f7281a.findLastVisibleItemPosition());
                        }
                    }
                });
            }
        });
    }
}
