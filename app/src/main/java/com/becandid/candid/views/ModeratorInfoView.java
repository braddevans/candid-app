package com.becandid.candid.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;

public class ModeratorInfoView extends BlurLayout {
    @BindView(2131624079)
    View mBadgeContainer;
    @BindView(2131624081)
    TextView mModDesc;
    @BindView(2131624080)
    TextView mModTitle;

    public ModeratorInfoView(Context context, String str, String str2, String str3) {
        super(context);
        this.f7290a = str;
        LayoutInflater.from(context).inflate(R.layout.moderator_info_view, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mBadgeContainer, "#ffffff");
        this.mModTitle.setText(str2);
        this.mModDesc.setText(str3);
    }
}
