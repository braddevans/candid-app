package com.becandid.candid.views.viewholders.messaging;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.data.User;

public class MessagingHeaderViewHolder extends BaseViewHolder {

    /* renamed from: a */
    public static CandidAnimals f7555a;
    @BindView(2131624294)
    RelativeLayout messagesHeaderIconContainer;
    @BindView(2131624767)
    TextView messagesHeaderName;
    @BindView(2131624296)
    TextView messagesHeaderUserIcon;

    public MessagingHeaderViewHolder(View view) {
        super(view);
        f7555a = CandidAnimals.m14509a();
    }

    /* renamed from: a */
    public static void m10028a(MessagingHeaderViewHolder messagingHeaderViewHolder, User user) {
        int parseColor = Color.parseColor(user.icon_color);
        messagingHeaderViewHolder.messagesHeaderUserIcon.setTextColor(parseColor);
        TextView textView = messagingHeaderViewHolder.messagesHeaderUserIcon;
        CandidAnimals ivVar = f7555a;
        textView.setTypeface(CandidAnimals.m14511b());
        TextView textView2 = messagingHeaderViewHolder.messagesHeaderUserIcon;
        CandidAnimals ivVar2 = f7555a;
        textView2.setText(CandidAnimals.m14510a(user.icon_name));
        int width = messagingHeaderViewHolder.messagesHeaderIconContainer.getWidth();
        if (width == 0) {
            width = (int) (messagingHeaderViewHolder.messagesHeaderIconContainer.getResources().getDisplayMetrics().density * 30.0f);
        }
        int i = (16777215 & parseColor) | 1073741824;
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(i);
        gradientDrawable.setCornerRadius((float) (width / 2));
        messagingHeaderViewHolder.messagesHeaderIconContainer.setBackground(gradientDrawable);
        messagingHeaderViewHolder.messagesHeaderName.setText(user.post_name);
        messagingHeaderViewHolder.messagesHeaderName.setTextColor(parseColor);
    }
}
