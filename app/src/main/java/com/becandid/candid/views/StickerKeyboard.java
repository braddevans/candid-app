package com.becandid.candid.views;

import android.content.Context;
import android.support.p003v7.widget.GridLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.RecyclerView.Adapter;
import android.support.p003v7.widget.RecyclerView.ViewHolder;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.becandid.candid.R;
import java.util.HashMap;

public class StickerKeyboard extends RecyclerView {

    /* renamed from: b */
    static HashMap<String, Integer> f7411b = new HashMap<>();

    /* renamed from: c */
    static String[] f7412c = {"happy", "sad", "shock", "angelic", "angry", "cool", "laugh", "love", "crying", "dead", "devilish", "dizzy", "tears", "uhhh", "upset", "wink", "shy", "worried", "kissright", "kissleft", "photoop", "ghost", "ambition", "grinning", "gross", "hmph", "sneaky", "studious", "hot", "cold", "sick", "ninja", "zombie", "alien", "robot", "luchador", "policeofficer", "fire", "balloons", "heart", "heartbreak", "baseball", "basketball", "soccer", "tennis", "bowling", "iceskate", "burger", "icecream", "pizza", "ramen", "sushi", "cake", "popcorn", "cappuccino", "beer", "wine", "champagne", "hotdrink", "apple", "orange", "redcup", "retrohandheld", "gamecontroller", "laptop", "smartphone", "clock", "tablet", "notebook", "camera_sticker", "present", "books", "teddybear", "backpack", "locker", "schoolbus", "luggage", "lifepreserver", "airplane", "taxi", "tickets", "palmtree", "paintset", "envelope", "shuriken", "trophy", "jersey", "lipstick", "compactmirror", "necklace", "purse", "stiletto", "crown", "diamond", "weddingring", "rose", "mask", "lipprint", "eighthnote", "sun", "moon", "male", "female", "rainbow"};

    /* renamed from: a */
    public C1848a f7413a;

    /* renamed from: com.becandid.candid.views.StickerKeyboard$a */
    public interface C1848a {
        void clickSticker(String str);
    }

    /* renamed from: com.becandid.candid.views.StickerKeyboard$b */
    public class C1849b extends ViewHolder implements OnClickListener {

        /* renamed from: a */
        ImageView f7416a;

        /* renamed from: b */
        String f7417b;

        public C1849b(View view) {
            super(view);
            this.f7416a = (ImageView) view;
            this.f7416a.setOnClickListener(this);
        }

        public void onClick(View view) {
            if (this.f7417b != null) {
                StickerKeyboard.this.f7413a.clickSticker(this.f7417b);
            }
        }
    }

    static {
        f7411b.put("airplane", Integer.valueOf(R.drawable.airplane));
        f7411b.put("alien", Integer.valueOf(R.drawable.alien));
        f7411b.put("ambition", Integer.valueOf(R.drawable.ambition));
        f7411b.put("angelic", Integer.valueOf(R.drawable.angelic));
        f7411b.put("angry", Integer.valueOf(R.drawable.angry));
        f7411b.put("apple", Integer.valueOf(R.drawable.apple));
        f7411b.put("backpack", Integer.valueOf(R.drawable.backpack));
        f7411b.put("balloons", Integer.valueOf(R.drawable.balloons));
        f7411b.put("baseball", Integer.valueOf(R.drawable.baseball));
        f7411b.put("basketball", Integer.valueOf(R.drawable.basketball));
        f7411b.put("beer", Integer.valueOf(R.drawable.beer));
        f7411b.put("books", Integer.valueOf(R.drawable.books));
        f7411b.put("bowling", Integer.valueOf(R.drawable.bowling));
        f7411b.put("burger", Integer.valueOf(R.drawable.burger));
        f7411b.put("cake", Integer.valueOf(R.drawable.cake));
        f7411b.put("camera_sticker", Integer.valueOf(R.drawable.camera_sticker));
        f7411b.put("cappuccino", Integer.valueOf(R.drawable.cappuccino));
        f7411b.put("champagne", Integer.valueOf(R.drawable.champagne));
        f7411b.put("clock", Integer.valueOf(R.drawable.clock));
        f7411b.put("cold", Integer.valueOf(R.drawable.cold));
        f7411b.put("compactmirror", Integer.valueOf(R.drawable.compactmirror));
        f7411b.put("cool", Integer.valueOf(R.drawable.cool));
        f7411b.put("crown", Integer.valueOf(R.drawable.crown));
        f7411b.put("crying", Integer.valueOf(R.drawable.crying));
        f7411b.put("dead", Integer.valueOf(R.drawable.dead));
        f7411b.put("devilish", Integer.valueOf(R.drawable.devilish));
        f7411b.put("diamond", Integer.valueOf(R.drawable.diamond));
        f7411b.put("dizzy", Integer.valueOf(R.drawable.dizzy));
        f7411b.put("eighthnote", Integer.valueOf(R.drawable.eighthnote));
        f7411b.put("envelope", Integer.valueOf(R.drawable.envelope));
        f7411b.put("female", Integer.valueOf(R.drawable.female));
        f7411b.put("fire", Integer.valueOf(R.drawable.fire));
        f7411b.put("gamecontroller", Integer.valueOf(R.drawable.gamecontroller));
        f7411b.put("ghost", Integer.valueOf(R.drawable.ghost));
        f7411b.put("grinning", Integer.valueOf(R.drawable.grinning));
        f7411b.put("gross", Integer.valueOf(R.drawable.gross));
        f7411b.put("happy", Integer.valueOf(R.drawable.happy));
        f7411b.put("heart", Integer.valueOf(R.drawable.heart));
        f7411b.put("heartbreak", Integer.valueOf(R.drawable.heartbreak));
        f7411b.put("hmph", Integer.valueOf(R.drawable.hmph));
        f7411b.put("hot", Integer.valueOf(R.drawable.hot));
        f7411b.put("hotdrink", Integer.valueOf(R.drawable.hotdrink));
        f7411b.put("icecream", Integer.valueOf(R.drawable.icecream));
        f7411b.put("iceskate", Integer.valueOf(R.drawable.iceskate));
        f7411b.put("jersey", Integer.valueOf(R.drawable.jersey));
        f7411b.put("kissleft", Integer.valueOf(R.drawable.kissleft));
        f7411b.put("kissright", Integer.valueOf(R.drawable.kissright));
        f7411b.put("laptop", Integer.valueOf(R.drawable.laptop));
        f7411b.put("laugh", Integer.valueOf(R.drawable.laugh));
        f7411b.put("lifepreserver", Integer.valueOf(R.drawable.lifepreserver));
        f7411b.put("lipprint", Integer.valueOf(R.drawable.lipprint));
        f7411b.put("lipstick", Integer.valueOf(R.drawable.lipstick));
        f7411b.put("locker", Integer.valueOf(R.drawable.locker));
        f7411b.put("love", Integer.valueOf(R.drawable.love));
        f7411b.put("luchador", Integer.valueOf(R.drawable.luchador));
        f7411b.put("luggage", Integer.valueOf(R.drawable.luggage));
        f7411b.put("male", Integer.valueOf(R.drawable.male));
        f7411b.put("mask", Integer.valueOf(R.drawable.mask));
        f7411b.put("moon", Integer.valueOf(R.drawable.moon));
        f7411b.put("necklace", Integer.valueOf(R.drawable.necklace));
        f7411b.put("ninja", Integer.valueOf(R.drawable.ninja));
        f7411b.put("notebook", Integer.valueOf(R.drawable.notebook));
        f7411b.put("orange", Integer.valueOf(R.drawable.orange));
        f7411b.put("paintset", Integer.valueOf(R.drawable.paintset));
        f7411b.put("palmtree", Integer.valueOf(R.drawable.palmtree));
        f7411b.put("photoop", Integer.valueOf(R.drawable.photoop));
        f7411b.put("pizza", Integer.valueOf(R.drawable.pizza));
        f7411b.put("policeofficer", Integer.valueOf(R.drawable.policeofficer));
        f7411b.put("popcorn", Integer.valueOf(R.drawable.popcorn));
        f7411b.put("present", Integer.valueOf(R.drawable.present));
        f7411b.put("purse", Integer.valueOf(R.drawable.purse));
        f7411b.put("rainbow", Integer.valueOf(R.drawable.rainbow));
        f7411b.put("ramen", Integer.valueOf(R.drawable.ramen));
        f7411b.put("redcup", Integer.valueOf(R.drawable.redcup));
        f7411b.put("retrohandheld", Integer.valueOf(R.drawable.retrohandheld));
        f7411b.put("robot", Integer.valueOf(R.drawable.robot));
        f7411b.put("rose", Integer.valueOf(R.drawable.rose));
        f7411b.put("sad", Integer.valueOf(R.drawable.sad));
        f7411b.put("schoolbus", Integer.valueOf(R.drawable.schoolbus));
        f7411b.put("shock", Integer.valueOf(R.drawable.shock));
        f7411b.put("shuriken", Integer.valueOf(R.drawable.shuriken));
        f7411b.put("shy", Integer.valueOf(R.drawable.shy));
        f7411b.put("sick", Integer.valueOf(R.drawable.sick));
        f7411b.put("smartphone", Integer.valueOf(R.drawable.smartphone));
        f7411b.put("sneaky", Integer.valueOf(R.drawable.sneaky));
        f7411b.put("soccer", Integer.valueOf(R.drawable.soccer));
        f7411b.put("stiletto", Integer.valueOf(R.drawable.stiletto));
        f7411b.put("studious", Integer.valueOf(R.drawable.studious));
        f7411b.put("sun", Integer.valueOf(R.drawable.sun));
        f7411b.put("sushi", Integer.valueOf(R.drawable.sushi));
        f7411b.put("tablet", Integer.valueOf(R.drawable.tablet));
        f7411b.put("taxi", Integer.valueOf(R.drawable.taxi));
        f7411b.put("tears", Integer.valueOf(R.drawable.tears));
        f7411b.put("teddybear", Integer.valueOf(R.drawable.teddybear));
        f7411b.put("tennis", Integer.valueOf(R.drawable.tennis));
        f7411b.put("tickets", Integer.valueOf(R.drawable.tickets));
        f7411b.put("trophy", Integer.valueOf(R.drawable.trophy));
        f7411b.put("uhhh", Integer.valueOf(R.drawable.uhhh));
        f7411b.put("upset", Integer.valueOf(R.drawable.upset));
        f7411b.put("weddingring", Integer.valueOf(R.drawable.weddingring));
        f7411b.put("wine", Integer.valueOf(R.drawable.wine));
        f7411b.put("wink", Integer.valueOf(R.drawable.wink));
        f7411b.put("worried", Integer.valueOf(R.drawable.worried));
        f7411b.put("zombie", Integer.valueOf(R.drawable.zombie));
    }

    public StickerKeyboard(Context context) {
        super(context);
        m9919a();
    }

    public StickerKeyboard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m9919a();
    }

    public StickerKeyboard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m9919a();
    }

    /* renamed from: a */
    private void m9919a() {
        setLayoutManager(new GridLayoutManager(getContext(), 2, 0, false));
        final int length = f7412c.length;
        setAdapter(new Adapter<C1849b>() {
            /* renamed from: a */
            public C1849b onCreateViewHolder(ViewGroup viewGroup, int i) {
                return new C1849b(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sticker_item, viewGroup, false));
            }

            /* renamed from: a */
            public void onBindViewHolder(C1849b bVar, int i) {
                String str = StickerKeyboard.f7412c[i];
                if (!str.equals(bVar.f7417b)) {
                    bVar.f7417b = str;
                    bVar.f7416a.setImageResource(((Integer) StickerKeyboard.f7411b.get(str)).intValue());
                }
            }

            public int getItemCount() {
                return length;
            }
        });
    }

    public boolean fling(int i, int i2) {
        return super.fling((int) (((double) i) * 0.8d), i2);
    }
}
