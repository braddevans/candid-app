package com.becandid.candid.views.viewholders;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.p001v4.app.ActivityCompat;
import android.support.p003v7.app.AlertDialog;
import android.support.p003v7.app.AlertDialog.Builder;
import android.support.p003v7.widget.PopupMenu;
import android.support.p003v7.widget.PopupMenu.OnMenuItemClickListener;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.CommunitySettingsActivity;
import com.becandid.candid.activities.GroupDetailsActivity;
import com.becandid.candid.activities.MessageActivity;
import com.becandid.candid.activities.PostDetailsActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.EmptyClass;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.Post;
import com.becandid.candid.models.NetworkData;
import com.becandid.thirdparty.BlurTask;
import com.becandid.thirdparty.BlurTask.BadgeType;
import com.facebook.FacebookException;
import com.facebook.share.model.SharePhoto.C2077a;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.SharePhotoContent.C2079a;
import com.facebook.share.widget.ShareDialog;
import java.text.DecimalFormat;
import java.util.Iterator;
import p012rx.schedulers.Schedulers;

public abstract class BasePostViewHolder extends BaseViewHolder {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static String f7427a;
    @BindView(2131625033)
    LinearLayout mCommentPostBadge;
    @BindView(2131625075)
    public TextView mCommentPostBtnOgImage;
    @BindView(2131625031)
    LinearLayout mCommentPostContainer;
    @BindView(2131625034)
    TextView mCommentPostText;
    @BindView(2131625071)
    View mFalseRumorContainerOgImage;
    @BindView(2131625072)
    ImageView mFalseRumorIconOgImage;
    @BindView(2131625073)
    public TextView mFalseRumorTextOgImage;
    @BindView(2131624326)
    View mFooter;
    @BindView(2131625042)
    TextView mFooterCommentIcon;
    @BindView(2131625045)
    ImageView mFooterCommentImage;
    @BindView(2131625043)
    ImageView mFooterCommentModIcon;
    @BindView(2131625039)
    View mFooterCommentSnippet;
    @BindView(2131625046)
    TextView mFooterCommentText;
    @BindView(2131625044)
    TextView mFooterCommentTimestamp;
    @BindView(2131625040)
    TextView mFooterCommentValue;
    @BindView(2131624323)
    View mHeader;
    @BindView(2131625065)
    public TextView mLikePostBtnOgImage;
    @BindView(2131625023)
    LinearLayout mLikeRumorPostBadge;
    @BindView(2131625019)
    LinearLayout mLikeRumorPostContainer;
    @BindView(2131625021)
    ImageView mLikeRumorPostIcon;
    @BindView(2131625024)
    TextView mLikeRumorPostText;
    @BindView(2131625037)
    LinearLayout mMessagePostContainer;
    @BindView(2131625051)
    public TextView mPostAuthor;
    @BindView(2131624156)
    TextView mPostCaption;
    @BindView(2131624557)
    TextView mPostCaptionOgImage;
    @BindView(2131624561)
    LinearLayout mPostContainer;
    @BindView(2131625061)
    public TextView mPostGroup;
    @BindView(2131625058)
    public FrameLayout mPostGroupContainer;
    @BindView(2131625060)
    public View mPostGroupMenu;
    @BindView(2131625059)
    RelativeLayout mPostGroupMenuContainer;
    @BindView(2131625077)
    TextView mPostGroupOgImage;
    @BindView(2131625052)
    View mPostHeaderBadge;
    @BindView(2131625054)
    FrameLayout mPostHeaderBadgeDot;
    @BindView(2131625055)
    TextView mPostHeaderBadgeScore;
    @BindView(2131625053)
    TextView mPostHeaderBadgeText;
    @BindView(2131625048)
    public TextView mPostIcon;
    @BindView(2131624559)
    View mPostImageFooter;
    @BindView(2131625049)
    public ImageView mPostModIcon;
    @BindView(2131625050)
    View mPostPopupAction;
    @BindView(2131625085)
    LinearLayout mPostRelated;
    @BindView(2131624560)
    View mPostRelatedBar;
    @BindView(2131625088)
    LinearLayout mPostRelatedBottom;
    @BindView(2131625089)
    View mPostRumorAlert;
    @BindView(2131625090)
    ImageView mPostRumorAlertDrawable;
    @BindView(2131625091)
    TextView mPostRumorAlertText;
    @BindView(2131624554)
    View mPostShareOgImage;
    @BindView(2131625056)
    public TextView mPostTimestamp;
    @BindView(2131625092)
    View mPostTrending;
    @BindView(2131625093)
    ImageView mPostTrendingDrawable;
    @BindView(2131625094)
    TextView mPostTrendingText;
    @BindView(2131624101)
    ProgressBar mProgressBar;
    @BindView(2131625028)
    TextView mRumorFalseText;
    @BindView(2131625022)
    TextView mRumorTrueText;
    @BindView(2131625035)
    FrameLayout mShareButton;
    @BindView(2131625036)
    ImageView mShareIcon;
    @BindView(2131625066)
    View mTrueRumorContainerOgImage;
    @BindView(2131625067)
    ImageView mTrueRumorIconOgImage;
    @BindView(2131625068)
    public TextView mTrueRumorTextOgImage;
    @BindView(2131625070)
    public TextView mUnlikePostBtnOgImage;
    @BindView(2131625029)
    LinearLayout mUnlikeRumorPostBadge;
    @BindView(2131625025)
    LinearLayout mUnlikeRumorPostContainer;
    @BindView(2131625027)
    ImageView mUnlikeRumorPostIcon;
    @BindView(2131625030)
    TextView mUnlikeRumorPostText;
    @BindView(2131625057)
    ImageView reportFlag;

    public BasePostViewHolder(View view, Activity activity) {
        super(view, activity);
    }

    public BasePostViewHolder(View view, Activity activity, String str) {
        super(view, activity);
        f7427a = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9927a(int i, int i2, int i3, final Activity activity) {
        ApiService.m14297a().mo14121b(Integer.toString(i), Integer.toString(i2), Integer.toString(i3)).mo9256b(Schedulers.m16145io()).mo9258b((bjy<? super T>) new bjy<Post>() {
            /* renamed from: a */
            public void onNext(Post post) {
                RxBus.m14573a().mo14349a(new C2663t(post, activity));
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9928a(ImageView imageView, int i, TextView textView, boolean z) {
        if (z) {
            imageView.setColorFilter(Color.parseColor("#00FF00"));
            textView.setText(Integer.toString(i + 1));
            return;
        }
        imageView.setColorFilter(Color.parseColor("#FFFFFF"));
        textView.setText(Integer.toString(i - 1));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9929a(ImageView imageView, TextView textView, Context context, boolean z) {
        int parseInt = Integer.parseInt(textView.getText().toString());
        if (z) {
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_active));
            textView.setText(Integer.toString(parseInt + 1));
            return;
        }
        imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_inactive));
        textView.setText(Integer.toString(parseInt - 1));
    }

    /* JADX WARNING: type inference failed for: r0v146, types: [android.text.SpannableString] */
    /* JADX WARNING: type inference failed for: r25v1, types: [android.text.Spannable, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v37, types: [java.lang.CharSequence] */
    /* JADX WARNING: type inference failed for: r0v177, types: [android.text.Spannable] */
    /* JADX WARNING: type inference failed for: r0v199, types: [android.text.Spannable] */
    /* JADX WARNING: type inference failed for: r0v213, types: [android.text.Spannable] */
    /* JADX WARNING: type inference failed for: r25v2, types: [android.text.Spannable, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v57, types: [java.lang.CharSequence] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v146, types: [android.text.SpannableString]
  assigns: [android.text.SpannableString, android.text.Spannable]
  uses: [android.text.SpannableString, android.text.Spannable, ?[OBJECT, ARRAY], java.lang.Object]
  mth insns count: 852
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 6 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m9934a(com.becandid.candid.views.viewholders.BasePostViewHolder r35, com.becandid.candid.data.Post r36) {
        /*
            r0 = r35
            r1 = r36
            r2 = r35
            r0.m9943d(r1, r2)
            r0 = r35
            r1 = r36
            r2 = r35
            r0.m9939b(r1, r2)
            r0 = r35
            r1 = r36
            r2 = r35
            r0.m9942c(r1, r2)
            r0 = r35
            r1 = r36
            r2 = r35
            r0.mo10895a(r1, r2)
            android.os.Handler r14 = new android.os.Handler
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r31 = r0
            android.os.Looper r31 = r31.getMainLooper()
            r0 = r31
            r14.<init>(r0)
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r31 = r0
            double r22 = p000.C2699jj.m14623b(r31)
            r0 = r36
            int r0 = r0.rumor
            r31 = r0
            r32 = 1
            r0 = r31
            r1 = r32
            if (r0 != r1) goto L_0x04c5
            r0 = r35
            android.widget.ImageView r0 = r0.mLikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838242(0x7f0202e2, float:1.728146E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
            r0 = r35
            android.widget.ImageView r0 = r0.mUnlikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838241(0x7f0202e1, float:1.7281459E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
            r0 = r35
            android.widget.TextView r0 = r0.mRumorTrueText
            r31 = r0
            r32 = 0
            r31.setVisibility(r32)
            r0 = r35
            android.widget.TextView r0 = r0.mRumorFalseText
            r31 = r0
            r32 = 0
            r31.setVisibility(r32)
            r32 = 4616752568008179712(0x4012000000000000, double:4.5)
            int r31 = (r22 > r32 ? 1 : (r22 == r32 ? 0 : -1))
            if (r31 <= 0) goto L_0x03c5
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 53
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 0
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r31 = 16
            r0 = r31
            r1 = r20
            r1.gravity = r0
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 58
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 0
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r31 = 16
            r0 = r31
            r1 = r20
            r1.gravity = r0
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r32 = 0
            r31.setBackground(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r32 = 0
            r31.setBackground(r32)
        L_0x0139:
            r0 = r36
            int r0 = r0.opinion_value
            r19 = r0
            java.lang.String r31 = "#FFFFFF"
            int r30 = android.graphics.Color.parseColor(r31)
            java.lang.String r31 = "#FF0000"
            int r21 = android.graphics.Color.parseColor(r31)
            java.lang.String r31 = "#00FF00"
            int r10 = android.graphics.Color.parseColor(r31)
            r31 = 1
            r0 = r19
            r1 = r31
            if (r0 != r1) goto L_0x0485
            r0 = r35
            android.widget.ImageView r0 = r0.mLikeRumorPostIcon
            r31 = r0
            r0 = r31
            r0.setColorFilter(r10)
            r0 = r35
            android.widget.ImageView r0 = r0.mUnlikeRumorPostIcon
            r31 = r0
            r0 = r31
            r1 = r30
            r0.setColorFilter(r1)
        L_0x0171:
            r0 = r35
            android.widget.TextView r0 = r0.mLikeRumorPostText
            r31 = r0
            r0 = r36
            int r0 = r0.num_true
            r32 = r0
            java.lang.String r32 = java.lang.Integer.toString(r32)
            r31.setText(r32)
            r0 = r35
            android.widget.TextView r0 = r0.mUnlikeRumorPostText
            r31 = r0
            r0 = r36
            int r0 = r0.num_false
            r32 = r0
            java.lang.String r32 = java.lang.Integer.toString(r32)
            r31.setText(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostContainer
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$1 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$1
            r0 = r32
            r1 = r36
            r2 = r35
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostContainer
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$12 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$12
            r0 = r32
            r1 = r36
            r2 = r35
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
        L_0x01bf:
            r32 = 4616752568008179712(0x4012000000000000, double:4.5)
            int r31 = (r22 > r32 ? 1 : (r22 == r32 ? 0 : -1))
            if (r31 <= 0) goto L_0x0757
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 28
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 0
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r31 = 16
            r0 = r31
            r1 = r20
            r1.gravity = r0
            r0 = r35
            android.widget.LinearLayout r0 = r0.mCommentPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mCommentPostBadge
            r31 = r0
            r32 = 0
            r31.setBackground(r32)
        L_0x0213:
            r0 = r36
            int r0 = r0.related_to_post
            r31 = r0
            if (r31 <= 0) goto L_0x07f6
            r0 = r35
            android.view.View r0 = r0.mPostRelatedBar
            r31 = r0
            r32 = 0
            r31.setVisibility(r32)
            r0 = r36
            int r0 = r0.first_related_post
            r31 = r0
            r32 = 1
            r0 = r31
            r1 = r32
            if (r0 != r1) goto L_0x07b8
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelated
            r31 = r0
            r32 = 0
            r31.setVisibility(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelatedBottom
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
        L_0x024a:
            r0 = r35
            android.widget.LinearLayout r0 = r0.mCommentPostContainer
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$20 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$20
            r0 = r32
            r1 = r35
            r2 = r36
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
            r0 = r35
            android.widget.FrameLayout r0 = r0.mShareButton
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$21 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$21
            r0 = r32
            r1 = r35
            r2 = r36
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
            r0 = r36
            int r0 = r0.thats_me
            r31 = r0
            r32 = 1
            r0 = r31
            r1 = r32
            if (r0 == r1) goto L_0x0819
            com.becandid.candid.data.Config r31 = com.becandid.candid.data.AppState.config
            java.lang.String r32 = "android_disable_post_message_button"
            r33 = 0
            int r31 = r31.getInt(r32, r33)
            if (r31 != 0) goto L_0x0819
            r0 = r35
            android.widget.LinearLayout r0 = r0.mMessagePostContainer
            r31 = r0
            r32 = 0
            r31.setVisibility(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mMessagePostContainer
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$22 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$22
            r0 = r32
            r1 = r36
            r2 = r35
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
        L_0x02ab:
            r0 = r35
            android.view.View r0 = r0.mPostPopupAction
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$23 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$23
            r0 = r32
            r1 = r35
            r2 = r36
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
            com.becandid.candid.data.Config r31 = com.becandid.candid.data.AppState.config
            java.lang.String r32 = "disable_group_tagging"
            r33 = 0
            int r7 = r31.getInt(r32, r33)
            r0 = r36
            java.lang.String r0 = r0.caption
            r29 = r0
            r24 = 0
            if (r29 == 0) goto L_0x09c0
            android.text.SpannableString r25 = new android.text.SpannableString
            r0 = r25
            r1 = r29
            r0.<init>(r1)
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r31 = r0
            r0 = r31
            boolean r0 = r0 instanceof com.becandid.candid.activities.PostDetailsActivity
            r31 = r0
            if (r31 != 0) goto L_0x033b
            java.util.HashSet<java.lang.Integer> r31 = com.becandid.candid.data.AppState.expandedPostIds
            if (r31 == 0) goto L_0x033b
            java.util.HashSet<java.lang.Integer> r31 = com.becandid.candid.data.AppState.expandedPostIds
            r0 = r36
            int r0 = r0.post_id
            r32 = r0
            java.lang.Integer r32 = java.lang.Integer.valueOf(r32)
            boolean r31 = r31.contains(r32)
            if (r31 != 0) goto L_0x033b
            java.lang.String r31 = "#99FFFFFF"
            int r31 = android.graphics.Color.parseColor(r31)
            r0 = r29
            r1 = r31
            android.text.Spannable r25 = p000.C2699jj.m14602a(r0, r1)
            java.lang.String r31 = r25.toString()
            java.lang.String r32 = "See More"
            boolean r31 = r31.endsWith(r32)
            if (r31 == 0) goto L_0x033b
            r24 = 1
            r0 = r35
            android.widget.TextView r0 = r0.mPostCaption
            r31 = r0
            r0 = r31
            r1 = r25
            r0.setText(r1)
            r0 = r35
            android.widget.TextView r0 = r0.mPostCaption
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$24 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$24
            r0 = r32
            r1 = r36
            r0.<init>(r14, r1)
            r31.setOnClickListener(r32)
        L_0x033b:
            java.util.ArrayList r28 = new java.util.ArrayList
            r28.<init>()
            if (r7 != 0) goto L_0x0378
            r0 = r36
            java.lang.String r0 = r0.mentioned_groups_info
            r31 = r0
            if (r31 == 0) goto L_0x0378
            r0 = r36
            java.lang.String r0 = r0.mentioned_groups_info
            r31 = r0
            java.lang.String r32 = ";"
            java.lang.String[] r13 = r31.split(r32)
            int r0 = r13.length
            r32 = r0
            r31 = 0
        L_0x035b:
            r0 = r31
            r1 = r32
            if (r0 >= r1) goto L_0x0378
            r11 = r13[r31]
            java.lang.String r33 = ","
            r0 = r33
            java.lang.String[] r16 = r11.split(r0)
            r0 = r16
            int r0 = r0.length
            r33 = r0
            r34 = 3
            r0 = r33
            r1 = r34
            if (r0 == r1) goto L_0x0826
        L_0x0378:
            r31 = 0
            int r32 = r28.size()
            int r32 = r32 + -1
            r0 = r28
            r1 = r31
            r2 = r32
            com.becandid.candid.data.DataUtil.sortTextTags(r0, r1, r2)
            r15 = 0
        L_0x038a:
            boolean r31 = r28.isEmpty()
            if (r31 != 0) goto L_0x03b8
            r31 = 0
            r0 = r28
            r1 = r31
            java.lang.Object r6 = r0.remove(r1)
            com.becandid.candid.data.TextTag r6 = (com.becandid.candid.data.TextTag) r6
            int r0 = r6.startIndex
            r31 = r0
            int r32 = r25.length()
            r0 = r31
            r1 = r32
            if (r0 >= r1) goto L_0x03b8
            int r0 = r6.endIndex
            r31 = r0
            int r32 = r25.length()
            r0 = r31
            r1 = r32
            if (r0 <= r1) goto L_0x0852
        L_0x03b8:
            r26 = r15
        L_0x03ba:
            int r31 = r25.length()
            r0 = r31
            if (r15 >= r0) goto L_0x0939
            int r15 = r15 + 1
            goto L_0x03ba
        L_0x03c5:
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 48
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 8
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r33 = r0
            int r32 = p000.C2699jj.m14599a(r32, r33)
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 53
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 8
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r33 = r0
            int r32 = p000.C2699jj.m14599a(r32, r33)
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838013(0x7f0201fd, float:1.7280996E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setBackground(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838013(0x7f0201fd, float:1.7280996E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setBackground(r32)
            goto L_0x0139
        L_0x0485:
            r31 = -1
            r0 = r19
            r1 = r31
            if (r0 != r1) goto L_0x04a9
            r0 = r35
            android.widget.ImageView r0 = r0.mLikeRumorPostIcon
            r31 = r0
            r0 = r31
            r1 = r30
            r0.setColorFilter(r1)
            r0 = r35
            android.widget.ImageView r0 = r0.mUnlikeRumorPostIcon
            r31 = r0
            r0 = r31
            r1 = r21
            r0.setColorFilter(r1)
            goto L_0x0171
        L_0x04a9:
            r0 = r35
            android.widget.ImageView r0 = r0.mLikeRumorPostIcon
            r31 = r0
            r0 = r31
            r1 = r30
            r0.setColorFilter(r1)
            r0 = r35
            android.widget.ImageView r0 = r0.mUnlikeRumorPostIcon
            r31 = r0
            r0 = r31
            r1 = r30
            r0.setColorFilter(r1)
            goto L_0x0171
        L_0x04c5:
            r0 = r35
            android.widget.TextView r0 = r0.mRumorTrueText
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            r0 = r35
            android.widget.TextView r0 = r0.mRumorFalseText
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            android.widget.LinearLayout$LayoutParams r18 = new android.widget.LinearLayout$LayoutParams
            r31 = 0
            r32 = -1
            r33 = 1065353216(0x3f800000, float:1.0)
            r0 = r18
            r1 = r31
            r2 = r32
            r3 = r33
            r0.<init>(r1, r2, r3)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostContainer
            r31 = r0
            r0 = r31
            r1 = r18
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostContainer
            r31 = r0
            r0 = r31
            r1 = r18
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mCommentPostContainer
            r31 = r0
            r0 = r31
            r1 = r18
            r0.setLayoutParams(r1)
            r32 = 4616752568008179712(0x4012000000000000, double:4.5)
            int r31 = (r22 > r32 ? 1 : (r22 == r32 ? 0 : -1))
            if (r31 <= 0) goto L_0x0623
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 28
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 0
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r31 = 16
            r0 = r31
            r1 = r20
            r1.gravity = r0
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 30
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 0
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r31 = 16
            r0 = r31
            r1 = r20
            r1.gravity = r0
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r32 = 0
            r31.setBackground(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r32 = 0
            r31.setBackground(r32)
        L_0x05b7:
            r0 = r36
            int r0 = r0.like_value
            r17 = r0
            r31 = 1
            r0 = r17
            r1 = r31
            if (r0 != r1) goto L_0x06e3
            r0 = r35
            android.widget.ImageView r0 = r0.mLikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838029(0x7f02020d, float:1.7281029E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
            r0 = r35
            android.widget.ImageView r0 = r0.mUnlikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838031(0x7f02020f, float:1.7281033E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
        L_0x05f9:
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostContainer
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$18 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$18
            r0 = r32
            r1 = r36
            r2 = r35
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostContainer
            r31 = r0
            com.becandid.candid.views.viewholders.BasePostViewHolder$19 r32 = new com.becandid.candid.views.viewholders.BasePostViewHolder$19
            r0 = r32
            r1 = r36
            r2 = r35
            r0.<init>(r1, r2)
            r31.setOnClickListener(r32)
            goto L_0x01bf
        L_0x0623:
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 22
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 8
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r33 = r0
            int r32 = p000.C2699jj.m14599a(r32, r33)
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 24
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 8
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r33 = r0
            int r32 = p000.C2699jj.m14599a(r32, r33)
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mLikeRumorPostBadge
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838013(0x7f0201fd, float:1.7280996E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setBackground(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mUnlikeRumorPostBadge
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838013(0x7f0201fd, float:1.7280996E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setBackground(r32)
            goto L_0x05b7
        L_0x06e3:
            r31 = -1
            r0 = r17
            r1 = r31
            if (r0 != r1) goto L_0x0721
            r0 = r35
            android.widget.ImageView r0 = r0.mLikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838033(0x7f020211, float:1.7281037E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
            r0 = r35
            android.widget.ImageView r0 = r0.mUnlikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838030(0x7f02020e, float:1.728103E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
            goto L_0x05f9
        L_0x0721:
            r0 = r35
            android.widget.ImageView r0 = r0.mLikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838033(0x7f020211, float:1.7281037E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
            r0 = r35
            android.widget.ImageView r0 = r0.mUnlikeRumorPostIcon
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838031(0x7f02020f, float:1.7281033E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setImageDrawable(r32)
            goto L_0x05f9
        L_0x0757:
            android.widget.FrameLayout$LayoutParams r20 = new android.widget.FrameLayout$LayoutParams
            r31 = -2
            r32 = -2
            r0 = r20
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)
            r31 = 22
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            int r31 = p000.C2699jj.m14599a(r31, r32)
            r32 = 8
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r33 = r0
            int r32 = p000.C2699jj.m14599a(r32, r33)
            r33 = 0
            r34 = 0
            r0 = r20
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setMargins(r1, r2, r3, r4)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mCommentPostBadge
            r31 = r0
            r0 = r31
            r1 = r20
            r0.setLayoutParams(r1)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mCommentPostBadge
            r31 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r32 = r0
            android.content.res.Resources r32 = r32.getResources()
            r33 = 2130838013(0x7f0201fd, float:1.7280996E38)
            android.graphics.drawable.Drawable r32 = r32.getDrawable(r33)
            r31.setBackground(r32)
            goto L_0x0213
        L_0x07b8:
            r0 = r36
            int r0 = r0.last_related_post
            r31 = r0
            r32 = 1
            r0 = r31
            r1 = r32
            if (r0 != r1) goto L_0x07de
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelated
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelatedBottom
            r31 = r0
            r32 = 0
            r31.setVisibility(r32)
            goto L_0x024a
        L_0x07de:
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelated
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelatedBottom
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            goto L_0x024a
        L_0x07f6:
            r0 = r35
            android.view.View r0 = r0.mPostRelatedBar
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelated
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            r0 = r35
            android.widget.LinearLayout r0 = r0.mPostRelatedBottom
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            goto L_0x024a
        L_0x0819:
            r0 = r35
            android.widget.LinearLayout r0 = r0.mMessagePostContainer
            r31 = r0
            r32 = 8
            r31.setVisibility(r32)
            goto L_0x02ab
        L_0x0826:
            r33 = 0
            r33 = r16[r33]
            int r12 = java.lang.Integer.parseInt(r33)
            r33 = 1
            r33 = r16[r33]
            int r27 = java.lang.Integer.parseInt(r33)
            r33 = 2
            r33 = r16[r33]
            int r9 = java.lang.Integer.parseInt(r33)
            com.becandid.candid.data.TextTag r33 = new com.becandid.candid.data.TextTag
            r0 = r33
            r1 = r27
            r0.<init>(r1, r9, r12)
            r0 = r28
            r1 = r33
            r0.add(r1)
            int r31 = r31 + 1
            goto L_0x035b
        L_0x0852:
            int r0 = r6.startIndex
            r31 = r0
            if (r31 < 0) goto L_0x03b8
            int r0 = r6.endIndex
            r31 = r0
            if (r31 <= 0) goto L_0x03b8
            java.lang.String r31 = r25.toString()
            java.lang.String r32 = "See More"
            boolean r31 = r31.endsWith(r32)
            if (r31 == 0) goto L_0x0878
            int r0 = r6.endIndex
            r31 = r0
            int r32 = r25.length()
            r0 = r31
            r1 = r32
            if (r0 > r1) goto L_0x03b8
        L_0x0878:
            r26 = r15
            int r31 = r25.length()
            r0 = r26
            r1 = r31
            if (r0 > r1) goto L_0x03b8
        L_0x0884:
            int r0 = r6.startIndex
            r31 = r0
            r0 = r31
            if (r15 >= r0) goto L_0x088f
            int r15 = r15 + 1
            goto L_0x0884
        L_0x088f:
            int r31 = r25.length()
            r0 = r31
            if (r15 <= r0) goto L_0x089b
            int r15 = r25.length()
        L_0x089b:
            r31 = -1
            r0 = r31
            if (r15 <= r0) goto L_0x08ec
            r31 = -1
            r0 = r26
            r1 = r31
            if (r0 <= r1) goto L_0x08ec
            int r31 = r15 - r26
            if (r31 <= 0) goto L_0x08ec
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r31 = r0
            r0 = r31
            boolean r0 = r0 instanceof com.becandid.candid.activities.PostDetailsActivity
            r31 = r0
            if (r31 != 0) goto L_0x08ec
            if (r24 != 0) goto L_0x08ec
            java.lang.String r31 = "#ffffff"
            r0 = r36
            int r0 = r0.post_id
            r32 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r33 = r0
            r0 = r36
            java.lang.String r0 = r0.icon_color
            r34 = r0
            r0 = r31
            r1 = r32
            r2 = r33
            r3 = r24
            r4 = r34
            android.text.style.ClickableSpan r31 = p000.C2699jj.m14605a(r0, r1, r2, r3, r4)
            r32 = 33
            r0 = r25
            r1 = r31
            r2 = r26
            r3 = r32
            r0.setSpan(r1, r2, r15, r3)
        L_0x08ec:
            int r0 = r6.startIndex     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            r31 = r0
            r32 = -1
            r0 = r31
            r1 = r32
            if (r0 <= r1) goto L_0x038a
            int r0 = r6.endIndex     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            r31 = r0
            r32 = -1
            r0 = r31
            r1 = r32
            if (r0 <= r1) goto L_0x038a
            java.lang.String r31 = "#ffffff"
            int r0 = r6.groupId     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            r32 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            r33 = r0
            android.text.style.ClickableSpan r31 = p000.C2699jj.m14604a(r31, r32, r33)     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            int r0 = r6.startIndex     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            r32 = r0
            int r0 = r6.endIndex     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            r33 = r0
            r34 = 33
            r0 = r25
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r0.setSpan(r1, r2, r3, r4)     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            int r0 = r6.endIndex     // Catch:{ IndexOutOfBoundsException -> 0x0933 }
            r31 = r0
            int r15 = r31 + 1
            goto L_0x038a
        L_0x0933:
            r8 = move-exception
            p000.Crashlytics.m16437a(r8)
            goto L_0x038a
        L_0x0939:
            r31 = -1
            r0 = r31
            if (r15 <= r0) goto L_0x098a
            r31 = -1
            r0 = r26
            r1 = r31
            if (r0 <= r1) goto L_0x098a
            int r31 = r15 - r26
            if (r31 <= 0) goto L_0x098a
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r31 = r0
            r0 = r31
            boolean r0 = r0 instanceof com.becandid.candid.activities.PostDetailsActivity
            r31 = r0
            if (r31 != 0) goto L_0x098a
            if (r24 != 0) goto L_0x098a
            java.lang.String r31 = "#ffffff"
            r0 = r36
            int r0 = r0.post_id
            r32 = r0
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r33 = r0
            r0 = r36
            java.lang.String r0 = r0.icon_color
            r34 = r0
            r0 = r31
            r1 = r32
            r2 = r33
            r3 = r24
            r4 = r34
            android.text.style.ClickableSpan r31 = p000.C2699jj.m14605a(r0, r1, r2, r3, r4)
            r32 = 33
            r0 = r25
            r1 = r31
            r2 = r26
            r3 = r32
            r0.setSpan(r1, r2, r15, r3)
        L_0x098a:
            r0 = r35
            android.content.Context r0 = r0.f10906f
            r31 = r0
            r0 = r31
            boolean r0 = r0 instanceof com.becandid.candid.activities.PostDetailsActivity
            r31 = r0
            if (r31 != 0) goto L_0x09a5
            if (r24 != 0) goto L_0x09a5
            r0 = r35
            android.widget.TextView r0 = r0.mPostCaption
            r31 = r0
            r32 = 0
            r31.setTextIsSelectable(r32)
        L_0x09a5:
            r0 = r35
            android.widget.TextView r0 = r0.mPostCaption
            r31 = r0
            android.text.method.MovementMethod r32 = android.text.method.LinkMovementMethod.getInstance()
            r31.setMovementMethod(r32)
            r0 = r35
            android.widget.TextView r0 = r0.mPostCaption
            r31 = r0
            r0 = r31
            r1 = r25
            r0.setText(r1)
        L_0x09bf:
            return
        L_0x09c0:
            r0 = r35
            android.widget.TextView r0 = r0.mPostCaption
            r31 = r0
            r0 = r31
            r1 = r29
            r0.setText(r1)
            goto L_0x09bf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.becandid.candid.views.viewholders.BasePostViewHolder.m9934a(com.becandid.candid.views.viewholders.BasePostViewHolder, com.becandid.candid.data.Post):void");
    }

    /* renamed from: a */
    private void m9935a(String str, int i, TextView textView, Context context, int i2) {
        CandidAnimals a = CandidAnimals.m14509a();
        textView.setTypeface(CandidAnimals.m14511b());
        textView.setTextColor(i);
        textView.setText(CandidAnimals.m14510a(str));
        textView.setTextSize(1, (float) i2);
        int width = textView.getWidth();
        if (width == 0) {
            width = (int) (textView.getResources().getDisplayMetrics().density * 40.0f);
        }
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(context.getResources().getColor(17170443));
        gradientDrawable.setCornerRadius((float) (width / 2));
        textView.setBackground(gradientDrawable);
        textView.getBackground().setAlpha(192);
        textView.setVisibility(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9936b(ImageView imageView, int i, TextView textView, boolean z) {
        if (z) {
            imageView.setColorFilter(Color.parseColor("#FF0000"));
            textView.setText(Integer.toString(i + 1));
            return;
        }
        imageView.setColorFilter(Color.parseColor("#FFFFFF"));
        textView.setText(Integer.toString(i - 1));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9937b(ImageView imageView, TextView textView, Context context, boolean z) {
        int parseInt = Integer.parseInt(textView.getText().toString());
        if (z) {
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_broken_active));
            textView.setText(Integer.toString(parseInt + 1));
            return;
        }
        imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_broken_inactive));
        textView.setText(Integer.toString(parseInt - 1));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m9938b(final Post post, Builder builder, int i, final boolean z) {
        String[] split = AppState.config.getString("report_post_reasons", "nsfw:Pornographic,spam:Solicitation and Spam,hate:Hate Speech,offtopic:Off Topic,illegal:Illegal Content").split(",");
        String[] strArr = new String[split.length];
        final String[] strArr2 = new String[split.length];
        for (int i2 = 0; i2 < split.length; i2++) {
            strArr2[i2] = split[i2].split(":")[0];
            strArr[i2] = split[i2].split(":")[1];
        }
        final int[] iArr = {-1};
        builder.setTitle(i).setSingleChoiceItems((CharSequence[]) strArr, -1, (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                iArr[0] = i;
                ((AlertDialog) dialogInterface).getButton(-1).setEnabled(true);
            }
        }).setPositiveButton((int) R.string.ok, (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (iArr[0] >= 0) {
                    if (z) {
                        ApiService.m14297a().mo14119b(post, strArr2[iArr[0]]).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
                            /* renamed from: a */
                            public void onNext(EmptyClass emptyClass) {
                            }

                            public void onCompleted() {
                                RxBus.m14573a().mo14349a(new C2664u(post.post_id));
                                Context applicationContext = GossipApplication.m9012a().getApplicationContext();
                                Toast.makeText(applicationContext, applicationContext.getResources().getString(R.string.you_reported_this_post), 0).show();
                            }

                            public void onError(Throwable th) {
                            }
                        });
                    } else {
                        ApiService.m14297a().mo14103a(post, strArr2[iArr[0]]).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
                            /* renamed from: a */
                            public void onNext(EmptyClass emptyClass) {
                            }

                            public void onCompleted() {
                                RxBus.m14573a().mo14349a(new C2664u(post.post_id));
                                Context applicationContext = GossipApplication.m9012a().getApplicationContext();
                                Toast.makeText(applicationContext, applicationContext.getResources().getString(R.string.you_deleted_this_post), 0).show();
                            }

                            public void onError(Throwable th) {
                            }
                        });
                    }
                }
            }
        }).setNegativeButton((CharSequence) "Cancel", (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog create = builder.create();
        create.setOnShowListener(new OnShowListener() {
            public void onShow(DialogInterface dialogInterface) {
                if (iArr[0] < 0) {
                    ((AlertDialog) dialogInterface).getButton(-1).setEnabled(false);
                }
            }
        });
        create.show();
    }

    /* renamed from: b */
    private void m9939b(Post post, BasePostViewHolder basePostViewHolder) {
        int parseColor = Color.parseColor(post.icon_color);
        if (post.trending != 1 || post.rumor == 1) {
            basePostViewHolder.mPostTrending.setVisibility(8);
            return;
        }
        basePostViewHolder.mPostTrending.setVisibility(0);
        basePostViewHolder.mPostTrendingDrawable.setColorFilter(parseColor);
        basePostViewHolder.mPostTrendingText.setTextColor(parseColor);
    }

    /* renamed from: c */
    private void m9942c(Post post, BasePostViewHolder basePostViewHolder) {
        int parseColor = Color.parseColor(post.icon_color);
        if (post.rumor == 1) {
            basePostViewHolder.mPostRumorAlert.setVisibility(0);
            basePostViewHolder.mPostRumorAlertDrawable.setColorFilter(parseColor);
            basePostViewHolder.mPostRumorAlertText.setTextColor(parseColor);
            return;
        }
        basePostViewHolder.mPostRumorAlert.setVisibility(8);
    }

    /* renamed from: d */
    private void m9943d(Post post, BasePostViewHolder basePostViewHolder) {
        String str = post.icon_name;
        int parseColor = Color.parseColor(post.icon_color);
        if (post.is_candid_mod == 1) {
            basePostViewHolder.mPostIcon.setVisibility(8);
            basePostViewHolder.mPostModIcon.setVisibility(0);
            final BasePostViewHolder basePostViewHolder2 = basePostViewHolder;
            basePostViewHolder.mPostModIcon.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (!AppState.blurTaskCalledOnFlight) {
                        AppState.blurTaskCalledOnFlight = true;
                        new BlurTask((Activity) basePostViewHolder2.f10906f, ((Activity) basePostViewHolder2.f10906f).findViewById(16908290), BadgeType.MOD, basePostViewHolder2.f10906f.getString(R.string.moderator_post), basePostViewHolder2.f10906f.getString(R.string.this_poster_is_an_official_candid_moderator)).execute(new Void[0]);
                    }
                }
            });
        } else {
            m9935a(str, parseColor, basePostViewHolder.mPostIcon, basePostViewHolder.f10906f, 40);
            if (AppState.config.getBoolean("messaging_turned_on", true) && post.thats_me != 1) {
                final Post post2 = post;
                basePostViewHolder.mPostIcon.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (post2.messaging_blocked != null) {
                            new Builder(BasePostViewHolder.this.f10906f).setMessage((CharSequence) post2.messaging_blocked).setPositiveButton((int) R.string.ok, (OnClickListener) null).show();
                        } else if (post2.messaging_disabled != null) {
                            new Builder(BasePostViewHolder.this.f10906f).setMessage((CharSequence) post2.messaging_disabled).setPositiveButton((int) R.string.ok, (OnClickListener) null).show();
                        } else if (!AppState.hasMessagedFromAlert) {
                            new Builder(BasePostViewHolder.this.f10906f).setTitle((CharSequence) "Message User?").setMessage((CharSequence) "Would you like to send this user a message?").setNegativeButton((CharSequence) BasePostViewHolder.this.f10906f.getResources().getString(R.string.no), (OnClickListener) null).setPositiveButton((CharSequence) BasePostViewHolder.this.f10906f.getResources().getString(R.string.yes), (OnClickListener) new OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(BasePostViewHolder.this.f10906f, MessageActivity.class);
                                    intent.putExtra("post_id", Integer.toString(post2.post_id));
                                    if (post2.user_name != null) {
                                        intent.putExtra("user_name", post2.user_name);
                                    }
                                    BasePostViewHolder.this.f10906f.startActivity(intent);
                                }
                            }).create().show();
                            AppState.hasMessagedFromAlert = true;
                            AppState.saveState(GossipApplication.m9012a());
                        } else {
                            Intent intent = new Intent(BasePostViewHolder.this.f10906f, MessageActivity.class);
                            intent.putExtra("post_id", Integer.toString(post2.post_id));
                            if (post2.user_name != null) {
                                intent.putExtra("user_name", post2.user_name);
                            }
                            BasePostViewHolder.this.f10906f.startActivity(intent);
                        }
                    }
                });
                basePostViewHolder.mPostModIcon.setVisibility(8);
            }
        }
        basePostViewHolder.mPostAuthor.setText(post.user_name);
        Triangle klVar = new Triangle();
        klVar.mo14475a(Color.parseColor("#ffffff"));
        int a = C2699jj.m14599a(10, this.f10906f);
        int a2 = C2699jj.m14599a(6, this.f10906f);
        int a3 = C2699jj.m14599a(1, this.f10906f);
        klVar.setBounds(0, a3, a, a2);
        basePostViewHolder.mPostAuthor.setCompoundDrawables(null, null, klVar, null);
        basePostViewHolder.mPostTimestamp.setText(post.post_time_ago);
        basePostViewHolder.mPostGroup.setText(post.group_name);
        basePostViewHolder.mPostGroup.setTextColor(parseColor);
        final Post post3 = post;
        basePostViewHolder.mPostGroup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!(BasePostViewHolder.this.f10906f instanceof GroupDetailsActivity)) {
                    Intent intent = new Intent(BasePostViewHolder.this.f10906f, GroupDetailsActivity.class);
                    intent.putExtra("group_id", post3.group_id);
                    BasePostViewHolder.this.f10906f.startActivity(intent);
                }
            }
        });
        final Post post4 = post;
        basePostViewHolder.mPostGroupContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!(BasePostViewHolder.this.f10906f instanceof GroupDetailsActivity)) {
                    Intent intent = new Intent(BasePostViewHolder.this.f10906f, GroupDetailsActivity.class);
                    intent.putExtra("group_id", post4.group_id);
                    BasePostViewHolder.this.f10906f.startActivity(intent);
                }
            }
        });
        Triangle klVar2 = new Triangle();
        klVar2.mo14475a(parseColor);
        klVar2.setBounds(0, a3, a, a2);
        basePostViewHolder.mPostGroupMenu.setBackground(klVar2);
        final BasePostViewHolder basePostViewHolder3 = basePostViewHolder;
        final Post post5 = post;
        basePostViewHolder.mPostGroupMenuContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                if (!(basePostViewHolder3.f10906f instanceof GroupDetailsActivity)) {
                    popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            switch (menuItem.getItemId()) {
                                case R.id.group_menu_details /*2131625126*/:
                                    basePostViewHolder3.f10906f.startActivity(GroupDetailsActivity.startGroupDetailsActivity(basePostViewHolder3.f10906f, post5.group_id));
                                    break;
                                case R.id.group_menu_leave /*2131625127*/:
                                    ApiService.m14297a().mo14136d(post5.group_id).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                                        /* renamed from: a */
                                        public void onNext(NetworkData networkData) {
                                            int i = 0;
                                            while (true) {
                                                if (i >= AppState.groups.size()) {
                                                    break;
                                                } else if (((Group) AppState.groups.get(i)).group_id == post5.group_id) {
                                                    AppState.groups.remove(i);
                                                    break;
                                                } else {
                                                    i++;
                                                }
                                            }
                                            RxBus.m14573a().mo14349a(new C2662s(post5.group_id, post5.post_id));
                                        }

                                        public void onCompleted() {
                                        }

                                        public void onError(Throwable th) {
                                            Crashlytics.m16437a(th);
                                            Log.d("GroupPopupLeave", th.toString());
                                        }
                                    });
                                    break;
                                case R.id.group_menu_community /*2131625128*/:
                                    BasePostViewHolder.this.f10906f.startActivity(new Intent(BasePostViewHolder.this.f10906f, CommunitySettingsActivity.class));
                                    break;
                                case R.id.group_menu_join /*2131625129*/:
                                    ApiService.m14297a().mo14128c(post5.group_id).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                                        /* renamed from: a */
                                        public void onNext(NetworkData networkData) {
                                            if (networkData.success) {
                                                RxBus.m14573a().mo14349a(new C2660q(networkData.group.group_id, post5.post_id));
                                            }
                                        }

                                        public void onCompleted() {
                                        }

                                        public void onError(Throwable th) {
                                            Crashlytics.m16437a(th);
                                            Log.d("GroupPopupLeave", th.toString());
                                        }
                                    });
                                    break;
                            }
                            return false;
                        }
                    });
                    popupMenu.getMenu().add(1, R.id.group_menu_details, 0, R.string.group_details);
                    boolean z = true;
                    if (post5.moderator == 1) {
                        z = false;
                    }
                    if (AppState.groups != null) {
                        Iterator it = AppState.groups.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Group group = (Group) it.next();
                            if (group.group_id == post5.group_id) {
                                if (group.moderator != 1) {
                                    if (group.community_membership == 1) {
                                        popupMenu.getMenu().add(1, R.id.group_menu_community, 0, R.string.edit_community);
                                    } else {
                                        popupMenu.getMenu().add(1, R.id.group_menu_leave, 0, R.string.leave_group);
                                    }
                                    z = false;
                                }
                            }
                        }
                    }
                    if (z) {
                        popupMenu.getMenu().add(1, R.id.group_menu_join, 0, R.string.join_group);
                    }
                    popupMenu.show();
                    return;
                }
                popupMenu.setOnMenuItemClickListener((OnMenuItemClickListener) BasePostViewHolder.this.f10906f);
                boolean z2 = true;
                if (post5.moderator == 1) {
                    z2 = false;
                }
                if (AppState.groups != null) {
                    Iterator it2 = AppState.groups.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        Group group2 = (Group) it2.next();
                        if (group2.group_id == post5.group_id) {
                            if (group2.moderator != 1) {
                                if (group2.community_membership == 1) {
                                    popupMenu.getMenu().add(1, R.id.group_menu_community, 0, R.string.edit_community);
                                } else {
                                    popupMenu.getMenu().add(1, R.id.group_menu_leave, 0, R.string.leave_group);
                                }
                                popupMenu.getMenu().add(1, R.id.group_menu_report, 0, R.string.report_group);
                                z2 = false;
                            } else {
                                popupMenu.getMenu().add(1, R.id.group_menu_edit, 0, R.string.edit_group);
                                if (group2.num_posts == 0) {
                                    popupMenu.getMenu().add(1, R.id.group_menu_delete, 0, R.string.delete_group);
                                }
                            }
                        }
                    }
                }
                if (z2) {
                    popupMenu.getMenu().add(1, R.id.group_menu_join, 0, R.string.join_group);
                }
                popupMenu.show();
            }
        });
        if (post.thats_me == 1) {
            basePostViewHolder.reportFlag.setVisibility(8);
        } else {
            basePostViewHolder.reportFlag.setVisibility(0);
            final Post post6 = post;
            basePostViewHolder.reportFlag.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Builder builder = new Builder(BasePostViewHolder.this.f10906f);
                    final int[] iArr = {-1};
                    String[] split = AppState.config.getString("report_post_reasons", "nsfw:Pornographic,spam:Solicitation and Spam,hate:Hate Speech,offtopic:Off Topic,illegal:Illegal Content").split(",");
                    String[] strArr = new String[split.length];
                    final String[] strArr2 = new String[split.length];
                    for (int i = 0; i < split.length; i++) {
                        strArr2[i] = split[i].split(":")[0];
                        strArr[i] = split[i].split(":")[1];
                    }
                    builder.setTitle((int) R.string.why_are_you_reporting_this_post).setSingleChoiceItems((CharSequence[]) strArr, -1, (OnClickListener) new OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            iArr[0] = i;
                            ((AlertDialog) dialogInterface).getButton(-1).setEnabled(true);
                        }
                    }).setPositiveButton((int) R.string.ok, (OnClickListener) new OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (iArr[0] < 0) {
                                Log.d("BPVH", "No reason selected");
                            } else {
                                ApiService.m14297a().mo14119b(post6, strArr2[iArr[0]]).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
                                    /* renamed from: a */
                                    public void onNext(EmptyClass emptyClass) {
                                    }

                                    public void onCompleted() {
                                        RxBus.m14573a().mo14349a(new C2664u(post6.post_id));
                                        Context applicationContext = GossipApplication.m9012a().getApplicationContext();
                                        Toast.makeText(applicationContext, applicationContext.getResources().getString(R.string.you_reported_this_post), 0).show();
                                    }

                                    public void onError(Throwable th) {
                                    }
                                });
                            }
                        }
                    }).setNegativeButton((CharSequence) "Cancel", (OnClickListener) new OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    AlertDialog create = builder.create();
                    create.setOnShowListener(new OnShowListener() {
                        public void onShow(DialogInterface dialogInterface) {
                            if (iArr[0] < 0) {
                                ((AlertDialog) dialogInterface).getButton(-1).setEnabled(false);
                            }
                        }
                    });
                    create.show();
                }
            });
        }
        if (post.quality_score != 0.0f || post.is_friend == 1 || post.thats_me == 1 || post.is_candid_mod == 1) {
            final String format = new DecimalFormat("#").format((double) post.quality_score);
            basePostViewHolder.mPostHeaderBadge.setVisibility(0);
            String str2 = "";
            if (post.is_candid_mod == 1) {
                str2 = "MOD";
            } else if (post.thats_me == 1) {
                str2 = "YOU";
            } else if (post.is_friend == 1) {
                str2 = "FRIEND";
            }
            basePostViewHolder.mPostHeaderBadgeText.setText(str2);
            basePostViewHolder.mPostHeaderBadgeText.setTextColor(parseColor);
            if (post.quality_score != 0.0f) {
                basePostViewHolder.mPostHeaderBadgeScore.setVisibility(0);
                basePostViewHolder.mPostHeaderBadgeScore.setText(format);
                basePostViewHolder.mPostHeaderBadgeScore.setTextColor(parseColor);
                if (!str2.isEmpty()) {
                    basePostViewHolder.mPostHeaderBadgeDot.setVisibility(0);
                    Drawable drawable = basePostViewHolder.f10906f.getResources().getDrawable(R.drawable.dot_circle);
                    drawable.setColorFilter(parseColor, Mode.SRC_IN);
                    basePostViewHolder.mPostHeaderBadgeDot.setBackground(drawable);
                } else {
                    basePostViewHolder.mPostHeaderBadgeDot.setVisibility(8);
                }
            } else {
                basePostViewHolder.mPostHeaderBadgeScore.setVisibility(8);
                basePostViewHolder.mPostHeaderBadgeDot.setVisibility(8);
            }
            if (post.quality_score != 0.0f) {
                final BasePostViewHolder basePostViewHolder4 = basePostViewHolder;
                basePostViewHolder.mPostHeaderBadge.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (!AppState.blurTaskCalledOnFlight) {
                            AppState.blurTaskCalledOnFlight = true;
                            new BlurTask((Activity) basePostViewHolder4.f10906f, ((Activity) basePostViewHolder4.f10906f).findViewById(16908290), format, BadgeType.POST_QUALITY_SCORE, basePostViewHolder4.f10906f.getString(R.string.quality_score_for_the_post), basePostViewHolder4.f10906f.getString(R.string.the_more_engaging_entertaining_and_substantive_you_post_the_higher_your_quality_score)).execute(new Void[0]);
                        }
                    }
                });
                return;
            }
            basePostViewHolder.mPostHeaderBadge.setOnClickListener(null);
            return;
        }
        basePostViewHolder.mPostHeaderBadge.setVisibility(8);
    }

    /* renamed from: a */
    public void mo10895a(Post post, BasePostViewHolder basePostViewHolder) {
        int i = post.num_likes;
        int i2 = post.num_dislikes;
        basePostViewHolder.mCommentPostText.setText(Integer.toString(post.num_comments));
        basePostViewHolder.mLikeRumorPostText.setText(Integer.toString(i));
        basePostViewHolder.mUnlikeRumorPostText.setText(Integer.toString(i2));
        String str = post.comment_text;
        String str2 = post.comment_icon_name;
        int i3 = post.comment_image_height;
        int i4 = post.comment_image_width;
        String str3 = post.comment_source_url;
        String str4 = post.comment_time_ago;
        String str5 = post.comment_sticker_name;
        int i5 = post.num_comments;
        int i6 = post.comment_is_candid_mod;
        if (i5 <= 1) {
            basePostViewHolder.mFooterCommentValue.setVisibility(8);
        } else {
            basePostViewHolder.mFooterCommentValue.setVisibility(0);
            basePostViewHolder.mFooterCommentValue.setText(String.format(basePostViewHolder.f10906f.getString(R.string.read_all_comments), new Object[]{Integer.toString(i5)}));
        }
        if (str2 == null || str2.isEmpty()) {
            basePostViewHolder.mFooterCommentSnippet.setVisibility(8);
            return;
        }
        basePostViewHolder.mFooterCommentSnippet.setVisibility(0);
        mo10897a(post.icon_color, str2, str, str4, str3, i4, i3, str5, basePostViewHolder, i6, post.post_id);
    }

    /* renamed from: a */
    public void mo10896a(BasePostViewHolder basePostViewHolder, Post post, int i) {
        basePostViewHolder.mPostShareOgImage.setVisibility(4);
        if (post.rumor == 1) {
            basePostViewHolder.mTrueRumorContainerOgImage.setVisibility(0);
            basePostViewHolder.mFalseRumorContainerOgImage.setVisibility(0);
            basePostViewHolder.mLikePostBtnOgImage.setVisibility(8);
            basePostViewHolder.mUnlikePostBtnOgImage.setVisibility(8);
            int parseColor = Color.parseColor("#FFFFFF");
            basePostViewHolder.mTrueRumorIconOgImage.setColorFilter(parseColor);
            basePostViewHolder.mFalseRumorIconOgImage.setColorFilter(parseColor);
            basePostViewHolder.mTrueRumorTextOgImage.setText("True " + Integer.toString(post.num_true));
            basePostViewHolder.mFalseRumorTextOgImage.setText("False " + Integer.toString(post.num_false));
        } else {
            basePostViewHolder.mTrueRumorContainerOgImage.setVisibility(8);
            basePostViewHolder.mFalseRumorContainerOgImage.setVisibility(8);
            basePostViewHolder.mLikePostBtnOgImage.setVisibility(0);
            basePostViewHolder.mUnlikePostBtnOgImage.setVisibility(0);
            basePostViewHolder.mLikePostBtnOgImage.setText(Integer.toString(post.num_likes));
            basePostViewHolder.mUnlikePostBtnOgImage.setText(Integer.toString(post.num_dislikes));
        }
        basePostViewHolder.mCommentPostBtnOgImage.setText(Integer.toString(post.num_comments));
        basePostViewHolder.mPostCaptionOgImage.setText(post.caption);
        basePostViewHolder.mPostGroupOgImage.setText(post.group_name);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(0);
        gradientDrawable.setColor(C2699jj.f10798a);
        basePostViewHolder.mPostImageFooter.setBackground(gradientDrawable);
        final ShareDialog shareDialog = new ShareDialog((Activity) this.f10906f);
        shareDialog.mo32a(C3115a.m17289a(), (FacebookCallback<RESULT>) new FacebookCallback<C0087a>() {
            /* renamed from: a */
            public void onSuccess(C0087a aVar) {
            }

            public void onCancel() {
            }

            public void onError(FacebookException facebookException) {
            }
        });
        ViewTreeObserver viewTreeObserver = basePostViewHolder.mPostShareOgImage.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            final int i2 = i;
            final BasePostViewHolder basePostViewHolder2 = basePostViewHolder;
            final Post post2 = post;
            viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    if (BasePostViewHolder.this.mPostShareOgImage != null) {
                        View view = BasePostViewHolder.this.mPostShareOgImage;
                        BasePostViewHolder.this.mPostShareOgImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Config.ARGB_8888);
                        Canvas canvas = new Canvas(createBitmap);
                        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
                        view.draw(canvas);
                        if (createBitmap != null) {
                            if (i2 == ShareUtils.f10789a) {
                                if (shareDialog != null) {
                                    SharePhotoContent a = new C2079a().mo11957a(new C2077a().mo11944a(createBitmap).mo11951c()).mo11959a();
                                    ShareDialog shareDialog = shareDialog;
                                    if (ShareDialog.m11055a(SharePhotoContent.class)) {
                                        shareDialog.mo36b(a);
                                    }
                                }
                            } else if (i2 == 1989) {
                                ShareUtils.m14581a().mo14355a(createBitmap);
                                ShareUtils.m14581a().mo14354a(basePostViewHolder2.f10906f);
                                if (post2.share_info != null) {
                                    ShareUtils.m14581a().mo14356a(post2.share_info.url);
                                }
                                if (ContextCompat.checkSelfPermission(basePostViewHolder2.f10906f, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                                    ActivityCompat.requestPermissions((Activity) basePostViewHolder2.f10906f, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1989);
                                } else {
                                    ShareUtils.m14581a().mo14357b();
                                }
                            }
                        }
                        basePostViewHolder2.mPostShareOgImage.setVisibility(8);
                    }
                }
            });
        }
    }

    /* renamed from: a */
    public void mo10897a(final String str, String str2, String str3, String str4, String str5, int i, int i2, String str6, BasePostViewHolder basePostViewHolder, int i3, int i4) {
        if (i3 == 1) {
            basePostViewHolder.mFooterCommentModIcon.setVisibility(0);
            basePostViewHolder.mFooterCommentIcon.setVisibility(8);
        } else {
            basePostViewHolder.mFooterCommentIcon.setVisibility(0);
            m9935a(str2, Color.parseColor(str), basePostViewHolder.mFooterCommentIcon, basePostViewHolder.f10906f, 25);
            basePostViewHolder.mFooterCommentModIcon.setVisibility(8);
        }
        basePostViewHolder.mFooterCommentTimestamp.setText(str4);
        if (str3 == null || str3.isEmpty()) {
            basePostViewHolder.mFooterCommentText.setVisibility(8);
        } else {
            basePostViewHolder.mFooterCommentText.setVisibility(0);
            basePostViewHolder.mFooterCommentText.setText(str3);
        }
        boolean z = (i2 == -1 || i == -1 || str5 == null || str5.isEmpty()) ? false : true;
        boolean z2 = (i2 == -1 || i == -1 || str6 == null) ? false : true;
        float f = basePostViewHolder.f10906f.getResources().getDisplayMetrics().density;
        if (z) {
            basePostViewHolder.mFooterCommentImage.setVisibility(0);
            basePostViewHolder.mFooterCommentImage.setLayoutParams(new LayoutParams((int) (((float) i) * f), (int) (((float) i2) * f)));
            GossipApplication.f6095c.mo14650a(str5).mo14576h().mo14554a(basePostViewHolder.mFooterCommentImage);
        } else if (z2) {
            basePostViewHolder.mFooterCommentImage.setVisibility(0);
            basePostViewHolder.mFooterCommentImage.setLayoutParams(new LayoutParams((int) (((float) i) * f), (int) (((float) i2) * f)));
            basePostViewHolder.mFooterCommentImage.setImageDrawable(GossipApplication.f6093a.getResources().getDrawable(GossipApplication.f6093a.getResources().getIdentifier(str6.toLowerCase(), "drawable", GossipApplication.f6093a.getPackageName())));
        } else {
            basePostViewHolder.mFooterCommentImage.setVisibility(8);
        }
        final int i5 = i4;
        basePostViewHolder.mFooterCommentSnippet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PostDetailsActivity.class);
                intent.putExtra("post_id", i5);
                intent.putExtra("op_color", str);
                if (BasePostViewHolder.f7427a != null) {
                    intent.putExtra("feed_type", BasePostViewHolder.f7427a);
                }
                BasePostViewHolder.this.f10906f.startActivity(intent);
            }
        });
    }
}
