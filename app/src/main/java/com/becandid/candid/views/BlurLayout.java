package com.becandid.candid.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.BindView;

public abstract class BlurLayout extends RelativeLayout {

    /* renamed from: a */
    protected String f7290a;
    @BindView(2131624077)
    View mBackgroundContainer;
    @BindView(2131624084)
    ImageView mBlurImage;

    public BlurLayout(Context context) {
        super(context);
    }

    /* renamed from: b */
    private void m9796b() {
        if (!TextUtils.isEmpty(this.f7290a)) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public Void doInBackground(Void... voidArr) {
                    BlurUtils.m14767b(BlurLayout.this.f7290a);
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo10765a() {
        if (!TextUtils.isEmpty(this.f7290a)) {
            this.mBackgroundContainer.setVisibility(0);
            Bitmap a = BlurUtils.m14763a(this.f7290a);
            if (a != null) {
                this.mBlurImage.setImageBitmap(a);
                this.mBlurImage.setAlpha(0.075f);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m9796b();
    }
}
