package com.becandid.candid.views;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.BlurLayout$$ViewBinder.C1791a;
import com.becandid.candid.views.PostQualityFeedbackView;

public class PostQualityFeedbackView$$ViewBinder<T extends PostQualityFeedbackView> extends BlurLayout$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.PostQualityFeedbackView$$ViewBinder$a */
    /* compiled from: PostQualityFeedbackView$$ViewBinder */
    public static class C1844a<T extends PostQualityFeedbackView> extends C1791a<T> {
        protected C1844a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            super.mo10764a(t);
            t.mMessageContainer = null;
            t.mOkBtn = null;
            t.mBadgeIcn = null;
            t.mBadgeTitle = null;
            t.mBadgeDesc = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1844a aVar = (C1844a) super.bind(finder, t, obj);
        t.mMessageContainer = (View) finder.findRequiredView(obj, R.id.message_container_quality_feedback, "field 'mMessageContainer'");
        t.mOkBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.ok, "field 'mOkBtn'"), R.id.ok, "field 'mOkBtn'");
        t.mBadgeIcn = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_icon, "field 'mBadgeIcn'"), R.id.badge_icon, "field 'mBadgeIcn'");
        t.mBadgeTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_title_quality_feedback, "field 'mBadgeTitle'"), R.id.badge_title_quality_feedback, "field 'mBadgeTitle'");
        t.mBadgeDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_desc_quality_feedback, "field 'mBadgeDesc'"), R.id.badge_desc_quality_feedback, "field 'mBadgeDesc'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1844a<T> mo10762a(T t) {
        return new C1844a<>(t);
    }
}
