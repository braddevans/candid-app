package com.becandid.candid.views;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.ChoosePhotoDialogView;

public class ChoosePhotoDialogView$$ViewBinder<T extends ChoosePhotoDialogView> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.ChoosePhotoDialogView$$ViewBinder$a */
    /* compiled from: ChoosePhotoDialogView$$ViewBinder */
    public static class C1799a<T extends ChoosePhotoDialogView> implements Unbinder {

        /* renamed from: a */
        View f7306a;

        /* renamed from: b */
        View f7307b;

        /* renamed from: c */
        View f7308c;

        /* renamed from: d */
        private T f7309d;

        protected C1799a(T t) {
            this.f7309d = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10784a(T t) {
            t.spinny = null;
            t.suggestedPhotoView = null;
            this.f7306a.setOnClickListener(null);
            this.f7307b.setOnClickListener(null);
            this.f7308c.setOnClickListener(null);
        }

        public final void unbind() {
            if (this.f7309d == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10784a(this.f7309d);
            this.f7309d = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1799a a = mo10783a(t);
        t.spinny = (ProgressBar) finder.castView((View) finder.findRequiredView(obj, R.id.choose_photo_loading, "field 'spinny'"), R.id.choose_photo_loading, "field 'spinny'");
        t.suggestedPhotoView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.choose_photo_suggestions, "field 'suggestedPhotoView'"), R.id.choose_photo_suggestions, "field 'suggestedPhotoView'");
        View view = (View) finder.findRequiredView(obj, R.id.choose_photo_take_photo, "method 'takePhoto'");
        a.f7306a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.takePhoto(view);
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.choose_photo_library, "method 'library'");
        a.f7307b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.library(view);
            }
        });
        View view3 = (View) finder.findRequiredView(obj, R.id.choose_photo_cancel, "method 'cancel'");
        a.f7308c = view3;
        view3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.cancel(view);
            }
        });
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1799a<T> mo10783a(T t) {
        return new C1799a<>(t);
    }
}
