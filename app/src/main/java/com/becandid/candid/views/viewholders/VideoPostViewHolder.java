package com.becandid.candid.views.viewholders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.FullScreenVideoActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Post;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.volokh.danylo.video_player_manager.p008ui.MediaPlayerWrapper.C2248a;
import com.volokh.danylo.video_player_manager.p008ui.VideoPlayerView;

public class VideoPostViewHolder extends BasePostViewHolder {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public int f7539a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f7540b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public String f7541c;

    /* renamed from: d */
    private String f7542d;
    @BindView(2131624584)
    public ImageView postVideoCover;
    @BindView(2131624582)
    public FrameLayout postVideoPlaceholder;
    @BindView(2131624585)
    public View postVideoPlayBtn;
    @BindView(2131624583)
    public VideoPlayerView postVideoPlayer;
    @BindView(2131624587)
    public ProgressBar postVideoProgressBar;
    @BindView(2131624581)
    View postVideoRow;
    @BindView(2131624586)
    public TextView postVideoVerified;

    public VideoPostViewHolder(View view, Activity activity) {
        super(view, activity);
    }

    public VideoPostViewHolder(View view, Activity activity, String str) {
        super(view, activity, str);
    }

    /* renamed from: a */
    public static void m10005a(VideoPostViewHolder videoPostViewHolder, Post post) {
        BasePostViewHolder.m9934a((BasePostViewHolder) videoPostViewHolder, post);
        if (post.theme_color == 0) {
            post.theme_color = R.color.gossip_grey;
        }
        C2699jj.m14616a(videoPostViewHolder.postVideoRow, post.theme_color);
        if (post.link_url == null || post.source_url == null) {
            videoPostViewHolder.postVideoPlaceholder.setVisibility(8);
            videoPostViewHolder.f7541c = null;
            videoPostViewHolder.f7542d = null;
            return;
        }
        videoPostViewHolder.postVideoPlaceholder.setVisibility(0);
        videoPostViewHolder.postVideoCover.setVisibility(0);
        videoPostViewHolder.postVideoPlayBtn.setVisibility(0);
        videoPostViewHolder.postVideoPlayer.setVisibility(0);
        videoPostViewHolder.postVideoProgressBar.setVisibility(8);
        videoPostViewHolder.f7541c = post.link_url;
        videoPostViewHolder.f7542d = post.source_url;
        videoPostViewHolder.postVideoPlayer.mo13122a(videoPostViewHolder.m10010c(videoPostViewHolder, post));
        if (post.from_camera == 1 && AppState.config.getInt("android_msg_image_upload", 1) == 1) {
            videoPostViewHolder.postVideoVerified.setVisibility(0);
        } else {
            videoPostViewHolder.postVideoVerified.setVisibility(8);
        }
        if (post.actual_height == -1 && post.actual_width == -1) {
            Display defaultDisplay = ((WindowManager) videoPostViewHolder.f10906f.getSystemService("window")).getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            int i = point.x - (((int) (5.0f * (((float) videoPostViewHolder.f10906f.getResources().getDisplayMetrics().densityDpi) / 160.0f))) * 2);
            post.actual_height = (int) ((((float) i) * ((float) post.height)) / ((float) post.width));
            post.actual_width = i;
        }
        LayoutParams layoutParams = (LayoutParams) videoPostViewHolder.postVideoPlaceholder.getLayoutParams();
        layoutParams.width = post.actual_width;
        layoutParams.height = post.actual_height;
        videoPostViewHolder.postVideoPlaceholder.setLayoutParams(layoutParams);
        final Context context = videoPostViewHolder.f10906f;
        BitmapTypeRequest j = GossipApplication.f6095c.mo14657d().mo14598j();
        PaletteBitmapTranscoder jmVar = new PaletteBitmapTranscoder(context);
        BitmapRequestBuilder a = j.mo14577a(jmVar, PaletteBitmap.class).mo14559b(DiskCacheStrategy.SOURCE).mo14560b(post.source_url).mo14558b(Priority.IMMEDIATE);
        final VideoPostViewHolder videoPostViewHolder2 = videoPostViewHolder;
        final Post post2 = post;
        C19191 r0 = new ImageViewTarget<PaletteBitmap>(videoPostViewHolder.postVideoCover) {
            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void setResource(PaletteBitmap jkVar) {
                ((ImageView) this.view).setImageBitmap(jkVar.f10824b);
                int darkMutedColor = jkVar.f10823a.getDarkMutedColor(ContextCompat.getColor(context, R.color.gossip_grey));
                videoPostViewHolder2.mPostIcon.setTextColor(darkMutedColor);
                videoPostViewHolder2.mPostHeaderBadgeScore.setTextColor(darkMutedColor);
                videoPostViewHolder2.mPostGroup.setTextColor(darkMutedColor);
                Triangle klVar = new Triangle();
                klVar.mo14475a(darkMutedColor);
                klVar.setBounds(0, C2699jj.m14599a(1, context), C2699jj.m14599a(10, context), C2699jj.m14599a(6, context));
                videoPostViewHolder2.mPostGroupMenu.setBackground(klVar);
                videoPostViewHolder2.mFooterCommentIcon.setTextColor(darkMutedColor);
                videoPostViewHolder2.mPostHeaderBadgeText.setTextColor(darkMutedColor);
                Drawable drawable = videoPostViewHolder2.f10906f.getResources().getDrawable(R.drawable.dot_circle);
                drawable.setColorFilter(darkMutedColor, Mode.SRC_IN);
                videoPostViewHolder2.mPostHeaderBadgeDot.setBackground(drawable);
                videoPostViewHolder2.postVideoCover.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        VideoPostViewHolder.m10013d(videoPostViewHolder2, post2);
                    }
                });
            }
        };
        a.mo14601a((Target) r0);
        VideoPlayerView videoPlayerView = videoPostViewHolder.postVideoPlayer;
        final Post post3 = post;
        C19212 r02 = new OnClickListener(videoPostViewHolder) {

            /* renamed from: a */
            final /* synthetic */ VideoPostViewHolder f7547a;

            {
                this.f7547a = r1;
            }

            public void onClick(View view) {
                VideoPostViewHolder.m10013d(this.f7547a, post3);
            }
        };
        videoPlayerView.setOnClickListener(r02);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m10008b() {
        return (this.postVideoPlayer == null || this.postVideoPlayer.getMediaPlayer() == null) ? false : true;
    }

    /* renamed from: c */
    private C2248a m10010c(final VideoPostViewHolder videoPostViewHolder, final Post post) {
        final boolean b = GossipApplication.m9013a(videoPostViewHolder.f10906f).mo16744b(videoPostViewHolder.f7541c);
        return new C2248a() {
            /* renamed from: a */
            public void mo8812a() {
                if (!(VideoPostViewHolder.this.postVideoPlayer == null || VideoPostViewHolder.this.postVideoPlayer.getMediaPlayer() == null)) {
                    VideoPostViewHolder.this.postVideoPlayer.mo13135k();
                }
                if (b) {
                    videoPostViewHolder.postVideoCover.setVisibility(8);
                    videoPostViewHolder.postVideoPlayBtn.setVisibility(8);
                    videoPostViewHolder.postVideoProgressBar.setVisibility(8);
                    videoPostViewHolder.postVideoPlayer.setVisibility(0);
                    videoPostViewHolder.postVideoPlayer.setOnClickListener(VideoPostViewHolder.m10013d(videoPostViewHolder, post));
                    return;
                }
                videoPostViewHolder.postVideoCover.setVisibility(0);
                videoPostViewHolder.postVideoProgressBar.setVisibility(0);
                videoPostViewHolder.postVideoPlayBtn.setVisibility(8);
            }

            /* renamed from: a */
            public void mo8813a(int i) {
            }

            /* renamed from: a */
            public void mo8814a(int i, int i2) {
                videoPostViewHolder.f7539a = i;
                videoPostViewHolder.f7540b = i2;
            }

            /* renamed from: b */
            public void mo8818b() {
                videoPostViewHolder.m10012c();
            }

            /* renamed from: b */
            public void mo8819b(int i) {
                if (i == 3) {
                    videoPostViewHolder.postVideoCover.setVisibility(8);
                    videoPostViewHolder.postVideoPlayBtn.setVisibility(8);
                    videoPostViewHolder.postVideoProgressBar.setVisibility(8);
                    videoPostViewHolder.postVideoPlayer.setVisibility(0);
                    videoPostViewHolder.postVideoPlayer.setOnClickListener(VideoPostViewHolder.m10013d(videoPostViewHolder, post));
                } else if (i == 701) {
                    videoPostViewHolder.postVideoProgressBar.setVisibility(0);
                } else if (i == 702) {
                    videoPostViewHolder.postVideoCover.setVisibility(8);
                    videoPostViewHolder.postVideoPlayBtn.setVisibility(8);
                    videoPostViewHolder.postVideoProgressBar.setVisibility(8);
                    videoPostViewHolder.postVideoPlayer.setVisibility(0);
                    videoPostViewHolder.postVideoPlayer.setOnClickListener(VideoPostViewHolder.m10013d(videoPostViewHolder, post));
                }
            }

            /* renamed from: b */
            public void mo8820b(int i, int i2) {
                videoPostViewHolder.m10012c();
            }

            /* renamed from: c */
            public void mo8821c() {
                videoPostViewHolder.m10012c();
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m10012c() {
        this.postVideoPlayer.setVisibility(8);
        this.postVideoCover.setVisibility(0);
        this.postVideoPlayBtn.setVisibility(0);
        this.postVideoProgressBar.setVisibility(8);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static OnClickListener m10013d(final VideoPostViewHolder videoPostViewHolder, final Post post) {
        return new OnClickListener() {
            public void onClick(View view) {
                if (!(post == null || post.getVideoPlayerManager() == null)) {
                    post.getVideoPlayerManager().mo8822d();
                }
                videoPostViewHolder.postVideoCover.setVisibility(0);
                videoPostViewHolder.postVideoPlayBtn.setVisibility(0);
                videoPostViewHolder.postVideoProgressBar.setVisibility(8);
                int i = videoPostViewHolder.m10008b() ? videoPostViewHolder.postVideoPlayer.getMediaPlayer().mo13087g() : 0;
                Intent intent = new Intent(videoPostViewHolder.f10906f, FullScreenVideoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("post_id", post.post_id);
                bundle.putString("source_url", post.source_url);
                bundle.putInt("num_likes", post.num_likes);
                bundle.putInt("num_dislikes", post.num_dislikes);
                bundle.putString("icon_name", post.icon_name);
                bundle.putString("icon_color", post.icon_color);
                bundle.putString("user_name", post.user_name);
                bundle.putInt("like_value", post.like_value);
                bundle.putInt("num_comments", post.num_comments);
                bundle.putBoolean("fromDetails", true);
                bundle.putInt("is_rumor", post.rumor);
                bundle.putInt("num_true", post.num_true);
                bundle.putInt("num_false", post.num_false);
                bundle.putInt("opinion_value", post.opinion_value);
                bundle.putString("share_info_url", post.share_info.url);
                bundle.putString("share_info_title", post.share_info.title);
                bundle.putString("share_info_image", post.share_info.image);
                bundle.putInt("from_camera", post.from_camera);
                bundle.putString("video_url", videoPostViewHolder.f7541c);
                bundle.putInt("curr_pos", i);
                bundle.putInt("uploaded", post.from_camera);
                intent.putExtras(bundle);
                videoPostViewHolder.f10906f.startActivity(intent);
            }
        };
    }
}
