package com.becandid.candid.views;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.ChoosePhotoDialogView.PhotoSuggestionHolder;

public class ChoosePhotoDialogView$PhotoSuggestionHolder$$ViewBinder<T extends PhotoSuggestionHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.ChoosePhotoDialogView$PhotoSuggestionHolder$$ViewBinder$a */
    /* compiled from: ChoosePhotoDialogView$PhotoSuggestionHolder$$ViewBinder */
    public static class C1800a<T extends PhotoSuggestionHolder> implements Unbinder {

        /* renamed from: a */
        private T f7310a;

        protected C1800a(T t) {
            this.f7310a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10787a(T t) {
            t.imageView = null;
        }

        public final void unbind() {
            if (this.f7310a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10787a(this.f7310a);
            this.f7310a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1800a a = mo10786a(t);
        t.imageView = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.suggestion_image, "field 'imageView'"), R.id.suggestion_image, "field 'imageView'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1800a<T> mo10786a(T t) {
        return new C1800a<>(t);
    }
}
