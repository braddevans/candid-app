package com.becandid.candid.views.viewholders.messaging;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.PostDetailsActivity;
import com.becandid.candid.data.Message;
import com.becandid.candid.data.Post;
import com.becandid.candid.util.RoundedCornersTransformation;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.text.DecimalFormat;

public class MessagingPostViewHolder extends BaseViewHolder {
    @BindView(2131624323)
    View mHeader;
    @BindView(2131625051)
    public TextView mPostAuthor;
    @BindView(2131624546)
    TextView mPostCaption;
    @BindView(2131625061)
    public TextView mPostGroup;
    @BindView(2131625058)
    public FrameLayout mPostGroupContainer;
    @BindView(2131625060)
    public View mPostGroupMenu;
    @BindView(2131625059)
    RelativeLayout mPostGroupMenuContainer;
    @BindView(2131625052)
    View mPostHeaderBadge;
    @BindView(2131625054)
    FrameLayout mPostHeaderBadgeDot;
    @BindView(2131625055)
    TextView mPostHeaderBadgeScore;
    @BindView(2131625053)
    TextView mPostHeaderBadgeText;
    @BindView(2131625048)
    public TextView mPostIcon;
    @BindView(2131624547)
    ImageView mPostImage;
    @BindView(2131624551)
    TextView mPostLinkDesc;
    @BindView(2131624552)
    TextView mPostLinkDomain;
    @BindView(2131624549)
    ImageView mPostLinkImage;
    @BindView(2131624548)
    LinearLayout mPostLinkPlacehoder;
    @BindView(2131624550)
    TextView mPostLinkTitle;
    @BindView(2131625049)
    public ImageView mPostModIcon;
    @BindView(2131625050)
    View mPostPopupAction;
    @BindView(2131624545)
    LinearLayout mPostRow;
    @BindView(2131625056)
    public TextView mPostTimestamp;
    @BindView(2131624553)
    TextView messagePostDeleted;
    @BindView(2131624544)
    FrameLayout messagePostWrapper;

    public MessagingPostViewHolder(View view, Activity activity) {
        super(view, activity);
    }

    /* renamed from: a */
    private void m10032a(Post post, MessagingPostViewHolder messagingPostViewHolder) {
        String str = post.icon_name;
        int parseColor = Color.parseColor(post.icon_color);
        if (post.is_candid_mod == 1) {
            messagingPostViewHolder.mPostIcon.setVisibility(8);
            messagingPostViewHolder.mPostModIcon.setVisibility(0);
        } else {
            m10034a(str, parseColor, messagingPostViewHolder.mPostIcon, messagingPostViewHolder.f10906f, 40);
            messagingPostViewHolder.mPostModIcon.setVisibility(8);
        }
        messagingPostViewHolder.mPostAuthor.setText(post.user_name);
        messagingPostViewHolder.mPostTimestamp.setText(post.post_time_ago);
        messagingPostViewHolder.mPostGroup.setText(post.group_name);
        messagingPostViewHolder.mPostGroup.setTextColor(parseColor);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.setMargins(0, 0, 0, 0);
        messagingPostViewHolder.mPostGroup.setLayoutParams(layoutParams);
        messagingPostViewHolder.mPostGroup.setPadding(C2699jj.m14599a(10, this.f10906f), C2699jj.m14599a(4, this.f10906f), C2699jj.m14599a(10, this.f10906f), C2699jj.m14599a(4, this.f10906f));
        if (post.quality_score != 0.0f || post.is_friend == 1 || post.thats_me == 1 || post.is_candid_mod == 1) {
            String format = new DecimalFormat("#").format((double) post.quality_score);
            messagingPostViewHolder.mPostHeaderBadge.setVisibility(0);
            String str2 = "";
            if (post.is_candid_mod == 1) {
                str2 = "MOD";
            } else if (post.thats_me == 1) {
                str2 = "YOU";
            } else if (post.is_friend == 1) {
                str2 = "FRIEND";
            }
            messagingPostViewHolder.mPostHeaderBadgeText.setText(str2);
            messagingPostViewHolder.mPostHeaderBadgeText.setTextColor(parseColor);
            if (post.quality_score != 0.0f) {
                messagingPostViewHolder.mPostHeaderBadgeScore.setVisibility(0);
                messagingPostViewHolder.mPostHeaderBadgeScore.setText(format);
                messagingPostViewHolder.mPostHeaderBadgeScore.setTextColor(parseColor);
                if (!str2.isEmpty()) {
                    messagingPostViewHolder.mPostHeaderBadgeDot.setVisibility(0);
                    Drawable drawable = messagingPostViewHolder.f10906f.getResources().getDrawable(R.drawable.dot_circle);
                    drawable.setColorFilter(parseColor, Mode.SRC_IN);
                    messagingPostViewHolder.mPostHeaderBadgeDot.setBackground(drawable);
                    return;
                }
                messagingPostViewHolder.mPostHeaderBadgeDot.setVisibility(8);
                return;
            }
            messagingPostViewHolder.mPostHeaderBadgeScore.setVisibility(8);
            messagingPostViewHolder.mPostHeaderBadgeDot.setVisibility(8);
            return;
        }
        messagingPostViewHolder.mPostHeaderBadge.setVisibility(8);
    }

    /* renamed from: a */
    public static void m10033a(MessagingPostViewHolder messagingPostViewHolder, Message message) {
        final Post post = message.post;
        if (post == null) {
            messagingPostViewHolder.mPostRow.setVisibility(8);
            messagingPostViewHolder.messagePostDeleted.setVisibility(0);
            return;
        }
        messagingPostViewHolder.mPostRow.setVisibility(0);
        messagingPostViewHolder.messagePostDeleted.setVisibility(8);
        final MessagingPostViewHolder messagingPostViewHolder2 = messagingPostViewHolder;
        messagingPostViewHolder.messagePostWrapper.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PostDetailsActivity.startPostDetailsActivity(post.post_id, messagingPostViewHolder2.f10906f, post.icon_color);
            }
        });
        messagingPostViewHolder.m10032a(post, messagingPostViewHolder);
        C2699jj.m14618a((View) messagingPostViewHolder.mPostRow, post.icon_color);
        messagingPostViewHolder.mPostCaption.setTypeface(FontFactory.m14101a().mo13970a("JosefinSans-SemiBold.ttf"));
        String str = post.caption;
        if (str != null) {
            messagingPostViewHolder.mPostCaption.setText(C2699jj.m14603a(str, Color.parseColor("#99FFFFFF"), 100, 3));
        } else {
            messagingPostViewHolder.mPostCaption.setVisibility(8);
        }
        boolean z = false;
        if (post.link_domain != null) {
            messagingPostViewHolder.mPostLinkPlacehoder.setVisibility(0);
            messagingPostViewHolder.mPostLinkTitle.setText(post.og_title);
            messagingPostViewHolder.mPostLinkDesc.setText(post.og_desc);
            messagingPostViewHolder.mPostLinkDomain.setText(post.link_domain);
            z = true;
        } else {
            messagingPostViewHolder.mPostLinkPlacehoder.setVisibility(8);
        }
        RoundedCornersTransformation roundedCornersTransformation = new RoundedCornersTransformation(messagingPostViewHolder.f10906f, C2699jj.m14599a(10, messagingPostViewHolder.f10906f), 3, CornerType.TOP);
        String str2 = null;
        boolean z2 = false;
        if (post.thumb_url != null) {
            str2 = post.thumb_url;
            z2 = true;
        } else if (post.source_url != null) {
            str2 = post.source_url;
            z2 = true;
        }
        if (z2) {
            int i = post.width;
            int i2 = post.height;
            if (str2 == null || i == 0 || i2 == 0) {
                Crashlytics.m16437a((Throwable) new Exception("thumb_url and source_url are both null"));
            } else if (z) {
                messagingPostViewHolder.mPostLinkImage.setVisibility(0);
                messagingPostViewHolder.mPostImage.setVisibility(8);
                GossipApplication.f6095c.mo14650a(str2).mo14540a().mo14576h().mo14590a((Transformation<Bitmap>[]) new Transformation[]{roundedCornersTransformation}).mo14559b(DiskCacheStrategy.SOURCE).mo14601a((Target) new ViewTarget<ImageView, GlideDrawable>(messagingPostViewHolder.mPostLinkImage) {
                    /* renamed from: a */
                    public void onResourceReady(GlideDrawable pmVar, GlideAnimation<? super GlideDrawable> rpVar) {
                        ((ImageView) this.view).setImageDrawable(pmVar);
                    }
                });
            } else {
                messagingPostViewHolder.mPostImage.setVisibility(0);
                messagingPostViewHolder.mPostLinkImage.setVisibility(8);
                if (post.localBitmapPath != null) {
                    Options options = new Options();
                    options.inPreferredConfig = Config.ARGB_8888;
                    GossipApplication.f6095c.mo14649a(BitmapFactory.decodeFile(post.localBitmapPath, options)).mo14540a().mo14559b(DiskCacheStrategy.SOURCE).mo14601a((Target) new ViewTarget<ImageView, GlideDrawable>(messagingPostViewHolder.mPostImage) {
                        /* renamed from: a */
                        public void onResourceReady(GlideDrawable pmVar, GlideAnimation<? super GlideDrawable> rpVar) {
                            ((ImageView) this.view).setImageDrawable(pmVar);
                        }
                    });
                    return;
                }
                GossipApplication.f6095c.mo14650a(str2).mo14540a().mo14559b(DiskCacheStrategy.SOURCE).mo14601a((Target) new ViewTarget<ImageView, GlideDrawable>(messagingPostViewHolder.mPostImage) {
                    /* renamed from: a */
                    public void onResourceReady(GlideDrawable pmVar, GlideAnimation<? super GlideDrawable> rpVar) {
                        ((ImageView) this.view).setImageDrawable(pmVar);
                    }
                });
            }
        } else {
            messagingPostViewHolder.mPostLinkImage.setVisibility(8);
            messagingPostViewHolder.mPostImage.setVisibility(8);
        }
    }

    /* renamed from: a */
    private void m10034a(String str, int i, TextView textView, Context context, int i2) {
        CandidAnimals a = CandidAnimals.m14509a();
        textView.setTypeface(CandidAnimals.m14511b());
        textView.setTextColor(i);
        textView.setText(CandidAnimals.m14510a(str));
        textView.setTextSize(1, (float) i2);
        int width = textView.getWidth();
        if (width == 0) {
            width = (int) (textView.getResources().getDisplayMetrics().density * 40.0f);
        }
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(context.getResources().getColor(17170443));
        gradientDrawable.setCornerRadius((float) (width / 2));
        textView.setBackground(gradientDrawable);
        textView.getBackground().setAlpha(192);
        textView.setVisibility(0);
    }
}
