package com.becandid.candid.views;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.BlurLayout$$ViewBinder.C1791a;
import com.becandid.candid.views.QualityScoreInfoView;

public class QualityScoreInfoView$$ViewBinder<T extends QualityScoreInfoView> extends BlurLayout$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.QualityScoreInfoView$$ViewBinder$a */
    /* compiled from: QualityScoreInfoView$$ViewBinder */
    public static class C1846a<T extends QualityScoreInfoView> extends C1791a<T> {
        protected C1846a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            super.mo10764a(t);
            t.mScoreText = null;
            t.mBadgeContainer = null;
            t.mScoreTitle = null;
            t.mScoreDesc = null;
            t.mClosePopup = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1846a aVar = (C1846a) super.bind(finder, t, obj);
        t.mScoreText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.score_text, "field 'mScoreText'"), R.id.score_text, "field 'mScoreText'");
        t.mBadgeContainer = (View) finder.findRequiredView(obj, R.id.badge_container, "field 'mBadgeContainer'");
        t.mScoreTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_title, "field 'mScoreTitle'"), R.id.badge_title, "field 'mScoreTitle'");
        t.mScoreDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_desc, "field 'mScoreDesc'"), R.id.badge_desc, "field 'mScoreDesc'");
        t.mClosePopup = (Button) finder.castView((View) finder.findOptionalView(obj, R.id.got_it_thanks, null), R.id.got_it_thanks, "field 'mClosePopup'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1846a<T> mo10762a(T t) {
        return new C1846a<>(t);
    }
}
