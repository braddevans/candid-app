package com.becandid.candid.views;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.BlurLayout$$ViewBinder.C1791a;
import com.becandid.candid.views.MessagingPopupView;

public class MessagingPopupView$$ViewBinder<T extends MessagingPopupView> extends BlurLayout$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.MessagingPopupView$$ViewBinder$a */
    /* compiled from: MessagingPopupView$$ViewBinder */
    public static class C1830a<T extends MessagingPopupView> extends C1791a<T> {
        protected C1830a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            super.mo10764a(t);
            t.mMessageContainerEnabled = null;
            t.mKeepEnabledTextView = null;
            t.mDisabledTextView = null;
            t.mBadgeIcon = null;
            t.mManualDeleteMsgContainer = null;
            t.mAutoDeleteMsgContainer = null;
            t.mManualCheckmark = null;
            t.mAutoCheckmark = null;
            t.mFinishBtn = null;
            t.mMessageCloseBtn = null;
            t.badgeDescEnabled = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1830a aVar = (C1830a) super.bind(finder, t, obj);
        t.mMessageContainerEnabled = (View) finder.findRequiredView(obj, R.id.message_container_enabled, "field 'mMessageContainerEnabled'");
        t.mKeepEnabledTextView = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.keep_enabled, "field 'mKeepEnabledTextView'"), R.id.keep_enabled, "field 'mKeepEnabledTextView'");
        t.mDisabledTextView = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.disabled, "field 'mDisabledTextView'"), R.id.disabled, "field 'mDisabledTextView'");
        t.mBadgeIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_icon, "field 'mBadgeIcon'"), R.id.badge_icon, "field 'mBadgeIcon'");
        t.mManualDeleteMsgContainer = (View) finder.findRequiredView(obj, R.id.manual_delete_message, "field 'mManualDeleteMsgContainer'");
        t.mAutoDeleteMsgContainer = (View) finder.findRequiredView(obj, R.id.auto_delete_message, "field 'mAutoDeleteMsgContainer'");
        t.mManualCheckmark = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.manual_checkmark, "field 'mManualCheckmark'"), R.id.manual_checkmark, "field 'mManualCheckmark'");
        t.mAutoCheckmark = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.auto_checkmark, "field 'mAutoCheckmark'"), R.id.auto_checkmark, "field 'mAutoCheckmark'");
        t.mFinishBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.finish, "field 'mFinishBtn'"), R.id.finish, "field 'mFinishBtn'");
        t.mMessageCloseBtn = (View) finder.findRequiredView(obj, R.id.message_close, "field 'mMessageCloseBtn'");
        t.badgeDescEnabled = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_desc_enabled, "field 'badgeDescEnabled'"), R.id.badge_desc_enabled, "field 'badgeDescEnabled'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1830a<T> mo10762a(T t) {
        return new C1830a<>(t);
    }
}
