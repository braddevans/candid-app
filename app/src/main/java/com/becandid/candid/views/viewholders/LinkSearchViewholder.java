package com.becandid.candid.views.viewholders;

import android.support.p003v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LinkSearchViewholder extends ViewHolder {
    @BindView(2131624721)
    public TextView linkDesc;
    @BindView(2131624722)
    public TextView linkHost;
    @BindView(2131624718)
    public ImageView linkImage;
    @BindView(2131624719)
    public ImageView linkSearchResultIcon;
    @BindView(2131624720)
    public TextView linkTitle;

    public LinkSearchViewholder(View view) {
        super(view);
        ButterKnife.bind((Object) this, view);
    }
}
