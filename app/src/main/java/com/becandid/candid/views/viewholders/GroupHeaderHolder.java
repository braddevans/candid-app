package com.becandid.candid.views.viewholders;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.p003v7.widget.PopupMenu;
import android.support.p003v7.widget.PopupMenu.OnMenuItemClickListener;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.GroupDetailsActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Post;
import com.becandid.candid.util.RoundedCornersTransformation;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class GroupHeaderHolder extends BaseViewHolder {
    @BindView(2131624540)
    TextView groupAbout;
    @BindView(2131624530)
    RelativeLayout groupHeader;
    @BindView(2131624533)
    ImageView groupImage;
    @BindView(2131624534)
    TextView groupJoin;
    @BindView(2131624535)
    TextView groupLeave;
    @BindView(2131624531)
    ImageView groupMenu;
    @BindView(2131624541)
    TextView groupStats;

    public GroupHeaderHolder(View view, Activity activity) {
        super(view, activity);
    }

    /* renamed from: a */
    public void mo10946a(final GroupHeaderHolder groupHeaderHolder, final Post post) {
        int[] iArr = {R.id.group_tag_0, R.id.group_tag_1, R.id.group_tag_2};
        for (int i = 0; i < iArr.length; i++) {
            TextView textView = (TextView) this.groupHeader.findViewById(iArr[i]);
            if (i < post.tags.size()) {
                textView.setText(bih.m8245a((String) post.tags.get(i)));
                textView.setVisibility(0);
            } else {
                textView.setVisibility(8);
            }
        }
        this.groupAbout.setText(post.about);
        this.groupAbout.setMaxLines(AppState.config.getInt("max_group_desc_lines", 7));
        StringBuilder sb = new StringBuilder(50);
        sb.append(post.num_posts + " POST");
        if (post.num_posts != 1) {
            sb.append("S");
        }
        sb.append("  |  ");
        sb.append(post.num_members + " MEMBER");
        if (post.num_members != 1) {
            sb.append("S");
        }
        this.groupStats.setText(sb.toString());
        if (post.groupRoundedImage == null || post.groupBackgroundDrawable == null) {
            GossipApplication.f6095c.mo14650a(post.imageUrl()).mo14598j().mo14540a().mo14572d(17301613).mo14559b(DiskCacheStrategy.ALL).mo14601a((Target) new SimpleTarget<Bitmap>() {
                /* renamed from: a */
                public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> rpVar) {
                    post.groupRoundedImage = (Bitmap) new RoundedCornersTransformation(GroupHeaderHolder.this.f10906f, (int) (60.0f * GroupHeaderHolder.this.f10906f.getResources().getDisplayMetrics().density), 0, CornerType.ALL).mo10703a(bitmap, GroupHeaderHolder.this.groupImage.getWidth(), GroupHeaderHolder.this.groupImage.getHeight()).mo14377d();
                    GroupHeaderHolder.this.groupImage.setImageBitmap(post.groupRoundedImage);
                    Bitmap a = BitmapUtils.m14507a(bitmap, GroupHeaderHolder.this.groupHeader.getWidth(), GroupHeaderHolder.this.groupHeader.getHeight());
                    if (a != null) {
                        post.groupBackgroundDrawable = a;
                        GroupHeaderHolder.this.groupHeader.setBackground(new BitmapDrawable(GroupHeaderHolder.this.f10906f.getResources(), post.groupBackgroundDrawable));
                    }
                }
            });
        } else {
            this.groupImage.setImageBitmap(post.groupRoundedImage);
            this.groupHeader.setBackground(new BitmapDrawable(this.f10906f.getResources(), post.groupBackgroundDrawable));
        }
        if (post.isMember() || post.moderator == 1) {
            this.groupLeave.setVisibility(0);
            this.groupJoin.setVisibility(8);
        } else {
            this.groupLeave.setVisibility(8);
            this.groupJoin.setVisibility(0);
        }
        this.groupJoin.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (GroupHeaderHolder.this.f10906f instanceof GroupDetailsActivity) {
                    ((GroupDetailsActivity) GroupHeaderHolder.this.f10906f).joinGroupClick(view);
                }
                GroupHeaderHolder.this.groupLeave.setText("Leave");
                GroupHeaderHolder.this.groupLeave.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if (GroupHeaderHolder.this.f10906f instanceof GroupDetailsActivity) {
                            ((GroupDetailsActivity) GroupHeaderHolder.this.f10906f).leaveGroupClick(view);
                        }
                    }
                });
            }
        });
        ((BaseActivity) this.f10906f).addToSubscriptionList(RxBus.m14573a().mo14348a(C2659p.class, (bjy<T>) new bjy<C2659p>() {
            /* renamed from: a */
            public void onNext(C2659p pVar) {
                if (pVar.f10653a.group_id == post.group_id) {
                    GroupHeaderHolder.this.groupJoin.setVisibility(8);
                    GroupHeaderHolder.this.groupLeave.setVisibility(0);
                    GroupHeaderHolder.this.groupLeave.setEnabled(true);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        if (post.community_membership == 1) {
            this.groupLeave.setText("Community");
        }
        this.groupLeave.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (!(GroupHeaderHolder.this.f10906f instanceof GroupDetailsActivity)) {
                    return;
                }
                if (post.community_membership == 1) {
                    ((GroupDetailsActivity) GroupHeaderHolder.this.f10906f).editCommunityClick(view);
                } else {
                    ((GroupDetailsActivity) GroupHeaderHolder.this.f10906f).leaveGroupClick(view);
                }
            }
        });
        ((BaseActivity) this.f10906f).addToSubscriptionList(RxBus.m14573a().mo14348a(C2661r.class, (bjy<T>) new bjy<C2661r>() {
            /* renamed from: a */
            public void onNext(C2661r rVar) {
                if (rVar.f10656a == post.group_id) {
                    GroupHeaderHolder.this.groupLeave.setVisibility(8);
                    GroupHeaderHolder.this.groupJoin.setVisibility(0);
                    GroupHeaderHolder.this.groupJoin.setEnabled(true);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        this.groupMenu.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                popupMenu.setOnMenuItemClickListener((OnMenuItemClickListener) groupHeaderHolder.f10906f);
                if (post.moderator == 1 && post.num_posts == 0) {
                    popupMenu.getMenu().add(1, R.id.group_menu_delete, 0, R.string.delete_group);
                    popupMenu.getMenu().add(1, R.id.group_menu_edit, 0, R.string.edit_group);
                } else if (post.moderator == 1) {
                    popupMenu.getMenu().add(1, R.id.group_menu_edit, 0, R.string.edit_group);
                } else {
                    popupMenu.getMenu().add(1, R.id.group_menu_report, 0, R.string.report_group);
                }
                popupMenu.show();
            }
        });
        this.groupHeader.measure(MeasureSpec.makeMeasureSpec(this.f10906f.getResources().getDisplayMetrics().widthPixels, 1073741824), 0);
    }
}
