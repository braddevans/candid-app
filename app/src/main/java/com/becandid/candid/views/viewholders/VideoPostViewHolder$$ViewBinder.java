package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.BasePostViewHolder$$ViewBinder.C1898a;
import com.becandid.candid.views.viewholders.VideoPostViewHolder;
import com.volokh.danylo.video_player_manager.p008ui.VideoPlayerView;

public class VideoPostViewHolder$$ViewBinder<T extends VideoPostViewHolder> extends BasePostViewHolder$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.VideoPostViewHolder$$ViewBinder$a */
    /* compiled from: VideoPostViewHolder$$ViewBinder */
    public static class C1924a<T extends VideoPostViewHolder> extends C1898a<T> {
        protected C1924a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10942a(T t) {
            super.mo10942a(t);
            t.postVideoPlayer = null;
            t.postVideoCover = null;
            t.postVideoPlayBtn = null;
            t.postVideoPlaceholder = null;
            t.postVideoProgressBar = null;
            t.postVideoVerified = null;
            t.postVideoRow = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1924a aVar = (C1924a) super.bind(finder, t, obj);
        t.postVideoPlayer = (VideoPlayerView) finder.castView((View) finder.findRequiredView(obj, R.id.post_video_player, "field 'postVideoPlayer'"), R.id.post_video_player, "field 'postVideoPlayer'");
        t.postVideoCover = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_video_cover, "field 'postVideoCover'"), R.id.post_video_cover, "field 'postVideoCover'");
        t.postVideoPlayBtn = (View) finder.findRequiredView(obj, R.id.post_video_play_btn, "field 'postVideoPlayBtn'");
        t.postVideoPlaceholder = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_video_frame, "field 'postVideoPlaceholder'"), R.id.post_video_frame, "field 'postVideoPlaceholder'");
        t.postVideoProgressBar = (ProgressBar) finder.castView((View) finder.findRequiredView(obj, R.id.post_video_loading, "field 'postVideoProgressBar'"), R.id.post_video_loading, "field 'postVideoProgressBar'");
        t.postVideoVerified = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_video_verified, "field 'postVideoVerified'"), R.id.post_video_verified, "field 'postVideoVerified'");
        t.postVideoRow = (View) finder.findRequiredView(obj, R.id.post_video_row, "field 'postVideoRow'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1924a<T> mo10941a(T t) {
        return new C1924a<>(t);
    }
}
