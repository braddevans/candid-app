package com.becandid.candid.views;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.PopupWithBlurBackgroundActivity;

public class QualityScoreInfoView extends BlurLayout {
    @BindView(2131624079)
    View mBadgeContainer;
    @BindView(2131625084)
    Button mClosePopup;
    @BindView(2131624081)
    TextView mScoreDesc;
    @BindView(2131624731)
    TextView mScoreText;
    @BindView(2131624080)
    TextView mScoreTitle;

    public QualityScoreInfoView(Activity activity, String str, String str2) {
        super(activity);
        this.f7290a = str;
        LayoutInflater.from(activity).inflate(R.layout.me_quality_score_info, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mBadgeContainer, "#ffffff");
        this.mScoreText.setText(str2);
    }

    public QualityScoreInfoView(final Activity activity, String str, String str2, String str3, String str4) {
        super(activity);
        this.f7290a = str;
        LayoutInflater.from(activity).inflate(R.layout.post_quality_score_info, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mBadgeContainer, "#ffffff");
        this.mScoreText.setText(str2);
        this.mScoreTitle.setText(str3);
        this.mScoreDesc.setText(str4);
        this.mClosePopup.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ((PopupWithBlurBackgroundActivity) activity).slideOutAnimation(view);
            }
        });
    }
}
