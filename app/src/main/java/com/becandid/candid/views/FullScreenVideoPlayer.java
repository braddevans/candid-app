package com.becandid.candid.views;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.MessageActivity;

public class FullScreenVideoPlayer extends FrameLayout {
    @BindView(2131624187)
    View mFullScreenExit;
    @BindView(2131624184)
    FrameLayout mVideoPlaceholder;
    @BindView(2131624188)
    LinearLayout mVideoProgressBar;
    @BindView(2131624185)
    VideoView mVideoView;

    public FullScreenVideoPlayer(Context context) {
        super(context);
    }

    public FullScreenVideoPlayer(Context context, String str, final int i, int i2, int i3, int i4) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.activity_full_screen_video, this, true);
        ButterKnife.bind((View) this);
        if (str == null) {
            m9834a();
            return;
        }
        HttpProxyCacheServer a = GossipApplication.m9013a(context);
        a.mo16743a((CacheListener) (MessageActivity) context, str);
        this.mVideoView.setVideoPath(a.mo16740a(str));
        final MediaController mediaController = new MediaController(context);
        mediaController.setAnchorView(this.mVideoView);
        this.mVideoView.setMediaController(mediaController);
        this.mVideoView.requestFocus();
        if (!(i3 == 0 || i2 == 0)) {
            LayoutParams layoutParams = (LayoutParams) this.mVideoPlaceholder.getLayoutParams();
            layoutParams.width = C2699jj.m14599a(i2, context);
            layoutParams.height = C2699jj.m14599a(i3, context);
            this.mVideoPlaceholder.setLayoutParams(layoutParams);
        }
        this.mVideoProgressBar.setVisibility(0);
        this.mVideoPlaceholder.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                FullScreenVideoPlayer.this.mVideoPlaceholder.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                FullScreenVideoPlayer.this.mVideoView.setOnPreparedListener(new OnPreparedListener() {
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        FullScreenVideoPlayer.this.mVideoProgressBar.setVisibility(8);
                        FullScreenVideoPlayer.this.mVideoView.start();
                        FullScreenVideoPlayer.this.mVideoView.seekTo(i);
                        mediaController.setVisibility(0);
                        mediaPlayer.setLooping(true);
                    }
                });
            }
        });
        if (i4 == 0) {
        }
        this.mFullScreenExit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FullScreenVideoPlayer.this.m9834a();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9834a() {
        RxBus.m14573a().mo14349a(new C2641bc());
    }
}
