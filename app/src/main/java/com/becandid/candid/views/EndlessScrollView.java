package com.becandid.candid.views;

import android.content.Context;
import android.support.p001v4.widget.NestedScrollView;
import android.util.AttributeSet;

public class EndlessScrollView extends NestedScrollView {

    /* renamed from: a */
    private C1809a f7328a;

    /* renamed from: com.becandid.candid.views.EndlessScrollView$a */
    public interface C1809a {
        /* renamed from: a */
        void mo10801a(EndlessScrollView endlessScrollView, int i, int i2, int i3, int i4);
    }

    public EndlessScrollView(Context context) {
        super(context);
    }

    public EndlessScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public EndlessScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (this.f7328a != null) {
            this.f7328a.mo10801a(this, i, i2, i3, i4);
        }
    }

    public void setScrollViewListener(C1809a aVar) {
        this.f7328a = aVar;
    }
}
