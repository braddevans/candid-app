package com.becandid.candid.views;

import android.support.p003v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.HeaderView;

public class HeaderView$$ViewBinder<T extends HeaderView> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.HeaderView$$ViewBinder$a */
    /* compiled from: HeaderView$$ViewBinder */
    public static class C1823a<T extends HeaderView> implements Unbinder {

        /* renamed from: a */
        private T f7385a;

        protected C1823a(T t) {
            this.f7385a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10841a(T t) {
            t.mToolbar = null;
            t.mToolbarText = null;
        }

        public final void unbind() {
            if (this.f7385a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10841a(this.f7385a);
            this.f7385a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1823a a = mo10840a(t);
        t.mToolbar = (Toolbar) finder.castView((View) finder.findRequiredView(obj, R.id.toolbar, "field 'mToolbar'"), R.id.toolbar, "field 'mToolbar'");
        t.mToolbarText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.toolbar_title, "field 'mToolbarText'"), R.id.toolbar_title, "field 'mToolbarText'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1823a<T> mo10840a(T t) {
        return new C1823a<>(t);
    }
}
