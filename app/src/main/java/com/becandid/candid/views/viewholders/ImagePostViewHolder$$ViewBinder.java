package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.BasePostViewHolder$$ViewBinder.C1898a;
import com.becandid.candid.views.viewholders.ImagePostViewHolder;

public class ImagePostViewHolder$$ViewBinder<T extends ImagePostViewHolder> extends BasePostViewHolder$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.ImagePostViewHolder$$ViewBinder$a */
    /* compiled from: ImagePostViewHolder$$ViewBinder */
    public static class C1910a<T extends ImagePostViewHolder> extends C1898a<T> {
        protected C1910a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10942a(T t) {
            super.mo10942a(t);
            t.mPostCaption = null;
            t.mPostImage = null;
            t.mPostImageUploaded = null;
            t.mPostImageRow = null;
            t.mPostLoadingImageProgress = null;
            t.mPostImageContainer = null;
            t.mPostLoadingImageProgressContainer = null;
            t.mPostGifPlay = null;
            t.mGifImgPlaceholder = null;
            t.mPostImageImage = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1910a aVar = (C1910a) super.bind(finder, t, obj);
        t.mPostCaption = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_caption, "field 'mPostCaption'"), R.id.post_caption, "field 'mPostCaption'");
        t.mPostImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_image, "field 'mPostImage'"), R.id.post_image, "field 'mPostImage'");
        t.mPostImageUploaded = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_image_verified, "field 'mPostImageUploaded'"), R.id.post_image_verified, "field 'mPostImageUploaded'");
        t.mPostImageRow = (View) finder.findRequiredView(obj, R.id.post_image_row, "field 'mPostImageRow'");
        t.mPostLoadingImageProgress = (View) finder.findRequiredView(obj, R.id.post_loading_image_progress, "field 'mPostLoadingImageProgress'");
        t.mPostImageContainer = (View) finder.findRequiredView(obj, R.id.post_image_container, "field 'mPostImageContainer'");
        t.mPostLoadingImageProgressContainer = (View) finder.findRequiredView(obj, R.id.post_loading_image_progress_container, "field 'mPostLoadingImageProgressContainer'");
        t.mPostGifPlay = (View) finder.findRequiredView(obj, R.id.post_gif_play, "field 'mPostGifPlay'");
        t.mGifImgPlaceholder = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.gif_img_placeholder, "field 'mGifImgPlaceholder'"), R.id.gif_img_placeholder, "field 'mGifImgPlaceholder'");
        t.mPostImageImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_image_image, "field 'mPostImageImage'"), R.id.post_image_image, "field 'mPostImageImage'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1910a<T> mo10941a(T t) {
        return new C1910a<>(t);
    }
}
