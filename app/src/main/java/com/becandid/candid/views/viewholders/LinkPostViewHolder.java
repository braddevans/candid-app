package com.becandid.candid.views.viewholders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.PostDetailsActivity;
import com.becandid.candid.activities.WebViewActivity;
import com.becandid.candid.activities.YouTubePlayerActivity;
import com.becandid.candid.data.Post;
import com.becandid.candid.util.RoundedCornersTransformation;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class LinkPostViewHolder extends BasePostViewHolder {

    /* renamed from: a */
    static RoundedCornersTransformation f7527a;
    @BindView(2131624156)
    TextView mPostCaption;
    @BindView(2131624558)
    ImageView mPostImageImage;
    @BindView(2131624562)
    View mPostImageRow;
    @BindView(2131624574)
    View mPostInfoImage;
    @BindView(2131624551)
    TextView mPostLinkDesc;
    @BindView(2131624576)
    TextView mPostLinkDescImage;
    @BindView(2131624552)
    TextView mPostLinkDomain;
    @BindView(2131624577)
    TextView mPostLinkDomainImage;
    @BindView(2131624549)
    ImageView mPostLinkImage;
    @BindView(2131624548)
    View mPostLinkPlacehoder;
    @BindView(2131624570)
    View mPostLinkRow;
    @BindView(2131624550)
    TextView mPostLinkTitle;
    @BindView(2131624575)
    TextView mPostLinkTitleImage;
    @BindView(2131624578)
    View mPostYoutubeIcon;
    @BindView(2131624168)
    LinearLayout mPostYoutubePlaceholder;
    @BindView(2131624573)
    TextView mYoutubeDesc;
    @BindView(2131624571)
    ImageView mYoutubeThumb;
    @BindView(2131624572)
    TextView mYoutubeTitle;

    public LinkPostViewHolder(View view, Activity activity) {
        super(view, activity);
        m9979a(activity);
    }

    public LinkPostViewHolder(View view, Activity activity, String str) {
        super(view, activity, str);
        m9979a(activity);
    }

    /* renamed from: a */
    private void m9979a(Context context) {
        f7527a = new RoundedCornersTransformation(context, (int) (10.0f * context.getResources().getDisplayMetrics().density), 3, CornerType.TOP);
    }

    /* renamed from: a */
    private static void m9980a(Post post, LinkPostViewHolder linkPostViewHolder) {
        final Context context = linkPostViewHolder.f10906f;
        linkPostViewHolder.mYoutubeTitle.setText(post.og_title);
        linkPostViewHolder.mYoutubeDesc.setText(post.og_desc);
        final Post post2 = post;
        linkPostViewHolder.mPostYoutubePlaceholder.setOnClickListener(new OnClickListener(linkPostViewHolder) {

            /* renamed from: a */
            final /* synthetic */ LinkPostViewHolder f7528a;

            {
                this.f7528a = r1;
            }

            public void onClick(View view) {
                Intent intent = new Intent(this.f7528a.f10906f, YouTubePlayerActivity.class);
                intent.putExtra("youtube_video_id", post2.youtube_video_id);
                this.f7528a.f10906f.startActivity(intent);
            }
        });
        if (post.actual_height == -1 && post.actual_width == -1) {
            Display defaultDisplay = ((WindowManager) linkPostViewHolder.f10906f.getSystemService("window")).getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            int i = point.x - (((int) (5.0f * (((float) linkPostViewHolder.f10906f.getResources().getDisplayMetrics().densityDpi) / 160.0f))) * 2);
            int i2 = (i * 9) / 16;
            post.actual_width = i;
            post.actual_height = i2;
        }
        linkPostViewHolder.mYoutubeThumb.setLayoutParams(new LayoutParams(post.actual_width, post.actual_height));
        final LinkPostViewHolder linkPostViewHolder2 = linkPostViewHolder;
        GossipApplication.f6095c.mo14657d().mo14598j().mo14577a(new PaletteBitmapTranscoder(context), PaletteBitmap.class).mo14559b(DiskCacheStrategy.SOURCE).mo14560b("https://img.youtube.com/vi/" + post.youtube_video_id + "/0.jpg").mo14540a().mo14558b(Priority.IMMEDIATE).mo14601a((Target) new ImageViewTarget<PaletteBitmap>(linkPostViewHolder.mYoutubeThumb) {
            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void setResource(PaletteBitmap jkVar) {
                ((ImageView) this.view).setImageBitmap(jkVar.f10824b);
                int darkMutedColor = jkVar.f10823a.getDarkMutedColor(ContextCompat.getColor(context, R.color.gossip_grey));
                C2699jj.m14616a(linkPostViewHolder2.mPostImageRow, darkMutedColor);
                linkPostViewHolder2.mPostIcon.setTextColor(darkMutedColor);
                linkPostViewHolder2.mPostHeaderBadgeScore.setTextColor(darkMutedColor);
                linkPostViewHolder2.mPostGroup.setTextColor(darkMutedColor);
                Triangle klVar = new Triangle();
                klVar.mo14475a(darkMutedColor);
                klVar.setBounds(0, C2699jj.m14599a(1, context), C2699jj.m14599a(10, context), C2699jj.m14599a(6, context));
                linkPostViewHolder2.mPostGroupMenu.setBackground(klVar);
                linkPostViewHolder2.mFooterCommentIcon.setTextColor(darkMutedColor);
                linkPostViewHolder2.mPostHeaderBadgeText.setTextColor(darkMutedColor);
                Drawable drawable = linkPostViewHolder2.f10906f.getResources().getDrawable(R.drawable.dot_circle);
                drawable.setColorFilter(darkMutedColor, Mode.SRC_IN);
                linkPostViewHolder2.mPostHeaderBadgeDot.setBackground(drawable);
            }
        });
    }

    /* renamed from: a */
    public static void m9981a(LinkPostViewHolder linkPostViewHolder, Post post) {
        BasePostViewHolder.m9934a((BasePostViewHolder) linkPostViewHolder, post);
        C2699jj.m14618a(linkPostViewHolder.mPostImageRow, post.icon_color);
        if (post.caption == null || post.caption.isEmpty()) {
            linkPostViewHolder.mPostCaption.setVisibility(8);
        } else {
            linkPostViewHolder.mPostCaption.setVisibility(0);
        }
        if (post.youtube_video_id != null) {
            linkPostViewHolder.mPostYoutubePlaceholder.setVisibility(0);
            linkPostViewHolder.mPostLinkPlacehoder.setVisibility(8);
            m9980a(post, linkPostViewHolder);
            return;
        }
        linkPostViewHolder.mPostYoutubePlaceholder.setVisibility(8);
        linkPostViewHolder.mPostLinkPlacehoder.setVisibility(0);
        m9982b(post, linkPostViewHolder);
    }

    /* renamed from: b */
    private static void m9982b(Post post, LinkPostViewHolder linkPostViewHolder) {
        final Context context = linkPostViewHolder.f10906f;
        linkPostViewHolder.mPostLinkTitle.setText(post.og_title);
        linkPostViewHolder.mPostLinkDesc.setText(post.og_desc);
        linkPostViewHolder.mPostLinkDomain.setText(post.link_domain);
        final Post post2 = post;
        linkPostViewHolder.mPostLinkRow.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (!(context instanceof PostDetailsActivity)) {
                    PostDetailsActivity.startPostDetailsActivity(post2.post_id, context, post2.icon_color);
                }
            }
        });
        final Post post3 = post;
        final LinkPostViewHolder linkPostViewHolder2 = linkPostViewHolder;
        linkPostViewHolder.mPostLinkPlacehoder.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                String str = post3.link_url;
                if (str == null || str.isEmpty()) {
                    Crashlytics.m16437a((Throwable) new Exception("link url is null or empty"));
                    return;
                }
                Intent intent = new Intent(linkPostViewHolder2.f10906f, WebViewActivity.class);
                intent.putExtra("title", post3.og_title);
                intent.putExtra("url", str);
                linkPostViewHolder2.f10906f.startActivity(intent);
            }
        });
        String str = post.thumb_url != null ? post.thumb_url : post.source_url;
        if (str == null) {
            Crashlytics.m16437a((Throwable) new Exception("thumb_url and source_url are both null"));
            linkPostViewHolder.mPostLinkImage.setVisibility(8);
            Glide.m15043a((View) linkPostViewHolder.mPostLinkImage);
            return;
        }
        if (post.actual_height == -1 && post.actual_width == -1) {
            Display defaultDisplay = ((WindowManager) linkPostViewHolder.f10906f.getSystemService("window")).getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            int i = point.x - (((int) (15.0f * (((float) linkPostViewHolder.f10906f.getResources().getDisplayMetrics().densityDpi) / 160.0f))) * 2);
            int i2 = (int) (((double) i) * 0.55d);
            post.actual_width = i;
            post.actual_height = i2;
        }
        linkPostViewHolder.mPostLinkImage.setVisibility(0);
        linkPostViewHolder.mPostLinkImage.setLayoutParams(new LinearLayout.LayoutParams(post.actual_width, post.actual_height));
        final LinkPostViewHolder linkPostViewHolder3 = linkPostViewHolder;
        GossipApplication.f6095c.mo14657d().mo14598j().mo14577a(new PaletteBitmapTranscoder(context), PaletteBitmap.class).mo14559b(DiskCacheStrategy.SOURCE).mo14560b(str).mo14567b((Transformation<Bitmap>[]) new Transformation[]{f7527a}).mo14558b(Priority.IMMEDIATE).mo14601a((Target) new ImageViewTarget<PaletteBitmap>(linkPostViewHolder.mPostLinkImage) {
            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void setResource(PaletteBitmap jkVar) {
                ((ImageView) this.view).setImageBitmap(jkVar.f10824b);
                int darkMutedColor = jkVar.f10823a.getDarkMutedColor(ContextCompat.getColor(context, R.color.gossip_grey));
                C2699jj.m14616a(linkPostViewHolder3.mPostImageRow, darkMutedColor);
                linkPostViewHolder3.mPostIcon.setTextColor(darkMutedColor);
                linkPostViewHolder3.mPostHeaderBadgeScore.setTextColor(darkMutedColor);
                linkPostViewHolder3.mPostGroup.setTextColor(darkMutedColor);
                Triangle klVar = new Triangle();
                klVar.mo14475a(darkMutedColor);
                klVar.setBounds(0, C2699jj.m14599a(1, context), C2699jj.m14599a(10, context), C2699jj.m14599a(6, context));
                linkPostViewHolder3.mPostGroupMenu.setBackground(klVar);
                linkPostViewHolder3.mFooterCommentIcon.setTextColor(darkMutedColor);
                linkPostViewHolder3.mPostHeaderBadgeText.setTextColor(darkMutedColor);
                Drawable drawable = linkPostViewHolder3.f10906f.getResources().getDrawable(R.drawable.dot_circle);
                drawable.setColorFilter(darkMutedColor, Mode.SRC_IN);
                linkPostViewHolder3.mPostHeaderBadgeDot.setBackground(drawable);
            }
        });
    }

    /* renamed from: a */
    public void mo10896a(BasePostViewHolder basePostViewHolder, Post post, int i) {
        super.mo10896a(basePostViewHolder, post, i);
        LinkPostViewHolder linkPostViewHolder = (LinkPostViewHolder) basePostViewHolder;
        Display defaultDisplay = ((WindowManager) basePostViewHolder.f10906f.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        int i2 = point.x;
        if (post.youtube_video_id == null) {
            linkPostViewHolder.mPostLinkTitleImage.setText(post.og_title);
            linkPostViewHolder.mPostLinkDescImage.setText(post.og_desc);
            linkPostViewHolder.mPostLinkDomainImage.setText(post.link_domain);
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setShape(0);
            gradientDrawable.setColor(C2699jj.f10798a);
            linkPostViewHolder.mPostInfoImage.setBackground(gradientDrawable);
            linkPostViewHolder.mPostImageImage.setImageDrawable(linkPostViewHolder.mPostLinkImage.getDrawable().getCurrent());
            linkPostViewHolder.mPostImageImage.setLayoutParams(new LayoutParams(i2, post.actual_height));
            linkPostViewHolder.mPostImageImage.setScaleType(ScaleType.FIT_XY);
            return;
        }
        linkPostViewHolder.mPostImageImage.setImageDrawable(linkPostViewHolder.mYoutubeThumb.getDrawable().getCurrent());
        linkPostViewHolder.mPostImageImage.setLayoutParams(new LayoutParams(i2, post.actual_height));
        linkPostViewHolder.mPostImageImage.setScaleType(ScaleType.FIT_XY);
        linkPostViewHolder.mPostYoutubeIcon.setVisibility(0);
    }
}
