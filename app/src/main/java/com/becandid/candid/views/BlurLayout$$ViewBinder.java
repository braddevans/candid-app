package com.becandid.candid.views;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.BlurLayout;

public class BlurLayout$$ViewBinder<T extends BlurLayout> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.BlurLayout$$ViewBinder$a */
    /* compiled from: BlurLayout$$ViewBinder */
    public static class C1791a<T extends BlurLayout> implements Unbinder {

        /* renamed from: a */
        private T f7292a;

        protected C1791a(T t) {
            this.f7292a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            t.mBlurImage = null;
            t.mBackgroundContainer = null;
        }

        public final void unbind() {
            if (this.f7292a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10764a(this.f7292a);
            this.f7292a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1791a a = mo10762a(t);
        t.mBlurImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.blur_image, "field 'mBlurImage'"), R.id.blur_image, "field 'mBlurImage'");
        t.mBackgroundContainer = (View) finder.findRequiredView(obj, R.id.container, "field 'mBackgroundContainer'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1791a<T> mo10762a(T t) {
        return new C1791a<>(t);
    }
}
