package com.becandid.candid.views;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.GroupHeaderView;

public class GroupHeaderView$$ViewBinder<T extends GroupHeaderView> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.GroupHeaderView$$ViewBinder$a */
    /* compiled from: GroupHeaderView$$ViewBinder */
    public static class C1816a<T extends GroupHeaderView> implements Unbinder {

        /* renamed from: a */
        View f7339a;

        /* renamed from: b */
        View f7340b;

        /* renamed from: c */
        private T f7341c;

        protected C1816a(T t) {
            this.f7341c = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10813a(T t) {
            this.f7339a.setOnClickListener(null);
            this.f7340b.setOnClickListener(null);
        }

        public final void unbind() {
            if (this.f7341c == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10813a(this.f7341c);
            this.f7341c = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1816a a = mo10812a(t);
        View view = (View) finder.findRequiredView(obj, R.id.group_search_btn, "method 'onGroupSearchClick'");
        a.f7339a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onGroupSearchClick(view);
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.create_group_btn, "method 'onCreateGroupClick'");
        a.f7340b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCreateGroupClick(view);
            }
        });
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1816a<T> mo10812a(T t) {
        return new C1816a<>(t);
    }
}
