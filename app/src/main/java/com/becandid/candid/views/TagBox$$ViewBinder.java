package com.becandid.candid.views;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.TagBox;

public class TagBox$$ViewBinder<T extends TagBox> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.TagBox$$ViewBinder$a */
    /* compiled from: TagBox$$ViewBinder */
    public static class C1852a<T extends TagBox> implements Unbinder {

        /* renamed from: a */
        private T f7426a;

        protected C1852a(T t) {
            this.f7426a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10894a(T t) {
            t.mTagIcon = null;
            t.mTagName = null;
        }

        public final void unbind() {
            if (this.f7426a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10894a(this.f7426a);
            this.f7426a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1852a a = mo10893a(t);
        t.mTagIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.tag_icon, "field 'mTagIcon'"), R.id.tag_icon, "field 'mTagIcon'");
        t.mTagName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.tag_name, "field 'mTagName'"), R.id.tag_name, "field 'mTagName'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1852a<T> mo10893a(T t) {
        return new C1852a<>(t);
    }
}
