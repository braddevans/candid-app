package com.becandid.candid.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.PopupWithBlurBackgroundActivity;

public class CommunityPopupView extends BlurLayout {

    /* renamed from: b */
    PopupWithBlurBackgroundActivity f7326b;
    @BindView(2131624479)
    View mMessageContainer;
    @BindView(2131624482)
    Button mOkBtn;

    public CommunityPopupView(Context context, String str, boolean z) {
        super(context);
        this.f7326b = (PopupWithBlurBackgroundActivity) context;
        this.f7290a = str;
        LayoutInflater.from(context).inflate(R.layout.community_popup_view, this, true);
        ButterKnife.bind((View) this);
        mo10765a();
        C2699jj.m14618a(this.mMessageContainer, "#ffffff");
        this.mOkBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CommunityPopupView.this.m9826b();
                RxBus.m14573a().mo14349a(new C2649f("community"));
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9826b() {
        FrameLayout frameLayout = (FrameLayout) this.f7326b.findViewById(R.id.popup_info_container);
        if (frameLayout != null) {
            this.f7326b.slideOutAnimation(frameLayout);
        }
    }
}
