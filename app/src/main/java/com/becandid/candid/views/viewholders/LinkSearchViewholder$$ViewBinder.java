package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.LinkSearchViewholder;

public class LinkSearchViewholder$$ViewBinder<T extends LinkSearchViewholder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.LinkSearchViewholder$$ViewBinder$a */
    /* compiled from: LinkSearchViewholder$$ViewBinder */
    public static class C1917a<T extends LinkSearchViewholder> implements Unbinder {

        /* renamed from: a */
        private T f7538a;

        protected C1917a(T t) {
            this.f7538a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10973a(T t) {
            t.linkImage = null;
            t.linkDesc = null;
            t.linkTitle = null;
            t.linkHost = null;
            t.linkSearchResultIcon = null;
        }

        public final void unbind() {
            if (this.f7538a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10973a(this.f7538a);
            this.f7538a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1917a a = mo10972a(t);
        t.linkImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.link_search_result_image, "field 'linkImage'"), R.id.link_search_result_image, "field 'linkImage'");
        t.linkDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.link_search_result_desc, "field 'linkDesc'"), R.id.link_search_result_desc, "field 'linkDesc'");
        t.linkTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.link_search_result_title, "field 'linkTitle'"), R.id.link_search_result_title, "field 'linkTitle'");
        t.linkHost = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.link_search_result_host, "field 'linkHost'"), R.id.link_search_result_host, "field 'linkHost'");
        t.linkSearchResultIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.link_search_result_icon, "field 'linkSearchResultIcon'"), R.id.link_search_result_icon, "field 'linkSearchResultIcon'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1917a<T> mo10972a(T t) {
        return new C1917a<>(t);
    }
}
