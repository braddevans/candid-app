package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.GifSearchViewHolder;

public class GifSearchViewHolder$$ViewBinder<T extends GifSearchViewHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.GifSearchViewHolder$$ViewBinder$a */
    /* compiled from: GifSearchViewHolder$$ViewBinder */
    public static class C1899a<T extends GifSearchViewHolder> implements Unbinder {

        /* renamed from: a */
        private T f7507a;

        protected C1899a(T t) {
            this.f7507a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10945a(T t) {
            t.mGifImg = null;
            t.mGifLoadingImageProgressContainer = null;
            t.mGifLoadingImageProgress = null;
        }

        public final void unbind() {
            if (this.f7507a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10945a(this.f7507a);
            this.f7507a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1899a a = mo10944a(t);
        t.mGifImg = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.gif_image, "field 'mGifImg'"), R.id.gif_image, "field 'mGifImg'");
        t.mGifLoadingImageProgressContainer = (View) finder.findRequiredView(obj, R.id.loading_image_progress_container, "field 'mGifLoadingImageProgressContainer'");
        t.mGifLoadingImageProgress = (View) finder.findRequiredView(obj, R.id.loading_image_progress, "field 'mGifLoadingImageProgress'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1899a<T> mo10944a(T t) {
        return new C1899a<>(t);
    }
}
