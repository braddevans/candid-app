package com.becandid.candid.views;

import android.content.Context;
import android.graphics.Rect;
import android.support.p003v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;

public class AlwaysOnAutoCompleteView extends AppCompatAutoCompleteTextView {
    public AlwaysOnAutoCompleteView(Context context) {
        super(context);
    }

    public AlwaysOnAutoCompleteView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AlwaysOnAutoCompleteView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean enoughToFilter() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        if (z && getAdapter() != null) {
            performFiltering(getText(), 0);
        }
    }

    public void performCompletion() {
        if (isPopupShowing()) {
            ArrayAdapter arrayAdapter = (ArrayAdapter) getAdapter();
            getOnItemClickListener().onItemClick(null, null, 0, 0);
        }
    }
}
