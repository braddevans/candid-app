package com.becandid.candid.views.viewholders;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.viewholders.BasePostViewHolder;

public class BasePostViewHolder$$ViewBinder<T extends BasePostViewHolder> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.viewholders.BasePostViewHolder$$ViewBinder$a */
    /* compiled from: BasePostViewHolder$$ViewBinder */
    public static class C1898a<T extends BasePostViewHolder> implements Unbinder {

        /* renamed from: a */
        private T f7506a;

        protected C1898a(T t) {
            this.f7506a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10942a(T t) {
            t.mLikeRumorPostContainer = null;
            t.mLikeRumorPostIcon = null;
            t.mLikeRumorPostText = null;
            t.mLikeRumorPostBadge = null;
            t.mRumorTrueText = null;
            t.mUnlikeRumorPostContainer = null;
            t.mUnlikeRumorPostIcon = null;
            t.mUnlikeRumorPostText = null;
            t.mUnlikeRumorPostBadge = null;
            t.mRumorFalseText = null;
            t.mCommentPostContainer = null;
            t.mCommentPostBadge = null;
            t.mCommentPostText = null;
            t.mShareButton = null;
            t.mShareIcon = null;
            t.mMessagePostContainer = null;
            t.mPostAuthor = null;
            t.mPostIcon = null;
            t.mPostModIcon = null;
            t.mPostGroup = null;
            t.mPostGroupContainer = null;
            t.mPostGroupMenu = null;
            t.mPostGroupMenuContainer = null;
            t.mPostTimestamp = null;
            t.mFooter = null;
            t.mHeader = null;
            t.mPostPopupAction = null;
            t.mPostHeaderBadge = null;
            t.mPostHeaderBadgeText = null;
            t.mPostHeaderBadgeScore = null;
            t.mPostHeaderBadgeDot = null;
            t.reportFlag = null;
            t.mFooterCommentIcon = null;
            t.mFooterCommentModIcon = null;
            t.mFooterCommentImage = null;
            t.mFooterCommentTimestamp = null;
            t.mFooterCommentText = null;
            t.mFooterCommentSnippet = null;
            t.mFooterCommentValue = null;
            t.mPostContainer = null;
            t.mPostRelated = null;
            t.mPostRelatedBottom = null;
            t.mPostRelatedBar = null;
            t.mPostTrending = null;
            t.mPostRumorAlert = null;
            t.mPostTrendingDrawable = null;
            t.mPostTrendingText = null;
            t.mPostRumorAlertDrawable = null;
            t.mPostRumorAlertText = null;
            t.mPostCaption = null;
            t.mProgressBar = null;
            t.mPostShareOgImage = null;
            t.mPostGroupOgImage = null;
            t.mPostCaptionOgImage = null;
            t.mLikePostBtnOgImage = null;
            t.mUnlikePostBtnOgImage = null;
            t.mTrueRumorTextOgImage = null;
            t.mFalseRumorTextOgImage = null;
            t.mTrueRumorIconOgImage = null;
            t.mFalseRumorIconOgImage = null;
            t.mCommentPostBtnOgImage = null;
            t.mTrueRumorContainerOgImage = null;
            t.mFalseRumorContainerOgImage = null;
            t.mPostImageFooter = null;
        }

        public final void unbind() {
            if (this.f7506a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10942a(this.f7506a);
            this.f7506a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1898a a = mo10941a(t);
        t.mLikeRumorPostContainer = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.like_rumor_post_container, "field 'mLikeRumorPostContainer'"), R.id.like_rumor_post_container, "field 'mLikeRumorPostContainer'");
        t.mLikeRumorPostIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.like_rumor_post_icon, "field 'mLikeRumorPostIcon'"), R.id.like_rumor_post_icon, "field 'mLikeRumorPostIcon'");
        t.mLikeRumorPostText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.like_rumor_post_badge_text, "field 'mLikeRumorPostText'"), R.id.like_rumor_post_badge_text, "field 'mLikeRumorPostText'");
        t.mLikeRumorPostBadge = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.like_rumor_post_badge, "field 'mLikeRumorPostBadge'"), R.id.like_rumor_post_badge, "field 'mLikeRumorPostBadge'");
        t.mRumorTrueText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.rumor_true_text, "field 'mRumorTrueText'"), R.id.rumor_true_text, "field 'mRumorTrueText'");
        t.mUnlikeRumorPostContainer = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.unlike_rumor_post_container, "field 'mUnlikeRumorPostContainer'"), R.id.unlike_rumor_post_container, "field 'mUnlikeRumorPostContainer'");
        t.mUnlikeRumorPostIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.unlike_rumor_post_icon, "field 'mUnlikeRumorPostIcon'"), R.id.unlike_rumor_post_icon, "field 'mUnlikeRumorPostIcon'");
        t.mUnlikeRumorPostText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.unlike_rumor_post_badge_text, "field 'mUnlikeRumorPostText'"), R.id.unlike_rumor_post_badge_text, "field 'mUnlikeRumorPostText'");
        t.mUnlikeRumorPostBadge = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.unlike_rumor_post_badge, "field 'mUnlikeRumorPostBadge'"), R.id.unlike_rumor_post_badge, "field 'mUnlikeRumorPostBadge'");
        t.mRumorFalseText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.rumor_false_text, "field 'mRumorFalseText'"), R.id.rumor_false_text, "field 'mRumorFalseText'");
        t.mCommentPostContainer = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.comment_post_container, "field 'mCommentPostContainer'"), R.id.comment_post_container, "field 'mCommentPostContainer'");
        t.mCommentPostBadge = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.comment_post_badge, "field 'mCommentPostBadge'"), R.id.comment_post_badge, "field 'mCommentPostBadge'");
        t.mCommentPostText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_post_badge_text, "field 'mCommentPostText'"), R.id.comment_post_badge_text, "field 'mCommentPostText'");
        t.mShareButton = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.share_post_container, "field 'mShareButton'"), R.id.share_post_container, "field 'mShareButton'");
        t.mShareIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.share_post_icon, "field 'mShareIcon'"), R.id.share_post_icon, "field 'mShareIcon'");
        t.mMessagePostContainer = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_post_container, "field 'mMessagePostContainer'"), R.id.message_post_container, "field 'mMessagePostContainer'");
        t.mPostAuthor = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_author, "field 'mPostAuthor'"), R.id.post_author, "field 'mPostAuthor'");
        t.mPostIcon = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_icon, "field 'mPostIcon'"), R.id.post_icon, "field 'mPostIcon'");
        t.mPostModIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_mod_icon, "field 'mPostModIcon'"), R.id.post_mod_icon, "field 'mPostModIcon'");
        t.mPostGroup = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_group, "field 'mPostGroup'"), R.id.post_group, "field 'mPostGroup'");
        t.mPostGroupContainer = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_group_container, "field 'mPostGroupContainer'"), R.id.post_group_container, "field 'mPostGroupContainer'");
        t.mPostGroupMenu = (View) finder.findRequiredView(obj, R.id.post_group_menu, "field 'mPostGroupMenu'");
        t.mPostGroupMenuContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_group_menu_container, "field 'mPostGroupMenuContainer'"), R.id.post_group_menu_container, "field 'mPostGroupMenuContainer'");
        t.mPostTimestamp = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_timestamp, "field 'mPostTimestamp'"), R.id.post_timestamp, "field 'mPostTimestamp'");
        t.mFooter = (View) finder.findRequiredView(obj, R.id.post_footer, "field 'mFooter'");
        t.mHeader = (View) finder.findRequiredView(obj, R.id.post_header, "field 'mHeader'");
        t.mPostPopupAction = (View) finder.findRequiredView(obj, R.id.post_popup_action, "field 'mPostPopupAction'");
        t.mPostHeaderBadge = (View) finder.findRequiredView(obj, R.id.header_badge, "field 'mPostHeaderBadge'");
        t.mPostHeaderBadgeText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.header_badge_text, "field 'mPostHeaderBadgeText'"), R.id.header_badge_text, "field 'mPostHeaderBadgeText'");
        t.mPostHeaderBadgeScore = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.header_badge_score, "field 'mPostHeaderBadgeScore'"), R.id.header_badge_score, "field 'mPostHeaderBadgeScore'");
        t.mPostHeaderBadgeDot = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.header_badge_dot, "field 'mPostHeaderBadgeDot'"), R.id.header_badge_dot, "field 'mPostHeaderBadgeDot'");
        t.reportFlag = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.report_flag, "field 'reportFlag'"), R.id.report_flag, "field 'reportFlag'");
        t.mFooterCommentIcon = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.footer_comment_icon, "field 'mFooterCommentIcon'"), R.id.footer_comment_icon, "field 'mFooterCommentIcon'");
        t.mFooterCommentModIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.footer_comment_mod_icon, "field 'mFooterCommentModIcon'"), R.id.footer_comment_mod_icon, "field 'mFooterCommentModIcon'");
        t.mFooterCommentImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.footer_comment_image, "field 'mFooterCommentImage'"), R.id.footer_comment_image, "field 'mFooterCommentImage'");
        t.mFooterCommentTimestamp = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.footer_comment_timestamp, "field 'mFooterCommentTimestamp'"), R.id.footer_comment_timestamp, "field 'mFooterCommentTimestamp'");
        t.mFooterCommentText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.footer_comment_text, "field 'mFooterCommentText'"), R.id.footer_comment_text, "field 'mFooterCommentText'");
        t.mFooterCommentSnippet = (View) finder.findRequiredView(obj, R.id.footer_comment_snippet, "field 'mFooterCommentSnippet'");
        t.mFooterCommentValue = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.footer_comment_amount, "field 'mFooterCommentValue'"), R.id.footer_comment_amount, "field 'mFooterCommentValue'");
        t.mPostContainer = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_container, "field 'mPostContainer'"), R.id.post_container, "field 'mPostContainer'");
        t.mPostRelated = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_related, "field 'mPostRelated'"), R.id.post_related, "field 'mPostRelated'");
        t.mPostRelatedBottom = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_related_bottom, "field 'mPostRelatedBottom'"), R.id.post_related_bottom, "field 'mPostRelatedBottom'");
        t.mPostRelatedBar = (View) finder.findRequiredView(obj, R.id.related_posts_bar, "field 'mPostRelatedBar'");
        t.mPostTrending = (View) finder.findRequiredView(obj, R.id.post_trending, "field 'mPostTrending'");
        t.mPostRumorAlert = (View) finder.findRequiredView(obj, R.id.post_rumor_alert, "field 'mPostRumorAlert'");
        t.mPostTrendingDrawable = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_trending_drawable, "field 'mPostTrendingDrawable'"), R.id.post_trending_drawable, "field 'mPostTrendingDrawable'");
        t.mPostTrendingText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_trending_text, "field 'mPostTrendingText'"), R.id.post_trending_text, "field 'mPostTrendingText'");
        t.mPostRumorAlertDrawable = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_rumor_alert_drawable, "field 'mPostRumorAlertDrawable'"), R.id.post_rumor_alert_drawable, "field 'mPostRumorAlertDrawable'");
        t.mPostRumorAlertText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_rumor_alert_text, "field 'mPostRumorAlertText'"), R.id.post_rumor_alert_text, "field 'mPostRumorAlertText'");
        t.mPostCaption = (TextView) finder.castView((View) finder.findOptionalView(obj, R.id.post_caption, null), R.id.post_caption, "field 'mPostCaption'");
        t.mProgressBar = (ProgressBar) finder.castView((View) finder.findRequiredView(obj, R.id.progress_bar, "field 'mProgressBar'"), R.id.progress_bar, "field 'mProgressBar'");
        t.mPostShareOgImage = (View) finder.findRequiredView(obj, R.id.post_share_image, "field 'mPostShareOgImage'");
        t.mPostGroupOgImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_image_group, "field 'mPostGroupOgImage'"), R.id.post_image_group, "field 'mPostGroupOgImage'");
        t.mPostCaptionOgImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_caption_image, "field 'mPostCaptionOgImage'"), R.id.post_caption_image, "field 'mPostCaptionOgImage'");
        t.mLikePostBtnOgImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.like_post_image, "field 'mLikePostBtnOgImage'"), R.id.like_post_image, "field 'mLikePostBtnOgImage'");
        t.mUnlikePostBtnOgImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.unlike_post_image, "field 'mUnlikePostBtnOgImage'"), R.id.unlike_post_image, "field 'mUnlikePostBtnOgImage'");
        t.mTrueRumorTextOgImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.true_rumor_text_image, "field 'mTrueRumorTextOgImage'"), R.id.true_rumor_text_image, "field 'mTrueRumorTextOgImage'");
        t.mFalseRumorTextOgImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.false_rumor_text_image, "field 'mFalseRumorTextOgImage'"), R.id.false_rumor_text_image, "field 'mFalseRumorTextOgImage'");
        t.mTrueRumorIconOgImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.true_rumor_icon_image, "field 'mTrueRumorIconOgImage'"), R.id.true_rumor_icon_image, "field 'mTrueRumorIconOgImage'");
        t.mFalseRumorIconOgImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.false_rumor_icon_image, "field 'mFalseRumorIconOgImage'"), R.id.false_rumor_icon_image, "field 'mFalseRumorIconOgImage'");
        t.mCommentPostBtnOgImage = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_post_image, "field 'mCommentPostBtnOgImage'"), R.id.comment_post_image, "field 'mCommentPostBtnOgImage'");
        t.mTrueRumorContainerOgImage = (View) finder.findRequiredView(obj, R.id.true_rumor_container_image, "field 'mTrueRumorContainerOgImage'");
        t.mFalseRumorContainerOgImage = (View) finder.findRequiredView(obj, R.id.false_rumor_container_image, "field 'mFalseRumorContainerOgImage'");
        t.mPostImageFooter = (View) finder.findRequiredView(obj, R.id.post_image_footer, "field 'mPostImageFooter'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1898a<T> mo10941a(T t) {
        return new C1898a<>(t);
    }
}
