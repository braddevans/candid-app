package com.becandid.candid.views;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.views.CommunityOnboardingSimpleView;

public class CommunityOnboardingSimpleView$$ViewBinder<T extends CommunityOnboardingSimpleView> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.CommunityOnboardingSimpleView$$ViewBinder$a */
    /* compiled from: CommunityOnboardingSimpleView$$ViewBinder */
    public static class C1806a<T extends CommunityOnboardingSimpleView> implements Unbinder {

        /* renamed from: a */
        View f7321a;

        /* renamed from: b */
        View f7322b;

        /* renamed from: c */
        View f7323c;

        /* renamed from: d */
        View f7324d;

        /* renamed from: e */
        private T f7325e;

        protected C1806a(T t) {
            this.f7325e = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10795a(T t) {
            this.f7321a.setOnClickListener(null);
            this.f7322b.setOnClickListener(null);
            this.f7323c.setOnClickListener(null);
            this.f7324d.setOnClickListener(null);
        }

        public final void unbind() {
            if (this.f7325e == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10795a(this.f7325e);
            this.f7325e = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1806a a = mo10794a(t);
        View view = (View) finder.findRequiredView(obj, R.id.community_skip, "method 'onCommunitySkipClick'");
        a.f7321a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCommunitySkipClick(view);
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.onboarding_community_school, "method 'onCommunitySchoolClick'");
        a.f7322b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCommunitySchoolClick(view);
            }
        });
        View view3 = (View) finder.findRequiredView(obj, R.id.onboarding_community_college, "method 'onCommunityCollegeClick'");
        a.f7323c = view3;
        view3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCommunityCollegeClick(view);
            }
        });
        View view4 = (View) finder.findRequiredView(obj, R.id.onboarding_community_company, "method 'onCommunityCompanyClick'");
        a.f7324d = view4;
        view4.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCommunityCompanyClick(view);
            }
        });
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1806a<T> mo10794a(T t) {
        return new C1806a<>(t);
    }
}
