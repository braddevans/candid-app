package com.becandid.candid.views;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.views.BlurLayout$$ViewBinder.C1791a;
import com.becandid.candid.views.NicknamePopupView;

public class NicknamePopupView$$ViewBinder<T extends NicknamePopupView> extends BlurLayout$$ViewBinder<T> {

    /* renamed from: com.becandid.candid.views.NicknamePopupView$$ViewBinder$a */
    /* compiled from: NicknamePopupView$$ViewBinder */
    public static class C1840a<T extends NicknamePopupView> extends C1791a<T> {
        protected C1840a(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10764a(T t) {
            super.mo10764a(t);
            t.mMessageContainerAddNickname = null;
            t.mMessageContainerSecondNickname = null;
            t.mBadgeIcon = null;
            t.mFinishBtn = null;
            t.mNicknameCloseButton = null;
            t.secondNicknameDesc = null;
            t.addNicknameDesc = null;
            t.mNoButton = null;
            t.mYesButton = null;
            t.mOkButton = null;
            t.mCancelButton = null;
            t.editNickname = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1840a aVar = (C1840a) super.bind(finder, t, obj);
        t.mMessageContainerAddNickname = (View) finder.findRequiredView(obj, R.id.message_container_add_nickname, "field 'mMessageContainerAddNickname'");
        t.mMessageContainerSecondNickname = (View) finder.findRequiredView(obj, R.id.message_container_second_nickname, "field 'mMessageContainerSecondNickname'");
        t.mBadgeIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_icon, "field 'mBadgeIcon'"), R.id.badge_icon, "field 'mBadgeIcon'");
        t.mFinishBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.finish, "field 'mFinishBtn'"), R.id.finish, "field 'mFinishBtn'");
        t.mNicknameCloseButton = (View) finder.findRequiredView(obj, R.id.nickname_close, "field 'mNicknameCloseButton'");
        t.secondNicknameDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_desc_second_nickname, "field 'secondNicknameDesc'"), R.id.badge_desc_second_nickname, "field 'secondNicknameDesc'");
        t.addNicknameDesc = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_desc_add_nickname, "field 'addNicknameDesc'"), R.id.badge_desc_add_nickname, "field 'addNicknameDesc'");
        t.mNoButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.no, "field 'mNoButton'"), R.id.no, "field 'mNoButton'");
        t.mYesButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.yes, "field 'mYesButton'"), R.id.yes, "field 'mYesButton'");
        t.mOkButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.ok, "field 'mOkButton'"), R.id.ok, "field 'mOkButton'");
        t.mCancelButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.cancel, "field 'mCancelButton'"), R.id.cancel, "field 'mCancelButton'");
        t.editNickname = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.edit_nickname, "field 'editNickname'"), R.id.edit_nickname, "field 'editNickname'");
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1840a<T> mo10762a(T t) {
        return new C1840a<>(t);
    }
}
