package com.becandid.candid.fragments;

import android.support.p003v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.InviteContactNameFragment;

public class InviteContactNameFragment$$ViewBinder<T extends InviteContactNameFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.InviteContactNameFragment$$ViewBinder$a */
    /* compiled from: InviteContactNameFragment$$ViewBinder */
    public static class C1611a<T extends InviteContactNameFragment> implements Unbinder {

        /* renamed from: a */
        private T f6828a;

        protected C1611a(T t) {
            this.f6828a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10429a(T t) {
            t.mBackNav = null;
            t.mInviteName = null;
            t.mSendText = null;
        }

        public final void unbind() {
            if (this.f6828a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10429a(this.f6828a);
            this.f6828a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1611a a = mo10428a(t);
        t.mBackNav = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.backNav, "field 'mBackNav'"), R.id.backNav, "field 'mBackNav'");
        t.mInviteName = (AppCompatEditText) finder.castView((View) finder.findRequiredView(obj, R.id.invite_name, "field 'mInviteName'"), R.id.invite_name, "field 'mInviteName'");
        t.mSendText = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.send_text, "field 'mSendText'"), R.id.send_text, "field 'mSendText'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1611a<T> mo10428a(T t) {
        return new C1611a<>(t);
    }
}
