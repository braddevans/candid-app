package com.becandid.candid.fragments;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.NearbyFeedFragment;

public class NearbyFeedFragment$$ViewBinder<T extends NearbyFeedFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.NearbyFeedFragment$$ViewBinder$a */
    /* compiled from: NearbyFeedFragment$$ViewBinder */
    public static class C1635a<T extends NearbyFeedFragment> implements Unbinder {

        /* renamed from: a */
        private T f6877a;

        protected C1635a(T t) {
            this.f6877a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10462a(T t) {
            t.mSliderFirstPos = null;
            t.mSliderSecondPos = null;
            t.mSliderThirdPos = null;
            t.mSliderLastPos = null;
            t.mSliderFirstView = null;
            t.mSliderSecondView = null;
            t.mSliderThirdView = null;
            t.mSliderLastView = null;
            t.mNearbyFeedContent = null;
            t.mNearbyFeedPermission = null;
            t.mCloserBtn = null;
            t.mFurtherBtn = null;
            t.mNoNearbyContentText = null;
        }

        public final void unbind() {
            if (this.f6877a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10462a(this.f6877a);
            this.f6877a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1635a a = mo10461a(t);
        t.mSliderFirstPos = (View) finder.findRequiredView(obj, R.id.slider_first_pos, "field 'mSliderFirstPos'");
        t.mSliderSecondPos = (View) finder.findRequiredView(obj, R.id.slider_second_pos, "field 'mSliderSecondPos'");
        t.mSliderThirdPos = (View) finder.findRequiredView(obj, R.id.slider_third_pos, "field 'mSliderThirdPos'");
        t.mSliderLastPos = (View) finder.findRequiredView(obj, R.id.slider_last_pos, "field 'mSliderLastPos'");
        t.mSliderFirstView = (View) finder.findRequiredView(obj, R.id.slider_main_view, "field 'mSliderFirstView'");
        t.mSliderSecondView = (View) finder.findRequiredView(obj, R.id.slider_second_pos_view, "field 'mSliderSecondView'");
        t.mSliderThirdView = (View) finder.findRequiredView(obj, R.id.slider_third_pos_view, "field 'mSliderThirdView'");
        t.mSliderLastView = (View) finder.findRequiredView(obj, R.id.slider_last_pos_view, "field 'mSliderLastView'");
        t.mNearbyFeedContent = (View) finder.findRequiredView(obj, R.id.nearby_feed_content, "field 'mNearbyFeedContent'");
        t.mNearbyFeedPermission = (View) finder.findRequiredView(obj, R.id.nearby_feed_permission, "field 'mNearbyFeedPermission'");
        t.mCloserBtn = (View) finder.findRequiredView(obj, R.id.closer_distance, "field 'mCloserBtn'");
        t.mFurtherBtn = (View) finder.findRequiredView(obj, R.id.further_distance, "field 'mFurtherBtn'");
        t.mNoNearbyContentText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.no_nearby_content, "field 'mNoNearbyContentText'"), R.id.no_nearby_content, "field 'mNoNearbyContentText'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1635a<T> mo10461a(T t) {
        return new C1635a<>(t);
    }
}
