package com.becandid.candid.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.p003v7.widget.GridLayoutManager;
import android.support.p003v7.widget.GridLayoutManager.SpanSizeLookup;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.RecyclerView.ItemDecoration;
import android.support.p003v7.widget.RecyclerView.State;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.becandid.candid.R;
import com.becandid.candid.fragments.VideoFragment.C1642a;
import com.becandid.candid.util.video.FLSprite.SpecialEffect;
import java.util.LinkedHashMap;

public class VideoSpritesSelectFragment extends Fragment implements C2468a {

    /* renamed from: d */
    private static LinkedHashMap<Integer, C1644a> f6905d;

    /* renamed from: a */
    private RecyclerView f6906a;

    /* renamed from: b */
    private GridLayoutManager f6907b;

    /* renamed from: c */
    private C1642a f6908c;

    /* renamed from: com.becandid.candid.fragments.VideoSpritesSelectFragment$a */
    public static class C1644a {

        /* renamed from: a */
        public SpecialEffect f6910a;

        /* renamed from: b */
        public Drawable f6911b;
    }

    /* renamed from: com.becandid.candid.fragments.VideoSpritesSelectFragment$b */
    public static class C1645b extends ItemDecoration {

        /* renamed from: a */
        private int f6912a = 10;

        public C1645b(Context context) {
        }

        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, State state) {
            rect.set(this.f6912a, this.f6912a, this.f6912a, this.f6912a);
        }
    }

    /* renamed from: a */
    private LinkedHashMap<Integer, C1644a> m9472a() {
        LinkedHashMap<Integer, C1644a> linkedHashMap = new LinkedHashMap<>();
        Resources resources = getResources();
        return linkedHashMap;
    }

    /* renamed from: a */
    public void mo10476a(int i) {
        if (this.f6908c != null) {
            C1644a aVar = (C1644a) f6905d.get(Integer.valueOf(i));
            if (aVar != null) {
                this.f6908c.onSpriteSelected(i, aVar.f6910a);
            }
        }
        getFragmentManager().popBackStackImmediate();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_video_sprite_add, viewGroup, false);
        this.f6906a = (RecyclerView) inflate.findViewById(R.id.video_sprite_select_rv);
        this.f6906a.addItemDecoration(new C1645b(getActivity()));
        this.f6906a.setHasFixedSize(true);
        this.f6907b = new GridLayoutManager(getActivity(), 3);
        this.f6907b.setSpanSizeLookup(new SpanSizeLookup() {
            public int getSpanSize(int i) {
                return 1;
            }
        });
        this.f6906a.setLayoutManager(this.f6907b);
        if (f6905d == null) {
            f6905d = m9472a();
        }
        this.f6906a.setAdapter(new FLSpritesAdapter(f6905d, this));
        this.f6908c = (C1642a) getActivity();
        return inflate;
    }
}
