package com.becandid.candid.fragments;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.InviteContactPeopleFragment;

public class InviteContactPeopleFragment$$ViewBinder<T extends InviteContactPeopleFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.InviteContactPeopleFragment$$ViewBinder$a */
    /* compiled from: InviteContactPeopleFragment$$ViewBinder */
    public static class C1620a<T extends InviteContactPeopleFragment> implements Unbinder {

        /* renamed from: a */
        View f6848a;

        /* renamed from: b */
        View f6849b;

        /* renamed from: c */
        View f6850c;

        /* renamed from: d */
        private T f6851d;

        protected C1620a(T t) {
            this.f6851d = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10440a(T t) {
            t.mPostSnippet = null;
            t.mEntirePostSnippet = null;
            t.mPostPlaceholder = null;
            t.mPostPreview = null;
            this.f6848a.setOnClickListener(null);
            this.f6849b.setOnClickListener(null);
            this.f6850c.setOnClickListener(null);
        }

        public final void unbind() {
            if (this.f6851d == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10440a(this.f6851d);
            this.f6851d = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1620a a = mo10439a(t);
        t.mPostSnippet = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_snippet, "field 'mPostSnippet'"), R.id.post_snippet, "field 'mPostSnippet'");
        t.mEntirePostSnippet = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.entire_post_snippet, "field 'mEntirePostSnippet'"), R.id.entire_post_snippet, "field 'mEntirePostSnippet'");
        t.mPostPlaceholder = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_placeholder, "field 'mPostPlaceholder'"), R.id.post_placeholder, "field 'mPostPlaceholder'");
        t.mPostPreview = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_preview, "field 'mPostPreview'"), R.id.post_preview, "field 'mPostPreview'");
        View view = (View) finder.findRequiredView(obj, R.id.contacts_invite_all, "method 'inviteAll'");
        a.f6848a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.inviteAll(view);
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.contacts_choose, "method 'chooseContacts'");
        a.f6849b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.chooseContacts(view);
            }
        });
        View view3 = (View) finder.findRequiredView(obj, R.id.contacts_close, "method 'onContactClose'");
        a.f6850c = view3;
        view3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onContactClose();
            }
        });
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1620a<T> mo10439a(T t) {
        return new C1620a<>(t);
    }
}
