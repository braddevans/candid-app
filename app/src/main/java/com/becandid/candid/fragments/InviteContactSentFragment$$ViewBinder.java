package com.becandid.candid.fragments;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.InviteContactSentFragment;

public class InviteContactSentFragment$$ViewBinder<T extends InviteContactSentFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.InviteContactSentFragment$$ViewBinder$a */
    /* compiled from: InviteContactSentFragment$$ViewBinder */
    public static class C1623a<T extends InviteContactSentFragment> implements Unbinder {

        /* renamed from: a */
        View f6858a;

        /* renamed from: b */
        View f6859b;

        /* renamed from: c */
        private T f6860c;

        protected C1623a(T t) {
            this.f6860c = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10445a(T t) {
            this.f6858a.setOnClickListener(null);
            this.f6859b.setOnClickListener(null);
        }

        public final void unbind() {
            if (this.f6860c == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10445a(this.f6860c);
            this.f6860c = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1623a a = mo10444a(t);
        View view = (View) finder.findRequiredView(obj, R.id.contacts_close, "method 'onContactClose'");
        a.f6858a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onContactClose();
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.got_it, "method 'onClose'");
        a.f6859b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onClose();
            }
        });
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1623a<T> mo10444a(T t) {
        return new C1623a<>(t);
    }
}
