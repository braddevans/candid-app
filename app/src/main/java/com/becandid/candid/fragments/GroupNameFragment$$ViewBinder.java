package com.becandid.candid.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.GroupNameFragment;

public class GroupNameFragment$$ViewBinder<T extends GroupNameFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.GroupNameFragment$$ViewBinder$a */
    /* compiled from: GroupNameFragment$$ViewBinder */
    public static class C1599a<T extends GroupNameFragment> implements Unbinder {

        /* renamed from: a */
        private T f6802a;

        protected C1599a(T t) {
            this.f6802a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10403a(T t) {
            t.groupNameHeader = null;
            t.groupNameInfo = null;
            t.nextBtn = null;
            t.mNamePlaceholder = null;
            t.mProgressBar = null;
        }

        public final void unbind() {
            if (this.f6802a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10403a(this.f6802a);
            this.f6802a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1599a a = mo10402a(t);
        t.groupNameHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_name_header, "field 'groupNameHeader'"), R.id.group_name_header, "field 'groupNameHeader'");
        t.groupNameInfo = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.group_name, "field 'groupNameInfo'"), R.id.group_name, "field 'groupNameInfo'");
        t.nextBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.next, "field 'nextBtn'"), R.id.next, "field 'nextBtn'");
        t.mNamePlaceholder = (View) finder.findRequiredView(obj, R.id.group_name_placeholder, "field 'mNamePlaceholder'");
        t.mProgressBar = (View) finder.findRequiredView(obj, R.id.progress_bar, "field 'mProgressBar'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1599a<T> mo10402a(T t) {
        return new C1599a<>(t);
    }
}
