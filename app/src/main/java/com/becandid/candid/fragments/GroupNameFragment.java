package com.becandid.candid.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.support.p001v4.app.FragmentTransaction;
import android.support.p003v7.app.AlertDialog;
import android.support.p003v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.CreateGroupActivity;
import com.becandid.candid.activities.GroupDetailsActivity;
import com.becandid.candid.data.Group;
import com.becandid.candid.models.GroupNameCheck;
import p012rx.schedulers.Schedulers;

public class GroupNameFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public Bundle f6791a;

    /* renamed from: b */
    private Unbinder f6792b;
    @BindView(2131624624)
    TextView groupNameHeader;
    @BindView(2131624151)
    EditText groupNameInfo;
    @BindView(2131624623)
    View mNamePlaceholder;
    @BindView(2131624101)
    View mProgressBar;
    @BindView(2131624625)
    Button nextBtn;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9391a() {
        FragmentActivity activity = getActivity();
        C2699jj.m14610a((Context) activity);
        if (activity instanceof CreateGroupActivity) {
            ((CreateGroupActivity) activity).setGroupName(this.groupNameInfo.getText().toString());
        }
        GroupInfoFragment groupInfoFragment = new GroupInfoFragment();
        groupInfoFragment.setArguments(this.f6791a);
        FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        beginTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
        beginTransaction.replace(R.id.placeholder, groupInfoFragment);
        beginTransaction.addToBackStack(null);
        beginTransaction.commit();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_group_name, viewGroup, false);
        this.f6792b = ButterKnife.bind((Object) this, inflate);
        this.f6791a = getArguments();
        if (this.f6791a.containsKey("group_name")) {
            this.groupNameHeader.setText("Group Name");
            this.groupNameInfo.setText(this.f6791a.getString("group_name"));
            this.groupNameInfo.setSelection(this.f6791a.getString("group_name").length());
            this.nextBtn.setEnabled(true);
        }
        this.groupNameInfo.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (editable.toString() == null || editable.toString().isEmpty()) {
                    GroupNameFragment.this.nextBtn.setEnabled(false);
                } else {
                    GroupNameFragment.this.nextBtn.setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        this.nextBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                GroupNameFragment.this.mProgressBar.setVisibility(0);
                GroupNameFragment.this.mNamePlaceholder.setAlpha(0.3f);
                String obj = GroupNameFragment.this.groupNameInfo.getText().toString();
                final boolean booleanExtra = GroupNameFragment.this.getActivity().getIntent().getBooleanExtra("for_post", false);
                if (!GroupNameFragment.this.f6791a.containsKey("group_name") || !obj.equals(GroupNameFragment.this.f6791a.get("group_name"))) {
                    ApiService.m14297a().mo14109a(obj, booleanExtra).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<GroupNameCheck>() {
                        /* renamed from: a */
                        public void onNext(final GroupNameCheck groupNameCheck) {
                            if (groupNameCheck.isSuccess()) {
                                GroupNameFragment.this.m9391a();
                                return;
                            }
                            String error = groupNameCheck.getError();
                            final int groupId = groupNameCheck.getGroupId();
                            boolean z = groupNameCheck.getGroupName() != null;
                            Builder builder = new Builder(GroupNameFragment.this.getActivity());
                            builder.setMessage((CharSequence) error);
                            if (z) {
                                builder.setPositiveButton((int) R.string.yes, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        if (booleanExtra) {
                                            Intent intent = new Intent();
                                            Group group = new Group();
                                            group.group_id = groupNameCheck.getGroupId();
                                            group.group_name = groupNameCheck.getGroupName();
                                            intent.putExtra("group", group);
                                            GroupNameFragment.this.getActivity().setResult(-1, intent);
                                        } else {
                                            GroupNameFragment.this.getActivity().startActivity(GroupDetailsActivity.startGroupDetailsActivity(GroupNameFragment.this.getContext(), groupId));
                                        }
                                        GroupNameFragment.this.getActivity().finish();
                                    }
                                }).setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        GroupNameFragment.this.mProgressBar.setVisibility(8);
                                        GroupNameFragment.this.mNamePlaceholder.setAlpha(1.0f);
                                        dialogInterface.dismiss();
                                    }
                                });
                            } else {
                                builder.setNeutralButton((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        GroupNameFragment.this.mProgressBar.setVisibility(8);
                                        GroupNameFragment.this.mNamePlaceholder.setAlpha(1.0f);
                                        dialogInterface.dismiss();
                                    }
                                });
                            }
                            AlertDialog create = builder.create();
                            create.setCanceledOnTouchOutside(false);
                            create.setCancelable(false);
                            create.show();
                        }

                        public void onCompleted() {
                        }

                        public void onError(Throwable th) {
                        }
                    });
                } else {
                    GroupNameFragment.this.m9391a();
                }
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f6792b.unbind();
    }
}
