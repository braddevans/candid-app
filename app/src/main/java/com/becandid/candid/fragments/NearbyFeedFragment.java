package com.becandid.candid.fragments;

import android.app.Activity;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.p003v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Post;
import com.becandid.candid.models.NetworkData;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class NearbyFeedFragment extends FeedFragment {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public int f6861a = (AppState.config.getInt("default_nearby_index", 2) + 1);

    /* renamed from: b */
    private final int f6862b = 2;

    /* renamed from: c */
    private View f6863c;

    /* renamed from: d */
    private Unbinder f6864d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public List<Integer> f6865e;

    /* renamed from: f */
    private C1634a f6866f;
    @BindView(2131624796)
    View mCloserBtn;
    @BindView(2131624798)
    View mFurtherBtn;
    @BindView(2131624794)
    View mNearbyFeedContent;
    @BindView(2131624800)
    View mNearbyFeedPermission;
    @BindView(2131624799)
    TextView mNoNearbyContentText;
    @BindView(2131624501)
    View mSliderFirstPos;
    @BindView(2131624500)
    View mSliderFirstView;
    @BindView(2131624504)
    View mSliderLastPos;
    @BindView(2131624508)
    View mSliderLastView;
    @BindView(2131624502)
    View mSliderSecondPos;
    @BindView(2131624505)
    View mSliderSecondView;
    @BindView(2131624503)
    View mSliderThirdPos;
    @BindView(2131624507)
    View mSliderThirdView;

    /* renamed from: com.becandid.candid.fragments.NearbyFeedFragment$a */
    public interface C1634a {
        /* renamed from: a */
        void mo10459a(int i, int i2, int i3);
    }

    /* renamed from: a */
    private void m9437a() {
        ApiService.m14297a().mo14130c(LocationUtil.m14546b()).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                if (networkData != null) {
                    NearbyFeedFragment.this.f6865e = networkData.nearby_levels;
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m9439c(int i) {
        int i2 = 0;
        this.f6861a = i;
        mo10446a(this.f10256m, 0);
        if (this.f6863c != this.mSliderFirstView) {
            this.f6863c.setVisibility(8);
        }
        int[] iArr = {0, 0};
        switch (i) {
            case 1:
                this.f6863c = this.mSliderFirstView;
                this.mSliderFirstPos.getLocationOnScreen(iArr);
                i2 = iArr[0];
                break;
            case 2:
                this.f6863c = this.mSliderSecondView;
                this.mSliderSecondPos.getLocationOnScreen(iArr);
                i2 = iArr[0];
                break;
            case 3:
                this.f6863c = this.mSliderThirdView;
                this.mSliderThirdPos.getLocationOnScreen(iArr);
                i2 = iArr[0];
                break;
            case 4:
                this.f6863c = this.mSliderLastView;
                this.mSliderLastPos.getLocationOnScreen(iArr);
                i2 = iArr[0];
                break;
        }
        this.f6863c.setVisibility(0);
        if (this.f6865e != null && this.f6866f != null && i >= 1) {
            this.f6866f.mo10459a(i, ((Integer) this.f6865e.get(i - 1)).intValue(), i2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public bjz mo10446a(String str, int i) {
        if (this.f10259p != null) {
            this.f10259p.setRefreshing(true);
        }
        if (!(this.f10259p == null || this.mNoNearbyContentText == null)) {
            this.f10259p.setVisibility(0);
            this.mNoNearbyContentText.setVisibility(8);
        }
        if (this.f10255l != null) {
            return ApiService.m14297a().mo14122b(str, null, this.f10255l.mo13766b(str), this.f6861a).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(mo13893h());
        }
        Crashlytics.m16437a((Throwable) new Exception("Feed Adapter is NULL"));
        return null;
    }

    /* renamed from: a */
    public void mo10447a(C1634a aVar) {
        this.f6866f = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo10448i() {
        if (this.f10255l != null) {
            this.f10255l.mo13769c();
        }
        if (!(this.f10259p == null || this.mNoNearbyContentText == null)) {
            this.f10259p.setVisibility(8);
            this.mNoNearbyContentText.setVisibility(0);
        }
        if (this.f6861a != 4) {
            this.mNoNearbyContentText.setText(AppState.config.getString("nearby_nodata_message", getString(R.string.nearby_nodata_message)));
        } else {
            this.mNoNearbyContentText.setText(AppState.config.getString("nearby_nodata_limit_message", getString(R.string.nearby_nodata_limit_message)));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f6864d = ButterKnife.bind((Object) this, onCreateView);
        LocationManager locationManager = (LocationManager) getActivity().getSystemService("location");
        if (ContextCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_COARSE_LOCATION") != 0 || !locationManager.isProviderEnabled("network")) {
            this.mNearbyFeedContent.setVisibility(8);
            this.mNearbyFeedPermission.setVisibility(0);
            onCreateView.findViewById(R.id.enable_location_btn).setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    NearbyFeedFragment.this.requestPermissions(new String[]{"android.permission.ACCESS_COARSE_LOCATION"}, 2);
                }
            });
        } else {
            this.mNearbyFeedContent.setVisibility(0);
            this.mNearbyFeedPermission.setVisibility(8);
        }
        this.f6863c = this.mSliderFirstView;
        this.mSliderFirstPos.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                NearbyFeedFragment.this.m9439c(1);
            }
        });
        this.mSliderSecondPos.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                NearbyFeedFragment.this.m9439c(2);
            }
        });
        this.mSliderThirdPos.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                NearbyFeedFragment.this.m9439c(3);
            }
        });
        this.mSliderLastPos.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                NearbyFeedFragment.this.m9439c(4);
            }
        });
        bjz a = RxBus.m14573a().mo14348a(C2669z.class, (bjy<T>) new bjy<C2669z>() {
            /* renamed from: a */
            public void onNext(C2669z zVar) {
                if (!zVar.f10671a) {
                    NearbyFeedFragment.this.mNearbyFeedContent.setVisibility(0);
                    NearbyFeedFragment.this.mNearbyFeedPermission.setVisibility(8);
                    NearbyFeedFragment.this.mo10446a(NearbyFeedFragment.this.f10256m, 0);
                    return;
                }
                LocationUtil.m14545a((Activity) NearbyFeedFragment.this.getActivity());
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        });
        this.mCloserBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (NearbyFeedFragment.this.f6861a != 1) {
                    NearbyFeedFragment.this.m9439c(NearbyFeedFragment.this.f6861a - 1);
                }
            }
        });
        this.mFurtherBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (NearbyFeedFragment.this.f6861a != 4) {
                    NearbyFeedFragment.this.m9439c(NearbyFeedFragment.this.f6861a + 1);
                }
            }
        });
        ((BaseActivity) getActivity()).addToSubscriptionList(a);
        m9439c(this.f6861a);
        m9437a();
        return onCreateView;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f6864d.unbind();
    }

    public void onLoadMore(String str) {
        if (str != null) {
            ApiService.m14297a().mo14122b(this.f10256m, str, this.f10255l.mo13766b(this.f10256m), this.f6861a).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<List<Post>>() {
                /* renamed from: a */
                public void onNext(List<Post> list) {
                    if (list == null || list.isEmpty()) {
                        NearbyFeedFragment.this.f10255l.mo13763a((String) null);
                    } else if (list.size() != 1 || (list.get(0) instanceof Post)) {
                        NearbyFeedFragment.this.f10255l.mo13768b(list);
                        NearbyFeedFragment.this.f10255l.mo13763a(Integer.toString(((Post) list.get(list.size() - 1)).post_id));
                    }
                }

                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    C2699jj.m14614a((RecyclerView) NearbyFeedFragment.this.f10258o, NearbyFeedFragment.this.f10255l);
                }
            });
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        switch (i) {
            case 2:
                if (iArr.length > 0 && iArr[0] == 0) {
                    LocationUtil.m14545a((Activity) getActivity());
                    m9437a();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
