package com.becandid.candid.fragments.messages;

import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.VideoView;
import butterknife.BindView;

public class FullScreenVideoFragment extends Fragment {
    @BindView(2131624187)
    View mFullScreenExit;
    @BindView(2131624184)
    FrameLayout mVideoPlaceholder;
    @BindView(2131624188)
    LinearLayout mVideoProgressBar;
    @BindView(2131624185)
    VideoView mVideoView;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return null;
    }

    public void onDestroyView() {
        super.onDestroyView();
    }
}
