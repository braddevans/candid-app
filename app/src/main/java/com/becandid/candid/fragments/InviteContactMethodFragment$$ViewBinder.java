package com.becandid.candid.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.InviteContactMethodFragment;

public class InviteContactMethodFragment$$ViewBinder<T extends InviteContactMethodFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.InviteContactMethodFragment$$ViewBinder$a */
    /* compiled from: InviteContactMethodFragment$$ViewBinder */
    public static class C1608a<T extends InviteContactMethodFragment> implements Unbinder {

        /* renamed from: a */
        View f6823a;

        /* renamed from: b */
        private T f6824b;

        protected C1608a(T t) {
            this.f6824b = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10421a(T t) {
            t.mAnonPlaceHolder = null;
            t.mKnownPlaceHolder = null;
            t.mContinueBtn = null;
            t.mAnonCheckmark = null;
            t.mKnownCheckmark = null;
            this.f6823a.setOnClickListener(null);
        }

        public final void unbind() {
            if (this.f6824b == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10421a(this.f6824b);
            this.f6824b = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1608a a = mo10420a(t);
        t.mAnonPlaceHolder = (View) finder.findRequiredView(obj, R.id.anon_placeholder, "field 'mAnonPlaceHolder'");
        t.mKnownPlaceHolder = (View) finder.findRequiredView(obj, R.id.known_placeholder, "field 'mKnownPlaceHolder'");
        t.mContinueBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.method_continue, "field 'mContinueBtn'"), R.id.method_continue, "field 'mContinueBtn'");
        t.mAnonCheckmark = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.anon_checkmark, "field 'mAnonCheckmark'"), R.id.anon_checkmark, "field 'mAnonCheckmark'");
        t.mKnownCheckmark = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.known_checkmark, "field 'mKnownCheckmark'"), R.id.known_checkmark, "field 'mKnownCheckmark'");
        View view = (View) finder.findRequiredView(obj, R.id.contacts_close, "method 'onContactClose'");
        a.f6823a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onContactClose();
            }
        });
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1608a<T> mo10420a(T t) {
        return new C1608a<>(t);
    }
}
