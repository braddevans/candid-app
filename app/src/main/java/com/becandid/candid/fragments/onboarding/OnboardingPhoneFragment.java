package com.becandid.candid.fragments.onboarding;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.p001v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.activities.WebViewActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.ContactsInfo;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import java.util.ArrayList;
import p012rx.schedulers.Schedulers;

public class OnboardingPhoneFragment extends OnboardingBaseFragment {

    /* renamed from: b */
    String f7105b;

    /* renamed from: c */
    private Unbinder f7106c;
    @BindView(2131624965)
    TextView countryCode;
    @BindView(2131624966)
    EditText phone;
    @BindView(2131624962)
    TextView phoneHeader;
    @BindView(2131624992)
    TextView phoneInfo;
    @BindView(2131624993)
    TextView phonePrivacy;
    @BindView(2131624963)
    TextView phoneSubheader;
    @BindView(2131624967)
    Button phoneSubmit;
    @BindView(2131624961)
    TextView skip;
    @BindView(2131624994)
    FrameLayout spinny;
    @BindView(2131624968)
    RelativeLayout wrapper;

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m9649c() {
        ApiService.m14297a().mo14168n("onboarding/phone/skip").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        AppState.contactsInfo = null;
        ((BaseActivity) getActivity()).closeKeyboard();
        if (AppState.account != null) {
            Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Skip Phone Number").mo16461a("logged_in", "true"));
        } else {
            Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Skip Phone Number").mo16461a("logged_in", "false"));
        }
        if (this.f7105b == null) {
            this.spinny.setVisibility(0);
            ApiService.m14297a().mo14125b(false).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                /* renamed from: a */
                public void onNext(NetworkData networkData) {
                    OnboardingPhoneFragment.this.spinny.setVisibility(8);
                    if (!networkData.success) {
                        if (OnboardingPhoneFragment.this.getActivity() instanceof OnboardingActivity) {
                            ((OnboardingActivity) OnboardingPhoneFragment.this.getActivity()).communitySuggestions = networkData.groups;
                        }
                        RxBus.m14573a().mo14349a(new C2631at("phone"));
                        return;
                    }
                    if (networkData.my_info != null) {
                        AppState.account = networkData.my_info;
                        ((OnboardingActivity) OnboardingPhoneFragment.this.getActivity()).finishSyncAccount();
                    }
                    if (networkData.config != null) {
                        AppState.setConfig(networkData.config);
                    }
                    if (networkData.activity_settings != null) {
                        AppState.notificationSettings = new ArrayList<>(networkData.activity_settings);
                    }
                }

                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    OnboardingPhoneFragment.this.spinny.setVisibility(8);
                    Crashlytics.m16437a(th);
                    RxBus.m14573a().mo14349a(new C2631at("phone"));
                }
            });
        } else if (this.f7105b.equals("fb")) {
            Bundle bundle = new Bundle();
            bundle.putString("second_fragment", this.f7105b);
            ((OnboardingActivity) getActivity()).switchFragment("phone", this.f7105b, bundle);
        } else {
            getActivity().setResult(-1);
            getActivity().finish();
        }
    }

    /* renamed from: d */
    private void m9650d() {
        this.phoneHeader.setText(AppState.config.getString("phone_header", getString(R.string.phone_header)));
        this.phoneSubheader.setText(AppState.config.getString("phone_subheader", getString(R.string.phone_subheader)));
        this.phone.setHint(AppState.config.getString("phone_input", getString(R.string.phone_input_hint)));
        this.phonePrivacy.setText(AppState.config.getString("more_info", getString(R.string.privacy)));
        this.phoneInfo.setText(AppState.config.getString("phone_info", getString(R.string.phone_info)));
    }

    /* renamed from: a */
    public Animation mo10632a(int i, int i2, int i3, final int i4, final View view) {
        float f = getResources().getDisplayMetrics().density;
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, ((float) i) * f, 0, 0.0f, 0, ((float) i2) * f);
        translateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        translateAnimation.setDuration(200);
        translateAnimation.setStartOffset((long) (i3 + 1000));
        translateAnimation.setFillAfter(true);
        translateAnimation.setFillEnabled(true);
        translateAnimation.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        view.findViewById(i4).setVisibility(0);
                    }
                }, 1000);
            }
        });
        return translateAnimation;
    }

    /* renamed from: a */
    public void mo10633a(View view) {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, -0.5f, 0, 0.0f, 0, 0.0f);
        translateAnimation.setDuration(200);
        translateAnimation.setStartOffset(800);
        translateAnimation.setFillAfter(true);
        translateAnimation.setFillEnabled(true);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.5f, 0, 0.0f, 0, 0.0f);
        translateAnimation2.setDuration(200);
        translateAnimation2.setStartOffset(800);
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setFillEnabled(true);
        Animation a = mo10632a(-45, -10, 0, R.id.phone_spark_1, view);
        Animation a2 = mo10632a(-28, -20, 60, R.id.phone_spark_2, view);
        Animation a3 = mo10632a(-23, -35, 30, R.id.phone_spark_3, view);
        Animation a4 = mo10632a(-2, -35, 80, R.id.phone_spark_4, view);
        Animation a5 = mo10632a(18, -30, 120, R.id.phone_spark_5, view);
        Animation a6 = mo10632a(16, 30, 30, R.id.phone_spark_6, view);
        Animation a7 = mo10632a(-2, 35, 100, R.id.phone_spark_7, view);
        Animation a8 = mo10632a(-30, 23, 40, R.id.phone_spark_8, view);
        Animation a9 = mo10632a(-35, 8, 130, R.id.phone_spark_9, view);
        Animation a10 = mo10632a(-15, -38, 180, R.id.phone_spark_10, view);
        Animation a11 = mo10632a(3, -34, 60, R.id.phone_spark_11, view);
        Animation a12 = mo10632a(22, -28, 80, R.id.phone_spark_12, view);
        Animation a13 = mo10632a(29, -10, 140, R.id.phone_spark_13, view);
        Animation a14 = mo10632a(30, 8, 40, R.id.phone_spark_14, view);
        Animation a15 = mo10632a(20, 24, 50, R.id.phone_spark_15, view);
        Animation a16 = mo10632a(4, 32, 110, R.id.phone_spark_16, view);
        Animation a17 = mo10632a(-16, 32, 30, R.id.phone_spark_17, view);
        view.findViewById(R.id.phone_icon_bunny).startAnimation(translateAnimation);
        view.findViewById(R.id.phone_icon_cat).startAnimation(translateAnimation2);
        view.findViewById(R.id.phone_spark_1).startAnimation(a);
        view.findViewById(R.id.phone_spark_2).startAnimation(a2);
        view.findViewById(R.id.phone_spark_3).startAnimation(a3);
        view.findViewById(R.id.phone_spark_4).startAnimation(a4);
        view.findViewById(R.id.phone_spark_5).startAnimation(a5);
        view.findViewById(R.id.phone_spark_6).startAnimation(a6);
        view.findViewById(R.id.phone_spark_7).startAnimation(a7);
        view.findViewById(R.id.phone_spark_8).startAnimation(a8);
        view.findViewById(R.id.phone_spark_9).startAnimation(a9);
        view.findViewById(R.id.phone_spark_10).startAnimation(a10);
        view.findViewById(R.id.phone_spark_11).startAnimation(a11);
        view.findViewById(R.id.phone_spark_12).startAnimation(a12);
        view.findViewById(R.id.phone_spark_13).startAnimation(a13);
        view.findViewById(R.id.phone_spark_14).startAnimation(a14);
        view.findViewById(R.id.phone_spark_15).startAnimation(a15);
        view.findViewById(R.id.phone_spark_16).startAnimation(a16);
        view.findViewById(R.id.phone_spark_17).startAnimation(a17);
    }

    /* renamed from: a */
    public boolean mo10634a() {
        switch (((TelephonyManager) getActivity().getSystemService("phone")).getSimState()) {
            case 0:
            case 1:
            case 4:
                return false;
            default:
                return true;
        }
    }

    /* renamed from: b */
    public void mo10635b() {
        this.phoneSubmit.setEnabled(false);
        String obj = this.phone.getText().toString();
        String charSequence = this.countryCode.getText().toString();
        if (AppState.contactsInfo == null) {
            AppState.contactsInfo = new ContactsInfo();
        }
        AppState.contactsInfo.phone_number = charSequence + obj;
        this.spinny.setVisibility(0);
        ((OnboardingActivity) getActivity()).setPhoneNumber(obj);
        ApiService.m14297a().mo14157i(AppState.contactsInfo.phone_number).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                if (AppState.account != null) {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Number").mo16461a("success", "true")).mo16461a("logged_in", "true"));
                } else {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Number").mo16461a("success", "true")).mo16461a("logged_in", "false"));
                }
                if (networkData == null || !networkData.success) {
                    OnboardingPhoneFragment.this.spinny.setVisibility(8);
                    OnboardingPhoneFragment.this.phoneSubmit.setEnabled(true);
                    if (networkData != null) {
                        Toast.makeText(OnboardingPhoneFragment.this.getActivity(), networkData.error, 0).show();
                        return;
                    }
                    return;
                }
                OnboardingPhoneFragment.this.spinny.setVisibility(8);
                Bundle bundle = new Bundle();
                if (OnboardingPhoneFragment.this.f7105b != null) {
                    bundle.putString("second_fragment", OnboardingPhoneFragment.this.f7105b);
                }
                ((OnboardingActivity) OnboardingPhoneFragment.this.getActivity()).switchFragment("phone", "verify", bundle);
                ((BaseActivity) OnboardingPhoneFragment.this.getActivity()).closeKeyboard();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                if (AppState.account != null) {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Number").mo16461a("success", "false")).mo16461a("logged_in", "true"));
                } else {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Number").mo16461a("success", "false")).mo16461a("logged_in", "false"));
                }
                OnboardingPhoneFragment.this.spinny.setVisibility(8);
                OnboardingPhoneFragment.this.phoneSubmit.setEnabled(true);
                Toast.makeText(OnboardingPhoneFragment.this.getActivity(), th.toString(), 0).show();
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f7105b = arguments.getString("second_fragment");
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        final View inflate = layoutInflater.inflate(R.layout.onboarding_phone, viewGroup, false);
        this.f7106c = ButterKnife.bind((Object) this, inflate);
        ((BaseActivity) getActivity()).enableKeyboardEvents(inflate);
        m9650d();
        this.phonePrivacy.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(OnboardingPhoneFragment.this.getContext(), WebViewActivity.class);
                intent.putExtra("title", "Why Can I Trust Candid?");
                intent.putExtra("url", GossipApplication.f6096d + "content/whysafe");
                OnboardingPhoneFragment.this.getContext().startActivity(intent);
            }
        });
        this.phone.requestFocus();
        ((BaseActivity) getActivity()).openKeyboard();
        Rect rect = new Rect();
        inflate.getWindowVisibleDisplayFrame(rect);
        final int i = rect.bottom - rect.top;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Rect rect = new Rect();
                inflate.getWindowVisibleDisplayFrame(rect);
                if (Float.compare(((float) (rect.bottom - rect.top)) / ((float) i), 0.52f) >= 0) {
                    Fragment findFragmentByTag = OnboardingPhoneFragment.this.f10360a.getSupportFragmentManager().findFragmentByTag("phone");
                    if (findFragmentByTag != null && findFragmentByTag.isVisible()) {
                        OnboardingPhoneFragment.this.wrapper.setVisibility(0);
                        OnboardingPhoneFragment.this.mo10633a(inflate);
                    }
                }
            }
        }, 800);
        final bch a = bch.m7625a("Select Country");
        String string = AppState.config.getString("show_phone_skip", "sim");
        char c = 65535;
        switch (string.hashCode()) {
            case 96673:
                if (string.equals("all")) {
                    c = 0;
                    break;
                }
                break;
            case 113879:
                if (string.equals("sim")) {
                    c = 1;
                    break;
                }
                break;
            case 3387192:
                if (string.equals("none")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.skip.setVisibility(0);
                this.skip.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        OnboardingPhoneFragment.this.m9649c();
                    }
                });
                if (mo10634a()) {
                    this.countryCode.setText(a.mo8779a((Context) getActivity()).mo8787a());
                    break;
                }
                break;
            case 1:
                if (mo10634a()) {
                    this.countryCode.setText(a.mo8779a((Context) getActivity()).mo8787a());
                    break;
                } else {
                    this.skip.setVisibility(0);
                    this.skip.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            OnboardingPhoneFragment.this.m9649c();
                        }
                    });
                    break;
                }
            case 2:
                if (mo10634a()) {
                    this.countryCode.setText(a.mo8779a((Context) getActivity()).mo8787a());
                    break;
                }
                break;
        }
        this.countryCode.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OnboardingPhoneFragment.this.countryCode.setEnabled(false);
                a.show(OnboardingPhoneFragment.this.getFragmentManager(), "COUNTRY_PICKER");
                a.mo8780a((bci) new bci() {
                    /* renamed from: a */
                    public void mo8786a(String str, String str2, String str3, int i) {
                        OnboardingPhoneFragment.this.countryCode.setText(str3);
                        OnboardingPhoneFragment.this.countryCode.setEnabled(true);
                        a.dismiss();
                    }
                });
            }
        });
        this.phoneSubmit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OnboardingPhoneFragment.this.mo10635b();
            }
        });
        this.phone.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.length() >= 5) {
                    OnboardingPhoneFragment.this.phoneSubmit.setEnabled(true);
                } else {
                    OnboardingPhoneFragment.this.phoneSubmit.setEnabled(false);
                }
            }
        });
        if (((OnboardingActivity) getActivity()).getPhoneNumber() != null) {
            this.phone.setText(((OnboardingActivity) getActivity()).getPhoneNumber());
        }
        return inflate;
    }
}
