package com.becandid.candid.fragments.onboarding;

import android.app.Activity;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.MeSettingsActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.views.TagBox;
import com.becandid.candid.views.TagBox.C1851a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class OnboardingTagsFragment extends Fragment implements C1851a {

    /* renamed from: a */
    private Unbinder f7125a;

    /* renamed from: b */
    private HashSet<String> f7126b;

    /* renamed from: c */
    private Bundle f7127c;
    @BindView(2131625000)
    Button tagCancel;
    @BindView(2131624999)
    RelativeLayout tagContainer;
    @BindView(2131625001)
    Button tagSubmit;
    @BindView(2131624996)
    TextView tagsHeader;

    /* renamed from: b */
    private void m9661b() {
        this.tagsHeader.setText(AppState.config.getString("tags_header", getString(R.string.tags_header)));
    }

    /* renamed from: c */
    private void m9662c() {
        if (AppState.activeTags == null && AppState.account.tags != null) {
            this.f7126b = new HashSet<>(AppState.account.tags);
        }
        int childCount = this.tagContainer.getChildCount();
        for (int i = 0; i < childCount; i++) {
            LinearLayout linearLayout = (LinearLayout) this.tagContainer.getChildAt(i);
            for (int i2 = 0; i2 < linearLayout.getChildCount(); i2++) {
                TagBox tagBox = (TagBox) linearLayout.getChildAt(i2);
                tagBox.setTagClickListener(this);
                if (!(AppState.account == null || AppState.account.tags == null || !AppState.account.tags.contains(tagBox.getTagName()))) {
                    tagBox.setSelected(true);
                    this.tagSubmit.setEnabled(true);
                }
            }
        }
    }

    /* renamed from: a */
    public void mo10654a() {
        this.tagSubmit.setEnabled(false);
        if (this.f7126b == null || this.f7126b.isEmpty()) {
            AppState.activeTags = new ArrayList<>();
            AppState.account.tags = AppState.activeTags;
        } else {
            AppState.activeTags = new ArrayList<>(this.f7126b);
            AppState.account.tags = AppState.activeTags;
        }
        if (this.f7127c == null || !this.f7127c.containsKey(MeSettingsActivity.TAG_SETTINGS_KEY)) {
            ((OnboardingActivity) getActivity()).finishGetGroups();
            return;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("tags", DataUtil.join(AppState.activeTags));
        ApiService.m14297a().mo14158i((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                RxBus.m14573a().mo14349a(new C2637az(1, networkData.unread_groups_count, true));
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        });
        getActivity().finish();
    }

    /* renamed from: a */
    public void mo10405a(View view, String str) {
        view.setSelected(!view.isSelected());
        if (this.f7126b == null) {
            this.f7126b = new HashSet<>();
        }
        if (this.f7126b.contains(str)) {
            this.f7126b.remove(str);
            if (this.f7126b.isEmpty()) {
                this.tagSubmit.setEnabled(false);
                return;
            }
            return;
        }
        this.f7126b.add(str);
        this.tagSubmit.setEnabled(true);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_tags, viewGroup, false);
        this.f7125a = ButterKnife.bind((Object) this, inflate);
        m9661b();
        if (AppState.tags == null) {
            AppState.tags = Arrays.asList(new String[]{"Politics", "Celebrities", "Music", "Technology", "Fashion", "Business", "School", "Art", "Photography", "LGBT", "Relationships", "Sports", "Funny", "Teens", "Confessions", "Personal", "Sex", "Family", "Work", "Faith", "Food", "Entertainment", "Womens Issues"});
        }
        List<String> list = AppState.tags;
        this.f7127c = getArguments();
        if (this.f7127c == null || !this.f7127c.containsKey(MeSettingsActivity.TAG_SETTINGS_KEY)) {
            C2699jj.m14609a((Activity) getActivity(), this.tagContainer, list);
        } else {
            C2699jj.m14609a((Activity) getActivity(), this.tagContainer, list);
            this.f7126b = new HashSet<>(AppState.account.tags);
            if (!this.f7126b.isEmpty()) {
                this.tagSubmit.setEnabled(true);
            }
            this.tagCancel.setVisibility(0);
            this.tagCancel.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    OnboardingTagsFragment.this.getActivity().finish();
                }
            });
        }
        this.tagSubmit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OnboardingTagsFragment.this.mo10654a();
            }
        });
        return inflate;
    }

    public void onResume() {
        super.onResume();
        m9662c();
    }
}
