package com.becandid.candid.fragments;

import android.support.design.widget.CoordinatorLayout;
import android.support.p003v7.widget.RecyclerView;
import android.support.percent.PercentRelativeLayout;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.CommunityFeedFragment;
import com.becandid.candid.views.CommunityOnboardingSimpleView;

public class CommunityFeedFragment$$ViewBinder<T extends CommunityFeedFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.CommunityFeedFragment$$ViewBinder$a */
    /* compiled from: CommunityFeedFragment$$ViewBinder */
    public static class C1586a<T extends CommunityFeedFragment> implements Unbinder {

        /* renamed from: a */
        private T f6778a;

        protected C1586a(T t) {
            this.f6778a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10378a(T t) {
            t.communityFeedOnboarding = null;
            t.communityFeedContent = null;
            t.button = null;
            t.spinny = null;
            t.subheader = null;
            t.header = null;
            t.skip = null;
            t.addHighSchool = null;
            t.highSchoolDefault = null;
            t.highSchoolSelected = null;
            t.highSchoolMembers = null;
            t.highSchoolImgSelected = null;
            t.highSchoolSelectedName = null;
            t.highSchoolText = null;
            t.addCollege = null;
            t.collegeDefault = null;
            t.collegeSelected = null;
            t.collegeMembers = null;
            t.collegeImgSelected = null;
            t.collegeSelectedName = null;
            t.collegeText = null;
            t.addNone = null;
            t.noneText = null;
            t.noneCheck = null;
            t.communityFeedFilterContainer = null;
            t.communityFeedSettings = null;
            t.communityFeedSettingsFilter = null;
            t.communityFeedTagRecycler = null;
            t.communityFeedTagRecyclerEmpty = null;
            t.communityFeedAdd = null;
            t.communityFeedEmpty = null;
            t.communityFeedHighschool = null;
            t.communityFeedCollege = null;
            t.communityFeedWork = null;
            t.secondRowDouble = null;
            t.secondRowSingle = null;
            t.communityFeedOnboardingSimple = null;
        }

        public final void unbind() {
            if (this.f6778a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10378a(this.f6778a);
            this.f6778a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1586a a = mo10377a(t);
        t.communityFeedOnboarding = (PercentRelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_feed_onboarding, "field 'communityFeedOnboarding'"), R.id.community_feed_onboarding, "field 'communityFeedOnboarding'");
        t.communityFeedContent = (CoordinatorLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_feed_content, "field 'communityFeedContent'"), R.id.community_feed_content, "field 'communityFeedContent'");
        t.button = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.community_button, "field 'button'"), R.id.community_button, "field 'button'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_spinny, "field 'spinny'"), R.id.community_spinny, "field 'spinny'");
        t.subheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_subheader, "field 'subheader'"), R.id.community_subheader, "field 'subheader'");
        t.header = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_header, "field 'header'"), R.id.community_header, "field 'header'");
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_skip, "field 'skip'"), R.id.community_skip, "field 'skip'");
        t.addHighSchool = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_add_highschool, "field 'addHighSchool'"), R.id.community_add_highschool, "field 'addHighSchool'");
        t.highSchoolDefault = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_default, "field 'highSchoolDefault'"), R.id.community_highschool_default, "field 'highSchoolDefault'");
        t.highSchoolSelected = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_selected, "field 'highSchoolSelected'"), R.id.community_highschool_selected, "field 'highSchoolSelected'");
        t.highSchoolMembers = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_num_members, "field 'highSchoolMembers'"), R.id.community_highschool_num_members, "field 'highSchoolMembers'");
        t.highSchoolImgSelected = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_img_selected, "field 'highSchoolImgSelected'"), R.id.community_highschool_img_selected, "field 'highSchoolImgSelected'");
        t.highSchoolSelectedName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_name, "field 'highSchoolSelectedName'"), R.id.community_highschool_name, "field 'highSchoolSelectedName'");
        t.highSchoolText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_text, "field 'highSchoolText'"), R.id.community_highschool_text, "field 'highSchoolText'");
        t.addCollege = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_add_college, "field 'addCollege'"), R.id.community_add_college, "field 'addCollege'");
        t.collegeDefault = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_default, "field 'collegeDefault'"), R.id.community_college_default, "field 'collegeDefault'");
        t.collegeSelected = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_selected, "field 'collegeSelected'"), R.id.community_college_selected, "field 'collegeSelected'");
        t.collegeMembers = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_num_members, "field 'collegeMembers'"), R.id.community_college_num_members, "field 'collegeMembers'");
        t.collegeImgSelected = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_img_selected, "field 'collegeImgSelected'"), R.id.community_college_img_selected, "field 'collegeImgSelected'");
        t.collegeSelectedName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_name, "field 'collegeSelectedName'"), R.id.community_college_name, "field 'collegeSelectedName'");
        t.collegeText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_text, "field 'collegeText'"), R.id.community_college_text, "field 'collegeText'");
        t.addNone = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_add_none, "field 'addNone'"), R.id.community_add_none, "field 'addNone'");
        t.noneText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_none_text, "field 'noneText'"), R.id.community_none_text, "field 'noneText'");
        t.noneCheck = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.community_none_check, "field 'noneCheck'"), R.id.community_none_check, "field 'noneCheck'");
        t.communityFeedFilterContainer = (View) finder.findRequiredView(obj, R.id.community_feed_settings_container, "field 'communityFeedFilterContainer'");
        t.communityFeedSettings = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_feed_settings_button, "field 'communityFeedSettings'"), R.id.community_feed_settings_button, "field 'communityFeedSettings'");
        t.communityFeedSettingsFilter = (View) finder.findRequiredView(obj, R.id.community_feed_settings_filter, "field 'communityFeedSettingsFilter'");
        t.communityFeedTagRecycler = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.community_feed_tag_recycler, "field 'communityFeedTagRecycler'"), R.id.community_feed_tag_recycler, "field 'communityFeedTagRecycler'");
        t.communityFeedTagRecyclerEmpty = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_feed_tag_recycler_empty, "field 'communityFeedTagRecyclerEmpty'"), R.id.community_feed_tag_recycler_empty, "field 'communityFeedTagRecyclerEmpty'");
        t.communityFeedAdd = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.community_feed_add, "field 'communityFeedAdd'"), R.id.community_feed_add, "field 'communityFeedAdd'");
        t.communityFeedEmpty = (View) finder.findRequiredView(obj, R.id.community_feed_empty, "field 'communityFeedEmpty'");
        t.communityFeedHighschool = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.highschool_feed, "field 'communityFeedHighschool'"), R.id.highschool_feed, "field 'communityFeedHighschool'");
        t.communityFeedCollege = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.college_feed, "field 'communityFeedCollege'"), R.id.college_feed, "field 'communityFeedCollege'");
        t.communityFeedWork = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.work_feed, "field 'communityFeedWork'"), R.id.work_feed, "field 'communityFeedWork'");
        t.secondRowDouble = (View) finder.findRequiredView(obj, R.id.second_row_double, "field 'secondRowDouble'");
        t.secondRowSingle = (View) finder.findRequiredView(obj, R.id.second_row_single, "field 'secondRowSingle'");
        t.communityFeedOnboardingSimple = (CommunityOnboardingSimpleView) finder.castView((View) finder.findRequiredView(obj, R.id.community_feed_onboarding_simple, "field 'communityFeedOnboardingSimple'"), R.id.community_feed_onboarding_simple, "field 'communityFeedOnboardingSimple'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1586a<T> mo10377a(T t) {
        return new C1586a<>(t);
    }
}
