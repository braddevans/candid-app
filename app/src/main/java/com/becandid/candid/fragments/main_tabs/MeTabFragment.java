package com.becandid.candid.fragments.main_tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.C0411a;
import android.support.design.widget.TabLayout.C0416d;
import android.support.p001v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.MeSettingsActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.activities.SeeAllBadgesActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.ContactsInfo;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.FacebookInfo.LoadCallback;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.User;
import com.becandid.candid.models.JoinedFacebookData;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.views.BadgeInfoView;
import com.becandid.candid.views.BadgeInfoView.C1788a;
import com.becandid.candid.views.TabViewPager;
import com.becandid.thirdparty.BlurTask;
import com.becandid.thirdparty.BlurTask.BadgeType;
import com.facebook.AccessToken;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class MeTabFragment extends BaseMainTabFragment implements C0411a, C1788a {
    @BindView(2131625106)
    LinearLayout buttonPanel;
    @BindView(2131625107)
    View connectContact;
    @BindView(2131625108)
    View connectFb;

    /* renamed from: d */
    private final String f6938d = "Activity";

    /* renamed from: e */
    private final String f6939e = "Groups";

    /* renamed from: f */
    private final String f6940f = "Posts";

    /* renamed from: g */
    private final String f6941g = "History";

    /* renamed from: h */
    private CallbackManager f6942h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public boolean f6943i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public long f6944j = 0;

    /* renamed from: k */
    private TabLayout f6945k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public Context f6946l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public BaseActivity f6947m;
    @BindView(2131624083)
    ImageView mBadgeIcon;
    @BindView(2131624350)
    FrameLayout mBadgeInfoContainer;
    @BindView(2131624080)
    TextView mBadgeTitle;
    @BindView(2131625105)
    TextView mGroupsCount;
    @BindView(2131625103)
    TextView mNumContacts;
    @BindView(2131625102)
    TextView mNumFriends;
    @BindView(2131625104)
    TextView mPostsCount;
    @BindView(2131625097)
    View mProfileContainer;
    @BindView(2131625111)
    View mProfileSpinny;
    @BindView(2131625100)
    TextView mQualityScore;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public BadgeInfoView f6948n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public View f6949o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public TabPagerAdapter f6950p;
    @BindView(2131625110)
    TabViewPager viewPager;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9518a(View view) {
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down_animation);
        loadAnimation.setDuration(500);
        this.f6949o.startAnimation(loadAnimation);
        view.postDelayed(new Runnable() {
            public void run() {
                MeTabFragment.this.mProfileContainer.setVisibility(0);
                MeTabFragment.this.mBadgeInfoContainer.removeView(MeTabFragment.this.f6948n);
                MeTabFragment.this.mBadgeInfoContainer.setVisibility(8);
            }
        }, 500);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9519a(BadgeInfoView badgeInfoView) {
        badgeInfoView.setSeeAllBadgesListener(this);
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public void m9528l() {
        ((MeGroupsFragment) this.f6950p.mo13888a("Groups")).mo13911b();
        ((MePostsFragment) this.f6950p.mo13888a("Posts")).mo13948b();
        ((MeHistoryFragment) this.f6950p.mo13888a("History")).mo13942b();
    }

    /* renamed from: a */
    public void mo10479a() {
        super.mo10479a();
        mo10502a(false, false);
    }

    /* renamed from: a */
    public void mo10501a(C2637az azVar) {
        int currentItem = this.viewPager.getCurrentItem();
        if (azVar != null && currentItem != 0 && azVar.f10618a == 3 && AppState.config.getBoolean("messaging_turned_on", true)) {
            ((TextView) this.f6945k.mo1765a(0).mo1811a()).setCompoundDrawablesWithIntrinsicBounds(0, 0, azVar.f10619b > 0 ? R.drawable.unread_dot : 0, 0);
            ((TextView) this.f6945k.mo1765a(0).mo1811a()).setMaxLines(1);
        }
    }

    /* renamed from: a */
    public void mo10502a(final boolean z, final boolean z2) {
        if (AppState.account == null || AppState.account.user_id <= 0) {
            Toast.makeText(getContext(), "Unable to find your profile.", 1).show();
            return;
        }
        this.f6943i = true;
        if (z) {
            this.viewPager.setVisibility(8);
            this.mProfileSpinny.setVisibility(0);
        }
        ApiService.m14297a().mo14141e(AppState.account.user_id).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                if (AppState.account != null) {
                    MeTabFragment.this.f10276c = false;
                    MeTabFragment.this.f6943i = false;
                    MeTabFragment.this.mProfileSpinny.setVisibility(8);
                    MeTabFragment.this.viewPager.setVisibility(0);
                    if (MeTabFragment.this.mo10507f()) {
                        if (MeTabFragment.this.f6950p.mo13888a("Activity") != null) {
                            ((MeActivityFragment) MeTabFragment.this.f6950p.mo13888a("Activity")).mo13905a(z2);
                        }
                        MeTabFragment.this.m9528l();
                    } else {
                        if (MeTabFragment.this.f6950p.mo13888a("Activity") != null) {
                            ((MeActivityFragment) MeTabFragment.this.f6950p.mo13888a("Activity")).mo13905a(z2);
                        }
                        if (z) {
                            MeTabFragment.this.m9528l();
                        }
                    }
                    if (networkData.profile != null) {
                        AppState.account.num_posts = networkData.profile.num_posts;
                        AppState.account.num_groups = networkData.profile.num_groups;
                        AppState.account.num_fb_friends = networkData.profile.num_fb_friends;
                        AppState.account.num_phone_friends = networkData.profile.num_phone_friends;
                        AppState.account.quality_score = networkData.profile.quality_score;
                    }
                    MeTabFragment.this.f6944j = System.currentTimeMillis() / 1000;
                    MeTabFragment.this.mo10505d();
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                MeTabFragment.this.f10276c = false;
                MeTabFragment.this.f6943i = false;
                if (MeTabFragment.this.viewPager != null && MeTabFragment.this.mProfileSpinny != null) {
                    MeTabFragment.this.viewPager.setVisibility(0);
                    MeTabFragment.this.mProfileSpinny.setVisibility(8);
                    Crashlytics.m16437a(th);
                }
            }
        });
    }

    /* renamed from: b */
    public void mo10480b() {
        this.mProfileContainer.setVisibility(0);
    }

    /* renamed from: c */
    public void mo10481c() {
        this.mProfileContainer.setVisibility(8);
    }

    @OnClick({2131625107})
    public void connectContacts(View view) {
        if (AppState.contactsInfo == null) {
            AppState.contactsInfo = new ContactsInfo();
        }
        if (!AppState.account.have_phone_number || ContextCompat.checkSelfPermission(view.getContext(), "android.permission.READ_CONTACTS") == 0) {
            Intent intent = new Intent(view.getContext(), OnboardingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("first_fragment", "phone");
            bundle.putString("second_fragment", "phone");
            intent.putExtras(bundle);
            view.getContext().startActivity(intent);
            return;
        }
        ActivityCompat.requestPermissions((Activity) view.getContext(), new String[]{"android.permission.READ_CONTACTS"}, 1);
    }

    @OnClick({2131625108})
    public void connectFB(final View view) {
        view.setVisibility(8);
        if (this.f6942h == null) {
            this.f6942h = C3115a.m17289a();
            abo.m502c().mo264a(this.f6942h, (FacebookCallback<abp>) new FacebookCallback<abp>() {
                /* renamed from: a */
                public void onSuccess(abp abp) {
                    Log.d("FBConnect", "success");
                    AppState.setFBInfo(abp.mo274a(), new LoadCallback() {
                        public void onNext(int i) {
                            if (AppState.account != null) {
                                AppState.account.num_fb_friends = i;
                            }
                            MeTabFragment.this.mo10505d();
                            MeTabFragment.this.mo10510i();
                        }
                    });
                }

                public void onCancel() {
                    Log.d("FBConnect", "cancel");
                    view.setVisibility(0);
                }

                public void onError(FacebookException facebookException) {
                    Log.e("FBConnect", facebookException.toString());
                    if (!(facebookException instanceof FacebookAuthorizationException) || AccessToken.m10408a() == null) {
                        Toast.makeText(GossipApplication.m9012a(), "Unable to connect to Facebook", 1).show();
                        view.setVisibility(0);
                        return;
                    }
                    abo.m502c().mo270d();
                    abo.m502c().mo261a((Activity) MeTabFragment.this.f6947m, (Collection<String>) Arrays.asList("public_profile,user_friends,user_work_history,user_education_history".split(",")));
                }
            });
        }
        abo.m502c().mo261a((Activity) this.f6947m, (Collection<String>) Arrays.asList("public_profile,user_friends,user_work_history,user_education_history".split(",")));
    }

    /* renamed from: d */
    public void mo10505d() {
        if (AppState.account != null) {
            this.mPostsCount.setText(Integer.toString(AppState.account.num_posts));
            this.mGroupsCount.setText(Integer.toString(AppState.account.num_groups));
            this.mNumFriends.setText(Integer.toString(AppState.account.num_fb_friends));
            this.mNumContacts.setText(Integer.toString(Integer.valueOf(AppState.account.num_phone_friends).intValue()));
            final String format = new DecimalFormat("#").format((double) AppState.account.quality_score);
            if (format == null || format.equals("0")) {
                this.mQualityScore.setVisibility(8);
            } else {
                this.mQualityScore.setVisibility(0);
                this.mQualityScore.setText(format);
                this.mQualityScore.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if (!AppState.blurTaskCalledOnFlight) {
                            AppState.blurTaskCalledOnFlight = true;
                            new BlurTask((Activity) MeTabFragment.this.f6946l, ((Activity) MeTabFragment.this.f6946l).findViewById(16908290), format, BadgeType.ME_QUALITY_SCORE, null, null).execute(new Void[0]);
                        }
                    }
                });
            }
            C2548a a = BadgeFactory.m14098a().mo13968a(AppState.account.badge_status);
            if (a != null) {
                this.mBadgeIcon.setImageResource(a.f10366c);
                this.mBadgeTitle.setText(a.f10365b);
            }
            if (!AppState.account.have_fb || !AppState.account.have_phone_number) {
                this.buttonPanel.setVisibility(0);
                if (AppState.fbInfo != null) {
                    this.connectFb.setVisibility(8);
                } else {
                    this.connectFb.setVisibility(0);
                }
                if (AppState.account.have_phone_number) {
                    this.connectContact.setVisibility(8);
                } else {
                    this.connectContact.setVisibility(0);
                }
            } else {
                this.buttonPanel.setVisibility(8);
            }
        }
    }

    /* renamed from: e */
    public void mo10506e() {
        if (this.viewPager != null) {
            this.viewPager.setCurrentItem(0, true);
            mo10502a(true, true);
            if (getView() != null) {
                AppBarLayout appBarLayout = (AppBarLayout) getView().findViewById(R.id.appbar);
                if (appBarLayout != null) {
                    appBarLayout.setExpanded(true, true);
                }
            }
        }
    }

    /* renamed from: f */
    public boolean mo10507f() {
        return (System.currentTimeMillis() / 1000) - this.f6944j > ((long) AppState.config.getInt("profile_update_time", 300));
    }

    /* renamed from: g */
    public void mo10508g() {
        GossipApplication.m9012a().mo9699c();
        mo10511j();
        this.connectContact.setVisibility(8);
    }

    /* renamed from: h */
    public void mo10509h() {
        this.connectContact.setEnabled(true);
    }

    /* renamed from: i */
    public void mo10510i() {
        HashMap hashMap = new HashMap();
        hashMap.put("fb_token", AppState.fbInfo.fbToken.mo11466b());
        hashMap.put("fb_uid", AppState.fbInfo.fbToken.mo11476i());
        HashMap hashMap2 = new HashMap();
        hashMap2.put("type", "fb");
        hashMap2.put("fb_uid", AppState.fbInfo.fbToken.mo11476i());
        hashMap2.put("id_list", DataUtil.join(AppState.fbInfo.friendIds));
        bjs.m8432a(ApiService.m14297a().mo14124b((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()), ApiService.m14297a().mo14100a(AppState.fbInfo).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()), (bkk<? super T1, ? super T2, ? extends R>) new bkk<ays, NetworkData, JoinedFacebookData>() {
            /* renamed from: a */
            public JoinedFacebookData call(ays ays, NetworkData networkData) {
                return new JoinedFacebookData(ays, networkData);
            }
        }).mo9258b((bjy<? super T>) new bjy<JoinedFacebookData>() {
            /* renamed from: a */
            public void onNext(JoinedFacebookData joinedFacebookData) {
                NetworkData networkData = joinedFacebookData.autoJoin;
                if (AppState.groups == null) {
                    AppState.groups = new ArrayList();
                }
                if (!(networkData == null || networkData.groups == null)) {
                    for (Group group : networkData.groups) {
                        if (!AppState.groups.contains(group)) {
                            AppState.groups.add(group);
                        }
                        if (group.community_membership == 1 && AppState.setCommunity == 0) {
                            AppState.setCommunity = 1;
                        }
                        RxBus.m14573a().mo14349a(new C2659p(group));
                    }
                }
                if (!(networkData == null || networkData.my_info == null)) {
                    AppState.account.num_groups = networkData.my_info.num_groups;
                }
                MeTabFragment.this.mo10505d();
            }

            public void onCompleted() {
                RxBus.m14573a().mo14349a(new C2640bb());
            }

            public void onError(Throwable th) {
            }
        });
    }

    /* renamed from: j */
    public void mo10511j() {
        HashMap hashMap = new HashMap();
        hashMap.put("type", "phone_number");
        hashMap.put("id_list", DataUtil.join(AppState.contactsInfo.contacts));
        ApiService.m14297a().mo14133c((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            /* renamed from: a */
            public void onNext(ays ays) {
                AppState.account.num_phone_friends = ays.mo8410b("num_phone_friends").mo8390g();
                AppState.saveState(GossipApplication.m9012a());
                MeTabFragment.this.mo10505d();
                RxBus.m14573a().mo14349a(new C2640bb());
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        });
    }

    /* renamed from: k */
    public void mo10512k() {
        m9518a((View) this.mBadgeInfoContainer);
        final Context context = getContext();
        this.mProfileContainer.postDelayed(new Runnable() {
            public void run() {
                context.startActivity(new Intent(context, SeeAllBadgesActivity.class));
            }
        }, 600);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (this.f6942h != null) {
            this.f6942h.mo11618a(i, i2, intent);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.profile_tab, viewGroup, false);
        this.f10275b = ButterKnife.bind((Object) this, inflate);
        this.f6946l = getContext();
        this.f6947m = (BaseActivity) getActivity();
        this.f6945k = (TabLayout) inflate.findViewById(R.id.me_subtabs);
        this.f6945k.setSelectedTabIndicatorColor(getResources().getColor(R.color.gossip));
        this.f6945k.setTabTextColors(Color.parseColor("#888888"), getResources().getColor(R.color.gossip));
        this.f6950p = new TabPagerAdapter(this.f6947m.getSupportFragmentManager());
        MeGroupsFragment hzVar = new MeGroupsFragment();
        MePostsFragment ibVar = new MePostsFragment();
        MeHistoryFragment iaVar = new MeHistoryFragment();
        if (AppState.config.getBoolean("messaging_turned_on", true)) {
            this.f6950p.mo13889a(new MeActivityFragment(), "Activity");
        }
        this.f6950p.mo13889a(hzVar, "Groups");
        this.f6950p.mo13889a(ibVar, "Posts");
        this.f6950p.mo13889a(iaVar, "History");
        this.viewPager.setAdapter(this.f6950p);
        this.viewPager.setOffscreenPageLimit(3);
        this.viewPager.setCurrTab("me");
        this.f6945k.setupWithViewPager(this.viewPager);
        this.f6945k.setOnTabSelectedListener(this);
        for (int i = 0; i < this.f6945k.getTabCount(); i++) {
            TextView textView = (TextView) LayoutInflater.from(this.f6946l).inflate(R.layout.custom_message_tab, null);
            textView.setText(this.f6945k.mo1765a(i).mo1815d());
            textView.setCompoundDrawablePadding(0);
            this.f6945k.mo1765a(i).mo1809a((View) textView);
        }
        inflate.findViewById(R.id.me_tab_settings).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MeTabFragment.this.getContext().startActivity(new Intent(MeTabFragment.this.getContext(), MeSettingsActivity.class));
            }
        });
        this.f6947m.addToSubscriptionList(RxBus.m14573a().mo14348a(C2657n.class, (bjy<T>) new bjy<C2657n>() {
            /* renamed from: a */
            public void onNext(C2657n nVar) {
                ((MePostsFragment) MeTabFragment.this.f6950p.mo13888a("Posts")).mo13947a(nVar.f10650a);
                User user = AppState.account;
                user.num_posts++;
                MeTabFragment.this.mo10505d();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        this.f6947m.addToSubscriptionList(RxBus.m14573a().mo14348a(C2664u.class, (bjy<T>) new bjy<C2664u>() {
            /* renamed from: a */
            public void onNext(C2664u uVar) {
                ((MePostsFragment) MeTabFragment.this.f6950p.mo13888a("Posts")).mo13946a(uVar.f10667a);
                User user = AppState.account;
                user.num_posts--;
                MeTabFragment.this.mo10505d();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        this.f6947m.addToSubscriptionList(RxBus.m14573a().mo14348a(C2659p.class, (bjy<T>) new bjy<C2659p>() {
            /* renamed from: a */
            public void onNext(C2659p pVar) {
                ((MeGroupsFragment) MeTabFragment.this.f6950p.mo13888a("Groups")).mo13910a(pVar.f10653a);
                if (!(AppState.groups == null || AppState.account == null)) {
                    AppState.account.num_groups = AppState.groups.size();
                }
                MeTabFragment.this.mo10505d();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        this.f6947m.addToSubscriptionList(RxBus.m14573a().mo14348a(C2661r.class, (bjy<T>) new bjy<C2661r>() {
            /* renamed from: a */
            public void onNext(C2661r rVar) {
                ((MeGroupsFragment) MeTabFragment.this.f6950p.mo13888a("Groups")).mo13909a(rVar.f10656a);
                AppState.account.num_groups = AppState.groups.size();
                MeTabFragment.this.mo10479a();
                MeTabFragment.this.mo10505d();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        this.f6947m.addToSubscriptionList(RxBus.m14573a().mo14348a(C2647d.class, (bjy<T>) new bjy<C2647d>() {
            /* renamed from: a */
            public void onNext(C2647d dVar) {
                MeTabFragment.this.f6948n = new BadgeInfoView(MeTabFragment.this.f6947m, AppState.account.badge_status, true, dVar.f10635a);
                MeTabFragment.this.m9519a(MeTabFragment.this.f6948n);
                MeTabFragment.this.mBadgeInfoContainer.setVisibility(0);
                MeTabFragment.this.mProfileContainer.setVisibility(8);
                MeTabFragment.this.mBadgeInfoContainer.addView(MeTabFragment.this.f6948n);
                Animation loadAnimation = AnimationUtils.loadAnimation(MeTabFragment.this.f6947m, R.anim.slide_up_animation);
                loadAnimation.setDuration(500);
                MeTabFragment.this.f6949o = MeTabFragment.this.mBadgeInfoContainer.findViewById(R.id.badge_info);
                MeTabFragment.this.f6949o.startAnimation(loadAnimation);
                MeTabFragment.this.mBadgeInfoContainer.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        MeTabFragment.this.m9518a(view);
                    }
                });
                AppState.blurTaskCalledOnFlight = false;
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        mo10505d();
        this.mBadgeIcon.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (AppState.account != null && AppState.account.badge_status != null && !AppState.blurTaskCalledOnFlight) {
                    AppState.blurTaskCalledOnFlight = true;
                    new BlurTask(MeTabFragment.this.f6947m, R.id.profile_container).execute(new Void[0]);
                }
            }
        });
        return inflate;
    }

    public void onResume() {
        super.onResume();
        if (!this.f6943i && mo10507f()) {
            mo10502a(false, false);
        }
    }

    public void onTabReselected(C0416d dVar) {
        mo10479a();
    }

    public void onTabSelected(C0416d dVar) {
        this.viewPager.setCurrentItem(dVar.mo1814c(), true);
        if (dVar.mo1814c() == 0 && AppState.config.getBoolean("messaging_turned_on", true)) {
            TextView textView = (TextView) dVar.mo1811a();
            textView.setTextColor(getResources().getColor(R.color.gossip));
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    public void onTabUnselected(C0416d dVar) {
        if (dVar.mo1814c() == 0 && AppState.config.getBoolean("messaging_turned_on", true)) {
            TextView textView = (TextView) dVar.mo1811a();
            textView.setTextColor(Color.parseColor("#888888"));
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }
}
