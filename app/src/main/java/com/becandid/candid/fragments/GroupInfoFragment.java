package com.becandid.candid.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.support.p001v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.CreateGroupActivity;
import com.becandid.candid.models.UploadMediaResponse;
import com.becandid.candid.util.RoundedCornersTransformation;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;
import p012rx.schedulers.Schedulers;

public class GroupInfoFragment extends Fragment implements OnClickListener {

    /* renamed from: a */
    private RoundedCornersTransformation f6779a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public PhotoHelper f6780b;

    /* renamed from: c */
    private Unbinder f6781c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Bundle f6782d;
    @BindView(2131624618)
    View mChoosePhotoPlaceholder;
    @BindView(2131624617)
    EditText mGroupInfo;
    @BindView(2131624619)
    ImageView mGroupPhoto;
    @BindView(2131624621)
    Button mNextBtn;
    @BindView(2131624622)
    Button mSkipBtn;

    /* renamed from: a */
    public void mo10379a() {
        if (this.f6780b != null) {
            if (this.f6780b.f10753f != null && this.f6780b.f10749b != null && this.f6780b.f10748a != null) {
                this.mGroupPhoto.setImageBitmap((Bitmap) this.f6779a.mo10703a(this.f6780b.f10753f, this.mGroupPhoto.getWidth(), this.mGroupPhoto.getHeight()).mo14377d());
                this.f6780b.mo14337b();
                ((BaseActivity) getActivity()).addToSubscriptionList(RxBus.m14573a().mo14348a(C2627ap.class, (bjy<T>) new bjy<C2627ap>() {
                    /* renamed from: a */
                    public void onNext(C2627ap apVar) {
                        if (apVar != null && apVar.f10607a != null) {
                            RxBus.m14573a().mo14349a(new C2639ba(apVar.f10607a));
                        }
                    }

                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                    }
                }));
            } else if (this.f6780b.f10749b != null) {
                GossipApplication.f6095c.mo14650a(this.f6780b.f10749b.toString()).mo14598j().mo14567b((Transformation<Bitmap>[]) new Transformation[]{this.f6779a}).mo14554a(this.mGroupPhoto);
            }
        }
    }

    /* renamed from: a */
    public void mo10380a(PhotoHelper jbVar) {
        this.f6780b = jbVar;
    }

    public void choosePhoto() {
        if (this.f6780b == null) {
            this.f6780b = new PhotoHelper((BaseActivity) getActivity());
        }
        FragmentActivity activity = getActivity();
        if (activity instanceof CreateGroupActivity) {
            this.f6780b.choosePhoto(((CreateGroupActivity) activity).getmGroupName());
        } else {
            this.f6780b.choosePhoto();
        }
    }

    public void onClick(View view) {
        choosePhoto();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_group_info, viewGroup, false);
        this.f6781c = ButterKnife.bind((Object) this, inflate);
        this.f6782d = getArguments();
        this.mChoosePhotoPlaceholder.setOnClickListener(this);
        this.f6779a = new RoundedCornersTransformation(getContext(), (int) (100.0f * getResources().getDisplayMetrics().density), 0, CornerType.ALL);
        if (this.f6782d.containsKey("group_info")) {
            this.mGroupInfo.setText(this.f6782d.getString("group_info"));
            this.mGroupInfo.setSelection(this.f6782d.getString("group_info").length());
            this.mNextBtn.setEnabled(true);
        }
        this.mGroupInfo.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) {
                    GroupInfoFragment.this.mNextBtn.setEnabled(false);
                } else {
                    GroupInfoFragment.this.mNextBtn.setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        mo10380a(((BaseActivity) getActivity()).getPhotoHelper());
        if (this.f6782d.containsKey("source_url")) {
            this.f6780b.choosePhoto(this.f6782d.getString("source_url"), 0, 0);
            this.mNextBtn.setEnabled(true);
        }
        this.mSkipBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FragmentActivity activity = GroupInfoFragment.this.getActivity();
                C2699jj.m14610a((Context) activity);
                if (activity instanceof CreateGroupActivity) {
                    ((CreateGroupActivity) activity).setGroupPhotoSkipped(true);
                    ((CreateGroupActivity) activity).setGroupSourceUrl(null);
                }
                GroupTagsFragment groupTagsFragment = new GroupTagsFragment();
                groupTagsFragment.setArguments(GroupInfoFragment.this.f6782d);
                FragmentTransaction beginTransaction = GroupInfoFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
                beginTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
                beginTransaction.replace(R.id.placeholder, groupTagsFragment);
                beginTransaction.addToBackStack(null);
                beginTransaction.commit();
            }
        });
        this.mNextBtn.setOnClickListener(new OnClickListener() {
            public void onClick(final View view) {
                final FragmentActivity activity = GroupInfoFragment.this.getActivity();
                C2699jj.m14610a((Context) activity);
                if (activity instanceof CreateGroupActivity) {
                    ((CreateGroupActivity) activity).setGroupAbout(GroupInfoFragment.this.mGroupInfo.getText().toString());
                }
                GroupTagsFragment groupTagsFragment = new GroupTagsFragment();
                groupTagsFragment.setArguments(GroupInfoFragment.this.f6782d);
                FragmentTransaction beginTransaction = GroupInfoFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
                beginTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
                beginTransaction.replace(R.id.placeholder, groupTagsFragment);
                beginTransaction.addToBackStack(null);
                beginTransaction.commit();
                if (GroupInfoFragment.this.f6780b != null && GroupInfoFragment.this.f6780b.f10749b != null && GroupInfoFragment.this.f6780b.f10748a != null) {
                    ApiService.m14297a().mo14129c(GroupInfoFragment.this.f6780b.f10749b, GroupInfoFragment.this.f6780b.f10748a).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<UploadMediaResponse>() {
                        /* renamed from: a */
                        public void onNext(UploadMediaResponse uploadMediaResponse) {
                            if (uploadMediaResponse != null) {
                                ((CreateGroupActivity) activity).setGroupSourceUrl(uploadMediaResponse.full_url);
                            }
                        }

                        public void onCompleted() {
                        }

                        public void onError(Throwable th) {
                            Crashlytics.m16437a(th);
                            Toast.makeText(view.getContext(), GroupInfoFragment.this.getString(R.string.unable_to_upload_image), 0).show();
                        }
                    });
                }
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f6781c.unbind();
    }
}
