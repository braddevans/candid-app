package com.becandid.candid.fragments.main_tabs;

import android.support.p001v4.widget.SwipeRefreshLayout;
import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.main_tabs.ActivityTabFragment;

public class ActivityTabFragment$$ViewBinder<T extends ActivityTabFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.main_tabs.ActivityTabFragment$$ViewBinder$a */
    /* compiled from: ActivityTabFragment$$ViewBinder */
    public static class C1651a<T extends ActivityTabFragment> implements Unbinder {

        /* renamed from: a */
        private T f6921a;

        protected C1651a(T t) {
            this.f6921a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10487a(T t) {
            t.mSwipeRefresh = null;
            t.mRecyclerView = null;
            t.mEmptyView = null;
            t.mSettingView = null;
        }

        public final void unbind() {
            if (this.f6921a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10487a(this.f6921a);
            this.f6921a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1651a a = mo10486a(t);
        t.mSwipeRefresh = (SwipeRefreshLayout) finder.castView((View) finder.findRequiredView(obj, R.id.activity_refresh, "field 'mSwipeRefresh'"), R.id.activity_refresh, "field 'mSwipeRefresh'");
        t.mRecyclerView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.activity_recycler, "field 'mRecyclerView'"), R.id.activity_recycler, "field 'mRecyclerView'");
        t.mEmptyView = (View) finder.findRequiredView(obj, R.id.activity_empty, "field 'mEmptyView'");
        t.mSettingView = (View) finder.findRequiredView(obj, R.id.activity_settings, "field 'mSettingView'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1651a<T> mo10486a(T t) {
        return new C1651a<>(t);
    }
}
