package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingVerifyFragment;

public class OnboardingVerifyFragment$$ViewBinder<T extends OnboardingVerifyFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingVerifyFragment$$ViewBinder$a */
    /* compiled from: OnboardingVerifyFragment$$ViewBinder */
    public static class C1765a<T extends OnboardingVerifyFragment> implements Unbinder {

        /* renamed from: a */
        private T f7142a;

        protected C1765a(T t) {
            this.f7142a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10677a(T t) {
            t.resend = null;
            t.verify = null;
            t.submit = null;
            t.verifySubheader = null;
            t.verifyHeader = null;
            t.spinny = null;
        }

        public final void unbind() {
            if (this.f7142a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10677a(this.f7142a);
            this.f7142a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1765a a = mo10676a(t);
        t.resend = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.verify_resend, "field 'resend'"), R.id.verify_resend, "field 'resend'");
        t.verify = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.verify_input_text, "field 'verify'"), R.id.verify_input_text, "field 'verify'");
        t.submit = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.verify_input_button, "field 'submit'"), R.id.verify_input_button, "field 'submit'");
        t.verifySubheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.verify_subheader, "field 'verifySubheader'"), R.id.verify_subheader, "field 'verifySubheader'");
        t.verifyHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.verify_header, "field 'verifyHeader'"), R.id.verify_header, "field 'verifyHeader'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.verify_spinny, "field 'spinny'"), R.id.verify_spinny, "field 'spinny'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1765a<T> mo10676a(T t) {
        return new C1765a<>(t);
    }
}
