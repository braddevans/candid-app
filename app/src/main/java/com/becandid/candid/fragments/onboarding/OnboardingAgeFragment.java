package com.becandid.candid.fragments.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.activities.VerifyAgeActivity;
import com.becandid.candid.activities.WebViewActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.EmptySubscriber;
import java.util.HashMap;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class OnboardingAgeFragment extends Fragment {

    /* renamed from: a */
    private Unbinder f7002a;
    @BindView(2131624853)
    TextView ageHeader;
    @BindView(2131624860)
    Button ageOver;
    @BindView(2131624854)
    TextView ageSubheader;
    @BindView(2131624858)
    Button ageUnder;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f7003b;
    @BindView(2131624861)
    TextView privacy;
    @BindView(2131624852)
    TextView skip;
    @BindView(2131624862)
    FrameLayout spinny;

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9582b() {
        ApiService.m14297a().mo14168n("onboarding/age/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        HashMap hashMap = new HashMap();
        hashMap.put("age", AppState.age);
        ApiService.m14297a().mo14110a((Map<String, String>) hashMap).mo9256b(Schedulers.newThread()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
    }

    /* renamed from: a */
    public void mo10553a() {
        this.ageHeader.setText(AppState.config.getString("age_prompt_title", "Approximately, How Old Are You?"));
        this.ageSubheader.setText(AppState.config.getString("age_prompt_desc", "This information will help us recommend the right groups for you"));
        this.privacy.setText(AppState.config.getString("more_info", getString(R.string.privacy)));
        this.ageOver.setText(AppState.config.getString("age_prompt_over_18", "Over 18"));
        this.ageUnder.setText(AppState.config.getString("age_prompt_under_18", "Under 18"));
    }

    /* renamed from: a */
    public void mo10554a(String str) {
        ApiService.m14297a().mo14168n("onboarding/age/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        FragmentActivity activity = getActivity();
        if (activity instanceof OnboardingActivity) {
            ((OnboardingActivity) activity).switchFragment(str);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f7003b = arguments.getBoolean("existing_user", false);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_age, viewGroup, false);
        this.f7002a = ButterKnife.bind((Object) this, inflate);
        mo10553a();
        if (AppState.config.getInt("show_age_skip_android", 0) == 1) {
            this.skip.setVisibility(0);
            this.skip.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    ApiService.m14297a().mo14168n("onboarding/age/skip").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                    OnboardingAgeFragment.this.mo10554a("age");
                }
            });
        } else {
            this.skip.setVisibility(8);
        }
        if (this.f7003b) {
            this.privacy.setVisibility(8);
            this.skip.setVisibility(8);
        } else {
            this.privacy.setVisibility(0);
            this.privacy.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(OnboardingAgeFragment.this.getContext(), WebViewActivity.class);
                    intent.putExtra("title", "Why Can I Trust Candid?");
                    intent.putExtra("url", GossipApplication.f6096d + "content/whysafe");
                    OnboardingAgeFragment.this.getContext().startActivity(intent);
                }
            });
        }
        this.ageOver.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AppState.age = "18_plus";
                if (OnboardingAgeFragment.this.f7003b) {
                    OnboardingAgeFragment.this.m9582b();
                    ((VerifyAgeActivity) OnboardingAgeFragment.this.getActivity()).onActivityResult();
                } else {
                    OnboardingAgeFragment.this.mo10554a("age");
                }
                AppState.saveState(GossipApplication.m9012a());
            }
        });
        this.ageUnder.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AppState.age = "under_18";
                if (OnboardingAgeFragment.this.f7003b) {
                    OnboardingAgeFragment.this.m9582b();
                    ((VerifyAgeActivity) OnboardingAgeFragment.this.getActivity()).onActivityResult();
                } else {
                    OnboardingAgeFragment.this.mo10554a("age");
                }
                AppState.saveState(GossipApplication.m9012a());
            }
        });
        return inflate;
    }
}
