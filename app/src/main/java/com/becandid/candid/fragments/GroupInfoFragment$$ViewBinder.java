package com.becandid.candid.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.GroupInfoFragment;

public class GroupInfoFragment$$ViewBinder<T extends GroupInfoFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.GroupInfoFragment$$ViewBinder$a */
    /* compiled from: GroupInfoFragment$$ViewBinder */
    public static class C1592a<T extends GroupInfoFragment> implements Unbinder {

        /* renamed from: a */
        private T f6790a;

        protected C1592a(T t) {
            this.f6790a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10392a(T t) {
            t.mGroupPhoto = null;
            t.mChoosePhotoPlaceholder = null;
            t.mSkipBtn = null;
            t.mGroupInfo = null;
            t.mNextBtn = null;
        }

        public final void unbind() {
            if (this.f6790a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10392a(this.f6790a);
            this.f6790a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1592a a = mo10391a(t);
        t.mGroupPhoto = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.ivGroupPhoto, "field 'mGroupPhoto'"), R.id.ivGroupPhoto, "field 'mGroupPhoto'");
        t.mChoosePhotoPlaceholder = (View) finder.findRequiredView(obj, R.id.choosePhotoPlaceholder, "field 'mChoosePhotoPlaceholder'");
        t.mSkipBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.skipBtn, "field 'mSkipBtn'"), R.id.skipBtn, "field 'mSkipBtn'");
        t.mGroupInfo = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.group_info, "field 'mGroupInfo'"), R.id.group_info, "field 'mGroupInfo'");
        t.mNextBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.nextBtn, "field 'mNextBtn'"), R.id.nextBtn, "field 'mNextBtn'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1592a<T> mo10391a(T t) {
        return new C1592a<>(t);
    }
}
