package com.becandid.candid.fragments.onboarding;

import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.NetworkData;
import java.util.HashMap;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class OnboardingVerifyFragment extends Fragment {

    /* renamed from: a */
    String f7132a;

    /* renamed from: b */
    private Unbinder f7133b;
    @BindView(2131625008)
    TextView resend;
    @BindView(2131625009)
    FrameLayout spinny;
    @BindView(2131625007)
    Button submit;
    @BindView(2131625006)
    EditText verify;
    @BindView(2131625003)
    TextView verifyHeader;
    @BindView(2131625004)
    TextView verifySubheader;

    /* renamed from: c */
    private void m9669c() {
        this.verifyHeader.setText(AppState.config.getString("code_header", getString(R.string.verify_header)));
        this.verifySubheader.setText(AppState.config.getString("code_subheader", getString(R.string.verify_subheader)));
        this.verify.setHint(AppState.config.getString("code_input", getString(R.string.verify_input_hint)));
        this.resend.setText(AppState.config.getString("resend_code_button", getString(R.string.verify_resend)));
    }

    /* renamed from: a */
    public void mo10661a() {
        this.submit.setClickable(false);
        this.spinny.setVisibility(0);
        ApiService.m14297a().mo14104a(this.verify.getText().toString(), AppState.contactsInfo.phone_number).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                OnboardingVerifyFragment.this.spinny.setVisibility(8);
                if (networkData != null && networkData.success) {
                    if (AppState.account != null) {
                        AppState.account.have_phone_number = true;
                    }
                    ((BaseActivity) OnboardingVerifyFragment.this.getActivity()).closeKeyboard();
                    AppState.contactsInfo.otpCode = OnboardingVerifyFragment.this.verify.getText().toString();
                    if (OnboardingVerifyFragment.this.f7132a != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString("second_fragment", OnboardingVerifyFragment.this.f7132a);
                        ((OnboardingActivity) OnboardingVerifyFragment.this.getActivity()).switchFragment("verify", "contacts", bundle);
                        return;
                    }
                    OnboardingVerifyFragment.this.mo10663a("verify", "contacts");
                } else if (networkData != null && !networkData.success) {
                    Toast.makeText(OnboardingVerifyFragment.this.getActivity(), "That does not match the expected code", 1).show();
                    OnboardingVerifyFragment.this.submit.setClickable(true);
                    OnboardingVerifyFragment.this.spinny.setVisibility(8);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Toast.makeText(OnboardingVerifyFragment.this.getActivity(), "Unable to verify your code, please try again", 1).show();
                Crashlytics.m16437a(th);
                Log.d("VerifyPhone", th.toString());
                OnboardingVerifyFragment.this.spinny.setVisibility(8);
                OnboardingVerifyFragment.this.submit.setClickable(true);
            }
        });
    }

    /* renamed from: a */
    public void mo10662a(final View view, final boolean z, int i) {
        AlphaAnimation alphaAnimation = z ? new AlphaAnimation(0.0f, 1.0f) : new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setFillEnabled(true);
        if (i != -1) {
            alphaAnimation.setStartOffset((long) i);
        }
        alphaAnimation.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (!z) {
                    view.setClickable(false);
                    view.setVisibility(8);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                if (z) {
                    view.setClickable(true);
                    view.setVisibility(0);
                }
            }
        });
        view.startAnimation(alphaAnimation);
    }

    /* renamed from: a */
    public void mo10663a(String str, String str2) {
        FragmentActivity activity = getActivity();
        if (activity instanceof OnboardingActivity) {
            ((OnboardingActivity) activity).switchFragment(str, str2, null);
        }
    }

    /* renamed from: b */
    public void mo10664b() {
        HashMap hashMap = new HashMap();
        hashMap.put("otp_code", this.verify.getText().toString());
        hashMap.put("phone_number", AppState.contactsInfo.phone_number);
        this.submit.setClickable(false);
        this.spinny.setVisibility(0);
        ApiService.m14297a().mo14124b((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            /* renamed from: a */
            public void onNext(ays ays) {
                if (ays != null && ays.mo8410b("success") != null && ays.mo8410b("success").mo8391h()) {
                    AppState.account.have_phone_number = true;
                    if (OnboardingVerifyFragment.this.f7132a != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString("second_fragment", OnboardingVerifyFragment.this.f7132a);
                        ((OnboardingActivity) OnboardingVerifyFragment.this.getActivity()).switchFragment("verify", "contacts", bundle);
                    } else {
                        OnboardingVerifyFragment.this.mo10663a("verify", "contacts");
                    }
                    if (AppState.account != null) {
                        Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Verify Phone Number").mo16461a("success", "true")).mo16461a("logged_in", "true"));
                    } else {
                        Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Verify Phone Number").mo16461a("success", "true")).mo16461a("logged_in", "false"));
                    }
                } else if (ays != null && ays.mo8410b("success") != null && !ays.mo8410b("success").mo8391h()) {
                    Toast.makeText(OnboardingVerifyFragment.this.getActivity(), "That does not match the expected code", 1).show();
                    OnboardingVerifyFragment.this.submit.setClickable(true);
                    OnboardingVerifyFragment.this.spinny.setVisibility(8);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                if (AppState.account != null) {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Verify Phone Number").mo16461a("success", "false")).mo16461a("logged_in", "true"));
                } else {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Verify Phone Number").mo16461a("success", "false")).mo16461a("logged_in", "false"));
                }
                Crashlytics.m16437a(th);
                OnboardingVerifyFragment.this.submit.setClickable(true);
                OnboardingVerifyFragment.this.spinny.setVisibility(8);
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f7132a = arguments.getString("second_fragment");
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_verify, viewGroup, false);
        this.f7133b = ButterKnife.bind((Object) this, inflate);
        ((BaseActivity) getActivity()).enableKeyboardEvents(inflate);
        m9669c();
        Bundle arguments = getArguments();
        mo10662a(this.resend, true, 5000);
        this.resend.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (OnboardingVerifyFragment.this.f7132a != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("second_fragment", OnboardingVerifyFragment.this.f7132a);
                    ((OnboardingActivity) OnboardingVerifyFragment.this.getActivity()).switchFragment("verify", "phone", bundle);
                    return;
                }
                OnboardingVerifyFragment.this.mo10663a("verify", "phone");
            }
        });
        this.verify.requestFocus();
        ((BaseActivity) getActivity()).openKeyboard();
        this.verify.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.length() >= 4) {
                    OnboardingVerifyFragment.this.submit.setEnabled(true);
                } else {
                    OnboardingVerifyFragment.this.submit.setEnabled(false);
                }
            }
        });
        this.submit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (AppState.account != null) {
                    OnboardingVerifyFragment.this.mo10664b();
                } else {
                    OnboardingVerifyFragment.this.mo10661a();
                }
            }
        });
        return inflate;
    }
}
