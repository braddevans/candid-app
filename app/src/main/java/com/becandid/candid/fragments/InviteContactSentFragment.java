package com.becandid.candid.fragments;

import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity;

public class InviteContactSentFragment extends Fragment {

    /* renamed from: a */
    private Unbinder f6852a;

    /* renamed from: b */
    private Bundle f6853b;

    @OnClick({2131624659})
    public void onClose() {
        getActivity().finish();
    }

    @OnClick({2131624634})
    public void onContactClose() {
        getActivity().finish();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_invite_contact_sent, viewGroup, false);
        this.f6852a = ButterKnife.bind((Object) this, inflate);
        this.f6853b = getArguments();
        if (getActivity() instanceof InviteContactsActivity) {
            ((InviteContactsActivity) getActivity()).sendContactsToServer();
        }
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f6852a.unbind();
    }
}
