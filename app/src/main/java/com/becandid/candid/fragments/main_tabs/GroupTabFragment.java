package com.becandid.candid.fragments.main_tabs;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.C0411a;
import android.support.design.widget.TabLayout.C0416d;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.GroupDetailsActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.views.GroupStackView;
import com.becandid.candid.views.GroupStackView.C1822c;
import p012rx.schedulers.Schedulers;

public class GroupTabFragment extends BaseMainTabFragment implements C1822c {

    /* renamed from: d */
    private long f6931d = 0;
    @BindView(2131624715)
    View discoverEmpty;

    /* renamed from: e */
    private String f6932e;
    @BindView(2131624711)
    View forYouEmpty;
    @BindView(2131624717)
    GroupStackView groupStackView;
    @BindView(2131624716)
    View loading;
    @BindView(2131624710)
    TabLayout tabBar;

    /* renamed from: a */
    private bjy m9502a(final boolean z, final boolean z2) {
        return new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                if (GroupTabFragment.this.tabBar != null) {
                    if (z2 != (GroupTabFragment.this.tabBar.getSelectedTabPosition() == 0)) {
                        return;
                    }
                }
                GroupTabFragment.this.loading.setVisibility(8);
                if (networkData != null && networkData.groups != null) {
                    if (networkData.has_more) {
                        GroupTabFragment.this.groupStackView.setLoadMoreGroups(true);
                    } else {
                        GroupTabFragment.this.groupStackView.setLoadMoreGroups(false);
                    }
                    if ((!z && GroupTabFragment.this.groupStackView.mo10819b() != 0) || networkData.groups.size() != 0) {
                        GroupTabFragment.this.groupStackView.setVisibility(0);
                    } else if (z2) {
                        GroupTabFragment.this.forYouEmpty.setVisibility(0);
                    } else {
                        GroupTabFragment.this.discoverEmpty.setVisibility(0);
                    }
                    if (GroupTabFragment.this.groupStackView.mo10819b() == 0) {
                        GroupTabFragment.this.groupStackView.setGroups(networkData.groups);
                    } else {
                        GroupTabFragment.this.groupStackView.mo10817a(networkData.groups);
                    }
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                if (GroupTabFragment.this.loading != null) {
                    GroupTabFragment.this.loading.setVisibility(8);
                }
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9504a(boolean z) {
        boolean z2 = false;
        if (z && this.groupStackView != null) {
            this.groupStackView.setVisibility(8);
            this.forYouEmpty.setVisibility(8);
            this.discoverEmpty.setVisibility(8);
            this.loading.setVisibility(0);
        }
        if (this.tabBar.getSelectedTabPosition() == 0) {
            z2 = true;
        }
        ApiService.m14297a().mo14101a(AppState.fbInfo, z2, this.f6932e).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9502a(z, z2));
    }

    /* renamed from: d */
    private void m9505d() {
        int i = 2;
        if (AppState.config != null && AppState.config.has("groups_load_more_count")) {
            i = AppState.config.getInt("groups_load_more_count");
        }
        if (this.groupStackView.mo10818a() && this.groupStackView.mo10819b() <= i) {
            ApiService.m14297a().mo14144f().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9502a(true, this.tabBar.getSelectedTabPosition() == 0));
        }
    }

    /* renamed from: a */
    public void mo10479a() {
        super.mo10479a();
        m9504a(true);
        this.f10276c = false;
    }

    /* renamed from: a */
    public void mo10496a(String str) {
        this.f6932e = str;
    }

    /* renamed from: b */
    public void mo10480b() {
    }

    /* renamed from: c */
    public void mo10481c() {
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.groups_tab, viewGroup, false);
        this.f10275b = ButterKnife.bind((Object) this, inflate);
        this.f10274a = getContext();
        this.groupStackView.setListener(this);
        this.tabBar.setSelectedTabIndicatorColor(getResources().getColor(R.color.gossip));
        this.tabBar.setTabTextColors(Color.parseColor("#888888"), getResources().getColor(R.color.gossip));
        this.tabBar.mo1766a(this.tabBar.mo1764a().mo1810a((CharSequence) "For You"));
        this.tabBar.mo1766a(this.tabBar.mo1764a().mo1810a((CharSequence) "Discover"));
        this.tabBar.setOnTabSelectedListener(new C0411a() {
            public void onTabReselected(C0416d dVar) {
                GroupTabFragment.this.mo10479a();
            }

            public void onTabSelected(C0416d dVar) {
                GroupTabFragment.this.m9504a(true);
            }

            public void onTabUnselected(C0416d dVar) {
            }
        });
        m9504a(true);
        return inflate;
    }

    public void onEmptyGroups() {
        this.groupStackView.setVisibility(8);
        if (this.tabBar.getSelectedTabPosition() == 0) {
            this.forYouEmpty.setVisibility(0);
        } else {
            this.discoverEmpty.setVisibility(0);
        }
    }

    public void onJoinGroup(Group group) {
        m9505d();
    }

    public void onResume() {
        super.onResume();
    }

    public void onSkipGroup(Group group) {
        m9505d();
    }

    public void onTouchEvent(Group group) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.f6931d < 250) {
            Intent intent = new Intent(this.f10274a, GroupDetailsActivity.class);
            intent.putExtra("group_id", group.group_id);
            this.f10274a.startActivity(intent);
            return;
        }
        this.f6931d = currentTimeMillis;
    }
}
