package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingAgeFragment;

public class OnboardingAgeFragment$$ViewBinder<T extends OnboardingAgeFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingAgeFragment$$ViewBinder$a */
    /* compiled from: OnboardingAgeFragment$$ViewBinder */
    public static class C1696a<T extends OnboardingAgeFragment> implements Unbinder {

        /* renamed from: a */
        private T f7008a;

        protected C1696a(T t) {
            this.f7008a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10561a(T t) {
            t.privacy = null;
            t.skip = null;
            t.ageOver = null;
            t.ageUnder = null;
            t.spinny = null;
            t.ageSubheader = null;
            t.ageHeader = null;
        }

        public final void unbind() {
            if (this.f7008a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10561a(this.f7008a);
            this.f7008a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1696a a = mo10560a(t);
        t.privacy = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.age_privacy, "field 'privacy'"), R.id.age_privacy, "field 'privacy'");
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.age_skip, "field 'skip'"), R.id.age_skip, "field 'skip'");
        t.ageOver = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.age_over, "field 'ageOver'"), R.id.age_over, "field 'ageOver'");
        t.ageUnder = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.age_under, "field 'ageUnder'"), R.id.age_under, "field 'ageUnder'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.age_spinny, "field 'spinny'"), R.id.age_spinny, "field 'spinny'");
        t.ageSubheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.age_subheader, "field 'ageSubheader'"), R.id.age_subheader, "field 'ageSubheader'");
        t.ageHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.age_header, "field 'ageHeader'"), R.id.age_header, "field 'ageHeader'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1696a<T> mo10560a(T t) {
        return new C1696a<>(t);
    }
}
