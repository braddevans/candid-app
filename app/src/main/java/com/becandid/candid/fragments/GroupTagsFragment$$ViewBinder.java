package com.becandid.candid.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.GroupTagsFragment;

public class GroupTagsFragment$$ViewBinder<T extends GroupTagsFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.GroupTagsFragment$$ViewBinder$a */
    /* compiled from: GroupTagsFragment$$ViewBinder */
    public static class C1603a<T extends GroupTagsFragment> implements Unbinder {

        /* renamed from: a */
        private T f6813a;

        protected C1603a(T t) {
            this.f6813a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10414a(T t) {
            t.mTagsView = null;
            t.mCreateGroupBtn = null;
            t.mProgressBar = null;
            t.mTagsPlaceholder = null;
        }

        public final void unbind() {
            if (this.f6813a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10414a(this.f6813a);
            this.f6813a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1603a a = mo10413a(t);
        t.mTagsView = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.tags_view, "field 'mTagsView'"), R.id.tags_view, "field 'mTagsView'");
        t.mCreateGroupBtn = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.create_group, "field 'mCreateGroupBtn'"), R.id.create_group, "field 'mCreateGroupBtn'");
        t.mProgressBar = (View) finder.findRequiredView(obj, R.id.progress_bar, "field 'mProgressBar'");
        t.mTagsPlaceholder = (View) finder.findRequiredView(obj, R.id.tags_placeholder, "field 'mTagsPlaceholder'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1603a<T> mo10413a(T t) {
        return new C1603a<>(t);
    }
}
