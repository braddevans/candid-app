package com.becandid.candid.fragments;

import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity;

public class InviteContactMethodFragment extends Fragment {

    /* renamed from: a */
    private Unbinder f6814a;

    /* renamed from: b */
    private Bundle f6815b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public boolean f6816c = false;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f6817d = false;
    @BindView(2131624639)
    ImageView mAnonCheckmark;
    @BindView(2131624638)
    View mAnonPlaceHolder;
    @BindView(2131624644)
    Button mContinueBtn;
    @BindView(2131624642)
    ImageView mKnownCheckmark;
    @BindView(2131624641)
    View mKnownPlaceHolder;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9409a() {
        if (this.f6817d || this.f6816c) {
            this.mContinueBtn.setEnabled(true);
        } else {
            this.mContinueBtn.setEnabled(false);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9411a(boolean z) {
        if (z) {
            this.mAnonPlaceHolder.setBackground(getResources().getDrawable(R.drawable.green_checkmark_bg));
            this.mAnonCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.green_circle_check));
            return;
        }
        this.mAnonPlaceHolder.setBackground(getResources().getDrawable(R.drawable.empty_checkmark_bg));
        this.mAnonCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.empty_checkmark));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9415b(boolean z) {
        if (z) {
            this.mKnownPlaceHolder.setBackground(getResources().getDrawable(R.drawable.green_checkmark_bg));
            this.mKnownCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.green_circle_check));
            return;
        }
        this.mKnownPlaceHolder.setBackground(getResources().getDrawable(R.drawable.empty_checkmark_bg));
        this.mKnownCheckmark.setImageDrawable(getResources().getDrawable(R.drawable.empty_checkmark));
    }

    @OnClick({2131624634})
    public void onContactClose() {
        getActivity().finish();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_invite_contact_method, viewGroup, false);
        this.f6814a = ButterKnife.bind((Object) this, inflate);
        this.f6815b = getArguments();
        this.mAnonPlaceHolder.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                boolean z = true;
                if (!InviteContactMethodFragment.this.f6816c) {
                    InviteContactMethodFragment.this.m9411a(true);
                    InviteContactMethodFragment.this.m9415b(false);
                    InviteContactMethodFragment.this.f6817d = false;
                } else {
                    InviteContactMethodFragment.this.m9411a(false);
                }
                InviteContactMethodFragment inviteContactMethodFragment = InviteContactMethodFragment.this;
                if (InviteContactMethodFragment.this.f6816c) {
                    z = false;
                }
                inviteContactMethodFragment.f6816c = z;
                InviteContactMethodFragment.this.m9409a();
            }
        });
        this.mKnownPlaceHolder.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                boolean z = true;
                if (!InviteContactMethodFragment.this.f6817d) {
                    InviteContactMethodFragment.this.m9411a(false);
                    InviteContactMethodFragment.this.m9415b(true);
                    InviteContactMethodFragment.this.f6816c = false;
                } else {
                    InviteContactMethodFragment.this.m9415b(false);
                }
                InviteContactMethodFragment inviteContactMethodFragment = InviteContactMethodFragment.this;
                if (InviteContactMethodFragment.this.f6817d) {
                    z = false;
                }
                inviteContactMethodFragment.f6817d = z;
                InviteContactMethodFragment.this.m9409a();
            }
        });
        this.mContinueBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (InviteContactMethodFragment.this.f6816c) {
                    InviteContactSentFragment inviteContactSentFragment = new InviteContactSentFragment();
                    if (InviteContactMethodFragment.this.getActivity() instanceof InviteContactsActivity) {
                        ((InviteContactsActivity) InviteContactMethodFragment.this.getActivity()).switchFragment(inviteContactSentFragment);
                    }
                } else if (InviteContactMethodFragment.this.f6817d) {
                    InviteContactNameFragment inviteContactNameFragment = new InviteContactNameFragment();
                    if (InviteContactMethodFragment.this.getActivity() instanceof InviteContactsActivity) {
                        ((InviteContactsActivity) InviteContactMethodFragment.this.getActivity()).switchFragment(inviteContactNameFragment);
                    }
                }
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f6814a.unbind();
    }
}
