package com.becandid.candid.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.p001v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity;
import com.becandid.candid.data.Post;
import com.becandid.candid.views.viewholders.ImagePostViewHolder;
import com.becandid.candid.views.viewholders.LinkPostViewHolder;
import com.becandid.candid.views.viewholders.TextPostViewHolder;
import com.becandid.candid.views.viewholders.VideoPostViewHolder;

public class InviteContactPeopleFragment extends Fragment {

    /* renamed from: a */
    int f6829a = -1;

    /* renamed from: b */
    int f6830b = -1;

    /* renamed from: c */
    int f6831c = -1;

    /* renamed from: d */
    int f6832d = -1;

    /* renamed from: e */
    Runnable f6833e = new Runnable() {
        public void run() {
            InviteContactPeopleFragment.this.m9425a();
        }
    };

    /* renamed from: f */
    private Unbinder f6834f;

    /* renamed from: g */
    private Bundle f6835g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Handler f6836h;
    @BindView(2131624655)
    FrameLayout mEntirePostSnippet;
    @BindView(2131624652)
    ImageView mPostPlaceholder;
    @BindView(2131624651)
    ImageView mPostPreview;
    @BindView(2131624650)
    FrameLayout mPostSnippet;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9425a() {
        this.mEntirePostSnippet.setDrawingCacheEnabled(true);
        this.mEntirePostSnippet.buildDrawingCache();
        Bitmap drawingCache = this.mEntirePostSnippet.getDrawingCache();
        if (drawingCache != null) {
            if (this.f6829a == -1 && this.f6830b == -1) {
                int height = this.mEntirePostSnippet.getHeight();
                int width = this.mEntirePostSnippet.getWidth();
                this.f6829a = this.mPostSnippet.getHeight();
                this.f6830b = (this.f6829a * width) / height;
            }
            if (this.f6829a > 0 && this.f6830b > 0) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(drawingCache, this.f6830b, this.f6829a, true));
                LayoutParams layoutParams = new LayoutParams(this.f6830b, this.f6829a);
                layoutParams.addRule(3, R.id.invite_header);
                layoutParams.addRule(2, R.id.invite_footer);
                layoutParams.addRule(14);
                this.mPostSnippet.setLayoutParams(layoutParams);
                this.mPostPreview.setImageDrawable(bitmapDrawable);
                this.mEntirePostSnippet.setVisibility(8);
            }
        }
    }

    /* renamed from: a */
    private void m9427a(String str, String str2) {
        Post post = (Post) new aym().mo8357a(str, Post.class);
        if (post == null) {
            Crashlytics.m16437a(new Throwable("post object is null in setup post preview"));
            getActivity().finish();
            return;
        }
        this.f6831c = post.group_id;
        if (getActivity() instanceof InviteContactsActivity) {
            ((InviteContactsActivity) getActivity()).setGroupId(this.f6831c);
        }
        String str3 = post.type;
        Context context = getContext();
        char c = 65535;
        switch (str3.hashCode()) {
            case 3321850:
                if (str3.equals("link")) {
                    c = 1;
                    break;
                }
                break;
            case 100313435:
                if (str3.equals("image")) {
                    c = 0;
                    break;
                }
                break;
            case 112202875:
                if (str3.equals("video")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                ImagePostViewHolder imagePostViewHolder = new ImagePostViewHolder(LayoutInflater.from(context).inflate(R.layout.feed_row_image, null, false), (Activity) getActivity(), true);
                if (post.localBitmapPath == null) {
                    post.localBitmapPath = str2;
                }
                ImagePostViewHolder.m9971a(imagePostViewHolder, post);
                this.mEntirePostSnippet.addView(imagePostViewHolder.itemView);
                break;
            case 1:
                LinkPostViewHolder linkPostViewHolder = new LinkPostViewHolder(LayoutInflater.from(context).inflate(R.layout.feed_row_link, null, false), getActivity());
                LinkPostViewHolder.m9981a(linkPostViewHolder, post);
                this.mEntirePostSnippet.addView(linkPostViewHolder.itemView);
                break;
            case 2:
                VideoPostViewHolder videoPostViewHolder = new VideoPostViewHolder(LayoutInflater.from(context).inflate(R.layout.feed_row_video, null, false), getActivity());
                VideoPostViewHolder.m10005a(videoPostViewHolder, post);
                this.mEntirePostSnippet.addView(videoPostViewHolder.itemView);
                break;
            default:
                TextPostViewHolder textPostViewHolder = new TextPostViewHolder(LayoutInflater.from(context).inflate(R.layout.feed_row_text, null, false), getActivity());
                TextPostViewHolder.m9995a(textPostViewHolder, post);
                this.mEntirePostSnippet.addView(textPostViewHolder.itemView);
                break;
        }
        this.mEntirePostSnippet.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                InviteContactPeopleFragment.this.mEntirePostSnippet.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                InviteContactPeopleFragment.this.mEntirePostSnippet.requestLayout();
                InviteContactPeopleFragment.this.f6836h.post(InviteContactPeopleFragment.this.f6833e);
            }
        });
    }

    @OnClick({2131624654})
    public void chooseContacts(View view) {
        ((InviteContactsActivity) getActivity()).chooseContacts(view);
    }

    @OnClick({2131624653})
    public void inviteAll(View view) {
        ((InviteContactsActivity) getActivity()).inviteAll(view);
    }

    @OnClick({2131624634})
    public void onContactClose() {
        getActivity().finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00bb, code lost:
        if (r1.equals("Post") != false) goto L_0x006f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View onCreateView(android.view.LayoutInflater r9, android.view.ViewGroup r10, android.os.Bundle r11) {
        /*
            r8 = this;
            r4 = 0
            r5 = -1
            r6 = 2130968707(0x7f040083, float:1.7546075E38)
            android.view.View r3 = r9.inflate(r6, r10, r4)
            butterknife.Unbinder r6 = butterknife.ButterKnife.bind(r8, r3)
            r8.f6834f = r6
            android.os.Bundle r6 = r8.getArguments()
            r8.f6835g = r6
            android.os.Handler r6 = new android.os.Handler
            android.support.v4.app.FragmentActivity r7 = r8.getActivity()
            android.os.Looper r7 = r7.getMainLooper()
            r6.<init>(r7)
            r8.f6836h = r6
            android.os.Bundle r6 = r8.f6835g
            java.lang.String r7 = "post"
            java.lang.String r0 = r6.getString(r7)
            android.os.Bundle r6 = r8.f6835g
            java.lang.String r7 = "link_image_path"
            java.lang.String r2 = r6.getString(r7)
            if (r0 == 0) goto L_0x0058
            r8.m9427a(r0, r2)
        L_0x0039:
            r4 = 2131624653(0x7f0e02cd, float:1.8876492E38)
            android.view.View r4 = r3.findViewById(r4)
            com.becandid.candid.fragments.InviteContactPeopleFragment$2 r5 = new com.becandid.candid.fragments.InviteContactPeopleFragment$2
            r5.<init>()
            r4.setOnClickListener(r5)
            r4 = 2131624654(0x7f0e02ce, float:1.8876494E38)
            android.view.View r4 = r3.findViewById(r4)
            com.becandid.candid.fragments.InviteContactPeopleFragment$3 r5 = new com.becandid.candid.fragments.InviteContactPeopleFragment$3
            r5.<init>()
            r4.setOnClickListener(r5)
            return r3
        L_0x0058:
            android.widget.ImageView r6 = r8.mPostPlaceholder
            r6.setVisibility(r4)
            android.os.Bundle r6 = r8.f6835g
            java.lang.String r7 = "invite_type"
            java.lang.String r1 = r6.getString(r7)
            if (r1 == 0) goto L_0x007a
            int r6 = r1.hashCode()
            switch(r6) {
                case 2493632: goto L_0x00b5;
                case 69076575: goto L_0x00be;
                default: goto L_0x006e;
            }
        L_0x006e:
            r4 = r5
        L_0x006f:
            switch(r4) {
                case 0: goto L_0x00c8;
                case 1: goto L_0x00d1;
                default: goto L_0x0072;
            }
        L_0x0072:
            android.widget.ImageView r4 = r8.mPostPlaceholder
            r6 = 2130838057(0x7f020229, float:1.7281086E38)
            r4.setImageResource(r6)
        L_0x007a:
            android.os.Bundle r4 = r8.f6835g
            java.lang.String r6 = "groupId"
            int r4 = r4.getInt(r6, r5)
            r8.f6831c = r4
            android.support.v4.app.FragmentActivity r4 = r8.getActivity()
            boolean r4 = r4 instanceof com.becandid.candid.activities.InviteContactsActivity
            if (r4 == 0) goto L_0x0097
            android.support.v4.app.FragmentActivity r4 = r8.getActivity()
            com.becandid.candid.activities.InviteContactsActivity r4 = (com.becandid.candid.activities.InviteContactsActivity) r4
            int r6 = r8.f6831c
            r4.setGroupId(r6)
        L_0x0097:
            android.os.Bundle r4 = r8.f6835g
            java.lang.String r6 = "upsellId"
            int r4 = r4.getInt(r6, r5)
            r8.f6832d = r4
            android.support.v4.app.FragmentActivity r4 = r8.getActivity()
            boolean r4 = r4 instanceof com.becandid.candid.activities.InviteContactsActivity
            if (r4 == 0) goto L_0x0039
            android.support.v4.app.FragmentActivity r4 = r8.getActivity()
            com.becandid.candid.activities.InviteContactsActivity r4 = (com.becandid.candid.activities.InviteContactsActivity) r4
            int r5 = r8.f6832d
            r4.setUpsellId(r5)
            goto L_0x0039
        L_0x00b5:
            java.lang.String r6 = "Post"
            boolean r6 = r1.equals(r6)
            if (r6 == 0) goto L_0x006e
            goto L_0x006f
        L_0x00be:
            java.lang.String r4 = "Group"
            boolean r4 = r1.equals(r4)
            if (r4 == 0) goto L_0x006e
            r4 = 1
            goto L_0x006f
        L_0x00c8:
            android.widget.ImageView r4 = r8.mPostPlaceholder
            r6 = 2130838056(0x7f020228, float:1.7281084E38)
            r4.setImageResource(r6)
            goto L_0x007a
        L_0x00d1:
            android.widget.ImageView r4 = r8.mPostPlaceholder
            r6 = 2130838058(0x7f02022a, float:1.7281088E38)
            r4.setImageResource(r6)
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.becandid.candid.fragments.InviteContactPeopleFragment.onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View");
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f6834f.unbind();
    }

    public void onPause() {
        super.onPause();
        this.f6836h.removeCallbacks(this.f6833e);
    }

    public void onResume() {
        super.onResume();
        if (this.mPostPlaceholder.getVisibility() != 0) {
            ViewTreeObserver viewTreeObserver = this.mEntirePostSnippet.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        if (InviteContactPeopleFragment.this.mEntirePostSnippet != null) {
                            InviteContactPeopleFragment.this.mEntirePostSnippet.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            InviteContactPeopleFragment.this.f6836h.postDelayed(InviteContactPeopleFragment.this.f6833e, 3000);
                        }
                    }
                });
            }
        }
    }
}
