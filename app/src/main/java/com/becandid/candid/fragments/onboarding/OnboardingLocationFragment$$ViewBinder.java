package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingLocationFragment;

public class OnboardingLocationFragment$$ViewBinder<T extends OnboardingLocationFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingLocationFragment$$ViewBinder$a */
    /* compiled from: OnboardingLocationFragment$$ViewBinder */
    public static class C1741a<T extends OnboardingLocationFragment> implements Unbinder {

        /* renamed from: a */
        private T f7104a;

        protected C1741a(T t) {
            this.f7104a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10631a(T t) {
            t.privacy = null;
            t.skip = null;
            t.locationButton = null;
            t.spinny = null;
            t.locationSubheader = null;
            t.locationInfo = null;
            t.locationHeader = null;
        }

        public final void unbind() {
            if (this.f7104a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10631a(this.f7104a);
            this.f7104a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1741a a = mo10630a(t);
        t.privacy = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.location_privacy, "field 'privacy'"), R.id.location_privacy, "field 'privacy'");
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.location_skip, "field 'skip'"), R.id.location_skip, "field 'skip'");
        t.locationButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.location_button, "field 'locationButton'"), R.id.location_button, "field 'locationButton'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.location_spinny, "field 'spinny'"), R.id.location_spinny, "field 'spinny'");
        t.locationSubheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.location_subheader, "field 'locationSubheader'"), R.id.location_subheader, "field 'locationSubheader'");
        t.locationInfo = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.location_info, "field 'locationInfo'"), R.id.location_info, "field 'locationInfo'");
        t.locationHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.location_header, "field 'locationHeader'"), R.id.location_header, "field 'locationHeader'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1741a<T> mo10630a(T t) {
        return new C1741a<>(t);
    }
}
