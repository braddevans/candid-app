package com.becandid.candid.fragments.onboarding;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.CommunitySearchActivity;
import com.becandid.candid.activities.CommunitySettingsActivity;
import com.becandid.candid.activities.MainTabsActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.activities.WebViewActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.util.CropTransformation;
import com.becandid.candid.util.CropTransformation.CropType;
import com.becandid.candid.util.RoundedCornersTransformation;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;
import java.util.ArrayList;
import java.util.HashMap;
import p012rx.schedulers.Schedulers;

public class OnboardingCommunityFragment extends Fragment {

    /* renamed from: a */
    Handler f7009a;
    @BindView(2131624883)
    RelativeLayout addCollege;
    @BindView(2131624874)
    RelativeLayout addHighSchool;
    @BindView(2131624902)
    RelativeLayout addNone;

    /* renamed from: b */
    RelativeLayout f7010b;
    @BindView(2131624869)
    Button button;

    /* renamed from: c */
    RelativeLayout f7011c;
    @BindView(2131624884)
    RelativeLayout collegeDefault;
    @BindView(2131624888)
    ImageView collegeImgSelected;
    @BindView(2131624891)
    TextView collegeMembers;
    @BindView(2131624887)
    RelativeLayout collegeSelected;
    @BindView(2131624890)
    TextView collegeSelectedName;
    @BindView(2131624886)
    TextView collegeText;

    /* renamed from: d */
    RelativeLayout f7012d;

    /* renamed from: e */
    TextView f7013e;

    /* renamed from: f */
    ImageView f7014f;

    /* renamed from: g */
    TextView f7015g;

    /* renamed from: h */
    TextView f7016h;
    @BindView(2131624865)
    TextView header;
    @BindView(2131624875)
    RelativeLayout highSchoolDefault;
    @BindView(2131624879)
    ImageView highSchoolImgSelected;
    @BindView(2131624882)
    TextView highSchoolMembers;
    @BindView(2131624878)
    RelativeLayout highSchoolSelected;
    @BindView(2131624881)
    TextView highSchoolSelectedName;
    @BindView(2131624877)
    TextView highSchoolText;

    /* renamed from: i */
    private Unbinder f7017i;
    @BindView(2131624870)
    TextView info;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public String f7018j;

    /* renamed from: k */
    private String f7019k;

    /* renamed from: l */
    private String f7020l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public String f7021m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public String f7022n;
    @BindView(2131624903)
    ImageView noneCheck;
    @BindView(2131624904)
    TextView noneText;

    /* renamed from: o */
    private String f7023o;

    /* renamed from: p */
    private String f7024p;
    @BindView(2131624871)
    TextView privacy;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public String f7025q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public String f7026r;

    /* renamed from: s */
    private String f7027s;
    @BindView(2131624892)
    View secondRowDouble;
    @BindView(2131624905)
    View secondRowSingle;
    @BindView(2131624864)
    TextView skip;
    @BindView(2131624872)
    FrameLayout spinny;
    @BindView(2131624866)
    TextView subheader;

    /* renamed from: t */
    private String f7028t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public String f7029u;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public boolean f7030v;

    /* renamed from: w */
    private RoundedCornersTransformation f7031w;

    /* renamed from: c */
    private void m9592c() {
        this.header.setText(AppState.config.getString("community_header", getString(R.string.community_header)));
        this.subheader.setText(AppState.config.getString("community_subheader", getString(R.string.community_subheader)));
        this.info.setText(AppState.config.getString("community_info", getString(R.string.community_info)));
        this.privacy.setText(AppState.config.getString("more_info", getString(R.string.privacy)));
    }

    /* renamed from: a */
    public void mo10562a() {
        if (this.addHighSchool.isSelected() || this.addCollege.isSelected() || this.f7010b.isSelected() || this.addNone.isSelected()) {
            this.button.setEnabled(true);
        } else {
            this.button.setEnabled(false);
        }
    }

    /* renamed from: a */
    public void mo10563a(String str) {
        FragmentActivity activity = getActivity();
        if (activity instanceof OnboardingActivity) {
            ((OnboardingActivity) activity).switchFragment(str);
        }
    }

    /* renamed from: b */
    public void mo10564b() {
        if (AppState.groups == null) {
            AppState.groups = new ArrayList();
        }
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        Group group = new Group();
        if (this.addHighSchool.isSelected() && this.f7019k != null) {
            arrayList.add(this.f7019k);
            group.group_id = Integer.parseInt(this.f7019k);
            group.group_name = this.f7018j;
            group.community_membership = 1;
            group.group_type = "school";
            group.num_members = Integer.parseInt(this.f7020l);
            group.thumb_url = this.f7021m;
            AppState.groups.add(group);
        }
        if (this.addCollege.isSelected() && this.f7023o != null) {
            Group group2 = new Group();
            arrayList.add(this.f7023o);
            group2.group_id = Integer.parseInt(this.f7023o);
            group2.group_name = this.f7022n;
            group2.community_membership = 1;
            group2.group_type = "college";
            group2.num_members = Integer.parseInt(this.f7024p);
            group2.thumb_url = this.f7025q;
            AppState.groups.add(group2);
        }
        if (this.f7010b.isSelected() && this.f7027s != null) {
            Group group3 = new Group();
            arrayList.add(this.f7027s);
            group3.group_id = Integer.parseInt(this.f7027s);
            group3.group_name = this.f7026r;
            group3.community_membership = 1;
            group3.group_type = "company";
            group3.num_members = Integer.parseInt(this.f7028t);
            group3.thumb_url = this.f7029u;
            AppState.groups.add(group3);
        }
        hashMap.put("group_ids", bid.m8220a(arrayList.toArray(), ","));
        ApiService.m14297a().mo14179x(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        });
        AppState.setCommunity = 1;
        if (this.f7030v) {
            getActivity().finish();
        } else {
            mo10563a("community");
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            Bundle extras = intent.getExtras();
            switch (i) {
                case CommunitySettingsActivity.REQUEST_HIGHSCHOOL /*201*/:
                    if (extras.containsKey("queryString") && extras.containsKey("queryId") && extras.containsKey("num_members") && extras.containsKey("thumb_url")) {
                        this.f7018j = extras.getString("queryString");
                        this.f7019k = extras.getString("queryId");
                        this.f7020l = extras.getString("num_members");
                        this.f7021m = extras.getString("thumb_url");
                        this.addHighSchool.setSelected(true);
                        this.highSchoolDefault.setVisibility(8);
                        this.highSchoolSelected.setVisibility(0);
                        this.highSchoolMembers.setText(this.f7020l + " Members");
                        this.highSchoolImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                OnboardingCommunityFragment.this.highSchoolImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(OnboardingCommunityFragment.this.f7021m).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(OnboardingCommunityFragment.this.getContext(), OnboardingCommunityFragment.this.highSchoolImgSelected.getMeasuredWidth(), OnboardingCommunityFragment.this.highSchoolImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(OnboardingCommunityFragment.this.highSchoolImgSelected);
                            }
                        });
                        this.highSchoolSelectedName.setText(this.f7018j);
                        if (this.addNone.isSelected()) {
                            this.addNone.callOnClick();
                            break;
                        }
                    }
                    break;
                case CommunitySettingsActivity.REQUEST_COLLEGE /*202*/:
                    if (extras.containsKey("queryString") && extras.containsKey("queryId") && extras.containsKey("num_members") && extras.containsKey("thumb_url")) {
                        this.f7022n = extras.getString("queryString");
                        this.f7023o = extras.getString("queryId");
                        this.f7024p = extras.getString("num_members");
                        this.f7025q = extras.getString("thumb_url");
                        this.addCollege.setSelected(true);
                        this.collegeDefault.setVisibility(8);
                        this.collegeSelected.setVisibility(0);
                        this.collegeMembers.setText(this.f7024p + " Members");
                        this.collegeImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                OnboardingCommunityFragment.this.collegeImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(OnboardingCommunityFragment.this.f7025q).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(OnboardingCommunityFragment.this.getContext(), OnboardingCommunityFragment.this.collegeImgSelected.getMeasuredWidth(), OnboardingCommunityFragment.this.collegeImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(OnboardingCommunityFragment.this.collegeImgSelected);
                            }
                        });
                        this.collegeSelectedName.setText(this.f7022n);
                        if (this.addNone.isSelected()) {
                            this.addNone.callOnClick();
                            break;
                        }
                    }
                    break;
                case 203:
                    if (extras.containsKey("queryString") && extras.containsKey("queryId") && extras.containsKey("num_members") && extras.containsKey("thumb_url")) {
                        this.f7026r = extras.getString("queryString");
                        this.f7027s = extras.getString("queryId");
                        this.f7028t = extras.getString("num_members");
                        this.f7029u = extras.getString("thumb_url");
                        this.f7010b.setSelected(true);
                        this.f7011c.setVisibility(8);
                        this.f7012d.setVisibility(0);
                        this.f7013e.setText(this.f7028t + " Members");
                        this.f7014f.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                OnboardingCommunityFragment.this.f7014f.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(OnboardingCommunityFragment.this.f7029u).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(OnboardingCommunityFragment.this.getContext(), OnboardingCommunityFragment.this.f7014f.getMeasuredWidth(), OnboardingCommunityFragment.this.f7014f.getMeasuredHeight(), CropType.CENTER)}).mo14554a(OnboardingCommunityFragment.this.f7014f);
                            }
                        });
                        this.f7015g.setText(this.f7026r);
                        if (this.addNone.isSelected()) {
                            this.addNone.callOnClick();
                            break;
                        }
                    }
                    break;
            }
            mo10562a();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(MainTabsActivity.ONBOARDING_COMMUNITY_KEY)) {
            this.f7030v = true;
        }
        this.f7031w = new RoundedCornersTransformation(getContext(), (int) (10.0f * getContext().getResources().getDisplayMetrics().density), 0, CornerType.TOP);
        AppState.hasShownCommunityPopup = true;
        AppState.saveState(GossipApplication.m9012a());
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_community, viewGroup, false);
        this.f7017i = ButterKnife.bind((Object) this, inflate);
        this.f7009a = new Handler(getContext().getMainLooper());
        if (AppState.config.getInt("enable_community_does_not_apply_button", 0) == 1) {
            this.f7010b = (RelativeLayout) inflate.findViewById(R.id.community_add_work);
            this.f7011c = (RelativeLayout) inflate.findViewById(R.id.community_work_default);
            this.f7012d = (RelativeLayout) inflate.findViewById(R.id.community_work_selected);
            this.f7013e = (TextView) inflate.findViewById(R.id.community_work_num_members);
            this.f7014f = (ImageView) inflate.findViewById(R.id.community_work_img_selected);
            this.f7015g = (TextView) inflate.findViewById(R.id.community_work_name);
            this.f7016h = (TextView) inflate.findViewById(R.id.community_work_text);
            this.secondRowDouble.setVisibility(0);
            this.secondRowSingle.setVisibility(8);
        } else {
            this.f7010b = (RelativeLayout) inflate.findViewById(R.id.community_add_work_single);
            this.f7011c = (RelativeLayout) inflate.findViewById(R.id.community_work_default_single);
            this.f7012d = (RelativeLayout) inflate.findViewById(R.id.community_work_selected_single);
            this.f7013e = (TextView) inflate.findViewById(R.id.community_work_num_members_single);
            this.f7014f = (ImageView) inflate.findViewById(R.id.community_work_img_selected_single);
            this.f7015g = (TextView) inflate.findViewById(R.id.community_work_name_single);
            this.f7016h = (TextView) inflate.findViewById(R.id.community_work_text_single);
            this.secondRowDouble.setVisibility(8);
            this.secondRowSingle.setVisibility(0);
        }
        this.info.setVisibility(0);
        this.privacy.setVisibility(0);
        if (AppState.config.getString("community_high_school_button") != null) {
            this.highSchoolText.setText(AppState.config.getString("community_high_school_button"));
        }
        if (AppState.config.getString("community_college_button") != null) {
            this.collegeText.setText(AppState.config.getString("community_college_button"));
        }
        if (AppState.config.getString("community_company_button") != null) {
            this.f7016h.setText(AppState.config.getString("community_company_button"));
        }
        if ((getActivity() instanceof OnboardingActivity) && ((OnboardingActivity) getActivity()).communitySuggestions != null) {
            for (Group group : ((OnboardingActivity) getActivity()).communitySuggestions) {
                String str = group.group_type;
                char c = 65535;
                switch (str.hashCode()) {
                    case -907977868:
                        if (str.equals("school")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 949445015:
                        if (str.equals("college")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 950484093:
                        if (str.equals("company")) {
                            c = 2;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        this.f7018j = group.group_name;
                        this.f7020l = Integer.toString(group.num_members);
                        this.f7021m = group.thumb_url != null ? group.thumb_url : group.source_url;
                        this.f7019k = Integer.toString(group.group_id);
                        this.addHighSchool.setSelected(true);
                        this.highSchoolDefault.setVisibility(8);
                        this.highSchoolSelected.setVisibility(0);
                        this.highSchoolMembers.setText(this.f7020l + " Members");
                        this.highSchoolImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                OnboardingCommunityFragment.this.highSchoolImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(OnboardingCommunityFragment.this.f7021m).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(OnboardingCommunityFragment.this.getContext(), OnboardingCommunityFragment.this.highSchoolImgSelected.getMeasuredWidth(), OnboardingCommunityFragment.this.highSchoolImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(OnboardingCommunityFragment.this.highSchoolImgSelected);
                            }
                        });
                        this.highSchoolSelectedName.setText(this.f7018j);
                        break;
                    case 1:
                        this.f7022n = group.group_name;
                        this.f7024p = Integer.toString(group.num_members);
                        this.f7025q = group.thumb_url != null ? group.thumb_url : group.source_url;
                        this.f7023o = Integer.toString(group.group_id);
                        this.addCollege.setSelected(true);
                        this.collegeDefault.setVisibility(8);
                        this.collegeSelected.setVisibility(0);
                        this.collegeMembers.setText(this.f7024p + " Members");
                        this.collegeImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                OnboardingCommunityFragment.this.collegeImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(OnboardingCommunityFragment.this.f7025q).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(OnboardingCommunityFragment.this.getContext(), OnboardingCommunityFragment.this.collegeImgSelected.getMeasuredWidth(), OnboardingCommunityFragment.this.collegeImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(OnboardingCommunityFragment.this.collegeImgSelected);
                            }
                        });
                        this.collegeSelectedName.setText(this.f7022n);
                        break;
                    case 2:
                        this.f7026r = group.group_name;
                        this.f7028t = Integer.toString(group.num_members);
                        this.f7029u = group.thumb_url != null ? group.thumb_url : group.source_url;
                        this.f7027s = Integer.toString(group.group_id);
                        this.f7010b.setSelected(true);
                        this.f7011c.setVisibility(8);
                        this.f7012d.setVisibility(0);
                        this.f7013e.setText(this.f7028t + " Members");
                        this.f7014f.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                OnboardingCommunityFragment.this.f7014f.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(OnboardingCommunityFragment.this.f7029u).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(OnboardingCommunityFragment.this.getContext(), OnboardingCommunityFragment.this.f7014f.getMeasuredWidth(), OnboardingCommunityFragment.this.f7014f.getMeasuredHeight(), CropType.CENTER)}).mo14554a(OnboardingCommunityFragment.this.f7014f);
                            }
                        });
                        this.f7015g.setText(this.f7026r);
                        break;
                }
            }
            mo10562a();
        }
        m9592c();
        this.privacy.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(OnboardingCommunityFragment.this.getContext(), WebViewActivity.class);
                intent.putExtra("title", "Why Can I Trust Candid?");
                intent.putExtra("url", GossipApplication.f6096d + "content/whysafe");
                OnboardingCommunityFragment.this.getContext().startActivity(intent);
            }
        });
        if (AppState.config.getInt("android_enable_skip_onboarding_community", 1) == 1) {
            this.skip.setVisibility(0);
            this.skip.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    AppState.hasShownCommunityPopup = true;
                    AppState.saveState(GossipApplication.m9012a());
                    if (OnboardingCommunityFragment.this.f7030v) {
                        OnboardingCommunityFragment.this.getActivity().finish();
                    } else {
                        OnboardingCommunityFragment.this.mo10563a("community");
                    }
                }
            });
        } else {
            this.skip.setVisibility(8);
        }
        this.button.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OnboardingCommunityFragment.this.mo10564b();
            }
        });
        this.addHighSchool.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (OnboardingCommunityFragment.this.addHighSchool.isSelected()) {
                    OnboardingCommunityFragment.this.addHighSchool.setSelected(false);
                    OnboardingCommunityFragment.this.highSchoolDefault.setVisibility(0);
                    OnboardingCommunityFragment.this.highSchoolSelected.setVisibility(8);
                } else {
                    Intent intent = new Intent(OnboardingCommunityFragment.this.getActivity(), CommunitySearchActivity.class);
                    intent.putExtra("queryType", "school");
                    intent.putExtra("searchViewHint", AppState.config.getString("community_search_high_school_placeholder", "ex. Mill High School"));
                    if (OnboardingCommunityFragment.this.f7018j != null) {
                        intent.putExtra("queryString", OnboardingCommunityFragment.this.f7018j);
                    }
                    OnboardingCommunityFragment.this.startActivityForResult(intent, CommunitySettingsActivity.REQUEST_HIGHSCHOOL);
                }
                OnboardingCommunityFragment.this.mo10562a();
            }
        });
        this.addCollege.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (OnboardingCommunityFragment.this.addCollege.isSelected()) {
                    OnboardingCommunityFragment.this.addCollege.setSelected(false);
                    OnboardingCommunityFragment.this.collegeDefault.setVisibility(0);
                    OnboardingCommunityFragment.this.collegeSelected.setVisibility(8);
                } else {
                    Intent intent = new Intent(OnboardingCommunityFragment.this.getActivity(), CommunitySearchActivity.class);
                    intent.putExtra("queryType", "college");
                    intent.putExtra("searchViewHint", AppState.config.getString("community_search_college_placeholder", "ex. University of San Francisco"));
                    if (OnboardingCommunityFragment.this.f7022n != null) {
                        intent.putExtra("queryString", OnboardingCommunityFragment.this.f7022n);
                    }
                    OnboardingCommunityFragment.this.startActivityForResult(intent, CommunitySettingsActivity.REQUEST_COLLEGE);
                }
                OnboardingCommunityFragment.this.mo10562a();
            }
        });
        this.f7010b.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (OnboardingCommunityFragment.this.f7010b.isSelected()) {
                    OnboardingCommunityFragment.this.f7010b.setSelected(false);
                    OnboardingCommunityFragment.this.f7011c.setVisibility(0);
                    OnboardingCommunityFragment.this.f7012d.setVisibility(8);
                } else {
                    Intent intent = new Intent(OnboardingCommunityFragment.this.getActivity(), CommunitySearchActivity.class);
                    intent.putExtra("queryType", "company");
                    intent.putExtra("searchViewHint", AppState.config.getString("community_search_work_placeholder", "ex. MyLikes"));
                    if (OnboardingCommunityFragment.this.f7026r != null) {
                        intent.putExtra("queryString", OnboardingCommunityFragment.this.f7026r);
                    }
                    OnboardingCommunityFragment.this.startActivityForResult(intent, 203);
                }
                OnboardingCommunityFragment.this.mo10562a();
            }
        });
        this.addNone.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (OnboardingCommunityFragment.this.addNone.isSelected()) {
                    OnboardingCommunityFragment.this.addNone.setSelected(false);
                    OnboardingCommunityFragment.this.noneCheck.setVisibility(8);
                } else {
                    OnboardingCommunityFragment.this.addNone.setSelected(true);
                    OnboardingCommunityFragment.this.noneCheck.setVisibility(0);
                    if (OnboardingCommunityFragment.this.addHighSchool.isSelected()) {
                        OnboardingCommunityFragment.this.addHighSchool.callOnClick();
                    }
                    if (OnboardingCommunityFragment.this.addCollege.isSelected()) {
                        OnboardingCommunityFragment.this.addCollege.callOnClick();
                    }
                    if (OnboardingCommunityFragment.this.f7010b.isSelected()) {
                        OnboardingCommunityFragment.this.f7010b.callOnClick();
                    }
                }
                OnboardingCommunityFragment.this.mo10562a();
            }
        });
        return inflate;
    }
}
