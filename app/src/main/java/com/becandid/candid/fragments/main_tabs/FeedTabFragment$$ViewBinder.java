package com.becandid.candid.fragments.main_tabs;

import android.support.design.widget.TabLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.main_tabs.FeedTabFragment;
import com.becandid.candid.views.MainTabViewPager;
import com.venmo.view.TooltipView;

public class FeedTabFragment$$ViewBinder<T extends FeedTabFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.main_tabs.FeedTabFragment$$ViewBinder$a */
    /* compiled from: FeedTabFragment$$ViewBinder */
    public static class C1655a<T extends FeedTabFragment> implements Unbinder {

        /* renamed from: a */
        private T f6930a;

        protected C1655a(T t) {
            this.f6930a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10495a(T t) {
            t.mFeedTabs = null;
            t.mNewPostsText = null;
            t.viewPager = null;
            t.mFeedTab = null;
            t.mTooltipView = null;
        }

        public final void unbind() {
            if (this.f6930a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10495a(this.f6930a);
            this.f6930a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1655a a = mo10494a(t);
        t.mFeedTabs = (TabLayout) finder.castView((View) finder.findRequiredView(obj, R.id.feed_tabs, "field 'mFeedTabs'"), R.id.feed_tabs, "field 'mFeedTabs'");
        t.mNewPostsText = (View) finder.findRequiredView(obj, R.id.feed_new_posts, "field 'mNewPostsText'");
        t.viewPager = (MainTabViewPager) finder.castView((View) finder.findRequiredView(obj, R.id.feed_view_pager, "field 'viewPager'"), R.id.feed_view_pager, "field 'viewPager'");
        t.mFeedTab = (View) finder.findRequiredView(obj, R.id.feed_tab_header, "field 'mFeedTab'");
        t.mTooltipView = (TooltipView) finder.castView((View) finder.findRequiredView(obj, R.id.tooltip, "field 'mTooltipView'"), R.id.tooltip, "field 'mTooltipView'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1655a<T> mo10494a(T t) {
        return new C1655a<>(t);
    }
}
