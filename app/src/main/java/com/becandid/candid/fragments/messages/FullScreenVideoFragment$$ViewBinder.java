package com.becandid.candid.fragments.messages;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.VideoView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.messages.FullScreenVideoFragment;

public class FullScreenVideoFragment$$ViewBinder<T extends FullScreenVideoFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.messages.FullScreenVideoFragment$$ViewBinder$a */
    /* compiled from: FullScreenVideoFragment$$ViewBinder */
    public static class C1691a<T extends FullScreenVideoFragment> implements Unbinder {

        /* renamed from: a */
        private T f7001a;

        protected C1691a(T t) {
            this.f7001a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10552a(T t) {
            t.mVideoView = null;
            t.mVideoPlaceholder = null;
            t.mVideoProgressBar = null;
            t.mFullScreenExit = null;
        }

        public final void unbind() {
            if (this.f7001a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10552a(this.f7001a);
            this.f7001a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1691a a = mo10551a(t);
        t.mVideoView = (VideoView) finder.castView((View) finder.findRequiredView(obj, R.id.video_view, "field 'mVideoView'"), R.id.video_view, "field 'mVideoView'");
        t.mVideoPlaceholder = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.video_player_placeholder, "field 'mVideoPlaceholder'"), R.id.video_player_placeholder, "field 'mVideoPlaceholder'");
        t.mVideoProgressBar = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.video_player_progress_bar, "field 'mVideoProgressBar'"), R.id.video_player_progress_bar, "field 'mVideoProgressBar'");
        t.mFullScreenExit = (View) finder.findRequiredView(obj, R.id.fullscreen_exit, "field 'mFullScreenExit'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1691a<T> mo10551a(T t) {
        return new C1691a<>(t);
    }
}
