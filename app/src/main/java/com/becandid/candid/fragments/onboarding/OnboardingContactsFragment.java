package com.becandid.candid.fragments.onboarding;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p003v7.app.AlertDialog.Builder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class OnboardingContactsFragment extends Fragment {

    /* renamed from: a */
    final int f7066a = 1;

    /* renamed from: b */
    String f7067b;

    /* renamed from: c */
    private Unbinder f7068c;
    @BindView(2131624922)
    Button contactsButton;
    @BindView(2131624093)
    TextView contactsHeader;
    @BindView(2131624920)
    TextView contactsSubheader;
    @BindView(2131624921)
    TextView contactsSubheader2;
    @BindView(2131624919)
    TextView skip;
    @BindView(2131624097)
    FrameLayout spinny;

    /* renamed from: c */
    private void m9614c() {
        this.contactsHeader.setText(AppState.config.getString("contacts_header", getString(R.string.contacts_header)));
        this.contactsSubheader.setText(AppState.config.getString("contacts_subheader", getString(R.string.contacts_subheader)));
        this.contactsSubheader2.setText(AppState.config.getString("contacts_subheader_2", ""));
        if (this.contactsSubheader2.getText().equals("")) {
            this.contactsSubheader2.setVisibility(8);
        }
        this.contactsButton.setText(AppState.config.getString("contacts_button", getString(R.string.contacts_button)));
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m9615d() {
        ApiService.m14297a().mo14168n("onboarding/phone/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        GossipApplication.m9012a().mo9699c();
        if (this.f7067b != null) {
            mo10592b();
            if (this.f7067b.equals("fb")) {
                Bundle bundle = new Bundle();
                bundle.putString("second_fragment", this.f7067b);
                ((OnboardingActivity) getActivity()).switchFragment("contacts", this.f7067b, bundle);
                return;
            }
            getActivity().setResult(-1);
            ((OnboardingActivity) getActivity()).finish();
            return;
        }
        m9616e();
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m9616e() {
        this.spinny.setVisibility(0);
        ApiService.m14297a().mo14125b(false).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                OnboardingContactsFragment.this.spinny.setVisibility(8);
                if (!networkData.success) {
                    if (OnboardingContactsFragment.this.getActivity() instanceof OnboardingActivity) {
                        ((OnboardingActivity) OnboardingContactsFragment.this.getActivity()).communitySuggestions = networkData.groups;
                    }
                    RxBus.m14573a().mo14349a(new C2631at("contacts"));
                    return;
                }
                if (networkData.my_info != null) {
                    AppState.account = networkData.my_info;
                    ((OnboardingActivity) OnboardingContactsFragment.this.getActivity()).finishSyncAccount();
                }
                if (networkData.config != null) {
                    AppState.setConfig(networkData.config);
                }
                if (networkData.activity_settings != null) {
                    AppState.notificationSettings = new ArrayList<>(networkData.activity_settings);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                OnboardingContactsFragment.this.spinny.setVisibility(8);
                Crashlytics.m16437a(th);
                RxBus.m14573a().mo14349a(new C2631at("contacts"));
            }
        });
    }

    /* renamed from: a */
    public void mo10591a() {
        if (ContextCompat.checkSelfPermission(getActivity(), "android.permission.READ_CONTACTS") != 0) {
            requestPermissions(new String[]{"android.permission.READ_CONTACTS"}, 1);
            return;
        }
        m9615d();
    }

    /* renamed from: b */
    public void mo10592b() {
        HashMap hashMap = new HashMap();
        hashMap.put("type", "phone");
        hashMap.put("id_list", DataUtil.join(AppState.contactsInfo.contacts));
        ApiService.m14297a().mo14133c((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            /* renamed from: a */
            public void onNext(ays ays) {
                try {
                    AppState.account.num_phone_friends = ays.mo8410b("num_phone_friends").mo8390g();
                    if (AppState.account != null) {
                        Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Contacts").mo16461a("success", "true")).mo16461a("logged_in", "true"));
                    } else {
                        Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Contacts").mo16461a("success", "true")).mo16461a("logged_in", "false"));
                    }
                    AppState.saveState(GossipApplication.m9012a());
                    RxBus.m14573a().mo14349a(new C2640bb());
                } catch (UnsupportedOperationException e) {
                    AppState.account.num_phone_friends = 0;
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Log.d("ContactsSync", th.toString());
                if (AppState.account != null) {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Contacts").mo16461a("success", "false")).mo16461a("logged_in", "true"));
                } else {
                    Answers.m16445c().mo16453a((CustomEvent) ((CustomEvent) new CustomEvent("Connect Phone Contacts").mo16461a("success", "false")).mo16461a("logged_in", "false"));
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f7067b = arguments.getString("second_fragment");
        }
        ((BaseActivity) getActivity()).addToSubscriptionList(RxBus.m14573a().mo14348a(C2629ar.class, (bjy<T>) new bjy<C2629ar>() {
            /* renamed from: a */
            public void onNext(C2629ar arVar) {
                OnboardingContactsFragment.this.m9615d();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_contacts, viewGroup, false);
        this.f7068c = ButterKnife.bind((Object) this, inflate);
        m9614c();
        if (AppState.config.getInt("show_contacts_skip_android", 0) == 1) {
            this.skip.setVisibility(0);
            this.skip.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (AppState.account != null) {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Skip Phone Contacts").mo16461a("logged_in", "true"));
                    } else {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Skip Phone Contacts").mo16461a("logged_in", "false"));
                    }
                    if (OnboardingContactsFragment.this.f7067b == null) {
                        OnboardingContactsFragment.this.m9616e();
                    } else if (OnboardingContactsFragment.this.f7067b.equals("fb")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("second_fragment", OnboardingContactsFragment.this.f7067b);
                        ((OnboardingActivity) OnboardingContactsFragment.this.getActivity()).switchFragment("contacts", OnboardingContactsFragment.this.f7067b, bundle);
                    } else {
                        OnboardingContactsFragment.this.getActivity().setResult(-1);
                        ((OnboardingActivity) OnboardingContactsFragment.this.getActivity()).finish();
                    }
                }
            });
        }
        this.contactsButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OnboardingContactsFragment.this.mo10591a();
            }
        });
        return inflate;
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        switch (i) {
            case 1:
                if (iArr.length <= 0 || iArr[0] != 0) {
                    Log.d("Permissions", "Unable to get contacts permission");
                    if (AppState.config == null || AppState.config.getBoolean("android_contacts_required", true)) {
                        Builder builder = new Builder(getActivity());
                        String str = "Contacts can be added later, but only to let you know which posts are from your friends.";
                        if (AppState.config != null && AppState.config.has("android_contacts_required_message")) {
                            str = AppState.config.getString("android_contacts_required_message");
                        }
                        builder.setMessage((CharSequence) str);
                        builder.setNeutralButton((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (OnboardingContactsFragment.this.f7067b == null) {
                                    OnboardingContactsFragment.this.m9616e();
                                } else if (OnboardingContactsFragment.this.f7067b.equals("fb")) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("second_fragment", OnboardingContactsFragment.this.f7067b);
                                    RxBus.m14573a().mo14349a(new C2631at("contacts", OnboardingContactsFragment.this.f7067b, bundle));
                                } else {
                                    OnboardingContactsFragment.this.getActivity().setResult(-1);
                                    ((OnboardingActivity) OnboardingContactsFragment.this.getActivity()).finish();
                                }
                            }
                        });
                        builder.create().show();
                        return;
                    } else if (this.f7067b == null) {
                        m9616e();
                        return;
                    } else if (this.f7067b.equals("fb")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("second_fragment", this.f7067b);
                        RxBus.m14573a().mo14349a(new C2631at("contacts", this.f7067b, bundle));
                        return;
                    } else {
                        getActivity().setResult(-1);
                        ((OnboardingActivity) getActivity()).finish();
                        return;
                    }
                } else {
                    RxBus.m14573a().mo14349a(new C2629ar());
                    return;
                }
            default:
                return;
        }
    }
}
