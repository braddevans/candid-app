package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingPhoneFragment;

public class OnboardingPhoneFragment$$ViewBinder<T extends OnboardingPhoneFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingPhoneFragment$$ViewBinder$a */
    /* compiled from: OnboardingPhoneFragment$$ViewBinder */
    public static class C1754a<T extends OnboardingPhoneFragment> implements Unbinder {

        /* renamed from: a */
        private T f7124a;

        protected C1754a(T t) {
            this.f7124a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10653a(T t) {
            t.phonePrivacy = null;
            t.phone = null;
            t.skip = null;
            t.countryCode = null;
            t.spinny = null;
            t.phoneSubmit = null;
            t.wrapper = null;
            t.phoneInfo = null;
            t.phoneSubheader = null;
            t.phoneHeader = null;
        }

        public final void unbind() {
            if (this.f7124a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10653a(this.f7124a);
            this.f7124a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1754a a = mo10652a(t);
        t.phonePrivacy = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.phone_privacy, "field 'phonePrivacy'"), R.id.phone_privacy, "field 'phonePrivacy'");
        t.phone = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.phone_input_text, "field 'phone'"), R.id.phone_input_text, "field 'phone'");
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.phone_skip, "field 'skip'"), R.id.phone_skip, "field 'skip'");
        t.countryCode = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.phone_input_country, "field 'countryCode'"), R.id.phone_input_country, "field 'countryCode'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.phone_spinny, "field 'spinny'"), R.id.phone_spinny, "field 'spinny'");
        t.phoneSubmit = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.phone_input_button, "field 'phoneSubmit'"), R.id.phone_input_button, "field 'phoneSubmit'");
        t.wrapper = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.phone_animation_wrapper, "field 'wrapper'"), R.id.phone_animation_wrapper, "field 'wrapper'");
        t.phoneInfo = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.phone_info, "field 'phoneInfo'"), R.id.phone_info, "field 'phoneInfo'");
        t.phoneSubheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.phone_subheader, "field 'phoneSubheader'"), R.id.phone_subheader, "field 'phoneSubheader'");
        t.phoneHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.phone_header, "field 'phoneHeader'"), R.id.phone_header, "field 'phoneHeader'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1754a<T> mo10652a(T t) {
        return new C1754a<>(t);
    }
}
