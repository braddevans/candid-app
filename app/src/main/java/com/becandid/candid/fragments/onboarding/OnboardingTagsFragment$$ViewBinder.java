package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingTagsFragment;

public class OnboardingTagsFragment$$ViewBinder<T extends OnboardingTagsFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingTagsFragment$$ViewBinder$a */
    /* compiled from: OnboardingTagsFragment$$ViewBinder */
    public static class C1758a<T extends OnboardingTagsFragment> implements Unbinder {

        /* renamed from: a */
        private T f7131a;

        protected C1758a(T t) {
            this.f7131a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10660a(T t) {
            t.tagContainer = null;
            t.tagSubmit = null;
            t.tagCancel = null;
            t.tagsHeader = null;
        }

        public final void unbind() {
            if (this.f7131a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10660a(this.f7131a);
            this.f7131a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1758a a = mo10659a(t);
        t.tagContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.tags_layout, "field 'tagContainer'"), R.id.tags_layout, "field 'tagContainer'");
        t.tagSubmit = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.tags_button, "field 'tagSubmit'"), R.id.tags_button, "field 'tagSubmit'");
        t.tagCancel = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.tags_button_cancel, "field 'tagCancel'"), R.id.tags_button_cancel, "field 'tagCancel'");
        t.tagsHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.tags_header, "field 'tagsHeader'"), R.id.tags_header, "field 'tagsHeader'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1758a<T> mo10659a(T t) {
        return new C1758a<>(t);
    }
}
