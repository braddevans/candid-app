package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingContactsFragment;

public class OnboardingContactsFragment$$ViewBinder<T extends OnboardingContactsFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingContactsFragment$$ViewBinder$a */
    /* compiled from: OnboardingContactsFragment$$ViewBinder */
    public static class C1724a<T extends OnboardingContactsFragment> implements Unbinder {

        /* renamed from: a */
        private T f7075a;

        protected C1724a(T t) {
            this.f7075a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10601a(T t) {
            t.skip = null;
            t.contactsButton = null;
            t.contactsSubheader = null;
            t.contactsSubheader2 = null;
            t.contactsHeader = null;
            t.spinny = null;
        }

        public final void unbind() {
            if (this.f7075a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10601a(this.f7075a);
            this.f7075a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1724a a = mo10600a(t);
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_skip, "field 'skip'"), R.id.contacts_skip, "field 'skip'");
        t.contactsButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_button, "field 'contactsButton'"), R.id.contacts_button, "field 'contactsButton'");
        t.contactsSubheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_subheader, "field 'contactsSubheader'"), R.id.contacts_subheader, "field 'contactsSubheader'");
        t.contactsSubheader2 = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_subheader_2, "field 'contactsSubheader2'"), R.id.contacts_subheader_2, "field 'contactsSubheader2'");
        t.contactsHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_header, "field 'contactsHeader'"), R.id.contacts_header, "field 'contactsHeader'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_spinny, "field 'spinny'"), R.id.contacts_spinny, "field 'spinny'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1724a<T> mo10600a(T t) {
        return new C1724a<>(t);
    }
}
