package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingCommunitySimpleFragment;

public class OnboardingCommunitySimpleFragment$$ViewBinder<T extends OnboardingCommunitySimpleFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingCommunitySimpleFragment$$ViewBinder$a */
    /* compiled from: OnboardingCommunitySimpleFragment$$ViewBinder */
    public static class C1717a<T extends OnboardingCommunitySimpleFragment> implements Unbinder {

        /* renamed from: a */
        View f7062a;

        /* renamed from: b */
        View f7063b;

        /* renamed from: c */
        View f7064c;

        /* renamed from: d */
        private T f7065d;

        protected C1717a(T t) {
            this.f7065d = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10590a(T t) {
            t.skip = null;
            this.f7062a.setOnClickListener(null);
            this.f7063b.setOnClickListener(null);
            this.f7064c.setOnClickListener(null);
        }

        public final void unbind() {
            if (this.f7065d == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10590a(this.f7065d);
            this.f7065d = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1717a a = mo10589a(t);
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_skip, "field 'skip'"), R.id.community_skip, "field 'skip'");
        View view = (View) finder.findRequiredView(obj, R.id.onboarding_community_school, "method 'onCommunitySchoolClick'");
        a.f7062a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCommunitySchoolClick(view);
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.onboarding_community_college, "method 'onCommunityCollegeClick'");
        a.f7063b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCommunityCollegeClick(view);
            }
        });
        View view3 = (View) finder.findRequiredView(obj, R.id.onboarding_community_company, "method 'onCommunityCompanyClick'");
        a.f7064c = view3;
        view3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.onCommunityCompanyClick(view);
            }
        });
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1717a<T> mo10589a(T t) {
        return new C1717a<>(t);
    }
}
