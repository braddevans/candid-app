package com.becandid.candid.fragments.main_tabs;

import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.main_tabs.MessageTabFragment;
import com.becandid.candid.views.TabViewPager;

public class MessageTabFragment$$ViewBinder<T extends MessageTabFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.main_tabs.MessageTabFragment$$ViewBinder$a */
    /* compiled from: MessageTabFragment$$ViewBinder */
    public static class C1690a<T extends MessageTabFragment> implements Unbinder {

        /* renamed from: a */
        private T f7000a;

        protected C1690a(T t) {
            this.f7000a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10549a(T t) {
            t.messagesSearch = null;
            t.messagesSettings = null;
            t.tabBar = null;
            t.viewPager = null;
            t.messagesSpinny = null;
            t.messagingDisabled = null;
            t.messagesContainer = null;
        }

        public final void unbind() {
            if (this.f7000a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10549a(this.f7000a);
            this.f7000a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1690a a = mo10548a(t);
        t.messagesSearch = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_search_btn, "field 'messagesSearch'"), R.id.messages_search_btn, "field 'messagesSearch'");
        t.messagesSettings = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_settings, "field 'messagesSettings'"), R.id.messages_settings, "field 'messagesSettings'");
        t.tabBar = (TabLayout) finder.castView((View) finder.findRequiredView(obj, R.id.messages_subtabs, "field 'tabBar'"), R.id.messages_subtabs, "field 'tabBar'");
        t.viewPager = (TabViewPager) finder.castView((View) finder.findRequiredView(obj, R.id.messages_view_pager, "field 'viewPager'"), R.id.messages_view_pager, "field 'viewPager'");
        t.messagesSpinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.messages_loading, "field 'messagesSpinny'"), R.id.messages_loading, "field 'messagesSpinny'");
        t.messagingDisabled = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_messaging_disabled, "field 'messagingDisabled'"), R.id.messages_messaging_disabled, "field 'messagingDisabled'");
        t.messagesContainer = (View) finder.findRequiredView(obj, R.id.messages_container, "field 'messagesContainer'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1690a<T> mo10548a(T t) {
        return new C1690a<>(t);
    }
}
