package com.becandid.candid.fragments.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.CommunitySearchActivity;
import com.becandid.candid.activities.CommunitySettingsActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import java.util.ArrayList;
import java.util.HashMap;
import p012rx.schedulers.Schedulers;

public class OnboardingCommunitySimpleFragment extends Fragment {

    /* renamed from: a */
    String f7047a;

    /* renamed from: b */
    String f7048b;

    /* renamed from: c */
    String f7049c;

    /* renamed from: d */
    String f7050d;

    /* renamed from: e */
    String f7051e;

    /* renamed from: f */
    private Unbinder f7052f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f7053g;
    @BindView(2131624864)
    TextView skip;

    /* renamed from: a */
    private void m9604a() {
        if (AppState.groups == null) {
            AppState.groups = new ArrayList();
        }
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        Group group = new Group();
        if (this.f7048b != null) {
            arrayList.add(this.f7048b);
            group.group_id = Integer.parseInt(this.f7048b);
            group.group_name = this.f7047a;
            group.community_membership = 1;
            group.group_type = this.f7051e;
            group.num_members = Integer.parseInt(this.f7049c);
            group.thumb_url = this.f7050d;
            if (!AppState.groups.contains(group)) {
                AppState.groups.add(group);
            }
        }
        hashMap.put("group_ids", bid.m8220a(arrayList.toArray(), ","));
        ApiService.m14297a().mo14179x(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        });
        AppState.setCommunity = 1;
        if (this.f7053g) {
            getActivity().finish();
        } else {
            mo10582a("community");
        }
    }

    /* renamed from: a */
    private void m9605a(int i) {
        Intent intent = new Intent(getActivity(), CommunitySearchActivity.class);
        intent.putExtra("hide_arrow", true);
        String str = null;
        String str2 = null;
        switch (i) {
            case CommunitySettingsActivity.REQUEST_HIGHSCHOOL /*201*/:
                str = "school";
                str2 = AppState.config.getString("community_search_high_school_placeholder", "ex. Mill High School");
                break;
            case CommunitySettingsActivity.REQUEST_COLLEGE /*202*/:
                str = "college";
                str2 = AppState.config.getString("community_search_college_placeholder", "ex. University of San Francisco");
                break;
            case 203:
                str = "company";
                str2 = AppState.config.getString("community_search_work_placeholder", "ex. MyLikes");
                break;
        }
        intent.putExtra("queryType", str);
        intent.putExtra("searchViewHint", str2);
        intent.putExtra("hideBackBtn", true);
        startActivityForResult(intent, i);
    }

    /* renamed from: a */
    public void mo10582a(String str) {
        FragmentActivity activity = getActivity();
        if (activity instanceof OnboardingActivity) {
            ((OnboardingActivity) activity).switchFragment(str);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras.containsKey("queryString") && extras.containsKey("queryId") && extras.containsKey("num_members") && extras.containsKey("thumb_url")) {
                this.f7047a = extras.getString("queryString");
                this.f7048b = extras.getString("queryId");
                this.f7049c = extras.getString("num_members");
                this.f7050d = extras.getString("thumb_url");
                switch (i) {
                    case CommunitySettingsActivity.REQUEST_HIGHSCHOOL /*201*/:
                        this.f7051e = "school";
                        break;
                    case CommunitySettingsActivity.REQUEST_COLLEGE /*202*/:
                        this.f7051e = "college";
                        break;
                    case 203:
                        this.f7051e = "company";
                        break;
                }
                m9604a();
            }
        } else {
            mo10582a("community");
        }
        ApiService.m14297a().mo14168n("onboarding/community/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
    }

    /* access modifiers changed from: 0000 */
    @OnClick({2131624916})
    public void onCommunityCollegeClick(View view) {
        m9605a((int) CommunitySettingsActivity.REQUEST_COLLEGE);
    }

    /* access modifiers changed from: 0000 */
    @OnClick({2131624917})
    public void onCommunityCompanyClick(View view) {
        m9605a(203);
    }

    /* access modifiers changed from: 0000 */
    @OnClick({2131624915})
    public void onCommunitySchoolClick(View view) {
        m9605a((int) CommunitySettingsActivity.REQUEST_HIGHSCHOOL);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_community_simple, viewGroup, false);
        this.f7052f = ButterKnife.bind((Object) this, inflate);
        if (AppState.config.getInt("android_enable_skip_onboarding_community", 1) == 1) {
            this.skip.setVisibility(0);
            this.skip.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    ApiService.m14297a().mo14168n("onboarding/community/skip").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                    AppState.hasShownCommunityPopup = true;
                    AppState.saveState(GossipApplication.m9012a());
                    if (OnboardingCommunitySimpleFragment.this.f7053g) {
                        OnboardingCommunitySimpleFragment.this.getActivity().finish();
                    } else {
                        OnboardingCommunitySimpleFragment.this.mo10582a("community");
                    }
                }
            });
        } else {
            this.skip.setVisibility(8);
        }
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f7052f.unbind();
    }
}
