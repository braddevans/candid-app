package com.becandid.candid.fragments.main_tabs;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.C0411a;
import android.support.design.widget.TabLayout.C0416d;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.fragments.CommunityFeedFragment;
import com.becandid.candid.fragments.NearbyFeedFragment;
import com.becandid.candid.fragments.NearbyFeedFragment.C1634a;
import com.becandid.candid.views.MainTabViewPager;
import com.venmo.view.TooltipView;
import java.util.ArrayList;
import java.util.List;

public class FeedTabFragment extends BaseMainTabFragment implements C0411a, C1634a {

    /* renamed from: d */
    BaseActivity f6922d;

    /* renamed from: e */
    final Runnable f6923e = new Runnable() {
        public void run() {
            FeedTabFragment.this.mTooltipView.setVisibility(8);
        }
    };

    /* renamed from: f */
    private TabPagerAdapter f6924f;

    /* renamed from: g */
    private List<String> f6925g;

    /* renamed from: h */
    private Handler f6926h;
    @BindView(2131624588)
    View mFeedTab;
    @BindView(2131624590)
    TabLayout mFeedTabs;
    @BindView(2131624592)
    View mNewPostsText;
    @BindView(2131624174)
    TooltipView mTooltipView;
    @BindView(2131624591)
    MainTabViewPager viewPager;

    /* renamed from: b */
    private String m9490b(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case -1480249367:
                if (str.equals("community")) {
                    c = 1;
                    break;
                }
                break;
            case -1049482625:
                if (str.equals("nearby")) {
                    c = 0;
                    break;
                }
                break;
            case -600094315:
                if (str.equals("friends")) {
                    c = 4;
                    break;
                }
                break;
            case 3208415:
                if (str.equals("home")) {
                    c = 2;
                    break;
                }
                break;
            case 1394955557:
                if (str.equals("trending")) {
                    c = 3;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return "Nearby";
            case 1:
                return "Community";
            case 2:
                return "New";
            case 3:
                return "Hot";
            case 4:
                return "Friends";
            default:
                return "Friends";
        }
    }

    /* renamed from: a */
    public void mo10479a() {
        super.mo10479a();
        if (this.viewPager != null) {
            for (int i = 0; i < this.viewPager.getChildCount(); i++) {
                ((FeedFragment) this.f6924f.getItem(i)).mo13890a(0);
            }
        }
    }

    /* renamed from: a */
    public void mo10488a(int i) {
        ((FeedFragment) this.f6924f.getItem(this.viewPager.getCurrentItem())).mo13891b(i);
    }

    /* renamed from: a */
    public void mo10459a(int i, int i2, int i3) {
        this.f6926h.removeCallbacks(this.f6923e);
        this.mTooltipView.setVisibility(0);
        this.mTooltipView.setText(Integer.toString(i2) + "\n miles");
        LayoutParams layoutParams = (LayoutParams) this.mTooltipView.getLayoutParams();
        layoutParams.setMargins(i3, 0, 0, 0);
        this.mTooltipView.setLayoutParams(layoutParams);
        this.f6926h.postDelayed(this.f6923e, 2000);
    }

    /* renamed from: a */
    public void mo10489a(String str) {
        if (AppState.tabsOrder.contains(str)) {
            this.viewPager.setCurrentItem(AppState.tabsOrder.indexOf(str));
        }
    }

    /* renamed from: b */
    public void mo10480b() {
        if (this.mFeedTab != null) {
            this.mFeedTab.setVisibility(0);
        }
    }

    /* renamed from: c */
    public void mo10481c() {
        if (this.mFeedTab != null) {
            this.mFeedTab.setVisibility(8);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (this.f6922d != null && this.f6922d.callbackManager != null) {
            this.f6922d.callbackManager.mo11618a(i, i2, intent);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FeedFragment communityFeedFragment;
        View inflate = layoutInflater.inflate(R.layout.feed_tab, viewGroup, false);
        this.f10275b = ButterKnife.bind((Object) this, inflate);
        this.f6922d = (BaseActivity) getActivity();
        this.f10274a = getContext();
        this.f6926h = new Handler(Looper.getMainLooper());
        this.mFeedTabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.gossip));
        this.mFeedTabs.setTabTextColors(Color.parseColor("#888888"), getResources().getColor(R.color.gossip));
        this.f6925g = AppState.tabsOrder;
        if (this.f6925g == null || this.f6925g.isEmpty()) {
            this.f6925g = new ArrayList();
            this.f6925g.add("home");
            this.f6925g.add("trending");
            this.f6925g.add("community");
            this.f6925g.add("nearby");
        }
        this.f6924f = new TabPagerAdapter(this.f6922d.getSupportFragmentManager());
        for (String str : this.f6925g) {
            char c = 65535;
            switch (str.hashCode()) {
                case -1480249367:
                    if (str.equals("community")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1049482625:
                    if (str.equals("nearby")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    communityFeedFragment = new NearbyFeedFragment();
                    break;
                case 1:
                    communityFeedFragment = new CommunityFeedFragment();
                    break;
                default:
                    communityFeedFragment = new FeedFragment();
                    break;
            }
            Bundle bundle2 = new Bundle();
            bundle2.putString("feed_type", str);
            communityFeedFragment.setArguments(bundle2);
            this.f6924f.mo13889a(communityFeedFragment, m9490b(str));
        }
        this.viewPager.setSwipeEnabled(false);
        this.viewPager.setAdapter(this.f6924f);
        this.viewPager.setOffscreenPageLimit(3);
        this.viewPager.setCurrTab("feed");
        this.mFeedTabs.setupWithViewPager(this.viewPager);
        this.mFeedTabs.setOnTabSelectedListener(this);
        ((BaseActivity) this.f10274a).addToSubscriptionList(RxBus.m14573a().mo14348a(C2624am.class, (bjy<T>) new bjy<C2624am>() {
            /* renamed from: a */
            public void onNext(C2624am amVar) {
                if (AppState.relatedPosts != null && !AppState.relatedPosts.isEmpty() && FeedTabFragment.this.viewPager != null && !amVar.f10604b) {
                    FeedTabFragment.this.viewPager.setCurrentItem(0);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        ((BaseActivity) this.f10274a).addToSubscriptionList(RxBus.m14573a().mo14348a(C2657n.class, (bjy<T>) new bjy<C2657n>() {
            /* renamed from: a */
            public void onNext(C2657n nVar) {
                FeedTabFragment.this.viewPager.setCurrentItem(0);
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        return inflate;
    }

    public void onResume() {
        super.onResume();
    }

    public void onTabReselected(C0416d dVar) {
        ((FeedFragment) this.f6924f.getItem(dVar.mo1814c())).mo13890a(0);
    }

    public void onTabSelected(C0416d dVar) {
        this.viewPager.setCurrentItem(dVar.mo1814c(), true);
        FeedFragment hwVar = (FeedFragment) this.f6924f.getItem(dVar.mo1814c());
        if (hwVar instanceof NearbyFeedFragment) {
            ((NearbyFeedFragment) hwVar).mo10447a((C1634a) this);
        }
    }

    public void onTabUnselected(C0416d dVar) {
    }
}
