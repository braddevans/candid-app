package com.becandid.candid.fragments.onboarding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.activities.WebViewActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.FacebookInfo.LoadCallback;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import com.facebook.AccessToken;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import p012rx.schedulers.Schedulers;

public class OnboardingFacebookFragment extends Fragment {

    /* renamed from: a */
    String f7076a;

    /* renamed from: b */
    private Unbinder f7077b;

    /* renamed from: c */
    private CallbackManager f7078c;
    @BindView(2131624944)
    RelativeLayout fbButton;
    @BindView(2131624925)
    TextView fbHeader;
    @BindView(2131624947)
    TextView fbInfo;
    @BindView(2131624948)
    TextView fbInfo2;
    @BindView(2131624949)
    TextView fbPrivacy;
    @BindView(2131624926)
    TextView fbSubheader;
    @BindView(2131624924)
    TextView skip;
    @BindView(2131624950)
    FrameLayout spinny;

    /* renamed from: c */
    private void m9625c() {
        if (AppState.config != null) {
            this.fbHeader.setText(AppState.config.getString("fb_header", getString(R.string.fb_header)));
            this.fbSubheader.setText(AppState.config.getString("fb_subheader", getString(R.string.fb_subheader)));
            this.fbPrivacy.setText(AppState.config.getString("more_info", getString(R.string.privacy)));
            this.fbInfo.setText(AppState.config.getString("fb_info", getString(R.string.fb_info)));
            this.fbInfo2.setText(AppState.config.getString("fb_info_2", ""));
            return;
        }
        this.fbHeader.setText(getString(R.string.fb_header));
        this.fbSubheader.setText(getString(R.string.fb_subheader));
        this.fbPrivacy.setText(getString(R.string.privacy));
        this.fbInfo.setText(getString(R.string.fb_info));
        this.fbInfo2.setText("");
    }

    /* renamed from: a */
    public AnimationSet mo10602a(int i, int i2, int i3, int i4, View view) {
        AnimationSet animationSet = new AnimationSet(false);
        float f = getResources().getDisplayMetrics().density;
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, ((float) i) * f, 0, 0.0f, 0, ((float) i2) * f);
        translateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        translateAnimation.setDuration(200);
        translateAnimation.setStartOffset((long) (i3 + 1400));
        translateAnimation.setFillAfter(true);
        translateAnimation.setFillEnabled(true);
        animationSet.addAnimation(translateAnimation);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0, 0.0f, 0, 0.0f, 0, 0.0f, 2, 1.0f);
        translateAnimation2.setInterpolator(new AccelerateDecelerateInterpolator());
        translateAnimation2.setDuration(400);
        translateAnimation2.setStartOffset((long) (i3 + 2200));
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setFillEnabled(true);
        final View view2 = view;
        final int i5 = i4;
        final int i6 = i3;
        translateAnimation2.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                view2.findViewById(i5).setVisibility(8);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        view2.findViewById(i5).bringToFront();
                        view2.findViewById(R.id.fb_bottom).bringToFront();
                    }
                }, (long) (i6 + 2200));
            }
        });
        animationSet.addAnimation(translateAnimation2);
        return animationSet;
    }

    /* renamed from: a */
    public void mo10603a() {
        if (this.f7078c == null) {
            this.f7078c = C3115a.m17289a();
            abo.m502c().mo264a(this.f7078c, (FacebookCallback<abp>) new FacebookCallback<abp>() {
                /* renamed from: a */
                public void onSuccess(abp abp) {
                    if (AppState.account != null) {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook Successful").mo16461a("logged_in", "true"));
                    } else {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook Successful").mo16461a("logged_in", "false"));
                    }
                    OnboardingFacebookFragment.this.spinny.setVisibility(8);
                    ApiService.m14297a().mo14168n("onboarding/fb/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                    if (OnboardingFacebookFragment.this.f7076a != null) {
                        AppState.setFBInfo(abp.mo274a());
                        OnboardingFacebookFragment.this.spinny.setVisibility(0);
                        ((BaseActivity) OnboardingFacebookFragment.this.getActivity()).addToSubscriptionList(RxBus.m14573a().mo14348a(C2668y.class, (bjy<T>) new bjy<C2668y>() {
                            /* renamed from: a */
                            public void onNext(C2668y yVar) {
                                OnboardingFacebookFragment.this.spinny.setVisibility(8);
                                ((BaseActivity) OnboardingFacebookFragment.this.getActivity()).sendFbDataToServer();
                                OnboardingFacebookFragment.this.getActivity().setResult(-1);
                                ((OnboardingActivity) OnboardingFacebookFragment.this.getActivity()).finish();
                            }

                            public void onCompleted() {
                            }

                            public void onError(Throwable th) {
                                Crashlytics.m16437a(th);
                            }
                        }));
                        return;
                    }
                    AppState.setFBInfo(abp.mo274a(), new LoadCallback() {
                        public void onNext(int i) {
                            OnboardingFacebookFragment.this.mo10606b();
                        }
                    });
                }

                public void onCancel() {
                    if (AppState.account != null) {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook Cancelled").mo16461a("logged_in", "true"));
                    } else {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook Cancelled").mo16461a("logged_in", "false"));
                    }
                    Log.d("FBConnect", "cancel");
                    OnboardingFacebookFragment.this.spinny.setVisibility(8);
                    OnboardingFacebookFragment.this.fbButton.setEnabled(true);
                }

                public void onError(FacebookException facebookException) {
                    if (AppState.account != null) {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook Error").mo16461a("logged_in", "true"));
                    } else {
                        Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook Error").mo16461a("logged_in", "false"));
                    }
                    Crashlytics.m16437a((Throwable) facebookException);
                    if (!(facebookException instanceof FacebookAuthorizationException) || AccessToken.m10408a() == null) {
                        Toast.makeText(GossipApplication.m9012a(), "Unable to connect to Facebook", 1).show();
                        OnboardingFacebookFragment.this.spinny.setVisibility(8);
                        OnboardingFacebookFragment.this.fbButton.setEnabled(true);
                        return;
                    }
                    abo.m502c().mo270d();
                    abo.m502c().mo261a((Activity) OnboardingFacebookFragment.this.getActivity(), (Collection<String>) Arrays.asList("public_profile,user_friends,user_work_history,user_education_history".split(",")));
                }
            });
        }
        abo.m502c().mo263a((Fragment) this, (Collection<String>) Arrays.asList("public_profile,user_friends,user_work_history,user_education_history".split(",")));
    }

    /* renamed from: a */
    public void mo10604a(View view) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.75f, 1.0f, 0.75f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setStartOffset(500);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, -10.0f, 1, 0.5f, 1, 1.0f);
        rotateAnimation.setDuration(200);
        rotateAnimation.setStartOffset(1000);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setFillEnabled(true);
        RotateAnimation rotateAnimation2 = new RotateAnimation(0.0f, 10.0f, 1, 0.5f, 1, 1.0f);
        rotateAnimation2.setDuration(200);
        rotateAnimation2.setStartOffset(1200);
        rotateAnimation2.setFillAfter(true);
        rotateAnimation2.setFillEnabled(true);
        AnimationSet a = mo10602a(-110, 20, 30, R.id.fb_spark_1, view);
        AnimationSet a2 = mo10602a(-125, -10, 50, R.id.fb_spark_2, view);
        AnimationSet a3 = mo10602a(-115, -40, 30, R.id.fb_spark_3, view);
        AnimationSet a4 = mo10602a(-90, -80, 40, R.id.fb_spark_4, view);
        AnimationSet a5 = mo10602a(-30, -105, 50, R.id.fb_spark_5, view);
        AnimationSet a6 = mo10602a(-15, -80, 10, R.id.fb_spark_6, view);
        AnimationSet a7 = mo10602a(68, -90, 20, R.id.fb_spark_7, view);
        AnimationSet a8 = mo10602a(115, -50, 30, R.id.fb_spark_8, view);
        AnimationSet a9 = mo10602a(100, -25, 10, R.id.fb_spark_9, view);
        AnimationSet a10 = mo10602a(110, 8, 20, R.id.fb_spark_10, view);
        AnimationSet a11 = mo10602a(100, 30, 40, R.id.fb_spark_11, view);
        view.findViewById(R.id.group_layout).startAnimation(scaleAnimation);
        view.findViewById(R.id.group_traveling).startAnimation(rotateAnimation);
        view.findViewById(R.id.group_candy).startAnimation(rotateAnimation2);
        view.findViewById(R.id.fb_spark_1).startAnimation(a);
        view.findViewById(R.id.fb_spark_2).startAnimation(a2);
        view.findViewById(R.id.fb_spark_3).startAnimation(a3);
        view.findViewById(R.id.fb_spark_4).startAnimation(a4);
        view.findViewById(R.id.fb_spark_5).startAnimation(a5);
        view.findViewById(R.id.fb_spark_6).startAnimation(a6);
        view.findViewById(R.id.fb_spark_7).startAnimation(a7);
        view.findViewById(R.id.fb_spark_8).startAnimation(a8);
        view.findViewById(R.id.fb_spark_9).startAnimation(a9);
        view.findViewById(R.id.fb_spark_10).startAnimation(a10);
        view.findViewById(R.id.fb_spark_11).startAnimation(a11);
    }

    /* renamed from: a */
    public void mo10605a(String str) {
        FragmentActivity activity = getActivity();
        if (activity instanceof OnboardingActivity) {
            ((OnboardingActivity) activity).switchFragment(str);
        }
    }

    /* renamed from: b */
    public void mo10606b() {
        if (AppState.config.getInt("disable_mobile_fb_phone_login", 0) == 0) {
            ApiService.m14297a().mo14125b(true).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                /* renamed from: a */
                public void onNext(NetworkData networkData) {
                    if (!networkData.success) {
                        if (OnboardingFacebookFragment.this.getActivity() instanceof OnboardingActivity) {
                            ((OnboardingActivity) OnboardingFacebookFragment.this.getActivity()).communitySuggestions = networkData.groups;
                        }
                        RxBus.m14573a().mo14349a(new C2631at("fb"));
                        return;
                    }
                    if (networkData.my_info != null) {
                        AppState.account = networkData.my_info;
                        ((OnboardingActivity) OnboardingFacebookFragment.this.getActivity()).finishSyncAccount();
                    }
                    if (networkData.config != null) {
                        AppState.setConfig(networkData.config);
                    }
                    if (networkData.activity_settings != null) {
                        AppState.notificationSettings = new ArrayList<>(networkData.activity_settings);
                    }
                }

                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    RxBus.m14573a().mo14349a(new C2631at("fb"));
                }
            });
        } else {
            mo10605a("fb");
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (this.f7078c != null) {
            this.f7078c.mo11618a(i, i2, intent);
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f7076a = arguments.getString("second_fragment");
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_fb, viewGroup, false);
        this.f7077b = ButterKnife.bind((Object) this, inflate);
        m9625c();
        this.skip.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ApiService.m14297a().mo14168n("onboarding/fb/skip").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                if (OnboardingFacebookFragment.this.f7076a != null) {
                    OnboardingFacebookFragment.this.getActivity().setResult(-1);
                    OnboardingFacebookFragment.this.getActivity().finish();
                    return;
                }
                OnboardingFacebookFragment.this.mo10605a("fb");
            }
        });
        this.fbButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (AppState.account != null) {
                    Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook").mo16461a("logged_in", "true"));
                } else {
                    Answers.m16445c().mo16453a((CustomEvent) new CustomEvent("Connect To Facebook").mo16461a("logged_in", "false"));
                }
                OnboardingFacebookFragment.this.mo10603a();
            }
        });
        this.fbPrivacy.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(OnboardingFacebookFragment.this.getContext(), WebViewActivity.class);
                intent.putExtra("title", "Why Can I Trust Candid?");
                intent.putExtra("url", GossipApplication.f6096d + "content/whysafe");
                OnboardingFacebookFragment.this.getContext().startActivity(intent);
            }
        });
        mo10604a(inflate);
        return inflate;
    }
}
