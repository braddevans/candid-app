package com.becandid.candid.fragments.main_tabs;

import android.support.design.widget.TabLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.main_tabs.GroupTabFragment;
import com.becandid.candid.views.GroupStackView;

public class GroupTabFragment$$ViewBinder<T extends GroupTabFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.main_tabs.GroupTabFragment$$ViewBinder$a */
    /* compiled from: GroupTabFragment$$ViewBinder */
    public static class C1658a<T extends GroupTabFragment> implements Unbinder {

        /* renamed from: a */
        private T f6937a;

        protected C1658a(T t) {
            this.f6937a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10500a(T t) {
            t.groupStackView = null;
            t.tabBar = null;
            t.loading = null;
            t.forYouEmpty = null;
            t.discoverEmpty = null;
        }

        public final void unbind() {
            if (this.f6937a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10500a(this.f6937a);
            this.f6937a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1658a a = mo10499a(t);
        t.groupStackView = (GroupStackView) finder.castView((View) finder.findRequiredView(obj, R.id.groups_stack, "field 'groupStackView'"), R.id.groups_stack, "field 'groupStackView'");
        t.tabBar = (TabLayout) finder.castView((View) finder.findRequiredView(obj, R.id.groups_subtabs, "field 'tabBar'"), R.id.groups_subtabs, "field 'tabBar'");
        t.loading = (View) finder.findOptionalView(obj, R.id.groups_loading, null);
        t.forYouEmpty = (View) finder.findOptionalView(obj, R.id.groups_foryou_empty, null);
        t.discoverEmpty = (View) finder.findOptionalView(obj, R.id.groups_discover_empty, null);
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1658a<T> mo10499a(T t) {
        return new C1658a<>(t);
    }
}
