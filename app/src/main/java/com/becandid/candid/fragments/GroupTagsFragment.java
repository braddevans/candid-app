package com.becandid.candid.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.CreateGroupActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.views.TagBox;
import com.becandid.candid.views.TagBox.C1851a;
import java.util.ArrayList;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class GroupTagsFragment extends Fragment implements C1851a {

    /* renamed from: a */
    private Unbinder f6803a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Bundle f6804b;

    /* renamed from: c */
    private List<String> f6805c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f6806d = false;
    @BindView(2131624631)
    Button mCreateGroupBtn;
    @BindView(2131624101)
    View mProgressBar;
    @BindView(2131624626)
    View mTagsPlaceholder;
    @BindView(2131624629)
    RelativeLayout mTagsView;

    /* renamed from: a */
    public void mo10404a() {
        final CreateGroupActivity createGroupActivity = (CreateGroupActivity) getActivity();
        ApiService.m14297a().mo14098a(((Integer) this.f6804b.get("group_id")).intValue(), createGroupActivity.getmGroupName(), createGroupActivity.getmGroupAbout(), createGroupActivity.getGroupTags(), createGroupActivity.getmGroupSourceUrl()).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Group>() {
            /* renamed from: a */
            public void onNext(Group group) {
                for (int i = 0; i < AppState.groups.size(); i++) {
                    if (((Group) AppState.groups.get(i)).group_id == group.group_id) {
                        AppState.groups.remove(i);
                    }
                }
                AppState.groups.add(group);
                RxBus.m14573a().mo14349a(new C2666w(group));
                Intent intent = new Intent();
                intent.putExtra("group", group);
                createGroupActivity.setResult(-1, intent);
                createGroupActivity.finish();
            }

            public void onCompleted() {
                Toast.makeText(GroupTagsFragment.this.getContext(), "Successfully updated your group", 0).show();
            }

            public void onError(Throwable th) {
                GroupTagsFragment.this.mProgressBar.setVisibility(8);
                Toast.makeText(GroupTagsFragment.this.getContext(), "Failed to update your group", 0).show();
                createGroupActivity.setResult(0);
                GroupTagsFragment.this.getActivity().finish();
            }
        });
    }

    /* renamed from: a */
    public void mo10405a(View view, String str) {
        boolean z = true;
        boolean z2 = false;
        FragmentActivity activity = getActivity();
        if (activity instanceof CreateGroupActivity) {
            z2 = ((CreateGroupActivity) activity).addTag(str.toLowerCase());
        }
        if (z2) {
            view.setSelected(!view.isSelected());
        } else {
            Toast.makeText(view.getContext(), "You can only add three tags", 0).show();
        }
        Button button = this.mCreateGroupBtn;
        if (((CreateGroupActivity) activity).isTagsEmpty()) {
            z = false;
        }
        button.setEnabled(z);
    }

    /* renamed from: b */
    public void mo10406b() {
        final CreateGroupActivity createGroupActivity = (CreateGroupActivity) getActivity();
        ApiService.m14297a().mo14107a(createGroupActivity.getmGroupName(), createGroupActivity.getmGroupAbout(), createGroupActivity.getGroupTags(), createGroupActivity.getmGroupSourceUrl()).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Group>() {
            /* renamed from: a */
            public void onNext(Group group) {
                AppState.groups.add(group);
                RxBus.m14573a().mo14349a(new C2659p(group));
                Intent intent = new Intent();
                intent.putExtra("group", group);
                createGroupActivity.setResult(-1, intent);
                createGroupActivity.finish();
            }

            public void onCompleted() {
                Toast.makeText(GroupTagsFragment.this.getContext(), "Successfully created your group", 0).show();
            }

            public void onError(Throwable th) {
                GroupTagsFragment.this.mProgressBar.setVisibility(8);
                Toast.makeText(GroupTagsFragment.this.getContext(), "Failed to create a group", 0).show();
                createGroupActivity.setResult(0);
                GroupTagsFragment.this.getActivity().finish();
            }
        });
    }

    /* renamed from: c */
    public boolean mo10407c() {
        return this.mProgressBar.getVisibility() == 0;
    }

    /* renamed from: d */
    public void mo10408d() {
        if (mo10407c()) {
            this.mProgressBar.setVisibility(8);
            Toast.makeText(getContext(), "Failed to create a group", 0).show();
            getActivity().setResult(0);
            getActivity().finish();
            return;
        }
        this.f6806d = true;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_group_tags, viewGroup, false);
        this.f6803a = ButterKnife.bind((Object) this, inflate);
        this.f6804b = getArguments();
        final CreateGroupActivity createGroupActivity = (CreateGroupActivity) getActivity();
        if (createGroupActivity instanceof CreateGroupActivity) {
            createGroupActivity.resetTags();
        }
        this.f6805c = new ArrayList();
        if (this.f6804b.containsKey("tags")) {
            ArrayList stringArrayList = this.f6804b.getStringArrayList("tags");
            this.f6805c.addAll(stringArrayList);
            C2699jj.m14609a((Activity) getActivity(), this.mTagsView, AppState.groupTags);
            for (int i = 0; i < stringArrayList.size(); i++) {
                createGroupActivity.addTag((String) stringArrayList.get(i));
            }
            this.mCreateGroupBtn.setText("Update Group");
            this.mCreateGroupBtn.setEnabled(!createGroupActivity.isTagsEmpty());
        } else {
            C2699jj.m14609a((Activity) getActivity(), this.mTagsView, AppState.groupTags);
        }
        this.mCreateGroupBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                GroupTagsFragment.this.mProgressBar.setVisibility(0);
                GroupTagsFragment.this.mTagsPlaceholder.setAlpha(0.3f);
                if (GroupTagsFragment.this.f6806d || createGroupActivity.ismGroupPhotoFailedToUpload()) {
                    GroupTagsFragment.this.mo10408d();
                } else if (!createGroupActivity.isGroupPhotoSkipped() && createGroupActivity.getmGroupSourceUrl() == null) {
                } else {
                    if (GroupTagsFragment.this.f6804b.containsKey("edit_group")) {
                        GroupTagsFragment.this.mo10404a();
                    } else {
                        GroupTagsFragment.this.mo10406b();
                    }
                }
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f6803a.unbind();
    }

    public void onResume() {
        super.onResume();
        int childCount = this.mTagsView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            LinearLayout linearLayout = (LinearLayout) this.mTagsView.getChildAt(i);
            for (int i2 = 0; i2 < linearLayout.getChildCount(); i2++) {
                TagBox tagBox = (TagBox) linearLayout.getChildAt(i2);
                tagBox.setTagClickListener(this);
                if (this.f6805c.contains(tagBox.getTagNameLowerCase())) {
                    tagBox.setSelected(true);
                }
            }
        }
    }
}
