package com.becandid.candid.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.p001v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.support.percent.PercentRelativeLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.CommunitySearchActivity;
import com.becandid.candid.activities.CommunitySettingsActivity;
import com.becandid.candid.adapters.CommunityFilterAdapter;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.Post;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.util.CropTransformation;
import com.becandid.candid.util.CropTransformation.CropType;
import com.becandid.candid.util.RoundedCornersTransformation;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;
import com.becandid.candid.views.CommunityOnboardingSimpleView;
import com.becandid.candid.views.CommunityOnboardingSimpleView.C1801a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class CommunityFeedFragment extends FeedFragment implements C1801a {
    /* access modifiers changed from: private */

    /* renamed from: F */
    public static String[] f6724F = {"school", "college", "company"};
    /* access modifiers changed from: private */

    /* renamed from: A */
    public String f6725A;
    /* access modifiers changed from: private */

    /* renamed from: B */
    public String f6726B;
    /* access modifiers changed from: private */

    /* renamed from: C */
    public String f6727C;
    /* access modifiers changed from: private */

    /* renamed from: D */
    public String f6728D;
    /* access modifiers changed from: private */

    /* renamed from: E */
    public String f6729E;

    /* renamed from: G */
    private bjz f6730G;

    /* renamed from: H */
    private bjz f6731H;

    /* renamed from: I */
    private RoundedCornersTransformation f6732I;
    /* access modifiers changed from: private */

    /* renamed from: J */
    public CommunityFilterAdapter f6733J;

    /* renamed from: K */
    private bjz f6734K;

    /* renamed from: L */
    private int f6735L;

    /* renamed from: a */
    RelativeLayout f6736a;
    @BindView(2131624883)
    RelativeLayout addCollege;
    @BindView(2131624874)
    RelativeLayout addHighSchool;
    @BindView(2131624902)
    RelativeLayout addNone;

    /* renamed from: b */
    RelativeLayout f6737b;
    @BindView(2131624869)
    Button button;

    /* renamed from: c */
    RelativeLayout f6738c;
    @BindView(2131624884)
    RelativeLayout collegeDefault;
    @BindView(2131624888)
    ImageView collegeImgSelected;
    @BindView(2131624891)
    TextView collegeMembers;
    @BindView(2131624887)
    RelativeLayout collegeSelected;
    @BindView(2131624890)
    TextView collegeSelectedName;
    @BindView(2131624886)
    TextView collegeText;
    @BindView(2131624459)
    ImageView communityFeedAdd;
    @BindView(2131624466)
    TextView communityFeedCollege;
    @BindView(2131624454)
    CoordinatorLayout communityFeedContent;
    @BindView(2131624469)
    View communityFeedEmpty;
    @BindView(2131624456)
    View communityFeedFilterContainer;
    @BindView(2131624465)
    TextView communityFeedHighschool;
    @BindView(2131624470)
    PercentRelativeLayout communityFeedOnboarding;
    @BindView(2131624471)
    CommunityOnboardingSimpleView communityFeedOnboardingSimple;
    @BindView(2131624462)
    TextView communityFeedSettings;
    @BindView(2131624463)
    View communityFeedSettingsFilter;
    @BindView(2131624458)
    RecyclerView communityFeedTagRecycler;
    @BindView(2131624460)
    TextView communityFeedTagRecyclerEmpty;
    @BindView(2131624467)
    TextView communityFeedWork;

    /* renamed from: d */
    TextView f6739d;

    /* renamed from: e */
    ImageView f6740e;

    /* renamed from: f */
    TextView f6741f;

    /* renamed from: g */
    TextView f6742g;

    /* renamed from: h */
    List<Group> f6743h = new ArrayList();
    @BindView(2131624865)
    TextView header;
    @BindView(2131624875)
    RelativeLayout highSchoolDefault;
    @BindView(2131624879)
    ImageView highSchoolImgSelected;
    @BindView(2131624882)
    TextView highSchoolMembers;
    @BindView(2131624878)
    RelativeLayout highSchoolSelected;
    @BindView(2131624881)
    TextView highSchoolSelectedName;
    @BindView(2131624877)
    TextView highSchoolText;

    /* renamed from: i */
    bjy<C2635ax> f6744i = new bjy<C2635ax>() {
        /* renamed from: a */
        public void onNext(C2635ax axVar) {
            CommunityFeedFragment.this.mo13890a(0);
        }

        public void onCompleted() {
            RxBus.m14573a().mo14348a(C2635ax.class, CommunityFeedFragment.this.f6744i);
        }

        public void onError(Throwable th) {
        }
    };

    /* renamed from: j */
    bjy<C2659p> f6745j = new bjy<C2659p>() {
        /* renamed from: a */
        public void onNext(C2659p pVar) {
            if (!CommunityFeedFragment.this.f6743h.contains(pVar.f10653a)) {
                for (String equals : CommunityFeedFragment.f6724F) {
                    if (pVar.f10653a.group_type.equals(equals)) {
                        if (CommunityFeedFragment.this.f6743h.size() == 0) {
                            CommunityFeedFragment.this.communityFeedFilterContainer.setVisibility(0);
                        }
                        CommunityFeedFragment.this.f6743h.add(0, pVar.f10653a);
                        if (CommunityFeedFragment.this.communityFeedContent.getVisibility() != 0) {
                            CommunityFeedFragment.this.mo10348a();
                            return;
                        }
                        CommunityFeedFragment.this.f6733J.mo13771e().add(0, pVar.f10653a);
                        CommunityFeedFragment.this.f6733J.notifyItemInserted(0);
                        CommunityFeedFragment.this.mo13890a(0);
                        return;
                    }
                }
            }
        }

        public void onCompleted() {
            RxBus.m14573a().mo14348a(C2659p.class, CommunityFeedFragment.this.f6745j);
        }

        public void onError(Throwable th) {
        }
    };
    @BindView(2131624903)
    ImageView noneCheck;
    @BindView(2131624904)
    TextView noneText;

    /* renamed from: s */
    private Unbinder f6746s;
    @BindView(2131624892)
    View secondRowDouble;
    @BindView(2131624905)
    View secondRowSingle;
    @BindView(2131624864)
    TextView skip;
    @BindView(2131624872)
    FrameLayout spinny;
    @BindView(2131624866)
    TextView subheader;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public String f6747t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public String f6748u;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public String f6749v;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public String f6750w;
    /* access modifiers changed from: private */

    /* renamed from: x */
    public String f6751x;
    /* access modifiers changed from: private */

    /* renamed from: y */
    public String f6752y;
    /* access modifiers changed from: private */

    /* renamed from: z */
    public String f6753z;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9332a(Context context) {
        startActivity(new Intent(context, CommunitySettingsActivity.class));
        if (this.f6733J.mo13771e().size() != this.f6743h.size()) {
            this.f6733J.mo13769c();
            this.f6733J.mo13764a(this.f6743h);
        }
        this.f6734K = null;
        if (this.f6733J != null && this.f6733J.mo13771e() != null) {
            for (int i = 0; i < this.f6733J.mo13771e().size(); i++) {
                if (((Group) this.f6733J.mo13771e().get(i)).filterEnabled) {
                    ((Group) this.f6733J.mo13771e().get(i)).filterEnabled = false;
                    this.f6733J.notifyItemChanged(i);
                }
            }
            this.f10258o.setVisibility(0);
            this.communityFeedEmpty.setVisibility(8);
            mo13890a(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9335a(String str) {
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        if (str != null) {
            arrayList.add(str);
        }
        hashMap.put("group_ids", bid.m8220a(arrayList.toArray(), ","));
        ApiService.m14297a().mo14179x(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        });
        AppState.setCommunity = 1;
        mo10348a();
        mo13890a(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9336a(boolean z, int i) {
        if (!z) {
            this.f6733J.mo13769c();
            this.f6733J.mo13764a(this.f6743h);
            this.f6734K.unsubscribe();
            this.f6734K = null;
            this.f10258o.setVisibility(0);
            this.communityFeedEmpty.setVisibility(8);
            mo13890a(0);
        } else if (this.f6733J != null && this.f6733J.mo13771e() != null) {
            Group group = (Group) this.f6733J.mo13771e().get(i);
            this.f6743h.clear();
            this.f6743h.addAll(this.f6733J.mo13771e());
            if (this.f10259p != null) {
                this.f10259p.setRefreshing(true);
            }
            this.f6735L = ((Group) this.f6733J.mo13771e().get(i)).group_id;
            if (this.f6734K != null) {
                this.f6734K.unsubscribe();
            }
            this.f6734K = ApiService.m14297a().mo14097a(this.f6735L, (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9354j());
            this.f6733J.mo13769c();
            this.f6733J.mo10285a(group);
        }
    }

    /* renamed from: j */
    private bjy<NetworkData> m9354j() {
        return new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                CommunityFeedFragment.this.f10259p.setRefreshing(false);
                CommunityFeedFragment.this.f10260q.setVisibility(8);
                if (networkData == null) {
                    return;
                }
                if (!networkData.success) {
                    String str = "Unable to find that group.";
                    if (networkData.error != null) {
                        str = networkData.error;
                    }
                    Toast.makeText(CommunityFeedFragment.this.getActivity(), str, 0).show();
                } else if (networkData.posts == null || networkData.posts.size() <= 0) {
                    CommunityFeedFragment.this.communityFeedEmpty.setVisibility(0);
                    CommunityFeedFragment.this.f10258o.setVisibility(8);
                } else {
                    CommunityFeedFragment.this.communityFeedEmpty.setVisibility(8);
                    CommunityFeedFragment.this.f10258o.setVisibility(0);
                    List<Post> list = networkData.posts;
                    CommunityFeedFragment.this.f10255l.mo13764a(list);
                    CommunityFeedFragment.this.f10255l.mo13763a(Integer.toString(((Post) list.get(list.size() - 1)).post_id));
                    CommunityFeedFragment.this.f10258o.scrollToPosition(0);
                    CommunityFeedFragment.this.f10258o.setVisibility(0);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                CommunityFeedFragment.this.f10260q.setVisibility(8);
                CommunityFeedFragment.this.f10259p.setRefreshing(false);
            }
        };
    }

    /* renamed from: k */
    private bjz m9357k() {
        return RxBus.m14573a().mo14348a(C2635ax.class, this.f6744i);
    }

    /* renamed from: l */
    private bjz m9359l() {
        return RxBus.m14573a().mo14348a(C2659p.class, this.f6745j);
    }

    /* renamed from: m */
    private void m9361m() {
        this.header.setText(AppState.config.getString("community_header", getString(R.string.community_header)));
        this.subheader.setText(AppState.config.getString("community_subheader", getString(R.string.community_subheader)));
    }

    /* renamed from: a */
    public void mo10348a() {
        this.communityFeedContent.setVisibility(0);
        this.communityFeedOnboarding.setVisibility(8);
        this.communityFeedOnboardingSimple.setVisibility(8);
        if (AppState.groups != null) {
            for (Group group : AppState.groups) {
                if (group.community_membership == 1 && group.group_type != null && this.f6743h != null && !this.f6743h.contains(group)) {
                    this.f6743h.add(group);
                }
            }
        }
        this.communityFeedTagRecycler.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
        this.f6733J = new CommunityFilterAdapter(getContext());
        this.f6733J.mo13764a(this.f6743h);
        this.communityFeedTagRecycler.setAdapter(this.f6733J);
        this.communityFeedTagRecycler.addOnItemTouchListener(new RecyclerItemClickListener(getContext()) {
            public void onItemClick(View view, int i) {
                if (i >= 0) {
                    for (int i2 = 0; i2 < CommunityFeedFragment.this.f6733J.mo13771e().size(); i2++) {
                        if (i2 == i) {
                            ((Group) CommunityFeedFragment.this.f6733J.mo13771e().get(i2)).filterEnabled = !((Group) CommunityFeedFragment.this.f6733J.mo13771e().get(i2)).filterEnabled;
                        } else {
                            ((Group) CommunityFeedFragment.this.f6733J.mo13771e().get(i2)).filterEnabled = false;
                        }
                    }
                    CommunityFeedFragment.this.f6733J.notifyDataSetChanged();
                    CommunityFeedFragment.this.m9336a(((Group) CommunityFeedFragment.this.f6733J.mo13771e().get(i)).filterEnabled, i);
                }
            }
        });
        this.communityFeedAdd.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CommunityFeedFragment.this.m9332a(view.getContext());
            }
        });
        if (this.f6743h.size() > 0) {
            this.communityFeedFilterContainer.setVisibility(0);
        } else {
            this.communityFeedFilterContainer.setVisibility(8);
        }
    }

    /* renamed from: a */
    public void mo10349a(View view) {
        this.communityFeedContent.setVisibility(8);
        this.communityFeedOnboarding.setVisibility(0);
        m9361m();
        if (AppState.config.getInt("enable_community_does_not_apply_button", 0) == 1) {
            this.f6736a = (RelativeLayout) view.findViewById(R.id.community_add_work);
            this.f6737b = (RelativeLayout) view.findViewById(R.id.community_work_default);
            this.f6738c = (RelativeLayout) view.findViewById(R.id.community_work_selected);
            this.f6739d = (TextView) view.findViewById(R.id.community_work_num_members);
            this.f6740e = (ImageView) view.findViewById(R.id.community_work_img_selected);
            this.f6741f = (TextView) view.findViewById(R.id.community_work_name);
            this.f6742g = (TextView) view.findViewById(R.id.community_work_text);
            this.secondRowDouble.setVisibility(0);
            this.secondRowSingle.setVisibility(8);
        } else {
            this.f6736a = (RelativeLayout) view.findViewById(R.id.community_add_work_single);
            this.f6737b = (RelativeLayout) view.findViewById(R.id.community_work_default_single);
            this.f6738c = (RelativeLayout) view.findViewById(R.id.community_work_selected_single);
            this.f6739d = (TextView) view.findViewById(R.id.community_work_num_members_single);
            this.f6740e = (ImageView) view.findViewById(R.id.community_work_img_selected_single);
            this.f6741f = (TextView) view.findViewById(R.id.community_work_name_single);
            this.f6742g = (TextView) view.findViewById(R.id.community_work_text_single);
            this.secondRowDouble.setVisibility(8);
            this.secondRowSingle.setVisibility(0);
        }
        if (AppState.config.getString("community_high_school_button") != null) {
            this.highSchoolText.setText(AppState.config.getString("community_high_school_button"));
        }
        if (AppState.config.getString("community_college_button") != null) {
            this.collegeText.setText(AppState.config.getString("community_college_button"));
        }
        if (AppState.config.getString("community_company_button") != null) {
            this.f6742g.setText(AppState.config.getString("community_company_button"));
        }
        this.skip.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AppState.setCommunity = 1;
                CommunityFeedFragment.this.mo10348a();
                CommunityFeedFragment.this.mo13890a(0);
            }
        });
        this.button.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CommunityFeedFragment.this.mo10351c();
            }
        });
        this.addHighSchool.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (CommunityFeedFragment.this.addHighSchool.isSelected()) {
                    CommunityFeedFragment.this.addHighSchool.setSelected(false);
                    CommunityFeedFragment.this.highSchoolDefault.setVisibility(0);
                    CommunityFeedFragment.this.highSchoolSelected.setVisibility(8);
                } else {
                    Intent intent = new Intent(CommunityFeedFragment.this.getActivity(), CommunitySearchActivity.class);
                    intent.putExtra("queryType", "school");
                    intent.putExtra("searchViewHint", AppState.config.getString("community_search_high_school_placeholder", "ex. Mill High School"));
                    if (CommunityFeedFragment.this.f6747t != null) {
                        intent.putExtra("queryString", CommunityFeedFragment.this.f6747t);
                    }
                    CommunityFeedFragment.this.startActivityForResult(intent, CommunitySettingsActivity.REQUEST_HIGHSCHOOL);
                }
                CommunityFeedFragment.this.mo10350b();
            }
        });
        this.addCollege.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (CommunityFeedFragment.this.addCollege.isSelected()) {
                    CommunityFeedFragment.this.addCollege.setSelected(false);
                    CommunityFeedFragment.this.collegeDefault.setVisibility(0);
                    CommunityFeedFragment.this.collegeSelected.setVisibility(8);
                } else {
                    Intent intent = new Intent(CommunityFeedFragment.this.getActivity(), CommunitySearchActivity.class);
                    intent.putExtra("queryType", "college");
                    intent.putExtra("searchViewHint", AppState.config.getString("community_search_college_placeholder", "ex. University of San Francisco"));
                    if (CommunityFeedFragment.this.f6751x != null) {
                        intent.putExtra("queryString", CommunityFeedFragment.this.f6751x);
                    }
                    CommunityFeedFragment.this.startActivityForResult(intent, CommunitySettingsActivity.REQUEST_COLLEGE);
                }
                CommunityFeedFragment.this.mo10350b();
            }
        });
        this.f6736a.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (CommunityFeedFragment.this.f6736a.isSelected()) {
                    CommunityFeedFragment.this.f6736a.setSelected(false);
                    CommunityFeedFragment.this.f6737b.setVisibility(0);
                    CommunityFeedFragment.this.f6738c.setVisibility(8);
                } else {
                    Intent intent = new Intent(CommunityFeedFragment.this.getActivity(), CommunitySearchActivity.class);
                    intent.putExtra("queryType", "company");
                    intent.putExtra("searchViewHint", AppState.config.getString("community_search_work_placeholder", "ex. MyLikes"));
                    if (CommunityFeedFragment.this.f6726B != null) {
                        intent.putExtra("queryString", CommunityFeedFragment.this.f6726B);
                    }
                    CommunityFeedFragment.this.startActivityForResult(intent, 203);
                }
                CommunityFeedFragment.this.mo10350b();
            }
        });
        this.addNone.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (CommunityFeedFragment.this.addNone.isSelected()) {
                    CommunityFeedFragment.this.addNone.setSelected(false);
                    CommunityFeedFragment.this.noneCheck.setVisibility(8);
                } else {
                    CommunityFeedFragment.this.addNone.setSelected(true);
                    CommunityFeedFragment.this.noneCheck.setVisibility(0);
                    if (CommunityFeedFragment.this.addHighSchool.isSelected()) {
                        CommunityFeedFragment.this.addHighSchool.callOnClick();
                    }
                    if (CommunityFeedFragment.this.addCollege.isSelected()) {
                        CommunityFeedFragment.this.addCollege.callOnClick();
                    }
                    if (CommunityFeedFragment.this.f6736a.isSelected()) {
                        CommunityFeedFragment.this.f6736a.callOnClick();
                    }
                }
                CommunityFeedFragment.this.mo10350b();
            }
        });
    }

    /* renamed from: b */
    public void mo10350b() {
        if (this.addHighSchool.isSelected() || this.addCollege.isSelected() || this.f6736a.isSelected() || this.addNone.isSelected()) {
            this.button.setEnabled(true);
        } else {
            this.button.setEnabled(false);
        }
    }

    /* renamed from: c */
    public void mo10351c() {
        if (AppState.groups == null) {
            AppState.groups = new ArrayList();
        }
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        Group group = new Group();
        if (this.addHighSchool.isSelected() && this.f6748u != null) {
            arrayList.add(this.f6748u);
            group.group_id = Integer.parseInt(this.f6748u);
            group.group_name = this.f6747t;
            group.community_membership = 1;
            group.group_type = "school";
            group.num_members = Integer.parseInt(this.f6749v);
            group.thumb_url = this.f6750w;
            AppState.groups.add(group);
        }
        if (this.addCollege.isSelected() && this.f6752y != null) {
            Group group2 = new Group();
            arrayList.add(this.f6752y);
            group2.group_id = Integer.parseInt(this.f6752y);
            group2.group_name = this.f6751x;
            group2.community_membership = 1;
            group2.group_type = "college";
            group2.num_members = Integer.parseInt(this.f6753z);
            group2.thumb_url = this.f6725A;
            AppState.groups.add(group2);
        }
        if (this.f6736a.isSelected() && this.f6727C != null) {
            Group group3 = new Group();
            arrayList.add(this.f6727C);
            group3.group_id = Integer.parseInt(this.f6727C);
            group3.group_name = this.f6726B;
            group3.community_membership = 1;
            group3.group_type = "company";
            group3.num_members = Integer.parseInt(this.f6728D);
            group3.thumb_url = this.f6729E;
            AppState.groups.add(group3);
        }
        hashMap.put("group_ids", bid.m8220a(arrayList.toArray(), ","));
        ApiService.m14297a().mo14179x(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        });
        AppState.setCommunity = 1;
        mo10348a();
        mo13890a(0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo10352d() {
        if (this.f6734K != null) {
            this.f6734K.unsubscribe();
            this.f6734K = ApiService.m14297a().mo14097a(this.f6735L, (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9354j());
            return;
        }
        mo13890a(0);
    }

    /* renamed from: e */
    public void mo10353e() {
        mo10351c();
        this.communityFeedOnboardingSimple.setVisibility(8);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            Bundle extras = intent.getExtras();
            switch (i) {
                case CommunitySettingsActivity.REQUEST_HIGHSCHOOL /*201*/:
                    if (extras.containsKey("queryString") && extras.containsKey("queryId") && extras.containsKey("num_members") && extras.containsKey("thumb_url")) {
                        this.f6747t = extras.getString("queryString");
                        this.f6748u = extras.getString("queryId");
                        this.f6749v = extras.getString("num_members");
                        this.f6750w = extras.getString("thumb_url");
                        this.addHighSchool.setSelected(true);
                        this.highSchoolDefault.setVisibility(8);
                        this.highSchoolSelected.setVisibility(0);
                        this.highSchoolMembers.setText(this.f6749v + " Members");
                        this.highSchoolImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                CommunityFeedFragment.this.highSchoolImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(CommunityFeedFragment.this.f6750w).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(CommunityFeedFragment.this.getContext(), CommunityFeedFragment.this.highSchoolImgSelected.getMeasuredWidth(), CommunityFeedFragment.this.highSchoolImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(CommunityFeedFragment.this.highSchoolImgSelected);
                            }
                        });
                        this.highSchoolSelectedName.setText(this.f6747t);
                        if (this.addNone.isSelected()) {
                            this.addNone.callOnClick();
                            break;
                        }
                    }
                    break;
                case CommunitySettingsActivity.REQUEST_COLLEGE /*202*/:
                    if (extras.containsKey("queryString") && extras.containsKey("queryId") && extras.containsKey("num_members") && extras.containsKey("thumb_url")) {
                        this.f6751x = extras.getString("queryString");
                        this.f6752y = extras.getString("queryId");
                        this.f6753z = extras.getString("num_members");
                        this.f6725A = extras.getString("thumb_url");
                        this.addCollege.setSelected(true);
                        this.collegeDefault.setVisibility(8);
                        this.collegeSelected.setVisibility(0);
                        this.collegeMembers.setText(this.f6753z + " Members");
                        this.collegeImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                CommunityFeedFragment.this.collegeImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(CommunityFeedFragment.this.f6725A).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(CommunityFeedFragment.this.getContext(), CommunityFeedFragment.this.collegeImgSelected.getMeasuredWidth(), CommunityFeedFragment.this.collegeImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(CommunityFeedFragment.this.collegeImgSelected);
                            }
                        });
                        this.collegeSelectedName.setText(this.f6751x);
                        if (this.addNone.isSelected()) {
                            this.addNone.callOnClick();
                            break;
                        }
                    }
                    break;
                case 203:
                    if (extras.containsKey("queryString") && extras.containsKey("queryId") && extras.containsKey("num_members") && extras.containsKey("thumb_url")) {
                        this.f6726B = extras.getString("queryString");
                        this.f6727C = extras.getString("queryId");
                        this.f6728D = extras.getString("num_members");
                        this.f6729E = extras.getString("thumb_url");
                        this.f6736a.setSelected(true);
                        this.f6737b.setVisibility(8);
                        this.f6738c.setVisibility(0);
                        this.f6739d.setText(this.f6728D + " Members");
                        this.f6740e.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                            public void onGlobalLayout() {
                                CommunityFeedFragment.this.f6740e.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                GossipApplication.f6095c.mo14650a(CommunityFeedFragment.this.f6729E).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(CommunityFeedFragment.this.getContext(), CommunityFeedFragment.this.f6740e.getMeasuredWidth(), CommunityFeedFragment.this.f6740e.getMeasuredHeight(), CropType.CENTER)}).mo14554a(CommunityFeedFragment.this.f6740e);
                            }
                        });
                        this.f6741f.setText(this.f6726B);
                        if (this.addNone.isSelected()) {
                            this.addNone.callOnClick();
                            break;
                        }
                    }
                    break;
            }
            mo10350b();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f6746s = ButterKnife.bind((Object) this, onCreateView);
        this.f6732I = new RoundedCornersTransformation(getContext(), (int) (10.0f * getContext().getResources().getDisplayMetrics().density), 0, CornerType.TOP);
        if (AppState.groups != null) {
            Iterator it = AppState.groups.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((Group) it.next()).community_membership == 1) {
                        AppState.setCommunity = 1;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        this.communityFeedOnboardingSimple.setOnSkipClick(this);
        if (AppState.setCommunity == 1) {
            mo10348a();
        } else {
            ApiService.m14297a().mo14135d().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                /* renamed from: a */
                public void onNext(NetworkData networkData) {
                    if (networkData == null || networkData.groups == null || networkData.groups.isEmpty()) {
                        CommunityFeedFragment.this.communityFeedOnboarding.setVisibility(8);
                        CommunityFeedFragment.this.communityFeedOnboardingSimple.setVisibility(0);
                        return;
                    }
                    CommunityFeedFragment.this.communityFeedOnboarding.setVisibility(0);
                    CommunityFeedFragment.this.communityFeedOnboardingSimple.setVisibility(8);
                    for (Group group : networkData.groups) {
                        String str = group.group_type;
                        char c = 65535;
                        switch (str.hashCode()) {
                            case -907977868:
                                if (str.equals("school")) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case 949445015:
                                if (str.equals("college")) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 950484093:
                                if (str.equals("company")) {
                                    c = 2;
                                    break;
                                }
                                break;
                        }
                        switch (c) {
                            case 0:
                                CommunityFeedFragment.this.f6747t = group.group_name;
                                CommunityFeedFragment.this.f6749v = Integer.toString(group.num_members);
                                CommunityFeedFragment.this.f6750w = group.thumb_url != null ? group.thumb_url : group.source_url;
                                CommunityFeedFragment.this.f6748u = Integer.toString(group.group_id);
                                CommunityFeedFragment.this.addHighSchool.setSelected(true);
                                CommunityFeedFragment.this.highSchoolDefault.setVisibility(8);
                                CommunityFeedFragment.this.highSchoolSelected.setVisibility(0);
                                CommunityFeedFragment.this.highSchoolMembers.setText(CommunityFeedFragment.this.f6749v + " Members");
                                CommunityFeedFragment.this.highSchoolImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                                    public void onGlobalLayout() {
                                        CommunityFeedFragment.this.highSchoolImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                        GossipApplication.f6095c.mo14650a(CommunityFeedFragment.this.f6750w).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(CommunityFeedFragment.this.getContext(), CommunityFeedFragment.this.highSchoolImgSelected.getMeasuredWidth(), CommunityFeedFragment.this.highSchoolImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(CommunityFeedFragment.this.highSchoolImgSelected);
                                    }
                                });
                                CommunityFeedFragment.this.highSchoolSelectedName.setText(CommunityFeedFragment.this.f6747t);
                                break;
                            case 1:
                                CommunityFeedFragment.this.f6751x = group.group_name;
                                CommunityFeedFragment.this.f6753z = Integer.toString(group.num_members);
                                CommunityFeedFragment.this.f6725A = group.thumb_url != null ? group.thumb_url : group.source_url;
                                CommunityFeedFragment.this.f6752y = Integer.toString(group.group_id);
                                CommunityFeedFragment.this.addCollege.setSelected(true);
                                CommunityFeedFragment.this.collegeDefault.setVisibility(8);
                                CommunityFeedFragment.this.collegeSelected.setVisibility(0);
                                CommunityFeedFragment.this.collegeMembers.setText(CommunityFeedFragment.this.f6753z + " Members");
                                CommunityFeedFragment.this.collegeImgSelected.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                                    public void onGlobalLayout() {
                                        CommunityFeedFragment.this.collegeImgSelected.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                        GossipApplication.f6095c.mo14650a(CommunityFeedFragment.this.f6725A).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(CommunityFeedFragment.this.getContext(), CommunityFeedFragment.this.collegeImgSelected.getMeasuredWidth(), CommunityFeedFragment.this.collegeImgSelected.getMeasuredHeight(), CropType.CENTER)}).mo14554a(CommunityFeedFragment.this.collegeImgSelected);
                                    }
                                });
                                CommunityFeedFragment.this.collegeSelectedName.setText(CommunityFeedFragment.this.f6751x);
                                break;
                            case 2:
                                CommunityFeedFragment.this.f6726B = group.group_name;
                                CommunityFeedFragment.this.f6728D = Integer.toString(group.num_members);
                                CommunityFeedFragment.this.f6729E = group.thumb_url != null ? group.thumb_url : group.source_url;
                                CommunityFeedFragment.this.f6727C = Integer.toString(group.group_id);
                                CommunityFeedFragment.this.f6736a.setSelected(true);
                                CommunityFeedFragment.this.f6737b.setVisibility(8);
                                CommunityFeedFragment.this.f6738c.setVisibility(0);
                                CommunityFeedFragment.this.f6739d.setText(CommunityFeedFragment.this.f6728D + " Members");
                                CommunityFeedFragment.this.f6740e.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                                    public void onGlobalLayout() {
                                        CommunityFeedFragment.this.f6740e.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                        GossipApplication.f6095c.mo14650a(CommunityFeedFragment.this.f6729E).mo14572d(17301613).mo14590a((Transformation<Bitmap>[]) new Transformation[]{new CropTransformation(CommunityFeedFragment.this.getContext(), CommunityFeedFragment.this.f6740e.getMeasuredWidth(), CommunityFeedFragment.this.f6740e.getMeasuredHeight(), CropType.CENTER)}).mo14554a(CommunityFeedFragment.this.f6740e);
                                    }
                                });
                                CommunityFeedFragment.this.f6741f.setText(CommunityFeedFragment.this.f6726B);
                                break;
                        }
                        CommunityFeedFragment.this.mo10350b();
                    }
                }

                public void onCompleted() {
                }

                public void onError(Throwable th) {
                }
            });
            mo10349a(onCreateView);
        }
        this.f6730G = m9357k();
        this.f6731H = m9359l();
        this.f10259p.setOnRefreshListener(new OnRefreshListener() {
            public void onRefresh() {
                CommunityFeedFragment.this.mo10352d();
            }
        });
        BaseActivity baseActivity = (BaseActivity) getActivity();
        baseActivity.addToSubscriptionList(RxBus.m14573a().mo14348a(C2661r.class, (bjy<T>) new bjy<C2661r>() {
            /* renamed from: a */
            public void onNext(C2661r rVar) {
                for (int i = 0; i < CommunityFeedFragment.this.f6743h.size(); i++) {
                    if (rVar.f10656a == ((Group) CommunityFeedFragment.this.f6743h.get(i)).group_id) {
                        CommunityFeedFragment.this.f6743h.remove(i);
                        CommunityFeedFragment.this.f6733J.mo13771e().remove(i);
                        CommunityFeedFragment.this.f6733J.notifyItemRemoved(i);
                        CommunityFeedFragment.this.mo13890a(0);
                        if (CommunityFeedFragment.this.f6743h.size() == 0) {
                            CommunityFeedFragment.this.communityFeedFilterContainer.setVisibility(8);
                            return;
                        }
                        return;
                    }
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        baseActivity.addToSubscriptionList(RxBus.m14573a().mo14348a(C2652i.class, (bjy<T>) new bjy<C2652i>() {
            /* renamed from: a */
            public void onNext(C2652i iVar) {
                if (iVar != null) {
                    CommunityFeedFragment.this.communityFeedOnboardingSimple.setVisibility(8);
                    CommunityFeedFragment.this.m9335a(iVar.f10641a);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        return onCreateView;
    }

    public void onDestroy() {
        if (this.f6730G != null) {
            this.f6730G.unsubscribe();
        }
        if (this.f6731H != null) {
            this.f6731H.unsubscribe();
        }
        if (this.f6734K != null) {
            this.f6734K.unsubscribe();
        }
        super.onDestroy();
    }

    public void onLoadMore(String str) {
        if (this.f6734K == null) {
            super.onLoadMore(str);
        } else if (str != null && Integer.valueOf(str).intValue() > 0) {
            ApiService.m14297a().mo14097a(this.f6735L, str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                /* renamed from: a */
                public void onNext(NetworkData networkData) {
                    if (networkData.posts == null || networkData.posts.isEmpty()) {
                        CommunityFeedFragment.this.f10255l.mo13763a((String) null);
                        return;
                    }
                    List<Post> list = networkData.posts;
                    CommunityFeedFragment.this.f10255l.mo13768b(list);
                    if (!list.isEmpty()) {
                        CommunityFeedFragment.this.f10255l.mo13763a(Integer.toString(((Post) list.get(list.size() - 1)).post_id));
                    }
                }

                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    C2699jj.m14614a(CommunityFeedFragment.this.communityFeedTagRecycler, CommunityFeedFragment.this.f10255l);
                }
            });
        }
    }

    public void onResume() {
        super.onResume();
        if (this.f10261r != 0 && (System.currentTimeMillis() - this.f10261r) / 60000 > ((long) AppState.config.getInt("refresh_timeout", 5))) {
            if (!(this.f6733J == null || this.f6733J.mo13771e() == null || this.f6733J.mo13771e().size() == this.f6743h.size())) {
                this.f6733J.mo13769c();
                this.f6733J.mo13764a(this.f6743h);
            }
            if (this.f6733J != null && this.f6733J.mo13771e() != null) {
                for (int i = 0; i < this.f6733J.mo13771e().size(); i++) {
                    if (((Group) this.f6733J.mo13771e().get(i)).filterEnabled) {
                        ((Group) this.f6733J.mo13771e().get(i)).filterEnabled = false;
                        this.f6733J.notifyItemChanged(i);
                    }
                }
                this.f6734K = null;
                mo13890a(0);
            }
        }
    }
}
