package com.becandid.candid.fragments.main_tabs;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.main_tabs.MeTabFragment;
import com.becandid.candid.views.TabViewPager;

public class MeTabFragment$$ViewBinder<T extends MeTabFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.main_tabs.MeTabFragment$$ViewBinder$a */
    /* compiled from: MeTabFragment$$ViewBinder */
    public static class C1678a<T extends MeTabFragment> implements Unbinder {

        /* renamed from: a */
        View f6977a;

        /* renamed from: b */
        View f6978b;

        /* renamed from: c */
        private T f6979c;

        protected C1678a(T t) {
            this.f6979c = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10531a(T t) {
            t.mPostsCount = null;
            t.mGroupsCount = null;
            t.mNumContacts = null;
            t.mNumFriends = null;
            this.f6977a.setOnClickListener(null);
            t.connectFb = null;
            this.f6978b.setOnClickListener(null);
            t.connectContact = null;
            t.mBadgeIcon = null;
            t.mBadgeTitle = null;
            t.buttonPanel = null;
            t.mBadgeInfoContainer = null;
            t.mProfileContainer = null;
            t.viewPager = null;
            t.mQualityScore = null;
            t.mProfileSpinny = null;
        }

        public final void unbind() {
            if (this.f6979c == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10531a(this.f6979c);
            this.f6979c = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, final T t, Object obj) {
        C1678a a = mo10530a(t);
        t.mPostsCount = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_stat, "field 'mPostsCount'"), R.id.post_stat, "field 'mPostsCount'");
        t.mGroupsCount = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_stat, "field 'mGroupsCount'"), R.id.group_stat, "field 'mGroupsCount'");
        t.mNumContacts = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_stat, "field 'mNumContacts'"), R.id.contacts_stat, "field 'mNumContacts'");
        t.mNumFriends = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fb_stat, "field 'mNumFriends'"), R.id.fb_stat, "field 'mNumFriends'");
        View view = (View) finder.findRequiredView(obj, R.id.profile_connect_fb, "field 'connectFb' and method 'connectFB'");
        t.connectFb = view;
        a.f6977a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.connectFB(view);
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.profile_connect_contact, "field 'connectContact' and method 'connectContacts'");
        t.connectContact = view2;
        a.f6978b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.connectContacts(view);
            }
        });
        t.mBadgeIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_icon, "field 'mBadgeIcon'"), R.id.badge_icon, "field 'mBadgeIcon'");
        t.mBadgeTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.badge_title, "field 'mBadgeTitle'"), R.id.badge_title, "field 'mBadgeTitle'");
        t.buttonPanel = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.button_panel, "field 'buttonPanel'"), R.id.button_panel, "field 'buttonPanel'");
        t.mBadgeInfoContainer = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.badge_info_container, "field 'mBadgeInfoContainer'"), R.id.badge_info_container, "field 'mBadgeInfoContainer'");
        t.mProfileContainer = (View) finder.findRequiredView(obj, R.id.profile_container, "field 'mProfileContainer'");
        t.viewPager = (TabViewPager) finder.castView((View) finder.findRequiredView(obj, R.id.me_view_pager, "field 'viewPager'"), R.id.me_view_pager, "field 'viewPager'");
        t.mQualityScore = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.me_score, "field 'mQualityScore'"), R.id.me_score, "field 'mQualityScore'");
        t.mProfileSpinny = (View) finder.findRequiredView(obj, R.id.profile_spinny, "field 'mProfileSpinny'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1678a<T> mo10530a(T t) {
        return new C1678a<>(t);
    }
}
