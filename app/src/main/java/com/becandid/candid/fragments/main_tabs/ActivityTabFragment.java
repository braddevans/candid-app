package com.becandid.candid.fragments.main_tabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.widget.SwipeRefreshLayout;
import android.support.p001v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.NotificationSettingsActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.NetworkData;
import p012rx.schedulers.Schedulers;

public class ActivityTabFragment extends BaseMainTabFragment implements OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: d */
    public ActivityAdapter f6913d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public bjz f6914e;

    /* renamed from: f */
    private long f6915f;
    @BindView(2131624363)
    View mEmptyView;
    @BindView(2131624362)
    RecyclerView mRecyclerView;
    @BindView(2131624360)
    View mSettingView;
    @BindView(2131624361)
    SwipeRefreshLayout mSwipeRefresh;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public bjz m9477a(boolean z) {
        int i = 1;
        this.mSwipeRefresh.setRefreshing(true);
        this.f6915f = System.currentTimeMillis();
        if (!z) {
            i = 0;
        }
        return ApiService.m14297a().mo14120b(Integer.toString(i), (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9480d());
    }

    /* renamed from: d */
    private bjy m9480d() {
        return new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                ActivityTabFragment.this.f6913d.mo13764a(networkData.activity);
                ActivityTabFragment.this.f6913d.mo13763a(String.valueOf(networkData.last_activity_id));
                RxBus.m14573a().mo14349a(new C2637az(3, networkData.unread_count, true));
                RxBus.m14573a().mo14349a(new C2637az(1, networkData.unread_groups_count, true));
            }

            public void onCompleted() {
                ActivityTabFragment.this.mSwipeRefresh.setRefreshing(false);
                ActivityTabFragment.this.m9481e();
            }

            public void onError(Throwable th) {
                ActivityTabFragment.this.mSwipeRefresh.setRefreshing(false);
                ActivityTabFragment.this.m9481e();
                Crashlytics.m16437a(th);
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m9481e() {
        if (!this.f6913d.mo13770d()) {
            Log.d("Toggle", "empty");
            this.mRecyclerView.setVisibility(8);
            this.mEmptyView.setVisibility(0);
            return;
        }
        Log.d("Toggle", "not empty");
        this.mRecyclerView.setVisibility(0);
        this.mEmptyView.setVisibility(8);
    }

    /* renamed from: a */
    public void mo10479a() {
        super.mo10479a();
        this.mRecyclerView.scrollToPosition(0);
        if (this.f6914e != null) {
            this.f6914e.unsubscribe();
        }
        this.f6914e = m9477a(false);
        this.f6913d.mo13769c();
        this.f10276c = false;
    }

    /* renamed from: b */
    public void mo10480b() {
    }

    /* renamed from: c */
    public void mo10481c() {
    }

    public void onClick(View view) {
        if (view.getId() == R.id.activity_settings) {
            getContext().startActivity(new Intent(getContext(), NotificationSettingsActivity.class));
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.activity_tab, viewGroup, false);
        this.f10275b = ButterKnife.bind((Object) this, inflate);
        this.f10274a = getContext();
        bhw.m8184a(this.f10274a);
        this.mSettingView.setOnClickListener(this);
        this.mSwipeRefresh.setColorSchemeColors(R.color.gossip);
        this.mSwipeRefresh.setOnRefreshListener(new OnRefreshListener() {
            public void onRefresh() {
                if (ActivityTabFragment.this.f6914e != null) {
                    ActivityTabFragment.this.f6914e.unsubscribe();
                }
                ActivityTabFragment.this.f6914e = ActivityTabFragment.this.m9477a(true);
                ActivityTabFragment.this.f6913d.mo13769c();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.f10274a);
        this.mRecyclerView.setLayoutManager(linearLayoutManager);
        this.mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this.f10274a) {
            /* JADX WARNING: Code restructure failed: missing block: B:14:0x00af, code lost:
                if (r7.equals("showPost") != false) goto L_0x007c;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onItemClick(android.view.View r10, int r11) {
                /*
                    r9 = this;
                    r6 = 1
                    r4 = 0
                    if (r11 < 0) goto L_0x0014
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r5 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    hb r5 = r5.f6913d
                    java.util.List r5 = r5.mo13771e()
                    int r5 = r5.size()
                    if (r11 < r5) goto L_0x0022
                L_0x0014:
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r5 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    android.content.Context r5 = r5.f10274a
                    java.lang.String r6 = "Unable to find that notification"
                    android.widget.Toast r4 = android.widget.Toast.makeText(r5, r6, r4)
                    r4.show()
                L_0x0021:
                    return
                L_0x0022:
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r5 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    hb r5 = r5.f6913d
                    java.util.List r5 = r5.mo13771e()
                    java.lang.Object r0 = r5.get(r11)
                    com.becandid.candid.data.Notification r0 = (com.becandid.candid.data.Notification) r0
                    r0.unread = r4
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r5 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    hb r5 = r5.f6913d
                    r5.notifyItemChanged(r11)
                    ip r5 = p000.ApiService.m14297a()
                    int r7 = r0.activity_id
                    bjs r5 = r5.mo14116b(r7)
                    bjv r7 = p012rx.schedulers.Schedulers.m16145io()
                    bjs r5 = r5.mo9256b(r7)
                    bjv r7 = p000.bkc.m8469a()
                    bjs r5 = r5.mo9249a(r7)
                    com.becandid.candid.models.EmptySubscriber r7 = new com.becandid.candid.models.EmptySubscriber
                    r7.<init>()
                    r5.mo9258b(r7)
                    java.lang.String r5 = r0.onclick
                    java.lang.String r7 = ":"
                    java.lang.String[] r2 = r5.split(r7)
                    r5 = r2[r6]
                    java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                    int r3 = r5.intValue()
                    r7 = r2[r4]
                    r5 = -1
                    int r8 = r7.hashCode()
                    switch(r8) {
                        case -1925545598: goto L_0x00bc;
                        case -1572993592: goto L_0x00b2;
                        case -338943683: goto L_0x00a9;
                        case 2067279704: goto L_0x00c6;
                        default: goto L_0x007b;
                    }
                L_0x007b:
                    r4 = r5
                L_0x007c:
                    switch(r4) {
                        case 0: goto L_0x0080;
                        case 1: goto L_0x00d0;
                        case 2: goto L_0x00f5;
                        case 3: goto L_0x010e;
                        default: goto L_0x007f;
                    }
                L_0x007f:
                    goto L_0x0021
                L_0x0080:
                    android.content.Intent r1 = new android.content.Intent
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r4 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    android.content.Context r4 = r4.f10274a
                    java.lang.Class<com.becandid.candid.activities.PostDetailsActivity> r5 = com.becandid.candid.activities.PostDetailsActivity.class
                    r1.<init>(r4, r5)
                    java.lang.String r4 = "post_id"
                    r1.putExtra(r4, r3)
                    int r4 = r0.comment_id
                    if (r4 <= 0) goto L_0x00a0
                    java.lang.String r4 = "comment_id"
                    int r5 = r0.comment_id
                    r1.putExtra(r4, r5)
                    java.lang.String r4 = "scrollToBottom"
                    r1.putExtra(r4, r6)
                L_0x00a0:
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r4 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    android.content.Context r4 = r4.f10274a
                    r4.startActivity(r1)
                    goto L_0x0021
                L_0x00a9:
                    java.lang.String r8 = "showPost"
                    boolean r7 = r7.equals(r8)
                    if (r7 == 0) goto L_0x007b
                    goto L_0x007c
                L_0x00b2:
                    java.lang.String r4 = "showPostBottom"
                    boolean r4 = r7.equals(r4)
                    if (r4 == 0) goto L_0x007b
                    r4 = r6
                    goto L_0x007c
                L_0x00bc:
                    java.lang.String r4 = "showGroup"
                    boolean r4 = r7.equals(r4)
                    if (r4 == 0) goto L_0x007b
                    r4 = 2
                    goto L_0x007c
                L_0x00c6:
                    java.lang.String r4 = "showTab"
                    boolean r4 = r7.equals(r4)
                    if (r4 == 0) goto L_0x007b
                    r4 = 3
                    goto L_0x007c
                L_0x00d0:
                    android.content.Intent r1 = new android.content.Intent
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r4 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    android.content.Context r4 = r4.f10274a
                    java.lang.Class<com.becandid.candid.activities.PostDetailsActivity> r5 = com.becandid.candid.activities.PostDetailsActivity.class
                    r1.<init>(r4, r5)
                    java.lang.String r4 = "post_id"
                    r1.putExtra(r4, r3)
                    java.lang.String r4 = "comment_id"
                    int r5 = r0.comment_id
                    r1.putExtra(r4, r5)
                    java.lang.String r4 = "scrollToBottom"
                    r1.putExtra(r4, r6)
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r4 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    android.content.Context r4 = r4.f10274a
                    r4.startActivity(r1)
                    goto L_0x0021
                L_0x00f5:
                    android.content.Intent r1 = new android.content.Intent
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r4 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    android.content.Context r4 = r4.f10274a
                    java.lang.Class<com.becandid.candid.activities.GroupDetailsActivity> r5 = com.becandid.candid.activities.GroupDetailsActivity.class
                    r1.<init>(r4, r5)
                    java.lang.String r4 = "group_id"
                    r1.putExtra(r4, r3)
                    com.becandid.candid.fragments.main_tabs.ActivityTabFragment r4 = com.becandid.candid.fragments.main_tabs.ActivityTabFragment.this
                    android.content.Context r4 = r4.f10274a
                    r4.startActivity(r1)
                    goto L_0x0021
                L_0x010e:
                    jf r4 = p000.RxBus.m14573a()
                    iu$e r5 = new iu$e
                    r5.<init>(r3)
                    r4.mo14349a(r5)
                    goto L_0x0021
                */
                throw new UnsupportedOperationException("Method not decompiled: com.becandid.candid.fragments.main_tabs.ActivityTabFragment.C16472.onItemClick(android.view.View, int):void");
            }
        });
        this.f6913d = new ActivityAdapter(this.f10274a);
        this.mRecyclerView.setAdapter(this.f6913d);
        this.mRecyclerView.addOnScrollListener(new PagingScrollListener(linearLayoutManager, this.f6913d) {
            public void onLoadMore(String str) {
                if (str != null) {
                    ApiService.m14297a().mo14120b(Boolean.toString(true), str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                        /* renamed from: a */
                        public void onNext(NetworkData networkData) {
                            if (networkData == null || networkData.activity == null || networkData.activity.isEmpty()) {
                                ActivityTabFragment.this.f6913d.mo13763a((String) null);
                                return;
                            }
                            ActivityTabFragment.this.f6913d.mo13768b(networkData.activity);
                            ActivityTabFragment.this.f6913d.mo13763a(String.valueOf(networkData.last_activity_id));
                        }

                        public void onCompleted() {
                        }

                        public void onError(Throwable th) {
                        }
                    });
                }
            }
        });
        this.f6914e = m9477a(false);
        return inflate;
    }

    public void onResume() {
        super.onResume();
        if (AppState.account != null) {
            this.f6914e = m9477a(true);
        }
        bhw.m8184a(this.f10274a);
    }
}
