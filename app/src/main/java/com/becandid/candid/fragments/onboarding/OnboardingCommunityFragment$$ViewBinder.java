package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingCommunityFragment;

public class OnboardingCommunityFragment$$ViewBinder<T extends OnboardingCommunityFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingCommunityFragment$$ViewBinder$a */
    /* compiled from: OnboardingCommunityFragment$$ViewBinder */
    public static class C1711a<T extends OnboardingCommunityFragment> implements Unbinder {

        /* renamed from: a */
        private T f7046a;

        protected C1711a(T t) {
            this.f7046a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10581a(T t) {
            t.privacy = null;
            t.skip = null;
            t.button = null;
            t.spinny = null;
            t.subheader = null;
            t.info = null;
            t.header = null;
            t.addHighSchool = null;
            t.highSchoolDefault = null;
            t.highSchoolSelected = null;
            t.highSchoolMembers = null;
            t.highSchoolImgSelected = null;
            t.highSchoolSelectedName = null;
            t.highSchoolText = null;
            t.addCollege = null;
            t.collegeDefault = null;
            t.collegeSelected = null;
            t.collegeMembers = null;
            t.collegeImgSelected = null;
            t.collegeSelectedName = null;
            t.collegeText = null;
            t.addNone = null;
            t.noneText = null;
            t.noneCheck = null;
            t.secondRowDouble = null;
            t.secondRowSingle = null;
        }

        public final void unbind() {
            if (this.f7046a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10581a(this.f7046a);
            this.f7046a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1711a a = mo10580a(t);
        t.privacy = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_privacy, "field 'privacy'"), R.id.community_privacy, "field 'privacy'");
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_skip, "field 'skip'"), R.id.community_skip, "field 'skip'");
        t.button = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.community_button, "field 'button'"), R.id.community_button, "field 'button'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_spinny, "field 'spinny'"), R.id.community_spinny, "field 'spinny'");
        t.subheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_subheader, "field 'subheader'"), R.id.community_subheader, "field 'subheader'");
        t.info = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_info, "field 'info'"), R.id.community_info, "field 'info'");
        t.header = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_header, "field 'header'"), R.id.community_header, "field 'header'");
        t.addHighSchool = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_add_highschool, "field 'addHighSchool'"), R.id.community_add_highschool, "field 'addHighSchool'");
        t.highSchoolDefault = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_default, "field 'highSchoolDefault'"), R.id.community_highschool_default, "field 'highSchoolDefault'");
        t.highSchoolSelected = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_selected, "field 'highSchoolSelected'"), R.id.community_highschool_selected, "field 'highSchoolSelected'");
        t.highSchoolMembers = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_num_members, "field 'highSchoolMembers'"), R.id.community_highschool_num_members, "field 'highSchoolMembers'");
        t.highSchoolImgSelected = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_img_selected, "field 'highSchoolImgSelected'"), R.id.community_highschool_img_selected, "field 'highSchoolImgSelected'");
        t.highSchoolSelectedName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_name, "field 'highSchoolSelectedName'"), R.id.community_highschool_name, "field 'highSchoolSelectedName'");
        t.highSchoolText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_highschool_text, "field 'highSchoolText'"), R.id.community_highschool_text, "field 'highSchoolText'");
        t.addCollege = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_add_college, "field 'addCollege'"), R.id.community_add_college, "field 'addCollege'");
        t.collegeDefault = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_default, "field 'collegeDefault'"), R.id.community_college_default, "field 'collegeDefault'");
        t.collegeSelected = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_selected, "field 'collegeSelected'"), R.id.community_college_selected, "field 'collegeSelected'");
        t.collegeMembers = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_num_members, "field 'collegeMembers'"), R.id.community_college_num_members, "field 'collegeMembers'");
        t.collegeImgSelected = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_img_selected, "field 'collegeImgSelected'"), R.id.community_college_img_selected, "field 'collegeImgSelected'");
        t.collegeSelectedName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_name, "field 'collegeSelectedName'"), R.id.community_college_name, "field 'collegeSelectedName'");
        t.collegeText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_college_text, "field 'collegeText'"), R.id.community_college_text, "field 'collegeText'");
        t.addNone = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.community_add_none, "field 'addNone'"), R.id.community_add_none, "field 'addNone'");
        t.noneText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_none_text, "field 'noneText'"), R.id.community_none_text, "field 'noneText'");
        t.noneCheck = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.community_none_check, "field 'noneCheck'"), R.id.community_none_check, "field 'noneCheck'");
        t.secondRowDouble = (View) finder.findRequiredView(obj, R.id.second_row_double, "field 'secondRowDouble'");
        t.secondRowSingle = (View) finder.findRequiredView(obj, R.id.second_row_single, "field 'secondRowSingle'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1711a<T> mo10580a(T t) {
        return new C1711a<>(t);
    }
}
