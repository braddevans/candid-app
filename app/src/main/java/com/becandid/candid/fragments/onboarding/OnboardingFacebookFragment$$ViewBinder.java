package com.becandid.candid.fragments.onboarding;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingFacebookFragment;

public class OnboardingFacebookFragment$$ViewBinder<T extends OnboardingFacebookFragment> implements ViewBinder<T> {

    /* renamed from: com.becandid.candid.fragments.onboarding.OnboardingFacebookFragment$$ViewBinder$a */
    /* compiled from: OnboardingFacebookFragment$$ViewBinder */
    public static class C1734a<T extends OnboardingFacebookFragment> implements Unbinder {

        /* renamed from: a */
        private T f7091a;

        protected C1734a(T t) {
            this.f7091a = t;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10619a(T t) {
            t.fbPrivacy = null;
            t.skip = null;
            t.fbButton = null;
            t.spinny = null;
            t.fbInfo = null;
            t.fbInfo2 = null;
            t.fbSubheader = null;
            t.fbHeader = null;
        }

        public final void unbind() {
            if (this.f7091a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            mo10619a(this.f7091a);
            this.f7091a = null;
        }
    }

    /* renamed from: a */
    public Unbinder bind(Finder finder, T t, Object obj) {
        C1734a a = mo10618a(t);
        t.fbPrivacy = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fb_privacy, "field 'fbPrivacy'"), R.id.fb_privacy, "field 'fbPrivacy'");
        t.skip = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fb_skip, "field 'skip'"), R.id.fb_skip, "field 'skip'");
        t.fbButton = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fb_button_container, "field 'fbButton'"), R.id.fb_button_container, "field 'fbButton'");
        t.spinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fb_spinny, "field 'spinny'"), R.id.fb_spinny, "field 'spinny'");
        t.fbInfo = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fb_info, "field 'fbInfo'"), R.id.fb_info, "field 'fbInfo'");
        t.fbInfo2 = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fb_info_2, "field 'fbInfo2'"), R.id.fb_info_2, "field 'fbInfo2'");
        t.fbSubheader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fb_subheader, "field 'fbSubheader'"), R.id.fb_subheader, "field 'fbSubheader'");
        t.fbHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fb_header, "field 'fbHeader'"), R.id.fb_header, "field 'fbHeader'");
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C1734a<T> mo10618a(T t) {
        return new C1734a<>(t);
    }
}
