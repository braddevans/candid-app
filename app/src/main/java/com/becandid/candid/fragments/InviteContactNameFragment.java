package com.becandid.candid.fragments;

import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentTransaction;
import android.support.p003v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity;

public class InviteContactNameFragment extends Fragment {

    /* renamed from: a */
    private Unbinder f6825a;
    @BindView(2131624071)
    ImageView mBackNav;
    @BindView(2131624648)
    AppCompatEditText mInviteName;
    @BindView(2131624649)
    Button mSendText;

    public void backNavClick(View view) {
        getActivity().onBackPressed();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_invite_contact_name, viewGroup, false);
        this.f6825a = ButterKnife.bind((Object) this, inflate);
        this.mInviteName.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) {
                    InviteContactNameFragment.this.mSendText.setEnabled(false);
                } else {
                    InviteContactNameFragment.this.mSendText.setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        this.mSendText.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (InviteContactNameFragment.this.getActivity() instanceof InviteContactsActivity) {
                    ((InviteContactsActivity) InviteContactNameFragment.this.getActivity()).setSendInviteName(InviteContactNameFragment.this.mInviteName.getText().toString());
                    InviteContactSentFragment inviteContactSentFragment = new InviteContactSentFragment();
                    FragmentTransaction beginTransaction = InviteContactNameFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
                    beginTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
                    beginTransaction.replace(R.id.placeholder, inviteContactSentFragment, "SENT_FRAGMENT");
                    beginTransaction.addToBackStack(null);
                    beginTransaction.commit();
                }
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getWindow().setSoftInputMode(3);
    }

    public void onPause() {
        super.onPause();
        ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}
