package com.becandid.candid.fragments.main_tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.C0411a;
import android.support.design.widget.TabLayout.C0416d;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.MainTabsActivity;
import com.becandid.candid.activities.MessageSearchActivity;
import com.becandid.candid.activities.MessageSettingsActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Message;
import com.becandid.candid.data.MessageThread;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.views.TabViewPager;
import com.becandid.thirdparty.BlurTask;
import com.becandid.thirdparty.BlurTask.BadgeType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class MessageTabFragment extends BaseMainTabFragment {
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Context f6980d;

    /* renamed from: e */
    private BaseActivity f6981e;

    /* renamed from: f */
    private TabPagerAdapter f6982f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public MessagesActiveFragment f6983g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public MessagesThreadsFragment f6984h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public MessagesRequestsFragment f6985i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public bjz f6986j;
    @BindView(2131624768)
    View messagesContainer;
    @BindView(2131624769)
    TextView messagesSearch;
    @BindView(2131624770)
    ImageView messagesSettings;
    @BindView(2131624774)
    FrameLayout messagesSpinny;
    @BindView(2131624773)
    TextView messagingDisabled;
    @BindView(2131624771)
    TabLayout tabBar;
    @BindView(2131624772)
    TabViewPager viewPager;

    /* renamed from: a */
    public void mo10479a() {
        super.mo10479a();
        ((MainTabsActivity) this.f6980d).setShowMessagingPopup(false);
        this.f6986j = mo10535e();
    }

    /* renamed from: a */
    public void mo10532a(int i) {
        if (i == 1) {
            this.viewPager.setVisibility(8);
            this.messagingDisabled.setVisibility(0);
            this.messagesSearch.setEnabled(false);
            this.tabBar.setEnabled(false);
            return;
        }
        this.viewPager.setVisibility(0);
        this.messagingDisabled.setVisibility(8);
        this.messagesSearch.setEnabled(true);
        this.tabBar.setEnabled(true);
    }

    /* renamed from: a */
    public void mo10533a(int i, int i2, int i3) {
        if (this.tabBar != null) {
            int currentItem = this.viewPager.getCurrentItem();
            ((TextView) this.tabBar.mo1765a(0).mo1811a()).setCompoundDrawablesWithIntrinsicBounds(0, 0, (i <= 0 || currentItem == 0 || currentItem == 1) ? 0 : R.drawable.unread_dot, 0);
            ((TextView) this.tabBar.mo1765a(1).mo1811a()).setCompoundDrawablesWithIntrinsicBounds(0, 0, (i2 <= 0 || currentItem == 0 || currentItem == 1) ? 0 : R.drawable.unread_dot, 0);
            ((TextView) this.tabBar.mo1765a(2).mo1811a()).setCompoundDrawablesWithIntrinsicBounds(0, 0, (i3 <= 0 || currentItem == 2) ? 0 : R.drawable.unread_dot, 0);
        }
    }

    /* renamed from: b */
    public void mo10480b() {
        if (this.messagesContainer != null) {
            this.messagesContainer.setVisibility(0);
        }
    }

    /* renamed from: c */
    public void mo10481c() {
        this.messagesContainer.setVisibility(8);
    }

    /* renamed from: d */
    public void mo10534d() {
        final View findViewById = ((Activity) this.f6980d).findViewById(16908290);
        findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                findViewById.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                if (!AppState.hasMessagingShown) {
                    new BlurTask((Activity) MessageTabFragment.this.f6980d, ((Activity) MessageTabFragment.this.f6980d).findViewById(16908290), BadgeType.MESSAGE_ENABLED_TAB).execute(new Void[0]);
                    AppState.hasMessagingShown = true;
                    AppState.saveState(GossipApplication.m9012a());
                    return;
                }
                ((MainTabsActivity) MessageTabFragment.this.f6980d).setShowMessagingPopup(false);
            }
        });
    }

    /* renamed from: e */
    public bjz mo10535e() {
        HashMap hashMap = new HashMap();
        hashMap.put("include_messages", "1");
        return ApiService.m14297a().mo14171p(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            /* renamed from: a */
            public void onNext(NetworkData networkData) {
                if (networkData.threads != null) {
                    MessageTabFragment.this.f6984h.mo13955a((List) networkData.threads);
                    ArrayList arrayList = new ArrayList();
                    for (MessageThread messageThread : networkData.threads) {
                        if (messageThread.online == 1) {
                            arrayList.add(messageThread);
                        }
                    }
                    MessageTabFragment.this.f6983g.mo13955a((List) arrayList);
                    if (networkData.thread_next_page > 0) {
                        MessageTabFragment.this.f6984h.mo13954a(Integer.toString(networkData.thread_next_page));
                        MessageTabFragment.this.f6983g.mo13954a(Integer.toString(networkData.thread_next_page));
                    }
                }
                if (networkData.requests != null) {
                    MessageTabFragment.this.f6985i.mo13955a((List) networkData.requests);
                    if (networkData.request_next_page > 0) {
                        MessageTabFragment.this.f6985i.mo13954a(Integer.toString(networkData.request_next_page));
                    }
                }
                MessageTabFragment.this.mo10533a(0, networkData.new_threads, networkData.new_requests);
                RxBus.m14573a().mo14349a(new C2637az(2, networkData.new_threads + networkData.new_requests, true));
                MessageTabFragment.this.mo10536f();
            }

            public void onCompleted() {
                MessageTabFragment.this.f10276c = false;
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                MessageTabFragment.this.f10276c = false;
            }
        });
    }

    /* renamed from: f */
    public void mo10536f() {
        this.f6983g.mo13951a();
        this.f6984h.mo13951a();
        this.f6985i.mo13951a();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.messages_tab, viewGroup, false);
        this.f10275b = ButterKnife.bind((Object) this, inflate);
        this.f6981e = (BaseActivity) getActivity();
        this.f6980d = (BaseActivity) getActivity();
        this.f10274a = getContext();
        this.messagesSearch.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MessageTabFragment.this.f6980d.startActivity(new Intent(MessageTabFragment.this.f6980d, MessageSearchActivity.class));
            }
        });
        this.messagesSettings.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MessageTabFragment.this.f6980d.startActivity(new Intent(MessageTabFragment.this.f6980d, MessageSettingsActivity.class));
            }
        });
        this.f6982f = new TabPagerAdapter(this.f6981e.getSupportFragmentManager());
        this.f6983g = new MessagesActiveFragment();
        this.f6984h = new MessagesThreadsFragment();
        this.f6985i = new MessagesRequestsFragment();
        this.f6982f.mo13889a(this.f6983g, "Active");
        this.f6982f.mo13889a(this.f6984h, "Threads");
        this.f6982f.mo13889a(this.f6985i, "Requests");
        this.viewPager.setAdapter(this.f6982f);
        this.viewPager.setOffscreenPageLimit(2);
        this.viewPager.setCurrTab("messages");
        this.tabBar.setupWithViewPager(this.viewPager);
        for (int i = 0; i < this.tabBar.getTabCount(); i++) {
            TextView textView = (TextView) LayoutInflater.from(this.f6980d).inflate(R.layout.custom_message_tab, null);
            textView.setText(this.tabBar.mo1765a(i).mo1815d());
            textView.setCompoundDrawablePadding(0);
            this.tabBar.mo1765a(i).mo1809a((View) textView);
        }
        this.tabBar.setSelectedTabIndicatorColor(getResources().getColor(R.color.gossip));
        this.tabBar.setTabTextColors(Color.parseColor("#888888"), getResources().getColor(R.color.gossip));
        this.tabBar.setOnTabSelectedListener(new C0411a() {
            public void onTabReselected(C0416d dVar) {
                TextView textView = (TextView) dVar.mo1811a();
                textView.setTextColor(MessageTabFragment.this.getResources().getColor(R.color.gossip));
                textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                MessageTabFragment.this.mo10535e();
            }

            public void onTabSelected(C0416d dVar) {
                TextView textView = (TextView) dVar.mo1811a();
                textView.setTextColor(MessageTabFragment.this.getResources().getColor(R.color.gossip));
                textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                MessageTabFragment.this.viewPager.setCurrentItem(dVar.mo1814c(), true);
                if (dVar.mo1814c() == 0) {
                    TextView textView2 = (TextView) MessageTabFragment.this.tabBar.mo1765a(1).mo1811a();
                    textView2.setTextColor(Color.parseColor("#888888"));
                    textView2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else if (dVar.mo1814c() == 1) {
                    TextView textView3 = (TextView) MessageTabFragment.this.tabBar.mo1765a(0).mo1811a();
                    textView3.setTextColor(Color.parseColor("#888888"));
                    textView3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }

            public void onTabUnselected(C0416d dVar) {
                TextView textView = (TextView) dVar.mo1811a();
                textView.setTextColor(Color.parseColor("#888888"));
                textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (dVar.mo1814c() == 0) {
                    TextView textView2 = (TextView) MessageTabFragment.this.tabBar.mo1765a(1).mo1811a();
                    textView2.setTextColor(Color.parseColor("#888888"));
                    textView2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else if (dVar.mo1814c() == 1) {
                    TextView textView3 = (TextView) MessageTabFragment.this.tabBar.mo1765a(0).mo1811a();
                    textView3.setTextColor(Color.parseColor("#888888"));
                    textView3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        });
        this.f6986j = mo10535e();
        if (AppState.account != null) {
            mo10532a(AppState.account.messaging_disabled);
        }
        ((BaseActivity) this.f6980d).addToSubscriptionList(RxBus.m14573a().mo14348a(C2616ae.class, (bjy<T>) new bjy<C2616ae>() {
            /* renamed from: a */
            public void onNext(C2616ae aeVar) {
                MessageTabFragment.this.mo10532a(aeVar.f10584a);
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        ((BaseActivity) this.f6980d).addToSubscriptionList(RxBus.m14573a().mo14348a(C2612aa.class, (bjy<T>) new bjy<C2612aa>() {
            /* renamed from: a */
            public void onNext(C2612aa aaVar) {
                MessageTabFragment.this.f6986j = MessageTabFragment.this.mo10535e();
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        ((BaseActivity) this.f6980d).addToSubscriptionList(RxBus.m14573a().mo14348a(C2621aj.class, (bjy<T>) new bjy<C2621aj>() {
            /* renamed from: a */
            public void onNext(final C2621aj ajVar) {
                int i = 1;
                boolean a = MessageTabFragment.this.f6983g.mo13957a(ajVar);
                boolean a2 = MessageTabFragment.this.f6984h.mo13957a(ajVar);
                boolean a3 = MessageTabFragment.this.f6985i.mo13957a(ajVar);
                if (!(a | a2) && !a3) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("post_id", Integer.toString(ajVar.f10592b));
                    hashMap.put("post_name", ajVar.f10594d);
                    ApiService.m14297a().mo14169n((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                        /* renamed from: a */
                        public void onNext(NetworkData networkData) {
                            if (!networkData.success) {
                                Toast.makeText(MessageTabFragment.this.f6980d, networkData.error, 0).show();
                                return;
                            }
                            MessageThread messageThread = networkData.thread;
                            messageThread.unread_messages = 1;
                            if (!ajVar.f10593c.equals("")) {
                                ((Message) messageThread.messages.get(0)).subject = ajVar.f10593c;
                            }
                            if (messageThread.is_request == 1) {
                                MessageTabFragment.this.f6985i.mo13953a(messageThread);
                                MessageTabFragment.this.mo10533a(0, 0, 1);
                                return;
                            }
                            MessageTabFragment.this.f6984h.mo13953a(messageThread);
                            if (messageThread.online == 1) {
                                MessageTabFragment.this.f6983g.mo13953a(messageThread);
                                MessageTabFragment.this.mo10533a(1, 1, 0);
                                return;
                            }
                            MessageTabFragment.this.mo10533a(0, 1, 0);
                        }

                        public void onCompleted() {
                        }

                        public void onError(Throwable th) {
                        }
                    });
                    return;
                }
                int i2 = a ? 1 : 0;
                int i3 = a2 ? 1 : 0;
                if (!a3) {
                    i = 0;
                }
                MessageTabFragment.this.mo10533a(i2, i3, i);
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        ((BaseActivity) this.f6980d).addToSubscriptionList(RxBus.m14573a().mo14348a(C2667x.class, (bjy<T>) new bjy<C2667x>() {
            /* renamed from: a */
            public void onNext(C2667x xVar) {
                MessageThread b = MessageTabFragment.this.f6983g.mo13958b();
                MessageThread b2 = MessageTabFragment.this.f6985i.mo13958b();
                MessageThread b3 = MessageTabFragment.this.f6984h.mo13958b();
                if (b2 == null && b3 == null) {
                    MessageTabFragment.this.viewPager.setCurrentItem(1, true);
                } else if (b2 != null && b3 == null) {
                    MessageTabFragment.this.viewPager.setCurrentItem(2, true);
                } else if (b2 == null && b3 != null) {
                    MessageTabFragment.this.viewPager.setCurrentItem(1, true);
                } else if (b2 != null && b3 != null) {
                    if (b2.unread_messages > 0 && b3.unread_messages == 0) {
                        MessageTabFragment.this.viewPager.setCurrentItem(2, true);
                    } else if (b2.unread_messages == 0 && b3.unread_messages > 0) {
                        MessageTabFragment.this.viewPager.setCurrentItem(1, true);
                    } else if (b2.unread_messages <= 0 || b3.unread_messages <= 0) {
                        MessageTabFragment.this.viewPager.setCurrentItem(1, true);
                    } else if (((Message) b2.messages.get(0)).sent_time > ((Message) b3.messages.get(0)).sent_time) {
                        MessageTabFragment.this.viewPager.setCurrentItem(2, true);
                    } else {
                        MessageTabFragment.this.viewPager.setCurrentItem(1, true);
                    }
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }
        }));
        ((BaseActivity) this.f6980d).addToSubscriptionList(RxBus.m14573a().mo14348a(C2656m.class, (bjy<T>) new bjy<C2656m>() {
            /* renamed from: a */
            public void onNext(C2656m mVar) {
                if (mVar != null) {
                    MessageTabFragment.this.f6983g.mo13952a(mVar.f10648a, mVar.f10649b);
                    MessageTabFragment.this.f6984h.mo13952a(mVar.f10648a, mVar.f10649b);
                    MessageTabFragment.this.f6985i.mo13952a(mVar.f10648a, mVar.f10649b);
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
        return inflate;
    }

    public void onResume() {
        super.onResume();
        bhw.m8184a(this.f6980d);
        ApiService.m14297a().mo14148g().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        RxBus.m14573a().mo14349a(new C2637az(2, 0, true));
        this.f6986j = mo10535e();
    }
}
