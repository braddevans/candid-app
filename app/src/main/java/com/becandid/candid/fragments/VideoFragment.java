package com.becandid.candid.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.util.video.FLSprite.SpecialEffect;
import com.becandid.candid.util.video.VideoGLView;
import com.becandid.candid.util.video.VideoGLView.C1780b;
import java.io.IOException;

public class VideoFragment extends Fragment implements C1780b {

    /* renamed from: a */
    private static int f6878a = AppState.config.getInt("android_video_width", 720);

    /* renamed from: b */
    private static int f6879b = AppState.config.getInt("android_video_height", 1280);
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final int f6880c = AppState.config.getInt("android_video_max_record_time", 30);

    /* renamed from: d */
    private static final int f6881d = AppState.config.getInt("android_video_min_record_time", 2);
    /* access modifiers changed from: private */

    /* renamed from: e */
    public VideoGLView f6882e;

    /* renamed from: f */
    private ImageView f6883f;

    /* renamed from: g */
    private TransitionDrawable f6884g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public ProgressBar f6885h;

    /* renamed from: i */
    private ImageView f6886i;

    /* renamed from: j */
    private ImageView f6887j;

    /* renamed from: k */
    private ImageButton f6888k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public ImageButton f6889l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public TextView f6890m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public MediaMuxerWrapper f6891n;

    /* renamed from: o */
    private Bitmap f6892o;

    /* renamed from: p */
    private int f6893p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public C1642a f6894q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public Handler f6895r;

    /* renamed from: s */
    private final OnClickListener f6896s = new OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.video_album_view /*2131624685*/:
                    if (VideoFragment.this.f6894q != null) {
                        VideoFragment.this.f6894q.onPickFromGallery();
                        return;
                    }
                    return;
                case R.id.record_button /*2131624687*/:
                    if (VideoFragment.this.f6891n == null) {
                        VideoFragment.this.m9457d();
                        return;
                    } else {
                        VideoFragment.this.m9459e();
                        return;
                    }
                case R.id.video_add_sprite_smiley /*2131624688*/:
                    if (VideoFragment.this.f6894q != null) {
                        VideoFragment.this.f6894q.onOpenSpritesSelectView();
                        return;
                    }
                    return;
                case R.id.video_record_back /*2131624689*/:
                    if (VideoFragment.this.f6891n != null) {
                        VideoFragment.this.m9459e();
                    }
                    VideoFragment.this.f6882e.onPause();
                    VideoFragment.this.f6894q.finishActivity();
                    return;
                case R.id.video_record_camera_flip /*2131624690*/:
                    if (VideoFragment.this.f6882e != null) {
                        VideoFragment.this.f6882e.mo10715a();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: t */
    public int f6897t = 0;

    /* renamed from: u */
    private final C2710a f6898u = new C2710a() {
        /* renamed from: a */
        public void mo10474a(MediaEncoder jpVar) {
            if (jpVar instanceof MediaVideoEncoder) {
                VideoFragment.this.f6882e.setVideoEncoder((MediaVideoEncoder) jpVar);
            }
        }

        /* renamed from: b */
        public void mo10475b(MediaEncoder jpVar) {
            if (jpVar instanceof MediaVideoEncoder) {
                VideoFragment.this.f6882e.setVideoEncoder(null);
            }
        }
    };

    /* renamed from: com.becandid.candid.fragments.VideoFragment$a */
    public interface C1642a {
        void finishActivity();

        void onOpenSpritesSelectView();

        void onPickFromGallery();

        void onSpriteSelected(int i, SpecialEffect specialEffect);

        void onVideoRecordFinished(String str);
    }

    /* renamed from: a */
    public static Bitmap m9450a(Context context, int i) {
        Options options = new Options();
        options.inScaled = false;
        return BitmapFactory.decodeResource(context.getResources(), i, options);
    }

    /* renamed from: c */
    private void m9454c() {
        Display defaultDisplay = ((WindowManager) getActivity().getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        int i = point.x;
        if (i < 720) {
            f6878a = 360;
            f6879b = 640;
        } else if (i < 1080) {
            f6878a = 720;
            f6879b = 1280;
        } else {
            f6878a = 1080;
            f6879b = 1920;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m9457d() {
        try {
            this.f6883f.setImageDrawable(this.f6884g);
            this.f6884g.startTransition(500);
            this.f6891n = new MediaMuxerWrapper(".mp4");
            new MediaVideoEncoder(this.f6891n, this.f6898u, this.f6882e.getVideoWidth(), this.f6882e.getVideoHeight());
            new MediaAudioEncoder(this.f6891n, this.f6898u);
            this.f6891n.mo14398b();
            this.f6891n.mo14399c();
            m9461f();
        } catch (IOException e) {
            m9463g();
            this.f6884g.reverseTransition(500);
            Log.d("VideoFragment", "startCapture exception:", e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m9459e() {
        int i = this.f6897t;
        m9463g();
        this.f6884g.reverseTransition(500);
        if (this.f6891n != null) {
            String a = this.f6891n.mo14395a();
            this.f6891n.mo14400d();
            this.f6891n = null;
            if (a == null) {
                return;
            }
            if (i < f6881d) {
                Toast.makeText(getActivity(), "Minimum video length is " + Integer.toString(f6881d) + " seconds.", 0).show();
            } else {
                this.f6894q.onVideoRecordFinished(a);
            }
        }
    }

    /* renamed from: f */
    private void m9461f() {
        this.f6897t = 0;
        this.f6885h.setProgress(this.f6897t);
        this.f6885h.setVisibility(0);
        this.f6895r.postDelayed(new Runnable() {
            public void run() {
                if (VideoFragment.this.f6897t == VideoFragment.f6880c) {
                    VideoFragment.this.f6895r.removeCallbacks(this);
                    VideoFragment.this.m9459e();
                    Toast.makeText(VideoFragment.this.getActivity(), "Time's up! Recording Stopped.", 0).show();
                    return;
                }
                VideoFragment.this.f6897t = VideoFragment.this.f6897t + 1;
                VideoFragment.this.f6885h.setProgress(VideoFragment.this.f6897t);
                VideoFragment.this.f6895r.removeCallbacks(this);
                VideoFragment.this.f6895r.postDelayed(this, 1000);
            }
        }, 1000);
    }

    /* renamed from: g */
    private void m9463g() {
        this.f6897t = 0;
        this.f6885h.setVisibility(8);
        this.f6895r.removeCallbacksAndMessages(null);
    }

    /* renamed from: a */
    public void mo10463a() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (VideoGLView.f7234a != null && VideoGLView.f7234a.size() > 0 && VideoGLView.f7235b != null && VideoGLView.f7235b.size() > 0 && VideoFragment.this.f6889l != null) {
                    VideoFragment.this.f6889l.setVisibility(0);
                }
            }
        });
    }

    /* renamed from: a */
    public void mo10464a(int i, SpecialEffect specialEffect) {
        if (i != this.f6893p) {
            this.f6893p = i;
            this.f6892o = m9450a((Context) getActivity(), this.f6893p);
            if (this.f6892o != null) {
                this.f6882e.setSpriteBitmaps(new Bitmap[]{this.f6892o}, new SpecialEffect[]{specialEffect});
            }
        }
        if (this.f6886i != null) {
            this.f6886i.setVisibility(8);
        }
        if (this.f6889l != null) {
            this.f6889l.setVisibility(8);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof C1642a) {
            this.f6894q = (C1642a) activity;
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_video_record, viewGroup, false);
        m9454c();
        this.f6882e = (VideoGLView) inflate.findViewById(R.id.cameraView);
        this.f6882e.setDefaultWidthAndHeight(f6878a, f6879b);
        this.f6882e.setVideoSize(f6878a, f6879b);
        this.f6883f = (ImageView) inflate.findViewById(R.id.record_button);
        this.f6883f.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    if (VideoFragment.this.f6891n == null) {
                        VideoFragment.this.m9457d();
                    }
                } else if (motionEvent.getAction() == 1 && VideoFragment.this.f6891n != null) {
                    VideoFragment.this.m9459e();
                }
                return true;
            }
        });
        this.f6888k = (ImageButton) inflate.findViewById(R.id.video_record_back);
        this.f6888k.setOnClickListener(this.f6896s);
        this.f6886i = (ImageView) inflate.findViewById(R.id.video_add_sprite_smiley);
        this.f6886i.setVisibility(8);
        this.f6889l = (ImageButton) inflate.findViewById(R.id.video_record_camera_flip);
        this.f6889l.setOnClickListener(this.f6896s);
        this.f6889l.setVisibility(8);
        this.f6887j = (ImageView) inflate.findViewById(R.id.video_album_view);
        if (AppState.config.getInt("android_video_enable_gallery", 1) == 0) {
            this.f6887j.setVisibility(8);
        } else {
            this.f6887j.setVisibility(0);
            this.f6887j.setOnClickListener(this.f6896s);
        }
        this.f6884g = new TransitionDrawable(new Drawable[]{getResources().getDrawable(R.drawable.blue_record), getResources().getDrawable(R.drawable.red_record)});
        this.f6885h = (ProgressBar) inflate.findViewById(R.id.video_record_progress_circle);
        this.f6885h.setMax(f6880c);
        this.f6885h.setProgress(0);
        this.f6885h.setVisibility(8);
        this.f6895r = new Handler();
        this.f6890m = (TextView) inflate.findViewById(R.id.video_record_hint);
        this.f6895r.postDelayed(new Runnable() {
            public void run() {
                if (VideoFragment.this.f6890m != null) {
                    VideoFragment.this.f6890m.setVisibility(8);
                }
            }
        }, 3000);
        return inflate;
    }

    public void onPause() {
        this.f6882e.setCameraStatusListener(null);
        m9459e();
        this.f6882e.onPause();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f6882e.onResume();
        this.f6882e.setCameraStatusListener(this);
    }
}
