package com.becandid.candid.fragments.onboarding;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseActivity;
import com.becandid.candid.activities.OnboardingActivity;
import com.becandid.candid.activities.WebViewActivity;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.EmptySubscriber;
import p012rx.schedulers.Schedulers;

public class OnboardingLocationFragment extends Fragment {

    /* renamed from: a */
    final int f7092a = 0;

    /* renamed from: b */
    Handler f7093b;

    /* renamed from: c */
    Runnable f7094c = new Runnable() {
        public void run() {
            if (AppState.location == null) {
                Toast.makeText(OnboardingLocationFragment.this.getActivity(), "Location unavailable; we'll try again later", 1).show();
                OnboardingLocationFragment.this.spinny.setVisibility(8);
                RxBus.m14573a().mo14349a(new C2631at("location"));
            }
        }
    };

    /* renamed from: d */
    private Unbinder f7095d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f7096e = false;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public boolean f7097f = false;
    @BindView(2131624956)
    Button locationButton;
    @BindView(2131624953)
    TextView locationHeader;
    @BindView(2131624957)
    TextView locationInfo;
    @BindView(2131624954)
    TextView locationSubheader;
    @BindView(2131624958)
    TextView privacy;
    @BindView(2131624952)
    TextView skip;
    @BindView(2131624959)
    FrameLayout spinny;

    /* renamed from: c */
    private void m9640c() {
        this.locationHeader.setText(AppState.config.getString("location_header", getString(R.string.location_header)));
        this.locationSubheader.setText(AppState.config.getString("location_subheader", getString(R.string.location_subheader)));
        this.locationInfo.setText(AppState.config.getString("location_info", getString(R.string.location_info)));
        this.privacy.setText(AppState.config.getString("more_info", getString(R.string.privacy)));
        this.locationButton.setText(AppState.config.getString("allow_location_button", getString(R.string.location_button)));
    }

    /* renamed from: a */
    public void mo10620a() {
        if (ContextCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_COARSE_LOCATION") != 0) {
            requestPermissions(new String[]{"android.permission.ACCESS_COARSE_LOCATION"}, 0);
            return;
        }
        mo10622b();
    }

    /* renamed from: a */
    public void mo10621a(String str) {
        FragmentActivity activity = getActivity();
        if (activity instanceof OnboardingActivity) {
            ((OnboardingActivity) activity).switchFragment(str);
        }
    }

    /* renamed from: b */
    public void mo10622b() {
        if (!((LocationManager) getActivity().getSystemService("location")).isProviderEnabled("network")) {
            new Builder(getActivity()).setTitle("Location Is Disabled").setMessage("Do you want to enable it?").setPositiveButton(R.string.yes, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (OnboardingLocationFragment.this.isAdded()) {
                        OnboardingLocationFragment.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                        OnboardingLocationFragment.this.f7097f = true;
                        OnboardingLocationFragment.this.spinny.setVisibility(0);
                    }
                }
            }).setNegativeButton(R.string.no, null).create().show();
        } else {
            ApiService.m14297a().mo14168n("onboarding/location/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
            this.spinny.setVisibility(0);
            if (LocationUtil.m14546b() != null) {
                Fragment findFragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag("location");
                if (findFragmentByTag != null && findFragmentByTag.isVisible()) {
                    this.spinny.setVisibility(8);
                    RxBus.m14573a().mo14349a(new C2631at("location"));
                }
            } else {
                LocationUtil.m14544a();
            }
        }
        ((BaseActivity) getActivity()).addToSubscriptionList(RxBus.m14573a().mo14348a(C2669z.class, (bjy<T>) new bjy<C2669z>() {
            /* renamed from: a */
            public void onNext(C2669z zVar) {
                if (!OnboardingLocationFragment.this.f7096e) {
                    OnboardingLocationFragment.this.f7096e = true;
                    Fragment findFragmentByTag = OnboardingLocationFragment.this.getActivity().getSupportFragmentManager().findFragmentByTag("location");
                    if (findFragmentByTag != null && findFragmentByTag.isVisible()) {
                        OnboardingLocationFragment.this.spinny.setVisibility(8);
                        RxBus.m14573a().mo14349a(new C2631at("location"));
                    }
                }
            }

            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }
        }));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.onboarding_location, viewGroup, false);
        this.f7095d = ButterKnife.bind((Object) this, inflate);
        this.f7093b = new Handler(getContext().getMainLooper());
        m9640c();
        this.privacy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(OnboardingLocationFragment.this.getContext(), WebViewActivity.class);
                intent.putExtra("title", "Why Can I Trust Candid?");
                intent.putExtra("url", GossipApplication.f6096d + "content/whysafe");
                OnboardingLocationFragment.this.getContext().startActivity(intent);
            }
        });
        this.skip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OnboardingLocationFragment.this.mo10621a("location");
                ApiService.m14297a().mo14168n("onboarding/location/skip").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
            }
        });
        this.locationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OnboardingLocationFragment.this.mo10620a();
            }
        });
        return inflate;
    }

    public void onPause() {
        super.onPause();
        this.f7093b.removeCallbacks(this.f7094c);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        switch (i) {
            case 0:
                if (iArr.length <= 0 || iArr[0] != 0) {
                    RxBus.m14573a().mo14349a(new C2631at("location"));
                    return;
                } else {
                    mo10622b();
                    return;
                }
            default:
                return;
        }
    }

    public void onResume() {
        super.onResume();
        Fragment findFragmentByTag = getActivity().getSupportFragmentManager().findFragmentByTag("location");
        if (AppState.location != null && findFragmentByTag != null && findFragmentByTag.isVisible() && this.f7096e) {
            this.spinny.setVisibility(8);
            mo10621a("location");
        } else if (findFragmentByTag == null || !findFragmentByTag.isVisible() || !this.f7097f) {
            this.spinny.setVisibility(8);
        } else {
            this.f7093b.postDelayed(this.f7094c, 5000);
        }
    }
}
