package com.becandid.candid.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.p003v7.app.AlertDialog.Builder;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.PopupMenu;
import android.support.p003v7.widget.PopupMenu.OnMenuItemClickListener;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.RecyclerView.ItemAnimator;
import android.support.p003v7.widget.SimpleItemAnimator;
import android.text.Editable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Message;
import com.becandid.candid.data.MessageThread;
import com.becandid.candid.data.Post;
import com.becandid.candid.data.User;
import com.becandid.candid.models.BaseVideoMessage;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.models.UploadMediaResponse;
import com.becandid.candid.views.FullScreenVideoPlayer;
import com.becandid.candid.views.StickerKeyboard;
import com.becandid.candid.views.StickerKeyboard.C1848a;
import com.becandid.thirdparty.BlurTask;
import com.becandid.thirdparty.BlurTask.BadgeType;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.venmo.view.TooltipView;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import p012rx.schedulers.Schedulers;

public class MessageActivity extends PopupWithBlurBackgroundActivity implements C1848a, C2716c, CacheListener {
    public static final int FULL_SCREEN_VIDEO = 299;

    /* renamed from: a */
    final BroadcastReceiver f6446a = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Message message;
            Bundle extras = intent.getExtras();
            String string = extras.getString("user_info");
            if (!(extras.getString("message_id") == null || extras.getString("post_id") == null || string == null)) {
                try {
                    int parseInt = Integer.parseInt(extras.getString("message_id"));
                    int parseInt2 = Integer.parseInt(extras.getString("post_id"));
                    String string2 = new JSONObject(string).getString("post_name");
                    if (parseInt != 0 && parseInt2 != 0 && Integer.toString(parseInt2).equals(MessageActivity.this.f6448d) && string2.equals(MessageActivity.this.f6449e)) {
                        if (extras.getString("video_url") != null) {
                            HttpProxyCacheServer a = GossipApplication.m9013a(MessageActivity.this);
                            a.mo16743a((CacheListener) MessageActivity.this, extras.getString("video_url"));
                            message = new Message(MessageActivity.this.f6464t, a.mo16740a(extras.getString("video_url")));
                        } else {
                            message = new Message();
                        }
                        message.message_id = parseInt;
                        message.post_id = parseInt2;
                        message.message = extras.getString("message");
                        message.subject = extras.getString("subject");
                        if (extras.containsKey("picture_status")) {
                            MessageActivity.this.f6462r = extras.getString("picture_status");
                        }
                        if (extras.containsKey("video_url") && extras.containsKey("image_width") && extras.containsKey("image_height") && extras.containsKey("source_url")) {
                            message.video_url = extras.getString("video_url");
                            message.image_width = Integer.parseInt(extras.getString("image_width"));
                            message.image_height = Integer.parseInt(extras.getString("image_height"));
                            message.source_url = extras.getString("source_url");
                            message.uploaded = Integer.parseInt(extras.getString("uploaded"));
                        } else if (extras.containsKey("image_width") && extras.containsKey("image_height") && extras.containsKey("source_url")) {
                            message.image_width = Integer.parseInt(extras.getString("image_width"));
                            message.image_height = Integer.parseInt(extras.getString("image_height"));
                            message.source_url = extras.getString("source_url");
                            message.uploaded = Integer.parseInt(extras.getString("uploaded"));
                        } else if (extras.containsKey("sticker_name")) {
                            message.sticker_name = extras.getString("sticker_name");
                        }
                        MessageActivity.this.m9187a(message);
                    }
                } catch (JSONException e) {
                    Crashlytics.m16437a((Throwable) e);
                } catch (Exception e2) {
                    Crashlytics.m16437a((Throwable) e2);
                }
            }
            abortBroadcast();
        }
    };

    /* renamed from: b */
    private CandidAnimals f6447b;
    @BindView(2131624292)
    FrameLayout blockedFrameLayout;
    @BindView(2131624269)
    RelativeLayout contentRootView;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f6448d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f6449e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f6450f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public User f6451g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public User f6452h;
    @BindView(2131624295)
    TextView headerName;
    @BindView(2131624297)
    ImageView headerOnline;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public String f6453i;

    /* renamed from: j */
    private int f6454j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public String f6455k;

    /* renamed from: l */
    private boolean f6456l = false;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public MessagesAdapter f6457m;
    @BindView(2131624301)
    TextView mNickname;
    @BindView(2131624302)
    FrameLayout mVideoPlayerFullscreen;
    @BindView(2131624283)
    View messageGifButton;
    @BindView(2131624298)
    LinearLayout messageHeaderPostContainer;
    @BindView(2131624299)
    TextView messageHeaderPostText;
    @BindView(2131624300)
    ImageView messageHeaderRightArrow;
    @BindView(2131624296)
    TextView messageHeaderUserIcon;
    @BindView(2131624294)
    RelativeLayout messageHeaderUserIconContainer;
    @BindView(2131624277)
    EditText messageInput;
    @BindView(2131624279)
    View messageInputButtonsHidden;
    @BindView(2131624278)
    View messageInputButtonsVisible;
    @BindView(2131624273)
    RelativeLayout messageInputContainer;
    @BindView(2131624275)
    ImageView messagePhoto;
    @BindView(2131624280)
    ImageView messagePhotoButton;
    @BindView(2131624276)
    ImageView messagePhotoClear;
    @BindView(2131624286)
    ProgressBar messagePostBtnSpinny;
    @BindView(2131624272)
    RecyclerView messageRecycler;
    @BindView(2131624291)
    FrameLayout messageSpinnyContainer;
    @BindView(2131624284)
    TextView messageTextPlaceholder;
    @BindView(2131624281)
    ImageView messageVideoButton;

    /* renamed from: n */
    private LayoutParams f6458n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public Handler f6459o;

    /* renamed from: p */
    private final int f6460p = 60000;
    @BindView(2131624150)
    ImageView postButton;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public LinearLayoutManager f6461q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public String f6462r;
    @BindView(2131624289)
    TextView requestAccept;
    @BindView(2131624287)
    LinearLayout requestButtons;
    @BindView(2131624288)
    TextView requestIgnore;
    @BindView(2131624268)
    View root;

    /* renamed from: s */
    private Message f6463s;
    @BindView(2131624282)
    ImageView stickerButton;
    @BindView(2131624290)
    StickerKeyboard stickerKeyboard;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public bcw f6464t = new bcu(new bct() {
        public void onPlayerItemChanged(bcz bcz) {
        }
    });
    @BindView(2131624174)
    TooltipView tooltipView;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public bdq f6465u;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public bdu f6466v;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public Runnable f6467w = new Runnable() {
        public void run() {
            HashMap hashMap = new HashMap();
            if (MessageActivity.this.f6448d != null) {
                hashMap.put("post_id", MessageActivity.this.f6448d);
            }
            if (MessageActivity.this.f6449e != null) {
                hashMap.put("post_name", MessageActivity.this.f6449e);
            }
            ApiService.m14297a().mo14169n((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                }

                public void onNext(NetworkData networkData) {
                    if (!networkData.success) {
                        Toast.makeText(MessageActivity.this, networkData.error, 0).show();
                    }
                    MessageThread messageThread = networkData.thread;
                    if (messageThread.user_info != null) {
                        MessageActivity.this.f6451g = messageThread.user_info;
                    }
                    if (messageThread.target_user_info != null) {
                        MessageActivity.this.f6452h = messageThread.target_user_info;
                    }
                    ArrayList arrayList = new ArrayList();
                    if (messageThread.messages != null && !messageThread.messages.isEmpty()) {
                        for (Message message : messageThread.messages) {
                            if (!MessageActivity.this.f6457m.mo13880b(message.message_id)) {
                                arrayList.add(message);
                            }
                        }
                        MessageActivity.this.f6457m.mo13875a(0, (List<Message>) arrayList);
                    }
                    if (AppState.config.getInt("enable_nickname", 1) == 1 && messageThread.show_nickname_alert == 1 && networkData.thread.target_user_info.nickname != null && !networkData.thread.target_user_info.nickname.equals("")) {
                        MessageActivity.this.setupNicknameAlert(networkData.thread.target_user_info.nickname);
                    }
                }
            });
            MessageActivity.this.f6459o.postDelayed(MessageActivity.this.f6467w, 60000);
        }
    };

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9184a() {
        Message message = new Message();
        HashMap hashMap = new HashMap();
        String trim = this.messageInput.getText().toString().trim();
        if (trim.length() > 0) {
            hashMap.put("message", trim);
            message.message = trim;
        }
        if (this.f6453i != null) {
            hashMap.put("source_url", this.f6453i);
            message.source_url = this.f6453i;
            hashMap.put("uploaded", Integer.toString(this.f6454j));
            message.uploaded = this.f6454j;
            float f = 0.0f;
            float f2 = 0.0f;
            if (this.photoHelper != null) {
                f = (float) this.photoHelper.f10751d;
                f2 = (float) this.photoHelper.f10752e;
            }
            if (f <= 0.0f || f2 <= 0.0f) {
                hashMap.put("image_width", "240");
                message.image_width = 240;
                hashMap.put("image_height", "160");
                message.image_height = 160;
            } else if (((double) f) < (((double) f2) * 240.0d) / 160.0d) {
                hashMap.put("image_width", String.valueOf((int) ((((double) f) * 160.0d) / ((double) f2))));
                message.image_width = (int) ((((double) f) * 160.0d) / ((double) f2));
                hashMap.put("image_height", "160");
                message.image_height = 160;
            } else {
                hashMap.put("image_width", "240");
                message.image_width = 240;
                hashMap.put("image_height", String.valueOf((int) ((((double) f2) * 240.0d) / ((double) f))));
                message.image_height = (int) ((((double) f2) * 240.0d) / ((double) f));
            }
        } else if (this.f6455k != null) {
            hashMap.put("sticker_name", this.f6455k);
            message.sticker_name = this.f6455k;
            hashMap.put("image_width", "120");
            message.image_width = 120;
            hashMap.put("image_height", "120");
            message.image_height = 120;
        }
        m9189a(hashMap, message);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9187a(Message message) {
        message.sender = "other";
        this.f6457m.mo13877a(message);
        this.messageRecycler.smoothScrollToPosition(this.f6457m.getItemCount() - 1);
        markThreadRead();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9188a(BadgeType badgeType) {
        if (!this.f6456l) {
            this.f6456l = true;
            new BlurTask((Activity) this, (View) this.contentRootView, badgeType).execute(new Void[0]);
        }
    }

    /* renamed from: a */
    private void m9189a(HashMap<String, String> hashMap, final Message message) {
        if (hashMap.containsKey("message") || hashMap.containsKey("sticker_name") || hashMap.containsKey("source_url")) {
            this.messageInput.setText("");
            chooseSticker(null);
            hashMap.put("post_id", this.f6448d);
            message.post_id = Integer.parseInt(this.f6448d);
            if (this.f6449e != null) {
                hashMap.put("post_name", this.f6449e);
                message.user_info = new User();
                message.user_info.post_name = this.f6449e;
            }
            if (this.f6450f != null) {
                hashMap.put("thread_comment_id", this.f6450f);
            }
            hashMap.put("send", Boolean.toString(true));
            final long currentTimeMillis = System.currentTimeMillis();
            message.messageTempId = currentTimeMillis;
            ApiService.m14297a().mo14170o((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                    MessageActivity.this.messageSpinnyContainer.setVisibility(8);
                }

                public void onError(Throwable th) {
                    MessageActivity.this.messageSpinnyContainer.setVisibility(8);
                    if (!MessageActivity.this.isStopped) {
                        Toast.makeText(MessageActivity.this, th.toString(), 1).show();
                        MessageActivity.this.f6457m.mo13876a(message.messageTempId, false, false, null);
                    }
                }

                public void onNext(NetworkData networkData) {
                    if (networkData != null) {
                        MessageActivity.this.f6462r = networkData.picture_status;
                    }
                    if (!MessageActivity.this.isStopped) {
                        if (!(MessageActivity.this.f6453i == null && MessageActivity.this.f6455k == null)) {
                            MessageActivity.this.clearPhoto(null);
                        }
                        if (networkData == null || networkData.success) {
                            if (!(networkData == null || networkData.message == null)) {
                                MessageActivity.this.f6457m.mo13876a(currentTimeMillis, true, false, networkData.message);
                            }
                            if (AppState.config.getInt("enable_nickname", 1) == 1 && networkData != null && networkData.show_nickname_prompt == 1) {
                                MessageActivity.this.m9188a(BadgeType.MESSAGE_ADD_NICKNAME);
                                return;
                            }
                            return;
                        }
                        Toast.makeText(MessageActivity.this, networkData.error, 1).show();
                        if (networkData.error.contains("blocked") || networkData.error.contains("disabled")) {
                            MessageActivity.this.f6457m.mo13876a(currentTimeMillis, false, true, null);
                        }
                        MessageActivity.this.f6457m.mo13876a(currentTimeMillis, false, false, null);
                    }
                }
            });
            message.params = hashMap;
            this.f6457m.mo13877a(message);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    MessageActivity.this.scrollToBottom();
                }
            }, 200);
            return;
        }
        Toast.makeText(this, "Empty messages are not allowed.", 0).show();
    }

    public void checkAndUploadVideo() {
        if (this.videoHelper != null && this.videoHelper.f10883a != null && this.videoHelper.f10884b != null) {
            this.videoHelper.mo14422d();
        }
    }

    public void chooseSticker(View view) {
        if (this.stickerKeyboard.getVisibility() == 0 || view == null) {
            this.stickerKeyboard.setVisibility(8);
            this.stickerButton.setImageResource(R.drawable.stickerbutton);
            this.stickerButton.setAlpha(0.5f);
            return;
        }
        if (this.keyboardOpen) {
            closeKeyboard();
        }
        this.stickerKeyboard.setVisibility(0);
        this.stickerButton.setImageResource(R.drawable.stickerbutton_active);
        this.stickerButton.setAlpha(1.0f);
    }

    public void clearPhoto(View view) {
        super.clearPhoto();
        hideMessageInput();
        this.photoHelper = null;
        this.messagePhoto.setImageBitmap(null);
        this.f6453i = null;
        this.f6454j = 0;
        this.messagePhoto.setVisibility(8);
        if (this.f6458n != null) {
            this.messagePhoto.setLayoutParams(this.f6458n);
        }
        this.f6455k = null;
        this.messagePhotoClear.setVisibility(8);
        if (!this.keyboardOpen) {
            keyboardClosed();
        }
        updatePostButton();
    }

    public void clickSticker(String str) {
        clearPhoto(null);
        this.f6455k = str;
        this.messagePhoto.setVisibility(0);
        this.messagePhotoClear.setVisibility(0);
        this.messagePhoto.setImageDrawable(GossipApplication.f6093a.getResources().getDrawable(GossipApplication.f6093a.getResources().getIdentifier(str.toLowerCase(), "drawable", GossipApplication.f6093a.getPackageName())));
        this.f6458n = (LayoutParams) this.messagePhoto.getLayoutParams();
        LayoutParams layoutParams = (LayoutParams) this.messagePhoto.getLayoutParams();
        layoutParams.width = (int) (getResources().getDisplayMetrics().density * 50.0f);
        layoutParams.height = layoutParams.width;
        this.messagePhoto.setLayoutParams(layoutParams);
        showMessageInput();
        updatePostButton();
    }

    public void closingPopup() {
        this.f6456l = false;
        this.messageInput.requestFocus();
    }

    public bjz getDataSubscription(final boolean z) {
        HashMap hashMap = new HashMap();
        if (this.f6448d != null) {
            hashMap.put("post_id", this.f6448d);
        }
        if (this.f6449e != null) {
            hashMap.put("post_name", this.f6449e);
        }
        markThreadRead();
        return ApiService.m14297a().mo14169n((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                Log.d("ReadMessage", th.toString());
                MessageActivity.this.messageSpinnyContainer.setVisibility(8);
            }

            public void onNext(NetworkData networkData) {
                if (!networkData.success) {
                    Toast.makeText(MessageActivity.this, networkData.error, 0).show();
                    MessageActivity.this.finish();
                }
                MessageThread messageThread = networkData.thread;
                MessageActivity.this.f6462r = messageThread.picture_status;
                MessageActivity.this.f6451g = messageThread.user_info;
                MessageActivity.this.f6452h = messageThread.target_user_info;
                MessageActivity.this.f6457m.mo13879b(messageThread.user_info);
                MessageActivity.this.f6457m.mo13878a(messageThread.target_user_info);
                ArrayList arrayList = new ArrayList();
                if (messageThread.messages == null || messageThread.messages.isEmpty()) {
                    MessageActivity.this.f6457m.mo13763a((String) null);
                } else {
                    arrayList.addAll(messageThread.messages);
                    MessageActivity.this.f6457m.mo13760a(((Message) messageThread.messages.get(0)).message_id);
                }
                if (messageThread.thread_comment_id != null) {
                    MessageActivity.this.f6450f = messageThread.thread_comment_id;
                }
                for (int i = 0; i < arrayList.size(); i++) {
                    Message message = (Message) arrayList.get(i);
                    if (message.video_url != null) {
                        message.setVideoPlayManager(MessageActivity.this.f6464t);
                        HttpProxyCacheServer a = GossipApplication.m9013a(MessageActivity.this);
                        a.mo16743a((CacheListener) MessageActivity.this, message.video_url);
                        message.setVideoUrl(a.mo16740a(message.video_url));
                    }
                }
                MessageActivity.this.f6457m.mo13764a((List<T>) arrayList);
                MessageActivity.this.postButton.setEnabled(true);
                MessageActivity.this.setupActivityHeader(messageThread.target_user_info, messageThread.post, messageThread.user_info, messageThread);
                if (messageThread.target_user_info.is_blocked) {
                    MessageActivity.this.setupBlockedUser();
                }
                if (messageThread.is_request == 1) {
                    MessageActivity.this.setupRequest();
                }
                MessageActivity.this.messagePhotoButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if ("enabled".equals(MessageActivity.this.f6462r)) {
                            MessageActivity.this.takePhoto();
                        } else {
                            Toast.makeText(MessageActivity.this.getApplicationContext(), MessageActivity.this.f6462r, 0).show();
                        }
                    }
                });
                MessageActivity.this.messageVideoButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if ("enabled".equals(MessageActivity.this.f6462r)) {
                            MessageActivity.this.takeVideo();
                        } else {
                            Toast.makeText(MessageActivity.this.getApplicationContext(), MessageActivity.this.f6462r, 0).show();
                        }
                    }
                });
                MessageActivity.this.messageSpinnyContainer.setVisibility(8);
                if (AppState.config.getInt("enable_nickname", 1) == 1 && messageThread.show_nickname_alert == 1 && messageThread.target_user_info.nickname != null && !messageThread.target_user_info.nickname.equals("")) {
                    MessageActivity.this.setupNicknameAlert(messageThread.target_user_info.nickname);
                }
                if (z) {
                    MessageActivity.this.scrollToBottom();
                }
            }
        });
    }

    public void getPage(String str) {
        HashMap hashMap = new HashMap();
        if (this.f6448d != null) {
            hashMap.put("post_id", this.f6448d);
        }
        if (this.f6449e != null) {
            hashMap.put("post_name", this.f6449e);
        }
        hashMap.put("prev_message_id", str);
        ApiService.m14297a().mo14169n((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                Log.d("ReadMessage", th.toString());
            }

            public void onNext(NetworkData networkData) {
                if (!networkData.success) {
                    Toast.makeText(MessageActivity.this, networkData.error, 0).show();
                    return;
                }
                MessageThread messageThread = networkData.thread;
                if (messageThread.user_info != null) {
                    MessageActivity.this.f6451g = messageThread.user_info;
                }
                if (messageThread.target_user_info != null) {
                    MessageActivity.this.f6452h = messageThread.target_user_info;
                }
                if (messageThread.messages == null || messageThread.messages.isEmpty()) {
                    MessageActivity.this.f6457m.mo13763a((String) null);
                } else {
                    MessageActivity.this.f6457m.mo13881c(messageThread.messages);
                    MessageActivity.this.f6457m.mo13760a(((Message) messageThread.messages.get(0)).message_id);
                }
                if (AppState.config.getInt("enable_nickname", 1) == 1 && messageThread.show_nickname_alert == 1 && networkData.thread.target_user_info.nickname != null && !networkData.thread.target_user_info.nickname.equals("")) {
                    MessageActivity.this.setupNicknameAlert(networkData.thread.target_user_info.nickname);
                }
            }
        });
    }

    public User getTargetUserData() {
        if (this.f6452h != null) {
            return this.f6452h;
        }
        return null;
    }

    public User getUserData() {
        if (this.f6451g != null) {
            return this.f6451g;
        }
        return null;
    }

    public void hideCurrentScreen() {
        this.contentRootView.setVisibility(8);
    }

    public void hideMessageInput() {
        this.messagePhotoButton.setAlpha(1.0f);
        this.messagePhotoButton.setEnabled(true);
        this.messageVideoButton.setAlpha(1.0f);
        this.messageVideoButton.setEnabled(true);
        this.stickerButton.setAlpha(1.0f);
        this.stickerButton.setEnabled(true);
        this.messageGifButton.setAlpha(1.0f);
        this.messageGifButton.setEnabled(true);
    }

    public void ignoreNickname() {
        ApiService.m14297a().mo14132c(null, this.f6448d, this.f6449e).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(NetworkData networkData) {
                MessageActivity.this.mNickname.setVisibility(0);
                MessageActivity.this.mNickname.setText("Add a Nickname");
                MessageActivity.this.mNickname.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        new BlurTask((Activity) MessageActivity.this, (View) MessageActivity.this.contentRootView, BadgeType.MESSAGE_ADD_NICKNAME).execute(new Void[0]);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public void imageCaptured() {
        if (this.photoHelper == null) {
            Crashlytics.m16437a((Throwable) new Exception("photoHelper is NULL"));
        } else {
            this.photoHelper.mo14337b();
        }
    }

    /* access modifiers changed from: protected */
    public void keyboardClosed() {
        super.keyboardClosed();
        String trim = this.messageInput.getText().toString().trim();
        if (this.f6453i != null || trim.length() == 0) {
        }
    }

    /* access modifiers changed from: protected */
    public void keyboardOpened() {
        super.keyboardOpened();
        chooseSticker(null);
        scrollToBottom();
    }

    public void markNicknameAlertShown() {
        ApiService.m14297a().mo14131c(this.f6448d, this.f6449e).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
    }

    public void markThreadRead() {
        HashMap hashMap = new HashMap();
        if (this.f6448d != null) {
            hashMap.put("post_id", this.f6448d);
        }
        if (this.f6449e != null) {
            hashMap.put("post_name", this.f6449e);
        }
        ApiService.m14297a().mo14178w(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        int i3 = 1;
        super.onActivityResult(i, i2, intent);
        if (i2 == 299) {
            getDataSubscription(true);
        } else if (!(i2 == -1 || i2 == 307)) {
            this.messagePhoto.setImageBitmap(null);
            clearPhoto();
            clearVideo();
            return;
        }
        if (i == 801) {
            checkAndUploadVideo();
        } else if (this.photoHelper != null && this.photoHelper.f10753f != null && this.photoHelper.f10749b != null && this.photoHelper.f10748a != null) {
            this.messagePhoto.setImageBitmap(this.photoHelper.f10753f);
            if (this.photoHelper.f10751d > 0 && this.photoHelper.f10752e > 0) {
                LayoutParams layoutParams = (LayoutParams) this.messagePhoto.getLayoutParams();
                layoutParams.width = (int) ((((float) this.photoHelper.f10751d) / ((float) this.photoHelper.f10752e)) * ((float) layoutParams.height));
                this.messagePhoto.setLayoutParams(layoutParams);
            }
            if (this.photoHelper.f10754g != 0) {
                i3 = 0;
            }
            this.f6454j = i3;
            this.messagePhoto.setVisibility(0);
            this.messagePhotoClear.setVisibility(0);
            showMessageInput();
            imageCaptured();
            updatePostButton();
        } else if (i2 == 307) {
            String stringExtra = intent.getStringExtra("gif_url");
            final int intExtra = intent.getIntExtra("gif_width", 0);
            final int intExtra2 = intent.getIntExtra("gif_height", 0);
            this.messagePostBtnSpinny.setVisibility(0);
            this.postButton.setVisibility(8);
            this.messagePhoto.setVisibility(0);
            LayoutParams layoutParams2 = (LayoutParams) this.messagePhoto.getLayoutParams();
            layoutParams2.width = (layoutParams2.height * 16) / 9;
            this.messagePhoto.setLayoutParams(layoutParams2);
            this.f6454j = 1;
            showMessageInput();
            GossipApplication.f6095c.mo14650a(stringExtra).mo14599k().mo14540a().mo14558b(Priority.IMMEDIATE).mo14572d(17301612).mo14569c((int) R.mipmap.ic_launcher).mo14629c().mo14559b(DiskCacheStrategy.SOURCE).mo14565b((RequestListener<? super ModelType, GifDrawable>) new RequestListener<String, GifDrawable>() {
                public boolean onException(Exception exc, String str, Target<GifDrawable> sdVar, boolean z) {
                    return false;
                }

                public boolean onResourceReady(GifDrawable psVar, String str, Target<GifDrawable> sdVar, boolean z, boolean z2) {
                    byte[] d = psVar.mo15890d();
                    MessageActivity.this.photoHelper = MessageActivity.this.getPhotoHelper();
                    MessageActivity.this.photoHelper.mo14336a(d);
                    MessageActivity.this.photoHelper.f10751d = intExtra;
                    MessageActivity.this.photoHelper.f10752e = intExtra2;
                    MessageActivity.this.messagePhotoClear.setVisibility(0);
                    MessageActivity.this.messagePostBtnSpinny.setVisibility(8);
                    MessageActivity.this.postButton.setVisibility(0);
                    MessageActivity.this.updatePostButton();
                    return false;
                }
            }).mo14554a(this.messagePhoto);
        }
    }

    public void onBackPressed() {
        if (this.mVideoPlayerFullscreen.getVisibility() == 0) {
            this.mVideoPlayerFullscreen.removeAllViews();
            this.mVideoPlayerFullscreen.setVisibility(8);
            this.contentRootView.setVisibility(0);
            return;
        }
        try {
            Intent intent = new Intent();
            intent.putExtra("comment_id", this.f6450f);
            setResult(PostDetailsActivity.MESSAGING, intent);
            super.onBackPressed();
            finish();
        } catch (IllegalStateException e) {
            Crashlytics.m16437a((Throwable) e);
            finish();
        }
    }

    public void onCacheAvailable(File file, String str, int i) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            return;
        }
        if (extras.containsKey("post_id")) {
            this.f6448d = extras.getString("post_id");
        }
        if (extras.containsKey("user_name")) {
            this.f6449e = extras.getString("user_name");
        }
        if (extras.containsKey("comment_id")) {
            this.f6450f = extras.getString("comment_id");
        }
        this.f6447b = CandidAnimals.m14509a();
        this.f6459o = new Handler(getMainLooper());
        setContentView((int) R.layout.activity_message);
        ButterKnife.bind((Activity) this);
        enableKeyboardEvents(this.root);
        if (AppState.account.messaging_disabled == 1) {
            this.blockedFrameLayout.setVisibility(0);
            new Builder(this).setTitle((CharSequence) "Enable Messaging?").setMessage((CharSequence) "You've previously disabled messaging. Do you want to enable it?").setNegativeButton((CharSequence) getResources().getString(R.string.no), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    MessageActivity.this.finish();
                }
            }).setPositiveButton((CharSequence) getResources().getString(R.string.yes), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("messaging_disabled", "0");
                    ApiService.m14297a().mo14173r(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                    AppState.account.messaging_disabled = 0;
                    RxBus.m14573a().mo14349a(new C2616ae(0));
                    MessageActivity.this.blockedFrameLayout.setVisibility(8);
                }
            }).setCancelable(false).create().show();
        }
        if (AppState.config.getInt("android_video_messaging_enabled", 1) == 0 || VERSION.SDK_INT < 18) {
            this.messageVideoButton.setVisibility(8);
        } else {
            this.messageVideoButton.setVisibility(0);
        }
        this.stickerKeyboard.f7413a = this;
        this.f6461q = new LinearLayoutManager(this);
        this.messageRecycler.setLayoutManager(this.f6461q);
        this.f6461q.setStackFromEnd(true);
        this.f6457m = new MessagesAdapter(this, this.f6464t);
        this.messageRecycler.setAdapter(this.f6457m);
        this.messageRecycler.addOnScrollListener(new PagingScrollListener(this.f6461q, this.f6457m, 1, 25) {
            public void onLoadMore(String str) {
                if (str != null) {
                    MessageActivity.this.getPage(str);
                }
            }

            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                if (i == 0 && !MessageActivity.this.f6457m.mo13771e().isEmpty() && MessageActivity.this.f6465u != null && MessageActivity.this.f6466v != null) {
                    MessageActivity.this.f6465u.mo8844a(MessageActivity.this.f6466v, MessageActivity.this.f6461q.findFirstVisibleItemPosition(), MessageActivity.this.f6461q.findLastVisibleItemPosition());
                }
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                super.onScrolled(recyclerView, i, i2);
                MessageActivity.this.messageRecycler.post(new Runnable() {
                    public void run() {
                        if (MessageActivity.this.f6465u != null && MessageActivity.this.f6466v != null) {
                            MessageActivity.this.f6465u.mo8844a(MessageActivity.this.f6466v, MessageActivity.this.f6461q.findFirstVisibleItemPosition(), MessageActivity.this.f6461q.findLastVisibleItemPosition());
                        }
                    }
                });
            }
        });
        if (AppState.config.getInt("android_video_messaging_enabled", 1) == 1 && AppState.config.getInt("android_autoplay_video_enabled", 1) == 1) {
            this.f6466v = new bdv(this.f6461q, this.messageRecycler);
            this.f6465u = new bdr(new bdp(), this.f6457m.mo13771e());
        }
        ItemAnimator itemAnimator = this.messageRecycler.getItemAnimator();
        if (itemAnimator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        this.messageInput.addTextChangedListener(new AfterChangeTextWatcher() {
            public void afterTextChanged(Editable editable) {
                MessageActivity.this.updatePostButton();
            }
        });
        this.messageSpinnyContainer.setVisibility(0);
        addToSubscriptionList(getDataSubscription(false));
        busSubscribe(C2614ac.class, new bjy<C2614ac>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2614ac acVar) {
            }
        });
        busSubscribe(RxBus.m14573a().mo14348a(C2642bd.class, (bjy<T>) new bjy<C2642bd>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2642bd bdVar) {
                MessageActivity.this.closeKeyboard();
                FullScreenVideoPlayer fullScreenVideoPlayer = new FullScreenVideoPlayer(MessageActivity.this, bdVar.f10627a, bdVar.f10628b, bdVar.f10630d, bdVar.f10631e, bdVar.f10629c);
                MessageActivity.this.mVideoPlayerFullscreen.setVisibility(0);
                MessageActivity.this.contentRootView.setVisibility(8);
                MessageActivity.this.mVideoPlayerFullscreen.addView(fullScreenVideoPlayer);
            }
        }));
        busSubscribe(RxBus.m14573a().mo14348a(C2641bc.class, (bjy<T>) new bjy<C2641bc>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2641bc bcVar) {
                MessageActivity.this.mVideoPlayerFullscreen.removeAllViews();
                MessageActivity.this.mVideoPlayerFullscreen.setVisibility(8);
                MessageActivity.this.contentRootView.setVisibility(0);
            }
        }));
        if (AppState.config.getInt("android_gif_search_enabled") == 1) {
            this.messageGifButton.setVisibility(0);
            this.messageGifButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    MessageActivity.this.clearPhoto(null);
                    MessageActivity.this.startActivityForResult(new Intent(MessageActivity.this, GifSearchActivity.class), 307);
                }
            });
            return;
        }
        this.messageGifButton.setVisibility(8);
        this.messageGifButton.setOnClickListener(null);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getDataSubscription(true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.f6446a);
        this.f6459o.removeCallbacks(this.f6467w);
        this.f6464t.mo8822d();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        IntentFilter intentFilter = new IntentFilter("com.becandid.candid.MESSAGING");
        intentFilter.setPriority(2);
        registerReceiver(this.f6446a, intentFilter);
        this.f6459o.postDelayed(this.f6467w, 60000);
        RxBus.m14573a().mo14349a(new C2637az(2, 0, true));
        super.onResume();
        if (!this.f6457m.mo13771e().isEmpty()) {
            this.messageRecycler.post(new Runnable() {
                public void run() {
                    if (MessageActivity.this.f6465u != null && MessageActivity.this.f6466v != null) {
                        MessageActivity.this.f6465u.mo8844a(MessageActivity.this.f6466v, MessageActivity.this.f6461q.findFirstVisibleItemPosition(), MessageActivity.this.f6461q.findLastVisibleItemPosition());
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.f6464t.mo8823e();
        GossipApplication.m9013a(this).mo16742a((CacheListener) this);
    }

    public void onVideoCompressionStarted() {
        View findViewById = findViewById(R.id.message_input_buttons);
        if (findViewById != null) {
            Snackbar.m3350a(findViewById, getResources().getString(R.string.process_video), -2).mo1721a();
        }
    }

    public void onVideoPostUploaded(BaseVideoMessage baseVideoMessage) {
        if (baseVideoMessage instanceof Message) {
            Message message = (Message) baseVideoMessage;
            message.setVideoPlayManager(this.f6464t);
            HttpProxyCacheServer a = GossipApplication.m9013a(this);
            a.mo16743a((CacheListener) this, message.video_url);
            message.setVideoUrl(a.mo16740a(message.video_url));
            this.f6463s = message;
            if (this.f6463s != null) {
                this.f6457m.mo13877a(this.f6463s);
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        MessageActivity.this.scrollToBottom();
                    }
                }, 200);
                onVideoUploadSuccess();
            }
        }
    }

    public void onVideoUploadFailed() {
        showSnack(false);
    }

    public void onVideoUploadSuccess() {
        showSnack(true);
    }

    public void onVideoUploading() {
        View findViewById = findViewById(R.id.message_input_buttons);
        if (findViewById != null) {
            Snackbar.m3350a(findViewById, getResources().getString(R.string.upload_video), -2).mo1721a();
        }
    }

    @OnClick({2131624150})
    public void saveMessage(View view) {
        this.messageSpinnyContainer.setVisibility(0);
        if (this.photoHelper != null && this.photoHelper.f10749b != null && this.photoHelper.f10748a != null) {
            ApiService.m14297a().mo14129c(this.photoHelper.f10749b, this.photoHelper.f10748a).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<UploadMediaResponse>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Toast.makeText(MessageActivity.this, MessageActivity.this.getString(R.string.unable_to_upload_image), 0).show();
                    MessageActivity.this.messageSpinnyContainer.setVisibility(8);
                }

                public void onNext(UploadMediaResponse uploadMediaResponse) {
                    if (uploadMediaResponse != null) {
                        MessageActivity.this.f6453i = uploadMediaResponse.full_url;
                        MessageActivity.this.m9184a();
                    }
                }
            });
        } else if (this.photoHelper == null || this.photoHelper.f10758k == null) {
            m9184a();
        } else {
            ApiService.m14297a().mo14112a(this.photoHelper.f10758k).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<UploadMediaResponse>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Toast.makeText(MessageActivity.this, MessageActivity.this.getString(R.string.unable_to_upload_image), 0).show();
                    MessageActivity.this.messageSpinnyContainer.setVisibility(8);
                }

                public void onNext(UploadMediaResponse uploadMediaResponse) {
                    if (uploadMediaResponse != null) {
                        MessageActivity.this.f6453i = uploadMediaResponse.full_url;
                        MessageActivity.this.m9184a();
                    }
                }
            });
        }
    }

    public void scrollToBottom() {
        this.messageRecycler.smoothScrollToPosition(this.messageRecycler.getBottom());
    }

    public void setupActivityHeader(User user, Post post, User user2, MessageThread messageThread) {
        int parseColor = Color.parseColor(user.icon_color);
        this.messageHeaderUserIcon.setTextColor(parseColor);
        TextView textView = this.messageHeaderUserIcon;
        CandidAnimals ivVar = this.f6447b;
        textView.setTypeface(CandidAnimals.m14511b());
        TextView textView2 = this.messageHeaderUserIcon;
        CandidAnimals ivVar2 = this.f6447b;
        textView2.setText(CandidAnimals.m14510a(user.icon_name));
        int width = this.messageHeaderUserIconContainer.getWidth();
        if (width == 0) {
            width = (int) (this.messageHeaderUserIconContainer.getResources().getDisplayMetrics().density * 28.0f);
        }
        int i = (16777215 & parseColor) | 1073741824;
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(i);
        gradientDrawable.setCornerRadius((float) (width / 2));
        this.messageHeaderUserIconContainer.setBackground(gradientDrawable);
        if (user.online == 1) {
            this.headerOnline.setVisibility(0);
        } else {
            this.headerOnline.setVisibility(8);
        }
        Triangle klVar = new Triangle();
        klVar.setBounds(0, C2699jj.m14599a(1, (Context) this), C2699jj.m14599a(10, (Context) this), C2699jj.m14599a(6, (Context) this));
        klVar.mo14475a(parseColor);
        this.headerName.setCompoundDrawables(null, null, klVar, null);
        if (AppState.config.getInt("enable_nickname", 1) != 1 || user.nickname == null || user.nickname.equals("")) {
            this.headerName.setText(user.post_name);
        } else {
            this.headerName.setText(user.nickname);
        }
        this.headerName.setTextColor(parseColor);
        this.headerName.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(MessageActivity.this, view);
                popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.message_user_menu_block /*2131625146*/:
                                new Builder(MessageActivity.this).setTitle((CharSequence) "Block this Thread?").setMessage((CharSequence) "Are you sure you want to block this thread?").setNegativeButton((CharSequence) "No", (DialogInterface.OnClickListener) null).setPositiveButton((CharSequence) "Yes", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        RxBus.m14573a().mo14349a(new C2656m(Integer.parseInt(MessageActivity.this.f6448d), MessageActivity.this.f6449e));
                                        HashMap hashMap = new HashMap();
                                        hashMap.put("post_id", MessageActivity.this.f6448d);
                                        hashMap.put("post_name", MessageActivity.this.f6449e);
                                        ApiService.m14297a().mo14174s(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                                        MessageActivity.this.finish();
                                    }
                                }).show();
                                break;
                            case R.id.message_user_menu_delete_thread /*2131625148*/:
                                RxBus.m14573a().mo14349a(new C2656m(Integer.parseInt(MessageActivity.this.f6448d), MessageActivity.this.f6449e));
                                HashMap hashMap = new HashMap();
                                hashMap.put("post_id", MessageActivity.this.f6448d);
                                hashMap.put("post_name", MessageActivity.this.f6449e);
                                ApiService.m14297a().mo14176u(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                                MessageActivity.this.finish();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.getMenu().add(1, R.id.message_user_menu_block, 0, R.string.message_user_menu_block);
                popupMenu.getMenu().add(1, R.id.message_user_menu_delete_thread, 0, R.string.message_user_menu_delete_thread);
                popupMenu.show();
            }
        });
        if (post != null) {
            this.messageHeaderPostContainer.setVisibility(0);
            if (this.f6450f != null) {
                this.messageHeaderPostText.setText("See Comment");
            } else {
                this.messageHeaderPostText.setText("See Post");
            }
            Arrow kgVar = new Arrow();
            kgVar.setBounds(0, C2699jj.m14599a(2, (Context) this), C2699jj.m14599a(8, (Context) this), C2699jj.m14599a(10, (Context) this));
            kgVar.mo14465a(getResources().getColor(R.color.gossip_grey));
            this.messageHeaderRightArrow.setImageDrawable(kgVar);
            this.messageHeaderPostContainer.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), PostDetailsActivity.class);
                    intent.putExtra("post_id", Integer.parseInt(MessageActivity.this.f6448d));
                    if (MessageActivity.this.f6450f != null) {
                        intent.putExtra("comment_id", Integer.parseInt(MessageActivity.this.f6450f));
                        intent.putExtra("scrollToBottom", true);
                    }
                    MessageActivity.this.startActivity(intent);
                }
            });
        } else {
            this.messageHeaderPostContainer.setVisibility(8);
        }
        if (AppState.config.getInt("enable_nickname", 1) == 1 && user2.nickname != null && !user2.nickname.equals("")) {
            this.mNickname.setText("Chatting as " + user2.nickname);
            this.mNickname.setVisibility(0);
        } else if (AppState.config.getInt("enable_nickname", 1) == 1 && messageThread.can_add_nickname == 1) {
            this.mNickname.setText("Add Nickname");
            this.mNickname.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    MessageActivity messageActivity = MessageActivity.this;
                    BadgeType badgeType = BadgeType.BADGE;
                    messageActivity.m9188a(BadgeType.MESSAGE_ADD_NICKNAME);
                }
            });
            this.mNickname.setVisibility(0);
        } else {
            this.mNickname.setVisibility(8);
        }
    }

    public void setupBlockedUser() {
        this.blockedFrameLayout.setVisibility(0);
        new Builder(this).setTitle((CharSequence) "Unblock This Thread?").setMessage((CharSequence) "You've previously blocked this thread. Do you want to unblock it?").setNegativeButton((CharSequence) getResources().getString(R.string.no), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MessageActivity.this.finish();
            }
        }).setPositiveButton((CharSequence) getResources().getString(R.string.yes), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                HashMap hashMap = new HashMap();
                hashMap.put("post_id", MessageActivity.this.f6448d);
                hashMap.put("post_name", MessageActivity.this.f6449e);
                ApiService.m14297a().mo14175t(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                MessageActivity.this.blockedFrameLayout.setVisibility(8);
            }
        }).setCancelable(false).create().show();
    }

    public void setupNicknameAlert(String str) {
        this.headerName.setText(str);
        m9188a(BadgeType.MESSAGE_SECOND_NICKNAME);
    }

    public void setupRequest() {
        this.requestButtons.setVisibility(0);
        this.requestIgnore.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MessageActivity.this.requestIgnore.setEnabled(false);
                MessageActivity.this.requestAccept.setEnabled(true);
                HashMap hashMap = new HashMap();
                hashMap.put("post_id", MessageActivity.this.f6448d);
                hashMap.put("post_name", MessageActivity.this.f6449e);
                hashMap.put("ignore", "1");
                ApiService.m14297a().mo14177v(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                        Log.d("HandleRequest", th.toString());
                        MessageActivity.this.requestIgnore.setEnabled(true);
                    }

                    public void onNext(NetworkData networkData) {
                        if (networkData.success) {
                            RxBus.m14573a().mo14349a(new C2612aa(MessageActivity.this.f6448d, MessageActivity.this.f6449e, 1));
                            MessageActivity.this.finish();
                            return;
                        }
                        MessageActivity.this.requestIgnore.setEnabled(true);
                    }
                });
            }
        });
        this.requestAccept.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MessageActivity.this.requestIgnore.setEnabled(false);
                MessageActivity.this.requestAccept.setEnabled(true);
                HashMap hashMap = new HashMap();
                hashMap.put("post_id", MessageActivity.this.f6448d);
                hashMap.put("post_name", MessageActivity.this.f6449e);
                hashMap.put("ignore", "0");
                ApiService.m14297a().mo14177v(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                        Log.d("HandleRequest", th.toString());
                        MessageActivity.this.requestIgnore.setEnabled(true);
                        MessageActivity.this.requestAccept.setEnabled(true);
                    }

                    public void onNext(NetworkData networkData) {
                        if (networkData.success) {
                            MessageActivity.this.requestButtons.setVisibility(8);
                            RxBus.m14573a().mo14349a(new C2612aa(MessageActivity.this.f6448d, MessageActivity.this.f6449e, 0));
                            return;
                        }
                        MessageActivity.this.requestIgnore.setEnabled(true);
                        MessageActivity.this.requestAccept.setEnabled(true);
                    }
                });
            }
        });
    }

    public void showCurrentScreen() {
        this.contentRootView.setVisibility(0);
    }

    public void showMessageInput() {
        this.messageInputContainer.setVisibility(0);
        this.messagePhotoButton.setAlpha(0.3f);
        this.messagePhotoButton.setEnabled(false);
        this.messageVideoButton.setAlpha(0.3f);
        this.messageVideoButton.setEnabled(false);
        this.stickerButton.setAlpha(0.3f);
        this.stickerButton.setEnabled(false);
        this.messageGifButton.setAlpha(0.3f);
        this.messageGifButton.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void showSnack(boolean z) {
        super.showSnack(z);
        View findViewById = findViewById(R.id.message_input_buttons);
        if (findViewById == null) {
            return;
        }
        if (!z) {
            Snackbar.m3350a(findViewById, getResources().getString(R.string.uploaded_video_failed), -1).mo1721a();
        } else {
            Snackbar.m3350a(findViewById, getResources().getString(R.string.uploaded_video_successfully), -1).mo1721a();
        }
    }

    public void startMessage(View view) {
        this.messageInputContainer.setVisibility(0);
        this.messageInputButtonsVisible.setVisibility(8);
        this.messageInputButtonsHidden.setVisibility(0);
        this.messageInput.requestFocus();
        this.messageInput.post(new Runnable() {
            public void run() {
                MessageActivity.this.openKeyboard();
            }
        });
    }

    public void takePhoto() {
        clearPhoto(null);
        if (AppState.config.getInt("android_msg_image_upload", 1) == 1) {
            getPhotoHelper().choosePhoto(false);
        } else {
            getPhotoHelper().choosePhoto(true);
        }
    }

    public void takeVideo() {
        clearPhoto(null);
        if (getVideoHelper() != null) {
            getVideoHelper().mo14412a();
        }
        VideoHelper jtVar = new VideoHelper(this);
        jtVar.mo14417a((C2716c) this);
        jtVar.mo14421c(this.f6450f);
        jtVar.mo14416a(this.f6448d);
        jtVar.mo14419b(this.f6449e);
        jtVar.mo14420c();
    }

    public void updateNickname(final String str) {
        AppState.nickname = str;
        AppState.saveState(this);
        ApiService.m14297a().mo14132c(str, this.f6448d, this.f6449e).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(NetworkData networkData) {
                if (networkData.success) {
                    MessageActivity.this.mNickname.setVisibility(0);
                    MessageActivity.this.mNickname.setText("Chatting as " + str);
                    MessageActivity.this.mNickname.setOnClickListener(null);
                }
            }
        });
    }

    public void updatePostButton() {
        if (this.messageInput.getText().toString().trim().length() <= 0 && this.f6453i == null && this.f6455k == null && ((this.photoHelper == null || this.photoHelper.f10753f == null) && (this.photoHelper == null || this.photoHelper.f10758k == null))) {
            this.postButton.setImageResource(R.drawable.comment_send_grey);
            this.postButton.setClickable(false);
            return;
        }
        this.postButton.setImageResource(R.drawable.comment_send_orange);
        this.postButton.setClickable(true);
    }
}
