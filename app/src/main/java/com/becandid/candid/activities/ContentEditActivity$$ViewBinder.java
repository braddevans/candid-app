package com.becandid.candid.activities;

import android.support.p003v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.ContentEditActivity;

public class ContentEditActivity$$ViewBinder<T extends ContentEditActivity> implements ViewBinder<T> {

    /* compiled from: ContentEditActivity$$ViewBinder */
    public static class InnerUnbinder<T extends ContentEditActivity> implements Unbinder {

        /* renamed from: a */
        private T f6233a;

        protected InnerUnbinder(T t) {
            this.f6233a = t;
        }

        public final void unbind() {
            if (this.f6233a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6233a);
            this.f6233a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.root = null;
            t.editContainer = null;
            t.editText = null;
            t.editSave = null;
            t.editCancel = null;
            t.editSpinny = null;
            t.editDetails = null;
            t.editHeaderTitle = null;
            t.editTitle = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.root = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_root, "field 'root'"), R.id.content_edit_root, "field 'root'");
        t.editContainer = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_container, "field 'editContainer'"), R.id.content_edit_container, "field 'editContainer'");
        t.editText = (AppCompatEditText) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_content, "field 'editText'"), R.id.content_edit_content, "field 'editText'");
        t.editSave = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_save, "field 'editSave'"), R.id.content_edit_save, "field 'editSave'");
        t.editCancel = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_cancel, "field 'editCancel'"), R.id.content_edit_cancel, "field 'editCancel'");
        t.editSpinny = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_progress_bar, "field 'editSpinny'"), R.id.content_edit_progress_bar, "field 'editSpinny'");
        t.editDetails = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_details, "field 'editDetails'"), R.id.content_edit_details, "field 'editDetails'");
        t.editHeaderTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_header_title, "field 'editHeaderTitle'"), R.id.content_edit_header_title, "field 'editHeaderTitle'");
        t.editTitle = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.content_edit_header, "field 'editTitle'"), R.id.content_edit_header, "field 'editTitle'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
