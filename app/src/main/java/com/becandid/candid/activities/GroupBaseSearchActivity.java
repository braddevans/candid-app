package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.SearchView;
import android.support.p003v7.widget.SearchView.OnQueryTextListener;
import android.support.p003v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;

public class GroupBaseSearchActivity extends BaseActivity {

    /* renamed from: a */
    MenuItem f6346a;
    @BindView(2131624105)
    TextView mEmptyText;
    public QueryTextInterface mQueryTextInterface;
    @BindView(2131624104)
    View mSearchEmpty;
    @BindView(2131624106)
    View mSearchProgressBar;
    @BindView(2131624103)
    RecyclerView mSearchResultRecyclerView;
    protected SearchView mSearchView;
    @BindView(2131624102)
    View root;

    public interface QueryTextInterface {
        void onQueryTextChange(String str);

        void onQueryTextSubmit(String str);
    }

    public void onBackPressed() {
        if (this.mSearchView == null || this.f6346a == null || this.f6346a.isActionViewExpanded()) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_group_base_search);
        ButterKnife.bind((Activity) this);
        enableKeyboardEvents(this.root);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_search_menu, menu);
        this.f6346a = menu.findItem(R.id.action_search);
        this.mSearchView = (SearchView) MenuItemCompat.m12790a(this.f6346a);
        this.f6346a.expandActionView();
        this.mSearchView.requestFocus();
        this.mSearchView.setQueryHint("Search ...");
        this.mSearchView.setOnQueryTextListener(new OnQueryTextListener() {
            public boolean onQueryTextChange(String str) {
                if (GroupBaseSearchActivity.this.mQueryTextInterface != null) {
                    GroupBaseSearchActivity.this.mQueryTextInterface.onQueryTextChange(str);
                }
                return false;
            }

            public boolean onQueryTextSubmit(String str) {
                if (GroupBaseSearchActivity.this.mQueryTextInterface != null) {
                    GroupBaseSearchActivity.this.mQueryTextInterface.onQueryTextSubmit(str);
                }
                GroupBaseSearchActivity.this.mSearchView.clearFocus();
                return true;
            }
        });
        MenuItemCompat.m12789a(this.f6346a, (C2327e) new C2327e() {
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                GroupBaseSearchActivity.this.closeKeyboard();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        GroupBaseSearchActivity.this.finish();
                    }
                }, 500);
                return false;
            }

            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
