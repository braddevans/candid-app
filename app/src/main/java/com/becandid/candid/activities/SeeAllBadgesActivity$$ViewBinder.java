package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.SeeAllBadgesActivity;

public class SeeAllBadgesActivity$$ViewBinder<T extends SeeAllBadgesActivity> implements ViewBinder<T> {

    /* compiled from: SeeAllBadgesActivity$$ViewBinder */
    public static class InnerUnbinder<T extends SeeAllBadgesActivity> implements Unbinder {

        /* renamed from: a */
        private T f6665a;

        protected InnerUnbinder(T t) {
            this.f6665a = t;
        }

        public final void unbind() {
            if (this.f6665a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6665a);
            this.f6665a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mRecyclerView = null;
            t.mBadgeInfoContainer = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mRecyclerView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.see_all_badges_recyclerview, "field 'mRecyclerView'"), R.id.see_all_badges_recyclerview, "field 'mRecyclerView'");
        t.mBadgeInfoContainer = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.badge_info_container, "field 'mBadgeInfoContainer'"), R.id.badge_info_container, "field 'mBadgeInfoContainer'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
