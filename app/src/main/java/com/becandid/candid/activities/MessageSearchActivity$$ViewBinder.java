package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.MessageSearchActivity;

public class MessageSearchActivity$$ViewBinder<T extends MessageSearchActivity> implements ViewBinder<T> {

    /* compiled from: MessageSearchActivity$$ViewBinder */
    public static class InnerUnbinder<T extends MessageSearchActivity> implements Unbinder {

        /* renamed from: a */
        private T f6523a;

        protected InnerUnbinder(T t) {
            this.f6523a = t;
        }

        public final void unbind() {
            if (this.f6523a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6523a);
            this.f6523a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.resultRecycler = null;
            t.searchEdit = null;
            t.searchProgressBar = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.resultRecycler = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.message_search_results, "field 'resultRecycler'"), R.id.message_search_results, "field 'resultRecycler'");
        t.searchEdit = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.message_search_edit, "field 'searchEdit'"), R.id.message_search_edit, "field 'searchEdit'");
        t.searchProgressBar = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_search_progress_bar, "field 'searchProgressBar'"), R.id.message_search_progress_bar, "field 'searchProgressBar'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
