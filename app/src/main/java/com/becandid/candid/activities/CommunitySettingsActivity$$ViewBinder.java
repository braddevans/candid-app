package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.CommunitySettingsActivity;

public class CommunitySettingsActivity$$ViewBinder<T extends CommunitySettingsActivity> implements ViewBinder<T> {

    /* compiled from: CommunitySettingsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends CommunitySettingsActivity> implements Unbinder {

        /* renamed from: a */
        private T f6211a;

        protected InnerUnbinder(T t) {
            this.f6211a = t;
        }

        public final void unbind() {
            if (this.f6211a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6211a);
            this.f6211a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.highSchoolButton = null;
            t.highSchoolRecycler = null;
            t.highSchoolEmpty = null;
            t.collegeButton = null;
            t.collegeRecycler = null;
            t.collegeEmpty = null;
            t.workButton = null;
            t.workRecycler = null;
            t.workEmpty = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.highSchoolButton = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_highschool_button, "field 'highSchoolButton'"), R.id.community_settings_highschool_button, "field 'highSchoolButton'");
        t.highSchoolRecycler = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_highschool_recycler, "field 'highSchoolRecycler'"), R.id.community_settings_highschool_recycler, "field 'highSchoolRecycler'");
        t.highSchoolEmpty = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_highschool_recycler_empty, "field 'highSchoolEmpty'"), R.id.community_settings_highschool_recycler_empty, "field 'highSchoolEmpty'");
        t.collegeButton = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_college_button, "field 'collegeButton'"), R.id.community_settings_college_button, "field 'collegeButton'");
        t.collegeRecycler = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_college_recycler, "field 'collegeRecycler'"), R.id.community_settings_college_recycler, "field 'collegeRecycler'");
        t.collegeEmpty = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_college_recycler_empty, "field 'collegeEmpty'"), R.id.community_settings_college_recycler_empty, "field 'collegeEmpty'");
        t.workButton = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_work_button, "field 'workButton'"), R.id.community_settings_work_button, "field 'workButton'");
        t.workRecycler = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_work_recycler, "field 'workRecycler'"), R.id.community_settings_work_recycler, "field 'workRecycler'");
        t.workEmpty = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.community_settings_work_recycler_empty, "field 'workEmpty'"), R.id.community_settings_work_recycler_empty, "field 'workEmpty'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
