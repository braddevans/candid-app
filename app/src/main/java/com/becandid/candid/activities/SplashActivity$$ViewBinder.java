package com.becandid.candid.activities;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.SplashActivity;

public class SplashActivity$$ViewBinder<T extends SplashActivity> implements ViewBinder<T> {

    /* compiled from: SplashActivity$$ViewBinder */
    public static class InnerUnbinder<T extends SplashActivity> implements Unbinder {

        /* renamed from: a */
        private T f6677a;

        protected InnerUnbinder(T t) {
            this.f6677a = t;
        }

        public final void unbind() {
            if (this.f6677a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6677a);
            this.f6677a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mSpinny = null;
            t.mChangeEndpoint = null;
            t.termsAndGuidelines = null;
            t.onboardingButton = null;
            t.recoverAccountButton = null;
            t.recoverAccountText = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mSpinny = (View) finder.findRequiredView(obj, R.id.spinny, "field 'mSpinny'");
        t.mChangeEndpoint = (View) finder.findRequiredView(obj, R.id.change_endpoint, "field 'mChangeEndpoint'");
        t.termsAndGuidelines = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.terms_and_guidelines, "field 'termsAndGuidelines'"), R.id.terms_and_guidelines, "field 'termsAndGuidelines'");
        t.onboardingButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.onboarding_button, "field 'onboardingButton'"), R.id.onboarding_button, "field 'onboardingButton'");
        t.recoverAccountButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.recover_account_button, "field 'recoverAccountButton'"), R.id.recover_account_button, "field 'recoverAccountButton'");
        t.recoverAccountText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.recover_account_text, "field 'recoverAccountText'"), R.id.recover_account_text, "field 'recoverAccountText'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
