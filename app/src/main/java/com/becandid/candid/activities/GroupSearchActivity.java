package com.becandid.candid.activities;

import android.os.Bundle;
import android.support.p003v7.widget.LinearLayoutManager;
import android.view.View;
import com.becandid.candid.activities.GroupBaseSearchActivity.QueryTextInterface;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.GroupSearchResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class GroupSearchActivity extends GroupBaseSearchActivity {
    /* access modifiers changed from: private */

    /* renamed from: b */
    public List<GroupSearchResult> f6373b;

    /* renamed from: d */
    private bjz f6374d;

    /* renamed from: a */
    private void m9132a() {
        this.f6373b.clear();
        this.mSearchResultRecyclerView.getAdapter().notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9133a(ays ays) {
        GroupSearchResult groupSearchResult = new GroupSearchResult();
        try {
            groupSearchResult.groupName = ays.mo8410b("group_name").mo8385c();
            groupSearchResult.groupId = ays.mo8410b("group_id").mo8390g();
            groupSearchResult.firstTag = ays.mo8410b("first_tag").mo8385c();
            groupSearchResult.thumbUrl = ays.mo8410b("thumb_url").mo8385c();
            this.f6373b.add(groupSearchResult);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9136a(String str) {
        if (str.length() >= 3) {
            m9138b(str.toString());
            return;
        }
        this.mSearchProgressBar.setVisibility(8);
        this.mSearchResultRecyclerView.setVisibility(0);
    }

    /* renamed from: a */
    private void m9137a(Map<String, String> map) {
        if (this.f6374d != null) {
            this.f6374d.unsubscribe();
        }
        this.f6374d = ApiService.m14297a().mo14161j(map).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                GroupSearchActivity.this.mSearchProgressBar.setVisibility(8);
                GroupSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
            }

            public void onNext(ays ays) {
                if (!GroupSearchActivity.this.isStopped) {
                    ayo n = ays.mo8410b("groups").mo8399n();
                    for (int i = 0; i < n.mo8381a(); i++) {
                        GroupSearchActivity.this.m9133a(n.mo8382a(i).mo8398m());
                    }
                    GroupSearchActivity.this.mSearchResultRecyclerView.getAdapter().notifyDataSetChanged();
                    GroupSearchActivity.this.mSearchProgressBar.setVisibility(8);
                    GroupSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
                }
            }
        });
    }

    /* renamed from: b */
    private void m9138b(String str) {
        this.mSearchProgressBar.setVisibility(0);
        this.mSearchResultRecyclerView.setVisibility(8);
        m9132a();
        HashMap hashMap = new HashMap();
        hashMap.put("query", str);
        if (AppState.location != null) {
            hashMap.put("location", AppState.location.getLatitude() + "," + AppState.location.getLongitude() + "@" + (AppState.location.hasAccuracy() ? Float.valueOf(AppState.location.getAccuracy()) : "50"));
        }
        m9137a((Map<String, String>) hashMap);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f6373b = new ArrayList();
        this.mSearchResultRecyclerView.setHasFixedSize(true);
        this.mSearchResultRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.mSearchResultRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this) {
            public void onItemClick(View view, int i) {
                try {
                    GroupSearchActivity.this.startActivity(GroupDetailsActivity.startGroupDetailsActivity(GroupSearchActivity.this.getBaseContext(), ((GroupSearchResult) GroupSearchActivity.this.f6373b.get(i)).groupId));
                } catch (ArrayIndexOutOfBoundsException e) {
                    Crashlytics.m16437a((Throwable) e);
                }
            }
        });
        this.mSearchResultRecyclerView.setAdapter(new GroupSearchAdapter(this.f6373b, this));
        this.mQueryTextInterface = new QueryTextInterface() {
            public void onQueryTextChange(String str) {
                GroupSearchActivity.this.m9136a(str);
            }

            public void onQueryTextSubmit(String str) {
                GroupSearchActivity.this.m9136a(str);
            }
        };
    }
}
