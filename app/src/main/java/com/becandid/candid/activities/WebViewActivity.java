package com.becandid.candid.activities;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import java.lang.reflect.InvocationTargetException;

public class WebViewActivity extends BaseActivity {

    /* renamed from: a */
    String f6687a;

    /* renamed from: b */
    boolean f6688b;
    @BindView(2131624394)
    TextView mCopyLink;
    @BindView(2131624101)
    ProgressBar mProgressBar;
    @BindView(2131624395)
    View mSaveLink;
    @BindView(2131624393)
    TextView mTitle;
    @BindView(2131624396)
    WebView mWebView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_web_view);
        ButterKnife.bind((Activity) this);
        if (getIntent().hasExtra("url")) {
            this.f6687a = getIntent().getStringExtra("url");
            this.mWebView.loadUrl(this.f6687a);
        }
        if (getIntent().hasExtra("title")) {
            this.mTitle.setText(getIntent().getStringExtra("title"));
        }
        this.f6688b = getIntent().getBooleanExtra("enable_link_copy", true);
        if (!getIntent().hasExtra("show_copy_link") || !getIntent().getBooleanExtra("show_copy_link", false)) {
            this.mCopyLink.setVisibility(8);
            if (this.f6688b) {
                this.mSaveLink.setVisibility(0);
                this.mSaveLink.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        ((ClipboardManager) WebViewActivity.this.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("webview_link", WebViewActivity.this.f6687a.trim()));
                        Toast.makeText(WebViewActivity.this, "Link copied", 0).show();
                    }
                });
            } else {
                this.mSaveLink.setVisibility(8);
            }
        } else {
            this.mCopyLink.setVisibility(0);
            this.mSaveLink.setVisibility(8);
            this.mCopyLink.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    RxBus.m14573a().mo14349a(new C2615ad(WebViewActivity.this.f6687a));
                    WebViewActivity.this.finish();
                }
            });
        }
        this.mProgressBar.setMax(100);
        this.mProgressBar.getProgressDrawable().setColorFilter(-16776961, Mode.SRC_IN);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setSupportMultipleWindows(true);
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView webView, int i) {
                super.onProgressChanged(webView, i);
                WebViewActivity.this.setValue(i);
            }

            public void onReceivedTitle(WebView webView, String str) {
                super.onReceivedTitle(webView, str);
            }
        });
        this.mWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                WebViewActivity.this.f6687a = str;
                WebViewActivity.this.mProgressBar.setVisibility(8);
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                WebViewActivity.this.f6687a = str;
                return true;
            }
        });
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(i, keyEvent);
        }
        this.mWebView.goBack();
        return true;
    }

    public void onPause() {
        super.onPause();
        try {
            Class.forName("android.webkit.WebView").getMethod("onPause", null).invoke(this.mWebView, null);
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
        }
    }

    public void onResume() {
        super.onResume();
        this.mWebView.onResume();
    }

    public void setValue(int i) {
        this.mProgressBar.setProgress(i);
    }
}
