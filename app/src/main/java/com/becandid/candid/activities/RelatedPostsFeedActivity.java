package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.data.Post;
import java.lang.reflect.Type;
import java.util.List;

public class RelatedPostsFeedActivity extends BaseActivity {

    /* renamed from: a */
    private List<Post> f6656a;

    /* renamed from: b */
    private FeedAdapter f6657b;
    @BindView(2131624347)
    RecyclerView feedRecycler;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_related_posts_feed);
        ButterKnife.bind((Activity) this);
        if (!getIntent().hasExtra("related_posts")) {
            finish();
        }
        Type type = new azz<List<Post>>() {
        }.getType();
        this.f6656a = (List) new aym().mo8358a(getIntent().getStringExtra("related_posts"), type);
        for (Post post : this.f6656a) {
            post.related_to_post = 0;
        }
        this.f6657b = new FeedAdapter(this, null);
        this.feedRecycler.setAdapter(this.f6657b);
        this.feedRecycler.setLayoutManager(new LinearLayoutManager(this));
        this.f6657b.mo13764a(this.f6656a);
    }
}
