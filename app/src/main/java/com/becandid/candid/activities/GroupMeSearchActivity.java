package com.becandid.candid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.p003v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;
import com.becandid.candid.activities.GroupBaseSearchActivity.QueryTextInterface;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.Group;
import java.util.ArrayList;
import java.util.List;

public class GroupMeSearchActivity extends GroupBaseSearchActivity {
    /* access modifiers changed from: private */

    /* renamed from: b */
    public ProfileGroupsAdapter f6369b;

    /* renamed from: d */
    private List<Group> f6370d = new ArrayList();

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9130a(String str) {
        ArrayList groupSearch = groupSearch(str);
        this.f6369b.mo13764a((List<T>) groupSearch);
        if (groupSearch.size() == 0) {
            this.mSearchResultRecyclerView.setVisibility(8);
            this.mSearchEmpty.setVisibility(0);
            return;
        }
        this.mSearchResultRecyclerView.setVisibility(0);
        this.mSearchEmpty.setVisibility(8);
    }

    public ArrayList<Group> groupSearch(String str) {
        ArrayList<Group> arrayList = new ArrayList<>();
        if (this.f6370d == null) {
            Toast.makeText(this, "Unable to find your groups", 1).show();
            finish();
        } else {
            for (Group group : this.f6370d) {
                String lowerCase = group.group_name.toLowerCase();
                if (lowerCase.startsWith(str.toLowerCase())) {
                    arrayList.add(0, group);
                } else if (lowerCase.contains(str.toLowerCase())) {
                    arrayList.add(group);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f6370d = (List) getIntent().getExtras().getSerializable("groups");
        this.mSearchResultRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, 1));
        this.f6369b = new ProfileGroupsAdapter(this);
        this.mSearchResultRecyclerView.setAdapter(this.f6369b);
        this.mSearchResultRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext()) {
            public void onItemClick(View view, int i) {
                Intent intent = new Intent(view.getContext(), GroupDetailsActivity.class);
                Group b = GroupMeSearchActivity.this.f6369b.mo13885b(i);
                if (b != null) {
                    intent.putExtra("group_id", b.group_id);
                    intent.putExtra("group_name", b.group_name);
                    intent.putExtra("group_image", b.imageUrl());
                    intent.putExtra("group_json", DataUtil.toJson(b));
                    view.getContext().startActivity(intent);
                }
            }
        });
        this.mQueryTextInterface = new QueryTextInterface() {
            public void onQueryTextChange(String str) {
                GroupMeSearchActivity.this.m9130a(str);
            }

            public void onQueryTextSubmit(String str) {
                GroupMeSearchActivity.this.m9130a(str);
            }
        };
    }
}
