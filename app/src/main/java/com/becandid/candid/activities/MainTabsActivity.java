package com.becandid.candid.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.C0411a;
import android.support.design.widget.TabLayout.C0416d;
import android.support.p001v4.app.Fragment;
import android.support.p003v7.app.AlertDialog.Builder;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.Group;
import com.becandid.candid.fragments.main_tabs.ActivityTabFragment;
import com.becandid.candid.fragments.main_tabs.FeedTabFragment;
import com.becandid.candid.fragments.main_tabs.GroupTabFragment;
import com.becandid.candid.fragments.main_tabs.MeTabFragment;
import com.becandid.candid.fragments.main_tabs.MessageTabFragment;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.services.RegistrationIntentService;
import com.becandid.candid.views.MainTabViewPager;
import com.becandid.thirdparty.BlurTask;
import com.becandid.thirdparty.BlurTask.BadgeType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class MainTabsActivity extends PopupWithBlurBackgroundActivity implements C0411a {
    public static final int CREATE_GROUP = 1001;
    public static final int CREATE_POST_REQUEST = 888;
    public static final String ONBOARDING_COMMUNITY_KEY = "onboarding_tag";
    public static final int PERMISSION_READ_CONTACTS = 1;

    /* renamed from: a */
    final BroadcastReceiver f6415a = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras.getString("activity_id") != null) {
                MainTabsActivity.this.busSubscribe(C2637az.class, new bjy<C2637az>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                    }

                    public void onNext(C2637az azVar) {
                        MainTabsActivity.this.m9168a(azVar);
                    }
                });
            }
            if (extras.getString("notification_type") != null && !extras.getString("notification_type").equals("new_comment_other")) {
                abortBroadcast();
            }
        }
    };

    /* renamed from: b */
    private final int f6416b = 999;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Handler f6417d;

    /* renamed from: e */
    private final int f6418e = 60000;

    /* renamed from: f */
    private List<String> f6419f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public MeTabFragment f6420g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public FeedTabFragment f6421h;

    /* renamed from: i */
    private MessageTabFragment f6422i;

    /* renamed from: j */
    private MainTabPagerAdapter f6423j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public Runnable f6424k = new Runnable() {
        public void run() {
            MainTabsActivity.this.f6417d.postDelayed(MainTabsActivity.this.f6424k, 60000);
        }
    };
    @BindView(2131624248)
    TabLayout mBottomTabs;
    @BindView(2131624265)
    View mCreatePostBtn;
    @BindView(2131624263)
    MainTabViewPager mViewPager;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public FeedTabFragment m9162a() {
        return (FeedTabFragment) this.f6423j.mo13887b(0);
    }

    /* renamed from: a */
    private void m9166a(int i, int i2, int i3) {
        if (this.mViewPager.getCurrentItem() != i3) {
            String str = "";
            String num = i > 99 ? "99+" : Integer.toString(i);
            View a = this.mBottomTabs.mo1765a(i3).mo1811a();
            if (a != null) {
                View findViewById = a.findViewById(R.id.tab_notification_badge);
                TextView textView = (TextView) a.findViewById(R.id.tab_notification_unread);
                if (i < 1) {
                    findViewById.setVisibility(8);
                    return;
                }
                findViewById.setVisibility(0);
                textView.setText(num);
                if (i2 == 2 && !AppState.hasMessagingShown) {
                    setShowMessagingPopup(true);
                    new BlurTask((Activity) this, findViewById(16908290), BadgeType.MESSAGE_ENABLED_NEW).execute(new Void[0]);
                    AppState.hasMessagingShown = true;
                    AppState.saveState(GossipApplication.m9012a());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9168a(C2637az azVar) {
        int i = azVar.f10618a;
        int i2 = azVar.f10619b;
        int currentItem = this.mViewPager.getCurrentItem();
        if (i == 3 && currentItem != 4) {
            AppState.account.unread_activity_count = i2;
            m9166a(i2, i, 4);
        } else if (i == 0 && currentItem != 0) {
            AppState.account.unread_feed_count = i2;
            m9166a(i2, i, 0);
        } else if (i == 1 && currentItem != 1) {
            AppState.account.unread_groups_count = i2;
            m9166a(i2, i, 1);
        } else if (i == 2 && currentItem != 3) {
            AppState.account.unread_message_count = i2;
            m9166a(i2, i, 3);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public MeTabFragment m9170b() {
        return (!AppState.config.experimentConfig.containsKey("messaging_turned_on") || !AppState.config.getBoolean("messaging_turned_on")) ? (MeTabFragment) this.f6423j.mo13887b(3) : (MeTabFragment) this.f6423j.mo13887b(4);
    }

    /* renamed from: c */
    private MessageTabFragment m9172c() {
        if (!AppState.config.experimentConfig.containsKey("messaging_turned_on") || !AppState.config.getBoolean("messaging_turned_on")) {
            return null;
        }
        return (MessageTabFragment) this.f6423j.mo13887b(3);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m9174d() {
        if (AppState.account != null) {
            ApiService.m14297a().mo14120b(Boolean.toString(false), (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    MainTabsActivity.this.m9176e();
                }

                public void onNext(NetworkData networkData) {
                    if (networkData.unread_count > 0) {
                        AppState.account.unread_activity_count = networkData.unread_count;
                    }
                    if (networkData.unread_groups_count > 0) {
                        AppState.account.unread_groups_count = networkData.unread_groups_count;
                    }
                    if (AppState.account.unread_activity_count > 0 && networkData.unread_count == 0) {
                        AppState.account.unread_activity_count = 0;
                    }
                    MainTabsActivity.this.m9176e();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m9176e() {
        if (AppState.account != null) {
            m9166a(AppState.account.unread_feed_count, -1, 0);
            m9166a(AppState.account.unread_groups_count, -1, 1);
            m9166a(AppState.account.unread_activity_count, -1, 4);
            if (AppState.config.getBoolean("messaging_turned_on", true)) {
                m9166a(AppState.account.unread_message_count, -1, 3);
            }
        }
    }

    /* renamed from: f */
    private boolean m9178f() {
        aky a = aky.m2599a();
        int a2 = a.mo1116a((Context) this);
        if (a2 == 0) {
            return true;
        }
        if (a.mo1128a(a2)) {
            a.mo1117a((Activity) this, a2, 9000).show();
        } else {
            finish();
        }
        return false;
    }

    public void createPost(View view) {
        if (!this.mIsShowingPopup) {
            startActivityForResult(new Intent(this, CreatePostActivity.class), 888);
        }
    }

    public GroupTabFragment getGroupTabFragment() {
        this.mViewPager.setCurrentItem(1);
        return (GroupTabFragment) this.f6423j.mo13887b(1);
    }

    public void hideCurrentScreen() {
        Fragment b = this.f6423j.mo13887b(this.mViewPager.getCurrentItem());
        if (b != null && (b instanceof BaseMainTabFragment)) {
            ((BaseMainTabFragment) b).mo10481c();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        int currentItem = this.mViewPager.getCurrentItem();
        if (currentItem >= 0 && currentItem < this.f6419f.size()) {
            Fragment b = this.f6423j.mo13887b(currentItem);
            if (b != null && (b instanceof BaseMainTabFragment)) {
                b.onActivityResult(i, i2, intent);
            }
        }
        if (i == 888 && i2 == -1 && currentItem != 0) {
            this.mViewPager.setCurrentItem(0, true);
        }
        if (i == 1001 && i2 == -1 && intent != null) {
            Group group = (Group) intent.getSerializableExtra("group");
            if (group == null) {
                Crashlytics.m16437a(new Throwable("Group is NULL after creating a Group"));
                return;
            }
            Intent intent2 = new Intent(this, GroupDetailsActivity.class);
            intent2.putExtra("group_id", group.group_id);
            intent2.putExtra("group_json", DataUtil.toJson(group));
            startActivity(intent2);
        }
        if (i == 1104 && i2 == -1) {
            RxBus.m14573a().mo14349a(new C2640bb());
        }
        if (i == 999 && i2 == -1) {
            this.f6118c.setAlpha(1.0f);
        }
        if (i != 411) {
            return;
        }
        if (((LocationManager) getSystemService("location")).isProviderEnabled("network")) {
            RxBus.m14573a().mo14349a(new C2669z(false));
        } else {
            RxBus.m14573a().mo14349a(new C2669z(true));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (AppState.config == null || AppState.config.experimentConfig == null || !AppState.config.experimentConfig.containsKey("messaging_turned_on") || !AppState.config.getBoolean("messaging_turned_on")) {
            setContentView((int) R.layout.activity_main_tabs_no_messaging);
            this.f6419f = Arrays.asList(new String[]{"feed", "groups", "post", "me", "activity"});
        } else {
            setContentView((int) R.layout.activity_main_tabs_viewpager);
            this.f6419f = Arrays.asList(new String[]{"feed", "groups", "post", "messages", "me"});
        }
        ButterKnife.bind((Activity) this);
        AppState.mainTabsActivity = this;
        this.f6417d = new Handler(getMainLooper());
        this.f6118c = (RelativeLayout) findViewById(R.id.main_tabs_root);
        this.f6423j = new MainTabPagerAdapter(getSupportFragmentManager());
        this.f6423j.mo13861a(this.f6419f);
        this.mViewPager.setSwipeEnabled(false);
        this.mViewPager.setAdapter(this.f6423j);
        this.mViewPager.setOffscreenPageLimit(4);
        this.mBottomTabs.setupWithViewPager(this.mViewPager);
        this.mBottomTabs.setOnTabSelectedListener(this);
        this.mBottomTabs.setSelectedTabIndicatorHeight(0);
        for (int i = 0; i < this.mBottomTabs.getTabCount(); i++) {
            this.mBottomTabs.mo1765a(i).mo1809a(this.f6423j.mo13860a(i));
        }
        if (m9178f()) {
            startService(new Intent(this, RegistrationIntentService.class));
        }
        if (AppState.config != null && AppState.config.getBoolean("android_force_upgrade_version_number")) {
            Builder builder = new Builder(this);
            builder.setTitle((CharSequence) "A new version is available. Pleage update your app!").setCancelable(false).setPositiveButton((CharSequence) "Update", (OnClickListener) new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse("market://details?id=com.becandid.candid"));
                    MainTabsActivity.this.startActivity(intent);
                    MainTabsActivity.this.finish();
                }
            }).setNegativeButton((CharSequence) "Quit app", (OnClickListener) new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    System.exit(0);
                }
            });
            builder.show();
        }
        if (AppState.needAge == 1 && AppState.age == null) {
            if (AppState.fbInfo == null || AppState.fbInfo.fbToken == null || AppState.fbInfo.fbToken.mo11466b() == null) {
                this.f6118c.setAlpha(0.2f);
                startActivityForResult(new Intent(this, VerifyAgeActivity.class), 999);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("fb_token", AppState.fbInfo.fbToken.mo11466b());
                ApiService.m14297a().mo14110a((Map<String, String>) hashMap);
            }
        }
        busSubscribe(RxBus.m14573a().mo14348a(C2648e.class, (bjy<T>) new bjy<C2648e>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2648e eVar) {
                if (eVar.f10636a >= 0 && eVar.f10636a < 5 && eVar.f10636a != 2) {
                    MainTabsActivity.this.mViewPager.setCurrentItem(eVar.f10636a);
                }
            }
        }));
        busSubscribe(RxBus.m14573a().mo14348a(C2649f.class, (bjy<T>) new bjy<C2649f>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2649f fVar) {
                MainTabsActivity.this.mViewPager.setCurrentItem(0);
                MainTabsActivity.this.m9162a().mo10489a(fVar.f10637a);
            }
        }));
        busSubscribe(RxBus.m14573a().mo14348a(C2637az.class, (bjy<T>) new bjy<C2637az>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2637az azVar) {
                if (azVar.f10620c) {
                    MainTabsActivity.this.m9168a(azVar);
                    return;
                }
                MainTabsActivity.this.f6420g = MainTabsActivity.this.m9170b();
                MainTabsActivity.this.f6420g.mo10479a();
                MainTabsActivity.this.f6420g.mo10501a(azVar);
            }
        }));
        busSubscribe(RxBus.m14573a().mo14348a(C2640bb.class, (bjy<T>) new bjy<C2640bb>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2640bb bbVar) {
                MainTabsActivity.this.f6421h = MainTabsActivity.this.m9162a();
                MainTabsActivity.this.f6421h.mo10479a();
                MainTabsActivity.this.f6420g = MainTabsActivity.this.m9170b();
                MainTabsActivity.this.f6420g.mo10479a();
            }
        }));
        busSubscribe(RxBus.m14573a().mo14348a(C2650g.class, (bjy<T>) new bjy<C2650g>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2650g gVar) {
                MainTabsActivity.this.setShowMessagingPopup(false);
            }
        }));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AppState.mainTabsActivity = null;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            unregisterReceiver(this.f6415a);
        } catch (IllegalArgumentException e) {
            Crashlytics.m16437a((Throwable) e);
        }
        Fragment b = this.f6423j.mo13887b(this.mViewPager.getCurrentItem());
        if (b != null && ((b instanceof MeTabFragment) || (b instanceof ActivityTabFragment))) {
            ApiService.m14297a().mo14120b(Boolean.toString(true), (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        } else if (b != null && (b instanceof BaseMainTabFragment)) {
            ((BaseMainTabFragment) b).onPause();
        }
        this.f6417d.removeCallbacks(this.f6424k);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        switch (i) {
            case 1:
                this.f6420g = m9170b();
                if (iArr.length <= 0 || iArr[0] != 0) {
                    this.f6420g.mo10509h();
                    return;
                } else {
                    this.f6420g.mo10508g();
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("com.becandid.candid.GOT_PUSH");
        intentFilter.setPriority(2);
        registerReceiver(this.f6415a, intentFilter);
        Uri data = getIntent().getData();
        if (!(data == null || data.getPathSegments() == null || data.getPathSegments().isEmpty())) {
            int indexOf = this.f6419f.indexOf((String) data.getPathSegments().get(0));
            if (!(indexOf == -1 || indexOf == 2)) {
                this.mViewPager.setCurrentItem(indexOf);
            }
        }
        Fragment b = this.f6423j.mo13887b(this.mViewPager.getCurrentItem());
        if (b != null && (b instanceof BaseMainTabFragment)) {
            ((BaseMainTabFragment) b).onResume();
        }
        m9174d();
        ApiService.m14297a().mo14152h().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(NetworkData networkData) {
                if (networkData != null) {
                    try {
                        AppState.account.unread_message_count = networkData.new_threads + networkData.new_requests;
                        MainTabsActivity.this.m9174d();
                    } catch (Exception e) {
                        Crashlytics.m16437a((Throwable) e);
                    }
                }
            }
        });
        this.f6417d.postDelayed(this.f6424k, 60000);
    }

    public void onTabReselected(C0416d dVar) {
        Fragment b = this.f6423j.mo13887b(dVar.mo1814c());
        if (b != null && (b instanceof BaseMainTabFragment)) {
            if (b instanceof MeTabFragment) {
                ((MeTabFragment) b).mo10506e();
            } else {
                ((BaseMainTabFragment) b).mo10479a();
            }
        }
        dVar.mo1811a().findViewById(R.id.tab_notification_badge).setVisibility(8);
    }

    public void onTabSelected(C0416d dVar) {
        this.mViewPager.setCurrentItem(dVar.mo1814c(), true);
        int c = dVar.mo1814c();
        View a = dVar.mo1811a();
        ImageView imageView = (ImageView) a.findViewById(R.id.tab_button);
        ((TextView) a.findViewById(R.id.tab_text)).setTextColor(getResources().getColor(R.color.gossip));
        imageView.setImageResource(getResources().getIdentifier(((String) this.f6419f.get(c)) + "_tab_orange", "drawable", getPackageName()));
        a.findViewById(R.id.tab_container).setAlpha(1.0f);
        a.findViewById(R.id.tab_notification_badge).setVisibility(8);
        switch (c) {
            case 0:
                AppState.account.unread_feed_count = 0;
                return;
            case 1:
                AppState.account.unread_groups_count = 0;
                return;
            case 3:
                if (AppState.config.getBoolean("messaging_turned_on", true)) {
                    if (!AppState.hasMessagingShown) {
                        this.f6422i = m9172c();
                        this.f6422i.mo10534d();
                    }
                    RxBus.m14573a().mo14349a(new C2667x());
                    return;
                }
                return;
            case 4:
                AppState.account.unread_activity_count = 0;
                if (AppState.config.getBoolean("messaging_turned_on", true)) {
                    this.f6420g = m9170b();
                    if (this.f6420g != null) {
                        this.f6420g.mo10506e();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onTabUnselected(C0416d dVar) {
        int c = dVar.mo1814c();
        View a = dVar.mo1811a();
        ImageView imageView = (ImageView) a.findViewById(R.id.tab_button);
        ((TextView) a.findViewById(R.id.tab_text)).setTextColor(getResources().getColor(17170444));
        imageView.setImageResource(getResources().getIdentifier(((String) this.f6419f.get(c)) + "_tab", "drawable", getPackageName()));
        a.findViewById(R.id.tab_container).setAlpha(0.5f);
        a.findViewById(R.id.tab_notification_badge).setVisibility(8);
        if (c == 4) {
            ApiService.m14297a().mo14120b(Boolean.toString(true), (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        }
    }

    public void setShowMessagingPopup(boolean z) {
        this.mIsShowingPopup = z;
    }

    public void showCurrentScreen() {
        Fragment b = this.f6423j.mo13887b(this.mViewPager.getCurrentItem());
        if (b != null && (b instanceof BaseMainTabFragment)) {
            ((BaseMainTabFragment) b).mo10480b();
        }
    }

    /* access modifiers changed from: protected */
    public void showSnack(boolean z) {
        super.showSnack(z);
        View findViewById = findViewById(R.id.bottom_tabs);
        if (!z && findViewById != null) {
            Snackbar.m3350a(findViewById, "No Internet connection!", 0).mo1721a();
        }
    }

    public void upsellChange(int i) {
        super.upsellChange(i);
        this.f6421h = m9162a();
        this.f6421h.mo10488a(i);
    }
}
