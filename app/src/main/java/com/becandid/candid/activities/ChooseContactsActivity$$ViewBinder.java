package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.ChooseContactsActivity;

public class ChooseContactsActivity$$ViewBinder<T extends ChooseContactsActivity> implements ViewBinder<T> {

    /* compiled from: ChooseContactsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends ChooseContactsActivity> implements Unbinder {

        /* renamed from: a */
        View f6180a;

        /* renamed from: b */
        private T f6181b;

        protected InnerUnbinder(T t) {
            this.f6181b = t;
        }

        public final void unbind() {
            if (this.f6181b == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6181b);
            this.f6181b = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.recyclerView = null;
            t.contactsSearchOuter = null;
            t.search = null;
            this.f6180a.setOnClickListener(null);
            t.sendInvites = null;
        }
    }

    public Unbinder bind(Finder finder, final T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.recyclerView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_recycler, "field 'recyclerView'"), R.id.contacts_recycler, "field 'recyclerView'");
        t.contactsSearchOuter = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_search_outer, "field 'contactsSearchOuter'"), R.id.contacts_search_outer, "field 'contactsSearchOuter'");
        t.search = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.contacts_search, "field 'search'"), R.id.contacts_search, "field 'search'");
        View view = (View) finder.findRequiredView(obj, R.id.send_invites_button, "field 'sendInvites' and method 'sendInvites'");
        t.sendInvites = (Button) finder.castView(view, R.id.send_invites_button, "field 'sendInvites'");
        createUnbinder.f6180a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.sendInvites(view);
            }
        });
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
