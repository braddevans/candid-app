package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.SearchView;
import android.support.p003v7.widget.SearchView.OnQueryTextListener;
import android.support.p003v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.GroupBaseSearchActivity.QueryTextInterface;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.GroupSearchResult;
import java.util.ArrayList;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class CommunitySearchActivity extends BaseActivity {

    /* renamed from: a */
    MenuItem f6186a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public List<GroupSearchResult> f6187b;

    /* renamed from: d */
    private bjz f6188d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f6189e;

    /* renamed from: f */
    private String f6190f;
    @BindView(2131624105)
    TextView mEmptyText;
    public QueryTextInterface mQueryTextInterface;
    @BindView(2131624104)
    View mSearchEmpty;
    @BindView(2131624106)
    View mSearchProgressBar;
    @BindView(2131624103)
    RecyclerView mSearchResultRecyclerView;
    protected SearchView mSearchView;
    @BindView(2131624102)
    View root;

    /* renamed from: a */
    private void m9026a() {
        this.f6187b.clear();
        this.mSearchResultRecyclerView.getAdapter().notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9027a(ays ays) {
        GroupSearchResult groupSearchResult = new GroupSearchResult();
        try {
            groupSearchResult.groupName = ays.mo8410b("group_name").mo8385c();
            groupSearchResult.groupId = ays.mo8410b("group_id").mo8390g();
            groupSearchResult.firstTag = ays.mo8410b("first_tag").mo8385c();
            groupSearchResult.thumbUrl = ays.mo8410b("thumb_url").mo8385c();
            groupSearchResult.numMembers = ays.mo8410b("num_members").mo8390g();
            this.f6187b.add(groupSearchResult);
        } catch (Exception e) {
            Crashlytics.m16437a((Throwable) e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9030a(String str) {
        if (str.length() >= 2) {
            m9032b(str);
            return;
        }
        this.mSearchProgressBar.setVisibility(8);
        this.mSearchResultRecyclerView.setVisibility(0);
    }

    /* renamed from: b */
    private void m9032b(String str) {
        this.mSearchProgressBar.setVisibility(0);
        this.mSearchResultRecyclerView.setVisibility(8);
        m9026a();
        m9033c(str);
    }

    /* renamed from: c */
    private void m9033c(String str) {
        if (this.f6188d != null) {
            this.f6188d.unsubscribe();
        }
        this.f6188d = ApiService.m14297a().mo14138d(this.f6189e, str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                CommunitySearchActivity.this.mSearchProgressBar.setVisibility(8);
                CommunitySearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
                CommunitySearchActivity.this.mSearchEmpty.setVisibility(8);
            }

            public void onNext(ays ays) {
                CommunitySearchActivity.this.mSearchEmpty.setVisibility(8);
                if (!CommunitySearchActivity.this.isStopped) {
                    CommunitySearchActivity.this.mSearchProgressBar.setVisibility(8);
                    ayo n = ays.mo8410b("groups").mo8399n();
                    if (n.mo8381a() <= 0) {
                        CommunitySearchActivity.this.mSearchResultRecyclerView.setVisibility(8);
                        CommunitySearchActivity.this.mSearchEmpty.setVisibility(0);
                        return;
                    }
                    for (int i = 0; i < n.mo8381a(); i++) {
                        CommunitySearchActivity.this.m9027a(n.mo8382a(i).mo8398m());
                    }
                    CommunitySearchActivity.this.mSearchResultRecyclerView.getAdapter().notifyDataSetChanged();
                    CommunitySearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
                    CommunitySearchActivity.this.mSearchEmpty.setVisibility(8);
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent().getBooleanExtra("hide_arrow", false)) {
            setContentView((int) R.layout.activity_community_search_transparent_arrow);
        } else {
            setContentView((int) R.layout.activity_community_search);
        }
        ButterKnife.bind((Activity) this);
        enableKeyboardEvents(this.root);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        Bundle extras = getIntent().getExtras();
        if (!extras.containsKey("queryType")) {
            finish();
            return;
        }
        this.f6189e = extras.getString("queryType");
        this.f6190f = extras.getString("searchViewHint");
        this.f6187b = new ArrayList();
        this.mSearchResultRecyclerView.setHasFixedSize(true);
        this.mSearchResultRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.mSearchResultRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this) {
            public void onItemClick(View view, int i) {
                GroupSearchResult groupSearchResult = (GroupSearchResult) CommunitySearchActivity.this.f6187b.get(i);
                if (groupSearchResult != null && groupSearchResult.groupId != 0) {
                    Group group = new Group();
                    group.group_id = groupSearchResult.groupId;
                    if (AppState.groups == null) {
                        AppState.groups = new ArrayList();
                    }
                    if (!AppState.groups.contains(group)) {
                        group.group_id = groupSearchResult.groupId;
                        group.group_name = groupSearchResult.groupName;
                        group.community_membership = 1;
                        group.group_type = CommunitySearchActivity.this.f6189e;
                        group.num_members = groupSearchResult.numMembers;
                        group.thumb_url = groupSearchResult.thumbUrl;
                        AppState.groups.add(group);
                        Intent intent = new Intent();
                        intent.putExtra("queryString", groupSearchResult.groupName);
                        intent.putExtra("queryId", Integer.toString(groupSearchResult.groupId));
                        intent.putExtra("num_members", Integer.toString(groupSearchResult.numMembers));
                        intent.putExtra("thumb_url", groupSearchResult.thumbUrl);
                        CommunitySearchActivity.this.setResult(-1, intent);
                        RxBus.m14573a().mo14349a(new C2652i(Integer.toString(groupSearchResult.groupId)));
                    }
                    CommunitySearchActivity.this.finish();
                }
            }
        });
        this.mSearchResultRecyclerView.setAdapter(new CommunitySearchAdapter(this.f6187b, this));
        this.mQueryTextInterface = new QueryTextInterface() {
            public void onQueryTextChange(String str) {
                CommunitySearchActivity.this.m9030a(str);
            }

            public void onQueryTextSubmit(String str) {
                CommunitySearchActivity.this.m9030a(str);
            }
        };
        if (this.f6189e != null) {
            String str = null;
            String str2 = this.f6189e;
            char c = 65535;
            switch (str2.hashCode()) {
                case -907977868:
                    if (str2.equals("school")) {
                        c = 0;
                        break;
                    }
                    break;
                case 949445015:
                    if (str2.equals("college")) {
                        c = 1;
                        break;
                    }
                    break;
                case 950484093:
                    if (str2.equals("company")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    str = "Enter the name of the school you attend.\n\nYou'll always be anonymous.";
                    break;
                case 1:
                    str = "Enter the name of the college you attend.\n\nYou'll always be anonymous.";
                    break;
                case 2:
                    str = "Enter the name of the company you attend.\n\nYou'll always be anonymous.";
                    break;
            }
            if (str != null) {
                this.mSearchEmpty.setVisibility(0);
                this.mEmptyText.setText(str);
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_search_menu, menu);
        this.f6186a = menu.findItem(R.id.action_search);
        this.mSearchView = (SearchView) MenuItemCompat.m12790a(this.f6186a);
        this.f6186a.expandActionView();
        this.mSearchView.requestFocus();
        this.mSearchView.setQueryHint("Search ...");
        this.mSearchView.setOnQueryTextListener(new OnQueryTextListener() {
            public boolean onQueryTextChange(String str) {
                if (CommunitySearchActivity.this.mQueryTextInterface != null) {
                    CommunitySearchActivity.this.mQueryTextInterface.onQueryTextChange(str);
                }
                return false;
            }

            public boolean onQueryTextSubmit(String str) {
                if (CommunitySearchActivity.this.mQueryTextInterface != null) {
                    CommunitySearchActivity.this.mQueryTextInterface.onQueryTextSubmit(str);
                }
                CommunitySearchActivity.this.mSearchView.clearFocus();
                return true;
            }
        });
        MenuItemCompat.m12789a(this.f6186a, (C2327e) new C2327e() {
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                CommunitySearchActivity.this.closeKeyboard();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        CommunitySearchActivity.this.finish();
                    }
                }, 500);
                return false;
            }

            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return false;
            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras.containsKey("queryString")) {
            this.mSearchView.setQuery(extras.getString("queryString"), true);
        }
        this.mSearchView.setQueryHint(this.f6190f);
        return super.onCreateOptionsMenu(menu);
    }
}
