package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.models.NetworkData;
import p012rx.schedulers.Schedulers;

public class MessageSearchActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public MessageThreadsAdapter f6517a;
    @BindView(2131624308)
    RecyclerView resultRecycler;
    @BindView(2131624306)
    EditText searchEdit;
    @BindView(2131624307)
    LinearLayout searchProgressBar;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9207a() {
        this.f6517a.mo13769c();
        this.resultRecycler.getAdapter().notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9209a(final String str) {
        this.searchProgressBar.setVisibility(0);
        this.resultRecycler.setVisibility(8);
        m9207a();
        ApiService.m14297a().mo14166m(str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Log.d("GroupSearch", th.toString());
                MessageSearchActivity.this.searchProgressBar.setVisibility(8);
                MessageSearchActivity.this.resultRecycler.setVisibility(0);
            }

            public void onNext(NetworkData networkData) {
                if (!MessageSearchActivity.this.isStopped) {
                    if (!networkData.success) {
                        MessageSearchActivity.this.m9207a();
                        MessageSearchActivity.this.searchProgressBar.setVisibility(8);
                        MessageSearchActivity.this.resultRecycler.setVisibility(0);
                    } else if (!MessageSearchActivity.this.searchEdit.getText().toString().equals(str)) {
                        MessageSearchActivity.this.m9207a();
                        MessageSearchActivity.this.searchProgressBar.setVisibility(8);
                        MessageSearchActivity.this.resultRecycler.setVisibility(0);
                    } else {
                        if (networkData.results != null) {
                            MessageSearchActivity.this.f6517a.mo13764a(networkData.results);
                            MessageSearchActivity.this.f6517a.notifyDataSetChanged();
                        } else {
                            MessageSearchActivity.this.m9207a();
                        }
                        MessageSearchActivity.this.searchProgressBar.setVisibility(8);
                        MessageSearchActivity.this.resultRecycler.setVisibility(0);
                    }
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_message_search);
        ButterKnife.bind((Activity) this);
        this.f6517a = new MessageThreadsAdapter(this);
        this.resultRecycler.setAdapter(this.f6517a);
        this.resultRecycler.setLayoutManager(new LinearLayoutManager(this));
        this.resultRecycler.setHasFixedSize(true);
        this.resultRecycler.addOnItemTouchListener(new RecyclerItemClickListener(this) {
            public void onItemClick(View view, int i) {
                MessageSearchActivity.this.f6517a.mo13870b(i);
            }
        });
        this.searchEdit.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 3) {
                    MessageSearchActivity.this.m9209a(editable.toString());
                    return;
                }
                MessageSearchActivity.this.m9207a();
                MessageSearchActivity.this.searchProgressBar.setVisibility(8);
                MessageSearchActivity.this.resultRecycler.setVisibility(0);
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        this.searchEdit.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 3 && (keyEvent == null || keyEvent.getAction() != 1 || keyEvent.getKeyCode() != 66)) {
                    return false;
                }
                MessageSearchActivity.this.m9209a(MessageSearchActivity.this.searchEdit.getText().toString());
                return true;
            }
        });
    }
}
