package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.CommunitySearchActivity;

public class CommunitySearchActivity$$ViewBinder<T extends CommunitySearchActivity> implements ViewBinder<T> {

    /* compiled from: CommunitySearchActivity$$ViewBinder */
    public static class InnerUnbinder<T extends CommunitySearchActivity> implements Unbinder {

        /* renamed from: a */
        private T f6197a;

        protected InnerUnbinder(T t) {
            this.f6197a = t;
        }

        public final void unbind() {
            if (this.f6197a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6197a);
            this.f6197a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.root = null;
            t.mSearchResultRecyclerView = null;
            t.mSearchEmpty = null;
            t.mSearchProgressBar = null;
            t.mEmptyText = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.root = (View) finder.findRequiredView(obj, R.id.activity_group_base_search, "field 'root'");
        t.mSearchResultRecyclerView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.search_results, "field 'mSearchResultRecyclerView'"), R.id.search_results, "field 'mSearchResultRecyclerView'");
        t.mSearchEmpty = (View) finder.findRequiredView(obj, R.id.search_empty, "field 'mSearchEmpty'");
        t.mSearchProgressBar = (View) finder.findRequiredView(obj, R.id.search_progress_bar, "field 'mSearchProgressBar'");
        t.mEmptyText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.text_empty, "field 'mEmptyText'"), R.id.text_empty, "field 'mEmptyText'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
