package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.MessageActivity;
import com.becandid.candid.views.StickerKeyboard;
import com.venmo.view.TooltipView;

public class MessageActivity$$ViewBinder<T extends MessageActivity> implements ViewBinder<T> {

    /* compiled from: MessageActivity$$ViewBinder */
    public static class InnerUnbinder<T extends MessageActivity> implements Unbinder {

        /* renamed from: a */
        View f6515a;

        /* renamed from: b */
        private T f6516b;

        protected InnerUnbinder(T t) {
            this.f6516b = t;
        }

        public final void unbind() {
            if (this.f6516b == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6516b);
            this.f6516b = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.root = null;
            t.contentRootView = null;
            t.messageSpinnyContainer = null;
            t.blockedFrameLayout = null;
            t.mVideoPlayerFullscreen = null;
            t.headerName = null;
            t.headerOnline = null;
            t.messageHeaderUserIcon = null;
            t.messageHeaderUserIconContainer = null;
            t.messageHeaderPostText = null;
            t.messageHeaderRightArrow = null;
            t.messageHeaderPostContainer = null;
            t.mNickname = null;
            t.messageRecycler = null;
            t.messageTextPlaceholder = null;
            t.messageInputContainer = null;
            t.messageInputButtonsVisible = null;
            t.messageInputButtonsHidden = null;
            this.f6515a.setOnClickListener(null);
            t.postButton = null;
            t.messageInput = null;
            t.messagePhoto = null;
            t.messagePhotoButton = null;
            t.messageVideoButton = null;
            t.messagePhotoClear = null;
            t.tooltipView = null;
            t.requestButtons = null;
            t.requestIgnore = null;
            t.requestAccept = null;
            t.stickerKeyboard = null;
            t.stickerButton = null;
            t.messageGifButton = null;
            t.messagePostBtnSpinny = null;
        }
    }

    public Unbinder bind(Finder finder, final T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.root = (View) finder.findRequiredView(obj, R.id.root, "field 'root'");
        t.contentRootView = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.content_root, "field 'contentRootView'"), R.id.content_root, "field 'contentRootView'");
        t.messageSpinnyContainer = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_spinny_container, "field 'messageSpinnyContainer'"), R.id.message_spinny_container, "field 'messageSpinnyContainer'");
        t.blockedFrameLayout = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_blocked_frame_layout, "field 'blockedFrameLayout'"), R.id.message_blocked_frame_layout, "field 'blockedFrameLayout'");
        t.mVideoPlayerFullscreen = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.video_player_fullscreen, "field 'mVideoPlayerFullscreen'"), R.id.video_player_fullscreen, "field 'mVideoPlayerFullscreen'");
        t.headerName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.message_header_name, "field 'headerName'"), R.id.message_header_name, "field 'headerName'");
        t.headerOnline = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_header_online, "field 'headerOnline'"), R.id.message_header_online, "field 'headerOnline'");
        t.messageHeaderUserIcon = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_header_user_icon, "field 'messageHeaderUserIcon'"), R.id.messages_header_user_icon, "field 'messageHeaderUserIcon'");
        t.messageHeaderUserIconContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.messages_header_user_icon_container, "field 'messageHeaderUserIconContainer'"), R.id.messages_header_user_icon_container, "field 'messageHeaderUserIconContainer'");
        t.messageHeaderPostText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_header_post_text, "field 'messageHeaderPostText'"), R.id.messages_header_post_text, "field 'messageHeaderPostText'");
        t.messageHeaderRightArrow = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.messages_header_right_arrow, "field 'messageHeaderRightArrow'"), R.id.messages_header_right_arrow, "field 'messageHeaderRightArrow'");
        t.messageHeaderPostContainer = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_header_post_container, "field 'messageHeaderPostContainer'"), R.id.message_header_post_container, "field 'messageHeaderPostContainer'");
        t.mNickname = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.nickname, "field 'mNickname'"), R.id.nickname, "field 'mNickname'");
        t.messageRecycler = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.message_messages, "field 'messageRecycler'"), R.id.message_messages, "field 'messageRecycler'");
        t.messageTextPlaceholder = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.message_text_placeholder, "field 'messageTextPlaceholder'"), R.id.message_text_placeholder, "field 'messageTextPlaceholder'");
        t.messageInputContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.message_input_container, "field 'messageInputContainer'"), R.id.message_input_container, "field 'messageInputContainer'");
        t.messageInputButtonsVisible = (View) finder.findRequiredView(obj, R.id.message_input_buttons_visible, "field 'messageInputButtonsVisible'");
        t.messageInputButtonsHidden = (View) finder.findRequiredView(obj, R.id.message_input_buttons_hidden, "field 'messageInputButtonsHidden'");
        View view = (View) finder.findRequiredView(obj, R.id.post_button, "field 'postButton' and method 'saveMessage'");
        t.postButton = (ImageView) finder.castView(view, R.id.post_button, "field 'postButton'");
        createUnbinder.f6515a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.saveMessage(view);
            }
        });
        t.messageInput = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.message_text_input, "field 'messageInput'"), R.id.message_text_input, "field 'messageInput'");
        t.messagePhoto = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_photo, "field 'messagePhoto'"), R.id.message_photo, "field 'messagePhoto'");
        t.messagePhotoButton = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_photo_button, "field 'messagePhotoButton'"), R.id.message_photo_button, "field 'messagePhotoButton'");
        t.messageVideoButton = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_video_button, "field 'messageVideoButton'"), R.id.message_video_button, "field 'messageVideoButton'");
        t.messagePhotoClear = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_photo_clear, "field 'messagePhotoClear'"), R.id.message_photo_clear, "field 'messagePhotoClear'");
        t.tooltipView = (TooltipView) finder.castView((View) finder.findRequiredView(obj, R.id.tooltip, "field 'tooltipView'"), R.id.tooltip, "field 'tooltipView'");
        t.requestButtons = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.request_buttons, "field 'requestButtons'"), R.id.request_buttons, "field 'requestButtons'");
        t.requestIgnore = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.request_ignore, "field 'requestIgnore'"), R.id.request_ignore, "field 'requestIgnore'");
        t.requestAccept = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.request_accept, "field 'requestAccept'"), R.id.request_accept, "field 'requestAccept'");
        t.stickerKeyboard = (StickerKeyboard) finder.castView((View) finder.findRequiredView(obj, R.id.sticker_keyboard, "field 'stickerKeyboard'"), R.id.sticker_keyboard, "field 'stickerKeyboard'");
        t.stickerButton = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.message_sticker_button, "field 'stickerButton'"), R.id.message_sticker_button, "field 'stickerButton'");
        t.messageGifButton = (View) finder.findRequiredView(obj, R.id.message_gif_button, "field 'messageGifButton'");
        t.messagePostBtnSpinny = (ProgressBar) finder.castView((View) finder.findRequiredView(obj, R.id.post_btn_spinny, "field 'messagePostBtnSpinny'"), R.id.post_btn_spinny, "field 'messagePostBtnSpinny'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
