package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.MediaController;
import android.widget.VideoView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import java.io.File;

public class FullScreenVideoActivity extends BaseFullScreenActivity implements CacheListener {
    @BindView(2131624187)
    View mFullScreenExit;
    @BindView(2131624345)
    VideoView mVideoView;

    public void onBackPressed() {
        setResult(299, new Intent());
        super.onBackPressed();
    }

    public void onCacheAvailable(File file, String str, int i) {
        Log.d("CACHING", String.format("onCacheAvailable. percents: %d, file: %s, url: %s", new Object[]{Integer.valueOf(i), file, str}));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_post_video_fullscreen);
        ButterKnife.bind((Activity) this);
        Bundle extras = getIntent().getExtras();
        String string = extras.getString("video_url");
        final int i = extras.getInt("curr_pos", 0);
        int intExtra = getIntent().getIntExtra("uploaded", 1);
        if (string == null) {
            finish();
            return;
        }
        HttpProxyCacheServer a = GossipApplication.m9013a(this);
        a.mo16743a((CacheListener) this, string);
        this.mVideoView.setVideoPath(a.mo16740a(string));
        if (extras.containsKey("post_id")) {
            setupPost(extras);
            subscribeToUpdates("post");
            setupFooterShared(extras);
        }
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(this.mVideoView);
        this.mVideoView.setMediaController(mediaController);
        this.mVideoView.requestFocus();
        this.mVideoView.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                FullScreenVideoActivity.this.mVideoView.start();
                FullScreenVideoActivity.this.mVideoView.seekTo(i);
                mediaPlayer.setLooping(true);
            }
        });
        this.mFullScreenExit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FullScreenVideoActivity.this.onBackPressed();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        GossipApplication.m9013a(this).mo16742a((CacheListener) this);
    }
}
