package com.becandid.candid.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.AddLinkActivity;

public class AddLinkActivity$$ViewBinder<T extends AddLinkActivity> implements ViewBinder<T> {

    /* compiled from: AddLinkActivity$$ViewBinder */
    public static class InnerUnbinder<T extends AddLinkActivity> implements Unbinder {

        /* renamed from: a */
        View f6113a;

        /* renamed from: b */
        View f6114b;

        /* renamed from: c */
        private T f6115c;

        protected InnerUnbinder(T t) {
            this.f6115c = t;
        }

        public final void unbind() {
            if (this.f6115c == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6115c);
            this.f6115c = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.copiedLink = null;
            t.typedLink = null;
            t.spinny = null;
            this.f6113a.setOnClickListener(null);
            this.f6114b.setOnClickListener(null);
        }
    }

    public Unbinder bind(Finder finder, final T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.copiedLink = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.copied_link_text, "field 'copiedLink'"), R.id.copied_link_text, "field 'copiedLink'");
        t.typedLink = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.typed_link, "field 'typedLink'"), R.id.typed_link, "field 'typedLink'");
        t.spinny = (View) finder.findRequiredView(obj, R.id.add_link_spinny, "field 'spinny'");
        View view = (View) finder.findRequiredView(obj, R.id.typed_link_button, "method 'chooseTypedLink'");
        createUnbinder.f6113a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.chooseTypedLink(view);
            }
        });
        View view2 = (View) finder.findRequiredView(obj, R.id.copied_link_button, "method 'chooseCopiedLink'");
        createUnbinder.f6114b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.chooseCopiedLink(view);
            }
        });
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
