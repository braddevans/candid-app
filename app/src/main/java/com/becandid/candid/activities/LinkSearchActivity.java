package com.becandid.candid.activities;

import android.os.Bundle;
import android.support.p003v7.widget.LinearLayoutManager;
import com.becandid.candid.activities.GroupBaseSearchActivity.QueryTextInterface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class LinkSearchActivity extends GroupBaseSearchActivity {

    /* renamed from: b */
    private List<LinkSearchResult> f6409b;

    /* renamed from: d */
    private bjz f6410d;

    public class LinkSearchResult {
        public String description;
        public String final_link_url;
        public String host;
        public String source_url;
        public String title;

        LinkSearchResult() {
        }
    }

    /* renamed from: a */
    private void m9156a() {
        this.f6409b.clear();
        this.mSearchResultRecyclerView.getAdapter().notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9157a(ays ays) {
        LinkSearchResult linkSearchResult = new LinkSearchResult();
        try {
            linkSearchResult.host = ays.mo8410b("attribution_host").mo8385c();
            linkSearchResult.description = ays.mo8410b("description").mo8385c();
            linkSearchResult.title = ays.mo8410b("title").mo8385c();
            linkSearchResult.source_url = ays.mo8410b("source_url").mo8385c();
            linkSearchResult.final_link_url = ays.mo8410b("final_link_url").mo8385c();
            this.f6409b.add(linkSearchResult);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9160a(String str) {
        if (str.length() >= 3) {
            m9161b(str.toString());
            return;
        }
        this.mSearchProgressBar.setVisibility(8);
        this.mSearchResultRecyclerView.setVisibility(0);
    }

    /* renamed from: b */
    private void m9161b(String str) {
        this.mSearchProgressBar.setVisibility(0);
        this.mSearchResultRecyclerView.setVisibility(8);
        m9156a();
        HashMap hashMap = new HashMap();
        hashMap.put("query", str);
        if (this.f6410d != null) {
            this.f6410d.unsubscribe();
        }
        this.f6410d = ApiService.m14297a().mo14163k((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                LinkSearchActivity.this.mSearchProgressBar.setVisibility(8);
                LinkSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
            }

            public void onNext(ays ays) {
                if (!LinkSearchActivity.this.isStopped) {
                    ayo n = ays.mo8410b("links").mo8399n();
                    for (int i = 0; i < n.mo8381a(); i++) {
                        LinkSearchActivity.this.m9157a(n.mo8382a(i).mo8398m());
                    }
                    LinkSearchActivity.this.mSearchResultRecyclerView.getAdapter().notifyDataSetChanged();
                    LinkSearchActivity.this.mSearchProgressBar.setVisibility(8);
                    LinkSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f6409b = new ArrayList();
        this.mSearchResultRecyclerView.setHasFixedSize(true);
        this.mSearchResultRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.mSearchResultRecyclerView.setAdapter(new LinkSearchAdapter(this.f6409b, this));
        this.mQueryTextInterface = new QueryTextInterface() {
            public void onQueryTextChange(String str) {
                LinkSearchActivity.this.m9160a(str);
            }

            public void onQueryTextSubmit(String str) {
                LinkSearchActivity.this.m9160a(str);
            }
        };
        busSubscribe(C2615ad.class, new bjy<C2615ad>() {
            public void onCompleted() {
                LinkSearchActivity.this.finish();
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2615ad adVar) {
                LinkSearchActivity.this.finish();
            }
        });
    }
}
