package com.becandid.candid.activities;

import android.support.p001v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.GroupDetailsActivity;
import com.becandid.candid.views.AutoplayFeedRecyclerView;

public class GroupDetailsActivity$$ViewBinder<T extends GroupDetailsActivity> implements ViewBinder<T> {

    /* compiled from: GroupDetailsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends GroupDetailsActivity> implements Unbinder {

        /* renamed from: a */
        private T f6368a;

        protected InnerUnbinder(T t) {
            this.f6368a = t;
        }

        public final void unbind() {
            if (this.f6368a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6368a);
            this.f6368a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.groupPosts = null;
            t.loadingSpinny = null;
            t.gdHeaderName = null;
            t.gdJoin = null;
            t.gdCompose = null;
            t.groupEmpty = null;
            t.mSwipeContainer = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.groupPosts = (AutoplayFeedRecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.group_posts, "field 'groupPosts'"), R.id.group_posts, "field 'groupPosts'");
        t.loadingSpinny = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.loading_spinny, "field 'loadingSpinny'"), R.id.loading_spinny, "field 'loadingSpinny'");
        t.gdHeaderName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_details_header_name, "field 'gdHeaderName'"), R.id.group_details_header_name, "field 'gdHeaderName'");
        t.gdJoin = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_details_join, "field 'gdJoin'"), R.id.group_details_join, "field 'gdJoin'");
        t.gdCompose = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_details_compose, "field 'gdCompose'"), R.id.group_details_compose, "field 'gdCompose'");
        t.groupEmpty = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.group_posts_empty, "field 'groupEmpty'"), R.id.group_posts_empty, "field 'groupEmpty'");
        t.mSwipeContainer = (SwipeRefreshLayout) finder.castView((View) finder.findRequiredView(obj, R.id.swipeContainer, "field 'mSwipeContainer'"), R.id.swipeContainer, "field 'mSwipeContainer'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
