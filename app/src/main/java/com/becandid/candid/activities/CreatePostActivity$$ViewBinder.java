package com.becandid.candid.activities;

import android.support.p003v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.CreatePostActivity;
import com.becandid.candid.views.AlwaysOnAutoCompleteView;
import com.venmo.view.TooltipView;

public class CreatePostActivity$$ViewBinder<T extends CreatePostActivity> implements ViewBinder<T> {

    /* compiled from: CreatePostActivity$$ViewBinder */
    public static class InnerUnbinder<T extends CreatePostActivity> implements Unbinder {

        /* renamed from: a */
        View f6316a;

        /* renamed from: b */
        View f6317b;

        /* renamed from: c */
        View f6318c;

        /* renamed from: d */
        View f6319d;

        /* renamed from: e */
        View f6320e;

        /* renamed from: f */
        View f6321f;

        /* renamed from: g */
        private T f6322g;

        protected InnerUnbinder(T t) {
            this.f6322g = t;
        }

        public final void unbind() {
            if (this.f6322g == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6322g);
            this.f6322g = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.postContent = null;
            t.postCaption = null;
            t.postPhoto = null;
            this.f6316a.setOnClickListener(null);
            t.postButton = null;
            t.createGroupButton = null;
            t.groupNameSelector = null;
            t.friendMarker = null;
            t.friendSwitch = null;
            t.tooltipView = null;
            t.camBtn = null;
            this.f6317b.setOnClickListener(null);
            t.linkBtn = null;
            t.webSearchBtn = null;
            t.postSpinny = null;
            t.mRootView = null;
            t.linkContainer = null;
            t.nonYoutubeLinkContainer = null;
            t.youtubeLinkContainer = null;
            this.f6318c.setOnClickListener(null);
            t.gifSearchBtn = null;
            t.postPhotoOuter = null;
            t.imageProgressContainer = null;
            t.imageProgress = null;
            t.videoPlayBtn = null;
            t.previewVideoFrame = null;
            t.previewVideoPlay = null;
            t.previewVideoContainer = null;
            this.f6319d.setOnClickListener(null);
            this.f6320e.setOnClickListener(null);
            this.f6321f.setOnClickListener(null);
        }
    }

    public Unbinder bind(final Finder finder, final T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.postContent = (ScrollView) finder.castView((View) finder.findRequiredView(obj, R.id.post_content_container, "field 'postContent'"), R.id.post_content_container, "field 'postContent'");
        t.postCaption = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.post_caption, "field 'postCaption'"), R.id.post_caption, "field 'postCaption'");
        t.postPhoto = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_photo, "field 'postPhoto'"), R.id.post_photo, "field 'postPhoto'");
        View view = (View) finder.findRequiredView(obj, R.id.post_button, "field 'postButton' and method 'savePost'");
        t.postButton = (Button) finder.castView(view, R.id.post_button, "field 'postButton'");
        createUnbinder.f6316a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.savePost(view);
            }
        });
        t.createGroupButton = (View) finder.findRequiredView(obj, R.id.post_create_group, "field 'createGroupButton'");
        t.groupNameSelector = (AlwaysOnAutoCompleteView) finder.castView((View) finder.findRequiredView(obj, R.id.group_name, "field 'groupNameSelector'"), R.id.group_name, "field 'groupNameSelector'");
        t.friendMarker = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.friend_marker, "field 'friendMarker'"), R.id.friend_marker, "field 'friendMarker'");
        t.friendSwitch = (SwitchCompat) finder.castView((View) finder.findRequiredView(obj, R.id.friend_switch, "field 'friendSwitch'"), R.id.friend_switch, "field 'friendSwitch'");
        t.tooltipView = (TooltipView) finder.castView((View) finder.findRequiredView(obj, R.id.tooltip, "field 'tooltipView'"), R.id.tooltip, "field 'tooltipView'");
        t.camBtn = (View) finder.findRequiredView(obj, R.id.cam_button, "field 'camBtn'");
        View view2 = (View) finder.findRequiredView(obj, R.id.link_button, "field 'linkBtn' and method 'chooseLink'");
        t.linkBtn = view2;
        createUnbinder.f6317b = view2;
        view2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.chooseLink((ImageView) finder.castParam(view, "doClick", 0, "chooseLink", 0));
            }
        });
        t.webSearchBtn = (View) finder.findRequiredView(obj, R.id.web_search_button, "field 'webSearchBtn'");
        t.postSpinny = (View) finder.findRequiredView(obj, R.id.post_spinny, "field 'postSpinny'");
        t.mRootView = (View) finder.findRequiredView(obj, R.id.create_post_root, "field 'mRootView'");
        t.linkContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_link_outer, "field 'linkContainer'"), R.id.post_link_outer, "field 'linkContainer'");
        t.nonYoutubeLinkContainer = (View) finder.findRequiredView(obj, R.id.non_youtube_placeholder, "field 'nonYoutubeLinkContainer'");
        t.youtubeLinkContainer = (View) finder.findRequiredView(obj, R.id.youtube_placeholder, "field 'youtubeLinkContainer'");
        View view3 = (View) finder.findRequiredView(obj, R.id.gif_search_button, "field 'gifSearchBtn' and method 'chooseGif'");
        t.gifSearchBtn = (ImageView) finder.castView(view3, R.id.gif_search_button, "field 'gifSearchBtn'");
        createUnbinder.f6318c = view3;
        view3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.chooseGif();
            }
        });
        t.postPhotoOuter = (View) finder.findRequiredView(obj, R.id.post_photo_outer, "field 'postPhotoOuter'");
        t.imageProgressContainer = (View) finder.findRequiredView(obj, R.id.image_progress_container, "field 'imageProgressContainer'");
        t.imageProgress = (View) finder.findRequiredView(obj, R.id.image_progress, "field 'imageProgress'");
        t.videoPlayBtn = (View) finder.findRequiredView(obj, R.id.video_play_button, "field 'videoPlayBtn'");
        t.previewVideoFrame = (VideoView) finder.castView((View) finder.findRequiredView(obj, R.id.preview_video_frame, "field 'previewVideoFrame'"), R.id.preview_video_frame, "field 'previewVideoFrame'");
        t.previewVideoPlay = (View) finder.findRequiredView(obj, R.id.preview_video_play, "field 'previewVideoPlay'");
        t.previewVideoContainer = (View) finder.findRequiredView(obj, R.id.preview_video_container, "field 'previewVideoContainer'");
        View view4 = (View) finder.findRequiredView(obj, R.id.chosen_group_bubble, "method 'clearChosenGroup'");
        createUnbinder.f6319d = view4;
        view4.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.clearChosenGroup(view);
            }
        });
        View view5 = (View) finder.findRequiredView(obj, R.id.video_button, "method 'takeVideo'");
        createUnbinder.f6320e = view5;
        view5.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.takeVideo();
            }
        });
        View view6 = (View) finder.findRequiredView(obj, R.id.link_preview_clear, "method 'clearLink'");
        createUnbinder.f6321f = view6;
        view6.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.clearLink((ImageView) finder.castParam(view, "doClick", 0, "clearLink", 0));
            }
        });
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
