package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.adapters.CommentStatsAdapter;
import com.becandid.candid.data.User;
import com.becandid.candid.models.CommentData;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class CommentStatsActivity extends BaseActivity {
    @BindView(2131624100)
    TextView mOthersDisliked;
    @BindView(2131624099)
    TextView mOthersLiked;
    @BindView(2131624101)
    FrameLayout mProgressBar;
    @BindView(2131624098)
    RecyclerView mRecyclerView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_comment_stats);
        ButterKnife.bind((Activity) this);
        this.mProgressBar.setVisibility(0);
        Bundle extras = getIntent().getExtras();
        final int i = extras.getInt("post_id");
        final int i2 = extras.getInt("comment_id");
        if (i2 > 0) {
            ApiService.m14297a().mo14095a(i2).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<CommentData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    CommentStatsActivity.this.mProgressBar.setVisibility(8);
                }

                public void onNext(CommentData commentData) {
                    CommentStatsActivity.this.mProgressBar.setVisibility(8);
                    if (commentData != null) {
                        List<User> list = commentData.users;
                        if (list == null || list.size() <= 0) {
                            CommentStatsActivity.this.mRecyclerView.setVisibility(8);
                        } else {
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CommentStatsActivity.this);
                            CommentStatsAdapter commentStatsAdapter = new CommentStatsAdapter(list, i, i2);
                            CommentStatsActivity.this.mRecyclerView.setLayoutManager(linearLayoutManager);
                            CommentStatsActivity.this.mRecyclerView.setAdapter(commentStatsAdapter);
                            CommentStatsActivity.this.mRecyclerView.setHasFixedSize(true);
                        }
                        if (commentData.other_likes != 0) {
                            CommentStatsActivity.this.mOthersLiked.setText(C2699jj.m14624b(commentData.other_likes == 1 ? Integer.toString(commentData.other_likes) + " other user liked this comment" : Integer.toString(commentData.other_likes) + " other users liked this comment"));
                        } else {
                            CommentStatsActivity.this.mOthersLiked.setVisibility(8);
                        }
                        if (commentData.other_dislikes != 0) {
                            CommentStatsActivity.this.mOthersDisliked.setText(C2699jj.m14624b(commentData.other_dislikes == 1 ? Integer.toString(commentData.other_dislikes) + " other user disliked this comment" : Integer.toString(commentData.other_dislikes) + " other users disliked this comment"));
                        } else {
                            CommentStatsActivity.this.mOthersDisliked.setVisibility(8);
                        }
                    }
                }
            });
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
