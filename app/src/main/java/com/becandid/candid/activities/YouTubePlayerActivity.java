package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayerView;

public class YouTubePlayerActivity extends YouTubeBaseActivity {
    @BindView(2131624397)
    YouTubePlayerView youTubePlayerView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_you_tube_player);
        ButterKnife.bind((Activity) this);
        final String stringExtra = getIntent().getStringExtra("youtube_video_id");
        this.youTubePlayerView.mo12790a(getString(R.string.youtube_api_key), (C0945a) new C0945a() {
            public void onInitializationFailure(C0946b bVar, YouTubeInitializationResult youTubeInitializationResult) {
            }

            public void onInitializationSuccess(C0946b bVar, awz awz, boolean z) {
                awz.mo8142b(stringExtra);
            }
        });
    }
}
