package com.becandid.candid.activities;

import android.view.View;
import android.widget.VideoView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.activities.FullScreenVideoActivity;

public class FullScreenVideoActivity$$ViewBinder<T extends FullScreenVideoActivity> extends BaseFullScreenActivity$$ViewBinder<T> {

    /* compiled from: FullScreenVideoActivity$$ViewBinder */
    public static class InnerUnbinder<T extends FullScreenVideoActivity> extends com.becandid.candid.activities.BaseFullScreenActivity$$ViewBinder.InnerUnbinder<T> {
        protected InnerUnbinder(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            super.unbind(t);
            t.mVideoView = null;
            t.mFullScreenExit = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder innerUnbinder = (InnerUnbinder) super.bind(finder, t, obj);
        t.mVideoView = (VideoView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_video_view, "field 'mVideoView'"), R.id.fullscreen_video_view, "field 'mVideoView'");
        t.mFullScreenExit = (View) finder.findRequiredView(obj, R.id.fullscreen_exit, "field 'mFullScreenExit'");
        return innerUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
