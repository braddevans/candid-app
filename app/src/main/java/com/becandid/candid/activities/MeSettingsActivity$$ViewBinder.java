package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.MeSettingsActivity;

public class MeSettingsActivity$$ViewBinder<T extends MeSettingsActivity> implements ViewBinder<T> {

    /* compiled from: MeSettingsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends MeSettingsActivity> implements Unbinder {

        /* renamed from: a */
        private T f6445a;

        protected InnerUnbinder(T t) {
            this.f6445a = t;
        }

        public final void unbind() {
            if (this.f6445a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6445a);
            this.f6445a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.recyclerView = null;
            t.versionNumber = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.recyclerView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.me_settings_list, "field 'recyclerView'"), R.id.me_settings_list, "field 'recyclerView'");
        t.versionNumber = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.version_number, "field 'versionNumber'"), R.id.version_number, "field 'versionNumber'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
