package com.becandid.candid.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.p003v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData.NotificationSetting;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class NotificationSettingsActivity extends BaseActivity {

    /* renamed from: a */
    private ArrayList<NotificationSetting> f6535a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f6536b;

    class NotificationSettingsAdapter extends ArrayAdapter<NotificationSetting> {
        public NotificationSettingsAdapter(Context context, List<NotificationSetting> list) {
            super(context, -1, list);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(R.layout.notification_setting, viewGroup, false);
            }
            NotificationSetting notificationSetting = (NotificationSetting) getItem(i);
            ((ImageView) view.findViewById(R.id.setting_checkmark)).setImageResource(notificationSetting.disabled.intValue() == 0 ? R.drawable.green_circle_check : R.drawable.empty_checkmark);
            ((TextView) view.findViewById(R.id.setting_display_name)).setText(notificationSetting.display_name);
            return view;
        }
    }

    public void cancelClick(View view) {
        finish();
    }

    public void onBackPressed() {
        if (this.f6536b) {
            new Builder(this).setTitle((CharSequence) "Unsaved changes").setMessage((CharSequence) "Save the settings or reset them?").setNegativeButton((CharSequence) "Reset", (OnClickListener) new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    NotificationSettingsActivity.this.cancelClick(null);
                }
            }).setPositiveButton((CharSequence) "Save", (OnClickListener) new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    NotificationSettingsActivity.this.saveClick(null);
                }
            }).create().show();
        } else {
            super.onBackPressed();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_notification_settings);
        ListView listView = (ListView) findViewById(R.id.notification_settings_list);
        this.f6535a = new ArrayList<>(10);
        if (AppState.notificationSettings == null) {
            Crashlytics.m16437a(new Throwable("AppState notificationSettings is NULL"));
            finish();
            return;
        }
        Iterator it = AppState.notificationSettings.iterator();
        while (it.hasNext()) {
            this.f6535a.add(((NotificationSetting) it.next()).copy());
        }
        final NotificationSettingsAdapter notificationSettingsAdapter = new NotificationSettingsAdapter(this, this.f6535a);
        listView.setAdapter(notificationSettingsAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                NotificationSetting notificationSetting = (NotificationSetting) notificationSettingsAdapter.getItem(i);
                notificationSetting.disabled = Integer.valueOf(notificationSetting.disabled.intValue() == 1 ? 0 : 1);
                NotificationSettingsActivity.this.f6536b = true;
                notificationSettingsAdapter.notifyDataSetChanged();
            }
        });
    }

    public void saveClick(View view) {
        AppState.notificationSettings = this.f6535a;
        ArrayList arrayList = new ArrayList();
        Iterator it = this.f6535a.iterator();
        while (it.hasNext()) {
            NotificationSetting notificationSetting = (NotificationSetting) it.next();
            if (notificationSetting.disabled.intValue() == 1) {
                arrayList.add(notificationSetting.type);
            }
        }
        HashMap hashMap = new HashMap();
        hashMap.put("disabled", DataUtil.join(arrayList));
        ApiService.m14297a().mo14143e((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        finish();
    }
}
