package com.becandid.candid.activities;

import android.support.p001v4.widget.SwipeRefreshLayout;
import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.PostDetailsActivity;
import com.becandid.candid.views.StickerKeyboard;

public class PostDetailsActivity$$ViewBinder<T extends PostDetailsActivity> implements ViewBinder<T> {

    /* compiled from: PostDetailsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends PostDetailsActivity> implements Unbinder {

        /* renamed from: a */
        View f6638a;

        /* renamed from: b */
        private T f6639b;

        protected InnerUnbinder(T t) {
            this.f6639b = t;
        }

        public final void unbind() {
            if (this.f6639b == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6639b);
            this.f6639b = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.commentRecyclerView = null;
            t.mSwipeContainer = null;
            t.postHeaderText = null;
            t.commentInput = null;
            this.f6638a.setOnClickListener(null);
            t.commentButton = null;
            t.postPhoto = null;
            t.commentPhotoButton = null;
            t.stickerKeyboard = null;
            t.root = null;
            t.postSpinnyContainer = null;
            t.commentInputContainer = null;
            t.commentTextPlaceholder = null;
            t.stickerButton = null;
            t.commentPhotoClear = null;
            t.commentSpinnyContainer = null;
            t.mutePostIcon = null;
            t.relatedPostsButton = null;
            t.mReplyToBox = null;
            t.mReplyToTextHint = null;
            t.mReplyToClose = null;
            t.commentGifSearchBtn = null;
            t.commentPostBtnSpinny = null;
            t.commentInputButtonsVisible = null;
            t.commentInputButtonsHidden = null;
        }
    }

    public Unbinder bind(Finder finder, final T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.commentRecyclerView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.post_comments_recyclerview, "field 'commentRecyclerView'"), R.id.post_comments_recyclerview, "field 'commentRecyclerView'");
        t.mSwipeContainer = (SwipeRefreshLayout) finder.castView((View) finder.findRequiredView(obj, R.id.swipeContainer, "field 'mSwipeContainer'"), R.id.swipeContainer, "field 'mSwipeContainer'");
        t.postHeaderText = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.post_header_text, "field 'postHeaderText'"), R.id.post_header_text, "field 'postHeaderText'");
        t.commentInput = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.comment_text_input, "field 'commentInput'"), R.id.comment_text_input, "field 'commentInput'");
        View view = (View) finder.findRequiredView(obj, R.id.post_button, "field 'commentButton' and method 'saveComment'");
        t.commentButton = (ImageView) finder.castView(view, R.id.post_button, "field 'commentButton'");
        createUnbinder.f6638a = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                t.saveComment(view);
            }
        });
        t.postPhoto = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_photo, "field 'postPhoto'"), R.id.comment_photo, "field 'postPhoto'");
        t.commentPhotoButton = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_photo_button, "field 'commentPhotoButton'"), R.id.comment_photo_button, "field 'commentPhotoButton'");
        t.stickerKeyboard = (StickerKeyboard) finder.castView((View) finder.findRequiredView(obj, R.id.sticker_keyboard, "field 'stickerKeyboard'"), R.id.sticker_keyboard, "field 'stickerKeyboard'");
        t.root = (View) finder.findRequiredView(obj, R.id.post_root_view, "field 'root'");
        t.postSpinnyContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.post_spinny_container, "field 'postSpinnyContainer'"), R.id.post_spinny_container, "field 'postSpinnyContainer'");
        t.commentInputContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.comment_input_container, "field 'commentInputContainer'"), R.id.comment_input_container, "field 'commentInputContainer'");
        t.commentTextPlaceholder = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_text_placeholder, "field 'commentTextPlaceholder'"), R.id.comment_text_placeholder, "field 'commentTextPlaceholder'");
        t.stickerButton = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_sticker_button, "field 'stickerButton'"), R.id.comment_sticker_button, "field 'stickerButton'");
        t.commentPhotoClear = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_photo_clear, "field 'commentPhotoClear'"), R.id.comment_photo_clear, "field 'commentPhotoClear'");
        t.commentSpinnyContainer = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.comment_spinny_container, "field 'commentSpinnyContainer'"), R.id.comment_spinny_container, "field 'commentSpinnyContainer'");
        t.mutePostIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.post_mute_icon, "field 'mutePostIcon'"), R.id.post_mute_icon, "field 'mutePostIcon'");
        t.relatedPostsButton = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.related_posts_button, "field 'relatedPostsButton'"), R.id.related_posts_button, "field 'relatedPostsButton'");
        t.mReplyToBox = (View) finder.findRequiredView(obj, R.id.reply_to_box, "field 'mReplyToBox'");
        t.mReplyToTextHint = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.reply_to_text, "field 'mReplyToTextHint'"), R.id.reply_to_text, "field 'mReplyToTextHint'");
        t.mReplyToClose = (View) finder.findRequiredView(obj, R.id.reply_to_close, "field 'mReplyToClose'");
        t.commentGifSearchBtn = (View) finder.findRequiredView(obj, R.id.comment_gif_button, "field 'commentGifSearchBtn'");
        t.commentPostBtnSpinny = (ProgressBar) finder.castView((View) finder.findRequiredView(obj, R.id.post_btn_spinny, "field 'commentPostBtnSpinny'"), R.id.post_btn_spinny, "field 'commentPostBtnSpinny'");
        t.commentInputButtonsVisible = (View) finder.findRequiredView(obj, R.id.comment_input_buttons_visible, "field 'commentInputButtonsVisible'");
        t.commentInputButtonsHidden = (View) finder.findRequiredView(obj, R.id.comment_input_buttons_hidden, "field 'commentInputButtonsHidden'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
