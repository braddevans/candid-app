package com.becandid.candid.activities;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.YouTubePlayerActivity;
import com.google.android.youtube.player.YouTubePlayerView;

public class YouTubePlayerActivity$$ViewBinder<T extends YouTubePlayerActivity> implements ViewBinder<T> {

    /* compiled from: YouTubePlayerActivity$$ViewBinder */
    public static class InnerUnbinder<T extends YouTubePlayerActivity> implements Unbinder {

        /* renamed from: a */
        private T f6696a;

        protected InnerUnbinder(T t) {
            this.f6696a = t;
        }

        public final void unbind() {
            if (this.f6696a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6696a);
            this.f6696a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.youTubePlayerView = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.youTubePlayerView = (YouTubePlayerView) finder.castView((View) finder.findRequiredView(obj, R.id.youtube_player_view, "field 'youTubePlayerView'"), R.id.youtube_player_view, "field 'youTubePlayerView'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
