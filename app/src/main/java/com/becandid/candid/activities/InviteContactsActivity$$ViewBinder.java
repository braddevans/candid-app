package com.becandid.candid.activities;

import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity;

public class InviteContactsActivity$$ViewBinder<T extends InviteContactsActivity> implements ViewBinder<T> {

    /* compiled from: InviteContactsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends InviteContactsActivity> implements Unbinder {

        /* renamed from: a */
        private T f6400a;

        protected InnerUnbinder(T t) {
            this.f6400a = t;
        }

        public final void unbind() {
            if (this.f6400a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6400a);
            this.f6400a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mPlaceholder = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mPlaceholder = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.placeholder, "field 'mPlaceholder'"), R.id.placeholder, "field 'mPlaceholder'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
