package com.becandid.candid.activities;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.TutorialMuteActivity;

public class TutorialMuteActivity$$ViewBinder<T extends TutorialMuteActivity> implements ViewBinder<T> {

    /* compiled from: TutorialMuteActivity$$ViewBinder */
    public static class InnerUnbinder<T extends TutorialMuteActivity> implements Unbinder {

        /* renamed from: a */
        private T f6683a;

        protected InnerUnbinder(T t) {
            this.f6683a = t;
        }

        public final void unbind() {
            if (this.f6683a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6683a);
            this.f6683a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.muteYes = null;
            t.muteNo = null;
            t.continueSub = null;
            t.muteContainer = null;
            t.subscribeContainer = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.muteYes = (View) finder.findRequiredView(obj, R.id.tutorial_mute_yes, "field 'muteYes'");
        t.muteNo = (View) finder.findRequiredView(obj, R.id.tutorial_mute_no, "field 'muteNo'");
        t.continueSub = (View) finder.findRequiredView(obj, R.id.subscribe_continue, "field 'continueSub'");
        t.muteContainer = (View) finder.findRequiredView(obj, R.id.mute_container, "field 'muteContainer'");
        t.subscribeContainer = (View) finder.findRequiredView(obj, R.id.subscribe_container, "field 'subscribeContainer'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
