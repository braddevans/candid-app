package com.becandid.candid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentManager;
import android.support.p001v4.app.FragmentTransaction;
import android.widget.Toast;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.ContactsInfo;
import com.becandid.candid.data.Group;
import com.becandid.candid.fragments.onboarding.OnboardingAgeFragment;
import com.becandid.candid.fragments.onboarding.OnboardingCommunityFragment;
import com.becandid.candid.fragments.onboarding.OnboardingCommunitySimpleFragment;
import com.becandid.candid.fragments.onboarding.OnboardingContactsFragment;
import com.becandid.candid.fragments.onboarding.OnboardingFacebookFragment;
import com.becandid.candid.fragments.onboarding.OnboardingLocationFragment;
import com.becandid.candid.fragments.onboarding.OnboardingPhoneFragment;
import com.becandid.candid.fragments.onboarding.OnboardingTagsFragment;
import com.becandid.candid.fragments.onboarding.OnboardingVerifyFragment;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class OnboardingActivity extends BaseActivity {

    /* renamed from: a */
    private boolean f6542a = false;

    /* renamed from: b */
    private boolean f6543b = false;
    public List<Group> communitySuggestions;

    /* renamed from: d */
    private Bundle f6544d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public FragmentManager f6545e;

    /* renamed from: f */
    private OnboardingPhoneFragment f6546f;

    /* renamed from: g */
    private OnboardingCommunitySimpleFragment f6547g;

    /* renamed from: h */
    private OnboardingTagsFragment f6548h;

    /* renamed from: i */
    private Handler f6549i;

    /* renamed from: j */
    private int f6550j;

    /* renamed from: k */
    private int f6551k;

    /* renamed from: l */
    private int f6552l;

    /* renamed from: m */
    private String f6553m;
    public String otpCode;

    /* renamed from: a */
    private Fragment m9215a(String str) {
        m9217a();
        if (this.f6545e.findFragmentByTag(str) != null) {
            return this.f6545e.findFragmentByTag(str);
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -1480249367:
                if (str.equals("community")) {
                    c = 4;
                    break;
                }
                break;
            case -819951495:
                if (str.equals("verify")) {
                    c = 2;
                    break;
                }
                break;
            case -567451565:
                if (str.equals("contacts")) {
                    c = 3;
                    break;
                }
                break;
            case 3260:
                if (str.equals("fb")) {
                    c = 0;
                    break;
                }
                break;
            case 96511:
                if (str.equals("age")) {
                    c = 6;
                    break;
                }
                break;
            case 3552281:
                if (str.equals("tags")) {
                    c = 7;
                    break;
                }
                break;
            case 106642798:
                if (str.equals("phone")) {
                    c = 1;
                    break;
                }
                break;
            case 1901043637:
                if (str.equals("location")) {
                    c = 5;
                    break;
                }
                break;
            case 2038830570:
                if (str.equals("create_account")) {
                    c = 8;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return new OnboardingFacebookFragment();
            case 1:
                return new OnboardingPhoneFragment();
            case 2:
                return new OnboardingVerifyFragment();
            case 3:
                return new OnboardingContactsFragment();
            case 4:
                return (this.communitySuggestions == null || this.communitySuggestions.isEmpty()) ? new OnboardingCommunitySimpleFragment() : new OnboardingCommunityFragment();
            case 5:
                return new OnboardingLocationFragment();
            case 6:
                return new OnboardingAgeFragment();
            case 7:
                return new OnboardingTagsFragment();
            case 8:
                m9218b();
                return null;
            default:
                return null;
        }
    }

    /* renamed from: a */
    private void m9217a() {
        if (this.f6545e == null) {
            this.f6545e = getSupportFragmentManager();
        }
    }

    /* renamed from: b */
    private void m9218b() {
        ApiService.m14297a().mo14140e().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                Toast.makeText(OnboardingActivity.this, R.string.signup_error, 0).show();
            }

            public void onNext(NetworkData networkData) {
                AppState.saveState(GossipApplication.m9012a());
                if (!networkData.success) {
                    return;
                }
                if (networkData.is_new_user) {
                    OnboardingActivity.this.switchFragment("create_account");
                } else {
                    OnboardingActivity.this.finishSyncAccount();
                }
            }
        });
    }

    public void finish() {
        super.finish();
        setResult(-1);
    }

    public void finishGetGroups() {
        if (!AppState.config.has("skip_groups_onboarding")) {
            startActivity(new Intent(this, OnboardingGroupsActivity.class));
            finish();
        } else if (AppState.config.getInt("skip_groups_onboarding", 0) == 1) {
            ApiService.m14297a().mo14144f().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Toast.makeText(OnboardingActivity.this, "Unable to get get your recommended groups. Please try again", 0).show();
                }

                public void onNext(NetworkData networkData) {
                    OnboardingActivity.this.finishSyncAccount();
                }
            });
        }
        ApiService.m14297a().mo14168n("onboarding/tags/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
    }

    public void finishSyncAccount() {
        Intent intent;
        ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        if (AppState.referralPostId != 0) {
            intent = new Intent(this, PostDetailsActivity.class);
            intent.putExtra("post_id", AppState.referralPostId);
        } else {
            intent = new Intent(this, MainTabsActivity.class);
        }
        startActivity(intent);
        finish();
    }

    public String getNextOnboardingFragmentName(String str) {
        if (AppState.getOnboardingOrders() == null || AppState.getOnboardingOrders().isEmpty()) {
            return null;
        }
        if (str == null) {
            return (String) AppState.getOnboardingOrders().get(0);
        }
        int indexOf = AppState.getOnboardingOrders().indexOf(str);
        if (indexOf < AppState.getOnboardingOrders().size() - 1) {
            return (String) AppState.getOnboardingOrders().get(indexOf + 1);
        }
        return null;
    }

    public String getPhoneNumber() {
        return this.f6553m;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (AppState.contactsInfo == null) {
            AppState.contactsInfo = new ContactsInfo();
        }
        super.onCreate(bundle);
        if (AppState.config.has("skip_age_onboarding")) {
            this.f6551k = AppState.config.getInt("skip_age_onboarding", 0);
        }
        if (AppState.config.has("skip_tags_onboarding")) {
            this.f6550j = AppState.config.getInt("skip_tags_onboarding", 0);
        }
        if (AppState.config.has("skip_onboarding_community")) {
            this.f6552l = AppState.config.getInt("skip_onboarding_community", 0);
        }
        this.f6549i = new Handler(getMainLooper());
        setContentView((int) R.layout.activity_onboarding);
        m9217a();
        this.f6544d = getIntent().getExtras();
        if (this.f6544d != null && this.f6544d.containsKey("second_fragment")) {
            this.f6546f = new OnboardingPhoneFragment();
            this.f6546f.setArguments(this.f6544d);
            this.f6545e.beginTransaction().add(R.id.onboarding_placeholder, this.f6546f, "phone").commit();
        } else if (this.f6544d != null && this.f6544d.containsKey(MeSettingsActivity.TAG_SETTINGS_KEY)) {
            this.f6548h = new OnboardingTagsFragment();
            this.f6548h.setArguments(this.f6544d);
            this.f6545e.beginTransaction().add(R.id.onboarding_placeholder, this.f6548h, "tags").commit();
        } else if (this.f6544d == null || !this.f6544d.containsKey(MainTabsActivity.ONBOARDING_COMMUNITY_KEY)) {
            String nextOnboardingFragmentName = getNextOnboardingFragmentName(null);
            if (nextOnboardingFragmentName != null && nextOnboardingFragmentName.equals("create_account")) {
                m9218b();
            } else if (nextOnboardingFragmentName != null) {
                this.f6545e.beginTransaction().replace(R.id.onboarding_placeholder, m9215a(nextOnboardingFragmentName), nextOnboardingFragmentName).commit();
            }
        } else {
            this.f6547g = new OnboardingCommunitySimpleFragment();
            this.f6547g.setArguments(this.f6544d);
            this.f6545e.beginTransaction().add(R.id.onboarding_placeholder, this.f6547g, "community").commit();
        }
        addToSubscriptionList(RxBus.m14573a().mo14348a(C2631at.class, (bjy<T>) new bjy<C2631at>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2631at atVar) {
                Fragment findFragmentByTag = OnboardingActivity.this.f6545e.findFragmentByTag(atVar.f10610a);
                if (atVar.f10612c != null) {
                    if (findFragmentByTag != null && findFragmentByTag.isVisible()) {
                        OnboardingActivity.this.switchFragment(atVar.f10610a, atVar.f10611b, atVar.f10612c);
                    }
                } else if (findFragmentByTag != null && findFragmentByTag.isVisible()) {
                    if (findFragmentByTag instanceof OnboardingContactsFragment) {
                        OnboardingActivity.this.switchFragment(atVar.f10610a, OnboardingActivity.this.getNextOnboardingFragmentName("phone"), null);
                    } else {
                        OnboardingActivity.this.switchFragment(atVar.f10610a);
                    }
                }
            }
        }));
    }

    public void setPhoneNumber(String str) {
        this.f6553m = str;
    }

    public void switchFragment(String str) {
        switchFragment(str, getNextOnboardingFragmentName(str), null);
    }

    public void switchFragment(String str, String str2, Bundle bundle) {
        m9217a();
        Fragment findFragmentByTag = this.f6545e.findFragmentByTag(str);
        if (findFragmentByTag != null && findFragmentByTag.isVisible()) {
            C2699jj.m14613a(findFragmentByTag.getContext(), findFragmentByTag.getView());
        }
        if (str2 != null && str2.equals("community") && this.f6552l == 1) {
            str2 = "location";
        }
        if (str2 != null && str2.equals("age") && (this.f6551k == 1 || !(AppState.fbInfo == null || AppState.fbInfo.age == null))) {
            str2 = "tags";
        }
        if (str2 != null && str2.equals("tags") && this.f6550j == 1) {
            finishGetGroups();
        } else if (str2 != null) {
            Fragment a = m9215a(str2);
            ApiService.m14297a().mo14168n("onboarding/" + str2 + "/start").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
            if (bundle != null) {
                a.setArguments(bundle);
            }
            if (a != null) {
                try {
                    FragmentTransaction beginTransaction = this.f6545e.beginTransaction();
                    beginTransaction.replace(R.id.onboarding_placeholder, a, str2);
                    beginTransaction.commit();
                } catch (IllegalStateException e) {
                    Crashlytics.m16437a((Throwable) e);
                }
            }
        }
    }
}
