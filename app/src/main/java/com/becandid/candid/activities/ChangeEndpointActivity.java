package com.becandid.candid.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.p003v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;

public class ChangeEndpointActivity extends BaseActivity {
    public String PROD_ENDPOINT = "https://becandid.com/";
    public String STAGING_ENDPOINT = "https://dev.becandid.com/";

    /* renamed from: a */
    private String f6168a = this.PROD_ENDPOINT;
    @BindView(2131624087)
    EditText mEndpointText;
    @BindView(2131624088)
    RadioGroup mRadioGroup;

    /* renamed from: a */
    private String m9023a() {
        SharedPreferences sharedPreferences = getSharedPreferences("com.becandid.candid", 0);
        return sharedPreferences.contains("base_url") ? sharedPreferences.getString("base_url", GossipApplication.f6096d) : GossipApplication.f6096d;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_change_endpoint);
        ButterKnife.bind((Activity) this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator((int) R.drawable.back_chevron);
        if (AppState.mBaseUrl.equals(this.PROD_ENDPOINT)) {
            this.mRadioGroup.check(R.id.radio_prod);
        } else if (AppState.mBaseUrl.equals(this.STAGING_ENDPOINT)) {
            this.mRadioGroup.check(R.id.radio_staging);
        } else {
            this.mRadioGroup.check(R.id.radio_custom);
        }
        this.mEndpointText.setText(m9023a());
        this.mEndpointText.setEnabled(false);
    }

    public void onRadioButtonClicked(View view) {
        boolean isChecked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.radio_prod /*2131624089*/:
                if (isChecked) {
                    this.f6168a = this.PROD_ENDPOINT;
                    this.mEndpointText.setText(this.f6168a);
                    this.mEndpointText.setEnabled(false);
                    return;
                }
                break;
            case R.id.radio_staging /*2131624090*/:
                break;
            case R.id.radio_custom /*2131624091*/:
                break;
            default:
                return;
        }
        if (isChecked) {
            this.f6168a = this.STAGING_ENDPOINT;
            this.mEndpointText.setText(this.f6168a);
            this.mEndpointText.setEnabled(false);
            return;
        }
        if (isChecked) {
            this.mEndpointText.setEnabled(true);
            this.mEndpointText.requestFocus();
            ((InputMethodManager) getSystemService("input_method")).showSoftInput(this.mEndpointText, 1);
        }
    }

    public void onSaveEndpoint(View view) {
        AppState.mBaseUrl = this.mEndpointText.getText().toString();
        AppState.saveState(GossipApplication.m9012a());
        ApiService.m14297a().mo14114a(this.mEndpointText.getText().toString());
        finish();
    }
}
