package com.becandid.candid.activities;

import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Video;
import android.support.p001v4.app.NotificationCompat;
import android.widget.Toast;
import com.becandid.candid.R;
import com.becandid.candid.fragments.VideoFragment;
import com.becandid.candid.fragments.VideoFragment.C1642a;
import com.becandid.candid.fragments.VideoSpritesSelectFragment;
import com.becandid.candid.util.video.FLSprite.SpecialEffect;
import java.io.Serializable;

public class AddVideoActivity extends BaseActivity implements C1642a, Serializable {
    public static int PERMISSION_RECORD_AUDIO = 990;
    public static int SELECT_VIDEO_FROM_GALLERY = 5;

    /* renamed from: a */
    VideoFragment f6116a;

    /* renamed from: b */
    VideoSpritesSelectFragment f6117b;

    /* JADX INFO: finally extract failed */
    public static String getDataColumn(Context context, Uri uri, String str, String[] strArr) {
        Cursor cursor = null;
        String str2 = "_data";
        try {
            Cursor query = context.getContentResolver().query(uri, new String[]{"_data"}, str, strArr, null);
            if (query == null || !query.moveToFirst()) {
                if (query != null) {
                    query.close();
                }
                return null;
            }
            String string = query.getString(query.getColumnIndexOrThrow("_data"));
            if (query == null) {
                return string;
            }
            query.close();
            return string;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @TargetApi(19)
    public static String getPath(Context context, Uri uri) {
        if (!(VERSION.SDK_INT >= 19) || !DocumentsContract.isDocumentUri(context, uri)) {
            if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
            if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
            return null;
        } else if (isExternalStorageDocument(uri)) {
            String[] split = DocumentsContract.getDocumentId(uri).split(":");
            if ("primary".equalsIgnoreCase(split[0])) {
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            }
            return null;
        } else if (isDownloadsDocument(uri)) {
            return getDataColumn(context, ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(DocumentsContract.getDocumentId(uri)).longValue()), null, null);
        } else if (!isMediaDocument(uri)) {
            return null;
        } else {
            String[] split2 = DocumentsContract.getDocumentId(uri).split(":");
            String str = split2[0];
            Uri uri2 = null;
            if ("image".equals(str)) {
                uri2 = Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(str)) {
                uri2 = Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(str)) {
                uri2 = Audio.Media.EXTERNAL_CONTENT_URI;
            }
            String str2 = "_id=?";
            return getDataColumn(context, uri2, "_id=?", new String[]{split2[1]});
        }
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void finishActivity() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == SELECT_VIDEO_FROM_GALLERY && intent != null) {
            String path = getPath(this, intent.getData());
            if (path == null) {
                Toast.makeText(this, "Sorry that video cannot be uploaded.", 0).show();
                return;
            }
            Intent intent2 = new Intent(this, PreviewVideoActivity.class);
            intent2.setFlags(33554432);
            intent2.putExtra(PreviewVideoActivity.VIDEO_PATH, path);
            intent2.putExtra("uploaded", 1);
            startActivity(intent2);
            finish();
        }
    }

    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_video_record);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        getWindow().addFlags(NotificationCompat.FLAG_HIGH_PRIORITY);
        if (bundle == null) {
            this.f6116a = new VideoFragment();
            getFragmentManager().beginTransaction().add(R.id.containerVideo, this.f6116a).commit();
            this.f6117b = new VideoSpritesSelectFragment();
            FragmentTransaction beginTransaction = getFragmentManager().beginTransaction();
            beginTransaction.add(R.id.containerSprites, this.f6117b);
            beginTransaction.hide(this.f6117b);
            beginTransaction.commit();
        }
    }

    public void onOpenSpritesSelectView() {
        if (this.f6117b == null) {
            this.f6117b = new VideoSpritesSelectFragment();
        }
        FragmentTransaction beginTransaction = getFragmentManager().beginTransaction();
        beginTransaction.setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_up, R.animator.slide_down);
        beginTransaction.show(this.f6117b).addToBackStack(null).commit();
    }

    public void onPickFromGallery() {
        startActivityForResult(PhotoHelper.m14547a(), SELECT_VIDEO_FROM_GALLERY);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onSpriteSelected(int i, SpecialEffect specialEffect) {
        if (this.f6116a != null) {
            this.f6116a.mo10464a(i, specialEffect);
        }
    }

    public void onVideoRecordFinished(String str) {
        if (str != null) {
            Intent intent = new Intent(this, PreviewVideoActivity.class);
            intent.setFlags(33554432);
            intent.putExtra(PreviewVideoActivity.VIDEO_PATH, str);
            intent.putExtra("uploaded", 0);
            startActivity(intent);
            finish();
        }
    }
}
