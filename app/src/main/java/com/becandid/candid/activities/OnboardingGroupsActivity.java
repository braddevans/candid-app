package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.views.GroupStackView;
import com.becandid.candid.views.GroupStackView.C1822c;
import java.util.ArrayList;
import p012rx.schedulers.Schedulers;

public class OnboardingGroupsActivity extends BaseActivity implements C1822c {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public int f6557a = 10;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f6558b = 5;
    @BindView(2131624319)
    Button ogButton;
    @BindView(2131624321)
    TextView ogEmpty;
    @BindView(2131624317)
    TextView ogHeader;
    @BindView(2131624320)
    LinearLayout ogSpinny;
    @BindView(2131624318)
    GroupStackView ogStack;

    /* renamed from: a */
    private bjy m9220a() {
        return new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Toast.makeText(OnboardingGroupsActivity.this, "Unable to get get your recommended groups. Please try again", 0).show();
                Crashlytics.m16437a(th);
                OnboardingGroupsActivity.this.ogSpinny.setVisibility(8);
                OnboardingGroupsActivity.this.m9225c();
            }

            public void onNext(NetworkData networkData) {
                if (networkData.has_more) {
                    OnboardingGroupsActivity.this.ogStack.setLoadMoreGroups(true);
                } else {
                    OnboardingGroupsActivity.this.ogStack.setLoadMoreGroups(false);
                }
                if (OnboardingGroupsActivity.this.ogStack.mo10819b() == 0) {
                    OnboardingGroupsActivity.this.ogStack.setGroups(networkData.groups);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            OnboardingGroupsActivity.this.ogSpinny.setVisibility(8);
                            OnboardingGroupsActivity.this.ogStack.setVisibility(0);
                        }
                    }, 500);
                    if (networkData.groups != null && !networkData.groups.isEmpty()) {
                        OnboardingGroupsActivity.this.f6557a = networkData.onboarding_min_joins;
                        if (AppState.config != null && AppState.config.has("groups_load_more_count")) {
                            OnboardingGroupsActivity.this.f6558b = AppState.config.getInt("groups_load_more_count");
                        }
                        if (OnboardingGroupsActivity.this.f6557a >= networkData.groups.size()) {
                            OnboardingGroupsActivity.this.f6557a = networkData.groups.size();
                        }
                        OnboardingGroupsActivity.this.m9231f();
                        return;
                    }
                    return;
                }
                OnboardingGroupsActivity.this.ogStack.mo10817a(networkData.groups);
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9224b() {
        this.ogButton.setEnabled(false);
        this.ogSpinny.setVisibility(0);
        ApiService.m14297a().mo14144f().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9220a());
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m9225c() {
        this.ogButton.setText("Retry");
        this.ogButton.setEnabled(true);
        this.ogButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OnboardingGroupsActivity.this.m9227d();
                OnboardingGroupsActivity.this.ogButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        OnboardingGroupsActivity.this.launchMainActivity(OnboardingGroupsActivity.this.ogButton);
                    }
                });
                OnboardingGroupsActivity.this.m9224b();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m9227d() {
        this.ogHeader.setText(AppState.config.getString("onboarding_groups_header", getString(R.string.join_groups)));
        this.ogButton.setText(AppState.config.getString("onboarding_groups_done", getString(R.string.groups_continue)));
    }

    /* renamed from: e */
    private void m9229e() {
        if (this.ogStack.mo10818a() && this.ogStack.mo10819b() <= this.f6558b) {
            ApiService.m14297a().mo14144f().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9220a());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m9231f() {
        if (!AppState.config.getString("onboarding_groups_header").equals(getString(R.string.join_groups))) {
            return;
        }
        if (this.f6557a <= 0) {
            this.ogHeader.setText("Continue");
        } else {
            this.ogHeader.setText("Join " + Integer.toString(this.f6557a) + " groups to get started");
        }
    }

    public void launchMainActivity(View view) {
        Intent intent;
        ApiService.m14297a().mo14168n("onboarding/groups/enabled").mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        if (AppState.referralPostId != 0) {
            intent = new Intent(this, PostDetailsActivity.class);
            intent.putExtra("post_id", AppState.referralPostId);
        } else {
            intent = new Intent(this, MainTabsActivity.class);
        }
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_onboarding_groups);
        ButterKnife.bind((Activity) this);
        m9227d();
        this.ogStack.setListener(this);
        if (AppState.groups == null) {
            AppState.groups = new ArrayList();
        }
        m9224b();
    }

    public void onEmptyGroups() {
        this.ogStack.setVisibility(8);
        this.ogEmpty.setVisibility(0);
    }

    public void onJoinGroup(Group group) {
        this.f6557a--;
        m9231f();
        if (this.f6557a == 0 || this.ogStack.mo10819b() <= 1) {
            this.ogButton.setEnabled(true);
        }
        m9229e();
    }

    public void onSkipGroup(Group group) {
        if (!this.ogStack.mo10818a() && this.ogStack.mo10819b() < this.f6557a) {
            this.f6557a = this.ogStack.mo10819b();
            m9231f();
        }
        if (this.ogStack.mo10819b() <= 1) {
            this.ogButton.setEnabled(true);
        }
        m9229e();
    }

    public void onTouchEvent(Group group) {
    }
}
