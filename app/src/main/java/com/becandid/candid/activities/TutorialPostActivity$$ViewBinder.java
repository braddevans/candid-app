package com.becandid.candid.activities;

import android.support.p001v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.TutorialPostActivity;

public class TutorialPostActivity$$ViewBinder<T extends TutorialPostActivity> implements ViewBinder<T> {

    /* compiled from: TutorialPostActivity$$ViewBinder */
    public static class InnerUnbinder<T extends TutorialPostActivity> implements Unbinder {

        /* renamed from: a */
        private T f6686a;

        protected InnerUnbinder(T t) {
            this.f6686a = t;
        }

        public final void unbind() {
            if (this.f6686a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6686a);
            this.f6686a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.viewPager = null;
            t.ind1 = null;
            t.ind2 = null;
            t.ind3 = null;
            t.ind4 = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.viewPager = (ViewPager) finder.castView((View) finder.findRequiredView(obj, R.id.tutorial_viewpager, "field 'viewPager'"), R.id.tutorial_viewpager, "field 'viewPager'");
        t.ind1 = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.tutorial_indicator_1, "field 'ind1'"), R.id.tutorial_indicator_1, "field 'ind1'");
        t.ind2 = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.tutorial_indicator_2, "field 'ind2'"), R.id.tutorial_indicator_2, "field 'ind2'");
        t.ind3 = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.tutorial_indicator_3, "field 'ind3'"), R.id.tutorial_indicator_3, "field 'ind3'");
        t.ind4 = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.tutorial_indicator_4, "field 'ind4'"), R.id.tutorial_indicator_4, "field 'ind4'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
