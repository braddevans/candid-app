package com.becandid.candid.activities;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.BaseFullScreenActivity;

public class BaseFullScreenActivity$$ViewBinder<T extends BaseFullScreenActivity> implements ViewBinder<T> {

    /* compiled from: BaseFullScreenActivity$$ViewBinder */
    public static class InnerUnbinder<T extends BaseFullScreenActivity> implements Unbinder {

        /* renamed from: a */
        private T f6167a;

        protected InnerUnbinder(T t) {
            this.f6167a = t;
        }

        public final void unbind() {
            if (this.f6167a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6167a);
            this.f6167a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.fullscreenFooter = null;
            t.userIcon = null;
            t.userName = null;
            t.likeContainer = null;
            t.likeIcon = null;
            t.trueIcon = null;
            t.likeCount = null;
            t.trueCount = null;
            t.falseCount = null;
            t.dislikeContainer = null;
            t.dislikeIcon = null;
            t.falseIcon = null;
            t.dislikeCount = null;
            t.commentContainer = null;
            t.commentCount = null;
            t.shareContainer = null;
            t.replyContainer = null;
            t.fullscreenGradient = null;
            t.fullscreenHeader = null;
            t.fullscreenDownload = null;
            t.fullscreenExit = null;
            t.imageVerified = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.fullscreenFooter = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_footer, "field 'fullscreenFooter'"), R.id.fullscreen_footer, "field 'fullscreenFooter'");
        t.userIcon = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_user_icon, "field 'userIcon'"), R.id.fullscreen_user_icon, "field 'userIcon'");
        t.userName = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_user_name, "field 'userName'"), R.id.fullscreen_user_name, "field 'userName'");
        t.likeContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_like_container, "field 'likeContainer'"), R.id.fullscreen_like_container, "field 'likeContainer'");
        t.likeIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_like, "field 'likeIcon'"), R.id.fullscreen_like, "field 'likeIcon'");
        t.trueIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_true, "field 'trueIcon'"), R.id.fullscreen_true, "field 'trueIcon'");
        t.likeCount = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_like_count, "field 'likeCount'"), R.id.fullscreen_like_count, "field 'likeCount'");
        t.trueCount = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_true_count, "field 'trueCount'"), R.id.fullscreen_true_count, "field 'trueCount'");
        t.falseCount = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_false_count, "field 'falseCount'"), R.id.fullscreen_false_count, "field 'falseCount'");
        t.dislikeContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_dislike_container, "field 'dislikeContainer'"), R.id.fullscreen_dislike_container, "field 'dislikeContainer'");
        t.dislikeIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_dislike, "field 'dislikeIcon'"), R.id.fullscreen_dislike, "field 'dislikeIcon'");
        t.falseIcon = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_false, "field 'falseIcon'"), R.id.fullscreen_false, "field 'falseIcon'");
        t.dislikeCount = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_dislike_count, "field 'dislikeCount'"), R.id.fullscreen_dislike_count, "field 'dislikeCount'");
        t.commentContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_comment_container, "field 'commentContainer'"), R.id.fullscreen_comment_container, "field 'commentContainer'");
        t.commentCount = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_comment_count, "field 'commentCount'"), R.id.fullscreen_comment_count, "field 'commentCount'");
        t.shareContainer = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_share, "field 'shareContainer'"), R.id.fullscreen_share, "field 'shareContainer'");
        t.replyContainer = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_reply, "field 'replyContainer'"), R.id.fullscreen_reply, "field 'replyContainer'");
        t.fullscreenGradient = (FrameLayout) finder.castView((View) finder.findOptionalView(obj, R.id.fullscreen_gradient, null), R.id.fullscreen_gradient, "field 'fullscreenGradient'");
        t.fullscreenHeader = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_header, "field 'fullscreenHeader'"), R.id.fullscreen_header, "field 'fullscreenHeader'");
        t.fullscreenDownload = (View) finder.findRequiredView(obj, R.id.fullscreen_download, "field 'fullscreenDownload'");
        t.fullscreenExit = (View) finder.findRequiredView(obj, R.id.fullscreen_exit, "field 'fullscreenExit'");
        t.imageVerified = (View) finder.findRequiredView(obj, R.id.image_verified, "field 'imageVerified'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
