package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.thirdparty.nocropper.CropperView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageCropActivity extends BaseActivity {
    public static String ASPECT_X = "aspectX";
    public static String ASPECT_Y = "aspectY";
    public static String IMAGE_PATH = "image_path";
    public static String USE_FULL_IMAGE = "use_full_image";

    /* renamed from: a */
    private boolean f6378a = false;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Bitmap f6379b;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Handler f6380d;

    /* renamed from: e */
    private int f6381e = 5;

    /* renamed from: f */
    private int f6382f = 6;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public String f6383g;
    @BindView(2131624243)
    ImageView mCropButton;
    @BindView(2131624240)
    RelativeLayout mCropContainer;
    @BindView(2131624241)
    FrameLayout mCropperFrameLayout;
    @BindView(2131624242)
    CropperView mImageView;
    @BindView(2131624101)
    ProgressBar mProgressBar;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9139a() {
        Intent intent = getIntent();
        intent.putExtra(USE_FULL_IMAGE, false);
        Bitmap croppedBitmap = this.mImageView.getCroppedBitmap();
        if (croppedBitmap != null) {
            try {
                String str = Environment.getExternalStorageDirectory() + "/crop_test.jpg";
                m9141a(croppedBitmap, new File(str), 100);
                intent.putExtra(IMAGE_PATH, str);
                intent.putExtra(ASPECT_X, croppedBitmap.getWidth());
                intent.putExtra(ASPECT_Y, croppedBitmap.getHeight());
                setResult(-1, intent);
                C2699jj.m14627b(str, new ExifInterface(str).getAttributeInt("Orientation", 1));
            } catch (Exception e) {
                Toast.makeText(this, "Error while processing photo...", 0).show();
                setResult(0);
            }
        } else {
            Toast.makeText(this, "Error while processing photo...", 0).show();
            setResult(0);
        }
        finish();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9140a(int i, int i2) {
        TypedArray obtainStyledAttributes = getTheme().obtainStyledAttributes(new int[]{16843499});
        int dimension = (int) obtainStyledAttributes.getDimension(0, 0.0f);
        obtainStyledAttributes.recycle();
        int height = (int) (((double) ((((float) (i2 - this.mCropperFrameLayout.getHeight())) - ((float) this.mCropButton.getHeight())) / 2.0f)) + 0.5d);
        if (height > 0) {
            ((LayoutParams) this.mCropButton.getLayoutParams()).setMargins(0, 0, 0, height);
        }
        this.mCropContainer.requestLayout();
    }

    /* renamed from: a */
    private void m9141a(Bitmap bitmap, File file, int i) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        bitmap.compress(CompressFormat.JPEG, i, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9145a(String str) {
        try {
            this.f6379b = C2699jj.m14628c(str, 1920);
            if (this.mImageView.getWidth() != 0) {
                this.mImageView.setMaxZoom((float) ((this.mImageView.getWidth() * 2) / 1920));
            } else {
                this.mImageView.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
                    public boolean onPreDraw() {
                        ImageCropActivity.this.mImageView.getViewTreeObserver().removeOnPreDrawListener(this);
                        ImageCropActivity.this.mImageView.setMaxZoom((float) ((ImageCropActivity.this.mImageView.getWidth() * 2) / 1920));
                        return true;
                    }
                });
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    ImageCropActivity.this.mImageView.setImageBitmap(ImageCropActivity.this.f6379b);
                    ImageCropActivity.this.mProgressBar.setVisibility(8);
                }
            });
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(ImageCropActivity.this, "Could not process the image...  Please try again.", 0).show();
                    ImageCropActivity.this.finish();
                }
            });
        }
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_image_crop);
        ButterKnife.bind((Activity) this);
        this.f6380d = new Handler(getMainLooper());
        this.f6381e = getIntent().getIntExtra(ASPECT_X, 1);
        this.f6382f = getIntent().getIntExtra(ASPECT_Y, 1);
        this.f6383g = getIntent().getStringExtra(IMAGE_PATH);
        this.mProgressBar.setVisibility(0);
        this.mCropButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ImageCropActivity.this.m9139a();
            }
        });
        this.mCropContainer.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                ImageCropActivity.this.mCropContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ImageCropActivity.this.m9140a(ImageCropActivity.this.mCropContainer.getWidth(), ImageCropActivity.this.mCropContainer.getHeight());
                ImageCropActivity.this.f6380d.post(new Runnable() {
                    public void run() {
                        ImageCropActivity.this.m9145a(ImageCropActivity.this.f6383g);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
