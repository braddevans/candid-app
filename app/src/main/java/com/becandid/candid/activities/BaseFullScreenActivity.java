package com.becandid.candid.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.support.p001v4.app.ActivityCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity.InviteFlowTypes;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.EmptyClass;
import com.becandid.candid.data.Post;
import com.becandid.candid.data.UpdatePost;
import com.becandid.candid.data.UpdatePost.Updates;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import p012rx.schedulers.Schedulers;

public abstract class BaseFullScreenActivity extends BaseActivity {
    @BindView(2131624213)
    RelativeLayout commentContainer;
    @BindView(2131624215)
    TextView commentCount;
    protected boolean cropImage = true;
    @BindView(2131624207)
    RelativeLayout dislikeContainer;
    @BindView(2131624211)
    TextView dislikeCount;
    @BindView(2131624208)
    ImageView dislikeIcon;
    @BindView(2131624212)
    TextView falseCount;
    @BindView(2131624209)
    ImageView falseIcon;
    @BindView(2131624192)
    View fullscreenDownload;
    @BindView(2131624187)
    View fullscreenExit;
    @BindView(2131624195)
    RelativeLayout fullscreenFooter;
    @BindView(2131624194)
    FrameLayout fullscreenGradient;
    @BindView(2131624191)
    RelativeLayout fullscreenHeader;
    @BindView(2131624193)
    View imageVerified;
    @BindView(2131624201)
    RelativeLayout likeContainer;
    @BindView(2131624205)
    TextView likeCount;
    @BindView(2131624202)
    ImageView likeIcon;
    protected int likeValue;
    protected Activity mContext;
    protected GlideDrawable mGlideDrawable;
    protected boolean noFooter;
    protected boolean noOverlay;
    protected int numDislikes;
    protected int numFalse;
    protected int numLikes;
    protected int numTrue;
    protected int opinionValue;
    @BindView(2131624217)
    TextView replyContainer;
    @BindView(2131624216)
    TextView shareContainer;
    protected String shareInfoImg;
    protected String shareInfoUrl;
    protected String sourceUrl;
    @BindView(2131624206)
    TextView trueCount;
    @BindView(2131624203)
    ImageView trueIcon;
    @BindView(2131624197)
    TextView userIcon;
    @BindView(2131624199)
    TextView userName;

    public void disableLikes() {
        this.likeContainer.setEnabled(false);
        this.dislikeContainer.setEnabled(false);
    }

    public void enableLikes() {
        this.likeContainer.setEnabled(true);
        this.dislikeContainer.setEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void fadeIn() {
        this.fullscreenHeader.animate().setDuration(200).alpha(1.0f).setListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                super.onAnimationEnd(animator);
                BaseFullScreenActivity.this.fullscreenHeader.setVisibility(0);
            }
        });
        if (!this.noFooter) {
            this.fullscreenFooter.animate().setDuration(200).alpha(1.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animator) {
                    super.onAnimationEnd(animator);
                    BaseFullScreenActivity.this.fullscreenFooter.setVisibility(0);
                }
            });
            if (this.fullscreenGradient != null) {
                this.fullscreenGradient.animate().setDuration(200).alpha(1.0f).setListener(new AnimatorListenerAdapter() {
                    public void onAnimationStart(Animator animator) {
                        super.onAnimationEnd(animator);
                        BaseFullScreenActivity.this.fullscreenGradient.setVisibility(0);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public void fadeOut() {
        this.fullscreenHeader.animate().setDuration(200).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                BaseFullScreenActivity.this.fullscreenHeader.setVisibility(8);
            }
        });
        if (!this.noFooter) {
            this.fullscreenFooter.animate().setDuration(200).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    super.onAnimationEnd(animator);
                    BaseFullScreenActivity.this.fullscreenFooter.setVisibility(8);
                }
            });
            if (this.fullscreenGradient != null) {
                this.fullscreenGradient.animate().setDuration(200).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        super.onAnimationEnd(animator);
                        BaseFullScreenActivity.this.fullscreenGradient.setVisibility(8);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == 1104 && iArr.length > 0 && iArr[0] == 0) {
            saveImage();
            AccountUtils.m14503a();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if (extras.containsKey("from_camera") && extras.getInt("from_camera") == 1) {
            this.imageVerified.setVisibility(0);
        }
        this.fullscreenDownload.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(BaseFullScreenActivity.this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                    ActivityCompat.requestPermissions(BaseFullScreenActivity.this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1104);
                    return;
                }
                BaseFullScreenActivity.this.saveImage();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void saveImage() {
        if (this.mGlideDrawable instanceof GlideBitmapDrawable) {
            Bitmap b = ((GlideBitmapDrawable) this.mGlideDrawable).mo15859b();
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_PICTURES + File.separator + "Candid" + File.separator);
            if (!file.exists()) {
                file.mkdir();
            }
            File file2 = new File(file, new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".png");
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                b.compress(CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                ContentValues contentValues = new ContentValues();
                contentValues.put("mime_type", "image/png");
                contentValues.put("date_added", Long.valueOf(System.currentTimeMillis()));
                contentValues.put("datetaken", Long.valueOf(System.currentTimeMillis()));
                contentValues.put("_data", file2.toString());
                getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, contentValues);
                Toast.makeText(this, "Image saved", 0).show();
            } catch (Exception e) {
                Toast.makeText(this, "An error occured. Please try again later.", 0).show();
            }
        } else if (this.mGlideDrawable instanceof GifDrawable) {
            GifDrawable psVar = (GifDrawable) this.mGlideDrawable;
            File file3 = new File(Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_PICTURES + File.separator + "Candid" + File.separator);
            if (!file3.exists()) {
                file3.mkdir();
            }
            File file4 = new File(file3, new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".gif");
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file4);
                fileOutputStream2.write(psVar.mo15890d());
                fileOutputStream2.flush();
                fileOutputStream2.close();
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("mime_type", "image/gif");
                contentValues2.put("date_added", Long.valueOf(System.currentTimeMillis()));
                contentValues2.put("datetaken", Long.valueOf(System.currentTimeMillis()));
                contentValues2.put("_data", file4.toString());
                getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, contentValues2);
                Toast.makeText(this, "Image saved", 0).show();
            } catch (IOException e2) {
                Toast.makeText(this, "An error occured. Please try again later.", 0).show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void sendLikePost(int i, int i2, int i3) {
        ApiService.m14297a().mo14121b(Integer.toString(i), Integer.toString(i2), Integer.toString(i3)).mo9256b(Schedulers.m16145io()).mo9258b((bjy<? super T>) new bjy<Post>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(Post post) {
                RxBus.m14573a().mo14349a(new C2663t(post));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void setFalseRumor(ImageView imageView, int i, TextView textView, boolean z) {
        if (z) {
            imageView.setColorFilter(Color.parseColor("#FF0000"));
            textView.setText("False " + Integer.toString(i + 1));
            return;
        }
        imageView.clearColorFilter();
        textView.setText("False " + Integer.toString(i - 1));
    }

    /* access modifiers changed from: protected */
    public void setTrueRumor(ImageView imageView, int i, TextView textView, boolean z) {
        if (z) {
            imageView.setColorFilter(Color.parseColor("#FF0000"));
            textView.setText("True " + Integer.toString(i + 1));
            return;
        }
        imageView.clearColorFilter();
        textView.setText("True " + Integer.toString(i - 1));
    }

    /* access modifiers changed from: protected */
    public void setupComment(Bundle bundle) {
        final int i = bundle.getInt("comment_id");
        this.commentContainer.setVisibility(8);
        this.shareContainer.setVisibility(8);
        this.replyContainer.setVisibility(8);
        this.likeContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                int i;
                int i2;
                BaseFullScreenActivity.this.disableLikes();
                int i3 = BaseFullScreenActivity.this.numLikes;
                int i4 = BaseFullScreenActivity.this.numDislikes;
                if (BaseFullScreenActivity.this.likeIcon.isSelected()) {
                    i = 0;
                    i2 = BaseFullScreenActivity.this.numLikes - 1;
                } else {
                    i = 1;
                    i2 = BaseFullScreenActivity.this.numLikes + 1;
                    if (BaseFullScreenActivity.this.dislikeIcon.isSelected()) {
                        i4 = BaseFullScreenActivity.this.numDislikes - 1;
                    }
                }
                final C2634aw awVar = new C2634aw(i, i, i2, i4);
                ApiService.m14297a().mo14096a(i, i).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                        Log.d("LikeComment", th.toString());
                        BaseFullScreenActivity.this.enableLikes();
                    }

                    public void onNext(EmptyClass emptyClass) {
                        RxBus.m14573a().mo14349a(awVar);
                        BaseFullScreenActivity.this.enableLikes();
                    }
                });
            }
        });
        this.dislikeContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                int i;
                int i2;
                BaseFullScreenActivity.this.disableLikes();
                int i3 = BaseFullScreenActivity.this.numLikes;
                int i4 = BaseFullScreenActivity.this.numDislikes;
                if (BaseFullScreenActivity.this.dislikeIcon.isSelected()) {
                    i = 0;
                    i2 = BaseFullScreenActivity.this.numDislikes - 1;
                } else {
                    i = -1;
                    i2 = BaseFullScreenActivity.this.numDislikes + 1;
                    if (BaseFullScreenActivity.this.likeIcon.isSelected()) {
                        i3 = BaseFullScreenActivity.this.numLikes - 1;
                    }
                }
                final C2634aw awVar = new C2634aw(i, i, i3, i2);
                ApiService.m14297a().mo14096a(i, i).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                        Log.d("LikeComment", th.toString());
                        BaseFullScreenActivity.this.enableLikes();
                    }

                    public void onNext(EmptyClass emptyClass) {
                        RxBus.m14573a().mo14349a(awVar);
                        BaseFullScreenActivity.this.enableLikes();
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public void setupFooterShared(Bundle bundle) {
        String string = bundle.getString("user_name");
        String string2 = bundle.getString("icon_name");
        String string3 = bundle.getString("icon_color");
        int i = 0;
        try {
            i = Color.parseColor(this.cropImage ? C2699jj.m14608a(string3, 0.3f) : C2699jj.m14608a(string3, 0.7f));
            if (i == 0) {
                i = Color.parseColor(string3);
            }
        } catch (IllegalArgumentException e) {
            Crashlytics.m16437a((Throwable) e);
            if (i == 0) {
                i = Color.parseColor(string3);
            }
        } catch (NullPointerException e2) {
            Crashlytics.m16437a((Throwable) e2);
            if (i == 0) {
                i = Color.parseColor(string3);
            }
        } catch (Throwable th) {
            if (i == 0) {
                int parseColor = Color.parseColor(string3);
            }
            throw th;
        }
        this.userName.setText(string);
        this.userName.setTextColor(i);
        TextView textView = this.userIcon;
        CandidAnimals.m14509a();
        textView.setTypeface(CandidAnimals.m14511b());
        this.userIcon.setText(CandidAnimals.m14510a(string2));
        this.userIcon.setTextColor(i);
        GradientDrawable gradientDrawable = new GradientDrawable();
        int width = this.userIcon.getWidth();
        if (width == 0) {
            width = (int) (this.userIcon.getResources().getDisplayMetrics().density * 30.0f);
        }
        gradientDrawable.setColor((16777215 & i) | 1073741824);
        gradientDrawable.setCornerRadius((float) (width / 2));
        this.userIcon.setBackground(gradientDrawable);
        this.numLikes = bundle.getInt("num_likes", 0);
        this.numDislikes = bundle.getInt("num_dislikes", 0);
        this.likeValue = bundle.getInt("like_value", 0);
        this.numTrue = bundle.getInt("num_true", 0);
        this.numFalse = bundle.getInt("num_false", 0);
        this.opinionValue = bundle.getInt("opinion_value", 0);
        if (bundle.getInt("is_rumor", 0) == 1) {
            this.likeIcon.setVisibility(8);
            this.dislikeIcon.setVisibility(8);
            this.trueIcon.setVisibility(0);
            this.falseIcon.setVisibility(0);
            int parseColor2 = Color.parseColor("#FF0000");
            if (this.opinionValue == 1) {
                this.trueIcon.setColorFilter(parseColor2);
            } else if (this.opinionValue == -1) {
                this.falseIcon.setColorFilter(parseColor2);
            }
            this.trueCount.setText("True " + Integer.toString(this.numTrue));
            this.falseCount.setText("False " + Integer.toString(this.numFalse));
            return;
        }
        if (this.likeValue == 1) {
            this.likeIcon.setSelected(true);
        } else if (this.likeValue == -1) {
            this.dislikeIcon.setSelected(true);
        }
        this.likeCount.setText(Integer.toString(this.numLikes));
        this.dislikeCount.setText(Integer.toString(this.numDislikes));
    }

    /* access modifiers changed from: protected */
    public void setupPost(final Bundle bundle) {
        final int i = bundle.getInt("post_id");
        int i2 = bundle.getInt("num_comments", 0);
        final int i3 = bundle.getInt("is_rumor", 0);
        if (i3 == 1) {
            this.likeIcon.setVisibility(8);
            this.dislikeIcon.setVisibility(8);
            this.trueIcon.setVisibility(0);
            this.falseIcon.setVisibility(0);
            this.commentContainer.setVisibility(8);
        } else {
            this.commentCount.setText(Integer.toString(i2));
            this.commentContainer.setVisibility(0);
        }
        this.shareInfoUrl = bundle.getString("share_info_url", null);
        this.shareInfoImg = bundle.getString("share_info_image", null);
        if (!(this.shareInfoImg == null || this.shareInfoUrl == null)) {
            this.shareContainer.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Context context = view.getContext();
                    if (AppState.config.getInt("enable_share_post", 0) == 1) {
                        PopupMenu popupMenu = new PopupMenu(context, view);
                        popupMenu.getMenuInflater().inflate(R.menu.share_menu, popupMenu.getMenu());
                        popupMenu.show();
                        popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                switch (menuItem.getItemId()) {
                                    case R.id.share_menu_facebook /*2131625156*/:
                                        ShareUtils.m14585b(BaseFullScreenActivity.this.mContext, BaseFullScreenActivity.this.shareInfoUrl, BaseFullScreenActivity.this.shareInfoImg);
                                        return true;
                                    case R.id.share_menu_twitter /*2131625157*/:
                                        ShareUtils.m14585b(BaseFullScreenActivity.this.mContext, BaseFullScreenActivity.this.shareInfoUrl, BaseFullScreenActivity.this.shareInfoImg);
                                        return true;
                                    case R.id.share_menu_text /*2131625158*/:
                                        Intent intent = new Intent(BaseFullScreenActivity.this.mContext, InviteContactsActivity.class);
                                        intent.putExtra("invite_type", InviteFlowTypes.POST.toString());
                                        BaseFullScreenActivity.this.mContext.startActivity(intent);
                                        return true;
                                    case R.id.share_menu_others /*2131625159*/:
                                        ShareUtils.m14582a(BaseFullScreenActivity.this.mContext, BaseFullScreenActivity.this.shareInfoUrl, "Share Link");
                                        return true;
                                    default:
                                        return false;
                                }
                            }
                        });
                        return;
                    }
                    Toast.makeText(context, "Sharing is not available", 0).show();
                }
            });
        }
        this.replyContainer.setVisibility(8);
        this.likeContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                int i;
                if (i3 == 1) {
                    if (BaseFullScreenActivity.this.opinionValue == 1) {
                        BaseFullScreenActivity.this.setTrueRumor(BaseFullScreenActivity.this.trueIcon, BaseFullScreenActivity.this.numTrue, BaseFullScreenActivity.this.trueCount, false);
                        BaseFullScreenActivity.this.opinionValue = 0;
                    } else {
                        if (BaseFullScreenActivity.this.opinionValue == -1) {
                            BaseFullScreenActivity.this.setFalseRumor(BaseFullScreenActivity.this.falseIcon, BaseFullScreenActivity.this.numFalse, BaseFullScreenActivity.this.falseCount, false);
                        }
                        BaseFullScreenActivity.this.setTrueRumor(BaseFullScreenActivity.this.trueIcon, BaseFullScreenActivity.this.numTrue, BaseFullScreenActivity.this.trueCount, true);
                        BaseFullScreenActivity.this.opinionValue = 1;
                    }
                    BaseFullScreenActivity.this.sendLikePost(i, BaseFullScreenActivity.this.opinionValue, 1);
                    return;
                }
                BaseFullScreenActivity.this.disableLikes();
                final UpdatePost updatePost = new UpdatePost();
                updatePost.post_id = i;
                updatePost.updates.num_likes = Integer.valueOf(BaseFullScreenActivity.this.numLikes);
                updatePost.updates.num_dislikes = Integer.valueOf(BaseFullScreenActivity.this.numDislikes);
                updatePost.updates.like_value = Integer.valueOf(BaseFullScreenActivity.this.likeValue);
                UpdatePost updatePost2 = new UpdatePost();
                updatePost2.post_id = i;
                if (BaseFullScreenActivity.this.likeIcon.isSelected()) {
                    i = 0;
                    updatePost2.updates.num_likes = Integer.valueOf(BaseFullScreenActivity.this.numLikes - 1);
                } else {
                    i = 1;
                    updatePost2.updates.num_likes = Integer.valueOf(BaseFullScreenActivity.this.numLikes + 1);
                    if (BaseFullScreenActivity.this.dislikeIcon.isSelected()) {
                        updatePost2.updates.num_dislikes = Integer.valueOf(BaseFullScreenActivity.this.numDislikes - 1);
                    }
                }
                updatePost2.updates.like_value = Integer.valueOf(i);
                RxBus.m14573a().mo14349a(new C2636ay(updatePost2));
                ApiService.m14297a().mo14121b(Integer.toString(i), Integer.toString(i), (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Post>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                        Log.d("LikePost", th.toString());
                        RxBus.m14573a().mo14349a(new C2636ay(updatePost));
                    }

                    public void onNext(Post post) {
                        BaseFullScreenActivity.this.enableLikes();
                    }
                });
            }
        });
        this.dislikeContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                int i;
                if (i3 == 1) {
                    if (BaseFullScreenActivity.this.opinionValue == -1) {
                        BaseFullScreenActivity.this.setFalseRumor(BaseFullScreenActivity.this.falseIcon, BaseFullScreenActivity.this.numFalse, BaseFullScreenActivity.this.falseCount, false);
                        BaseFullScreenActivity.this.opinionValue = 0;
                    } else {
                        if (BaseFullScreenActivity.this.opinionValue == 1) {
                            BaseFullScreenActivity.this.setTrueRumor(BaseFullScreenActivity.this.trueIcon, BaseFullScreenActivity.this.numTrue, BaseFullScreenActivity.this.trueCount, false);
                        }
                        BaseFullScreenActivity.this.setFalseRumor(BaseFullScreenActivity.this.falseIcon, BaseFullScreenActivity.this.numFalse, BaseFullScreenActivity.this.falseCount, true);
                        BaseFullScreenActivity.this.opinionValue = -1;
                    }
                    BaseFullScreenActivity.this.sendLikePost(i, BaseFullScreenActivity.this.opinionValue, 1);
                    return;
                }
                BaseFullScreenActivity.this.disableLikes();
                final UpdatePost updatePost = new UpdatePost();
                updatePost.post_id = i;
                updatePost.updates.num_likes = Integer.valueOf(BaseFullScreenActivity.this.numLikes);
                updatePost.updates.num_dislikes = Integer.valueOf(BaseFullScreenActivity.this.numDislikes);
                updatePost.updates.like_value = Integer.valueOf(BaseFullScreenActivity.this.likeValue);
                UpdatePost updatePost2 = new UpdatePost();
                updatePost2.post_id = i;
                if (BaseFullScreenActivity.this.dislikeIcon.isSelected()) {
                    i = 0;
                    updatePost2.updates.num_dislikes = Integer.valueOf(BaseFullScreenActivity.this.numDislikes - 1);
                } else {
                    i = -1;
                    updatePost2.updates.num_dislikes = Integer.valueOf(BaseFullScreenActivity.this.numDislikes + 1);
                    if (BaseFullScreenActivity.this.likeIcon.isSelected()) {
                        updatePost2.updates.num_likes = Integer.valueOf(BaseFullScreenActivity.this.numLikes - 1);
                    }
                }
                updatePost2.updates.like_value = Integer.valueOf(i);
                RxBus.m14573a().mo14349a(new C2636ay(updatePost2));
                ApiService.m14297a().mo14121b(Integer.toString(i), Integer.toString(i), (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Post>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                        Log.d("LikePost", th.toString());
                        RxBus.m14573a().mo14349a(new C2636ay(updatePost));
                    }

                    public void onNext(Post post) {
                        BaseFullScreenActivity.this.enableLikes();
                    }
                });
            }
        });
        this.commentContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (BaseFullScreenActivity.this.getIntent().hasExtra("fromDetails")) {
                    BaseFullScreenActivity.this.finish();
                    return;
                }
                PostDetailsActivity.startPostDetailsActivity(i, BaseFullScreenActivity.this, bundle.getString("icon_color"));
                BaseFullScreenActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void subscribeToUpdates(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case 3446944:
                if (str.equals("post")) {
                    c = 0;
                    break;
                }
                break;
            case 950398559:
                if (str.equals("comment")) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                addToSubscriptionList(RxBus.m14573a().mo14348a(C2636ay.class, (bjy<T>) new bjy<C2636ay>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                    }

                    public void onNext(C2636ay ayVar) {
                        BaseFullScreenActivity.this.updatePost(ayVar);
                    }
                }));
                return;
            case 1:
                addToSubscriptionList(RxBus.m14573a().mo14348a(C2634aw.class, (bjy<T>) new bjy<C2634aw>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                        Crashlytics.m16437a(th);
                    }

                    public void onNext(C2634aw awVar) {
                        BaseFullScreenActivity.this.updateComment(awVar);
                    }
                }));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void updateComment(C2634aw awVar) {
        this.numLikes = awVar.f10615c;
        this.numDislikes = awVar.f10616d;
        this.likeValue = awVar.f10614b;
        updateFullscreenView();
    }

    /* access modifiers changed from: protected */
    public void updateFullscreenView() {
        this.likeCount.setText(Integer.toString(this.numLikes));
        this.dislikeCount.setText(Integer.toString(this.numDislikes));
        if (this.likeValue == 1) {
            this.likeIcon.setSelected(true);
            this.dislikeIcon.setSelected(false);
        } else if (this.likeValue == -1) {
            this.likeIcon.setSelected(false);
            this.dislikeIcon.setSelected(true);
        } else {
            this.likeIcon.setSelected(false);
            this.dislikeIcon.setSelected(false);
        }
    }

    /* access modifiers changed from: protected */
    public void updatePost(C2636ay ayVar) {
        Updates updates = ayVar.f10617a.updates;
        this.numLikes = updates.num_likes != null ? updates.num_likes.intValue() : this.numLikes;
        this.numDislikes = updates.num_dislikes != null ? updates.num_dislikes.intValue() : this.numDislikes;
        this.likeValue = updates.like_value != null ? updates.like_value.intValue() : this.likeValue;
        if (updates.num_comments != null) {
            this.commentCount.setText(Integer.toString(updates.num_comments.intValue()));
        }
        updateFullscreenView();
    }

    /* access modifiers changed from: protected */
    public void updatePostRumorAlert(C2663t tVar) {
        this.numTrue = tVar.f10663e;
        this.numFalse = tVar.f10664f;
        updatePostRumorStats();
    }

    /* access modifiers changed from: protected */
    public void updatePostRumorStats() {
        this.trueCount.setText("True " + Integer.toString(this.numTrue));
        this.falseCount.setText("False " + Integer.toString(this.numFalse));
    }
}
