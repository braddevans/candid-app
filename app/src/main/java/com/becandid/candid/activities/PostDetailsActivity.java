package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.p001v4.widget.SwipeRefreshLayout;
import android.support.p001v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.p003v7.app.AlertDialog.Builder;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.ListPopupWindow;
import android.support.p003v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Comment;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.DataUtil.StringTrimResult;
import com.becandid.candid.data.EmptyClass;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.Post;
import com.becandid.candid.data.TextTag;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.models.UploadMediaResponse;
import com.becandid.candid.views.StickerKeyboard;
import com.becandid.candid.views.StickerKeyboard.C1848a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class PostDetailsActivity extends PopupWithBlurBackgroundActivity implements C1848a, C2465a {
    public static final int COMMENT_EDIT = 203;
    public static final int COMMENT_INPUT_TYPE_FLAGS = 212992;
    public static final int FULLSCREEN_VIEW_REPLY = 198;
    public static final int GIF = 307;
    public static final int MESSAGING = 205;
    public static final int MUTE_THREAD = 199;
    public static final int NUMBER_OF_COMMENTS_PER_PAGE = 20;
    public static int NUMBER_OF_VISIBLE_COMMENTS = 2;
    public static final int SUBSCRIBE_THREAD = 299;
    public static HashSet<Integer> clipped;
    /* access modifiers changed from: private */

    /* renamed from: A */
    public Toast f6572A;
    /* access modifiers changed from: private */

    /* renamed from: B */
    public Runnable f6573B = new Runnable() {
        public void run() {
            if (AppState.config.getInt("android_enable_comment_collapse", 1) == 1) {
                HashMap hashMap = new HashMap();
                hashMap.put("post_id", PostDetailsActivity.this.f6576d);
                if (PostDetailsActivity.this.f6577e != null) {
                    hashMap.put("op_color", PostDetailsActivity.this.f6577e);
                }
                ApiService.m14297a().mo14139d((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Post>() {
                    public void onCompleted() {
                    }

                    public void onError(Throwable th) {
                    }

                    public void onNext(Post post) {
                        List<Comment> list = post.comments;
                        ArrayList arrayList = new ArrayList();
                        for (Comment comment : list) {
                            comment.is_master_comment = true;
                            arrayList.add(comment);
                            for (Comment add : comment.reply_comments) {
                                arrayList.add(add);
                            }
                        }
                        list.clear();
                        list.addAll(arrayList);
                        for (int i = 0; i < list.size(); i++) {
                            Comment comment2 = (Comment) list.get(i);
                            int i2 = 0;
                            while (true) {
                                if (i2 >= PostDetailsActivity.this.f6591s.size()) {
                                    break;
                                } else if (((Comment) PostDetailsActivity.this.f6591s.get(i2)).equals(comment2)) {
                                    PostDetailsActivity.this.f6591s.set(i2, comment2);
                                    comment2.seeMoreComments = ((Comment) PostDetailsActivity.this.f6592t.mo13771e().get(i2)).seeMoreComments;
                                    PostDetailsActivity.this.f6592t.mo13771e().set(i2, comment2);
                                    PostDetailsActivity.this.f6592t.notifyItemChanged(i2 + 1);
                                    break;
                                } else {
                                    i2++;
                                }
                            }
                        }
                    }
                });
            } else {
                PostDetailsActivity.this.getRxSubscription();
            }
            PostDetailsActivity.this.f6598z.postDelayed(PostDetailsActivity.this.f6573B, 60000);
        }
    };

    /* renamed from: a */
    int f6574a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public String f6575b;
    @BindView(2131624150)
    ImageView commentButton;
    @BindView(2131624340)
    View commentGifSearchBtn;
    @BindView(2131624335)
    EditText commentInput;
    @BindView(2131624337)
    View commentInputButtonsHidden;
    @BindView(2131624336)
    View commentInputButtonsVisible;
    @BindView(2131624331)
    RelativeLayout commentInputContainer;
    @BindView(2131624338)
    ImageView commentPhotoButton;
    @BindView(2131624334)
    ImageView commentPhotoClear;
    @BindView(2131624286)
    ProgressBar commentPostBtnSpinny;
    @BindView(2131624327)
    RecyclerView commentRecyclerView;
    @BindView(2131624343)
    FrameLayout commentSpinnyContainer;
    @BindView(2131624341)
    TextView commentTextPlaceholder;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f6576d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f6577e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public boolean f6578f = false;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public String f6579g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public String f6580h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public String f6581i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public String f6582j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public ArrayList<TextTag> f6583k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public ListPopupWindow f6584l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public GroupAutocompleteAdapter f6585m;
    @BindView(2131624328)
    View mReplyToBox;
    @BindView(2131624330)
    View mReplyToClose;
    @BindView(2131624329)
    TextView mReplyToTextHint;
    @BindView(2131624223)
    SwipeRefreshLayout mSwipeContainer;
    @BindView(2131624325)
    ImageView mutePostIcon;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public CharSequence f6586n = "";
    /* access modifiers changed from: private */

    /* renamed from: o */
    public boolean f6587o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public int f6588p;
    @BindView(2131624324)
    TextView postHeaderText;
    @BindView(2131624333)
    ImageView postPhoto;
    @BindView(2131624344)
    RelativeLayout postSpinnyContainer;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public int f6589q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public Post f6590r;
    @BindView(2131624342)
    LinearLayout relatedPostsButton;
    @BindView(2131624322)
    View root;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public List<Comment> f6591s;
    @BindView(2131624339)
    ImageView stickerButton;
    @BindView(2131624290)
    StickerKeyboard stickerKeyboard;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public CommentsAdapter f6592t;

    /* renamed from: u */
    private Intent f6593u;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public String f6594v;

    /* renamed from: w */
    private boolean f6595w;

    /* renamed from: x */
    private final int f6596x = 60000;
    /* access modifiers changed from: private */

    /* renamed from: y */
    public int f6597y;
    /* access modifiers changed from: private */

    /* renamed from: z */
    public Handler f6598z;

    /* renamed from: a */
    private bjz m9237a(final String str, final boolean z, final int i, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("post_id", str);
        if (str2 != null) {
            hashMap.put("op_color", str2);
        }
        return ApiService.m14297a().mo14139d((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Post>() {
            public void onCompleted() {
                PostDetailsActivity.this.postSpinnyContainer.setVisibility(8);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (i != -1) {
                            PostDetailsActivity.this.scrollToComment(i);
                        } else if (z) {
                            PostDetailsActivity.this.scrollToBottom();
                        }
                    }
                }, 200);
                PostDetailsActivity.this.mSwipeContainer.setRefreshing(false);
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
                PostDetailsActivity.this.postSpinnyContainer.setVisibility(8);
                PostDetailsActivity.this.mSwipeContainer.setRefreshing(false);
                RxBus.m14573a().mo14349a(new C2619ah());
            }

            public void onNext(Post post) {
                if (post == null) {
                    RxBus.m14573a().mo14349a(new C2664u(Integer.parseInt(str)));
                    return;
                }
                PostDetailsActivity.this.f6590r = post;
                new ArrayList().add(PostDetailsActivity.this.f6590r);
                PostDetailsActivity.this.f6591s = post.comments != null ? post.comments : null;
                ArrayList arrayList = new ArrayList();
                for (Comment comment : PostDetailsActivity.this.f6591s) {
                    comment.is_master_comment = true;
                    arrayList.add(comment);
                    if (AppState.config.getInt("android_enable_comment_collapse", 1) != 1 || comment.reply_comments == null || comment.reply_comments.size() <= PostDetailsActivity.this.f6597y + 1) {
                        for (Comment comment2 : comment.reply_comments) {
                            comment2.is_master_comment = false;
                            arrayList.add(comment2);
                        }
                    } else {
                        List<Comment> list = comment.reply_comments;
                        List<Comment> subList = list.subList(0, list.size() - PostDetailsActivity.this.f6597y);
                        List<Comment> subList2 = list.subList(list.size() - PostDetailsActivity.this.f6597y, list.size());
                        comment.seeMoreComments = subList;
                        for (Comment add : subList2) {
                            arrayList.add(add);
                        }
                    }
                }
                PostDetailsActivity.this.f6591s.clear();
                PostDetailsActivity.this.f6591s.addAll(arrayList);
                ArrayList arrayList2 = new ArrayList();
                if (PostDetailsActivity.this.f6591s.size() <= 20 || PostDetailsActivity.this.f6592t.mo13771e().size() != 0 || z || i != -1) {
                    arrayList2.addAll(PostDetailsActivity.this.f6591s);
                    PostDetailsActivity.this.f6592t.mo13763a((String) null);
                } else {
                    arrayList2.addAll(PostDetailsActivity.this.f6591s.subList(0, 20));
                    PostDetailsActivity.this.f6592t.mo13763a("1");
                }
                PostDetailsActivity.this.f6592t.mo13774a((List<Comment>) arrayList2, PostDetailsActivity.this.f6590r);
                if (AppState.hasMuted) {
                    PostDetailsActivity.this.toggleMuteIcon();
                } else if (AppState.config.getInt("android_disable_post_subscription", 1) != 1) {
                    PostDetailsActivity.this.startActivity(new Intent(PostDetailsActivity.this, TutorialMuteActivity.class));
                } else if (post.can_mute == 1 && PostDetailsActivity.this.f6580h.equals("comment")) {
                    PostDetailsActivity.this.startActivityForResult(new Intent(PostDetailsActivity.this, TutorialMuteActivity.class), PostDetailsActivity.MUTE_THREAD);
                }
                PostDetailsActivity.this.toggleMuteIcon();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9244a() {
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder();
        if (this.f6594v != null) {
            sb.append(this.f6594v);
        }
        sb.append(this.commentInput.getText().toString());
        String sb2 = sb.toString();
        int length = sb2.length();
        StringTrimResult trimWithCount = DataUtil.trimWithCount(sb2);
        if (trimWithCount.string.length() > 0) {
            hashMap.put("comment_text", sb2);
            this.f6594v = null;
        }
        if (!this.f6583k.isEmpty()) {
            String str = "";
            Iterator it = this.f6583k.iterator();
            while (it.hasNext()) {
                TextTag textTag = (TextTag) it.next();
                int i = (length - 1) - textTag.endIndex;
                textTag.startIndex -= trimWithCount.trimmedStart;
                textTag.endIndex -= trimWithCount.trimmedStart;
                if (trimWithCount.trimmedEnd >= i) {
                    textTag.endIndex = trimWithCount.string.length();
                }
                str = str + textTag.toString() + ";";
            }
            hashMap.put("mentioned_group_ids", str.substring(0, str.length() - 1));
        }
        if (this.f6575b != null) {
            hashMap.put("source_url", this.f6575b);
            float f = 0.0f;
            float f2 = 0.0f;
            if (this.photoHelper != null) {
                f = (float) this.photoHelper.f10751d;
                f2 = (float) this.photoHelper.f10752e;
            }
            if (f <= 0.0f || f2 <= 0.0f) {
                hashMap.put("image_width", "240");
                hashMap.put("image_height", "160");
            } else if (((double) f) < (((double) f2) * 240.0d) / 160.0d) {
                hashMap.put("image_width", String.valueOf((int) ((160.0d * ((double) f)) / ((double) f2))));
                hashMap.put("image_height", "160");
            } else {
                hashMap.put("image_width", "240");
                hashMap.put("image_height", String.valueOf((int) ((240.0d * ((double) f2)) / ((double) f))));
            }
            if (this.photoHelper != null) {
                hashMap.put("from_camera", Integer.toString(this.photoHelper.f10754g));
            } else {
                hashMap.put("from_camera", Integer.toString(0));
            }
        }
        if (this.f6579g != null) {
            hashMap.put("sticker_name", this.f6579g);
            hashMap.put("image_width", "120");
            hashMap.put("image_height", "120");
        }
        if (this.f6581i != null) {
            hashMap.put("reply_to_comment_id", this.f6581i);
        }
        m9247a(hashMap);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9245a(int i) {
        this.mSubscription = m9237a(this.f6576d, false, i, this.f6577e);
        addToSubscriptionList(this.mSubscription);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9246a(final String str) {
        if (this.f6598z == null) {
            this.f6598z = new Handler(getMainLooper());
        }
        this.f6598z.post(new Runnable() {
            public void run() {
                if (PostDetailsActivity.this.f6572A != null) {
                    PostDetailsActivity.this.f6572A.cancel();
                }
                PostDetailsActivity.this.f6572A = Toast.makeText(PostDetailsActivity.this, str, 0);
                PostDetailsActivity.this.f6572A.show();
            }
        });
    }

    /* renamed from: a */
    private void m9247a(final HashMap<String, String> hashMap) {
        if (hashMap.containsKey("comment_text") || hashMap.containsKey("sticker_name") || hashMap.containsKey("source_url")) {
            String str = (String) hashMap.get("comment_text");
            if (str != null) {
                String[] split = str.split(" ");
                if (split[0].startsWith("@") && split.length == 1 && !hashMap.containsKey("sticker_name") && !hashMap.containsKey("source_url")) {
                    Toast.makeText(this, "Empty comments are not allowed", 0).show();
                    return;
                }
            }
            this.f6583k.clear();
            this.commentInput.setText("");
            chooseSticker(null);
            hashMap.put("post_id", this.f6576d);
            this.f6581i = null;
            ApiService.m14297a().mo14155h((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    PostDetailsActivity.this.mReplyToBox.setVisibility(8);
                    if (!PostDetailsActivity.this.isStopped) {
                        if (hashMap.containsKey("comment_text")) {
                            PostDetailsActivity.this.commentInput.setText((CharSequence) hashMap.get("comment_text"));
                        }
                        PostDetailsActivity.this.commentSpinnyContainer.setVisibility(8);
                    }
                }

                public void onNext(final NetworkData networkData) {
                    PostDetailsActivity.this.mReplyToBox.setVisibility(8);
                    if (!PostDetailsActivity.this.isStopped && networkData != null) {
                        if (!networkData.success) {
                            Toast.makeText(PostDetailsActivity.this, networkData.error, 0).show();
                            if (hashMap.containsKey("comment_text")) {
                                PostDetailsActivity.this.commentInput.setText((CharSequence) hashMap.get("comment_text"));
                            }
                            PostDetailsActivity.this.commentSpinnyContainer.setVisibility(8);
                            return;
                        }
                        if (networkData.comment != null) {
                            C2620ai aiVar = new C2620ai();
                            aiVar.f10586a = Integer.valueOf(PostDetailsActivity.this.f6576d).intValue();
                            aiVar.f10587b = networkData.comment.comment_id;
                            aiVar.f10588c = networkData.comment.comment_text;
                            aiVar.f10589d = networkData.comment;
                            aiVar.f10590e = (String) hashMap.get("reply_to_comment_id");
                            if (aiVar.f10590e != null) {
                                aiVar.f10589d.is_master_comment = false;
                            } else {
                                aiVar.f10589d.is_master_comment = true;
                            }
                            RxBus.m14573a().mo14349a(aiVar);
                            if (networkData.update_post != null) {
                                RxBus.m14573a().mo14349a(new C2636ay(networkData.update_post));
                            }
                            if (!(PostDetailsActivity.this.f6575b == null && PostDetailsActivity.this.f6579g == null)) {
                                PostDetailsActivity.this.clearPhoto(null);
                            }
                            PostDetailsActivity.this.closeKeyboard();
                            PostDetailsActivity.this.commentSpinnyContainer.setVisibility(8);
                        }
                        if (AppState.config.getInt("enable_related_posts", 1) != 1 || networkData.related_posts == null || networkData.related_posts.isEmpty()) {
                            PostDetailsActivity.this.relatedPostsButton.setVisibility(8);
                        } else if (AppState.config.getInt("enable_related_posts_pop_up", 0) == 1) {
                            PostDetailsActivity.this.relatedPostsButton.setVisibility(0);
                            PostDetailsActivity.this.relatedPostsButton.setOnClickListener(new OnClickListener() {
                                public void onClick(View view) {
                                    Intent intent = new Intent(view.getContext(), RelatedPostsFeedActivity.class);
                                    intent.putExtra("related_posts", DataUtil.toJson(networkData.related_posts));
                                    PostDetailsActivity.this.startActivity(intent);
                                    PostDetailsActivity.this.relatedPostsButton.setVisibility(8);
                                }
                            });
                        } else if (PostDetailsActivity.this.f6582j != null) {
                            PostDetailsActivity.this.relatedPostsButton.setVisibility(0);
                            PostDetailsActivity.this.relatedPostsButton.setOnClickListener(new OnClickListener() {
                                public void onClick(View view) {
                                    AppState.relatedPosts = networkData.related_posts;
                                    RxBus.m14573a().mo14349a(new C2624am(networkData.update_post.post_id, true, PostDetailsActivity.this.f6582j));
                                    PostDetailsActivity.this.finish();
                                }
                            });
                        }
                    }
                }
            });
            return;
        }
        Toast.makeText(this, "Empty comments are not allowed.", 0).show();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9252b(int i) {
        getRxSubscription();
    }

    public static void startPostDetailsActivity(int i, Context context, String str) {
        Intent intent = new Intent(context, PostDetailsActivity.class);
        intent.putExtra("post_id", i);
        intent.putExtra("op_color", str);
        context.startActivity(intent);
    }

    public void choosePhoto(View view) {
        clearPhoto(null);
        getPhotoHelper().choosePhoto();
    }

    public void chooseSticker(View view) {
        if (this.stickerKeyboard.getVisibility() == 0 || view == null) {
            this.stickerKeyboard.setVisibility(8);
            this.stickerButton.setImageResource(R.drawable.stickerbutton);
            this.stickerButton.setAlpha(0.5f);
            return;
        }
        if (this.keyboardOpen) {
            closeKeyboard();
        }
        this.stickerKeyboard.setVisibility(0);
        this.stickerButton.setImageResource(R.drawable.stickerbutton_active);
        this.stickerButton.setAlpha(1.0f);
    }

    public void clearPhoto(View view) {
        hideCommentInput();
        this.postPhoto.setImageBitmap(null);
        this.photoHelper = null;
        this.f6575b = null;
        this.postPhoto.setVisibility(8);
        this.f6579g = null;
        this.commentPhotoClear.setVisibility(8);
        if (!this.keyboardOpen) {
            keyboardClosed();
        }
        updatePostButton();
    }

    public void clickSticker(String str) {
        clearPhoto(null);
        this.f6579g = str;
        this.postPhoto.setVisibility(0);
        this.commentPhotoClear.setVisibility(0);
        this.postPhoto.setImageDrawable(GossipApplication.f6093a.getResources().getDrawable(GossipApplication.f6093a.getResources().getIdentifier(str.toLowerCase(), "drawable", GossipApplication.f6093a.getPackageName())));
        LayoutParams layoutParams = (LayoutParams) this.postPhoto.getLayoutParams();
        layoutParams.width = (int) (getResources().getDisplayMetrics().density * 120.0f);
        layoutParams.height = layoutParams.width;
        this.postPhoto.setLayoutParams(layoutParams);
        showCommentInput();
        updatePostButton();
    }

    /* access modifiers changed from: protected */
    public void getRxSubscription() {
        this.mSubscription = m9237a(this.f6576d, false, -1, this.f6577e);
        addToSubscriptionList(this.mSubscription);
    }

    public void hideCommentInput() {
        this.commentPhotoButton.setAlpha(1.0f);
        this.commentPhotoButton.setEnabled(true);
        this.stickerButton.setAlpha(1.0f);
        this.stickerButton.setEnabled(true);
        this.commentGifSearchBtn.setAlpha(1.0f);
        this.commentGifSearchBtn.setEnabled(true);
    }

    public void hideCurrentScreen() {
        this.root.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void imageCaptured() {
        this.photoHelper.mo14337b();
    }

    /* access modifiers changed from: protected */
    public void keyboardClosed() {
        super.keyboardClosed();
        String trim = this.commentInput.getText().toString().trim();
        if (this.f6575b != null || trim.length() == 0) {
        }
    }

    /* access modifiers changed from: protected */
    public void keyboardOpened() {
        super.keyboardOpened();
        chooseSticker(null);
        scrollToBottom();
    }

    public void mutePost() {
        ApiService.m14297a().mo14146f(Integer.toString(this.f6590r.post_id)).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(EmptyClass emptyClass) {
                PostDetailsActivity.this.f6590r.muted_post = 1;
                Builder builder = new Builder(PostDetailsActivity.this);
                if (AppState.hasShownMutePopup || AppState.config.getInt("android_disable_post_subscription", 1) != 1) {
                    PostDetailsActivity.this.m9246a("Muted");
                } else {
                    builder.setTitle((CharSequence) "Muted").setMessage((CharSequence) "You will no longer receive notifications about this post.").setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null);
                    builder.create().show();
                    AppState.hasShownMutePopup = true;
                    AppState.saveState(GossipApplication.m9012a());
                }
                PostDetailsActivity.this.toggleMuteIcon();
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        if (r8 == 902) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r8, int r9, android.content.Intent r10) {
        /*
            r7 = this;
            r5 = 1
            r6 = 0
            super.onActivityResult(r8, r9, r10)
            r7.f6593u = r10
            r4 = 199(0xc7, float:2.79E-43)
            if (r9 != r4) goto L_0x0013
            com.becandid.candid.data.Post r4 = r7.f6590r
            r4.can_mute = r5
            r7.mutePost()
        L_0x0012:
            return
        L_0x0013:
            r4 = 299(0x12b, float:4.19E-43)
            if (r9 != r4) goto L_0x001f
            com.becandid.candid.data.Post r4 = r7.f6590r
            r4.can_mute = r5
            r7.unmutePost()
            goto L_0x0012
        L_0x001f:
            jb r4 = r7.photoHelper
            r4 = 901(0x385, float:1.263E-42)
            if (r8 == r4) goto L_0x002b
            jb r4 = r7.photoHelper
            r4 = 902(0x386, float:1.264E-42)
            if (r8 != r4) goto L_0x0089
        L_0x002b:
            jb r4 = r7.photoHelper
            if (r4 == 0) goto L_0x0089
            jb r4 = r7.photoHelper
            android.graphics.Bitmap r4 = r4.f10753f
            if (r4 == 0) goto L_0x0089
            jb r4 = r7.photoHelper
            android.net.Uri r4 = r4.f10749b
            if (r4 == 0) goto L_0x0089
            jb r4 = r7.photoHelper
            java.lang.String r4 = r4.f10748a
            if (r4 == 0) goto L_0x0089
            android.widget.ImageView r4 = r7.postPhoto
            jb r5 = r7.photoHelper
            android.graphics.Bitmap r5 = r5.f10753f
            r4.setImageBitmap(r5)
            jb r4 = r7.photoHelper
            int r4 = r4.f10751d
            if (r4 <= 0) goto L_0x0075
            jb r4 = r7.photoHelper
            int r4 = r4.f10752e
            if (r4 <= 0) goto L_0x0075
            android.widget.ImageView r4 = r7.postPhoto
            android.view.ViewGroup$LayoutParams r3 = r4.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r3 = (android.widget.RelativeLayout.LayoutParams) r3
            jb r4 = r7.photoHelper
            int r4 = r4.f10751d
            float r4 = (float) r4
            jb r5 = r7.photoHelper
            int r5 = r5.f10752e
            float r5 = (float) r5
            float r4 = r4 / r5
            int r5 = r3.height
            float r5 = (float) r5
            float r4 = r4 * r5
            int r4 = (int) r4
            r3.width = r4
            android.widget.ImageView r4 = r7.postPhoto
            r4.setLayoutParams(r3)
        L_0x0075:
            android.widget.ImageView r4 = r7.postPhoto
            r4.setVisibility(r6)
            android.widget.ImageView r4 = r7.commentPhotoClear
            r4.setVisibility(r6)
            r7.showCommentInput()
            r7.imageCaptured()
            r7.updatePostButton()
            goto L_0x0012
        L_0x0089:
            r4 = 203(0xcb, float:2.84E-43)
            if (r9 != r4) goto L_0x0092
            java.lang.String r4 = ""
            r7.f6581i = r4
            goto L_0x0012
        L_0x0092:
            r4 = 205(0xcd, float:2.87E-43)
            if (r9 != r4) goto L_0x009a
            r7.f6595w = r5
            goto L_0x0012
        L_0x009a:
            r4 = 307(0x133, float:4.3E-43)
            if (r9 != r4) goto L_0x0012
            java.lang.String r4 = "gif_url"
            java.lang.String r1 = r10.getStringExtra(r4)
            java.lang.String r4 = "gif_width"
            int r2 = r10.getIntExtra(r4, r6)
            java.lang.String r4 = "gif_height"
            int r0 = r10.getIntExtra(r4, r6)
            android.widget.ImageView r4 = r7.postPhoto
            r4.setVisibility(r6)
            android.widget.ImageView r4 = r7.postPhoto
            android.view.ViewGroup$LayoutParams r3 = r4.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r3 = (android.widget.RelativeLayout.LayoutParams) r3
            int r4 = r3.height
            int r4 = r4 * 16
            int r4 = r4 / 9
            r3.width = r4
            android.widget.ImageView r4 = r7.postPhoto
            r4.setLayoutParams(r3)
            r7.showCommentInput()
            android.widget.ProgressBar r4 = r7.commentPostBtnSpinny
            r4.setVisibility(r6)
            android.widget.ImageView r4 = r7.commentButton
            r5 = 8
            r4.setVisibility(r5)
            le r4 = com.becandid.candid.GossipApplication.f6095c
            kx r4 = r4.mo14650a(r1)
            lb r4 = r4.mo14599k()
            la r4 = r4.mo14540a()
            com.bumptech.glide.Priority r5 = com.bumptech.glide.Priority.IMMEDIATE
            la r4 = r4.mo14558b(r5)
            r5 = 17301612(0x108006c, float:2.4979558E-38)
            la r4 = r4.mo14572d(r5)
            r5 = 2130903042(0x7f030002, float:1.741289E38)
            la r4 = r4.mo14569c(r5)
            la r4 = r4.mo14629c()
            com.bumptech.glide.load.engine.DiskCacheStrategy r5 = com.bumptech.glide.load.engine.DiskCacheStrategy.SOURCE
            la r4 = r4.mo14559b(r5)
            com.becandid.candid.activities.PostDetailsActivity$20 r5 = new com.becandid.candid.activities.PostDetailsActivity$20
            r5.<init>(r2, r0)
            la r4 = r4.mo14565b(r5)
            android.widget.ImageView r5 = r7.postPhoto
            r4.mo14554a(r5)
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.becandid.candid.activities.PostDetailsActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
            finish();
        } catch (IllegalStateException e) {
            Crashlytics.m16437a((Throwable) e);
            finish();
        }
    }

    public void onCommentExpanded(List<Comment> list) {
        if (list != null && !list.isEmpty()) {
            this.f6591s.clear();
            this.f6591s.addAll(list);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_post_details);
        ButterKnife.bind((Activity) this);
        this.f6597y = AppState.config.getInt("android_number_of_visible_comments", NUMBER_OF_VISIBLE_COMMENTS);
        clipped = new HashSet<>();
        this.f6580h = AppState.config.getString("show_mute_tutorial", "none");
        this.f6591s = new ArrayList();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        this.commentRecyclerView.setLayoutManager(linearLayoutManager);
        this.f6592t = new CommentsAdapter(this.f6591s, this.f6590r, this);
        this.f6592t.mo13772a((C2465a) this);
        this.commentRecyclerView.setAdapter(this.f6592t);
        this.commentRecyclerView.setItemAnimator(null);
        this.commentRecyclerView.addOnScrollListener(new PagingScrollListener(linearLayoutManager, this.f6592t) {
            public void onLoadMore(String str) {
                if (str != null) {
                    ArrayList arrayList = new ArrayList();
                    int size = PostDetailsActivity.this.f6592t.mo13771e().size();
                    if (PostDetailsActivity.this.f6591s.size() > size + 20) {
                        arrayList.addAll(PostDetailsActivity.this.f6591s.subList(size, size + 20));
                        PostDetailsActivity.this.f6592t.mo13763a("1");
                    } else {
                        if (size < PostDetailsActivity.this.f6591s.size()) {
                            arrayList.addAll(PostDetailsActivity.this.f6591s.subList(size, PostDetailsActivity.this.f6591s.size()));
                        }
                        PostDetailsActivity.this.f6592t.mo13763a((String) null);
                    }
                    PostDetailsActivity.this.f6592t.mo13777c((List<Comment>) arrayList);
                }
            }

            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                super.onScrollStateChanged(recyclerView, i);
                if (i != 0 && PostDetailsActivity.this.relatedPostsButton.getVisibility() == 0) {
                    AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                    alphaAnimation.setDuration(500);
                    alphaAnimation.setAnimationListener(new AnimationListener() {
                        public void onAnimationEnd(Animation animation) {
                            PostDetailsActivity.this.relatedPostsButton.setVisibility(8);
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }

                        public void onAnimationStart(Animation animation) {
                        }
                    });
                    PostDetailsActivity.this.relatedPostsButton.startAnimation(alphaAnimation);
                }
            }
        });
        this.f6574a = AppState.config.getInt("disable_group_tagging", 0);
        this.f6583k = new ArrayList<>();
        if (AppState.groups == null) {
            AppState.groups = new ArrayList();
        }
        this.f6585m = new GroupAutocompleteAdapter(this, R.layout.group_search_result, new ArrayList(AppState.groups));
        this.f6584l = new ListPopupWindow(this);
        this.f6584l.setAdapter(this.f6585m);
        this.f6584l.setAnchorView(this.commentInput);
        this.f6584l.setAnimationStyle(0);
        this.f6584l.setHeight(-2);
        this.f6584l.setWidth(C2699jj.m14599a(150, (Context) this));
        this.f6584l.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Group group = (Group) adapterView.getItemAtPosition(i);
                String[] split = PostDetailsActivity.this.commentInput.getText().toString().split("(?=#)");
                int selectionStart = PostDetailsActivity.this.commentInput.getSelectionStart();
                int i2 = 0;
                int i3 = 0;
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("");
                ArrayList arrayList = new ArrayList();
                int i4 = 0;
                int length = split.length;
                for (int i5 = 0; i5 < length; i5++) {
                    String str = split[i5];
                    int i6 = i2;
                    i2 += str.length();
                    if (i6 >= selectionStart || selectionStart > i2) {
                        spannableStringBuilder.append(str);
                    } else {
                        int length2 = spannableStringBuilder.length();
                        spannableStringBuilder.append("#" + group.group_name);
                        i4 = (group.group_name.length() + 1) - (selectionStart - length2);
                        i3 = spannableStringBuilder.length();
                        spannableStringBuilder.append(" ");
                        spannableStringBuilder.append(str.substring(selectionStart - i6));
                        TextTag textTag = new TextTag(length2, i3, group.group_id);
                        arrayList.add(textTag);
                    }
                }
                Iterator it = PostDetailsActivity.this.f6583k.iterator();
                while (it.hasNext()) {
                    TextTag textTag2 = (TextTag) it.next();
                    if (selectionStart < textTag2.startIndex || selectionStart > textTag2.endIndex) {
                        if (textTag2.startIndex > selectionStart) {
                            textTag2.startIndex += i4;
                            textTag2.endIndex += i4;
                        }
                        arrayList.add(textTag2);
                    }
                }
                PostDetailsActivity.this.f6583k = arrayList;
                Iterator it2 = PostDetailsActivity.this.f6583k.iterator();
                while (it2.hasNext()) {
                    TextTag textTag3 = (TextTag) it2.next();
                    if (textTag3.startIndex > -1 && textTag3.endIndex > -1) {
                        spannableStringBuilder.setSpan(C2699jj.m14606a(), textTag3.startIndex, textTag3.endIndex, 33);
                    }
                }
                PostDetailsActivity.this.f6586n = spannableStringBuilder;
                PostDetailsActivity.this.commentInput.setText(spannableStringBuilder);
                PostDetailsActivity.this.commentInput.setSelection(i3);
                PostDetailsActivity.this.commentInput.setSelection(i3 + 1);
                PostDetailsActivity.this.f6584l.dismiss();
            }
        });
        this.commentInput.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z && PostDetailsActivity.this.f6574a == 0) {
                    C2699jj.m14620a((CharSequence) PostDetailsActivity.this.commentInput.getText(), PostDetailsActivity.this.commentInput, PostDetailsActivity.this.f6584l, PostDetailsActivity.this.f6585m);
                }
            }
        });
        this.commentInput.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (PostDetailsActivity.this.f6574a == 0 && !editable.toString().equals(PostDetailsActivity.this.f6586n.toString())) {
                    PostDetailsActivity.this.f6586n = editable.toString();
                    C2699jj.m14620a((CharSequence) editable, PostDetailsActivity.this.commentInput, PostDetailsActivity.this.f6584l, PostDetailsActivity.this.f6585m);
                    if (PostDetailsActivity.this.f6587o) {
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(editable.toString());
                        Iterator it = PostDetailsActivity.this.f6583k.iterator();
                        while (it.hasNext()) {
                            TextTag textTag = (TextTag) it.next();
                            if (textTag.startIndex > -1 && textTag.endIndex > -1) {
                                spannableStringBuilder.setSpan(C2699jj.m14606a(), textTag.startIndex, textTag.endIndex, 33);
                            }
                        }
                        PostDetailsActivity.this.f6587o = false;
                        PostDetailsActivity.this.f6586n = spannableStringBuilder.toString();
                        int selectionStart = PostDetailsActivity.this.commentInput.getSelectionStart();
                        PostDetailsActivity.this.commentInput.setText(spannableStringBuilder);
                        PostDetailsActivity.this.commentInput.setSelection(selectionStart);
                    }
                }
                if (PostDetailsActivity.this.f6578f) {
                    PostDetailsActivity.this.f6578f = false;
                    PostDetailsActivity.this.commentInput.setInputType((PostDetailsActivity.this.commentInput.getInputType() & -8193) | 16384);
                }
                PostDetailsActivity.this.updatePostButton();
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                PostDetailsActivity.this.f6588p = PostDetailsActivity.this.commentInput.getSelectionStart();
                PostDetailsActivity.this.f6589q = PostDetailsActivity.this.commentInput.getSelectionEnd();
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (PostDetailsActivity.this.f6574a == 0 && !charSequence.toString().equals(PostDetailsActivity.this.f6586n.toString())) {
                    int selectionStart = PostDetailsActivity.this.commentInput.getSelectionStart();
                    ArrayList arrayList = new ArrayList();
                    Iterator it = PostDetailsActivity.this.f6583k.iterator();
                    while (it.hasNext()) {
                        TextTag textTag = (TextTag) it.next();
                        if (PostDetailsActivity.this.f6588p <= textTag.startIndex && PostDetailsActivity.this.f6589q <= textTag.startIndex) {
                            textTag.startIndex += i3 - i2;
                            textTag.endIndex += i3 - i2;
                            arrayList.add(textTag);
                            PostDetailsActivity.this.f6587o = true;
                        } else if (PostDetailsActivity.this.f6588p <= textTag.startIndex && PostDetailsActivity.this.f6589q > textTag.startIndex) {
                            PostDetailsActivity.this.f6587o = true;
                        } else if (PostDetailsActivity.this.f6588p > textTag.startIndex && PostDetailsActivity.this.f6588p <= textTag.endIndex) {
                            PostDetailsActivity.this.f6587o = true;
                        } else if (selectionStart > textTag.endIndex) {
                            arrayList.add(textTag);
                        } else {
                            PostDetailsActivity.this.f6587o = true;
                        }
                    }
                    PostDetailsActivity.this.f6583k = arrayList;
                }
            }
        });
        this.stickerKeyboard.f7413a = this;
        this.root.requestLayout();
        enableKeyboardEvents(this.root);
        busSubscribe(C2664u.class, new bjy<C2664u>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2664u uVar) {
                if (PostDetailsActivity.this.f6576d != null && uVar.f10667a == Integer.valueOf(PostDetailsActivity.this.f6576d).intValue()) {
                    Toast.makeText(PostDetailsActivity.this, "That post has been deleted", 0).show();
                    PostDetailsActivity.this.finish();
                }
            }
        });
        busSubscribe(C2620ai.class, new bjy<C2620ai>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2620ai aiVar) {
                if (aiVar.f10586a == Integer.valueOf(PostDetailsActivity.this.f6576d).intValue()) {
                    if (aiVar.f10590e != null) {
                        try {
                            PostDetailsActivity.this.m9245a(aiVar.f10587b);
                        } catch (NumberFormatException e) {
                            Crashlytics.m16437a((Throwable) e);
                        }
                    } else if (PostDetailsActivity.this.f6592t != null) {
                        PostDetailsActivity.this.m9245a(aiVar.f10587b);
                    } else {
                        Crashlytics.m16437a((Throwable) new NullPointerException("commentsAdapter is NULL"));
                        return;
                    }
                }
                if (PostDetailsActivity.this.f6590r == null) {
                    Crashlytics.m16437a((Throwable) new Exception("mPost is NULL"));
                    return;
                }
                PostDetailsActivity.this.f6590r.can_mute = 1;
                PostDetailsActivity.this.toggleMuteIcon();
                if (!AppState.hasMuted && PostDetailsActivity.this.f6580h.equals("comment")) {
                    PostDetailsActivity.this.startActivityForResult(new Intent(PostDetailsActivity.this, TutorialMuteActivity.class), PostDetailsActivity.MUTE_THREAD);
                }
            }
        });
        busSubscribe(C2658o.class, new bjy<C2658o>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2658o oVar) {
                PostDetailsActivity.this.m9252b(oVar.f10651a);
            }
        });
        busSubscribe(C2665v.class, new bjy<C2665v>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2665v vVar) {
                PostDetailsActivity.this.m9252b(vVar.f10668a);
            }
        });
        this.mSwipeContainer.setOnRefreshListener(new OnRefreshListener() {
            public void onRefresh() {
                PostDetailsActivity.this.getRxSubscription();
            }
        });
        this.postSpinnyContainer.setVisibility(0);
        this.mutePostIcon.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (AppState.hasMuted || !PostDetailsActivity.this.f6580h.equals("click")) {
                    PostDetailsActivity.this.mutePostIcon.setEnabled(false);
                    if (AppState.config.getInt("android_disable_post_subscription", 1) != 1 && PostDetailsActivity.this.f6590r.can_mute == 0) {
                        PostDetailsActivity.this.f6590r.can_mute = 1;
                        PostDetailsActivity.this.f6590r.muted_post = 1;
                    }
                    if (PostDetailsActivity.this.f6590r.muted_post == 0) {
                        PostDetailsActivity.this.mutePost();
                    } else if (PostDetailsActivity.this.f6590r.muted_post == 1) {
                        PostDetailsActivity.this.unmutePost();
                    }
                } else {
                    PostDetailsActivity.this.startActivityForResult(new Intent(PostDetailsActivity.this, TutorialMuteActivity.class), PostDetailsActivity.MUTE_THREAD);
                }
            }
        });
        this.postHeaderText.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PostDetailsActivity.this.scrollToTop();
            }
        });
        this.mReplyToClose.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PostDetailsActivity.this.mReplyToBox.setVisibility(8);
                PostDetailsActivity.this.f6581i = null;
                PostDetailsActivity.this.f6594v = null;
            }
        });
        this.f6598z = new Handler(getMainLooper());
        if (AppState.config.getInt("android_gif_search_enabled") == 1) {
            this.commentGifSearchBtn.setVisibility(0);
            this.commentGifSearchBtn.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    PostDetailsActivity.this.clearPhoto(null);
                    PostDetailsActivity.this.startActivityForResult(new Intent(PostDetailsActivity.this, GifSearchActivity.class), 307);
                }
            });
            return;
        }
        this.commentGifSearchBtn.setVisibility(8);
        this.commentGifSearchBtn.setOnClickListener(null);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.f6576d = Integer.toString(intent.getIntExtra("post_id", 0));
        if (intent.hasExtra("op_color")) {
            this.f6577e = intent.getStringExtra("op_color");
        }
        this.mSubscription = m9237a(this.f6576d, intent.getBooleanExtra("scrollToBottom", false), intent.getIntExtra("comment_id", -1), this.f6577e);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f6598z.removeCallbacks(this.f6573B);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f6593u != null) {
            if (this.f6593u.getIntExtra("comment_id", -1) != -1) {
            }
            this.f6593u = null;
        } else {
            this.f6576d = Integer.toString(getIntent().getIntExtra("post_id", 0));
            if (getIntent().hasExtra("op_color")) {
                this.f6577e = getIntent().getStringExtra("op_color");
            }
            boolean booleanExtra = getIntent().getBooleanExtra("scrollToBottom", false);
            int intExtra = getIntent().getIntExtra("comment_id", -1);
            this.f6582j = getIntent().getStringExtra("feed_type");
            String encodedId = DataUtil.getEncodedId(getIntent().getData());
            if (encodedId != null) {
                this.f6576d = String.valueOf(DataUtil.decodeId(encodedId));
            }
            this.mSubscription = m9237a(this.f6576d, booleanExtra, intExtra, this.f6577e);
        }
        this.f6598z.postDelayed(this.f6573B, 60000);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    public void replyTo(String str, String str2, int i) {
        this.mReplyToBox.setVisibility(0);
        this.mReplyToTextHint.setHint(C2699jj.m14601a("Replying to " + str));
        if (!str.startsWith("@")) {
            this.f6594v = "@" + str;
        }
        if (!str.endsWith(" ")) {
            this.f6594v += " ";
        }
        if (((Comment) this.f6591s.get(i - 1)).is_master_comment && AppState.config != null && AppState.config.getInt("android_disable_at_mention_for_main_comment", 1) == 1) {
            this.f6594v = "";
        }
        this.f6581i = str2;
        startComment(null);
        this.f6578f = true;
    }

    @OnClick({2131624150})
    public void saveComment(View view) {
        this.commentSpinnyContainer.setVisibility(0);
        if (this.photoHelper != null && this.photoHelper.f10749b != null && this.photoHelper.f10748a != null) {
            ApiService.m14297a().mo14129c(this.photoHelper.f10749b, this.photoHelper.f10748a).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<UploadMediaResponse>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Toast.makeText(PostDetailsActivity.this, PostDetailsActivity.this.getString(R.string.unable_to_upload_image), 0).show();
                    PostDetailsActivity.this.commentSpinnyContainer.setVisibility(8);
                }

                public void onNext(UploadMediaResponse uploadMediaResponse) {
                    if (uploadMediaResponse != null) {
                        PostDetailsActivity.this.f6575b = uploadMediaResponse.full_url;
                        PostDetailsActivity.this.m9244a();
                    }
                }
            });
        } else if (this.photoHelper == null || this.photoHelper.f10758k == null) {
            m9244a();
        } else {
            ApiService.m14297a().mo14112a(this.photoHelper.f10758k).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<UploadMediaResponse>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Toast.makeText(PostDetailsActivity.this, PostDetailsActivity.this.getString(R.string.unable_to_upload_image), 0).show();
                    PostDetailsActivity.this.commentSpinnyContainer.setVisibility(8);
                }

                public void onNext(UploadMediaResponse uploadMediaResponse) {
                    if (uploadMediaResponse != null) {
                        PostDetailsActivity.this.f6575b = uploadMediaResponse.full_url;
                        PostDetailsActivity.this.m9244a();
                    }
                }
            });
        }
    }

    public void scrollToBottom() {
        if (this.f6595w) {
            this.f6595w = false;
        } else if (this.f6581i == null) {
            if (this.f6592t.mo13771e().size() != this.f6591s.size()) {
                this.f6592t.mo13774a(this.f6591s, this.f6590r);
                this.f6592t.mo13763a((String) null);
            }
            this.commentRecyclerView.scrollToPosition(this.f6592t.mo13771e().size());
        }
    }

    public void scrollToComment(int i) {
        if (this.f6592t.mo13771e().size() != this.f6591s.size()) {
            this.f6592t.mo13774a(this.f6591s, this.f6590r);
            this.f6592t.mo13763a((String) null);
        }
        int b = this.f6592t.mo13775b(i);
        if (b != -1) {
            this.commentRecyclerView.scrollToPosition(b + 1);
        } else {
            scrollToHiddenComment(i);
        }
    }

    public void scrollToHiddenComment(int i) {
        int c = this.f6592t.mo13776c(i);
        if (c != -1) {
            Comment comment = (Comment) this.f6592t.mo13771e().get(c);
            List<Comment> list = comment.seeMoreComments;
            int size = list.size() - 1;
            while (size >= 0) {
                Comment comment2 = (Comment) list.remove(size);
                this.f6592t.mo13767b(comment2, c + 1);
                this.f6591s.add(c + 1, comment2);
                size--;
                if (comment2.comment_id == i) {
                    comment.seeMoreComments = list;
                    scrollToComment(i);
                    this.f6592t.notifyDataSetChanged();
                    return;
                }
            }
            return;
        }
        Toast.makeText(this, R.string.comment_deleted, 0).show();
    }

    public void scrollToTop() {
        if (this.commentRecyclerView != null) {
            this.commentRecyclerView.scrollToPosition(0);
        }
    }

    public void showCommentInput() {
        this.commentInputContainer.setVisibility(0);
        this.commentButton.setVisibility(0);
        this.commentPhotoButton.setAlpha(0.3f);
        this.commentPhotoButton.setEnabled(false);
        this.stickerButton.setAlpha(0.3f);
        this.stickerButton.setEnabled(false);
        this.commentGifSearchBtn.setAlpha(0.3f);
        this.commentGifSearchBtn.setEnabled(false);
    }

    public void showCurrentScreen() {
        this.root.setVisibility(0);
    }

    public void startComment(View view) {
        this.commentInputContainer.setVisibility(0);
        this.commentButton.setVisibility(0);
        this.commentInputButtonsVisible.setVisibility(8);
        this.commentInputButtonsHidden.setVisibility(0);
        this.commentInput.requestFocus();
        this.commentInput.post(new Runnable() {
            public void run() {
                PostDetailsActivity.this.openKeyboard();
            }
        });
    }

    public void toggleMuteIcon() {
        if (AppState.config.getInt("android_disable_post_subscription", 1) == 1) {
            this.mutePostIcon.setEnabled(false);
            if (this.f6590r.can_mute == 1) {
                this.mutePostIcon.setVisibility(0);
                if (this.f6590r.muted_post == 0) {
                    this.mutePostIcon.setImageDrawable(getResources().getDrawable(R.drawable.mute_icon_disabled));
                } else {
                    this.mutePostIcon.setImageDrawable(getResources().getDrawable(R.drawable.mute_icon_enabled));
                }
                this.mutePostIcon.setEnabled(true);
                return;
            }
            this.mutePostIcon.setVisibility(8);
            return;
        }
        this.mutePostIcon.setVisibility(0);
        this.mutePostIcon.setEnabled(true);
        if (this.f6590r.can_mute == 0) {
            this.mutePostIcon.setImageDrawable(getResources().getDrawable(R.drawable.mute_icon_disabled));
        } else if (this.f6590r.muted_post == 0) {
            this.mutePostIcon.setImageDrawable(getResources().getDrawable(R.drawable.mute_icon_disabled));
            this.mutePostIcon.setColorFilter(ContextCompat.getColor(this, R.color.gossip));
        } else {
            this.mutePostIcon.setImageDrawable(getResources().getDrawable(R.drawable.mute_icon_enabled));
        }
    }

    public void unmutePost() {
        ApiService.m14297a().mo14150g(Integer.toString(this.f6590r.post_id)).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(EmptyClass emptyClass) {
                PostDetailsActivity.this.f6590r.muted_post = 0;
                Builder builder = new Builder(PostDetailsActivity.this);
                if (AppState.hasShownUnmutePopup || AppState.config.getInt("android_disable_post_subscription", 1) != 1) {
                    PostDetailsActivity.this.m9246a("Subscribed");
                } else {
                    builder.setTitle((CharSequence) "Unmuted Post").setMessage((CharSequence) "You will receive notifications about this post again.").setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null);
                    builder.create().show();
                    AppState.hasShownUnmutePopup = true;
                    AppState.saveState(GossipApplication.m9012a());
                }
                PostDetailsActivity.this.toggleMuteIcon();
            }
        });
    }

    public void updatePostButton() {
        if (this.commentInput.getText().toString().trim().length() <= 0 && this.f6575b == null && this.f6579g == null && ((this.photoHelper == null || this.photoHelper.f10753f == null) && (this.photoHelper == null || this.photoHelper.f10758k == null))) {
            this.commentButton.setImageResource(R.drawable.comment_send_grey);
            this.commentButton.setClickable(false);
            return;
        }
        this.commentButton.setImageResource(R.drawable.comment_send_orange);
        this.commentButton.setClickable(true);
    }
}
