package com.becandid.candid.activities;

import android.os.Bundle;
import android.support.p003v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import com.becandid.candid.activities.GroupBaseSearchActivity.QueryTextInterface;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.gif.GifData;
import com.becandid.candid.models.gif.Media;
import java.util.ArrayList;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class GifSearchActivity extends GroupBaseSearchActivity {
    public static final int GIF_ROW = 2;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public List<Media> f6335b;

    /* renamed from: d */
    private bjz f6336d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public GifSearchAdapter f6337e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public GifType f6338f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public String f6339g;

    enum GifType {
        TRENDING,
        SEARCH
    }

    /* renamed from: a */
    private void m9108a() {
        this.f6335b.clear();
        this.mSearchResultRecyclerView.getAdapter().notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9111a(String str) {
        if (str.length() >= 1) {
            this.f6339g = str.toString();
            m9112a(this.f6339g, (String) null);
            return;
        }
        this.mSearchProgressBar.setVisibility(8);
        this.mSearchResultRecyclerView.setVisibility(0);
        m9108a();
        m9114b();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9112a(String str, String str2) {
        this.f6338f = GifType.SEARCH;
        if (str2 == null) {
            this.mSearchProgressBar.setVisibility(0);
            this.mSearchResultRecyclerView.setVisibility(8);
            m9108a();
        }
        if (this.f6336d != null) {
            this.f6336d.unsubscribe();
        }
        this.f6336d = GifApiService.m14497a().mo14293a(str, str2).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<GifData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                GifSearchActivity.this.mSearchProgressBar.setVisibility(8);
                GifSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
            }

            public void onNext(GifData gifData) {
                GifSearchActivity.this.mSearchProgressBar.setVisibility(8);
                GifSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
                if (gifData == null || gifData.results == null || gifData.results.isEmpty()) {
                    GifSearchActivity.this.f6337e.mo13763a((String) null);
                    return;
                }
                GifSearchActivity.this.f6335b.addAll(gifData.results);
                GifSearchActivity.this.f6337e.notifyDataSetChanged();
                GifSearchActivity.this.f6337e.mo13763a(gifData.next);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9114b() {
        this.f6335b.addAll(AppState.gifCacheList);
        this.f6337e.notifyDataSetChanged();
        this.f6337e.mo13763a(AppState.gifCacheNextPos);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9116b(final String str) {
        this.f6338f = GifType.TRENDING;
        if (str == null) {
            this.mSearchProgressBar.setVisibility(0);
            this.mSearchResultRecyclerView.setVisibility(8);
            m9108a();
        }
        if (this.f6336d != null) {
            this.f6336d.unsubscribe();
        }
        this.f6336d = GifApiService.m14497a().mo14292a(str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<GifData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                GifSearchActivity.this.mSearchProgressBar.setVisibility(8);
                GifSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
                if (str == null) {
                    GifSearchActivity.this.m9114b();
                }
            }

            public void onNext(GifData gifData) {
                GifSearchActivity.this.mSearchProgressBar.setVisibility(8);
                GifSearchActivity.this.mSearchResultRecyclerView.setVisibility(0);
                if (gifData == null || gifData.results == null || gifData.results.isEmpty()) {
                    GifSearchActivity.this.f6337e.mo13763a((String) null);
                    return;
                }
                if (str == null) {
                    AppState.gifCacheList.clear();
                    AppState.gifCacheList.addAll(gifData.results);
                    AppState.gifCacheNextPos = gifData.next;
                }
                GifSearchActivity.this.f6335b.addAll(gifData.results);
                GifSearchActivity.this.f6337e.notifyDataSetChanged();
                GifSearchActivity.this.f6337e.mo13763a(gifData.next);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        this.mSearchResultRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        this.f6335b = new ArrayList();
        this.f6337e = new GifSearchAdapter(this.f6335b, this);
        this.mSearchResultRecyclerView.setAdapter(this.f6337e);
        m9116b((String) null);
        this.mQueryTextInterface = new QueryTextInterface() {
            public void onQueryTextChange(String str) {
                GifSearchActivity.this.m9111a(str);
            }

            public void onQueryTextSubmit(String str) {
                GifSearchActivity.this.m9111a(str);
            }
        };
        this.mSearchResultRecyclerView.addOnScrollListener(new PagingScrollListener(staggeredGridLayoutManager, this.f6337e) {
            public void onLoadMore(String str) {
                if (str != null && GifSearchActivity.this.f6338f != null) {
                    if (GifSearchActivity.this.f6338f == GifType.TRENDING) {
                        GifSearchActivity.this.m9116b(str);
                    } else if (GifSearchActivity.this.f6338f == GifType.SEARCH) {
                        GifSearchActivity.this.m9112a(GifSearchActivity.this.f6339g, str);
                    }
                }
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean onCreateOptionsMenu = super.onCreateOptionsMenu(menu);
        this.mSearchView.setQueryHint("Search Tenor for GIFs");
        return onCreateOptionsMenu;
    }
}
