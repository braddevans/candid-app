package com.becandid.candid.activities;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.OnboardingGroupsActivity;
import com.becandid.candid.views.GroupStackView;

public class OnboardingGroupsActivity$$ViewBinder<T extends OnboardingGroupsActivity> implements ViewBinder<T> {

    /* compiled from: OnboardingGroupsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends OnboardingGroupsActivity> implements Unbinder {

        /* renamed from: a */
        private T f6563a;

        protected InnerUnbinder(T t) {
            this.f6563a = t;
        }

        public final void unbind() {
            if (this.f6563a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6563a);
            this.f6563a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.ogStack = null;
            t.ogButton = null;
            t.ogSpinny = null;
            t.ogEmpty = null;
            t.ogHeader = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.ogStack = (GroupStackView) finder.castView((View) finder.findRequiredView(obj, R.id.onboarding_groups_stack, "field 'ogStack'"), R.id.onboarding_groups_stack, "field 'ogStack'");
        t.ogButton = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.onboarding_groups_button, "field 'ogButton'"), R.id.onboarding_groups_button, "field 'ogButton'");
        t.ogSpinny = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.onboarding_groups_spinny, "field 'ogSpinny'"), R.id.onboarding_groups_spinny, "field 'ogSpinny'");
        t.ogEmpty = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.onboarding_groups_empty, "field 'ogEmpty'"), R.id.onboarding_groups_empty, "field 'ogEmpty'");
        t.ogHeader = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.onboarding_groups_header, "field 'ogHeader'"), R.id.onboarding_groups_header, "field 'ogHeader'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
