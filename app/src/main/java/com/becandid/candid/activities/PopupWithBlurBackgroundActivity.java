package com.becandid.candid.activities;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.views.BadgeInfoView;
import com.becandid.candid.views.CommunityPopupView;
import com.becandid.candid.views.MessagingPopupView;
import com.becandid.candid.views.ModeratorInfoView;
import com.becandid.candid.views.NicknamePopupView;
import com.becandid.candid.views.PostMovedView;
import com.becandid.candid.views.PostQualityFeedbackView;
import com.becandid.candid.views.QualityScoreInfoView;
import com.becandid.thirdparty.BlurTask.BadgeType;

public abstract class PopupWithBlurBackgroundActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public boolean f6564a = true;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f6565b = 500;
    protected boolean mIsShowingPopup;

    /* renamed from: com.becandid.candid.activities.PopupWithBlurBackgroundActivity$3 */
    static /* synthetic */ class C14903 {

        /* renamed from: a */
        static final /* synthetic */ int[] f6571a = new int[BadgeType.values().length];

        static {
            try {
                f6571a[BadgeType.BADGE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f6571a[BadgeType.MOD.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f6571a[BadgeType.POST_QUALITY_SCORE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f6571a[BadgeType.ME_QUALITY_SCORE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f6571a[BadgeType.MESSAGE_ENABLED_NEW.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f6571a[BadgeType.MESSAGE_ENABLED_TAB.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f6571a[BadgeType.MESSAGE_ADD_NICKNAME.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f6571a[BadgeType.MESSAGE_SECOND_NICKNAME.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f6571a[BadgeType.COMMUNITY_ONBOARDING.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f6571a[BadgeType.POST_QUALITY_FEEDBACK.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f6571a[BadgeType.POST_MOVED.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
        }
    }

    public abstract void hideCurrentScreen();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addToSubscriptionList(RxBus.m14573a().mo14348a(C2638b.class, (bjy<T>) new bjy<C2638b>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2638b bVar) {
                String str = bVar.f10621a;
                View view = null;
                switch (C14903.f6571a[bVar.f10622b.ordinal()]) {
                    case 1:
                        view = new BadgeInfoView(this, AppState.account.badge_status, true, str);
                        break;
                    case 2:
                        view = new ModeratorInfoView(this, str, bVar.f10624d, bVar.f10625e);
                        break;
                    case 3:
                        view = new QualityScoreInfoView(this, str, bVar.f10623c, bVar.f10624d, bVar.f10625e);
                        break;
                    case 4:
                        view = new QualityScoreInfoView(this, str, bVar.f10623c);
                        break;
                    case 5:
                        view = new MessagingPopupView(this, str, true);
                        PopupWithBlurBackgroundActivity.this.f6564a = false;
                        break;
                    case 6:
                        view = new MessagingPopupView(this, str, false);
                        PopupWithBlurBackgroundActivity.this.f6564a = false;
                        break;
                    case 7:
                        view = new NicknamePopupView(this, str, false);
                        PopupWithBlurBackgroundActivity.this.f6564a = false;
                        break;
                    case 8:
                        view = new NicknamePopupView(this, str, true);
                        PopupWithBlurBackgroundActivity.this.f6564a = false;
                        break;
                    case 9:
                        view = new CommunityPopupView(this, str, true);
                        PopupWithBlurBackgroundActivity.this.f6564a = false;
                        break;
                    case 10:
                        view = new PostQualityFeedbackView(this, str);
                        PopupWithBlurBackgroundActivity.this.f6564a = false;
                        break;
                    case 11:
                        view = new PostMovedView(this, str, bVar.f10624d);
                        PopupWithBlurBackgroundActivity.this.f6564a = false;
                        break;
                }
                PopupWithBlurBackgroundActivity.this.closeKeyboard();
                FrameLayout frameLayout = (FrameLayout) PopupWithBlurBackgroundActivity.this.findViewById(R.id.popup_info_container);
                frameLayout.setVisibility(0);
                frameLayout.addView(view);
                Animation loadAnimation = AnimationUtils.loadAnimation(PopupWithBlurBackgroundActivity.this.getApplicationContext(), R.anim.slide_up_animation);
                loadAnimation.setDuration((long) PopupWithBlurBackgroundActivity.this.f6565b);
                frameLayout.findViewById(R.id.badge_info).startAnimation(loadAnimation);
                frameLayout.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if (PopupWithBlurBackgroundActivity.this.f6564a) {
                            view.setOnClickListener(null);
                            PopupWithBlurBackgroundActivity.this.slideOutAnimation(view);
                        }
                    }
                });
                PopupWithBlurBackgroundActivity.this.hideCurrentScreen();
            }
        }));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AppState.blurTaskCalledOnFlight = false;
        if (!this.f6564a) {
            RxBus.m14573a().mo14349a(new C2650g());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        View findViewById = findViewById(R.id.popup_info_container);
        if (findViewById != null) {
            findViewById.setVisibility(8);
        }
        showCurrentScreen();
        if (!this.f6564a) {
            RxBus.m14573a().mo14349a(new C2650g());
        }
    }

    public abstract void showCurrentScreen();

    public void slideOutAnimation(final View view) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_down_animation);
        loadAnimation.setDuration((long) this.f6565b);
        View findViewById = ((FrameLayout) findViewById(R.id.popup_info_container)).findViewById(R.id.badge_info);
        if (findViewById != null) {
            findViewById.startAnimation(loadAnimation);
        }
        view.postDelayed(new Runnable() {
            public void run() {
                ((FrameLayout) PopupWithBlurBackgroundActivity.this.findViewById(R.id.popup_info_container)).removeAllViews();
                view.setVisibility(8);
                PopupWithBlurBackgroundActivity.this.showCurrentScreen();
                AppState.blurTaskCalledOnFlight = false;
            }
        }, (long) this.f6565b);
        this.mIsShowingPopup = false;
        if (!this.f6564a) {
            RxBus.m14573a().mo14349a(new C2650g());
        }
    }
}
