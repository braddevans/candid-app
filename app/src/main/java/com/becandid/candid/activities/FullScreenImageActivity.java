package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class FullScreenImageActivity extends BaseFullScreenActivity {

    /* renamed from: a */
    private String f6323a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f6324b;
    @BindView(2131624189)
    RelativeLayout container;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f6325d;
    @BindView(2131624190)
    ImageView fullscreenImage;
    @BindView(2131624218)
    LinearLayout fullscreenSpinny;

    public void onBackPressed() {
        setResult(PostDetailsActivity.FULLSCREEN_VIEW_REPLY, new Intent());
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_fullscreen_image);
        ButterKnife.bind((Activity) this);
        final Bundle extras = getIntent().getExtras();
        if (extras.containsKey("source_url")) {
            this.f6323a = extras.getString("source_url");
        } else {
            Toast.makeText(this, "Error finding image url", 1).show();
            finish();
        }
        if (extras.containsKey("post_id")) {
            setupPost(extras);
            subscribeToUpdates("post");
        } else if (extras.containsKey("comment_id")) {
            setupComment(extras);
            subscribeToUpdates("comment");
        } else {
            this.fullscreenGradient.setVisibility(8);
            this.fullscreenFooter.setVisibility(8);
            this.f6325d = true;
        }
        final int i = getResources().getDisplayMetrics().heightPixels;
        Glide.m15040a((FragmentActivity) this).mo14650a(this.f6323a).mo14592b().mo14559b(DiskCacheStrategy.SOURCE).mo14565b((RequestListener<? super ModelType, GlideDrawable>) new RequestListener<String, GlideDrawable>() {
            public boolean onException(Exception exc, String str, Target<GlideDrawable> sdVar, boolean z) {
                Crashlytics.m16437a((Throwable) exc);
                FullScreenImageActivity.this.fullscreenSpinny.setVisibility(8);
                FullScreenImageActivity.this.fullscreenImage.setVisibility(0);
                return false;
            }

            public boolean onResourceReady(GlideDrawable pmVar, String str, Target<GlideDrawable> sdVar, boolean z, boolean z2) {
                FullScreenImageActivity.this.fullscreenImage.setLayoutParams(new LayoutParams(-1, -1));
                LayoutParams layoutParams = new LayoutParams(-1, i / 5);
                layoutParams.addRule(12);
                FullScreenImageActivity.this.fullscreenGradient.setLayoutParams(layoutParams);
                FullScreenImageActivity.this.fullscreenSpinny.setVisibility(8);
                FullScreenImageActivity.this.fullscreenImage.setVisibility(0);
                if (!FullScreenImageActivity.this.f6325d) {
                    FullScreenImageActivity.this.setupFooterShared(extras);
                }
                FullScreenImageActivity.this.mGlideDrawable = pmVar;
                return false;
            }
        }).mo14554a(this.fullscreenImage);
        new bns(this.fullscreenImage).mo9524a((C1237d) new C1237d() {
            public void onOutsidePhotoTap() {
            }

            public void onPhotoTap(View view, float f, float f2) {
                if (FullScreenImageActivity.this.f6324b) {
                    FullScreenImageActivity.this.fadeIn();
                } else {
                    FullScreenImageActivity.this.fadeOut();
                }
                FullScreenImageActivity.this.f6324b = !FullScreenImageActivity.this.f6324b;
            }
        });
        this.fullscreenExit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FullScreenImageActivity.this.onBackPressed();
            }
        });
        addToSubscriptionList(RxBus.m14573a().mo14348a(C2663t.class, (bjy<T>) new bjy<C2663t>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2663t tVar) {
                FullScreenImageActivity.this.updatePostRumorAlert(tVar);
            }
        }));
        this.mContext = this;
    }
}
