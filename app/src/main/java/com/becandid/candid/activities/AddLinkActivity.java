package com.becandid.candid.activities;

import android.app.Activity;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.R;
import com.google.gson.internal.LinkedTreeMap;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class AddLinkActivity extends BaseActivity {

    /* renamed from: a */
    private String f6105a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Map<String, String> f6106b;
    @BindView(2131624074)
    TextView copiedLink;
    @BindView(2131624076)
    View spinny;
    @BindView(2131624072)
    EditText typedLink;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9018a() {
        if (this.f6106b != null) {
            Intent intent = new Intent();
            intent.putExtra("link_info", (LinkedTreeMap) this.f6106b);
            setResult(-1, intent);
            finish();
        }
    }

    /* renamed from: a */
    private void m9020a(String str) {
        if (str == null || !StringUtils.m14594a(str)) {
            Toast.makeText(this, "That does not appear to be a valid url", 1).show();
            return;
        }
        this.spinny.setVisibility(0);
        addToSubscriptionList(ApiService.m14297a().mo14142e(str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Map<String, String>>() {
            public void onCompleted() {
                AddLinkActivity.this.spinny.setVisibility(8);
                AddLinkActivity.this.m9018a();
            }

            public void onError(Throwable th) {
                th.printStackTrace();
                Toast.makeText(AddLinkActivity.this, "There was an error fetching this link", 1);
            }

            public void onNext(Map<String, String> map) {
                if (map != null) {
                    AddLinkActivity.this.f6106b = map;
                } else {
                    Toast.makeText(AddLinkActivity.this, "There was an error fetching this link", 1);
                }
            }
        }));
    }

    @OnClick({2131624075})
    public void chooseCopiedLink(View view) {
        if (this.f6105a != null) {
            m9020a(this.f6105a);
        } else {
            Toast.makeText(this, "The clipboard doesn't seem to have a link.", 1).show();
        }
    }

    @OnClick({2131624073})
    public void chooseTypedLink(View view) {
        m9020a(this.typedLink.getText().toString());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_add_link);
        ButterKnife.bind((Activity) this);
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService("clipboard");
        this.f6105a = null;
        if (clipboardManager.hasPrimaryClip() && clipboardManager.getPrimaryClip().getItemCount() > 0) {
            Item itemAt = clipboardManager.getPrimaryClip().getItemAt(0);
            if (clipboardManager.getPrimaryClipDescription().hasMimeType("text/plain")) {
                this.f6105a = itemAt.getText().toString();
            } else if (clipboardManager.getPrimaryClipDescription().hasMimeType("text/uri-list")) {
                this.f6105a = itemAt.getUri().toString();
            }
        }
        if (this.f6105a != null && this.f6105a.startsWith("http")) {
            this.copiedLink.setText(this.f6105a);
            this.copiedLink.setVisibility(0);
        }
        this.typedLink.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 6 && (keyEvent == null || keyEvent.getAction() != 1 || keyEvent.getKeyCode() != 66)) {
                    return false;
                }
                AddLinkActivity.this.chooseTypedLink(null);
                return true;
            }
        });
    }
}
