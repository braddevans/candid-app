package com.becandid.candid.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.p003v7.app.AlertDialog.Builder;
import android.support.p003v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity.InviteFlowTypes;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.Group;
import com.becandid.candid.fragments.main_tabs.GroupTabFragment;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.JoinedFacebookData;
import com.becandid.candid.models.NetworkData;
import com.facebook.AccessToken;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookException;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class BaseActivity extends AppCompatActivity implements CacheListener {
    public static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 1104;
    public static final int UPSELL_FEED = 1104;
    public static final int UPSELL_GROUP = 929;

    /* renamed from: c */
    View f6118c;
    public CallbackManager callbackManager;
    protected ArrayList<bjz> eventSubscriptions = new ArrayList<>();
    public boolean isStopped;
    public boolean keyboardOpen;
    protected bjz mSubscription;
    protected PhotoHelper photoHelper;
    protected VideoHelper videoHelper;

    /* renamed from: a */
    private boolean m9021a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    /* renamed from: b */
    private void m9022b() {
        showSnack(m9021a());
    }

    public void addToSubscriptionList(bjz bjz) {
        this.eventSubscriptions.add(bjz);
    }

    public void backNavClick(View view) {
        onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void busSubscribe(bjz bjz) {
        this.eventSubscriptions.add(bjz);
    }

    /* access modifiers changed from: protected */
    public <T> void busSubscribe(Class<T> cls, bjy<T> bjy) {
        this.eventSubscriptions.add(RxBus.m14573a().mo14348a(cls, bjy));
    }

    /* access modifiers changed from: protected */
    public void clearPhoto() {
        if (this.photoHelper != null) {
            this.photoHelper.clearPhoto();
        }
    }

    /* access modifiers changed from: protected */
    public void clearVideo() {
        if (this.videoHelper != null) {
            this.videoHelper.mo14418b();
        }
    }

    public void closeKeyboard() {
        closeKeyboard(this.f6118c);
    }

    public void closeKeyboard(View view) {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void connectFacebook(int i) {
        if (this.callbackManager == null) {
            this.callbackManager = C3115a.m17289a();
            abo.m502c().mo264a(this.callbackManager, (FacebookCallback<abp>) new FacebookCallback<abp>() {
                public void onCancel() {
                    Log.d("FBConnect", "cancel");
                }

                public void onError(FacebookException facebookException) {
                    Log.e("FBConnect", facebookException.toString());
                    if (!(facebookException instanceof FacebookAuthorizationException) || AccessToken.m10408a() == null) {
                        Toast.makeText(GossipApplication.m9012a(), "Unable to connect to Facebook", 1).show();
                        return;
                    }
                    abo.m502c().mo270d();
                    abo.m502c().mo261a(BaseActivity.this.getParent(), (Collection<String>) Arrays.asList("public_profile,user_friends,user_work_history,user_education_history".split(",")));
                }

                public void onSuccess(abp abp) {
                    Log.d("FBConnect", "success");
                    AppState.setFBInfo(abp.mo274a());
                    bjz a = RxBus.m14573a().mo14348a(C2668y.class, (bjy<T>) new bjy<C2668y>() {
                        public void onCompleted() {
                        }

                        public void onError(Throwable th) {
                            Crashlytics.m16437a(th);
                        }

                        public void onNext(C2668y yVar) {
                            BaseActivity.this.sendFbDataToServer();
                        }
                    });
                }
            });
        }
        abo.m502c().mo261a((Activity) this, (Collection<String>) Arrays.asList("public_profile,user_friends,user_work_history,user_education_history".split(",")));
    }

    public void enableKeyboardEvents(final View view) {
        this.f6118c = view;
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        final Point point = new Point();
        defaultDisplay.getSize(point);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                int height = view.getHeight();
                if (height < point.y - 100 && !BaseActivity.this.keyboardOpen) {
                    BaseActivity.this.keyboardOpen = true;
                    BaseActivity.this.keyboardOpened();
                } else if (height >= point.y - 100 && BaseActivity.this.keyboardOpen) {
                    BaseActivity.this.keyboardOpen = false;
                    BaseActivity.this.keyboardClosed();
                }
            }
        });
    }

    public PhotoHelper getPhotoHelper() {
        if (this.photoHelper == null) {
            this.photoHelper = new PhotoHelper(this);
        }
        return this.photoHelper;
    }

    /* access modifiers changed from: protected */
    public void getRxSubscription() {
        this.mSubscription = null;
    }

    public VideoHelper getVideoHelper() {
        if (this.videoHelper == null) {
            this.videoHelper = new VideoHelper(this);
        }
        return this.videoHelper;
    }

    public void inviteFriends(final int i, final int i2) {
        new Builder(this).setTitle((CharSequence) "Invite Candid Friends").setMessage((CharSequence) "Do you want to anonymously recommend this group to your friends on Candid?").setCancelable(true).setNegativeButton((CharSequence) getResources().getString(R.string.no), (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).setPositiveButton((CharSequence) getResources().getString(R.string.yes), (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ApiService.m14297a().mo14145f(i2).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                BaseActivity.this.upsellChange(i);
                Toast.makeText(BaseActivity.this.getBaseContext(), "Your friends have been notified. Thanks!", 1).show();
            }
        }).create().show();
    }

    /* access modifiers changed from: protected */
    public void keyboardClosed() {
    }

    /* access modifiers changed from: protected */
    public void keyboardOpened() {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (this.photoHelper != null) {
            this.photoHelper.mo14332a(i, i2, intent);
        }
        if (this.videoHelper != null) {
            this.videoHelper.mo14413a(i, i2, intent);
        }
    }

    public void onCacheAvailable(File file, String str, int i) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("click_notification")) {
            ApiService.m14297a().mo14162k(extras.getString("click_notification")).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        }
        busSubscribe(C2632au.class, new bjy<C2632au>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2632au auVar) {
                ShareUtils.m14581a().mo14357b();
            }
        });
        busSubscribe(C2619ah.class, new bjy<C2619ah>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2619ah ahVar) {
                Toast.makeText(GossipApplication.m9012a().getApplicationContext(), R.string.network_error, 0).show();
            }
        });
        busSubscribe(C2633av.class, new bjy<C2633av>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2633av avVar) {
                Toast.makeText(GossipApplication.m9012a().getApplicationContext(), R.string.unexpected_error, 0).show();
            }
        });
        busSubscribe(C2617af.class, new bjy<C2617af>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2617af afVar) {
                BaseActivity.this.showSnack(afVar.f10585a);
            }
        });
        m9022b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Iterator it = this.eventSubscriptions.iterator();
        while (it.hasNext()) {
            ((bjz) it.next()).unsubscribe();
        }
    }

    public void onNetworkConnectionChanged(boolean z) {
        showSnack(z);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey("click_notification")) {
            ApiService.m14297a().mo14162k(extras.getString("click_notification")).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        }
        setIntent(intent);
    }

    public void onPhotoChosen(String str, int i, int i2) {
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (this.photoHelper != null) {
            this.photoHelper.mo14333a(i, strArr, iArr);
        }
        if (this.videoHelper != null) {
            this.videoHelper.mo14414a(i, strArr, iArr);
        }
        if (i == 1989 && iArr.length > 0 && iArr[0] == 0) {
            RxBus.m14573a().mo14349a(new C2632au());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.isStopped = false;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.isStopped = false;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.isStopped = true;
        GossipApplication.m9013a(this).mo16742a((CacheListener) this);
    }

    public void openKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(1, 1);
    }

    public void sendFbDataToServer() {
        HashMap hashMap = new HashMap();
        hashMap.put("fb_token", AppState.fbInfo.fbToken.mo11466b());
        hashMap.put("fb_uid", AppState.fbInfo.fbToken.mo11476i());
        HashMap hashMap2 = new HashMap();
        hashMap2.put("type", "fb");
        hashMap2.put("fb_uid", AppState.fbInfo.fbToken.mo11476i());
        hashMap2.put("id_list", DataUtil.join(AppState.fbInfo.friendIds));
        bjs.m8432a(ApiService.m14297a().mo14124b((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()), ApiService.m14297a().mo14100a(AppState.fbInfo).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()), (bkk<? super T1, ? super T2, ? extends R>) new bkk<ays, NetworkData, JoinedFacebookData>() {
            public JoinedFacebookData call(ays ays, NetworkData networkData) {
                return new JoinedFacebookData(ays, networkData);
            }
        }).mo9258b((bjy<? super T>) new bjy<JoinedFacebookData>() {
            public void onCompleted() {
                RxBus.m14573a().mo14349a(new C2640bb());
            }

            public void onError(Throwable th) {
            }

            public void onNext(JoinedFacebookData joinedFacebookData) {
                NetworkData networkData = joinedFacebookData.autoJoin;
                if (!(networkData == null || networkData.groups == null)) {
                    for (Group pVar : networkData.groups) {
                        RxBus.m14573a().mo14349a(new C2659p(pVar));
                    }
                }
                if (networkData != null && networkData.my_info != null) {
                    AppState.account.num_groups = networkData.my_info.num_groups;
                }
            }
        });
    }

    public void setPhotoHelper(PhotoHelper jbVar) {
        this.photoHelper = jbVar;
    }

    public void setVideoHelper(VideoHelper jtVar) {
        this.videoHelper = jtVar;
    }

    public void showConnectAccount(int i) {
        Intent intent = new Intent(this, OnboardingActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_facebook_last_fragment", true);
        bundle.putString("first_fragment", "phone");
        bundle.putString("second_fragment", "fb");
        intent.putExtras(bundle);
        startActivityForResult(intent, 1104);
    }

    public void showContactsDialog(int i) {
        Intent intent = new Intent(this, InviteContactsActivity.class);
        intent.putExtra("invite_type", InviteFlowTypes.DOWNLOAD.toString());
        intent.putExtra("upsellId", i);
        startActivityForResult(intent, UPSELL_GROUP);
    }

    public void showContactsDialog(int i, int i2) {
        Intent intent = new Intent(this, InviteContactsActivity.class);
        intent.putExtra("invite_type", InviteFlowTypes.GROUP.toString());
        intent.putExtra("groupId", i2);
        intent.putExtra("upsellId", i);
        startActivityForResult(intent, UPSELL_GROUP);
    }

    public void showGroupsTab(String str) {
        if (this instanceof MainTabsActivity) {
            GroupTabFragment groupTabFragment = ((MainTabsActivity) this).getGroupTabFragment();
            groupTabFragment.mo10496a(str);
            groupTabFragment.mo10479a();
        }
    }

    public void showPhoneConnect(int i) {
        Intent intent = new Intent(this, OnboardingActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("first_fragment", "phone");
        bundle.putString("second_fragment", "phone");
        intent.putExtras(bundle);
        startActivityForResult(intent, 1104);
    }

    /* access modifiers changed from: protected */
    public void showSnack(boolean z) {
    }

    public void showWebView(String str) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("url", GossipApplication.f6096d + str);
        startActivity(intent);
    }

    public void skipUpsell(int i) {
        ApiService.m14297a().mo14154h(Integer.toString(i)).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        upsellChange(i);
    }

    public void skipUpsell(int i, int i2) {
        ApiService.m14297a().mo14154h(Integer.toString(i) + "," + Integer.toString(i2)).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
        upsellChange(i);
    }

    public void startPostWithGroupId(int i) {
        if (i > 0) {
            startActivity(CreatePostActivity.startCreatePostActivityIntent(this, i));
        }
    }

    public void updateCommunity() {
        startActivity(new Intent(this, CommunitySettingsActivity.class));
    }

    public void upsellChange(int i) {
    }
}
