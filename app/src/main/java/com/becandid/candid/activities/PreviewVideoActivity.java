package com.becandid.candid.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.devbrackets.android.exomedia.p006ui.widget.EMVideoView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PreviewVideoActivity extends BaseActivity {
    public static final String VIDEO_PATH = "recorded_video_path";
    public static final String VIDEO_URI = "selected_video_uri";

    /* renamed from: k */
    private static final int f6640k = AppState.config.getInt("android_video_max_upload_duration", 16);
    /* access modifiers changed from: private */

    /* renamed from: a */
    public EMVideoView f6641a;

    /* renamed from: b */
    private ImageView f6642b;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public ImageView f6643d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public ImageView f6644e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public ProgressBar f6645f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public String f6646g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Uri f6647h;

    /* renamed from: i */
    private int f6648i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public Bitmap f6649j;

    /* renamed from: l */
    private OnClickListener f6650l = new OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.videoAccept /*2131624386*/:
                    PreviewVideoActivity.this.m9280a();
                    return;
                case R.id.videoCancel /*2131624387*/:
                    Intent intent = new Intent(view.getContext(), AddVideoActivity.class);
                    intent.setFlags(33554432);
                    PreviewVideoActivity.this.startActivity(intent);
                    PreviewVideoActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: m */
    public AsyncTask<Uri, Void, Bitmap> f6651m = new AsyncTask<Uri, Void, Bitmap>() {
        /* access modifiers changed from: protected */
        public Bitmap doInBackground(Uri... uriArr) {
            if (!(uriArr == null || uriArr[0] == null)) {
                try {
                    Uri uri = uriArr[0];
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                    mediaMetadataRetriever.setDataSource(PreviewVideoActivity.this, uri);
                    Bitmap frameAtTime = mediaMetadataRetriever.getFrameAtTime(1000000, 2);
                    if (frameAtTime == null) {
                        return null;
                    }
                    if (frameAtTime.getWidth() >= frameAtTime.getHeight()) {
                        int width = (frameAtTime.getWidth() / 2) - (frameAtTime.getHeight() / 2);
                        return Bitmap.createBitmap(frameAtTime, width, 0, frameAtTime.getWidth() - width, frameAtTime.getHeight());
                    }
                    int height = (frameAtTime.getHeight() / 2) - (frameAtTime.getWidth() / 2);
                    return Bitmap.createBitmap(frameAtTime, 0, height, frameAtTime.getWidth(), frameAtTime.getHeight() - height);
                } catch (Exception e) {
                    Crashlytics.m16436a(e.toString());
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            if (!PreviewVideoActivity.this.isFinishing() && bitmap != null) {
                PreviewVideoActivity.this.f6649j = bitmap;
                PreviewVideoActivity.this.f6644e.setEnabled(true);
                PreviewVideoActivity.this.f6645f.setVisibility(8);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PreviewVideoActivity.this.f6645f.setVisibility(0);
        }
    };

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9280a() {
        if (this.f6641a.mo11331b()) {
            this.f6641a.mo11334d();
        }
        this.f6644e.setVisibility(8);
        Uri uri = this.f6647h;
        if (uri == null) {
            uri = Uri.fromFile(new File(this.f6646g));
        }
        Intent intent = new Intent();
        intent.putExtra("videoUri", uri.toString());
        intent.putExtra("uploaded", this.f6648i);
        if (this.f6649j != null) {
            String str = "CandidVideoCover.jpg";
            File file = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_PICTURES + File.separator + "Candid");
            file.mkdirs();
            if (file.canWrite()) {
                try {
                    File file2 = new File(file, str);
                    try {
                        if (file2.exists() && file2.delete()) {
                            file2.createNewFile();
                        }
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        this.f6649j.compress(CompressFormat.JPEG, 85, fileOutputStream);
                        fileOutputStream.close();
                        this.f6649j.recycle();
                        intent.putExtra("coverUri", Uri.fromFile(file2).toString());
                        setResult(-1, intent);
                        finish();
                        return;
                    } catch (IOException e) {
                        e = e;
                        File file3 = file2;
                        Crashlytics.m16437a((Throwable) e);
                        setResult(0);
                        finish();
                    }
                } catch (IOException e2) {
                    e = e2;
                    Crashlytics.m16437a((Throwable) e);
                    setResult(0);
                    finish();
                }
            }
        }
        setResult(0);
        finish();
    }

    /* renamed from: a */
    private boolean m9281a(Uri uri) {
        if (new File(uri.getPath()).length() > 100000000) {
            Toast.makeText(this, "Max video file size is " + Integer.toString(100) + " MBs.", 0).show();
            Intent intent = new Intent(this, AddVideoActivity.class);
            intent.setFlags(33554432);
            startActivity(intent);
            finish();
            return false;
        }
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setDataSource(uri.getPath());
            String extractMetadata = mediaMetadataRetriever.extractMetadata(9);
            if (extractMetadata == null) {
                return true;
            }
            long parseLong = Long.parseLong(extractMetadata);
            if (f6640k <= 0 || parseLong <= ((long) (f6640k * 1000))) {
                return true;
            }
            Toast.makeText(this, "Max video length is " + Integer.toString(f6640k - 1) + " seconds.", 0).show();
            Intent intent2 = new Intent(this, AddVideoActivity.class);
            intent2.setFlags(33554432);
            startActivity(intent2);
            finish();
            return false;
        } catch (Exception e) {
            Crashlytics.m16437a((Throwable) e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_video_preview);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        this.f6641a = (EMVideoView) findViewById(R.id.videoView);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.f6646g = extras.getString(VIDEO_PATH);
            if (this.f6646g != null) {
                this.f6641a.setVideoPath(this.f6646g);
                if (!m9281a(Uri.parse(this.f6646g))) {
                    return;
                }
            } else {
                this.f6647h = (Uri) extras.getParcelable(VIDEO_URI);
                if (this.f6647h != null) {
                    this.f6641a.setVideoURI(this.f6647h);
                }
                if (!m9281a(this.f6647h)) {
                    return;
                }
            }
            this.f6648i = extras.getInt("uploaded", 0);
        } else {
            Intent intent = new Intent(this, AddVideoActivity.class);
            finish();
            startActivity(intent);
        }
        this.f6641a.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared() {
                try {
                    PreviewVideoActivity.this.f6641a.setScaleType(ScaleType.CENTER_CROP);
                    PreviewVideoActivity.this.f6641a.mo11333c();
                    if (PreviewVideoActivity.this.f6649j == null) {
                        Uri c = PreviewVideoActivity.this.f6647h;
                        if (c == null && PreviewVideoActivity.this.f6646g != null) {
                            c = Uri.parse(PreviewVideoActivity.this.f6646g);
                        }
                        if (c != null) {
                            PreviewVideoActivity.this.f6651m.execute(new Uri[]{c});
                            return;
                        }
                        PreviewVideoActivity.this.f6643d.setVisibility(8);
                    }
                } catch (Exception e) {
                    PreviewVideoActivity.this.finish();
                }
            }
        });
        this.f6641a.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion() {
                PreviewVideoActivity.this.f6641a.mo11336f();
            }
        });
        this.f6642b = (ImageView) findViewById(R.id.videoCancel);
        this.f6643d = (ImageView) findViewById(R.id.videoCover);
        this.f6643d.setVisibility(8);
        this.f6644e = (ImageView) findViewById(R.id.videoAccept);
        this.f6644e.setEnabled(false);
        this.f6645f = (ProgressBar) findViewById(R.id.videoPreviewProgressBar);
        this.f6645f.setVisibility(8);
        this.f6642b.setOnClickListener(this.f6650l);
        this.f6644e.setOnClickListener(this.f6650l);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
