package com.becandid.candid.activities;

import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.WebViewActivity;

public class WebViewActivity$$ViewBinder<T extends WebViewActivity> implements ViewBinder<T> {

    /* compiled from: WebViewActivity$$ViewBinder */
    public static class InnerUnbinder<T extends WebViewActivity> implements Unbinder {

        /* renamed from: a */
        private T f6693a;

        protected InnerUnbinder(T t) {
            this.f6693a = t;
        }

        public final void unbind() {
            if (this.f6693a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6693a);
            this.f6693a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mWebView = null;
            t.mTitle = null;
            t.mProgressBar = null;
            t.mCopyLink = null;
            t.mSaveLink = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mWebView = (WebView) finder.castView((View) finder.findRequiredView(obj, R.id.webview, "field 'mWebView'"), R.id.webview, "field 'mWebView'");
        t.mTitle = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.webview_header_title, "field 'mTitle'"), R.id.webview_header_title, "field 'mTitle'");
        t.mProgressBar = (ProgressBar) finder.castView((View) finder.findRequiredView(obj, R.id.progress_bar, "field 'mProgressBar'"), R.id.progress_bar, "field 'mProgressBar'");
        t.mCopyLink = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.webview_copy_link, "field 'mCopyLink'"), R.id.webview_copy_link, "field 'mCopyLink'");
        t.mSaveLink = (View) finder.findRequiredView(obj, R.id.webview_save_link, "field 'mSaveLink'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
