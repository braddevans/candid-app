package com.becandid.candid.activities;

import android.os.Bundle;
import android.support.p001v4.app.FragmentManager;
import com.becandid.candid.R;
import com.becandid.candid.fragments.onboarding.OnboardingAgeFragment;

public class VerifyAgeActivity extends BaseActivity {
    public void onActivityResult() {
        setResult(-1);
        finish();
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        setTheme(R.style.DialogTheme);
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_verify_age_small);
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        OnboardingAgeFragment onboardingAgeFragment = new OnboardingAgeFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putBoolean("existing_user", true);
        onboardingAgeFragment.setArguments(bundle2);
        supportFragmentManager.beginTransaction().add(R.id.verify_age_container, onboardingAgeFragment, "age").commit();
    }
}
