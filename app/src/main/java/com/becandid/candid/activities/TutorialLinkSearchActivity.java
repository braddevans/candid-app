package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;

public class TutorialLinkSearchActivity extends BaseActivity {
    @BindView(2131624366)
    Button mContinue;

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_tutorial_link_search);
        ButterKnife.bind((Activity) this);
        this.mContinue.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AppState.hasLinkSearched = true;
                AppState.saveState(GossipApplication.m9012a());
                TutorialLinkSearchActivity.this.finish();
            }
        });
    }
}
