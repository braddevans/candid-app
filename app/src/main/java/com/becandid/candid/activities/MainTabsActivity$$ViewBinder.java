package com.becandid.candid.activities;

import android.support.design.widget.TabLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.MainTabsActivity;
import com.becandid.candid.views.MainTabViewPager;

public class MainTabsActivity$$ViewBinder<T extends MainTabsActivity> implements ViewBinder<T> {

    /* compiled from: MainTabsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends MainTabsActivity> implements Unbinder {

        /* renamed from: a */
        private T f6437a;

        protected InnerUnbinder(T t) {
            this.f6437a = t;
        }

        public final void unbind() {
            if (this.f6437a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6437a);
            this.f6437a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mViewPager = null;
            t.mBottomTabs = null;
            t.mCreatePostBtn = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mViewPager = (MainTabViewPager) finder.castView((View) finder.findRequiredView(obj, R.id.viewpager, "field 'mViewPager'"), R.id.viewpager, "field 'mViewPager'");
        t.mBottomTabs = (TabLayout) finder.castView((View) finder.findRequiredView(obj, R.id.bottom_tabs, "field 'mBottomTabs'"), R.id.bottom_tabs, "field 'mBottomTabs'");
        t.mCreatePostBtn = (View) finder.findRequiredView(obj, R.id.create_post_btn, "field 'mCreatePostBtn'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
