package com.becandid.candid.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import com.becandid.candid.R;
import com.becandid.candid.activities.FullScreenImageActivity;

public class FullScreenImageActivity$$ViewBinder<T extends FullScreenImageActivity> extends BaseFullScreenActivity$$ViewBinder<T> {

    /* compiled from: FullScreenImageActivity$$ViewBinder */
    public static class InnerUnbinder<T extends FullScreenImageActivity> extends com.becandid.candid.activities.BaseFullScreenActivity$$ViewBinder.InnerUnbinder<T> {
        protected InnerUnbinder(T t) {
            super(t);
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            super.unbind(t);
            t.container = null;
            t.fullscreenImage = null;
            t.fullscreenSpinny = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder innerUnbinder = (InnerUnbinder) super.bind(finder, t, obj);
        t.container = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_image, "field 'container'"), R.id.fullscreen_image, "field 'container'");
        t.fullscreenImage = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_imageview, "field 'fullscreenImage'"), R.id.fullscreen_imageview, "field 'fullscreenImage'");
        t.fullscreenSpinny = (LinearLayout) finder.castView((View) finder.findRequiredView(obj, R.id.fullscreen_spinny, "field 'fullscreenSpinny'"), R.id.fullscreen_spinny, "field 'fullscreenSpinny'");
        return innerUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
