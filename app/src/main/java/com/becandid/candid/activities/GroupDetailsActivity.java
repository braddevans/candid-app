package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.widget.SwipeRefreshLayout;
import android.support.p001v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.p003v7.app.AlertDialog;
import android.support.p003v7.app.AlertDialog.Builder;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.PopupMenu.OnMenuItemClickListener;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.RecyclerView.ItemAnimator;
import android.support.p003v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.activities.InviteContactsActivity.InviteFlowTypes;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.Post;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.util.RoundedCornersTransformation;
import com.becandid.candid.util.RoundedCornersTransformation.CornerType;
import com.becandid.candid.views.AutoplayFeedRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class GroupDetailsActivity extends PopupWithBlurBackgroundActivity implements OnMenuItemClickListener, C2481a {
    public static final int CREATE_POST_REQUEST = 888;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public int f6351a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Group f6352b;

    /* renamed from: d */
    private RoundedCornersTransformation f6353d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public FeedAdapter f6354e;
    @BindView(2131624229)
    TextView gdCompose;
    @BindView(2131624227)
    TextView gdHeaderName;
    @BindView(2131624230)
    TextView gdJoin;
    @BindView(2131624226)
    TextView groupEmpty;
    @BindView(2131624225)
    AutoplayFeedRecyclerView groupPosts;
    @BindView(2131624231)
    LinearLayout loadingSpinny;
    @BindView(2131624223)
    SwipeRefreshLayout mSwipeContainer;

    /* renamed from: a */
    private bjz m9121a() {
        return ApiService.m14297a().mo14097a(this.f6351a, (String) null).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
                GroupDetailsActivity.this.mSwipeContainer.setRefreshing(false);
            }

            public void onError(Throwable th) {
                GroupDetailsActivity.this.loadingSpinny.setVisibility(8);
                GroupDetailsActivity.this.groupPosts.setVisibility(0);
                Crashlytics.m16437a(th);
                GroupDetailsActivity.this.mSwipeContainer.setRefreshing(false);
                RxBus.m14573a().mo14349a(new C2619ah());
            }

            public void onNext(NetworkData networkData) {
                if (networkData != null) {
                    if (!networkData.success) {
                        String str = "Unable to find that group.";
                        if (networkData.error != null) {
                            str = networkData.error;
                        }
                        Toast.makeText(GroupDetailsActivity.this, str, 0).show();
                        GroupDetailsActivity.this.finish();
                        return;
                    }
                    GroupDetailsActivity.this.f6352b = networkData.group;
                    GroupDetailsActivity.this.gdHeaderName.setText(GroupDetailsActivity.this.f6352b.group_name);
                    if (networkData.posts != null) {
                        List<Post> list = networkData.posts;
                        GroupDetailsActivity.this.m9125a(list);
                        GroupDetailsActivity.this.f6354e.mo13761a(GroupDetailsActivity.this.f6352b.convertToPost());
                        if (!list.isEmpty()) {
                            GroupDetailsActivity.this.f6354e.mo13763a(Integer.toString(((Post) list.get(list.size() - 1)).post_id));
                        }
                    }
                }
                GroupDetailsActivity.this.loadingSpinny.setVisibility(8);
                GroupDetailsActivity.this.groupPosts.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9125a(List<Post> list) {
        this.f6354e.mo13764a(list);
        if (list.size() > 0) {
            this.groupEmpty.setVisibility(8);
        } else {
            this.groupEmpty.setVisibility(0);
        }
    }

    public static Intent startGroupDetailsActivity(Context context, int i) {
        Intent intent = new Intent(context, GroupDetailsActivity.class);
        intent.putExtra("group_id", i);
        return intent;
    }

    public void createPostClick(View view) {
        if (this.f6352b != null) {
            Intent intent = new Intent(this, CreatePostActivity.class);
            intent.putExtra("group_id", this.f6352b.group_id);
            intent.putExtra("group_name", this.f6352b.group_name);
            startActivityForResult(intent, 888);
        }
    }

    public void editCommunityClick(View view) {
        startActivity(new Intent(this, CommunitySettingsActivity.class));
    }

    /* access modifiers changed from: protected */
    public void getRxSubscription() {
        this.mSubscription = m9121a();
        addToSubscriptionList(this.mSubscription);
    }

    public void hideCurrentScreen() {
        findViewById(R.id.group_root).setVisibility(8);
    }

    public void joinGroupClick(View view) {
        if (this.f6352b != null) {
            ApiService.m14297a().mo14128c(this.f6352b.group_id).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Toast.makeText(GroupDetailsActivity.this, "Unable to join this group", 0).show();
                }

                public void onNext(NetworkData networkData) {
                    Toast.makeText(GroupDetailsActivity.this, "You have joined this group", 0).show();
                    GroupDetailsActivity.this.gdCompose.setVisibility(0);
                    GroupDetailsActivity.this.f6354e.notifyItemChanged(0);
                }
            });
        }
    }

    public void leaveGroupClick(View view) {
        if (this.f6352b == null) {
            return;
        }
        if (this.f6352b.community_membership == 1) {
            startActivity(new Intent(view.getContext(), CommunitySettingsActivity.class));
        } else {
            ApiService.m14297a().mo14136d(this.f6352b.group_id).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                    GroupDetailsActivity.this.finish();
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Log.d("GroupDetailsActivity", th.toString());
                }

                public void onNext(NetworkData networkData) {
                    int i = 0;
                    while (true) {
                        if (i >= AppState.groups.size()) {
                            break;
                        } else if (((Group) AppState.groups.get(i)).group_id == GroupDetailsActivity.this.f6352b.group_id) {
                            AppState.groups.remove(i);
                            break;
                        } else {
                            i++;
                        }
                    }
                    RxBus.m14573a().mo14349a(new C2661r(GroupDetailsActivity.this.f6352b.group_id));
                    GroupDetailsActivity.this.gdCompose.setVisibility(8);
                    Toast.makeText(GroupDetailsActivity.this, "You have left this group", 0).show();
                }
            });
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 888) {
            if (i2 != -1) {
                Log.e("GroupDetails", "Error on activity result");
            }
        } else if (i == 929 && i2 == -1 && intent != null) {
            upsellChange(intent.getIntExtra("upsellId", -1));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_group_details);
        ButterKnife.bind((Activity) this);
        this.groupPosts.setVisibility(8);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        this.groupPosts.setLayoutManager(linearLayoutManager);
        this.f6354e = new FeedAdapter(this, null);
        this.f6354e.mo13820a((C2481a) this);
        this.groupPosts.setAdapter(this.f6354e);
        this.loadingSpinny.setVisibility(0);
        this.groupPosts.setupFeedRecyclerView(this.f6354e, linearLayoutManager);
        ItemAnimator itemAnimator = this.groupPosts.getItemAnimator();
        if (itemAnimator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        this.f6353d = new RoundedCornersTransformation((Context) this, (int) (40.0f * getResources().getDisplayMetrics().density), 0, CornerType.ALL);
        this.f6351a = getIntent().getIntExtra("group_id", 0);
        String encodedId = DataUtil.getEncodedId(getIntent().getData());
        if (encodedId != null) {
            this.f6351a = (int) DataUtil.decodeId(encodedId);
        }
        if (AppState.isGroupMember(this.f6351a)) {
            this.gdCompose.setVisibility(0);
        } else {
            this.gdCompose.setVisibility(8);
        }
        if (getIntent().hasExtra("group_name")) {
            this.gdHeaderName.setText(getIntent().getStringExtra("group_name"));
        }
        if (getIntent().hasExtra("group_image")) {
        }
        if (getIntent().hasExtra("group_json")) {
            this.f6352b = (Group) DataUtil.gson.mo8357a(getIntent().getStringExtra("group_json"), Group.class);
            if (this.f6352b.group_name != null) {
                this.gdHeaderName.setText(this.f6352b.group_name);
            }
        }
        getRxSubscription();
        this.mSwipeContainer.setOnRefreshListener(new OnRefreshListener() {
            public void onRefresh() {
                GroupDetailsActivity.this.getRxSubscription();
            }
        });
        busSubscribe(C2657n.class, new bjy<C2657n>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2657n nVar) {
                if (nVar.f10650a.group_id == GroupDetailsActivity.this.f6351a) {
                    GroupDetailsActivity.this.f6354e.mo13767b(nVar.f10650a, 1);
                    GroupDetailsActivity.this.groupPosts.smoothScrollToPosition(1);
                    GroupDetailsActivity.this.groupEmpty.setVisibility(8);
                    Group a = GroupDetailsActivity.this.f6352b;
                    a.num_posts++;
                } else {
                    GroupDetailsActivity.this.f6351a = nVar.f10650a.group_id;
                }
                GroupDetailsActivity.this.getRxSubscription();
            }
        });
        busSubscribe(C2666w.class, new bjy<C2666w>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2666w wVar) {
                if (wVar.f10670a.group_id == GroupDetailsActivity.this.f6351a) {
                    GroupDetailsActivity.this.f6352b = wVar.f10670a;
                    GroupDetailsActivity.this.f6354e.mo13762a(GroupDetailsActivity.this.f6352b.convertToPost(), 0);
                    GroupDetailsActivity.this.getRxSubscription();
                }
            }
        });
        busSubscribe(C2664u.class, new bjy<C2664u>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2664u uVar) {
                GroupDetailsActivity.this.f6354e.mo13822b(uVar.f10667a);
            }
        });
    }

    public void onErrorRetry(String str) {
        onLoadMore(str);
    }

    public void onLoadMore(String str) {
        if (str != null && Integer.valueOf(str).intValue() > 0) {
            ApiService.m14297a().mo14097a(this.f6351a, str).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    C2699jj.m14614a((RecyclerView) GroupDetailsActivity.this.groupPosts, GroupDetailsActivity.this.f6354e);
                }

                public void onNext(NetworkData networkData) {
                    if (networkData.posts == null || networkData.posts.isEmpty()) {
                        GroupDetailsActivity.this.f6354e.mo13763a((String) null);
                        return;
                    }
                    List<Post> list = networkData.posts;
                    GroupDetailsActivity.this.f6354e.mo13768b(list);
                    if (!list.isEmpty()) {
                        GroupDetailsActivity.this.f6354e.mo13763a(Integer.toString(((Post) list.get(list.size() - 1)).post_id));
                    }
                }
            });
        }
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        Log.d("GDA", "groupMenu");
        switch (menuItem.getItemId()) {
            case R.id.group_menu_leave /*2131625127*/:
                leaveGroupClick(null);
                break;
            case R.id.group_menu_community /*2131625128*/:
                editCommunityClick(null);
                break;
            case R.id.group_menu_join /*2131625129*/:
                joinGroupClick(null);
                break;
            case R.id.group_menu_delete /*2131625130*/:
                new Builder(this).setIconAttribute(16843605).setTitle((CharSequence) "Delete this group?").setMessage((CharSequence) "Are you sure you want to permanently delete this group?").setPositiveButton((CharSequence) "Yes", (OnClickListener) new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new HashMap().put("group_id", Integer.toString(GroupDetailsActivity.this.f6352b.group_id));
                        ApiService.m14297a().mo14149g(GroupDetailsActivity.this.f6351a).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                            public void onCompleted() {
                            }

                            public void onError(Throwable th) {
                                Crashlytics.m16437a(th);
                                Log.d("DeleteGroup", th.toString());
                            }

                            public void onNext(NetworkData networkData) {
                                RxBus.m14573a().mo14349a(new C2661r(GroupDetailsActivity.this.f6352b.group_id));
                                GroupDetailsActivity.this.finish();
                            }
                        });
                    }
                }).setNegativeButton((CharSequence) "No", (OnClickListener) null).show();
                break;
            case R.id.group_menu_report /*2131625131*/:
                String[] split = AppState.config.getString("report_group_reasons", "nsfw:Pornographic,spam:Solicitation and Spam,hate:Hate Speech,offtopic:Off Topic,illegal:Illegal Content").split(",");
                String[] strArr = new String[split.length];
                final String[] strArr2 = new String[split.length];
                for (int i = 0; i < split.length; i++) {
                    strArr2[i] = split[i].split(":")[0];
                    strArr[i] = split[i].split(":")[1];
                }
                new Builder(this).setTitle((CharSequence) "Why Are You Reporting This Group?").setSingleChoiceItems((CharSequence[]) strArr, 0, (OnClickListener) null).setPositiveButton((CharSequence) "Report Group", (OnClickListener) new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ApiService.m14297a().mo14117b(GroupDetailsActivity.this.f6351a, strArr2[((AlertDialog) dialogInterface).getListView().getCheckedItemPosition()]).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                            public void onCompleted() {
                            }

                            public void onError(Throwable th) {
                                Crashlytics.m16437a(th);
                                Log.d("ReportGroup", th.toString());
                            }

                            public void onNext(NetworkData networkData) {
                                new Builder(GroupDetailsActivity.this).setTitle((CharSequence) "Group Reported").setMessage((CharSequence) "This group has been reported.").setPositiveButton((CharSequence) "OK", (OnClickListener) null).create().show();
                            }
                        });
                    }
                }).setNegativeButton((CharSequence) "Cancel", (OnClickListener) null).create().show();
                break;
            case R.id.group_menu_edit /*2131625132*/:
                Intent intent = new Intent(this, CreateGroupActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("edit_group", true);
                bundle.putInt("group_id", this.f6352b.group_id);
                bundle.putString("group_name", this.f6352b.group_name);
                bundle.putString("group_info", this.f6352b.about);
                bundle.putString("source_url", this.f6352b.source_url);
                bundle.putStringArrayList("tags", (ArrayList) this.f6352b.tags);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.loadingSpinny.setVisibility(0);
        this.groupPosts.setVisibility(8);
        this.f6351a = intent.getIntExtra("group_id", 0);
        m9121a();
    }

    public void showContactsDialog(int i, int i2) {
        Intent intent = new Intent(this, InviteContactsActivity.class);
        intent.putExtra("invite_type", InviteFlowTypes.GROUP.toString());
        intent.putExtra("upsell_id", String.valueOf(i) + "," + String.valueOf(i2));
        intent.putExtra("groupId", i2);
        intent.putExtra("upsellId", i);
        startActivityForResult(intent, BaseActivity.UPSELL_GROUP);
    }

    public void showCurrentScreen() {
        findViewById(R.id.group_root).setVisibility(0);
    }

    public void upsellChange(int i) {
        this.f6354e.mo13822b(i);
    }
}
