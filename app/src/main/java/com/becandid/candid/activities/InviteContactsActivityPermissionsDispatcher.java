package com.becandid.candid.activities;

import android.content.Context;
import android.support.p001v4.app.ActivityCompat;
import android.view.View;
import java.lang.ref.WeakReference;

final class InviteContactsActivityPermissionsDispatcher {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final String[] f6401a = {"android.permission.READ_CONTACTS"};

    /* renamed from: b */
    private static bjo f6402b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final String[] f6403c = {"android.permission.READ_CONTACTS"};

    /* renamed from: d */
    private static bjo f6404d;

    static final class ChooseContactsPermissionRequest implements bjo {

        /* renamed from: a */
        private final WeakReference<InviteContactsActivity> f6405a;

        /* renamed from: b */
        private final View f6406b;

        private ChooseContactsPermissionRequest(InviteContactsActivity inviteContactsActivity, View view) {
            this.f6405a = new WeakReference<>(inviteContactsActivity);
            this.f6406b = view;
        }

        public void cancel() {
        }

        public void grant() {
            InviteContactsActivity inviteContactsActivity = (InviteContactsActivity) this.f6405a.get();
            if (inviteContactsActivity != null) {
                inviteContactsActivity.chooseContacts(this.f6406b);
            }
        }

        public void proceed() {
            InviteContactsActivity inviteContactsActivity = (InviteContactsActivity) this.f6405a.get();
            if (inviteContactsActivity != null) {
                ActivityCompat.requestPermissions(inviteContactsActivity, InviteContactsActivityPermissionsDispatcher.f6403c, 1);
            }
        }
    }

    static final class InviteAllPermissionRequest implements bjo {

        /* renamed from: a */
        private final WeakReference<InviteContactsActivity> f6407a;

        /* renamed from: b */
        private final View f6408b;

        private InviteAllPermissionRequest(InviteContactsActivity inviteContactsActivity, View view) {
            this.f6407a = new WeakReference<>(inviteContactsActivity);
            this.f6408b = view;
        }

        public void cancel() {
        }

        public void grant() {
            InviteContactsActivity inviteContactsActivity = (InviteContactsActivity) this.f6407a.get();
            if (inviteContactsActivity != null) {
                inviteContactsActivity.inviteAll(this.f6408b);
            }
        }

        public void proceed() {
            InviteContactsActivity inviteContactsActivity = (InviteContactsActivity) this.f6407a.get();
            if (inviteContactsActivity != null) {
                ActivityCompat.requestPermissions(inviteContactsActivity, InviteContactsActivityPermissionsDispatcher.f6401a, 0);
            }
        }
    }

    private InviteContactsActivityPermissionsDispatcher() {
    }

    /* renamed from: a */
    static void m9151a(InviteContactsActivity inviteContactsActivity, int i, int[] iArr) {
        switch (i) {
            case 0:
                if (bjp.m8408a((Context) inviteContactsActivity) >= 23 || bjp.m8410a((Context) inviteContactsActivity, f6401a)) {
                    if (bjp.m8412a(iArr) && f6402b != null) {
                        f6402b.grant();
                    }
                    f6402b = null;
                    return;
                }
                return;
            case 1:
                if (bjp.m8408a((Context) inviteContactsActivity) >= 23 || bjp.m8410a((Context) inviteContactsActivity, f6403c)) {
                    if (bjp.m8412a(iArr) && f6404d != null) {
                        f6404d.grant();
                    }
                    f6404d = null;
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* renamed from: a */
    static void m9152a(InviteContactsActivity inviteContactsActivity, View view) {
        if (bjp.m8410a((Context) inviteContactsActivity, f6401a)) {
            inviteContactsActivity.inviteAll(view);
            return;
        }
        f6402b = new InviteAllPermissionRequest(inviteContactsActivity, view);
        ActivityCompat.requestPermissions(inviteContactsActivity, f6401a, 0);
    }

    /* renamed from: b */
    static void m9154b(InviteContactsActivity inviteContactsActivity, View view) {
        if (bjp.m8410a((Context) inviteContactsActivity, f6403c)) {
            inviteContactsActivity.chooseContacts(view);
            return;
        }
        f6404d = new ChooseContactsPermissionRequest(inviteContactsActivity, view);
        ActivityCompat.requestPermissions(inviteContactsActivity, f6403c, 1);
    }
}
