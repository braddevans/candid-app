package com.becandid.candid.activities;

import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.CreateGroupActivity;

public class CreateGroupActivity$$ViewBinder<T extends CreateGroupActivity> implements ViewBinder<T> {

    /* compiled from: CreateGroupActivity$$ViewBinder */
    public static class InnerUnbinder<T extends CreateGroupActivity> implements Unbinder {

        /* renamed from: a */
        private T f6243a;

        protected InnerUnbinder(T t) {
            this.f6243a = t;
        }

        public final void unbind() {
            if (this.f6243a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6243a);
            this.f6243a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mPlaceholder = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mPlaceholder = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.placeholder, "field 'mPlaceholder'"), R.id.placeholder, "field 'mPlaceholder'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
