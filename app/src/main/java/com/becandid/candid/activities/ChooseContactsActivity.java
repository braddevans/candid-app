package com.becandid.candid.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.p003v7.app.AlertDialog.Builder;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.RecyclerView.OnScrollListener;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.R;
import com.becandid.candid.adapters.ContactsAdapter;
import com.becandid.candid.adapters.ContactsAdapter.C1547a;
import com.becandid.candid.adapters.ContactsAdapter.C1548b;
import com.becandid.candid.data.AppState;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class ChooseContactsActivity extends BaseActivity implements C1548b {

    /* renamed from: a */
    private ContactsAdapter f6170a;

    /* renamed from: b */
    private List<C1547a> f6171b;
    @BindView(2131624094)
    FrameLayout contactsSearchOuter;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public HashSet<C1547a> f6172d;
    @BindView(2131624092)
    RecyclerView recyclerView;
    @BindView(2131624095)
    EditText search;
    @BindView(2131624096)
    Button sendInvites;

    public void filterContacts(String str) {
        String lowerCase = str.toLowerCase();
        ArrayList arrayList = new ArrayList();
        String str2 = "";
        for (C1547a aVar : this.f6171b) {
            if (aVar.f6716c.toLowerCase().startsWith(lowerCase)) {
                String upperCase = aVar.f6716c.substring(0, 1).toUpperCase();
                if (!str2.equals(upperCase)) {
                    C1547a aVar2 = new C1547a();
                    if (upperCase.compareTo("A") < 0) {
                        aVar2.f6720g = "0";
                    } else {
                        aVar2.f6720g = upperCase;
                    }
                    str2 = upperCase;
                    arrayList.add(aVar2);
                }
                arrayList.add(aVar);
            }
        }
        this.f6170a.mo10292a((List<C1547a>) arrayList);
    }

    public void getContacts() {
        if (CandidAnimals.f10672a == null) {
            CandidAnimals.m14509a();
        }
        ArrayList arrayList = new ArrayList(CandidAnimals.f10672a.keySet());
        this.f6171b = new ArrayList();
        Cursor query = getContentResolver().query(Phone.CONTENT_URI, null, null, null, null);
        HashSet hashSet = new HashSet(16);
        while (query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("data1"));
            if (string != null) {
                try {
                    if (string.length() >= 7) {
                        PhoneNumberUtil a = PhoneNumberUtil.m12186a();
                        PhoneNumber a2 = a.mo12920a(string, "US");
                        C1547a aVar = new C1547a();
                        String[] split = query.getString(query.getColumnIndex("display_name")).split(" ", 2);
                        aVar.f6716c = split[0];
                        aVar.f6717d = split.length > 1 ? split[1] : "";
                        aVar.f6718e = a.mo12922a(a2, PhoneNumberFormat.E164);
                        if (!hashSet.contains(aVar.f6718e)) {
                            hashSet.add(aVar.f6718e);
                            aVar.f6714a = (String) arrayList.get(this.f6171b.size() % arrayList.size());
                            aVar.f6715b = (String) AppState.feedColors.get(this.f6171b.size() % AppState.feedColors.size());
                            this.f6171b.add(aVar);
                        }
                    }
                } catch (NumberParseException e) {
                    e.printStackTrace();
                }
            }
        }
        Collections.sort(this.f6171b, new Comparator<C1547a>() {
            public int compare(C1547a aVar, C1547a aVar2) {
                return aVar.f6716c.toLowerCase().compareTo(aVar2.f6716c.toLowerCase());
            }
        });
        filterContacts("");
    }

    public void invite(C1547a aVar, int i) {
        if (!aVar.f6721h) {
            this.f6172d.add(aVar);
        } else {
            this.f6172d.remove(aVar);
        }
        aVar.f6721h = !aVar.f6721h;
        this.f6170a.notifyItemChanged(i);
        this.sendInvites.setText("Invite (" + this.f6172d.size() + ")");
        if (this.f6172d.size() == 1) {
            this.sendInvites.setEnabled(true);
        } else if (this.f6172d.size() == 0) {
            this.sendInvites.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_choose_contacts);
        ButterKnife.bind((Activity) this);
        this.f6170a = new ContactsAdapter();
        getContacts();
        this.f6170a.mo10290a((C1548b) this);
        this.recyclerView.setAdapter(this.f6170a);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.setOnScrollListener(new OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                super.onScrolled(recyclerView, i, i2);
                ChooseContactsActivity.this.contactsSearchOuter.setY(ChooseContactsActivity.this.contactsSearchOuter.getY() - ((float) i2));
            }
        });
        this.search.addTextChangedListener(new AfterChangeTextWatcher() {
            public void afterTextChanged(Editable editable) {
                ChooseContactsActivity.this.filterContacts(editable.toString());
            }
        });
        this.f6172d = new HashSet<>();
    }

    @OnClick({2131624096})
    public void sendInvites(View view) {
        new Builder(this).setTitle((CharSequence) "Invite Contacts").setMessage((CharSequence) "Do you want to anonymously invite your selected contacts?").setCancelable(true).setNegativeButton((CharSequence) getResources().getString(R.string.no), (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).setPositiveButton((CharSequence) getResources().getString(R.string.yes), (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                ayo ayo = new ayo();
                Iterator it = ChooseContactsActivity.this.f6172d.iterator();
                while (it.hasNext()) {
                    C1547a aVar = (C1547a) it.next();
                    ays ays = new ays();
                    boolean z = false;
                    if (aVar.f6718e != null) {
                        ays.mo8408a("phone", aVar.f6718e);
                        z = true;
                    }
                    if (aVar.f6719f != null) {
                        ays.mo8408a("email", aVar.f6719f);
                        z = true;
                    }
                    if (z) {
                        ayo.mo8383a((ayq) ays);
                    }
                }
                intent.putExtra("contacts", ayo.toString());
                ChooseContactsActivity.this.setResult(-1, intent);
                ChooseContactsActivity.this.finish();
            }
        }).create().show();
    }
}
