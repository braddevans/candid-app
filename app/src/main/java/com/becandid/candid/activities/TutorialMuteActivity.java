package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.p001v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;

public class TutorialMuteActivity extends FragmentActivity {
    @BindView(2131624377)
    View continueSub;
    @BindView(2131624367)
    View muteContainer;
    @BindView(2131624372)
    View muteNo;
    @BindView(2131624371)
    View muteYes;
    @BindView(2131624373)
    View subscribeContainer;

    public void finishTutorial() {
        AppState.hasMuted = true;
        AppState.saveState(GossipApplication.m9012a());
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_tutorial_mute);
        ButterKnife.bind((Activity) this);
        if (AppState.config.getInt("android_disable_post_subscription", 1) == 1) {
            this.muteContainer.setVisibility(0);
            this.subscribeContainer.setVisibility(8);
            this.muteYes.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    TutorialMuteActivity.this.setResult(PostDetailsActivity.MUTE_THREAD);
                    TutorialMuteActivity.this.finishTutorial();
                }
            });
            this.muteNo.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    TutorialMuteActivity.this.finishTutorial();
                }
            });
            return;
        }
        this.muteContainer.setVisibility(8);
        this.subscribeContainer.setVisibility(0);
        this.continueSub.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                TutorialMuteActivity.this.finishTutorial();
            }
        });
    }
}
