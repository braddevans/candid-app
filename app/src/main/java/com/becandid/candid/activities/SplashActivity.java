package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.p001v4.app.ActivityCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.PersistentCookieStore;
import com.becandid.candid.models.NetworkData;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import p012rx.exceptions.CompositeException;
import p012rx.schedulers.Schedulers;

public class SplashActivity extends BaseActivity {
    public static final int CHANGE_ENDPOINT_CODE = 1001;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public boolean f6666a = false;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f6667b = false;
    @BindView(2131624357)
    View mChangeEndpoint;
    @BindView(2131624358)
    View mSpinny;
    @BindView(2131624355)
    Button onboardingButton;
    @BindView(2131624354)
    Button recoverAccountButton;
    @BindView(2131624353)
    TextView recoverAccountText;
    @BindView(2131624356)
    TextView termsAndGuidelines;

    class RegisterNewInstallTask extends AsyncTask<String, Void, Void> {
        RegisterNewInstallTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... strArr) {
            String str = strArr[0];
            HttpsURLConnection httpsURLConnection = null;
            try {
                httpsURLConnection = (HttpsURLConnection) new URL(str).openConnection();
                httpsURLConnection.setConnectTimeout(10000);
                httpsURLConnection.setReadTimeout(10000);
                httpsURLConnection.setRequestMethod("GET");
                httpsURLConnection.setRequestProperty("Content-Type", "application/json");
                long responseCode = (long) httpsURLConnection.getResponseCode();
                Log.d("NEW_INSTALL", str);
                Log.d("NEW_INSTALL", String.valueOf(responseCode));
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (Throwable th) {
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
                throw th;
            }
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m9292a(Context context) {
        return VERSION.SDK_INT >= 17 ? WebSettings.getDefaultUserAgent(context) : new WebView(context).getSettings().getUserAgentString();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9294a() {
        setContentView((int) R.layout.activity_splash);
        ButterKnife.bind((Activity) this);
        m9295a((View) this.onboardingButton);
        Spanned fromHtml = Html.fromHtml("By selecting \"Get Started Anonymously\" you agree to the ");
        Spanned fromHtml2 = Html.fromHtml("<a href=\"" + GossipApplication.f6096d + "content/terms\">Terms of Service</a>");
        Spanned fromHtml3 = Html.fromHtml("<a href=\"" + GossipApplication.f6096d + "content/community\">Community Guidelines</a>");
        this.termsAndGuidelines.setText(TextUtils.concat(new CharSequence[]{fromHtml, fromHtml2, " and ", fromHtml3}));
        this.termsAndGuidelines.setMovementMethod(LinkMovementMethod.getInstance());
        if (AccountUtils.m14506d()) {
            this.recoverAccountButton.setVisibility(0);
            this.recoverAccountText.setVisibility(0);
            return;
        }
        this.recoverAccountButton.setVisibility(8);
        this.recoverAccountText.setVisibility(8);
    }

    /* renamed from: a */
    private void m9295a(final View view) {
        this.f6666a = true;
        ApiService.m14297a().mo14127c().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<ays>() {
            public void onCompleted() {
                SplashActivity.this.f6666a = false;
            }

            public void onError(Throwable th) {
                Log.d("Splash", th.toString());
                if (th instanceof CompositeException) {
                    for (Throwable th2 : ((CompositeException) th).mo16158a()) {
                        Log.d("Splash", th2.toString());
                    }
                }
                SplashActivity.this.mSpinny.setVisibility(8);
                view.setEnabled(true);
                Answers.m16445c().mo16454a(new SignUpEvent().mo16564a("splash_page").mo16565a(false));
                SplashActivity.this.f6666a = false;
            }

            public void onNext(ays ays) {
                SplashActivity.this.mSpinny.setVisibility(8);
                view.setEnabled(true);
                Map map = DataUtil.toMap(ays.mo8412d("config"));
                Map map2 = DataUtil.toMap(ays.mo8412d("onboarding_data"));
                if (map != null && map.containsKey("onboarding_tab_order")) {
                    AppState.setOnboardingOrder((List) map.get("onboarding_tab_order"));
                }
                try {
                    AppState.internal = ays.mo8410b("debug").mo8391h();
                    if (AppState.internal) {
                        SplashActivity.this.mChangeEndpoint.setVisibility(0);
                    }
                    AppState.referralId = ays.mo8410b("referral_id").mo8385c();
                } catch (Exception e) {
                    Crashlytics.m16437a((Throwable) e);
                }
                try {
                    List list = DataUtil.toList(ays.mo8411c("under_18_tags"), String.class);
                    List list2 = DataUtil.toList(ays.mo8411c("18_plus_tags"), String.class);
                    AppState.setOnboardingTeenTags(list);
                    AppState.setOnboardingAdultTags(list2);
                } catch (Exception e2) {
                    Crashlytics.m16437a((Throwable) e2);
                }
                AppState.setConfig(map);
                AppState.setOnboardingTags(map2);
                if (SplashActivity.this.f6667b) {
                    SplashActivity.this.startActivity(new Intent(view.getContext(), OnboardingActivity.class));
                    Answers.m16445c().mo16454a(new SignUpEvent().mo16564a("splash_page").mo16565a(true));
                    SplashActivity.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9299a(final Info info) {
        if (info != null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    SplashActivity.this.m9300a(info.getId(), SplashActivity.this.m9292a((Context) SplashActivity.this));
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9300a(String str, String str2) {
        String str3 = "90676";
        if (str != null && str2 != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("https://appclick.co/PublicServices/MobileTrackingApiRestV1.svc/AppWasRun?AppId=");
            sb.append(str3);
            sb.append("&AndroidIDFA=");
            sb.append(str);
            new RegisterNewInstallTask().execute(new String[]{sb.toString()});
        }
    }

    /* renamed from: b */
    private void m9302b() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e) {
            Log.e("MyApplication", "couldn't get package info!");
        }
        if (!AppState.accountDisowned && packageInfo != null && packageInfo.firstInstallTime == packageInfo.lastUpdateTime) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        SplashActivity.this.m9299a(AdvertisingIdClient.getAdvertisingIdInfo(SplashActivity.this));
                    } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException | IllegalStateException e) {
                    }
                }
            }).start();
        }
    }

    public void changeEndpoint(View view) {
        startActivity(new Intent(this, ChangeEndpointActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (AppState.loggedin()) {
            ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainTabsActivity.class));
                    SplashActivity.this.finish();
                }

                public void onNext(NetworkData networkData) {
                    if (networkData != null && networkData.my_info != null) {
                        if (networkData.my_info.user_deleted == 1) {
                            AppState.disownAccount();
                            SplashActivity.this.m9294a();
                        } else if (AppState.needOnboarding == 1) {
                            Intent intent = new Intent(SplashActivity.this, OnboardingActivity.class);
                            intent.putExtra("need_onboarding", 1);
                            SplashActivity.this.startActivity(intent);
                            SplashActivity.this.finish();
                        } else {
                            SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainTabsActivity.class));
                            SplashActivity.this.finish();
                        }
                    }
                }
            });
            return;
        }
        m9302b();
        m9294a();
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i != 1104) {
            return;
        }
        if (iArr.length <= 0 || iArr[0] != 0) {
            this.recoverAccountButton.setEnabled(true);
            this.mSpinny.setVisibility(8);
            return;
        }
        Map b = AccountUtils.m14504b();
        if (b != null) {
            PersistentCookieStore.getCookieStore(this).setAllCookies(b);
            ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                    if (AppState.needOnboarding == 1) {
                        Intent intent = new Intent(SplashActivity.this, OnboardingActivity.class);
                        intent.putExtra("need_onboarding", 1);
                        SplashActivity.this.startActivity(intent);
                        SplashActivity.this.finish();
                        return;
                    }
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainTabsActivity.class));
                    SplashActivity.this.finish();
                }

                public void onError(Throwable th) {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainTabsActivity.class));
                    SplashActivity.this.finish();
                }

                public void onNext(NetworkData networkData) {
                }
            });
            return;
        }
        Toast.makeText(this, "Sorry, unable to recover account", 0).show();
        this.recoverAccountButton.setEnabled(true);
        this.mSpinny.setVisibility(8);
    }

    public void recoverAccount(View view) {
        view.setEnabled(false);
        this.mSpinny.setVisibility(0);
        if (ContextCompat.checkSelfPermission(GossipApplication.m9012a(), "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1104);
            return;
        }
        Map b = AccountUtils.m14504b();
        if (b != null) {
            PersistentCookieStore.getCookieStore(view.getContext()).setAllCookies(b);
            ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                    if (AppState.needOnboarding == 1) {
                        Intent intent = new Intent(SplashActivity.this, OnboardingActivity.class);
                        intent.putExtra("need_onboarding", 1);
                        SplashActivity.this.startActivity(intent);
                        SplashActivity.this.finish();
                        return;
                    }
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainTabsActivity.class));
                    SplashActivity.this.finish();
                }

                public void onError(Throwable th) {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainTabsActivity.class));
                    SplashActivity.this.finish();
                }

                public void onNext(NetworkData networkData) {
                }
            });
            return;
        }
        Toast.makeText(view.getContext(), "Sorry, unable to recover account", 0).show();
        view.setEnabled(true);
        this.mSpinny.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void showSnack(boolean z) {
        super.showSnack(z);
        View findViewById = findViewById(R.id.terms_and_guidelines);
        if (!z && findViewById != null) {
            Snackbar.m3350a(findViewById, "No Internet connection!", 0).mo1721a();
        }
    }

    public void startOnboarding(View view) {
        if (!this.f6666a) {
            m9295a(view);
        }
        this.f6667b = true;
        this.mSpinny.setVisibility(0);
        view.setEnabled(false);
    }
}
