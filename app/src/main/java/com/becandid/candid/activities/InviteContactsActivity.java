package com.becandid.candid.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentTransaction;
import android.support.p003v7.app.AlertDialog.Builder;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.adapters.ContactsAdapter.C1547a;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.EmptyClass;
import com.becandid.candid.fragments.InviteContactMethodFragment;
import com.becandid.candid.fragments.InviteContactPeopleFragment;
import com.becandid.candid.fragments.InviteContactSentFragment;
import com.becandid.candid.models.EmptySubscriber;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import java.util.ArrayList;
import java.util.HashSet;
import p012rx.schedulers.Schedulers;

public class InviteContactsActivity extends BaseActivity {
    public static final int CHOOSE_CONTACTS = 101;

    /* renamed from: a */
    Activity f6391a;

    /* renamed from: b */
    int f6392b = -1;

    /* renamed from: d */
    int f6393d = 0;

    /* renamed from: e */
    String f6394e;
    @BindView(2131624145)
    FrameLayout mPlaceholder;
    public String sendInviteName;

    public enum InviteFlowTypes {
        DOWNLOAD("Download"),
        GROUP("Group"),
        POST("Post");
        

        /* renamed from: a */
        private String f6399a;

        private InviteFlowTypes(String str) {
            this.f6399a = str;
        }

        public String toString() {
            return this.f6399a;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9149a() {
        InviteContactMethodFragment inviteContactMethodFragment = new InviteContactMethodFragment();
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
        beginTransaction.replace(R.id.placeholder, inviteContactMethodFragment);
        beginTransaction.addToBackStack(null);
        beginTransaction.commit();
    }

    public void chooseContacts(View view) {
        startActivityForResult(new Intent(this, ChooseContactsActivity.class), 101);
    }

    public void chooseContactsWithCheck(View view) {
        InviteContactsActivityPermissionsDispatcher.m9154b((InviteContactsActivity) this.f6391a, view);
    }

    public ayo getContacts() {
        new ArrayList(CandidAnimals.f10672a.keySet());
        ayo ayo = new ayo();
        Cursor query = getContentResolver().query(Phone.CONTENT_URI, null, null, null, null);
        HashSet hashSet = new HashSet(16);
        while (query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("data1"));
            if (string != null) {
                try {
                    if (string.length() >= 7) {
                        PhoneNumberUtil a = PhoneNumberUtil.m12186a();
                        PhoneNumber a2 = a.mo12920a(string, "US");
                        new C1547a();
                        String a3 = a.mo12922a(a2, PhoneNumberFormat.E164);
                        if (!hashSet.contains(a3)) {
                            hashSet.add(a3);
                            ayo.mo8383a((ayq) DataUtil.newJsonObject("phone", a3));
                        }
                    }
                } catch (NumberParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return ayo;
    }

    public void inviteAll(View view) {
        new Builder(this).setTitle((CharSequence) "Invite Contacts").setMessage((CharSequence) "Do you want to anonymously invite your selected contacts?").setCancelable(true).setNegativeButton((CharSequence) getResources().getString(R.string.no), (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).setPositiveButton((CharSequence) getResources().getString(R.string.yes), (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                InviteContactsActivity.this.setSendInviteData(InviteContactsActivity.this.getContacts().toString());
                if (AppState.config.getInt("enable_new_invite_flow", 0) == 1) {
                    InviteContactsActivity.this.m9149a();
                    Answers.m16445c().mo16453a(new CustomEvent("New Text Invite FLow"));
                    return;
                }
                InviteContactsActivity.this.sendContactsToServer();
                Answers.m16445c().mo16453a(new CustomEvent("Old Text Invite Flow"));
            }
        }).create().show();
    }

    public void inviteAllWithCheck(View view) {
        InviteContactsActivityPermissionsDispatcher.m9152a((InviteContactsActivity) this.f6391a, view);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 101 && i2 == -1) {
            setSendInviteData(intent.getStringExtra("contacts"));
            if (AppState.config.getInt("enable_new_invite_flow", 0) == 1) {
                m9149a();
                Answers.m16445c().mo16453a(new CustomEvent("New Text Invite FLow"));
                return;
            }
            sendContactsToServer();
            Answers.m16445c().mo16453a(new CustomEvent("Old Text Invite Flow"));
        }
    }

    public void onBackPressed() {
        if (!(getSupportFragmentManager().findFragmentById(R.id.placeholder) instanceof InviteContactSentFragment)) {
            super.onBackPressed();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_invite_contacts);
        ButterKnife.bind((Activity) this);
        setResult(0);
        this.f6391a = this;
        if (this.mPlaceholder != null && bundle == null) {
            InviteContactPeopleFragment inviteContactPeopleFragment = new InviteContactPeopleFragment();
            inviteContactPeopleFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add((int) R.id.placeholder, (Fragment) inviteContactPeopleFragment).commit();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        InviteContactsActivityPermissionsDispatcher.m9151a(this, i, iArr);
    }

    public void sendContactsToServer() {
        ApiService.m14297a().mo14105a(this.f6394e, getIntent().getStringExtra("upsell_id"), this.sendInviteName).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<EmptyClass>() {
            public void onCompleted() {
                if (InviteContactsActivity.this.f6392b != -1 || InviteContactsActivity.this.f6393d != 0) {
                    ApiService.m14297a().mo14145f(InviteContactsActivity.this.f6392b).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                    Intent intent = new Intent();
                    intent.putExtra("upsellId", InviteContactsActivity.this.f6393d);
                    InviteContactsActivity.this.setResult(-1, intent);
                    if (AppState.config.getInt("enable_new_invite_flow", 0) != 1) {
                        InviteContactsActivity.this.finish();
                        Toast.makeText(InviteContactsActivity.this.getBaseContext(), "Your friends have been notified. Thanks!", 1).show();
                    }
                }
            }

            public void onError(Throwable th) {
            }

            public void onNext(EmptyClass emptyClass) {
            }
        });
    }

    public void setGroupId(int i) {
        this.f6392b = i;
    }

    public void setSendInviteData(String str) {
        this.f6394e = str;
    }

    public void setSendInviteName(String str) {
        this.sendInviteName = str;
    }

    public void setUpsellId(int i) {
        this.f6393d = i;
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_left_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
        beginTransaction.replace(R.id.placeholder, fragment);
        beginTransaction.addToBackStack(null);
        beginTransaction.commit();
    }
}
