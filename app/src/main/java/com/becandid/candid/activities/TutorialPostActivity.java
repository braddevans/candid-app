package com.becandid.candid.activities;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.p001v4.app.FragmentActivity;
import android.support.p001v4.view.ViewPager;
import android.support.p001v4.view.ViewPager.C0497e;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;

public class TutorialPostActivity extends FragmentActivity {

    /* renamed from: a */
    private PagerAdapter f6684a;
    @BindView(2131624379)
    ImageView ind1;
    @BindView(2131624380)
    ImageView ind2;
    @BindView(2131624381)
    ImageView ind3;
    @BindView(2131624382)
    ImageView ind4;
    @BindView(2131624378)
    ViewPager viewPager;

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_tutorial_post);
        ButterKnife.bind((Activity) this);
        this.f6684a = new TutorialPagerAdapter(getSupportFragmentManager());
        this.viewPager.setAdapter(this.f6684a);
        this.viewPager.mo2884a((C0497e) new C0497e() {
            public void onPageScrollStateChanged(int i) {
            }

            public void onPageScrolled(int i, float f, int i2) {
            }

            public void onPageSelected(int i) {
                Drawable drawable = TutorialPostActivity.this.getResources().getDrawable(R.drawable.onboarding_dot_highlighted);
                Drawable drawable2 = TutorialPostActivity.this.getResources().getDrawable(R.drawable.onboarding_dot);
                TutorialPostActivity.this.ind1.setImageDrawable(drawable2);
                TutorialPostActivity.this.ind2.setImageDrawable(drawable2);
                TutorialPostActivity.this.ind3.setImageDrawable(drawable2);
                TutorialPostActivity.this.ind4.setImageDrawable(drawable2);
                switch (i) {
                    case 0:
                        TutorialPostActivity.this.ind1.setImageDrawable(drawable);
                        return;
                    case 1:
                        TutorialPostActivity.this.ind2.setImageDrawable(drawable);
                        return;
                    case 2:
                        TutorialPostActivity.this.ind3.setImageDrawable(drawable);
                        return;
                    case 3:
                        TutorialPostActivity.this.ind4.setImageDrawable(drawable);
                        return;
                    default:
                        return;
                }
            }
        });
    }
}
