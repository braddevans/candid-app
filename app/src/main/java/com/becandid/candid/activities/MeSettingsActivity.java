package com.becandid.candid.activities;

import android.app.Activity;
import android.app.UiModeManager;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.NetworkData;
import java.util.ArrayList;
import p012rx.schedulers.Schedulers;

public class MeSettingsActivity extends BaseActivity {
    public static final int HEADER = 0;
    public static final int ITEM = 1;
    public static final int OPTION = 2;
    public static String TAG_SETTINGS_KEY = "tag_settings";

    /* renamed from: a */
    private ArrayList<Setting> f6438a;

    /* renamed from: b */
    private MeSettingsAdapter f6439b;
    @BindView(2131624266)
    RecyclerView recyclerView;
    @BindView(2131624267)
    TextView versionNumber;

    public class ActivitySetting extends Setting {
        public Bundle bundle;
        public Class toStart;

        public ActivitySetting(int i, String str, Class cls, Bundle bundle2) {
            super(i, str);
            this.toStart = cls;
            this.bundle = bundle2;
        }
    }

    public class Setting {
        public String name;
        public int type;

        public Setting(int i, String str) {
            this.type = i;
            this.name = str;
        }
    }

    public class WebViewSetting extends Setting {
        public String url;

        public WebViewSetting(int i, String str, String str2) {
            super(i, str);
            this.url = str2;
        }
    }

    public void disownAccount() {
        ApiService.m14297a().mo14111a(true).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Toast.makeText(MeSettingsActivity.this, "Unable to disown account, please try again.", 1).show();
            }

            public void onNext(NetworkData networkData) {
                if (networkData.success) {
                    AppState.disownAccount();
                    Intent launchIntentForPackage = GossipApplication.m9012a().getBaseContext().getPackageManager().getLaunchIntentForPackage(GossipApplication.m9012a().getPackageName());
                    launchIntentForPackage.addFlags(32768);
                    launchIntentForPackage.addFlags(268435456);
                    MeSettingsActivity.this.startActivity(launchIntentForPackage);
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_me_settings);
        ButterKnife.bind((Activity) this);
        this.f6438a = new ArrayList<>();
        this.f6438a.add(new Setting(0, "ACCOUNT"));
        Bundle bundle2 = new Bundle();
        bundle2.putBoolean(TAG_SETTINGS_KEY, true);
        this.f6438a.add(new ActivitySetting(1, "Edit Interests", OnboardingActivity.class, bundle2));
        if ((AppState.tabsOrder != null && AppState.tabsOrder.contains("community")) || AppState.tabsOrder == null) {
            ActivitySetting activitySetting = new ActivitySetting(1, "Edit Community", CommunitySettingsActivity.class, null);
            this.f6438a.add(activitySetting);
        }
        ActivitySetting activitySetting2 = new ActivitySetting(1, "Notification Settings", NotificationSettingsActivity.class, null);
        this.f6438a.add(activitySetting2);
        this.f6438a.add(new Setting(0, "ABOUT"));
        this.f6438a.add(new WebViewSetting(1, "About Candid", "content/about"));
        this.f6438a.add(new WebViewSetting(1, "Terms of Service", "content/terms"));
        this.f6438a.add(new WebViewSetting(1, "Privacy Policy", "content/privacy"));
        this.f6438a.add(new WebViewSetting(1, "Community Guidelines", "content/community"));
        this.f6438a.add(new WebViewSetting(1, "Get Help", "content/help"));
        this.f6438a.add(new Setting(0, ""));
        this.f6438a.add(new Setting(1, "Disown Permanently"));
        if (AppState.internal) {
            this.f6438a.add(new Setting(0, "Internal"));
            ActivitySetting activitySetting3 = new ActivitySetting(1, "Change Endpoint", ChangeEndpointActivity.class, null);
            this.f6438a.add(activitySetting3);
        }
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.f6439b = new MeSettingsAdapter(this.f6438a, this);
        this.recyclerView.setAdapter(this.f6439b);
        String str = "© Candid 2017";
        try {
            str = str + " • Version " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName + " (" + getPackageManager().getPackageInfo(getPackageName(), 0).versionCode + ")";
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        this.versionNumber.setText(str);
        busSubscribe(C2630as.class, new bjy<C2630as>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2630as asVar) {
                UiModeManager uiModeManager = (UiModeManager) MeSettingsActivity.this.getSystemService("uimode");
                if (asVar.f10609a) {
                    uiModeManager.setNightMode(2);
                } else {
                    uiModeManager.setNightMode(1);
                }
                int currentModeType = uiModeManager.getCurrentModeType();
                int nightMode = uiModeManager.getNightMode();
            }
        });
    }
}
