package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.CommentStatsActivity;

public class CommentStatsActivity$$ViewBinder<T extends CommentStatsActivity> implements ViewBinder<T> {

    /* compiled from: CommentStatsActivity$$ViewBinder */
    public static class InnerUnbinder<T extends CommentStatsActivity> implements Unbinder {

        /* renamed from: a */
        private T f6185a;

        protected InnerUnbinder(T t) {
            this.f6185a = t;
        }

        public final void unbind() {
            if (this.f6185a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6185a);
            this.f6185a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mRecyclerView = null;
            t.mProgressBar = null;
            t.mOthersLiked = null;
            t.mOthersDisliked = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mRecyclerView = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_stats_recyclerview, "field 'mRecyclerView'"), R.id.comment_stats_recyclerview, "field 'mRecyclerView'");
        t.mProgressBar = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.progress_bar, "field 'mProgressBar'"), R.id.progress_bar, "field 'mProgressBar'");
        t.mOthersLiked = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_stats_others_liked, "field 'mOthersLiked'"), R.id.comment_stats_others_liked, "field 'mOthersLiked'");
        t.mOthersDisliked = (TextView) finder.castView((View) finder.findRequiredView(obj, R.id.comment_stats_others_disliked, "field 'mOthersDisliked'"), R.id.comment_stats_others_disliked, "field 'mOthersDisliked'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
