package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.p003v7.widget.AppCompatEditText;
import android.support.p003v7.widget.ListPopupWindow;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.TextTag;
import com.becandid.candid.models.NetworkData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class ContentEditActivity extends BaseActivity {

    /* renamed from: a */
    private String f6212a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f6213b;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public int f6214d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f6215e;
    @BindView(2131624142)
    Button editCancel;
    @BindView(2131624138)
    FrameLayout editContainer;
    @BindView(2131624140)
    TextView editDetails;
    @BindView(2131624135)
    TextView editHeaderTitle;
    @BindView(2131624143)
    Button editSave;
    @BindView(2131624144)
    FrameLayout editSpinny;
    @BindView(2131624139)
    AppCompatEditText editText;
    @BindView(2131624133)
    RelativeLayout editTitle;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public ArrayList<TextTag> f6216f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public ListPopupWindow f6217g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public GroupAutocompleteAdapter f6218h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public CharSequence f6219i = "";
    /* access modifiers changed from: private */

    /* renamed from: j */
    public boolean f6220j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public int f6221k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public int f6222l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public int f6223m;
    @BindView(2131624132)
    RelativeLayout root;

    /* access modifiers changed from: protected */
    public void keyboardClosed() {
        this.editDetails.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void keyboardOpened() {
        new Handler().post(new Runnable() {
            public void run() {
                Rect rect = new Rect();
                ContentEditActivity.this.root.getWindowVisibleDisplayFrame(rect);
                if (Float.compare(((float) (rect.bottom - rect.top)) / ((float) ContentEditActivity.this.f6214d), 0.5f) >= 0) {
                    ContentEditActivity.this.editDetails.setVisibility(8);
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_content_edit);
        ButterKnife.bind((Activity) this);
        enableKeyboardEvents(this.root);
        Rect rect = new Rect();
        this.root.getWindowVisibleDisplayFrame(rect);
        this.f6214d = rect.bottom - rect.top;
        if (getIntent().hasExtra("comment_id")) {
            this.f6215e = "comment";
            this.f6213b = getIntent().getIntExtra("comment_id", 0);
            this.editHeaderTitle.setText(R.string.edit_comment);
            this.editText.setText(R.string.comment_edit_title);
            this.editDetails.setText(R.string.comment_edit_details);
            this.f6212a = getIntent().getStringExtra("comment_text");
        } else if (getIntent().hasExtra("post_id")) {
            this.f6215e = "post";
            this.f6213b = getIntent().getIntExtra("post_id", 0);
            this.editHeaderTitle.setText(R.string.edit_post);
            this.editText.setText(R.string.post_edit_title);
            this.editDetails.setText(R.string.post_edit_details);
            this.f6212a = getIntent().getStringExtra("caption");
        } else {
            Toast.makeText(this, "Invalid edit attempt", 1).show();
            finish();
        }
        this.f6216f = new ArrayList();
        if (getIntent().hasExtra("group_tags")) {
            String[] split = getIntent().getStringExtra("group_tags").split(";");
            int length = split.length;
            for (int i = 0; i < length; i++) {
                String[] split2 = split[i].split(",");
                this.f6216f.add(new TextTag(Integer.parseInt(split2[1]), Integer.parseInt(split2[2]), Integer.parseInt(split2[0])));
            }
        }
        this.f6221k = AppState.config.getInt("disable_group_tagging", 0);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.f6212a);
        if (this.f6221k == 0 && !this.f6216f.isEmpty()) {
            ArrayList<TextTag> arrayList = new ArrayList<>();
            Iterator it = this.f6216f.iterator();
            while (it.hasNext()) {
                TextTag textTag = (TextTag) it.next();
                if (textTag.endIndex > spannableStringBuilder.length()) {
                    textTag.endIndex = spannableStringBuilder.length();
                }
                if (textTag.startIndex < 0) {
                    textTag.startIndex = 0;
                }
                if (textTag.startIndex >= spannableStringBuilder.length() || textTag.endIndex <= 0) {
                    break;
                }
                arrayList.add(textTag);
                if (textTag.startIndex > -1 && textTag.endIndex > -1) {
                    spannableStringBuilder.setSpan(C2699jj.m14606a(), textTag.startIndex, textTag.endIndex, 33);
                }
            }
            this.f6216f = arrayList;
        }
        this.editText.setText(spannableStringBuilder);
        this.editText.setSelection(this.editText.length());
        this.editText.requestFocus();
        openKeyboard();
        FrameLayout frameLayout = this.editContainer;
        C13221 r0 = new OnClickListener() {
            public void onClick(View view) {
                ContentEditActivity.this.editText.setSelection(ContentEditActivity.this.editText.length());
                ContentEditActivity.this.editText.requestFocus();
                if (!ContentEditActivity.this.keyboardOpen) {
                    ContentEditActivity.this.openKeyboard();
                }
            }
        };
        frameLayout.setOnClickListener(r0);
        if (AppState.groups == null) {
            AppState.groups = new ArrayList();
        }
        GroupAutocompleteAdapter hkVar = new GroupAutocompleteAdapter(this, R.layout.group_search_result, new ArrayList(AppState.groups));
        this.f6218h = hkVar;
        ListPopupWindow listPopupWindow = new ListPopupWindow(this);
        this.f6217g = listPopupWindow;
        this.f6217g.setAdapter(this.f6218h);
        this.f6217g.setAnchorView(this.editText);
        this.f6217g.setAnimationStyle(0);
        this.f6217g.setHeight(-2);
        this.f6217g.setWidth(C2699jj.m14599a(150, (Context) this));
        ListPopupWindow listPopupWindow2 = this.f6217g;
        C13232 r02 = new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Group group = (Group) adapterView.getItemAtPosition(i);
                String[] split = ContentEditActivity.this.editText.getText().toString().split("(?=#)");
                int selectionStart = ContentEditActivity.this.editText.getSelectionStart();
                int i2 = 0;
                int i3 = 0;
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("");
                ArrayList arrayList = new ArrayList();
                int i4 = 0;
                int length = split.length;
                for (int i5 = 0; i5 < length; i5++) {
                    String str = split[i5];
                    int i6 = i2;
                    i2 += str.length();
                    if (i6 >= selectionStart || selectionStart > i2) {
                        spannableStringBuilder.append(str);
                    } else {
                        int length2 = spannableStringBuilder.length();
                        spannableStringBuilder.append("#" + group.group_name);
                        i4 = (group.group_name.length() + 1) - (selectionStart - length2);
                        i3 = spannableStringBuilder.length();
                        spannableStringBuilder.append(" ");
                        spannableStringBuilder.append(str.substring(selectionStart - i6));
                        TextTag textTag = new TextTag(length2, i3, group.group_id);
                        arrayList.add(textTag);
                    }
                }
                Iterator it = ContentEditActivity.this.f6216f.iterator();
                while (it.hasNext()) {
                    TextTag textTag2 = (TextTag) it.next();
                    if (selectionStart < textTag2.startIndex || selectionStart > textTag2.endIndex) {
                        if (textTag2.startIndex > selectionStart) {
                            textTag2.startIndex += i4;
                            textTag2.endIndex += i4;
                        }
                        arrayList.add(textTag2);
                    }
                }
                ContentEditActivity.this.f6216f = arrayList;
                Iterator it2 = ContentEditActivity.this.f6216f.iterator();
                while (it2.hasNext()) {
                    TextTag textTag3 = (TextTag) it2.next();
                    if (textTag3.startIndex > -1 && textTag3.endIndex > -1) {
                        spannableStringBuilder.setSpan(C2699jj.m14606a(), textTag3.startIndex, textTag3.endIndex, 33);
                    }
                }
                ContentEditActivity.this.f6219i = spannableStringBuilder;
                ContentEditActivity.this.editText.setText(spannableStringBuilder);
                ContentEditActivity.this.editText.setSelection(i3 + 1);
                ContentEditActivity.this.f6217g.dismiss();
            }
        };
        listPopupWindow2.setOnItemClickListener(r02);
        AppCompatEditText appCompatEditText = this.editText;
        C13243 r03 = new OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z && ContentEditActivity.this.f6221k == 0) {
                    C2699jj.m14620a((CharSequence) ContentEditActivity.this.editText.getText(), (EditText) ContentEditActivity.this.editText, ContentEditActivity.this.f6217g, ContentEditActivity.this.f6218h);
                }
            }
        };
        appCompatEditText.setOnFocusChangeListener(r03);
        AppCompatEditText appCompatEditText2 = this.editText;
        C13254 r04 = new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (ContentEditActivity.this.f6221k == 0 && !editable.toString().equals(ContentEditActivity.this.f6219i.toString())) {
                    ContentEditActivity.this.f6219i = editable.toString();
                    C2699jj.m14620a((CharSequence) editable, (EditText) ContentEditActivity.this.editText, ContentEditActivity.this.f6217g, ContentEditActivity.this.f6218h);
                    if (ContentEditActivity.this.f6220j) {
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(editable.toString());
                        Iterator it = ContentEditActivity.this.f6216f.iterator();
                        while (it.hasNext()) {
                            TextTag textTag = (TextTag) it.next();
                            if (textTag.startIndex > -1 && textTag.endIndex > -1) {
                                spannableStringBuilder.setSpan(C2699jj.m14606a(), textTag.startIndex, textTag.endIndex, 33);
                            }
                        }
                        ContentEditActivity.this.f6220j = false;
                        ContentEditActivity.this.f6219i = spannableStringBuilder.toString();
                        int selectionStart = ContentEditActivity.this.editText.getSelectionStart();
                        ContentEditActivity.this.editText.setText(spannableStringBuilder);
                        ContentEditActivity.this.editText.setSelection(selectionStart);
                    }
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                ContentEditActivity.this.f6222l = ContentEditActivity.this.editText.getSelectionStart();
                ContentEditActivity.this.f6223m = ContentEditActivity.this.editText.getSelectionEnd();
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (ContentEditActivity.this.f6221k == 0 && !charSequence.toString().equals(ContentEditActivity.this.f6219i.toString())) {
                    int selectionStart = ContentEditActivity.this.editText.getSelectionStart();
                    ArrayList arrayList = new ArrayList();
                    Iterator it = ContentEditActivity.this.f6216f.iterator();
                    while (it.hasNext()) {
                        TextTag textTag = (TextTag) it.next();
                        if (ContentEditActivity.this.f6222l <= textTag.startIndex && ContentEditActivity.this.f6223m <= textTag.startIndex) {
                            textTag.startIndex += i3 - i2;
                            textTag.endIndex += i3 - i2;
                            arrayList.add(textTag);
                            ContentEditActivity.this.f6220j = true;
                        } else if (ContentEditActivity.this.f6222l <= textTag.startIndex && ContentEditActivity.this.f6223m > textTag.startIndex) {
                            ContentEditActivity.this.f6220j = true;
                        } else if (ContentEditActivity.this.f6222l > textTag.startIndex && ContentEditActivity.this.f6222l <= textTag.endIndex) {
                            ContentEditActivity.this.f6220j = true;
                        } else if (selectionStart > textTag.endIndex) {
                            arrayList.add(textTag);
                        } else {
                            ContentEditActivity.this.f6220j = true;
                        }
                    }
                    ContentEditActivity.this.f6216f = arrayList;
                }
            }
        };
        appCompatEditText2.addTextChangedListener(r04);
        Button button = this.editSave;
        C13265 r05 = new OnClickListener() {
            public void onClick(View view) {
                if (ContentEditActivity.this.editText.getText().toString().equals("") || ContentEditActivity.this.editText.getText().toString().trim().equals("")) {
                    Toast.makeText(ContentEditActivity.this, R.string.comment_edit_empty, 0).show();
                    return;
                }
                ContentEditActivity.this.editSpinny.setVisibility(0);
                HashMap hashMap = new HashMap();
                if (!ContentEditActivity.this.f6216f.isEmpty()) {
                    String str = "";
                    Iterator it = ContentEditActivity.this.f6216f.iterator();
                    while (it.hasNext()) {
                        str = str + ((TextTag) it.next()).toString() + ";";
                    }
                    hashMap.put("mentioned_group_ids", str.substring(0, str.length() - 1));
                }
                if (ContentEditActivity.this.f6215e.equals("comment")) {
                    hashMap.put("comment_id", Integer.toString(ContentEditActivity.this.f6213b));
                    hashMap.put("comment_text", ContentEditActivity.this.editText.getText().toString());
                    ApiService.m14297a().mo14167m((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                        public void onCompleted() {
                        }

                        public void onError(Throwable th) {
                            ContentEditActivity.this.editSpinny.setVisibility(8);
                            Toast.makeText(ContentEditActivity.this, th.toString(), 1).show();
                        }

                        public void onNext(NetworkData networkData) {
                            if (networkData.success) {
                                String str = null;
                                if (!ContentEditActivity.this.f6216f.isEmpty()) {
                                    String str2 = "";
                                    Iterator it = ContentEditActivity.this.f6216f.iterator();
                                    while (it.hasNext()) {
                                        str2 = str2 + ((TextTag) it.next()).toString() + ";";
                                    }
                                    str = str2.substring(0, str2.length() - 1);
                                }
                                RxBus.m14573a().mo14349a(new C2651h(ContentEditActivity.this.f6213b, ContentEditActivity.this.editText.getText().toString(), str));
                                Intent intent = new Intent();
                                intent.putExtra("comment_id", ContentEditActivity.this.f6213b);
                                ContentEditActivity.this.setResult(203, intent);
                                ContentEditActivity.this.finish();
                                return;
                            }
                            ContentEditActivity.this.editSpinny.setVisibility(8);
                            Toast.makeText(ContentEditActivity.this, networkData.error, 1).show();
                        }
                    });
                } else if (ContentEditActivity.this.f6215e.equals("post")) {
                    hashMap.put("post_id", Integer.toString(ContentEditActivity.this.f6213b));
                    hashMap.put("caption", ContentEditActivity.this.editText.getText().toString());
                    ApiService.m14297a().mo14165l((Map<String, String>) hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                        public void onCompleted() {
                        }

                        public void onError(Throwable th) {
                            ContentEditActivity.this.editSpinny.setVisibility(8);
                            Toast.makeText(ContentEditActivity.this, th.toString(), 1).show();
                        }

                        public void onNext(NetworkData networkData) {
                            if (networkData.success) {
                                String str = null;
                                if (!ContentEditActivity.this.f6216f.isEmpty()) {
                                    String str2 = "";
                                    Iterator it = ContentEditActivity.this.f6216f.iterator();
                                    while (it.hasNext()) {
                                        str2 = str2 + ((TextTag) it.next()).toString() + ";";
                                    }
                                    str = str2.substring(0, str2.length() - 1);
                                }
                                RxBus.m14573a().mo14349a(new C2622ak(ContentEditActivity.this.f6213b, ContentEditActivity.this.editText.getText().toString(), str));
                                ContentEditActivity.this.finish();
                                return;
                            }
                            ContentEditActivity.this.editSpinny.setVisibility(8);
                            Toast.makeText(ContentEditActivity.this, networkData.error, 1).show();
                        }
                    });
                }
            }
        };
        button.setOnClickListener(r05);
        Button button2 = this.editCancel;
        C13296 r06 = new OnClickListener() {
            public void onClick(View view) {
                ContentEditActivity.this.backNavClick(view);
            }
        };
        button2.setOnClickListener(r06);
    }
}
