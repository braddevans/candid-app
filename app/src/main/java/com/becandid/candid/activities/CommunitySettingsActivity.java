package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.p003v7.widget.LinearLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.Group;
import com.becandid.candid.models.NetworkData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class CommunitySettingsActivity extends BaseActivity {
    public static final int REQUEST_COLLEGE = 202;
    public static final int REQUEST_HIGHSCHOOL = 201;
    public static final int REQUEST_WORK = 203;

    /* renamed from: a */
    List<Group> f6198a;

    /* renamed from: b */
    List<Group> f6199b;
    @BindView(2131624123)
    TextView collegeButton;
    @BindView(2131624122)
    TextView collegeEmpty;
    @BindView(2131624121)
    RecyclerView collegeRecycler;

    /* renamed from: d */
    List<Group> f6200d;

    /* renamed from: e */
    CommunityTagAdapter f6201e;

    /* renamed from: f */
    CommunityTagAdapter f6202f;

    /* renamed from: g */
    CommunityTagAdapter f6203g;

    /* renamed from: h */
    boolean f6204h;
    @BindView(2131624115)
    TextView highSchoolButton;
    @BindView(2131624114)
    TextView highSchoolEmpty;
    @BindView(2131624113)
    RecyclerView highSchoolRecycler;
    @BindView(2131624131)
    TextView workButton;
    @BindView(2131624130)
    TextView workEmpty;
    @BindView(2131624129)
    RecyclerView workRecycler;

    /* renamed from: a */
    private void m9034a() {
        if (this.f6198a == null || this.f6198a.size() <= 0) {
            this.highSchoolRecycler.setVisibility(8);
            this.highSchoolEmpty.setVisibility(0);
        } else {
            this.highSchoolRecycler.setVisibility(0);
            this.highSchoolEmpty.setVisibility(8);
        }
        if (this.f6199b == null || this.f6199b.size() <= 0) {
            this.collegeRecycler.setVisibility(8);
            this.collegeEmpty.setVisibility(0);
        } else {
            this.collegeRecycler.setVisibility(0);
            this.collegeEmpty.setVisibility(8);
        }
        if (this.f6200d == null || this.f6200d.size() <= 0) {
            this.workRecycler.setVisibility(8);
            this.workEmpty.setVisibility(0);
            return;
        }
        this.workRecycler.setVisibility(0);
        this.workEmpty.setVisibility(8);
    }

    /* renamed from: a */
    private void m9035a(final Group group, final boolean z) {
        HashMap hashMap = new HashMap();
        ArrayList<Group> arrayList = new ArrayList<>();
        if (this.f6198a != null && this.f6198a.size() > 0) {
            arrayList.addAll(this.f6198a);
        }
        if (this.f6199b != null && this.f6199b.size() > 0) {
            arrayList.addAll(this.f6199b);
        }
        if (this.f6200d != null && this.f6200d.size() > 0) {
            arrayList.addAll(this.f6200d);
        }
        ArrayList arrayList2 = new ArrayList();
        for (Group group2 : arrayList) {
            arrayList2.add(Integer.toString(group2.group_id));
        }
        hashMap.put("group_ids", bid.m8218a((Iterable<?>) arrayList2, ","));
        ApiService.m14297a().mo14179x(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(NetworkData networkData) {
                if (!networkData.success) {
                    return;
                }
                if (z) {
                    RxBus.m14573a().mo14349a(new C2659p(group));
                } else {
                    RxBus.m14573a().mo14349a(new C2661r(group.group_id));
                }
            }
        });
        m9034a();
        this.f6204h = true;
    }

    public void backNavClick(View view) {
        if (this.f6204h) {
            RxBus.m14573a().mo14349a(new C2635ax());
        }
        super.backNavClick(view);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras.containsKey("queryString") && extras.containsKey("queryId")) {
                Group group = new Group();
                group.group_name = extras.getString("queryString");
                group.group_id = Integer.parseInt(extras.getString("queryId"));
                group.num_members = Integer.parseInt(extras.getString("num_members"));
                group.community_membership = 1;
                switch (i) {
                    case REQUEST_HIGHSCHOOL /*201*/:
                        group.group_type = "school";
                        this.f6198a.add(group);
                        this.f6201e.mo13806a(group);
                        this.highSchoolRecycler.smoothScrollToPosition(this.f6201e.getItemCount() - 1);
                        m9035a(group, true);
                        return;
                    case REQUEST_COLLEGE /*202*/:
                        group.group_type = "college";
                        this.f6199b.add(group);
                        this.f6202f.mo13806a(group);
                        this.collegeRecycler.smoothScrollToPosition(this.f6202f.getItemCount() - 1);
                        m9035a(group, true);
                        return;
                    case 203:
                        group.group_type = "company";
                        this.f6200d.add(group);
                        this.f6203g.mo13806a(group);
                        this.workRecycler.smoothScrollToPosition(this.f6203g.getItemCount() - 1);
                        m9035a(group, true);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_community_settings);
        ButterKnife.bind((Activity) this);
        this.f6198a = new ArrayList();
        this.f6199b = new ArrayList();
        this.f6200d = new ArrayList();
        if (AppState.groups != null) {
            for (Group group : AppState.groups) {
                if (group.community_membership == 1 && group.group_type != null) {
                    String str = group.group_type;
                    char c = 65535;
                    switch (str.hashCode()) {
                        case -907977868:
                            if (str.equals("school")) {
                                c = 0;
                                break;
                            }
                            break;
                        case 949445015:
                            if (str.equals("college")) {
                                c = 1;
                                break;
                            }
                            break;
                        case 950484093:
                            if (str.equals("company")) {
                                c = 2;
                                break;
                            }
                            break;
                    }
                    switch (c) {
                        case 0:
                            this.f6198a.add(group);
                            break;
                        case 1:
                            this.f6199b.add(group);
                            break;
                        case 2:
                            this.f6200d.add(group);
                            break;
                    }
                }
            }
        } else {
            AppState.groups = new ArrayList();
        }
        this.highSchoolRecycler.setLayoutManager(new LinearLayoutManager(this, 0, false));
        this.f6201e = new CommunityTagAdapter("school");
        this.f6201e.mo13764a((List) this.f6198a);
        this.highSchoolRecycler.setAdapter(this.f6201e);
        this.collegeRecycler.setLayoutManager(new LinearLayoutManager(this, 0, false));
        this.f6202f = new CommunityTagAdapter("college");
        this.f6202f.mo13764a((List) this.f6199b);
        this.collegeRecycler.setAdapter(this.f6202f);
        this.workRecycler.setLayoutManager(new LinearLayoutManager(this, 0, false));
        this.f6203g = new CommunityTagAdapter("company");
        this.f6203g.mo13764a((List) this.f6200d);
        this.workRecycler.setAdapter(this.f6203g);
        m9034a();
        this.highSchoolButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CommunitySearchActivity.class);
                intent.putExtra("queryType", "school");
                intent.putExtra("searchViewHint", "ex. Mill High School");
                CommunitySettingsActivity.this.startActivityForResult(intent, CommunitySettingsActivity.REQUEST_HIGHSCHOOL);
            }
        });
        this.collegeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CommunitySearchActivity.class);
                intent.putExtra("queryType", "college");
                intent.putExtra("searchViewHint", "ex. University of San Francisco");
                CommunitySettingsActivity.this.startActivityForResult(intent, CommunitySettingsActivity.REQUEST_COLLEGE);
            }
        });
        this.workButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CommunitySearchActivity.class);
                intent.putExtra("queryType", "company");
                intent.putExtra("searchViewHint", "ex. MyLikes");
                CommunitySettingsActivity.this.startActivityForResult(intent, 203);
            }
        });
    }

    public void removeGroup(String str, Group group) {
        List<Group> list;
        char c = 65535;
        switch (str.hashCode()) {
            case -907977868:
                if (str.equals("school")) {
                    c = 0;
                    break;
                }
                break;
            case 949445015:
                if (str.equals("college")) {
                    c = 1;
                    break;
                }
                break;
            case 950484093:
                if (str.equals("company")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                list = this.f6198a;
                break;
            case 1:
                list = this.f6199b;
                break;
            case 2:
                list = this.f6200d;
                break;
            default:
                return;
        }
        for (int i = 0; i < list.size(); i++) {
            if (((Group) list.get(i)).group_id == group.group_id) {
                list.remove(i);
            }
        }
        for (int i2 = 0; i2 < AppState.groups.size(); i2++) {
            if (((Group) AppState.groups.get(i2)).group_id == group.group_id) {
                AppState.groups.remove(i2);
                RxBus.m14573a().mo14349a(new C2661r(group.group_id));
            }
        }
        m9035a(group, false);
    }
}
