package com.becandid.candid.activities;

import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.TutorialLinkSearchActivity;

public class TutorialLinkSearchActivity$$ViewBinder<T extends TutorialLinkSearchActivity> implements ViewBinder<T> {

    /* compiled from: TutorialLinkSearchActivity$$ViewBinder */
    public static class InnerUnbinder<T extends TutorialLinkSearchActivity> implements Unbinder {

        /* renamed from: a */
        private T f6679a;

        protected InnerUnbinder(T t) {
            this.f6679a = t;
        }

        public final void unbind() {
            if (this.f6679a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6679a);
            this.f6679a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mContinue = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mContinue = (Button) finder.castView((View) finder.findRequiredView(obj, R.id.tutorial_continue, "field 'mContinue'"), R.id.tutorial_continue, "field 'mContinue'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
