package com.becandid.candid.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.p003v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.models.EmptySubscriber;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import p012rx.schedulers.Schedulers;

public class MessageSettingsActivity extends BaseActivity {

    /* renamed from: a */
    private ArrayList<String> f6524a;

    /* renamed from: b */
    private ArrayList<String> f6525b;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public ToggleSettingsAdapter f6526d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public ToggleSettingsAdapter f6527e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public boolean f6528f;

    class ToggleSettingsAdapter extends ArrayAdapter<String> {

        /* renamed from: b */
        private int f6534b;

        public ToggleSettingsAdapter(Context context, List<String> list, int i) {
            super(context, -1, list);
            this.f6534b = i;
        }

        public int getSetting() {
            return this.f6534b;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(R.layout.notification_setting, viewGroup, false);
            }
            ((ImageView) view.findViewById(R.id.setting_checkmark)).setImageResource(i == this.f6534b ? R.drawable.green_circle_check : R.drawable.empty_checkmark);
            ((TextView) view.findViewById(R.id.setting_display_name)).setText((CharSequence) getItem(i));
            return view;
        }

        public boolean selectOption(int i) {
            if (this.f6534b == i) {
                return false;
            }
            this.f6534b = i;
            notifyDataSetChanged();
            return true;
        }
    }

    public void cancelClick(View view) {
        finish();
    }

    public void onBackPressed() {
        if (this.f6528f) {
            new Builder(this).setTitle((CharSequence) "Unsaved changes").setMessage((CharSequence) "Save the settings or reset them?").setNegativeButton((CharSequence) "Reset", (OnClickListener) new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    MessageSettingsActivity.this.cancelClick(null);
                }
            }).setPositiveButton((CharSequence) "Save", (OnClickListener) new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    MessageSettingsActivity.this.saveClick(null);
                }
            }).create().show();
        } else {
            super.onBackPressed();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_message_settings);
        ListView listView = (ListView) findViewById(R.id.messaging_settings_list);
        ListView listView2 = (ListView) findViewById(R.id.deletion_settings_list);
        TextView textView = (TextView) findViewById(R.id.settings_subheader_deletion);
        this.f6524a = new ArrayList<>(2);
        this.f6524a.add(AppState.config.getString("enable_messaging_prompt", "Messaging enabled"));
        this.f6524a.add(AppState.config.getString("disable_messaging_prompt", "Messaging disabled"));
        this.f6525b = new ArrayList<>(2);
        this.f6525b.add(AppState.config.getString("self_delete_messages_prompt", "Messages manually deleted"));
        this.f6525b.add(AppState.config.getString("auto_delete_messages_prompt", "Messages deleted automatically after 2 days"));
        this.f6526d = new ToggleSettingsAdapter(this, this.f6524a, AppState.account.messaging_disabled);
        listView.setAdapter(this.f6526d);
        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                MessageSettingsActivity.this.f6528f = MessageSettingsActivity.this.f6526d.selectOption(i);
            }
        });
        this.f6527e = new ToggleSettingsAdapter(this, this.f6525b, AppState.account.message_auto_deletion);
        listView2.setAdapter(this.f6527e);
        listView2.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                MessageSettingsActivity.this.f6528f = MessageSettingsActivity.this.f6527e.selectOption(i);
            }
        });
        if (AppState.account.message_auto_deletion == 0) {
            textView.setVisibility(8);
            listView2.setVisibility(8);
            return;
        }
        textView.setVisibility(0);
        listView2.setVisibility(0);
    }

    public void saveClick(View view) {
        if (this.f6528f) {
            HashMap hashMap = new HashMap();
            hashMap.put("messaging_disabled", Integer.toString(this.f6526d.getSetting()));
            hashMap.put("message_auto_deletion", Integer.toString(this.f6527e.getSetting()));
            AppState.account.messaging_disabled = this.f6526d.getSetting();
            AppState.account.message_auto_deletion = this.f6527e.getSetting();
            ApiService.m14297a().mo14173r(hashMap).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
            RxBus.m14573a().mo14349a(new C2616ae(AppState.account.messaging_disabled));
        }
        finish();
    }
}
