package com.becandid.candid.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.ChangeEndpointActivity;

public class ChangeEndpointActivity$$ViewBinder<T extends ChangeEndpointActivity> implements ViewBinder<T> {

    /* compiled from: ChangeEndpointActivity$$ViewBinder */
    public static class InnerUnbinder<T extends ChangeEndpointActivity> implements Unbinder {

        /* renamed from: a */
        private T f6169a;

        protected InnerUnbinder(T t) {
            this.f6169a = t;
        }

        public final void unbind() {
            if (this.f6169a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6169a);
            this.f6169a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mRadioGroup = null;
            t.mEndpointText = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mRadioGroup = (RadioGroup) finder.castView((View) finder.findRequiredView(obj, R.id.radio_group, "field 'mRadioGroup'"), R.id.radio_group, "field 'mRadioGroup'");
        t.mEndpointText = (EditText) finder.castView((View) finder.findRequiredView(obj, R.id.endpoint, "field 'mEndpointText'"), R.id.endpoint, "field 'mEndpointText'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
