package com.becandid.candid.activities;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.p003v7.app.AlertDialog;
import android.support.p003v7.widget.ListPopupWindow;
import android.support.p003v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.VideoView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.data.DataUtil;
import com.becandid.candid.data.DataUtil.StringTrimResult;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.Post;
import com.becandid.candid.data.TextTag;
import com.becandid.candid.models.BaseVideoMessage;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData;
import com.becandid.candid.models.UploadMediaResponse;
import com.becandid.candid.views.AlwaysOnAutoCompleteView;
import com.becandid.thirdparty.BlurTask;
import com.becandid.thirdparty.BlurTask.BadgeType;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.venmo.view.TooltipView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import p012rx.exceptions.CompositeException;
import p012rx.schedulers.Schedulers;

public class CreatePostActivity extends PopupWithBlurBackgroundActivity implements OnClickListener, C2716c {
    public static final int ADD_LINK_REQUEST = 1002;
    public static final int CREATE_GROUP_FOR_POST = 1001;
    public static final int TUTORIAL_LINK_SEARCH = 1004;
    public static final int TUTORIAL_POST = 1003;

    /* renamed from: a */
    final Runnable f6244a = new Runnable() {
        public void run() {
            CreatePostActivity.this.tooltipView.setVisibility(8);
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: b */
    public String f6245b;
    @BindView(2131624176)
    View camBtn;
    @BindView(2131624154)
    View createGroupButton;

    /* renamed from: d */
    private int f6246d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f6247e = true;

    /* renamed from: f */
    private Handler f6248f;
    @BindView(2131624182)
    TextView friendMarker;
    @BindView(2131624181)
    SwitchCompat friendSwitch;

    /* renamed from: g */
    private String f6249g;
    @BindView(2131624180)
    ImageView gifSearchBtn;
    @BindView(2131624151)
    AlwaysOnAutoCompleteView groupNameSelector;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public GroupAutocompleteAdapter f6250h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public int f6251i;
    @BindView(2131624160)
    View imageProgress;
    @BindView(2131624159)
    View imageProgressContainer;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public ArrayList<TextTag> f6252j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public ListPopupWindow f6253k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public GroupAutocompleteAdapter f6254l;
    @BindView(2131624178)
    View linkBtn;
    @BindView(2131624162)
    RelativeLayout linkContainer;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public CharSequence f6255m = "";
    @BindView(2131624149)
    View mRootView;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public int f6256n;
    @BindView(2131624163)
    View nonYoutubeLinkContainer;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public int f6257o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public boolean f6258p;
    @BindView(2131624150)
    Button postButton;
    @BindView(2131624156)
    EditText postCaption;
    @BindView(2131624155)
    ScrollView postContent;
    @BindView(2131624158)
    ImageView postPhoto;
    @BindView(2131624157)
    View postPhotoOuter;
    @BindView(2131624147)
    View postSpinny;
    @BindView(2131624171)
    View previewVideoContainer;
    @BindView(2131624172)
    VideoView previewVideoFrame;
    @BindView(2131624173)
    View previewVideoPlay;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public Map<String, String> f6259q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public boolean f6260r;

    /* renamed from: s */
    private bjz f6261s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public String f6262t;
    @BindView(2131624174)
    TooltipView tooltipView;

    /* renamed from: u */
    private SimpleOnGestureListener f6263u = new SimpleOnGestureListener() {
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            if (CreatePostActivity.this.previewVideoFrame.isPlaying()) {
                CreatePostActivity.this.previewVideoFrame.pause();
                CreatePostActivity.this.previewVideoPlay.setVisibility(0);
            } else {
                CreatePostActivity.this.previewVideoFrame.start();
                CreatePostActivity.this.previewVideoPlay.setVisibility(8);
            }
            return true;
        }
    };
    @BindView(2131624161)
    View videoPlayBtn;
    @BindView(2131624179)
    View webSearchBtn;
    @BindView(2131624168)
    View youtubeLinkContainer;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Group m9054a(String str) {
        if (this.f6250h != null) {
            return this.f6250h.mo13845b(str);
        }
        return null;
    }

    /* renamed from: a */
    private void m9059a() {
        if (AppState.account == null || AppState.account.num_fb_friends + AppState.account.num_phone_friends < 1) {
            this.friendSwitch.setVisibility(8);
            this.friendMarker.setVisibility(8);
            return;
        }
        this.friendSwitch.setVisibility(0);
        this.friendMarker.setVisibility(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9060a(Bitmap bitmap, File file, int i) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        bitmap.compress(CompressFormat.PNG, i, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9064a(Group group) {
        if (group != null) {
            Log.d("CPA", "Choose group: " + group.group_name);
            this.f6246d = group.group_id;
            this.groupNameSelector.setVisibility(8);
            this.createGroupButton.setVisibility(8);
            findViewById(R.id.chosen_group_bubble).setVisibility(0);
            ((TextView) findViewById(R.id.chosen_group_name)).setText(group.group_name);
            this.postCaption.requestFocus();
        } else {
            this.f6246d = 0;
            findViewById(R.id.chosen_group_bubble).setVisibility(8);
            this.groupNameSelector.setVisibility(0);
            this.createGroupButton.setVisibility(0);
            this.groupNameSelector.setText("");
            this.groupNameSelector.requestFocus();
        }
        updatePostButton();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9065a(Post post, Map<String, String> map) {
        if (this.f6261s != null) {
            this.f6261s.unsubscribe();
        }
        this.f6261s = ApiService.m14297a().createPost(post, map).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b(m9084e());
        addToSubscriptionList(this.f6261s);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9066a(final Map<String, String> map) {
        if (map.containsKey("type") && ((String) map.get("type")).equals("video")) {
            map.put("type", "link");
        } else if (map.containsKey("type") && ((String) map.get("type")).equals("image")) {
            this.f6245b = (String) map.get("source_url");
            GossipApplication.f6095c.mo14650a(this.f6245b).mo14598j().mo14601a((Target) new BitmapImageViewTarget(this.postPhoto) {
                public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> rpVar) {
                    super.onResourceReady(bitmap, rpVar);
                    if (bitmap != null && bitmap.getWidth() > 0) {
                        CreatePostActivity.this.photoHelper = CreatePostActivity.this.getPhotoHelper();
                        CreatePostActivity.this.photoHelper.f10751d = bitmap.getWidth();
                        CreatePostActivity.this.photoHelper.f10752e = bitmap.getHeight();
                        CreatePostActivity.this.photoHelper.f10750c = CreatePostActivity.this.photoHelper.mo14330a(bitmap);
                        try {
                            CreatePostActivity.this.f6262t = Environment.getExternalStorageDirectory() + "/temp_link_img.png";
                            CreatePostActivity.this.m9060a(bitmap, new File(CreatePostActivity.this.f6262t), 100);
                            C2699jj.m14627b(CreatePostActivity.this.f6262t, new ExifInterface(CreatePostActivity.this.f6262t).getAttributeInt("Orientation", 1));
                        } catch (Exception e) {
                            Crashlytics.m16437a((Throwable) e);
                        }
                        CreatePostActivity.this.m9089g();
                    }
                }
            });
            this.postPhotoOuter.setVisibility(0);
            updatePostButton();
            return;
        }
        this.f6259q = map;
        this.linkContainer.setVisibility(0);
        if (!map.containsKey("youtube_video_id") || map.get("youtube_video_id") == null) {
            this.youtubeLinkContainer.setVisibility(8);
            this.nonYoutubeLinkContainer.setVisibility(0);
            ((TextView) this.linkContainer.findViewById(R.id.link_preview_title)).setText((CharSequence) this.f6259q.get("title"));
            String str = this.f6259q.containsKey("description") ? (String) this.f6259q.get("description") : "";
            TextView textView = (TextView) this.linkContainer.findViewById(R.id.link_preview_desc);
            if (str.length() > 0) {
                textView.setText(str);
                textView.setVisibility(0);
            } else {
                textView.setVisibility(8);
            }
            ((TextView) this.linkContainer.findViewById(R.id.link_preview_domain)).setText((CharSequence) this.f6259q.get("attribution_host"));
            String str2 = this.f6259q.containsKey("source_url") ? (String) this.f6259q.get("source_url") : null;
            ImageView imageView = (ImageView) this.linkContainer.findViewById(R.id.link_preview_image);
            if (str2 == null || str2.length() <= 0) {
                imageView.setVisibility(8);
            } else {
                GossipApplication.f6095c.mo14650a(str2).mo14540a().mo14554a(imageView);
                imageView.setVisibility(0);
            }
        } else {
            this.youtubeLinkContainer.setVisibility(0);
            this.nonYoutubeLinkContainer.setVisibility(8);
            YouTubePlayerFragment youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
            if (youTubePlayerFragment != null) {
                youTubePlayerFragment.mo12778a(getString(R.string.youtube_api_key), new C0945a() {
                    public void onInitializationFailure(C0946b bVar, YouTubeInitializationResult youTubeInitializationResult) {
                    }

                    public void onInitializationSuccess(C0946b bVar, awz awz, boolean z) {
                        awz.mo8141a((String) map.get("youtube_video_id"));
                        awz.mo8140a(0);
                    }
                });
            }
        }
        updatePostButton();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m9067a(boolean z) {
        this.f6248f.removeCallbacks(this.f6244a);
        this.tooltipView.setVisibility(0);
        if (z) {
            this.tooltipView.setText(getString(R.string.your_friends_can_see_this_post_as_usual));
        } else {
            this.tooltipView.setText(getString(R.string.your_friends_will_not_see_a_friend_marker));
        }
        this.f6248f.postDelayed(this.f6244a, 5000);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public Group m9071b(String str) {
        for (Group group : AppState.groups) {
            if (group.group_name.equals(str)) {
                return group;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m9073b() {
        if (AppState.groups == null) {
            ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<NetworkData>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    if (!CreatePostActivity.this.isStopped) {
                        Toast.makeText(CreatePostActivity.this, th.toString(), 1).show();
                    }
                }

                public void onNext(NetworkData networkData) {
                    if (!CreatePostActivity.this.isStopped) {
                        CreatePostActivity.this.m9073b();
                    }
                }
            });
            return;
        }
        this.f6250h = new GroupAutocompleteAdapter(this, R.layout.group_search_result, new ArrayList(AppState.groups));
        this.groupNameSelector.setThreshold(0);
        this.groupNameSelector.setAdapter(this.f6250h);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m9078c() {
        String[] split = this.postCaption.getText().toString().trim().split("\n");
        updatePostButton();
        if (split.length > 0) {
            String str = split[split.length - 1];
            if (str != null) {
                String[] split2 = str.split(" ");
                if (split2.length > 0) {
                    String str2 = split2[split2.length - 1];
                    if (str2 != null && StringUtils.m14594a(str2) && !str2.equals(this.f6249g)) {
                        this.f6249g = str2;
                        addToSubscriptionList(ApiService.m14297a().mo14142e(str2).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<Map<String, String>>() {
                            public void onCompleted() {
                            }

                            public void onError(Throwable th) {
                                th.printStackTrace();
                            }

                            public void onNext(Map<String, String> map) {
                                if (map != null) {
                                    CreatePostActivity.this.f6260r = true;
                                    CreatePostActivity.this.f6259q = map;
                                    CreatePostActivity.this.m9066a(CreatePostActivity.this.f6259q);
                                    CreatePostActivity.this.postCaption.setText("");
                                }
                            }
                        }));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m9082d() {
        if (this.f6246d == 0) {
            this.postSpinny.setVisibility(8);
            this.mRootView.setAlpha(1.0f);
            new Builder(this).setTitle("Please select a group").setPositiveButton(R.string.ok, null).create().show();
            return;
        }
        String trim = this.postCaption.getText().toString().trim();
        if (trim.length() <= 0 && this.f6245b == null && this.f6259q == null && this.videoHelper == null) {
            this.postSpinny.setVisibility(8);
            this.mRootView.setAlpha(1.0f);
            new Builder(this).setTitle("Blank posts are not allowed").setPositiveButton(R.string.ok, null).create().show();
            return;
        }
        Post post = new Post();
        post.group_id = this.f6246d;
        if (!this.f6247e) {
            post.friends_disabled = 1;
        }
        if (trim.length() > 0) {
            int length = this.postCaption.getText().toString().length();
            StringTrimResult trimWithCount = DataUtil.trimWithCount(this.postCaption.getText().toString());
            if (trimWithCount.string.length() > 0) {
                post.caption = trimWithCount.string;
            }
            if (!this.f6252j.isEmpty()) {
                String str = "";
                Iterator it = this.f6252j.iterator();
                while (it.hasNext()) {
                    TextTag textTag = (TextTag) it.next();
                    int i = (length - 1) - textTag.endIndex;
                    textTag.startIndex -= trimWithCount.trimmedStart;
                    textTag.endIndex -= trimWithCount.trimmedStart;
                    if (trimWithCount.trimmedEnd >= i) {
                        textTag.endIndex = trimWithCount.string.length();
                    }
                    str = str + textTag.toString() + ";";
                }
                post.mentioned_groups_info = str.substring(0, str.length() - 1);
            }
        }
        if (this.f6245b != null) {
            post.source_url = this.f6245b;
            if (this.photoHelper != null) {
                post.width = this.photoHelper.f10751d;
                post.height = this.photoHelper.f10752e;
                post.image_hash = this.photoHelper.f10750c;
                post.from_camera = this.photoHelper.f10754g;
            }
        }
        if (this.videoHelper != null) {
            post.source_url = this.videoHelper.f10888f;
            post.width = this.videoHelper.f10886d;
            post.height = this.videoHelper.f10887e;
            if (this.videoHelper.f10885c == 0) {
                post.from_camera = 1;
            } else {
                post.from_camera = 0;
            }
            post.link_url = this.videoHelper.f10889g;
        }
        m9065a(post, this.f6259q);
    }

    /* renamed from: e */
    private bjy<NetworkData> m9084e() {
        return new bjy<NetworkData>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                if (th instanceof CompositeException) {
                    for (Throwable th2 : ((CompositeException) th).mo16158a()) {
                        Log.d("CPA", th2.toString());
                    }
                }
                Crashlytics.m16437a(th);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        CreatePostActivity.this.postSpinny.setVisibility(8);
                        CreatePostActivity.this.mRootView.setAlpha(1.0f);
                    }
                }, 500);
            }

            public void onNext(NetworkData networkData) {
                if (networkData != null && networkData.success) {
                    final Post post = networkData.post;
                    if (networkData.server_message != null) {
                        final NetworkData networkData2 = networkData;
                        new Builder(CreatePostActivity.this).setTitle(networkData.server_message.title).setMessage(networkData.server_message.message).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ApiService.m14297a().mo14164l(networkData2.server_message.message_type).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                                RxBus.m14573a().mo14349a(new C2657n(post));
                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        CreatePostActivity.this.postSpinny.setVisibility(8);
                                        CreatePostActivity.this.mRootView.setAlpha(1.0f);
                                    }
                                }, 500);
                                Intent intent = new Intent();
                                intent.putExtra("json", DataUtil.toJson(post));
                                CreatePostActivity.this.setResult(-1, intent);
                                Intent intent2 = new Intent(CreatePostActivity.this.getBaseContext(), InviteContactsActivity.class);
                                intent2.putExtra("post", DataUtil.toJson(post));
                                CreatePostActivity.this.startActivity(intent2);
                                CreatePostActivity.this.finish();
                                CreatePostActivity.this.photoHelper = null;
                            }
                        }).create().show();
                    } else {
                        if (AppState.config.getInt("enable_related_posts", 1) == 1 && networkData.related_posts != null && !networkData.related_posts.isEmpty()) {
                            RxBus.m14573a().mo14349a(new C2624am(networkData.post.post_id, false, null));
                        }
                        if (CreatePostActivity.this.photoHelper != null) {
                            post.localBitmapPath = CreatePostActivity.this.photoHelper.f10748a;
                        }
                        RxBus.m14573a().mo14349a(new C2657n(post));
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                CreatePostActivity.this.postSpinny.setVisibility(8);
                                CreatePostActivity.this.mRootView.setAlpha(1.0f);
                            }
                        }, 500);
                        Intent intent = new Intent();
                        intent.putExtra("json", DataUtil.toJson(post));
                        CreatePostActivity.this.setResult(-1, intent);
                        if (networkData.display_url != null) {
                            Intent intent2 = new Intent(CreatePostActivity.this, WebViewActivity.class);
                            intent2.putExtra("title", "Suicide Hotline");
                            intent2.putExtra("url", networkData.display_url);
                            CreatePostActivity.this.startActivity(intent2);
                            CreatePostActivity.this.finish();
                            return;
                        }
                        if (networkData.post_moved == null || AppState.config.getInt("android_post_moved_enabled", 1) != 1) {
                            Intent intent3 = new Intent(CreatePostActivity.this.getBaseContext(), InviteContactsActivity.class);
                            intent3.putExtra("post", DataUtil.toJson(post));
                            intent3.putExtra("link_image_path", CreatePostActivity.this.f6262t);
                            CreatePostActivity.this.startActivity(intent3);
                            CreatePostActivity.this.finish();
                        } else {
                            CreatePostActivity.this.mIsShowingPopup = true;
                            new BlurTask(CreatePostActivity.this, CreatePostActivity.this.findViewById(16908290), BadgeType.POST_MOVED, networkData.post_moved, null).execute(new Void[0]);
                            ApiService.m14297a().mo14128c(post.group_id).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                            CreatePostActivity.this.finish();
                        }
                        CreatePostActivity.this.photoHelper = null;
                    }
                    ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                } else if (networkData.same_link_found == 1) {
                    String[] strArr = {"Join The Conversation", "Create A New Post"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreatePostActivity.this);
                    TextView textView = new TextView(CreatePostActivity.this.getApplicationContext());
                    textView.setText(networkData.error);
                    textView.setTextSize(1, 16.0f);
                    textView.setTypeface(Typeface.defaultFromStyle(1));
                    textView.setTextColor(CreatePostActivity.this.getResources().getColor(17170444));
                    textView.setGravity(17);
                    int a = C2699jj.m14599a(5, CreatePostActivity.this.getApplicationContext());
                    ArrayAdapter arrayAdapter = new ArrayAdapter(CreatePostActivity.this.getApplicationContext(), R.layout.simple_blue_text, strArr);
                    textView.setPadding(a, a, a, 0);
                    final NetworkData networkData3 = networkData;
                    builder.setCustomTitle(textView).setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    dialogInterface.dismiss();
                                    Intent intent = new Intent(CreatePostActivity.this, PostDetailsActivity.class);
                                    intent.putExtra("post_id", networkData3.post.post_id);
                                    CreatePostActivity.this.startActivity(intent);
                                    CreatePostActivity.this.finish();
                                    return;
                                case 1:
                                    dialogInterface.dismiss();
                                    HashMap hashMap = new HashMap();
                                    hashMap.putAll(CreatePostActivity.this.f6259q);
                                    hashMap.put("insist_posting", "1");
                                    networkData3.post.caption = CreatePostActivity.this.postCaption.getText().toString();
                                    CreatePostActivity.this.m9065a(networkData3.post, (Map<String, String>) hashMap);
                                    return;
                                default:
                                    return;
                            }
                        }
                    });
                    AlertDialog create = builder.create();
                    create.setCanceledOnTouchOutside(false);
                    create.setCancelable(false);
                    create.show();
                } else if (networkData.low_quality) {
                    CreatePostActivity.this.postSpinny.setVisibility(8);
                    CreatePostActivity.this.mRootView.setAlpha(1.0f);
                    CreatePostActivity.this.mIsShowingPopup = true;
                    new BlurTask((Activity) CreatePostActivity.this, CreatePostActivity.this.findViewById(16908290), BadgeType.POST_QUALITY_FEEDBACK).execute(new Void[0]);
                } else {
                    Toast.makeText(CreatePostActivity.this, networkData.error, 1).show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            CreatePostActivity.this.postSpinny.setVisibility(8);
                            CreatePostActivity.this.mRootView.setAlpha(1.0f);
                        }
                    }, 500);
                }
            }
        };
    }

    /* renamed from: f */
    private void m9087f() {
        if (this.videoHelper != null && this.videoHelper.f10883a != null && this.videoHelper.f10884b != null) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(this.videoHelper.f10883a.getPath());
            String extractMetadata = mediaMetadataRetriever.extractMetadata(19);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(18);
            String extractMetadata3 = mediaMetadataRetriever.extractMetadata(24);
            if (!(extractMetadata == null || extractMetadata2 == null || extractMetadata3 == null)) {
                int parseInt = Integer.parseInt(extractMetadata);
                int parseInt2 = Integer.parseInt(extractMetadata2);
                this.previewVideoContainer.setVisibility(0);
                this.previewVideoPlay.setVisibility(0);
                LayoutParams layoutParams = (LayoutParams) this.previewVideoFrame.getLayoutParams();
                Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                layoutParams.width = point.x - (C2699jj.m14599a(5, (Context) this) * 2);
                layoutParams.height = (layoutParams.width * parseInt2) / parseInt;
                this.previewVideoFrame.setLayoutParams(layoutParams);
                this.previewVideoFrame.setVideoURI(this.videoHelper.f10883a);
                this.previewVideoFrame.seekTo(100);
                final GestureDetector gestureDetector = new GestureDetector(this, this.f6263u);
                this.previewVideoFrame.setOnTouchListener(new OnTouchListener() {
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        gestureDetector.onTouchEvent(motionEvent);
                        return true;
                    }
                });
                this.previewVideoFrame.setOnCompletionListener(new OnCompletionListener() {
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        CreatePostActivity.this.previewVideoPlay.setVisibility(0);
                    }
                });
            }
            updatePostButton();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m9089g() {
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.postPhoto.getLayoutParams();
        layoutParams.width = point.x;
        layoutParams.height = (this.photoHelper.f10752e * layoutParams.width) / this.photoHelper.f10751d;
        this.postPhoto.setLayoutParams(layoutParams);
    }

    /* renamed from: h */
    private void m9091h() {
        this.camBtn.setAlpha(0.3f);
        this.camBtn.setEnabled(false);
        this.linkBtn.setAlpha(0.3f);
        this.linkBtn.setEnabled(false);
        this.webSearchBtn.setAlpha(0.3f);
        this.webSearchBtn.setEnabled(false);
        this.gifSearchBtn.setAlpha(0.3f);
        this.gifSearchBtn.setEnabled(false);
        this.videoPlayBtn.setAlpha(0.3f);
        this.videoPlayBtn.setEnabled(false);
    }

    /* renamed from: i */
    private void m9092i() {
        this.camBtn.setAlpha(1.0f);
        this.camBtn.setEnabled(true);
        this.linkBtn.setAlpha(1.0f);
        this.linkBtn.setEnabled(true);
        this.webSearchBtn.setAlpha(1.0f);
        this.webSearchBtn.setEnabled(true);
        this.gifSearchBtn.setAlpha(1.0f);
        this.gifSearchBtn.setEnabled(true);
        this.videoPlayBtn.setAlpha(1.0f);
        this.videoPlayBtn.setEnabled(true);
    }

    public static Intent startCreatePostActivityIntent(Context context, int i) {
        Intent intent = new Intent(context, CreatePostActivity.class);
        intent.putExtra("group_id", i);
        return intent;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo9883a(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.TEXT");
        if (stringExtra != null) {
            this.f6260r = false;
            this.postCaption.setText(stringExtra);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo9884b(Intent intent) {
        Uri uri = (Uri) intent.getParcelableExtra("android.intent.extra.STREAM");
        try {
            if (this.photoHelper == null) {
                this.photoHelper = getPhotoHelper();
            }
            File a = this.photoHelper.mo14329a(uri);
            this.photoHelper.mo14335a(a.getAbsolutePath(), Uri.fromFile(a), 902);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backNavClick(View view) {
        onBackPressed();
    }

    public void checkAndUploadVideo() {
        if (this.videoHelper != null && this.videoHelper.f10883a != null && this.videoHelper.f10884b != null) {
            this.videoHelper.mo14422d();
        }
    }

    @OnClick({2131624180})
    public void chooseGif() {
        clearPhoto(null);
        clearLink(null);
        startActivityForResult(new Intent(this, GifSearchActivity.class), 307);
    }

    @OnClick({2131624178})
    public void chooseLink(ImageView imageView) {
        Log.d("CPA", "add a link");
        startActivityForResult(new Intent(this, AddLinkActivity.class), ADD_LINK_REQUEST);
    }

    public void choosePhoto() {
        getPhotoHelper().choosePhoto();
    }

    @OnClick({2131624152})
    public void clearChosenGroup(View view) {
        m9064a((Group) null);
    }

    @OnClick({2131624170})
    public void clearLink(ImageView imageView) {
        m9092i();
        this.linkContainer.setVisibility(8);
        this.f6259q = null;
        updatePostButton();
    }

    public void clearPhoto(View view) {
        this.f6245b = null;
        this.photoHelper = null;
        this.videoHelper = null;
        this.postPhoto.setImageBitmap(null);
        this.postPhotoOuter.setVisibility(8);
        this.videoPlayBtn.setVisibility(8);
        this.previewVideoContainer.setVisibility(8);
        m9092i();
        updatePostButton();
    }

    public void createGroupClick(View view) {
        Log.d("CPA", "Create group?");
        Intent intent = new Intent(this, CreateGroupActivity.class);
        intent.putExtra("for_post", true);
        startActivityForResult(intent, 1001);
    }

    public void hideCurrentScreen() {
        if (this.mRootView != null) {
            this.mRootView.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void imageCaptured() {
        this.photoHelper.mo14337b();
    }

    /* access modifiers changed from: protected */
    public void keyboardOpened() {
        super.keyboardOpened();
        this.groupNameSelector.setDropDownHeight(this.postContent.getHeight());
        if (this.groupNameSelector.isFocused()) {
            this.groupNameSelector.showDropDown();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1001) {
            if (i2 == -1) {
                m9064a((Group) intent.getSerializableExtra("group"));
            }
        } else if (i == 1002) {
            if (intent != null) {
                if (intent.hasExtra("link_info")) {
                    try {
                        m9066a((Map) intent.getSerializableExtra("link_info"));
                    } catch (ClassCastException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (i == 1003 || i == 1004) {
            m9059a();
            this.groupNameSelector.requestFocus();
            openKeyboard();
        } else if (i == 801) {
            m9087f();
        } else if (i2 == 307) {
            String stringExtra = intent.getStringExtra("gif_url");
            int intExtra = intent.getIntExtra("gif_width", 0);
            int intExtra2 = intent.getIntExtra("gif_height", 0);
            this.postPhotoOuter.setVisibility(0);
            this.postPhoto.setVisibility(0);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.postPhoto.getLayoutParams();
            layoutParams.width = (layoutParams.height * 16) / 9;
            Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            layoutParams.width = point.x;
            layoutParams.height = (layoutParams.width * intExtra2) / intExtra;
            this.postPhoto.setLayoutParams(layoutParams);
            this.postPhoto.requestLayout();
            this.photoHelper = getPhotoHelper();
            this.photoHelper.f10754g = 0;
            this.photoHelper.f10751d = intExtra;
            this.photoHelper.f10752e = intExtra2;
            this.postButton.setSelected(false);
            this.postButton.setClickable(false);
            m9091h();
            if (AppState.config.getInt("android_enable_image_progress") == 1) {
                ImageLoadingUtil.m14520a(this.imageProgress, this.imageProgressContainer, (Context) this, layoutParams.width, layoutParams.height, 80, stringExtra, this.postPhoto, (View) null, 0, false, (ImageView) null, 0);
                return;
            }
            GossipApplication.f6095c.mo14650a(stringExtra).mo14599k().mo14572d(17301612).mo14569c((int) R.mipmap.ic_launcher).mo14629c().mo14559b(DiskCacheStrategy.SOURCE).mo14565b((RequestListener<? super ModelType, GifDrawable>) new RequestListener<String, GifDrawable>() {
                public boolean onException(Exception exc, String str, Target<GifDrawable> sdVar, boolean z) {
                    return false;
                }

                public boolean onResourceReady(GifDrawable psVar, String str, Target<GifDrawable> sdVar, boolean z, boolean z2) {
                    byte[] d = psVar.mo15890d();
                    Bitmap b = psVar.mo15887b();
                    try {
                        CreatePostActivity.this.f6262t = Environment.getExternalStorageDirectory() + "/temp_link_img.png";
                        CreatePostActivity.this.m9060a(b, new File(CreatePostActivity.this.f6262t), 100);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    CreatePostActivity.this.photoHelper.mo14336a(d);
                    CreatePostActivity.this.updatePostButton();
                    return false;
                }
            }).mo14554a(this.postPhoto);
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("json", "{\"cancelled\": 1}");
        setResult(0, intent);
        super.onBackPressed();
    }

    public void onClick(View view) {
        if (view.getId() == R.id.cam_button) {
            clearLink(null);
            choosePhoto();
        } else if (view.getId() == R.id.web_search_button) {
            Intent intent = new Intent(this, LinkSearchActivity.class);
            clearPhoto(null);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f6246d = 0;
        setContentView((int) R.layout.activity_create_post);
        ButterKnife.bind((Activity) this);
        this.f6118c = findViewById(R.id.create_post_root_relative);
        enableKeyboardEvents(this.mRootView);
        this.camBtn.setOnClickListener(this);
        this.webSearchBtn.setOnClickListener(this);
        m9073b();
        this.groupNameSelector.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (adapterView != null) {
                    CreatePostActivity.this.m9064a(CreatePostActivity.this.m9071b(((Group) adapterView.getItemAtPosition(i)).group_name));
                }
            }
        });
        this.groupNameSelector.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (view.getWindowVisibility() == 0) {
                    if (z) {
                        CreatePostActivity.this.groupNameSelector.showDropDown();
                    } else {
                        CreatePostActivity.this.groupNameSelector.dismissDropDown();
                    }
                }
            }
        });
        this.groupNameSelector.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CreatePostActivity.this.groupNameSelector.showDropDown();
            }
        });
        this.groupNameSelector.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                Log.d("CPA", "group action: " + i);
                if (i != 5 && (keyEvent == null || keyEvent.getAction() != 1 || keyEvent.getKeyCode() != 66)) {
                    return false;
                }
                Group b = CreatePostActivity.this.m9054a(CreatePostActivity.this.groupNameSelector.getText().toString());
                if (b == null) {
                    return true;
                }
                CreatePostActivity.this.m9064a(b);
                return true;
            }
        });
        this.groupNameSelector.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                String obj = editable.toString();
                if (CreatePostActivity.this.f6250h != null) {
                    ArrayList a = CreatePostActivity.this.f6250h.mo13844a(obj);
                    if (a.size() == 1) {
                        Group group = (Group) a.get(0);
                        if (group != null) {
                            CreatePostActivity.this.m9064a(group);
                        }
                    }
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        float f = getResources().getDisplayMetrics().density;
        this.groupNameSelector.setDropDownHeight((int) (140.0f * f));
        this.groupNameSelector.setDropDownVerticalOffset((int) (-11.0f * f));
        if (getIntent().hasExtra("group_id")) {
            int intExtra = getIntent().getIntExtra("group_id", 0);
            try {
                for (Group group : AppState.groups) {
                    if (group.group_id == intExtra) {
                        m9064a(group);
                    }
                }
            } catch (NullPointerException e) {
                Crashlytics.m16437a((Throwable) new Exception("groups ARE NULL: " + AppState.groups));
            }
        }
        this.f6251i = AppState.config.getInt("disable_group_tagging", 0);
        this.f6252j = new ArrayList<>();
        if (AppState.groups == null) {
            AppState.groups = new ArrayList();
        }
        this.f6254l = new GroupAutocompleteAdapter(this, R.layout.group_search_result, new ArrayList(AppState.groups));
        this.f6253k = new ListPopupWindow(this);
        this.f6253k.setAdapter(this.f6254l);
        this.f6253k.setAnchorView(this.postCaption);
        this.f6253k.setAnimationStyle(0);
        this.f6253k.setHeight(-2);
        this.f6253k.setWidth(C2699jj.m14599a(150, (Context) this));
        this.f6253k.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Group group = (Group) adapterView.getItemAtPosition(i);
                String[] split = CreatePostActivity.this.postCaption.getText().toString().split("(?=#)");
                int selectionStart = CreatePostActivity.this.postCaption.getSelectionStart();
                int i2 = 0;
                int i3 = 0;
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("");
                ArrayList arrayList = new ArrayList();
                int i4 = 0;
                int length = split.length;
                for (int i5 = 0; i5 < length; i5++) {
                    String str = split[i5];
                    int i6 = i2;
                    i2 += str.length();
                    if (i6 >= selectionStart || selectionStart > i2) {
                        spannableStringBuilder.append(str);
                    } else {
                        int length2 = spannableStringBuilder.length();
                        spannableStringBuilder.append("#" + group.group_name);
                        i4 = (group.group_name.length() + 1) - (selectionStart - length2);
                        i3 = spannableStringBuilder.length();
                        spannableStringBuilder.append(" ");
                        spannableStringBuilder.append(str.substring(selectionStart - i6));
                        TextTag textTag = new TextTag(length2, i3, group.group_id);
                        arrayList.add(textTag);
                    }
                }
                Iterator it = CreatePostActivity.this.f6252j.iterator();
                while (it.hasNext()) {
                    TextTag textTag2 = (TextTag) it.next();
                    if (selectionStart <= textTag2.startIndex || selectionStart > textTag2.endIndex) {
                        if (textTag2.startIndex > selectionStart) {
                            textTag2.startIndex += i4;
                            textTag2.endIndex += i4;
                        }
                        arrayList.add(textTag2);
                    }
                }
                CreatePostActivity.this.f6252j = arrayList;
                Iterator it2 = CreatePostActivity.this.f6252j.iterator();
                while (it2.hasNext()) {
                    TextTag textTag3 = (TextTag) it2.next();
                    if (textTag3.startIndex > -1 && textTag3.endIndex > -1) {
                        spannableStringBuilder.setSpan(C2699jj.m14606a(), textTag3.startIndex, textTag3.endIndex, 33);
                    }
                }
                CreatePostActivity.this.f6255m = spannableStringBuilder;
                CreatePostActivity.this.postCaption.setText(spannableStringBuilder);
                CreatePostActivity.this.postCaption.setSelection(i3 + 1);
                CreatePostActivity.this.f6253k.dismiss();
            }
        });
        this.postCaption.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z && CreatePostActivity.this.f6251i == 0) {
                    C2699jj.m14620a((CharSequence) CreatePostActivity.this.postCaption.getText(), CreatePostActivity.this.postCaption, CreatePostActivity.this.f6253k, CreatePostActivity.this.f6254l);
                }
            }
        });
        this.postCaption.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (CreatePostActivity.this.f6251i == 0 && !editable.toString().equals(CreatePostActivity.this.f6255m.toString())) {
                    CreatePostActivity.this.f6255m = editable.toString();
                    C2699jj.m14620a((CharSequence) editable, CreatePostActivity.this.postCaption, CreatePostActivity.this.f6253k, CreatePostActivity.this.f6254l);
                    if (CreatePostActivity.this.f6258p) {
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(editable.toString());
                        Iterator it = CreatePostActivity.this.f6252j.iterator();
                        while (it.hasNext()) {
                            TextTag textTag = (TextTag) it.next();
                            if (textTag.startIndex > -1 && textTag.endIndex > -1) {
                                spannableStringBuilder.setSpan(C2699jj.m14606a(), textTag.startIndex, textTag.endIndex, 33);
                            }
                        }
                        CreatePostActivity.this.f6258p = false;
                        CreatePostActivity.this.f6255m = spannableStringBuilder.toString();
                        int selectionStart = CreatePostActivity.this.postCaption.getSelectionStart();
                        CreatePostActivity.this.postCaption.setText(spannableStringBuilder);
                        CreatePostActivity.this.postCaption.setSelection(selectionStart);
                    }
                }
                if (!CreatePostActivity.this.f6260r) {
                    CreatePostActivity.this.m9078c();
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                CreatePostActivity.this.f6256n = CreatePostActivity.this.postCaption.getSelectionStart();
                CreatePostActivity.this.f6257o = CreatePostActivity.this.postCaption.getSelectionEnd();
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (CreatePostActivity.this.f6251i == 0 && !charSequence.toString().equals(CreatePostActivity.this.f6255m.toString())) {
                    int selectionStart = CreatePostActivity.this.postCaption.getSelectionStart();
                    ArrayList arrayList = new ArrayList();
                    Iterator it = CreatePostActivity.this.f6252j.iterator();
                    while (it.hasNext()) {
                        TextTag textTag = (TextTag) it.next();
                        if (CreatePostActivity.this.f6256n <= textTag.startIndex && CreatePostActivity.this.f6257o <= textTag.startIndex) {
                            textTag.startIndex += i3 - i2;
                            textTag.endIndex += i3 - i2;
                            arrayList.add(textTag);
                            CreatePostActivity.this.f6258p = true;
                        } else if (CreatePostActivity.this.f6256n <= textTag.startIndex && CreatePostActivity.this.f6257o > textTag.startIndex) {
                            CreatePostActivity.this.f6258p = true;
                        } else if (CreatePostActivity.this.f6256n > textTag.startIndex && CreatePostActivity.this.f6256n <= textTag.endIndex) {
                            CreatePostActivity.this.f6258p = true;
                        } else if (selectionStart > textTag.endIndex) {
                            arrayList.add(textTag);
                        } else {
                            CreatePostActivity.this.f6258p = true;
                        }
                    }
                    CreatePostActivity.this.f6252j = arrayList;
                }
            }
        });
        m9059a();
        this.friendMarker.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CreatePostActivity.this.friendSwitch.toggle();
            }
        });
        this.friendSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                CreatePostActivity.this.f6247e = z;
                CreatePostActivity.this.m9067a(CreatePostActivity.this.f6247e);
            }
        });
        this.f6248f = new Handler(getMainLooper());
        this.mRootView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CreatePostActivity.this.enableKeyboardEvents(CreatePostActivity.this.mRootView);
            }
        });
        if (!AppState.hasPosted) {
            this.friendSwitch.setVisibility(0);
            this.friendMarker.setVisibility(0);
            startActivityForResult(new Intent(this, TutorialPostActivity.class), TUTORIAL_POST);
        } else if (!AppState.hasLinkSearched) {
            startActivityForResult(new Intent(this, TutorialLinkSearchActivity.class), TUTORIAL_LINK_SEARCH);
        }
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        addToSubscriptionList(RxBus.m14573a().mo14348a(C2614ac.class, (bjy<T>) new bjy<C2614ac>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2614ac acVar) {
                if (CreatePostActivity.this.photoHelper != null && CreatePostActivity.this.photoHelper.f10753f != null && CreatePostActivity.this.photoHelper.f10749b != null && CreatePostActivity.this.photoHelper.f10748a != null) {
                    CreatePostActivity.this.postPhoto.setImageBitmap(CreatePostActivity.this.photoHelper.f10753f);
                    if (CreatePostActivity.this.photoHelper.f10751d > 0 && CreatePostActivity.this.photoHelper.f10752e > 0) {
                        CreatePostActivity.this.m9089g();
                    }
                    CreatePostActivity.this.postPhotoOuter.setVisibility(0);
                    CreatePostActivity.this.imageCaptured();
                    CreatePostActivity.this.updatePostButton();
                }
            }
        }));
        busSubscribe(C2615ad.class, new bjy<C2615ad>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2615ad adVar) {
                String str = adVar.f10583a;
                CreatePostActivity.this.f6260r = false;
                CreatePostActivity.this.postCaption.setText(str);
            }
        });
        busSubscribe(C2613ab.class, new bjy<C2613ab>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2613ab abVar) {
                if (CreatePostActivity.this.postPhoto != null && CreatePostActivity.this.postPhoto.getDrawable() != null && (CreatePostActivity.this.postPhoto.getDrawable() instanceof GifDrawable)) {
                    GifDrawable psVar = (GifDrawable) CreatePostActivity.this.postPhoto.getDrawable();
                    byte[] d = psVar.mo15890d();
                    Bitmap b = psVar.mo15887b();
                    if (d != null && b != null) {
                        try {
                            CreatePostActivity.this.f6262t = Environment.getExternalStorageDirectory() + "/temp_gif_img.png";
                            CreatePostActivity.this.m9060a(b, new File(CreatePostActivity.this.f6262t), 100);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        CreatePostActivity.this.photoHelper = CreatePostActivity.this.getPhotoHelper();
                        CreatePostActivity.this.photoHelper.mo14336a(d);
                        CreatePostActivity.this.updatePostButton();
                    }
                }
            }
        });
        busSubscribe(C2653j.class, new bjy<C2653j>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
            }

            public void onNext(C2653j jVar) {
                if (CreatePostActivity.this.videoHelper != null && jVar != null) {
                    CreatePostActivity.this.videoHelper.f10888f = jVar.f10644c;
                    CreatePostActivity.this.videoHelper.f10889g = jVar.f10642a;
                    CreatePostActivity.this.videoHelper.f10886d = jVar.f10645d;
                    CreatePostActivity.this.videoHelper.f10887e = jVar.f10646e;
                    CreatePostActivity.this.videoHelper.f10885c = jVar.f10643b;
                    CreatePostActivity.this.updatePostButton();
                    CreatePostActivity.this.onVideoUploadSuccess();
                    CreatePostActivity.this.m9082d();
                }
            }
        });
        if ("android.intent.action.SEND".equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                mo9883a(intent);
            } else if (type.startsWith("image/")) {
                mo9884b(intent);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onVideoCompressionStarted() {
    }

    public void onVideoPostUploaded(BaseVideoMessage baseVideoMessage) {
    }

    public void onVideoUploadFailed() {
        showSnack(false);
    }

    public void onVideoUploadSuccess() {
        showSnack(true);
    }

    public void onVideoUploading() {
        View findViewById = findViewById(R.id.create_post_buttons);
        if (findViewById != null) {
            Snackbar.m3350a(findViewById, "Video uploading ...", -2).mo1721a();
        }
    }

    @OnClick({2131624150})
    public void savePost(View view) {
        this.mRootView.setAlpha(0.3f);
        this.postSpinny.setVisibility(0);
        if (this.photoHelper != null && this.photoHelper.f10749b != null && this.photoHelper.f10748a != null) {
            ApiService.m14297a().mo14129c(this.photoHelper.f10749b, this.photoHelper.f10748a).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<UploadMediaResponse>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    CreatePostActivity.this.postSpinny.setVisibility(8);
                    CreatePostActivity.this.mRootView.setAlpha(1.0f);
                    Toast.makeText(CreatePostActivity.this, CreatePostActivity.this.getString(R.string.unable_to_upload_image), 0).show();
                }

                public void onNext(UploadMediaResponse uploadMediaResponse) {
                    if (uploadMediaResponse != null) {
                        CreatePostActivity.this.f6245b = uploadMediaResponse.full_url;
                        CreatePostActivity.this.m9082d();
                    }
                }
            });
        } else if (this.photoHelper != null && this.photoHelper.f10758k != null) {
            ApiService.m14297a().mo14112a(this.photoHelper.f10758k).mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new bjy<UploadMediaResponse>() {
                public void onCompleted() {
                }

                public void onError(Throwable th) {
                    Crashlytics.m16437a(th);
                    Toast.makeText(CreatePostActivity.this, CreatePostActivity.this.getString(R.string.unable_to_upload_image), 0).show();
                    CreatePostActivity.this.postSpinny.setVisibility(8);
                    CreatePostActivity.this.mRootView.setAlpha(1.0f);
                }

                public void onNext(UploadMediaResponse uploadMediaResponse) {
                    if (uploadMediaResponse != null) {
                        CreatePostActivity.this.f6245b = uploadMediaResponse.full_url;
                        CreatePostActivity.this.m9082d();
                    }
                }
            });
        } else if (this.videoHelper == null || this.videoHelper.f10883a == null || this.videoHelper.f10884b == null) {
            m9082d();
        } else {
            this.videoHelper.mo14422d();
        }
    }

    public void showCurrentScreen() {
        if (this.mRootView != null) {
            this.mRootView.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void showSnack(boolean z) {
        super.showSnack(z);
        View findViewById = findViewById(R.id.create_post_buttons);
        if (findViewById == null) {
            return;
        }
        if (!z) {
            Snackbar.m3350a(findViewById, "Video failed to upload, please try again!", 0).mo1721a();
        } else {
            Snackbar.m3350a(findViewById, "Video uploaded!", -1).mo1721a();
        }
    }

    @OnClick({2131624177})
    public void takeVideo() {
        if (getVideoHelper() != null) {
            getVideoHelper().mo14412a();
        }
        VideoHelper jtVar = new VideoHelper(this);
        jtVar.mo14417a((C2716c) this);
        jtVar.mo14420c();
    }

    public void updatePostButton() {
        String trim = this.postCaption.getText().toString().trim();
        if ((this.f6246d == 0 || (trim.length() <= 0 && this.f6245b == null && this.f6259q == null && (this.photoHelper == null || this.photoHelper.f10753f == null))) && ((this.photoHelper == null || this.photoHelper.f10758k == null) && (this.videoHelper == null || this.videoHelper.f10883a == null))) {
            this.postButton.setSelected(false);
            this.postButton.setClickable(false);
        } else {
            this.postButton.setSelected(true);
            this.postButton.setClickable(true);
        }
        if (this.f6245b != null || this.f6259q != null || ((this.photoHelper != null && this.photoHelper.f10753f != null) || (this.photoHelper != null && this.photoHelper.f10758k != null))) {
            m9091h();
        }
    }
}
