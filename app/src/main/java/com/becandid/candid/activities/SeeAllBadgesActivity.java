package com.becandid.candid.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.p003v7.widget.GridLayoutManager;
import android.support.p003v7.widget.RecyclerView;
import android.support.p003v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.data.AppState;
import com.becandid.candid.views.BadgeInfoView;

public class SeeAllBadgesActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public BadgeInfoView f6660a;
    @BindView(2131624350)
    FrameLayout mBadgeInfoContainer;
    @BindView(2131624349)
    RecyclerView mRecyclerView;

    public void onBackPressed() {
        super.onBackPressed();
        this.mRecyclerView.setVisibility(0);
        this.mBadgeInfoContainer.removeView(this.f6660a);
        this.mBadgeInfoContainer.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_see_all_badges);
        ButterKnife.bind((Activity) this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator((int) R.drawable.back_chevron);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        this.mRecyclerView.setHasFixedSize(true);
        this.mRecyclerView.setLayoutManager(gridLayoutManager);
        this.mRecyclerView.setAdapter(new AllBadgesAdapter(this));
        addToSubscriptionList(RxBus.m14573a().mo14348a(C2646c.class, (bjy<T>) new bjy<C2646c>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2646c cVar) {
                String str = cVar.f10633a;
                SeeAllBadgesActivity.this.f6660a = new BadgeInfoView(SeeAllBadgesActivity.this.getBaseContext(), cVar.f10634b, false, str);
                SeeAllBadgesActivity.this.mBadgeInfoContainer.setVisibility(0);
                SeeAllBadgesActivity.this.mRecyclerView.setVisibility(8);
                SeeAllBadgesActivity.this.mBadgeInfoContainer.addView(SeeAllBadgesActivity.this.f6660a);
                Animation loadAnimation = AnimationUtils.loadAnimation(SeeAllBadgesActivity.this.getBaseContext(), R.anim.slide_up_animation);
                loadAnimation.setDuration(500);
                final View findViewById = SeeAllBadgesActivity.this.mBadgeInfoContainer.findViewById(R.id.badge_info);
                findViewById.startAnimation(loadAnimation);
                SeeAllBadgesActivity.this.mBadgeInfoContainer.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        Animation loadAnimation = AnimationUtils.loadAnimation(SeeAllBadgesActivity.this.getBaseContext(), R.anim.slide_down_animation);
                        loadAnimation.setDuration(500);
                        findViewById.startAnimation(loadAnimation);
                        view.postDelayed(new Runnable() {
                            public void run() {
                                SeeAllBadgesActivity.this.mRecyclerView.setVisibility(0);
                                SeeAllBadgesActivity.this.mBadgeInfoContainer.removeView(SeeAllBadgesActivity.this.f6660a);
                                SeeAllBadgesActivity.this.mBadgeInfoContainer.setVisibility(8);
                            }
                        }, 500);
                    }
                });
                AppState.blurTaskCalledOnFlight = false;
            }
        }));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
