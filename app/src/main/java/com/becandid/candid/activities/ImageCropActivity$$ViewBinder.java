package com.becandid.candid.activities;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.ImageCropActivity;
import com.becandid.thirdparty.nocropper.CropperView;

public class ImageCropActivity$$ViewBinder<T extends ImageCropActivity> implements ViewBinder<T> {

    /* compiled from: ImageCropActivity$$ViewBinder */
    public static class InnerUnbinder<T extends ImageCropActivity> implements Unbinder {

        /* renamed from: a */
        private T f6390a;

        protected InnerUnbinder(T t) {
            this.f6390a = t;
        }

        public final void unbind() {
            if (this.f6390a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6390a);
            this.f6390a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.mImageView = null;
            t.mCropButton = null;
            t.mProgressBar = null;
            t.mCropContainer = null;
            t.mCropperFrameLayout = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.mImageView = (CropperView) finder.castView((View) finder.findRequiredView(obj, R.id.cropper_view, "field 'mImageView'"), R.id.cropper_view, "field 'mImageView'");
        t.mCropButton = (ImageView) finder.castView((View) finder.findRequiredView(obj, R.id.crop_button, "field 'mCropButton'"), R.id.crop_button, "field 'mCropButton'");
        t.mProgressBar = (ProgressBar) finder.castView((View) finder.findRequiredView(obj, R.id.progress_bar, "field 'mProgressBar'"), R.id.progress_bar, "field 'mProgressBar'");
        t.mCropContainer = (RelativeLayout) finder.castView((View) finder.findRequiredView(obj, R.id.crop_view_container, "field 'mCropContainer'"), R.id.crop_view_container, "field 'mCropContainer'");
        t.mCropperFrameLayout = (FrameLayout) finder.castView((View) finder.findRequiredView(obj, R.id.cropper_frame_layout, "field 'mCropperFrameLayout'"), R.id.cropper_frame_layout, "field 'mCropperFrameLayout'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
