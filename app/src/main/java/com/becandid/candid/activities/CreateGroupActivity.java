package com.becandid.candid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p003v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.becandid.candid.R;
import com.becandid.candid.fragments.GroupInfoFragment;
import com.becandid.candid.fragments.GroupNameFragment;
import com.becandid.candid.fragments.GroupTagsFragment;
import java.util.HashSet;
import java.util.Set;

public class CreateGroupActivity extends BaseActivity {

    /* renamed from: a */
    private Set<String> f6234a;

    /* renamed from: b */
    private String f6235b;

    /* renamed from: d */
    private String f6236d;

    /* renamed from: e */
    private String f6237e;

    /* renamed from: f */
    private boolean f6238f = true;

    /* renamed from: g */
    private boolean f6239g = false;

    /* renamed from: h */
    private Bundle f6240h;
    @BindView(2131624145)
    FrameLayout mPlaceholder;

    public boolean addTag(String str) {
        if (this.f6234a == null) {
            this.f6234a = new HashSet();
        }
        if (this.f6234a.contains(str.toLowerCase())) {
            this.f6234a.remove(str.toLowerCase());
            return true;
        } else if (this.f6234a.size() >= 3) {
            return false;
        } else {
            this.f6234a.add(str);
            return true;
        }
    }

    public String getGroupTags() {
        StringBuilder sb = new StringBuilder();
        for (String append : this.f6234a) {
            sb.append(append);
            sb.append(',');
        }
        return sb.toString();
    }

    public String getmGroupAbout() {
        return this.f6236d;
    }

    public String getmGroupName() {
        return this.f6235b;
    }

    public String getmGroupSourceUrl() {
        return this.f6237e;
    }

    public boolean isGroupPhotoSkipped() {
        return this.f6238f;
    }

    public boolean isTagsEmpty() {
        return this.f6234a.size() == 0;
    }

    public boolean ismGroupPhotoFailedToUpload() {
        return this.f6239g;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment findFragmentById = getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (findFragmentById != null && (findFragmentById instanceof GroupInfoFragment)) {
            setGroupPhotoSkipped(false);
            ((GroupInfoFragment) findFragmentById).mo10379a();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_create_group);
        ButterKnife.bind((Activity) this);
        this.f6240h = getIntent().getExtras();
        if (this.f6240h == null) {
            this.f6240h = new Bundle();
        }
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (this.f6240h.containsKey("edit_group")) {
            ((TextView) findViewById(R.id.toolbar_title)).setText("Edit Group");
        }
        if (this.mPlaceholder != null) {
            if (bundle == null) {
                GroupNameFragment groupNameFragment = new GroupNameFragment();
                groupNameFragment.setArguments(this.f6240h);
                getSupportFragmentManager().beginTransaction().add((int) R.id.placeholder, (Fragment) groupNameFragment).commit();
            } else {
                return;
            }
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator((int) R.drawable.back_chevron);
        busSubscribe(C2639ba.class, new bjy<C2639ba>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2639ba baVar) {
                CreateGroupActivity.this.setGroupSourceUrl(baVar.f10626a);
                Fragment findFragmentById = CreateGroupActivity.this.getSupportFragmentManager().findFragmentById(R.id.placeholder);
                if (findFragmentById != null && (findFragmentById instanceof GroupTagsFragment) && ((GroupTagsFragment) findFragmentById).mo10407c()) {
                    ((GroupTagsFragment) findFragmentById).mo10406b();
                }
            }
        });
        busSubscribe(C2618ag.class, new bjy<C2618ag>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Crashlytics.m16437a(th);
            }

            public void onNext(C2618ag agVar) {
                Fragment findFragmentById = CreateGroupActivity.this.getSupportFragmentManager().findFragmentById(R.id.placeholder);
                if (findFragmentById == null || !(findFragmentById instanceof GroupTagsFragment)) {
                    CreateGroupActivity.this.setmGroupPhotoFailedToUpload(true);
                } else {
                    ((GroupTagsFragment) findFragmentById).mo10408d();
                }
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                    return false;
                }
                setResult(0);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onPhotoChosen(String str, int i, int i2) {
        super.onPhotoChosen(str, i, i2);
        Fragment findFragmentById = getSupportFragmentManager().findFragmentById(R.id.placeholder);
        if (findFragmentById != null && (findFragmentById instanceof GroupInfoFragment)) {
            setGroupPhotoSkipped(false);
            ((GroupInfoFragment) findFragmentById).mo10379a();
            setGroupSourceUrl(str);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    public void resetTags() {
        if (this.f6234a != null) {
            this.f6234a.clear();
        }
    }

    public void setGroupAbout(String str) {
        this.f6236d = str;
    }

    public void setGroupName(String str) {
        this.f6235b = str;
    }

    public void setGroupPhotoSkipped(boolean z) {
        this.f6238f = z;
    }

    public void setGroupSourceUrl(String str) {
        this.f6237e = str;
    }

    public void setmGroupPhotoFailedToUpload(boolean z) {
        this.f6239g = z;
    }
}
