package com.becandid.candid.activities;

import android.support.p003v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import com.becandid.candid.R;
import com.becandid.candid.activities.RelatedPostsFeedActivity;

public class RelatedPostsFeedActivity$$ViewBinder<T extends RelatedPostsFeedActivity> implements ViewBinder<T> {

    /* compiled from: RelatedPostsFeedActivity$$ViewBinder */
    public static class InnerUnbinder<T extends RelatedPostsFeedActivity> implements Unbinder {

        /* renamed from: a */
        private T f6659a;

        protected InnerUnbinder(T t) {
            this.f6659a = t;
        }

        public final void unbind() {
            if (this.f6659a == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            unbind(this.f6659a);
            this.f6659a = null;
        }

        /* access modifiers changed from: protected */
        public void unbind(T t) {
            t.feedRecycler = null;
        }
    }

    public Unbinder bind(Finder finder, T t, Object obj) {
        InnerUnbinder createUnbinder = createUnbinder(t);
        t.feedRecycler = (RecyclerView) finder.castView((View) finder.findRequiredView(obj, R.id.related_posts_feed, "field 'feedRecycler'"), R.id.related_posts_feed, "field 'feedRecycler'");
        return createUnbinder;
    }

    /* access modifiers changed from: protected */
    public InnerUnbinder<T> createUnbinder(T t) {
        return new InnerUnbinder<>(t);
    }
}
