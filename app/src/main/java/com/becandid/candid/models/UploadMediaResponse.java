package com.becandid.candid.models;

public class UploadMediaResponse {
    public String full_url;

    public UploadMediaResponse(String str) {
        this.full_url = str;
    }
}
