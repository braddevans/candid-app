package com.becandid.candid.models;

import com.becandid.candid.models.NetworkData.UploadResponse;

public class JoinedVideoData {
    public String mCoverUrl;
    public String mVideoUrl;

    public JoinedVideoData(UploadResponse uploadResponse, UploadMediaResponse uploadMediaResponse) {
        this.mCoverUrl = uploadResponse.full_url;
        this.mVideoUrl = uploadMediaResponse.full_url;
    }
}
