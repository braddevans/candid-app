package com.becandid.candid.models.gif;

import java.util.List;

public class Media {
    public List<GifMedia> media;

    class GifInfo {
        public List<Integer> dims;
        public String preview;
        public String url;
    }

    class GifMedia {
        public GifInfo gif;
        public GifInfo tinygif;
    }

    public List<Integer> getDims() {
        if (this.media != null && !this.media.isEmpty()) {
            try {
                return ((GifMedia) this.media.get(0)).tinygif.dims;
            } catch (Exception e) {
                Crashlytics.m16437a((Throwable) e);
            }
        }
        return null;
    }

    public String getFullUrl() {
        if (this.media != null && !this.media.isEmpty()) {
            try {
                return ((GifMedia) this.media.get(0)).gif.url;
            } catch (Exception e) {
                Crashlytics.m16437a((Throwable) e);
            }
        }
        return null;
    }

    public String getThumbUrl() {
        if (this.media != null && !this.media.isEmpty()) {
            try {
                return ((GifMedia) this.media.get(0)).tinygif.preview;
            } catch (Exception e) {
                Crashlytics.m16437a((Throwable) e);
            }
        }
        return null;
    }

    public String getUrl() {
        if (this.media != null && !this.media.isEmpty()) {
            try {
                return ((GifMedia) this.media.get(0)).tinygif.url;
            } catch (Exception e) {
                Crashlytics.m16437a((Throwable) e);
            }
        }
        return null;
    }
}
