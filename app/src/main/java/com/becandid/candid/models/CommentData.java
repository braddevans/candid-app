package com.becandid.candid.models;

import com.becandid.candid.data.User;
import java.util.List;

public class CommentData {
    public int other_dislikes;
    public int other_likes;
    public List<User> users;
}
