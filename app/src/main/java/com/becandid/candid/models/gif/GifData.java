package com.becandid.candid.models.gif;

import java.util.List;

public class GifData {
    public String next;
    public List<Media> results;
}
