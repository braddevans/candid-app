package com.becandid.candid.models;

import android.graphics.Rect;
import android.view.View;
import com.becandid.candid.views.viewholders.VideoPostViewHolder;

public abstract class BaseVideoMessage implements bcv, bds {
    private static final String TAG = BaseVideoMessage.class.getSimpleName();
    private final Rect mCurrentViewRect;
    protected bcw mVideoPlayerManager;

    protected BaseVideoMessage() {
        this.mCurrentViewRect = new Rect();
        this.mVideoPlayerManager = null;
    }

    protected BaseVideoMessage(bcw bcw) {
        this.mCurrentViewRect = new Rect();
        this.mVideoPlayerManager = bcw;
    }

    private boolean viewIsPartiallyHiddenBottom(int i) {
        return this.mCurrentViewRect.bottom > 0 && this.mCurrentViewRect.bottom < i;
    }

    private boolean viewIsPartiallyHiddenTop() {
        return this.mCurrentViewRect.top > 0;
    }

    public bcw getVideoPlayerManager() {
        return this.mVideoPlayerManager;
    }

    public int getVisibilityPercents(View view) {
        if (view == null) {
            return 0;
        }
        view.getLocalVisibleRect(this.mCurrentViewRect);
        int height = view.getHeight();
        if (viewIsPartiallyHiddenTop()) {
            return ((height - this.mCurrentViewRect.top) * 100) / height;
        }
        if (viewIsPartiallyHiddenBottom(height)) {
            return (this.mCurrentViewRect.bottom * 100) / height;
        }
        return 100;
    }

    public void setActive(View view, int i) {
        try {
            BaseViewHolder juVar = (BaseViewHolder) view.getTag();
            if (juVar != null && (juVar instanceof VideoMessagingViewHolder)) {
                playNewVideo(new bcy(i, view), ((VideoMessagingViewHolder) juVar).f11008l, this.mVideoPlayerManager);
            } else if (juVar != null && (juVar instanceof VideoPostViewHolder)) {
                playNewVideo(new bcy(i, view), ((VideoPostViewHolder) juVar).postVideoPlayer, this.mVideoPlayerManager);
            }
        } catch (Exception e) {
        }
    }
}
