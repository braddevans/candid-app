package com.becandid.candid.models;

import com.becandid.candid.data.Comment;
import com.becandid.candid.data.Group;
import com.becandid.candid.data.Message;
import com.becandid.candid.data.MessageThread;
import com.becandid.candid.data.Notification;
import com.becandid.candid.data.Post;
import com.becandid.candid.data.UpdatePost;
import com.becandid.candid.data.User;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class NetworkData {
    public List<Notification> activity;
    public List<NotificationSetting> activity_settings;
    public Comment comment;
    public Map<String, Object> config;
    public boolean debug;
    public String display_url;
    public String error;
    public List<String> feed_colors;
    public List<String> feed_tab_order;
    public Group group;
    public List<String> group_tags;
    public List<Group> groups;
    public boolean has_more;
    public boolean is_new_user;
    public int last_activity_id;
    public boolean low_quality;
    public Message message;
    public User my_info;
    public List<Integer> nearby_levels;
    public int new_requests;
    public int new_threads;
    public int next_page;
    public int onboarding_min_joins;
    public String picture_status;
    public Post post;
    public String post_moved;
    public List<Post> posts;
    public User profile;
    public int referral_post_id;
    public List<Post> related_posts;
    public int request_next_page;
    public List<MessageThread> requests;
    public List<MessageThread> results;
    public int same_link_found;
    public ServerMessage server_message;
    public int show_nickname_prompt;
    public boolean success;
    public MessageThread thread;
    public int thread_next_page;
    public List<MessageThread> threads;
    public int unread_count = -1;
    public int unread_groups_count = -1;
    public UpdatePost update_post;

    public class NotificationSetting {
        public Integer disabled;
        public String display_name;
        public String type;

        public NotificationSetting() {
        }

        public NotificationSetting copy() {
            NotificationSetting notificationSetting = new NotificationSetting();
            notificationSetting.display_name = this.display_name;
            notificationSetting.type = this.type;
            notificationSetting.disabled = this.disabled;
            return notificationSetting;
        }
    }

    public static class UploadResponse {
        public String full_url;
        public Map<String, String> s3_data;
        public boolean success;
    }

    public String toString() {
        Field[] declaredFields;
        StringBuilder sb = new StringBuilder();
        String property = System.getProperty("line.separator");
        sb.append(getClass().getName());
        sb.append(" Object {");
        sb.append(property);
        for (Field field : getClass().getDeclaredFields()) {
            sb.append("  ");
            try {
                sb.append(field.getName());
                sb.append(": ");
                sb.append(field.get(this));
            } catch (IllegalAccessException e) {
                System.out.println(e);
            }
            sb.append(property);
        }
        sb.append("}");
        return sb.toString();
    }
}
