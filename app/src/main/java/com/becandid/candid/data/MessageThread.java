package com.becandid.candid.data;

import java.lang.reflect.Field;
import java.util.List;

public class MessageThread {
    public int can_add_nickname;
    public int is_request;
    public List<Message> messages;
    public int online;
    public String other_user_post_name;
    public String picture_status;
    public Post post;
    public int post_id;
    public long sent_time;
    public int show_nickname_alert;
    public User target_user_info;
    public String thread_comment_id;
    public int unread_messages;
    public User user_info;

    public String toString() {
        Field[] declaredFields;
        StringBuilder sb = new StringBuilder();
        String property = System.getProperty("line.separator");
        sb.append(getClass().getName());
        sb.append(" Object {");
        sb.append(property);
        for (Field field : getClass().getDeclaredFields()) {
            sb.append("  ");
            try {
                sb.append(field.getName());
                sb.append(": ");
                sb.append(field.get(this));
            } catch (IllegalAccessException e) {
                System.out.println(e);
            }
            sb.append(property);
        }
        sb.append("}");
        return sb.toString();
    }
}
