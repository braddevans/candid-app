package com.becandid.candid.data;

import android.os.Bundle;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.C2004b;
import com.facebook.GraphRequest.C2005c;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FacebookInfo {
    public String age;
    public AccessToken fbToken;
    public List<String> friendIds;
    public List<String> jobIds;
    public List<String> schoolIds;

    public interface LoadCallback {
        void onNext(int i);
    }

    /* access modifiers changed from: private */
    public String parseFriends(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = jSONObject.getJSONArray("data");
        if (this.friendIds == null) {
            this.friendIds = new ArrayList(jSONArray.length());
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            this.friendIds.add(jSONArray.getJSONObject(i).getString("id"));
        }
        if (jSONObject.has("paging") && jSONArray.length() > 20) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("paging");
            if (jSONObject2.has("next")) {
                String string = jSONObject2.getString("next");
                if (string.contains("/friends")) {
                    return string.substring(string.lastIndexOf("/", string.indexOf("/friends") - 1) + 1);
                }
            }
        }
        Log.d("FBINFO", "Friend ids: " + this.friendIds);
        return null;
    }

    /* access modifiers changed from: private */
    public void parseJobs(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = jSONObject.getJSONArray("work");
        this.jobIds = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            this.jobIds.add(jSONArray.getJSONObject(i).getJSONObject("employer").getString("id"));
        }
        Log.d("FBINFO", "Jobs ids: " + this.jobIds);
    }

    /* access modifiers changed from: private */
    public void parseSchools(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = jSONObject.getJSONArray("education");
        this.schoolIds = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            this.schoolIds.add(jSONArray.getJSONObject(i).getJSONObject("school").getString("id"));
        }
        Log.d("FBINFO", "School ids: " + this.schoolIds);
    }

    public void load() {
        load(null);
    }

    public void load(final LoadCallback loadCallback) {
        loadUserInfo(new LoadCallback() {
            public void onNext(int i) {
                bjs.m8428a(100, TimeUnit.MILLISECONDS).mo9249a(bkc.m8469a()).mo9259b((bkg<? super T>) new bkg<Long>() {
                    public void call(Long l) {
                        FacebookInfo.this.loadFriends(loadCallback);
                    }
                });
            }
        });
    }

    public void loadFriends(LoadCallback loadCallback) {
        this.friendIds = null;
        loadFriends("me/friends", loadCallback);
    }

    public void loadFriends(String str, final LoadCallback loadCallback) {
        if (this.fbToken != null) {
            Log.d("FBINFO", "Loading url: " + str);
            Bundle bundle = new Bundle();
            if (str.contains("?")) {
                try {
                    for (Entry entry : StringUtils.m14595b(str.substring(str.indexOf("?") + 1)).entrySet()) {
                        bundle.putString((String) entry.getKey(), (String) entry.getValue());
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                str = str.substring(0, str.indexOf("?"));
            }
            GraphRequest graphRequest = new GraphRequest(this.fbToken, str, null, null, new C2004b() {
                public void onCompleted(GraphResponse zlVar) {
                    JSONObject b = zlVar.mo16978b();
                    if (b != null) {
                        try {
                            String access$000 = FacebookInfo.this.parseFriends(b);
                            if (access$000 != null) {
                                FacebookInfo.this.loadFriends(access$000, loadCallback);
                            } else if (loadCallback != null) {
                                loadCallback.onNext(FacebookInfo.this.friendIds.size());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        RxBus.m14573a().mo14349a(new C2668y());
                    }
                }
            });
            bundle.putString("limit", "100");
            bundle.putString("fields", "data,paging");
            graphRequest.mo11547a(bundle);
            graphRequest.mo11562j();
        }
    }

    public void loadUserInfo(final LoadCallback loadCallback) {
        if (this.fbToken != null) {
            GraphRequest a = GraphRequest.m10467a(this.fbToken, (C2005c) new C2005c() {
                public void onCompleted(JSONObject jSONObject, GraphResponse zlVar) {
                    if (jSONObject != null) {
                        Log.d("FBINFO", "Me response: " + jSONObject);
                        try {
                            FacebookInfo.this.parseJobs(jSONObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            FacebookInfo.this.parseSchools(jSONObject);
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                        try {
                            FacebookInfo.this.age = jSONObject.getString("age_range");
                        } catch (JSONException e3) {
                            e3.printStackTrace();
                        }
                    } else {
                        Log.d("FBINFO", "Error getting /me: " + zlVar.mo16977a());
                    }
                    if (loadCallback != null) {
                        loadCallback.onNext(0);
                    }
                }
            });
            Bundle bundle = new Bundle();
            bundle.putString("fields", "work,education,age_range,gender,hometown");
            a.mo11547a(bundle);
            a.mo11562j();
        }
    }
}
