package com.becandid.candid.data;

import android.net.Uri;
import android.support.p001v4.app.NotificationCompat;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataUtil {
    static final char[] ALPHABET = "a34GpuRDcYZtwyzqhLndA9Qrf7MgoJl8Ik25sWBx_jmNCFSiK-PX10veTOEHUbV6".toCharArray();
    static char[] ALPHABET_MAP;
    public static final Pattern encoded_ids_re = Pattern.compile("\"encoded_(post_id|user_id|comment_id|group_id|last_activity_id|activity_id|referral_post_id|related_to_post)\": ?\"([^\"]+)\"");
    public static aym gson = new aym();

    public static class StringTrimResult {
        public String string;
        public int trimmedEnd;
        public int trimmedStart;

        public StringTrimResult(String str) {
            this.string = str;
        }
    }

    static {
        char[] cArr = new char[NotificationCompat.FLAG_LOCAL_ONLY];
        // fill-array-data instruction
        cArr[0] = 0;
        cArr[1] = 0;
        cArr[2] = 0;
        cArr[3] = 0;
        cArr[4] = 0;
        cArr[5] = 0;
        cArr[6] = 0;
        cArr[7] = 0;
        cArr[8] = 0;
        cArr[9] = 0;
        cArr[10] = 0;
        cArr[11] = 0;
        cArr[12] = 0;
        cArr[13] = 0;
        cArr[14] = 0;
        cArr[15] = 0;
        cArr[16] = 0;
        cArr[17] = 0;
        cArr[18] = 0;
        cArr[19] = 0;
        cArr[20] = 0;
        cArr[21] = 0;
        cArr[22] = 0;
        cArr[23] = 0;
        cArr[24] = 0;
        cArr[25] = 0;
        cArr[26] = 0;
        cArr[27] = 0;
        cArr[28] = 0;
        cArr[29] = 0;
        cArr[30] = 0;
        cArr[31] = 0;
        cArr[32] = 0;
        cArr[33] = 0;
        cArr[34] = 0;
        cArr[35] = 0;
        cArr[36] = 0;
        cArr[37] = 0;
        cArr[38] = 0;
        cArr[39] = 0;
        cArr[40] = 0;
        cArr[41] = 0;
        cArr[42] = 0;
        cArr[43] = 0;
        cArr[44] = 0;
        cArr[45] = 49;
        cArr[46] = 0;
        cArr[47] = 0;
        cArr[48] = 53;
        cArr[49] = 52;
        cArr[50] = 34;
        cArr[51] = 1;
        cArr[52] = 2;
        cArr[53] = 35;
        cArr[54] = 63;
        cArr[55] = 25;
        cArr[56] = 31;
        cArr[57] = 21;
        cArr[58] = 0;
        cArr[59] = 0;
        cArr[60] = 0;
        cArr[61] = 0;
        cArr[62] = 0;
        cArr[63] = 0;
        cArr[64] = 0;
        cArr[65] = 20;
        cArr[66] = 38;
        cArr[67] = 44;
        cArr[68] = 7;
        cArr[69] = 58;
        cArr[70] = 45;
        cArr[71] = 3;
        cArr[72] = 59;
        cArr[73] = 32;
        cArr[74] = 29;
        cArr[75] = 48;
        cArr[76] = 17;
        cArr[77] = 26;
        cArr[78] = 43;
        cArr[79] = 57;
        cArr[80] = 50;
        cArr[81] = 22;
        cArr[82] = 6;
        cArr[83] = 46;
        cArr[84] = 56;
        cArr[85] = 60;
        cArr[86] = 62;
        cArr[87] = 37;
        cArr[88] = 51;
        cArr[89] = 9;
        cArr[90] = 10;
        cArr[91] = 0;
        cArr[92] = 0;
        cArr[93] = 0;
        cArr[94] = 0;
        cArr[95] = 40;
        cArr[96] = 0;
        cArr[97] = 0;
        cArr[98] = 61;
        cArr[99] = 8;
        cArr[100] = 19;
        cArr[101] = 55;
        cArr[102] = 24;
        cArr[103] = 27;
        cArr[104] = 16;
        cArr[105] = 47;
        cArr[106] = 41;
        cArr[107] = 33;
        cArr[108] = 30;
        cArr[109] = 42;
        cArr[110] = 18;
        cArr[111] = 28;
        cArr[112] = 4;
        cArr[113] = 15;
        cArr[114] = 23;
        cArr[115] = 36;
        cArr[116] = 11;
        cArr[117] = 5;
        cArr[118] = 54;
        cArr[119] = 12;
        cArr[120] = 39;
        cArr[121] = 13;
        cArr[122] = 14;
        cArr[123] = 0;
        cArr[124] = 0;
        cArr[125] = 0;
        cArr[126] = 0;
        cArr[127] = 0;
        cArr[128] = 0;
        cArr[129] = 0;
        cArr[130] = 0;
        cArr[131] = 0;
        cArr[132] = 0;
        cArr[133] = 0;
        cArr[134] = 0;
        cArr[135] = 0;
        cArr[136] = 0;
        cArr[137] = 0;
        cArr[138] = 0;
        cArr[139] = 0;
        cArr[140] = 0;
        cArr[141] = 0;
        cArr[142] = 0;
        cArr[143] = 0;
        cArr[144] = 0;
        cArr[145] = 0;
        cArr[146] = 0;
        cArr[147] = 0;
        cArr[148] = 0;
        cArr[149] = 0;
        cArr[150] = 0;
        cArr[151] = 0;
        cArr[152] = 0;
        cArr[153] = 0;
        cArr[154] = 0;
        cArr[155] = 0;
        cArr[156] = 0;
        cArr[157] = 0;
        cArr[158] = 0;
        cArr[159] = 0;
        cArr[160] = 0;
        cArr[161] = 0;
        cArr[162] = 0;
        cArr[163] = 0;
        cArr[164] = 0;
        cArr[165] = 0;
        cArr[166] = 0;
        cArr[167] = 0;
        cArr[168] = 0;
        cArr[169] = 0;
        cArr[170] = 0;
        cArr[171] = 0;
        cArr[172] = 0;
        cArr[173] = 0;
        cArr[174] = 0;
        cArr[175] = 0;
        cArr[176] = 0;
        cArr[177] = 0;
        cArr[178] = 0;
        cArr[179] = 0;
        cArr[180] = 0;
        cArr[181] = 0;
        cArr[182] = 0;
        cArr[183] = 0;
        cArr[184] = 0;
        cArr[185] = 0;
        cArr[186] = 0;
        cArr[187] = 0;
        cArr[188] = 0;
        cArr[189] = 0;
        cArr[190] = 0;
        cArr[191] = 0;
        cArr[192] = 0;
        cArr[193] = 0;
        cArr[194] = 0;
        cArr[195] = 0;
        cArr[196] = 0;
        cArr[197] = 0;
        cArr[198] = 0;
        cArr[199] = 0;
        cArr[200] = 0;
        cArr[201] = 0;
        cArr[202] = 0;
        cArr[203] = 0;
        cArr[204] = 0;
        cArr[205] = 0;
        cArr[206] = 0;
        cArr[207] = 0;
        cArr[208] = 0;
        cArr[209] = 0;
        cArr[210] = 0;
        cArr[211] = 0;
        cArr[212] = 0;
        cArr[213] = 0;
        cArr[214] = 0;
        cArr[215] = 0;
        cArr[216] = 0;
        cArr[217] = 0;
        cArr[218] = 0;
        cArr[219] = 0;
        cArr[220] = 0;
        cArr[221] = 0;
        cArr[222] = 0;
        cArr[223] = 0;
        cArr[224] = 0;
        cArr[225] = 0;
        cArr[226] = 0;
        cArr[227] = 0;
        cArr[228] = 0;
        cArr[229] = 0;
        cArr[230] = 0;
        cArr[231] = 0;
        cArr[232] = 0;
        cArr[233] = 0;
        cArr[234] = 0;
        cArr[235] = 0;
        cArr[236] = 0;
        cArr[237] = 0;
        cArr[238] = 0;
        cArr[239] = 0;
        cArr[240] = 0;
        cArr[241] = 0;
        cArr[242] = 0;
        cArr[243] = 0;
        cArr[244] = 0;
        cArr[245] = 0;
        cArr[246] = 0;
        cArr[247] = 0;
        cArr[248] = 0;
        cArr[249] = 0;
        cArr[250] = 0;
        cArr[251] = 0;
        cArr[252] = 0;
        cArr[253] = 0;
        cArr[254] = 0;
        cArr[255] = 0;
        ALPHABET_MAP = cArr;
    }

    public static long decodeId(String str) {
        long j = 0;
        for (int length = str.length() - 1; length >= 0; length--) {
            j = (j << 6) + ((long) ALPHABET_MAP[str.charAt(length)]);
        }
        return shuffle_bits(1487642837374375497L ^ j);
    }

    public static String decodeIds(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        Matcher matcher = encoded_ids_re.matcher(str);
        while (matcher.find()) {
            matcher.appendReplacement(stringBuffer, "\"" + matcher.group(1) + "\":" + decodeId(matcher.group(2)));
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

    public static String getEncodedId(Uri uri) {
        if (uri == null || uri.getPathSegments() == null || uri.getPathSegments().isEmpty() || uri.getPathSegments().size() < 2) {
            return null;
        }
        return (String) uri.getPathSegments().get(1);
    }

    public static String join(List<String> list) {
        return join(list, ",");
    }

    public static String join(List<String> list, String str) {
        if (list == null) {
            Crashlytics.m16437a(new Throwable("list fbinfo friend ids is NULL"));
            return null;
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String str2 : list) {
            if (z) {
                z = false;
            } else {
                sb.append(str);
            }
            sb.append(str2);
        }
        return sb.toString();
    }

    public static ays newJsonObject(String... strArr) {
        if (strArr.length % 2 != 0) {
            Log.e("DataUtil", "Bad ");
        }
        ays ays = new ays();
        for (int i = 0; i < strArr.length; i += 2) {
            ays.mo8408a(strArr[i], strArr[i + 1]);
        }
        return ays;
    }

    public static long shuffle_bits(long j) {
        long j2 = 0;
        for (int i = 0; i < 64; i++) {
            j2 <<= 1;
            if (((1 << (((i % 8) * 8) + (i / 8))) & j) != 0) {
                j2 |= 1;
            }
        }
        return j2;
    }

    public static void sortTextTags(ArrayList<TextTag> arrayList, int i, int i2) {
        if (i < i2) {
            int i3 = (i + i2) / 2;
            sortTextTags(arrayList, i, i3);
            sortTextTags(arrayList, i3 + 1, i2);
            int i4 = i;
            int i5 = i3 + 1;
            if (((TextTag) arrayList.get(i3)).startIndex > ((TextTag) arrayList.get(i5)).startIndex) {
                while (i4 <= i3 && i5 <= i2) {
                    if (((TextTag) arrayList.get(i4)).startIndex <= ((TextTag) arrayList.get(i5)).startIndex) {
                        i4++;
                    } else {
                        TextTag textTag = (TextTag) arrayList.get(i5);
                        int i6 = i4 + 1;
                        Iterator it = new ArrayList(arrayList.subList(i4, i5)).iterator();
                        while (it.hasNext()) {
                            arrayList.set(i6, (TextTag) it.next());
                            i6++;
                        }
                        arrayList.set(i4, textTag);
                        i4++;
                        i3++;
                        i5++;
                    }
                }
            }
        }
    }

    public static boolean toBoolean(Object obj, boolean z) {
        return obj instanceof Number ? ((Number) obj).intValue() != 0 : obj instanceof Boolean ? ((Boolean) obj).booleanValue() : z;
    }

    public static float toFloat(Object obj, float f) {
        return obj instanceof Number ? ((Number) obj).floatValue() : f;
    }

    public static int toInt(Object obj, int i) {
        return obj instanceof Number ? ((Number) obj).intValue() : obj instanceof Boolean ? ((Boolean) obj).booleanValue() ? 1 : 0 : i;
    }

    public static String toJson(Object obj) {
        return gson.mo8360a(obj);
    }

    public static <T> List<T> toList(ayo ayo, Class<T> cls) {
        int a = ayo.mo8381a();
        ArrayList arrayList = new ArrayList(a);
        for (int i = 0; i < a; i++) {
            arrayList.add(gson.mo8353a(ayo.mo8382a(i), cls));
        }
        return arrayList;
    }

    public static Map<String, Object> toMap(ays ays) {
        return (Map) gson.mo8354a((ayq) ays, new azz<Map<String, Object>>() {
        }.getType());
    }

    public static Map<String, Object> toMap(String str) {
        return (Map) gson.mo8358a(str, new azz<Map<String, Object>>() {
        }.getType());
    }

    public static StringTrimResult trimWithCount(String str) {
        StringTrimResult stringTrimResult = new StringTrimResult(str);
        int i = 0;
        int length = str.length() - 1;
        int i2 = length;
        while (i <= i2 && str.charAt(i) <= ' ') {
            i++;
        }
        while (i2 >= i && str.charAt(i2) <= ' ') {
            i2--;
        }
        if (!(i == 0 && i2 == length)) {
            stringTrimResult.string = str.substring(i, i2 + 1);
            stringTrimResult.trimmedStart = i;
            stringTrimResult.trimmedEnd = (str.length() - 1) - i2;
        }
        return stringTrimResult;
    }
}
