package com.becandid.candid.data;

public class GroupSearchResult {
    public String firstTag;
    public int groupId;
    public String groupName;
    public int numMembers;
    public String thumbUrl;

    public String toString() {
        return "groupName: " + this.groupName + ", groupId: " + Integer.toString(this.groupId);
    }
}
