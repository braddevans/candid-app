package com.becandid.candid.data;

import java.lang.reflect.Field;
import java.util.List;

public class User {
    public String age;
    public String badge_status;
    public boolean have_fb;
    public boolean have_phone_number;
    public String icon_color;
    public String icon_name;
    public boolean is_blocked;
    public int liked;
    public int message_auto_deletion;
    public int messaging_disabled;
    public int need_age;
    public int need_onboarding;
    public String nickname;
    public int num_fb_friends;
    public int num_groups;
    public int num_phone_friends;
    public int num_posts;
    public int online;
    public String post_name;
    public float quality_score;
    public int set_community;
    public List<String> tags;
    public int thats_me;
    public int unread_activity_count = -1;
    public int unread_feed_count = -1;
    public int unread_groups_count = -1;
    public int unread_message_count = -1;
    public int user_deleted;
    public int user_id;
    public String user_name;

    public String toString() {
        Field[] declaredFields;
        StringBuilder sb = new StringBuilder();
        String property = System.getProperty("line.separator");
        sb.append(getClass().getName());
        sb.append(" Object {");
        sb.append(property);
        for (Field field : getClass().getDeclaredFields()) {
            sb.append("  ");
            try {
                sb.append(field.getName());
                sb.append(": ");
                sb.append(field.get(this));
            } catch (IllegalAccessException e) {
                System.out.println(e);
            }
            sb.append(property);
        }
        sb.append("}");
        return sb.toString();
    }
}
