package com.becandid.candid.data;

import android.app.NotificationManager;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.util.Log;
import com.becandid.candid.GossipApplication;
import com.becandid.candid.activities.MainTabsActivity;
import com.becandid.candid.data.FacebookInfo.LoadCallback;
import com.becandid.candid.models.EmptySubscriber;
import com.becandid.candid.models.NetworkData.NotificationSetting;
import com.becandid.candid.models.gif.Media;
import com.facebook.AccessToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import p012rx.schedulers.Schedulers;

public class AppState {
    public static User account;
    public static boolean accountDisowned = false;
    public static ArrayList<String> activeTags;
    private static List<String> adultTags;
    public static String age;
    public static boolean blurTaskCalledOnFlight = false;
    public static Config config;
    public static ContactsInfo contactsInfo;
    public static HashSet<Integer> expandedPostIds;
    public static FacebookInfo fbInfo;
    public static List<String> feedColors;
    public static List<Media> gifCacheList = new ArrayList();
    public static String gifCacheNextPos;
    public static List<String> groupTags;
    public static List<Group> groups;
    public static boolean hasLinkSearched;
    public static boolean hasMessagedFromAlert;
    public static boolean hasMessagingShown;
    public static boolean hasMuted;
    public static boolean hasPosted;
    public static boolean hasShownCommunityPopup;
    public static boolean hasShownMutePopup;
    public static boolean hasShownUnmutePopup;
    public static boolean hasVideoMessaged;
    public static boolean internal;
    public static Location location;
    public static String locationProvider;
    public static String mBaseUrl;
    public static MainTabsActivity mainTabsActivity;
    public static int needAge;
    public static int needOnboarding;
    public static String nickname;
    public static ArrayList<NotificationSetting> notificationSettings;
    private static List<String> onboardingOrders = new ArrayList();
    public static String referralId;
    public static int referralPostId;
    public static List<Post> relatedPosts;
    public static int setCommunity;
    public static List<String> tabsOrder;
    public static List<String> tags;
    private static List<String> teenTags;

    public static void clearState(ContextWrapper contextWrapper) {
        Log.d("APP", "Clear app state");
        Editor edit = contextWrapper.getSharedPreferences("com.becandid.candid", 0).edit();
        edit.remove("account");
        edit.remove("tags");
        edit.remove("colors");
        edit.remove("contactsInfo");
        edit.remove("hasPosted");
        edit.remove("hasMuted");
        edit.remove("hasMessagingShown");
        edit.remove("hasShownUnmutePopup");
        edit.remove("hasMessagedFromAlerts");
        edit.remove("hasLinkSearched");
        edit.remove("hasShownCommunityPopup");
        edit.remove("age");
        edit.remove("nickname");
        edit.clear();
        edit.commit();
    }

    public static void disownAccount() {
        tags = null;
        activeTags = null;
        location = null;
        locationProvider = null;
        fbInfo = null;
        contactsInfo = null;
        account = null;
        groups = null;
        hasPosted = false;
        hasMuted = false;
        hasShownMutePopup = false;
        hasShownUnmutePopup = false;
        hasMessagingShown = false;
        hasLinkSearched = false;
        hasVideoMessaged = false;
        hasShownCommunityPopup = false;
        age = null;
        teenTags = null;
        adultTags = null;
        nickname = null;
        setCommunity = 0;
        if (AccessToken.m10408a() != null) {
            abo.m502c().mo270d();
        }
        ((NotificationManager) GossipApplication.f6093a.getSystemService("notification")).cancelAll();
        ApiService.f10527a.removeAll();
        AccountUtils.m14505c();
        clearState(GossipApplication.m9012a());
        accountDisowned = true;
    }

    public static List<String> getOnboardingOrders() {
        return onboardingOrders;
    }

    public static boolean isGroupMember(int i) {
        if (groups == null) {
            return false;
        }
        for (Group group : groups) {
            if (group.group_id == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean isGroupModerator(int i) {
        if (groups == null) {
            return false;
        }
        for (Group group : groups) {
            if (group.group_id == i && group.moderator == 1) {
                return true;
            }
        }
        return false;
    }

    public static void loadState(ContextWrapper contextWrapper) {
        SharedPreferences sharedPreferences = contextWrapper.getSharedPreferences("com.becandid.candid", 0);
        Log.d("APP", "Checking shared prefs for app state");
        if (sharedPreferences.contains("base_url")) {
            mBaseUrl = sharedPreferences.getString("base_url", GossipApplication.f6096d);
        } else {
            mBaseUrl = GossipApplication.f6096d;
        }
        if (sharedPreferences.contains("config")) {
            setConfig(DataUtil.toMap(sharedPreferences.getString("config", "{}")));
        }
        if (sharedPreferences.contains("account")) {
            String string = sharedPreferences.getString("account", null);
            if (string != null) {
                account = (User) DataUtil.gson.mo8357a(string, User.class);
            }
            if (loggedin()) {
                ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                AccountUtils.m14503a();
            } else {
                Map b = AccountUtils.m14504b();
                if (b != null) {
                    PersistentCookieStore.getCookieStore(GossipApplication.m9012a()).setAllCookies(b);
                    ApiService.m14297a().mo14115b().mo9256b(Schedulers.m16145io()).mo9249a(bkc.m8469a()).mo9258b((bjy<? super T>) new EmptySubscriber<Object>());
                    AccountUtils.m14503a();
                }
            }
        }
        if (sharedPreferences.contains("tags")) {
            try {
                Type type = new azz<List<String>>() {
                }.getType();
                tags = (List) new aym().mo8358a(sharedPreferences.getString("tags", "{}"), type);
            } catch (ClassCastException e) {
            }
        }
        if (sharedPreferences.contains("teenTags")) {
            try {
                Type type2 = new azz<List<String>>() {
                }.getType();
                teenTags = (List) new aym().mo8358a(sharedPreferences.getString("teenTags", "{}"), type2);
            } catch (ClassCastException e2) {
            }
        }
        if (sharedPreferences.contains("adultTags")) {
            try {
                Type type3 = new azz<List<String>>() {
                }.getType();
                adultTags = (List) new aym().mo8358a(sharedPreferences.getString("adultTags", "{}"), type3);
            } catch (ClassCastException e3) {
            }
        }
        if (feedColors == null) {
            if (sharedPreferences.contains("colors")) {
                feedColors = new ArrayList(sharedPreferences.getStringSet("colors", null));
            } else {
                feedColors = new ArrayList(Arrays.asList(new String[]{"#3b6c9e", "#23cbba", "#5cc396", "#e3c02a", "#603785", "#1daff1", "#fd9231", "#94bb65", "#d2822c", "#945319", "#9ccb46", "#147b40", "#e6ad38", "#a837aa", "#e3515d", "#cc333e", "#465163", "#d46342"}));
            }
            reduceColorBrightness(feedColors);
        }
        if (sharedPreferences.contains("hasPosted")) {
            hasPosted = sharedPreferences.getBoolean("hasPosted", false);
        }
        if (sharedPreferences.contains("hasMuted")) {
            hasMuted = sharedPreferences.getBoolean("hasMuted", false);
        }
        if (sharedPreferences.contains("hasShownMutePopup")) {
            hasShownMutePopup = sharedPreferences.getBoolean("hasShownMutePopup", false);
        }
        if (sharedPreferences.contains("hasShownUnmutePopup")) {
            hasShownUnmutePopup = sharedPreferences.getBoolean("hasShownUnmutePopup", false);
        }
        if (sharedPreferences.contains("hasMessagingShown")) {
            hasMessagingShown = sharedPreferences.getBoolean("hasMessagingShown", false);
        }
        if (sharedPreferences.contains("hasMessagedFromAlert")) {
            hasMessagedFromAlert = sharedPreferences.getBoolean("hasMessagedFromAlert", false);
        }
        if (sharedPreferences.contains("hasLinkSearched")) {
            hasLinkSearched = sharedPreferences.getBoolean("hasLinkSearched", false);
        }
        if (sharedPreferences.contains("hasVideoMessaged")) {
            hasVideoMessaged = sharedPreferences.getBoolean("hasVideoMessaged", false);
        }
        if (sharedPreferences.contains("hasShownCommunityPopup")) {
            hasShownCommunityPopup = sharedPreferences.getBoolean("hasShownCommunityPopup", false);
        }
        if (AccessToken.m10408a() != null) {
            fbInfo = new FacebookInfo();
            fbInfo.fbToken = AccessToken.m10408a();
        }
        if (sharedPreferences.contains("tabsOrder")) {
            Type type4 = new azz<List<String>>() {
            }.getType();
            tabsOrder = (List) new aym().mo8358a(sharedPreferences.getString("tabsOrder", "{}"), type4);
        }
        if (sharedPreferences.contains("age")) {
            age = sharedPreferences.getString("age", null);
        }
        if (sharedPreferences.contains("nickname")) {
            nickname = sharedPreferences.getString("nickname", null);
        }
    }

    public static boolean loggedin() {
        return account != null;
    }

    private static void reduceColorBrightness(List<String> list) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("#23cbba");
        arrayList.add("#1daff1");
        arrayList.add("#fd9231");
        arrayList.add("#9ccb46");
        arrayList.add("#e3515d");
        for (String str : arrayList) {
            if (list.contains(str)) {
                list.add(list.indexOf(str), "#cc" + str.substring(1));
                list.remove(str);
            }
        }
    }

    public static void saveState(ContextWrapper contextWrapper) {
        saveState(contextWrapper, true);
    }

    public static void saveState(ContextWrapper contextWrapper, boolean z) {
        Editor edit = contextWrapper.getSharedPreferences("com.becandid.candid", 0).edit();
        if (!(config == null || config.experimentConfig == null)) {
            edit.putString("config", DataUtil.toJson(config.experimentConfig));
        }
        if (account != null) {
            edit.putString("account", DataUtil.toJson(account));
        }
        if (tags != null) {
            edit.putString("tags", DataUtil.toJson(tags));
        }
        if (teenTags != null) {
            edit.putString("teenTags", DataUtil.toJson(teenTags));
        }
        if (adultTags != null) {
            edit.putString("adultTags", DataUtil.toJson(adultTags));
        }
        if (feedColors != null) {
            edit.putStringSet("colors", new LinkedHashSet(feedColors));
        }
        if (tabsOrder != null) {
            edit.putString("tabsOrder", DataUtil.toJson(tabsOrder));
        }
        edit.putBoolean("hasPosted", hasPosted);
        edit.putBoolean("hasMuted", hasMuted);
        edit.putBoolean("hasMessagingShown", hasMessagingShown);
        edit.putBoolean("hasShownMutePopup", hasShownMutePopup);
        edit.putBoolean("hasShownUnmutePopup", hasShownUnmutePopup);
        edit.putBoolean("hasMessagedFromAlert", hasMessagedFromAlert);
        edit.putBoolean("hasLinkSearched", hasLinkSearched);
        edit.putBoolean("hasVideoMessaged", hasVideoMessaged);
        edit.putBoolean("hasShownCommunityPopup", hasShownCommunityPopup);
        if (age != null) {
            edit.putString("age", age);
            if ("under_18".equals(age) && teenTags != null && !teenTags.isEmpty()) {
                tags = teenTags;
                edit.putString("tags", DataUtil.toJson(tags));
            } else if (!(age == null || adultTags == null || adultTags.isEmpty())) {
                tags = adultTags;
                edit.putString("tags", DataUtil.toJson(tags));
            }
        }
        if (nickname != null) {
            edit.putString("nickname", nickname);
        }
        if (mBaseUrl != null) {
            edit.putString("base_url", mBaseUrl);
        }
        if (location != null) {
            edit.putString("cache_location", location.getLatitude() + "," + location.getLongitude() + "@" + (location.hasAccuracy() ? Float.valueOf(location.getAccuracy()) : "50"));
        }
        edit.commit();
        if (z) {
            AccountUtils.m14503a();
        }
    }

    public static void setConfig(Map<String, Object> map) {
        config = new Config();
        config.setExperimentConfig(map);
    }

    public static void setFBInfo(AccessToken accessToken) {
        if (account != null) {
            account.have_fb = true;
        }
        fbInfo = new FacebookInfo();
        fbInfo.fbToken = accessToken;
        fbInfo.load();
    }

    public static void setFBInfo(AccessToken accessToken, LoadCallback loadCallback) {
        if (account != null) {
            account.have_fb = true;
        }
        fbInfo = new FacebookInfo();
        fbInfo.fbToken = accessToken;
        fbInfo.load(loadCallback);
    }

    public static void setOnboardingAdultTags(List<String> list) {
        adultTags = list;
    }

    public static void setOnboardingOrder(List<String> list) {
        if (list == null) {
            list = new ArrayList<>();
            list.add("phone");
            list.add("fb");
            list.add("location");
            list.add("age");
            list.add("tags");
        }
        onboardingOrders.clear();
        onboardingOrders.addAll(list);
    }

    public static void setOnboardingTags(Map<String, Object> map) {
        tags = (ArrayList) map.get("tags");
    }

    public static void setOnboardingTeenTags(List<String> list) {
        teenTags = list;
    }
}
