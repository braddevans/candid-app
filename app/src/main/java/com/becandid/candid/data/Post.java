package com.becandid.candid.data;

import android.graphics.Bitmap;
import com.becandid.candid.models.BaseVideoMessage;
import com.volokh.danylo.video_player_manager.p008ui.VideoPlayerView;
import java.lang.reflect.Field;
import java.util.List;

public class Post extends BaseVideoMessage {
    public String about;
    public int actual_height = -1;
    public int actual_width = -1;
    public int animated;
    public int background_color = -1;
    public int can_mute;
    public String caption;
    public String comment_icon_name;
    public int comment_image_height = -1;
    public int comment_image_width = -1;
    public int comment_is_candid_mod;
    public String comment_source_url;
    public String comment_sticker_name;
    public String comment_text;
    public String comment_time_ago;
    public List<Comment> comments;
    public int community_membership = 0;
    public int first_related_post;
    public int friend_created = 0;
    public int friends_disabled;
    public int from_camera;
    public Bitmap groupBackgroundDrawable;
    public Bitmap groupRoundedImage;
    public int group_id;
    public String group_name;
    public int height;
    public String icon_color;
    public String icon_name;
    public String image_hash;
    public int is_candid_mod;
    public int is_friend;
    public int last_related_post;
    public String layout_xml;
    public int like_value;
    public String link_domain;
    public String link_url;
    public String localBitmapPath;
    private String mVideoUrl;
    public String mentioned_groups_info;
    public String messaging_blocked;
    public String messaging_disabled;
    public int moderator;
    public int muted_post;
    public int num_comments;
    public int num_dislikes;
    public int num_false;
    public int num_friends = 0;
    public int num_likes;
    public int num_members = 0;
    public int num_posts = 0;
    public int num_true;
    public String og_desc;
    public String og_title;
    public int opinion_value;
    public int owner_user_id;
    public int post_id;
    public String post_time_ago;
    public float quality_score;
    public int related_to_post;
    public int rumor;
    public ShareInfo share_info;
    public String source_url;
    public int t_joined;
    public int t_last_post;
    public List<String> tags;
    public int thats_me;
    public int theme_color = 0;
    public String thumb_url;
    public int trending;
    public String type;
    public String user_name;
    public int wait_for_play;
    public int width;
    public String youtube_video_id;

    public boolean equals(Object obj) {
        if (!(obj instanceof Post)) {
            return false;
        }
        return this.post_id == ((Post) obj).post_id;
    }

    public String imageUrl() {
        return (this.thumb_url == null || this.thumb_url.length() <= 0) ? this.source_url : this.thumb_url;
    }

    public boolean isMember() {
        return AppState.isGroupMember(this.group_id);
    }

    public void playNewVideo(bcz bcz, VideoPlayerView videoPlayerView, bcw<bcz> bcw) {
        if (bcw != null) {
            bcw.mo8816a(bcz, videoPlayerView, this.mVideoUrl);
        }
    }

    public void setVideoPlayManager(bcw bcw) {
        this.mVideoPlayerManager = bcw;
    }

    public void setVideoUrl(String str) {
        this.mVideoUrl = str;
    }

    public String toString() {
        Field[] declaredFields;
        StringBuilder sb = new StringBuilder();
        String property = System.getProperty("line.separator");
        sb.append(getClass().getName());
        sb.append(" Object {");
        sb.append(property);
        for (Field field : getClass().getDeclaredFields()) {
            sb.append("  ");
            try {
                sb.append(field.getName());
                sb.append(": ");
                sb.append(field.get(this));
            } catch (IllegalAccessException e) {
                System.out.println(e);
            }
            sb.append(property);
        }
        sb.append("}");
        return sb.toString();
    }
}
