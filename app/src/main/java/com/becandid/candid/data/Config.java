package com.becandid.candid.data;

import java.util.Map;

public class Config {
    public Map<String, Object> experimentConfig;

    public boolean getBoolean(String str) {
        return getBoolean(str, false);
    }

    public boolean getBoolean(String str, boolean z) {
        return (this.experimentConfig != null && this.experimentConfig.containsKey(str)) ? DataUtil.toBoolean(this.experimentConfig.get(str), z) : z;
    }

    public float getFloat(String str, float f) {
        return (this.experimentConfig != null && this.experimentConfig.containsKey(str)) ? DataUtil.toFloat(this.experimentConfig.get(str), f) : f;
    }

    public int getInt(String str) {
        return getInt(str, 0);
    }

    public int getInt(String str, int i) {
        return (this.experimentConfig != null && this.experimentConfig.containsKey(str)) ? DataUtil.toInt(this.experimentConfig.get(str), i) : i;
    }

    public String getString(String str) {
        return getString(str, "");
    }

    public String getString(String str, String str2) {
        if (this.experimentConfig == null || !this.experimentConfig.containsKey(str)) {
            return str2;
        }
        Object obj = this.experimentConfig.get(str);
        return (!(obj instanceof String) || ((String) obj).isEmpty()) ? str2 : (String) obj;
    }

    public boolean has(String str) {
        return this.experimentConfig != null && this.experimentConfig.containsKey(str);
    }

    public void setExperimentConfig(Map<String, Object> map) {
        this.experimentConfig = map;
    }
}
