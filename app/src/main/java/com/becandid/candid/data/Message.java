package com.becandid.candid.data;

import com.becandid.candid.models.BaseVideoMessage;
import com.volokh.danylo.video_player_manager.p008ui.VideoPlayerView;
import java.lang.reflect.Field;
import java.util.Map;

public class Message extends BaseVideoMessage {
    public boolean failedSend = false;
    public int image_height;
    public int image_width;
    private String mVideoUrl;
    public String message;
    public boolean messageHeader;
    public boolean messagePost;
    public long messageTempId;
    public int message_id;
    public Map<String, String> params;
    public Post post;
    public int post_id;
    public String sender = "self";
    public long sent_time;
    public String source_url;
    public String sticker_name;
    public String subject;
    public int uploaded;
    public User user_info;
    public String video_url;

    public Message() {
    }

    public Message(bcw bcw, String str) {
        super(bcw);
        this.mVideoUrl = str;
    }

    public void playNewVideo(bcz bcz, VideoPlayerView videoPlayerView, bcw<bcz> bcw) {
        if (bcw != null) {
            bcw.mo8816a(bcz, videoPlayerView, this.mVideoUrl);
        }
    }

    public void setVideoPlayManager(bcw bcw) {
        this.mVideoPlayerManager = bcw;
    }

    public void setVideoUrl(String str) {
        this.mVideoUrl = str;
    }

    public String toString() {
        Field[] declaredFields;
        StringBuilder sb = new StringBuilder();
        String property = System.getProperty("line.separator");
        sb.append(getClass().getName());
        sb.append(" Object {");
        sb.append(property);
        for (Field field : getClass().getDeclaredFields()) {
            sb.append("  ");
            try {
                sb.append(field.getName());
                sb.append(": ");
                sb.append(field.get(this));
            } catch (IllegalAccessException e) {
                System.out.println(e);
            }
            sb.append(property);
        }
        sb.append("}");
        return sb.toString();
    }
}
