package com.becandid.candid.data;

import java.lang.reflect.Field;
import java.util.List;

public class Comment {
    public int clipped = 1;
    public int comment_id;
    public String comment_text;
    public String comment_time_ago;
    public int from_camera;
    public String icon_color;
    public String icon_name;
    public int image_height;
    public int image_width;
    public int is_candid_mod;
    public int is_friend;
    public boolean is_master_comment = false;
    public int is_op;
    public int like_value;
    public String mentioned_groups_info;
    public String messaging_blocked;
    public String messaging_disabled;
    public int num_dislikes;
    public int num_likes;
    public int post_id;
    public List<Comment> reply_comments;
    public List<Comment> seeMoreComments;
    public int small_image_height;
    public int small_image_width;
    public String source_url;
    public String sticker_name;
    public int thats_me;
    public User user;
    public String user_name;

    public boolean equals(Object obj) {
        return (obj instanceof Comment) && this.comment_id == ((Comment) obj).comment_id;
    }

    public String toString() {
        Field[] declaredFields;
        StringBuilder sb = new StringBuilder();
        String property = System.getProperty("line.separator");
        sb.append(getClass().getName());
        sb.append(" Object {");
        sb.append(property);
        for (Field field : getClass().getDeclaredFields()) {
            sb.append("  ");
            try {
                sb.append(field.getName());
                sb.append(": ");
                sb.append(field.get(this));
            } catch (IllegalAccessException e) {
                System.out.println(e);
            }
            sb.append(property);
        }
        sb.append("}");
        return sb.toString();
    }
}
