package com.becandid.candid.data;

import java.lang.reflect.Field;

public class Notification {
    public String action_icon;
    public int activity_id;
    public String body;
    public int comment_id;
    public String icon_color;
    public String icon_name;
    public int image_height;
    public int image_width;
    public String onclick;
    public String source_url;
    public String sticker_name;
    public long t_create;
    public String title;
    public int unread;
    public String user_name;

    public String toString() {
        Field[] declaredFields;
        StringBuilder sb = new StringBuilder();
        String property = System.getProperty("line.separator");
        sb.append(getClass().getName());
        sb.append(" Object {");
        sb.append(property);
        for (Field field : getClass().getDeclaredFields()) {
            sb.append("  ");
            try {
                sb.append(field.getName());
                sb.append(": ");
                sb.append(field.get(this));
            } catch (IllegalAccessException e) {
                System.out.println(e);
            }
            sb.append(property);
        }
        sb.append("}");
        return sb.toString();
    }
}
