package com.becandid.candid.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.util.Log;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PersistentCookieStore implements CookieStore {
    private static PersistentCookieStore cookieStore;
    private static final aym sGson = new aym();
    private Map<URI, Set<HttpCookie>> allCookies;
    private SharedPreferences gsonSharedPreferences;

    private PersistentCookieStore(Context context) {
        loadAllFromPersistence(context);
    }

    private static URI cookieUri(URI uri, HttpCookie httpCookie) {
        URI uri2 = uri;
        if (httpCookie.getDomain() == null) {
            return uri2;
        }
        try {
            return new URI(uri.getScheme() == null ? "http" : uri.getScheme(), httpCookie.getDomain(), httpCookie.getPath() == null ? "/" : httpCookie.getPath(), null);
        } catch (URISyntaxException e) {
            Log.w("PersistentCookieStore", e);
            return uri2;
        }
    }

    private String getCookieKey(URI uri, HttpCookie httpCookie) {
        return uri.toString() + '|' + httpCookie.getName();
    }

    public static PersistentCookieStore getCookieStore(Context context) {
        if (cookieStore == null) {
            cookieStore = new PersistentCookieStore(context.getApplicationContext());
        }
        return cookieStore;
    }

    private String getGsonSerializedCookie(HttpCookie httpCookie) {
        return sGson.mo8360a((Object) httpCookie);
    }

    private List<HttpCookie> getValidCookies(URI uri) {
        HashSet hashSet = new HashSet();
        for (Entry entry : this.allCookies.entrySet()) {
            URI uri2 = (URI) entry.getKey();
            if (!TextUtils.isEmpty(uri2.getHost())) {
                String host = uri2.getHost();
                String str = host;
                if (!host.startsWith(".")) {
                    str = "." + host;
                }
                if (uri.getHost().equals(host) || uri.getHost().endsWith(str) || (uri.getHost().contains(".dev.") && uri.getHost().endsWith(host))) {
                    if (uri2.getPath() == null || "/".equals(uri2.getPath()) || uri2.getPath().equals(uri.getPath())) {
                        hashSet.addAll((Collection) entry.getValue());
                    }
                }
            }
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            HttpCookie httpCookie = (HttpCookie) it.next();
            if (httpCookie.hasExpired() && !"u".equals(httpCookie.getName())) {
                arrayList.add(httpCookie);
                it.remove();
            }
        }
        if (!arrayList.isEmpty()) {
            removeFromPersistence(uri, (List<HttpCookie>) arrayList);
        }
        return new ArrayList(hashSet);
    }

    private void loadAllFromPersistence(Context context) {
        this.allCookies = new ArrayMap();
        loadCookies(context, "com.becandid.likes.cookieStore");
    }

    private void loadCookies(Context context, String str) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(str, 0);
        this.gsonSharedPreferences = sharedPreferences;
        for (Entry entry : sharedPreferences.getAll().entrySet()) {
            try {
                URI uri = new URI(((String) entry.getKey()).split("\\|", 2)[0]);
                HttpCookie httpCookie = (HttpCookie) sGson.mo8357a((String) entry.getValue(), HttpCookie.class);
                if (httpCookie != null) {
                    Set set = (Set) this.allCookies.get(uri);
                    if (set == null) {
                        set = new HashSet();
                        this.allCookies.put(uri, set);
                    }
                    set.remove(httpCookie);
                    set.add(httpCookie);
                }
            } catch (URISyntaxException e) {
                Log.w("PersistentCookieStore", e);
            }
        }
    }

    private void removeAllFromPersistence() {
        this.gsonSharedPreferences.edit().clear().apply();
    }

    private void removeFromPersistence(URI uri, HttpCookie httpCookie) {
        Editor edit = this.gsonSharedPreferences.edit();
        edit.remove(getCookieKey(uri, httpCookie));
        edit.apply();
    }

    private void removeFromPersistence(URI uri, List<HttpCookie> list) {
        Editor edit = this.gsonSharedPreferences.edit();
        for (HttpCookie cookieKey : list) {
            edit.remove(getCookieKey(uri, cookieKey));
        }
        edit.apply();
    }

    private void saveToPersistence(URI uri, HttpCookie httpCookie) {
        Editor edit = this.gsonSharedPreferences.edit();
        edit.putString(getCookieKey(uri, httpCookie), getGsonSerializedCookie(httpCookie));
        edit.apply();
    }

    public synchronized void add(URI uri, HttpCookie httpCookie) {
        Log.d("PersistentCookieStore", "add: " + uri.toString());
        URI cookieUri = cookieUri(uri, httpCookie);
        Set set = (Set) this.allCookies.get(cookieUri);
        if (set == null) {
            set = new HashSet();
            this.allCookies.put(cookieUri, set);
        }
        set.remove(httpCookie);
        set.add(httpCookie);
        saveToPersistence(cookieUri, httpCookie);
    }

    public synchronized List<HttpCookie> get(URI uri) {
        return getValidCookies(uri);
    }

    public Map<URI, Set<HttpCookie>> getAllCookies() {
        return this.allCookies;
    }

    public synchronized List<HttpCookie> getCookies() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (URI validCookies : this.allCookies.keySet()) {
            arrayList.addAll(getValidCookies(validCookies));
        }
        return arrayList;
    }

    public synchronized List<URI> getURIs() {
        return new ArrayList(this.allCookies.keySet());
    }

    public synchronized boolean remove(URI uri, HttpCookie httpCookie) {
        boolean z;
        Set set = (Set) this.allCookies.get(uri);
        z = set != null && set.remove(httpCookie);
        if (z) {
            removeFromPersistence(uri, httpCookie);
        }
        return z;
    }

    public synchronized boolean removeAll() {
        this.allCookies.clear();
        removeAllFromPersistence();
        return true;
    }

    public void setAllCookies(Map<URI, Set<HttpCookie>> map) {
        this.allCookies = map;
        for (URI uri : this.allCookies.keySet()) {
            for (HttpCookie saveToPersistence : (Set) this.allCookies.get(uri)) {
                saveToPersistence(uri, saveToPersistence);
            }
        }
    }
}
