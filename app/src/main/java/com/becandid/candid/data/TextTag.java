package com.becandid.candid.data;

public class TextTag {
    public int endIndex;
    public int groupId;
    public int startIndex;

    public TextTag(int i, int i2, int i3) {
        this.startIndex = i;
        this.endIndex = i2;
        this.groupId = i3;
    }

    public String toString() {
        return Integer.toString(this.groupId) + "," + Integer.toString(this.startIndex) + "," + Integer.toString(this.endIndex);
    }
}
