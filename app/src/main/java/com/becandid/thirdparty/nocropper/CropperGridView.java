package com.becandid.thirdparty.nocropper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.os.Handler;
import android.support.p003v7.widget.helper.ItemTouchHelper.Callback;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;

public class CropperGridView extends View {

    /* renamed from: a */
    private long f7582a = 1500;

    /* renamed from: b */
    private Paint f7583b;

    /* renamed from: c */
    private int f7584c = 268435455;

    /* renamed from: d */
    private int f7585d = Callback.DEFAULT_DRAG_ANIMATION_DURATION;

    /* renamed from: e */
    private int f7586e = 3;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public boolean f7587f = false;

    /* renamed from: g */
    private Handler f7588g;

    /* renamed from: h */
    private Path f7589h;

    /* renamed from: i */
    private Runnable f7590i = new Runnable() {
        public void run() {
            CropperGridView.this.f7587f = false;
            CropperGridView.this.invalidate();
        }
    };

    public CropperGridView(Context context) {
        super(context);
        m10045a(context, (AttributeSet) null);
    }

    public CropperGridView(Context context, AttributeSet attributeSet) {
        super(context);
        m10045a(context, attributeSet);
    }

    public CropperGridView(Context context, AttributeSet attributeSet, int i) {
        super(context);
        m10045a(context, attributeSet);
    }

    /* renamed from: a */
    private void m10045a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2434a.CropperView);
            this.f7584c = obtainStyledAttributes.getColor(0, this.f7584c);
            float f = 255.0f * obtainStyledAttributes.getFloat(2, 1.0f);
            if (f < 0.0f) {
                f = 0.0f;
            } else if (f > 255.0f) {
                f = 255.0f;
            }
            this.f7585d = (int) f;
            this.f7586e = obtainStyledAttributes.getDimensionPixelOffset(1, this.f7586e);
            obtainStyledAttributes.recycle();
        }
        this.f7583b = new Paint();
        this.f7583b.setColor(this.f7584c);
        this.f7583b.setAntiAlias(true);
        this.f7583b.setStyle(Style.STROKE);
        this.f7583b.setStrokeCap(Cap.ROUND);
        this.f7583b.setStrokeWidth((float) this.f7586e);
        this.f7583b.setAlpha(this.f7585d);
        this.f7589h = new Path();
        this.f7588g = new Handler();
        if (isInEditMode()) {
            this.f7587f = true;
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f7587f) {
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            this.f7589h.reset();
            this.f7589h.moveTo((float) (width / 3), 0.0f);
            this.f7589h.lineTo((float) (width / 3), (float) height);
            this.f7589h.moveTo((float) ((width * 2) / 3), 0.0f);
            this.f7589h.lineTo((float) ((width * 2) / 3), (float) height);
            this.f7589h.moveTo(0.0f, (float) (height / 3));
            this.f7589h.lineTo((float) width, (float) (height / 3));
            this.f7589h.moveTo(0.0f, (float) ((height * 2) / 3));
            this.f7589h.lineTo((float) width, (float) ((height * 2) / 3));
            canvas.drawPath(this.f7589h, this.f7583b);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int i3 = getContext().getResources().getConfiguration().orientation;
        if (i3 == 1 || i3 == 0) {
            int size = MeasureSpec.getSize(i);
            setMeasuredDimension(size, MeasureSpec.makeMeasureSpec((int) ((((double) size) * CropperView.f7631a) + 0.5d), 1073741824));
            return;
        }
        int size2 = MeasureSpec.getSize(i2);
        setMeasuredDimension(MeasureSpec.makeMeasureSpec(size2, 1073741824), size2);
    }

    public void setShowGrid(boolean z) {
        if (this.f7587f != z) {
            this.f7587f = z;
            if (this.f7587f) {
                this.f7588g.removeCallbacks(this.f7590i);
                invalidate();
                return;
            }
            this.f7588g.postDelayed(this.f7590i, this.f7582a);
        }
    }
}
