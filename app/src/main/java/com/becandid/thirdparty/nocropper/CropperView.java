package com.becandid.thirdparty.nocropper;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.becandid.thirdparty.nocropper.CropperImageView.C1938a;

public class CropperView extends FrameLayout {

    /* renamed from: a */
    public static double f7631a = 1.1d;

    /* renamed from: b */
    private CropperImageView f7632b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public CropperGridView f7633c;

    /* renamed from: com.becandid.thirdparty.nocropper.CropperView$a */
    class C1942a implements C1938a {
        private C1942a() {
        }

        /* renamed from: a */
        public void mo11036a() {
            CropperView.this.f7633c.setShowGrid(true);
        }

        /* renamed from: b */
        public void mo11037b() {
            CropperView.this.f7633c.setShowGrid(false);
        }
    }

    public CropperView(Context context) {
        super(context);
        m10069a(context, null);
    }

    public CropperView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m10069a(context, attributeSet);
    }

    public CropperView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m10069a(context, attributeSet);
    }

    @TargetApi(21)
    public CropperView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m10069a(context, attributeSet);
    }

    /* renamed from: a */
    private void m10069a(Context context, AttributeSet attributeSet) {
        this.f7632b = new CropperImageView(context, attributeSet);
        this.f7633c = new CropperGridView(context, attributeSet);
        LayoutParams layoutParams = new LayoutParams(-1, 0);
        if (context.getResources().getConfiguration().orientation == 2) {
            layoutParams.width = 0;
            layoutParams.height = -1;
        }
        addView(this.f7632b, 0, layoutParams);
        addView(this.f7633c, 1, layoutParams);
        this.f7632b.setGestureCallback(new C1942a());
    }

    public Bitmap getCroppedBitmap() {
        return this.f7632b.getCroppedBitmap();
    }

    public int getCropperWidth() {
        if (this.f7632b != null) {
            return this.f7632b.getWidth();
        }
        return 0;
    }

    public float getMaxZoom() {
        return this.f7632b.getMaxZoom();
    }

    public float getMinZoom() {
        return this.f7632b.getMinZoom();
    }

    public int getPaddingColor() {
        return this.f7632b.getPaddingColor();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int i3 = getContext().getResources().getConfiguration().orientation;
        if (i3 == 1 || i3 == 0) {
            int size = MeasureSpec.getSize(i);
            setMeasuredDimension(size, MeasureSpec.makeMeasureSpec((int) ((((double) size) * f7631a) + 0.5d), 1073741824));
            return;
        }
        int size2 = MeasureSpec.getSize(i2);
        setMeasuredDimension(MeasureSpec.makeMeasureSpec(size2, 1073741824), size2);
    }

    public void setDebug(boolean z) {
        this.f7632b.setDEBUG(z);
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.f7632b.setImageBitmap(bitmap);
    }

    public void setMakeSquare(boolean z) {
        this.f7632b.setMakeSquare(z);
    }

    public void setMaxZoom(float f) {
        this.f7632b.setMaxZoom(f);
    }

    public void setMinZoom(float f) {
        this.f7632b.setMinZoom(f);
    }

    public void setPaddingColor(int i) {
        this.f7632b.setPaddingColor(i);
    }

    public void setPreScaling(boolean z) {
        this.f7632b.setDoPreScaling(z);
    }
}
