package com.becandid.thirdparty.nocropper;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class CropperImageView extends ImageView {

    /* renamed from: a */
    protected GestureDetector f7592a;

    /* renamed from: b */
    protected ScaleGestureDetector f7593b;

    /* renamed from: c */
    public boolean f7594c = false;

    /* renamed from: d */
    private float[] f7595d = new float[9];

    /* renamed from: e */
    private C1939b f7596e;

    /* renamed from: f */
    private C1940c f7597f;

    /* renamed from: g */
    private float f7598g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public float f7599h;

    /* renamed from: i */
    private float f7600i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public float f7601j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public float f7602k;

    /* renamed from: l */
    private boolean f7603l = false;

    /* renamed from: m */
    private boolean f7604m = true;

    /* renamed from: n */
    private Bitmap f7605n;

    /* renamed from: o */
    private boolean f7606o = false;

    /* renamed from: p */
    private float f7607p;

    /* renamed from: q */
    private boolean f7608q = false;

    /* renamed from: r */
    private C1938a f7609r;

    /* renamed from: s */
    private boolean f7610s = true;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public boolean f7611t = false;

    /* renamed from: u */
    private int f7612u = Color.rgb(246, 246, 246);

    /* renamed from: com.becandid.thirdparty.nocropper.CropperImageView$a */
    public interface C1938a {
        /* renamed from: a */
        void mo11036a();

        /* renamed from: b */
        void mo11037b();
    }

    /* renamed from: com.becandid.thirdparty.nocropper.CropperImageView$b */
    class C1939b extends SimpleOnGestureListener {
        private C1939b() {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            if (motionEvent != null && motionEvent2 != null && motionEvent.getPointerCount() <= 1 && motionEvent2.getPointerCount() <= 1) {
                CropperImageView.this.mo11004a(motionEvent, motionEvent2, f, f2);
            }
            return false;
        }
    }

    /* renamed from: com.becandid.thirdparty.nocropper.CropperImageView$c */
    class C1940c extends SimpleOnScaleGestureListener {

        /* renamed from: a */
        protected boolean f7629a;

        private C1940c() {
            this.f7629a = false;
        }

        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            Matrix imageMatrix = CropperImageView.this.getImageMatrix();
            CropperImageView.this.f7601j = scaleGestureDetector.getFocusX();
            CropperImageView.this.f7602k = scaleGestureDetector.getFocusY();
            imageMatrix.postScale(scaleGestureDetector.getScaleFactor(), scaleGestureDetector.getScaleFactor(), scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
            CropperImageView.this.setImageMatrix(imageMatrix);
            CropperImageView.this.invalidate();
            return true;
        }
    }

    public CropperImageView(Context context) {
        super(context);
        m10054a(context, (AttributeSet) null);
    }

    public CropperImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m10054a(context, attributeSet);
    }

    public CropperImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m10054a(context, attributeSet);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public float m10047a(Matrix matrix) {
        return m10048a(matrix, 0);
    }

    /* renamed from: a */
    private float m10048a(Matrix matrix, int i) {
        matrix.getValues(this.f7595d);
        return this.f7595d[i];
    }

    /* renamed from: a */
    private void m10052a(final float f, final float f2) {
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{0, 20});
        ofInt.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Matrix imageMatrix = CropperImageView.this.getImageMatrix();
                imageMatrix.postTranslate(f / 20.0f, f2 / 20.0f);
                CropperImageView.this.setImageMatrix(imageMatrix);
                CropperImageView.this.invalidate();
            }
        });
        ofInt.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
                CropperImageView.this.f7611t = false;
            }

            public void onAnimationEnd(Animator animator) {
                CropperImageView.this.f7611t = false;
            }

            public void onAnimationRepeat(Animator animator) {
                CropperImageView.this.f7611t = true;
            }

            public void onAnimationStart(Animator animator) {
                CropperImageView.this.f7611t = true;
            }
        });
        ofInt.start();
    }

    /* renamed from: a */
    private void m10053a(float f, float f2, float f3, float f4, float f5, float f6) {
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{0, 20});
        final float f7 = f6;
        final float f8 = f5;
        final float f9 = f2;
        final float f10 = f;
        final float f11 = f4;
        final float f12 = f3;
        ofInt.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Matrix imageMatrix = CropperImageView.this.getImageMatrix();
                imageMatrix.reset();
                Integer num = (Integer) valueAnimator.getAnimatedValue();
                imageMatrix.postScale((((f7 - f8) * ((float) num.intValue())) / 20.0f) + f8, (((f7 - f8) * ((float) num.intValue())) / 20.0f) + f8);
                imageMatrix.postTranslate((((f9 - f10) * ((float) num.intValue())) / 20.0f) + f10, (((f11 - f12) * ((float) num.intValue())) / 20.0f) + f12);
                CropperImageView.this.setImageMatrix(imageMatrix);
                CropperImageView.this.invalidate();
            }
        });
        ofInt.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
                CropperImageView.this.f7611t = false;
            }

            public void onAnimationEnd(Animator animator) {
                CropperImageView.this.f7611t = false;
            }

            public void onAnimationRepeat(Animator animator) {
                CropperImageView.this.f7611t = true;
            }

            public void onAnimationStart(Animator animator) {
                CropperImageView.this.f7611t = true;
            }
        });
        ofInt.start();
    }

    /* renamed from: a */
    private void m10054a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2434a.CropperView);
            this.f7612u = obtainStyledAttributes.getColor(3, this.f7612u);
            this.f7608q = obtainStyledAttributes.getBoolean(4, false);
        }
        this.f7596e = new C1939b();
        this.f7592a = new GestureDetector(context, this.f7596e, null, true);
        this.f7597f = new C1940c();
        this.f7593b = new ScaleGestureDetector(context, this.f7597f);
        setScaleType(ScaleType.MATRIX);
    }

    /* renamed from: a */
    private void m10055a(Drawable drawable, int i) {
        if (drawable == null) {
            if (this.f7594c) {
                Log.e("CropperImageView", "Drawable is null. I can't fit anything");
            }
        } else if (i != 0) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (this.f7594c) {
                Log.i("CropperImageView", "drawable size: (" + intrinsicWidth + " ," + intrinsicHeight + ")");
            }
            float min = ((float) Math.min(intrinsicWidth, intrinsicHeight)) / ((float) i);
            Matrix matrix = new Matrix();
            matrix.setScale(1.0f / min, 1.0f / min);
            matrix.postTranslate((((float) i) - (((float) intrinsicWidth) / min)) / 2.0f, (((float) i) - (((float) intrinsicHeight) / min)) / 2.0f);
            setImageMatrix(matrix);
        } else if (this.f7594c) {
            Log.e("CropperImageView", "Frame Dimension is 0. I'm quite boggled by it.");
        }
    }

    /* renamed from: a */
    private boolean m10056a(MotionEvent motionEvent) {
        float width;
        float f;
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return false;
        }
        Matrix imageMatrix = getImageMatrix();
        float a = m10048a(imageMatrix, 2);
        float a2 = m10048a(imageMatrix, 5);
        float a3 = m10048a(imageMatrix, 0);
        float a4 = m10048a(imageMatrix, 4);
        if (this.f7594c) {
            Log.i("CropperImageView", "onUp: " + a + " " + a2);
            Log.i("CropperImageView", "scale: " + a3);
            Log.i("CropperImageView", "min, max, base zoom: " + this.f7598g + ", " + this.f7599h + ", " + this.f7600i);
            Log.i("CropperImageView", "imageview size: " + getWidth() + " " + getHeight());
            Log.i("CropperImageView", "drawable size: " + drawable.getIntrinsicWidth() + " " + drawable.getIntrinsicHeight());
            Log.i("CropperImageView", "scaled drawable size: " + (((float) drawable.getIntrinsicWidth()) * a3) + " " + (((float) drawable.getIntrinsicHeight()) * a4));
        }
        if (a3 <= this.f7598g) {
            if (this.f7594c) {
                Log.i("CropperImageView", "set scale: " + this.f7598g);
            }
            float width2 = ((float) (getWidth() / 2)) - ((this.f7598g * ((float) drawable.getIntrinsicWidth())) / 2.0f);
            float height = ((float) (getHeight() / 2)) - ((this.f7598g * ((float) drawable.getIntrinsicHeight())) / 2.0f);
            if (mo11003a()) {
                m10053a(a, width2, a2, height, a3, this.f7598g);
            } else {
                imageMatrix.reset();
                imageMatrix.setScale(this.f7598g, this.f7598g);
                imageMatrix.postTranslate(width2, height);
                setImageMatrix(imageMatrix);
                invalidate();
                if (this.f7594c) {
                    Log.i("CropperImageView", "scale after invalidate: " + m10047a(imageMatrix));
                }
            }
            return true;
        } else if (a3 < this.f7600i) {
            int intrinsicHeight = drawable.getIntrinsicHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (intrinsicHeight <= intrinsicWidth) {
                f = ((float) (getHeight() / 2)) - ((((float) intrinsicHeight) * a3) / 2.0f);
                if (a >= 0.0f) {
                    width = 0.0f;
                } else {
                    float width3 = ((float) getWidth()) - (((float) drawable.getIntrinsicWidth()) * a3);
                    width = a < width3 ? width3 : a;
                }
            } else {
                width = ((float) (getWidth() / 2)) - ((((float) intrinsicWidth) * a3) / 2.0f);
                if (a2 >= 0.0f) {
                    f = 0.0f;
                } else {
                    float height2 = ((float) getHeight()) - (((float) drawable.getIntrinsicHeight()) * a4);
                    f = a2 < height2 ? height2 : a2;
                }
            }
            if (mo11003a()) {
                imageMatrix.reset();
                imageMatrix.postScale(a3, a3);
                imageMatrix.postTranslate(a, a2);
                setImageMatrix(imageMatrix);
                m10052a(width - a, f - a2);
            } else {
                imageMatrix.reset();
                imageMatrix.postScale(a3, a3);
                imageMatrix.postTranslate(width, f);
                setImageMatrix(imageMatrix);
                invalidate();
            }
            return true;
        } else if (!this.f7603l || a3 <= this.f7599h) {
            if (this.f7594c) {
                Log.i("CropperImageView", "adjust to sides");
            }
            m10060b();
            return true;
        } else {
            if (this.f7594c) {
                Log.i("CropperImageView", "set to max zoom");
                Log.i("CropperImageView", "isMaxZoomSet: " + this.f7603l);
            }
            if (mo11003a()) {
                m10062c();
            } else {
                imageMatrix.postScale(this.f7599h / a3, this.f7599h / a3, this.f7601j, this.f7602k);
                setImageMatrix(imageMatrix);
                invalidate();
                m10060b();
            }
            return true;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m10060b() {
        float f;
        float f2;
        boolean z = false;
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return false;
        }
        Matrix imageMatrix = getImageMatrix();
        float a = m10048a(imageMatrix, 2);
        float a2 = m10048a(imageMatrix, 5);
        float a3 = m10048a(imageMatrix, 2);
        float a4 = m10048a(imageMatrix, 5);
        float a5 = m10048a(imageMatrix, 0);
        float a6 = m10048a(imageMatrix, 4);
        if (a3 > 0.0f) {
            f = -a3;
            z = true;
        } else {
            float width = ((float) getWidth()) - (((float) drawable.getIntrinsicWidth()) * a5);
            if (a3 < width) {
                f = width - a3;
                z = true;
            } else {
                f = 0.0f;
            }
        }
        if (a4 > 0.0f) {
            f2 = -a4;
            z = true;
        } else {
            float height = ((float) getHeight()) - (((float) drawable.getIntrinsicHeight()) * a6);
            if (a4 < height) {
                f2 = height - a4;
                z = true;
            } else {
                f2 = 0.0f;
            }
        }
        if (z) {
            if (mo11003a()) {
                m10052a(f, f2);
            } else {
                imageMatrix.postTranslate(f, f2);
                setImageMatrix(imageMatrix);
                invalidate();
            }
        }
        return z;
    }

    /* renamed from: c */
    private void m10062c() {
        final float a = m10047a(getImageMatrix());
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{0, 20});
        ofInt.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Matrix imageMatrix = CropperImageView.this.getImageMatrix();
                if (CropperImageView.this.m10047a(imageMatrix) > CropperImageView.this.f7599h) {
                    double pow = Math.pow((double) (CropperImageView.this.f7599h / a), 0.05000000074505806d);
                    imageMatrix.postScale((float) pow, (float) pow, CropperImageView.this.f7601j, CropperImageView.this.f7602k);
                    CropperImageView.this.setImageMatrix(imageMatrix);
                    CropperImageView.this.invalidate();
                }
            }
        });
        ofInt.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
                CropperImageView.this.f7611t = false;
            }

            public void onAnimationEnd(Animator animator) {
                CropperImageView.this.f7611t = false;
                CropperImageView.this.m10060b();
            }

            public void onAnimationRepeat(Animator animator) {
                CropperImageView.this.f7611t = true;
            }

            public void onAnimationStart(Animator animator) {
                CropperImageView.this.f7611t = true;
            }
        });
        ofInt.start();
    }

    /* renamed from: a */
    public boolean mo11003a() {
        return this.f7610s;
    }

    /* renamed from: a */
    public boolean mo11004a(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        Matrix imageMatrix = getImageMatrix();
        imageMatrix.postTranslate(-f, -f2);
        setImageMatrix(imageMatrix);
        invalidate();
        return true;
    }

    public Bitmap getCroppedBitmap() {
        Bitmap createBitmap;
        if (this.f7605n == null) {
            Log.e("CropperImageView", "original image is not available");
            return null;
        }
        Matrix imageMatrix = getImageMatrix();
        if (this.f7606o) {
            imageMatrix.postScale(1.0f / this.f7607p, 1.0f / this.f7607p);
        }
        float a = m10048a(imageMatrix, 2);
        float a2 = m10048a(imageMatrix, 5);
        float a3 = m10048a(imageMatrix, 0);
        if (this.f7594c) {
            Log.i("CropperImageView", "xTrans: " + a + ", yTrans: " + a2 + " , scale: " + a3);
        }
        if (this.f7594c) {
            Log.i("CropperImageView", "old bitmap: " + this.f7605n.getWidth() + " " + this.f7605n.getHeight());
        }
        if (a > 0.0f && a2 > 0.0f && a3 <= this.f7598g) {
            return this.f7608q ? C2763km.m14819a(this.f7605n, this.f7612u) : this.f7605n;
        }
        float f = (-a2) / a3;
        float height = ((float) getHeight()) / a3;
        float f2 = (-a) / a3;
        float width = ((float) getWidth()) / a3;
        if (this.f7594c) {
            Log.i("CropperImageView", "cropY: " + f);
            Log.i("CropperImageView", "Y: " + height);
            Log.i("CropperImageView", "cropX: " + f2);
            Log.i("CropperImageView", "X: " + width);
        }
        if (f + height > ((float) this.f7605n.getHeight())) {
            f = ((float) this.f7605n.getHeight()) - height;
            if (this.f7594c) {
                Log.i("CropperImageView", "readjust cropY to: " + f);
            }
        }
        if (f < 0.0f) {
            f = 0.0f;
            height = (float) this.f7605n.getHeight();
            if (this.f7594c) {
                Log.i("CropperImageView", "readjust cropY to: " + 0.0f);
            }
        }
        if (f2 + width > ((float) this.f7605n.getWidth())) {
            f2 = ((float) this.f7605n.getWidth()) - width;
            if (this.f7594c) {
                Log.i("CropperImageView", "readjust cropX to: " + f2);
            }
        }
        if (f2 < 0.0f) {
            f2 = 0.0f;
            width = (float) this.f7605n.getWidth();
            if (this.f7594c) {
                Log.i("CropperImageView", "readjust cropX to: " + 0.0f);
            }
        }
        if (this.f7605n.getHeight() <= this.f7605n.getWidth()) {
            if (a2 >= 0.0f) {
                createBitmap = Bitmap.createBitmap(this.f7605n, (int) f2, 0, (int) width, this.f7605n.getHeight(), null, true);
                if (this.f7608q) {
                    createBitmap = C2763km.m14819a(createBitmap, this.f7612u);
                }
            } else {
                createBitmap = Bitmap.createBitmap(this.f7605n, (int) f2, (int) f, (int) width, (int) height, null, true);
            }
            if (!this.f7594c) {
                return createBitmap;
            }
            Log.i("CropperImageView", "width should be: " + this.f7605n.getWidth());
            Log.i("CropperImageView", "crop bitmap: " + createBitmap.getWidth() + " " + createBitmap.getHeight());
            return createBitmap;
        } else if (a < 0.0f) {
            return Bitmap.createBitmap(this.f7605n, (int) f2, (int) f, (int) width, (int) height, null, true);
        } else {
            Bitmap createBitmap2 = Bitmap.createBitmap(this.f7605n, 0, (int) f, this.f7605n.getWidth(), (int) height, null, true);
            return this.f7608q ? C2763km.m14819a(createBitmap2, this.f7612u) : createBitmap2;
        }
    }

    public float getMaxZoom() {
        return this.f7599h;
    }

    public float getMinZoom() {
        return this.f7598g;
    }

    public int getPaddingColor() {
        return this.f7612u;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.f7594c) {
            Log.i("CropperImageView", "onLayout: " + z + " [" + i + ", " + i2 + ", " + i3 + ", " + i4 + "]");
        }
        if ((z || this.f7604m) && this.f7604m) {
            Drawable drawable = getDrawable();
            if (drawable != null) {
                this.f7598g = ((float) (i4 - i2)) / ((float) Math.max(drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth()));
                this.f7600i = ((float) (i4 - i2)) / ((float) Math.min(drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth()));
                m10055a(drawable, i4 - i2);
                this.f7604m = false;
            } else if (this.f7594c) {
                Log.e("CropperImageView", "drawable is null");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int i3 = getContext().getResources().getConfiguration().orientation;
        if (i3 == 1 || i3 == 0) {
            setMeasuredDimension(i, MeasureSpec.makeMeasureSpec((int) ((((double) MeasureSpec.getSize(i)) * CropperView.f7631a) + 0.5d), 1073741824));
            return;
        }
        int size = MeasureSpec.getSize(i2);
        setMeasuredDimension(MeasureSpec.makeMeasureSpec(size, 1073741824), size);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f7611t) {
            return true;
        }
        if (motionEvent.getActionMasked() == 0 && this.f7609r != null) {
            this.f7609r.mo11036a();
        }
        this.f7593b.onTouchEvent(motionEvent);
        if (!this.f7593b.isInProgress()) {
            this.f7592a.onTouchEvent(motionEvent);
        }
        switch (motionEvent.getAction()) {
            case 1:
                if (this.f7609r != null) {
                    this.f7609r.mo11037b();
                }
                return m10056a(motionEvent);
            default:
                return true;
        }
    }

    public void setDEBUG(boolean z) {
        this.f7594c = z;
    }

    public void setDoPreScaling(boolean z) {
        this.f7606o = z;
    }

    public void setGestureCallback(C1938a aVar) {
        this.f7609r = aVar;
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.f7604m = true;
        if (bitmap == null) {
            this.f7605n = null;
            super.setImageBitmap(null);
            return;
        }
        if (bitmap.getHeight() > 1280 || bitmap.getWidth() > 1280) {
            Log.w("CropperImageView", "Bitmap size greater than 1280. This might cause memory issues");
        }
        this.f7605n = bitmap;
        if (this.f7606o) {
            this.f7607p = ((float) Math.max(bitmap.getWidth(), bitmap.getHeight())) / ((float) getWidth());
            super.setImageBitmap(Bitmap.createScaledBitmap(bitmap, (int) (((float) bitmap.getWidth()) / this.f7607p), (int) (((float) bitmap.getHeight()) / this.f7607p), false));
        } else {
            this.f7607p = 1.0f;
            super.setImageBitmap(bitmap);
        }
        requestLayout();
    }

    public void setMakeSquare(boolean z) {
        this.f7608q = z;
    }

    public void setMaxZoom(float f) {
        if (f <= 0.0f) {
            Log.e("CropperImageView", "Max zoom must be greater than 0");
            return;
        }
        this.f7599h = f;
        this.f7603l = true;
    }

    public void setMinZoom(float f) {
        if (f <= 0.0f) {
            Log.e("CropperImageView", "Min zoom must be greater than 0");
        } else {
            this.f7598g = f;
        }
    }

    public void setPaddingColor(int i) {
        this.f7612u = i;
    }

    public void setShowAnimation(boolean z) {
        this.f7610s = z;
    }
}
