package com.becandid.thirdparty;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.View;

public class BlurTask extends AsyncTask<Void, Void, Void> {

    /* renamed from: a */
    private Activity f7560a;

    /* renamed from: b */
    private int f7561b;

    /* renamed from: c */
    private Bitmap f7562c;

    /* renamed from: d */
    private String f7563d;

    /* renamed from: e */
    private String f7564e;

    /* renamed from: f */
    private BadgeType f7565f;

    /* renamed from: g */
    private View f7566g;

    /* renamed from: h */
    private String f7567h;

    /* renamed from: i */
    private String f7568i;

    /* renamed from: j */
    private String f7569j;

    public enum BadgeType {
        BADGE,
        MOD,
        POST_QUALITY_SCORE,
        ME_QUALITY_SCORE,
        MESSAGE_ENABLED_NEW,
        MESSAGE_ENABLED_TAB,
        MESSAGE_ADD_NICKNAME,
        MESSAGE_SECOND_NICKNAME,
        COMMUNITY_ONBOARDING,
        POST_QUALITY_FEEDBACK,
        POST_MOVED
    }

    public BlurTask(Activity activity, int i) {
        this.f7560a = activity;
        this.f7561b = i;
    }

    public BlurTask(Activity activity, int i, String str) {
        this.f7560a = activity;
        this.f7561b = i;
        this.f7564e = str;
    }

    public BlurTask(Activity activity, View view, BadgeType badgeType) {
        this.f7560a = activity;
        this.f7566g = view;
        this.f7565f = badgeType;
    }

    public BlurTask(Activity activity, View view, BadgeType badgeType, String str, String str2) {
        this.f7560a = activity;
        this.f7566g = view;
        this.f7565f = badgeType;
        this.f7568i = str;
        this.f7569j = str2;
    }

    public BlurTask(Activity activity, View view, String str, BadgeType badgeType, String str2, String str3) {
        this.f7560a = activity;
        this.f7566g = view;
        this.f7565f = badgeType;
        this.f7567h = str;
        this.f7568i = str2;
        this.f7569j = str3;
    }

    /* renamed from: a */
    private View m10041a() {
        return this.f7560a.findViewById(this.f7561b);
    }

    /* renamed from: b */
    private String m10042b() {
        Bitmap a = Blur.m14761a(this.f7560a, this.f7562c, 24);
        String a2 = BlurUtils.m14766a(this.f7560a, a);
        this.f7562c.recycle();
        a.recycle();
        return a2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        this.f7563d = m10042b();
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Void voidR) {
        if (this.f7564e != null) {
            RxBus.m14573a().mo14349a(new C2646c(this.f7563d, this.f7564e));
        } else if (this.f7565f != null) {
            RxBus.m14573a().mo14349a(new C2638b(this.f7563d, this.f7565f, this.f7567h, this.f7568i, this.f7569j));
        } else {
            RxBus.m14573a().mo14349a(new C2647d(this.f7563d));
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.f7566g == null) {
            this.f7566g = m10041a();
        }
        this.f7562c = BlurUtils.m14762a(this.f7566g, Color.parseColor("#FF000000"));
    }
}
