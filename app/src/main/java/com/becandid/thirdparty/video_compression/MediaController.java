package com.becandid.thirdparty.video_compression;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecList;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import java.io.File;
import java.nio.ByteBuffer;

@SuppressLint({"NewApi"})
public class MediaController {

    /* renamed from: a */
    private static volatile MediaController f7635a = null;

    /* renamed from: b */
    private boolean f7636b = true;

    @SuppressLint({"NewApi"})
    /* renamed from: a */
    public static int m10072a(MediaCodecInfo mediaCodecInfo, String str) {
        CodecCapabilities capabilitiesForType = mediaCodecInfo.getCapabilitiesForType(str);
        int i = 0;
        for (int i2 = 0; i2 < capabilitiesForType.colorFormats.length; i2++) {
            int i3 = capabilitiesForType.colorFormats[i2];
            if (m10078a(i3)) {
                i = i3;
                if (!mediaCodecInfo.getName().equals("OMX.SEC.AVC.Encoder") || i3 != 19) {
                    return i3;
                }
            }
        }
        return i;
    }

    @TargetApi(16)
    /* renamed from: a */
    private int m10073a(MediaExtractor mediaExtractor, boolean z) {
        int trackCount = mediaExtractor.getTrackCount();
        for (int i = 0; i < trackCount; i++) {
            String string = mediaExtractor.getTrackFormat(i).getString("mime");
            if (z) {
                if (string.startsWith("audio/")) {
                    return i;
                }
            } else if (string.startsWith("video/")) {
                return i;
            }
        }
        return -5;
    }

    @TargetApi(16)
    /* renamed from: a */
    private long m10074a(MediaExtractor mediaExtractor, MP4Builder koVar, BufferInfo bufferInfo, long j, long j2, File file, boolean z) throws Exception {
        int a = m10073a(mediaExtractor, z);
        if (a < 0) {
            return -1;
        }
        mediaExtractor.selectTrack(a);
        MediaFormat trackFormat = mediaExtractor.getTrackFormat(a);
        int a2 = koVar.mo14485a(trackFormat, z);
        int integer = trackFormat.getInteger("max-input-size");
        boolean z2 = false;
        if (j > 0) {
            mediaExtractor.seekTo(j, 0);
        } else {
            mediaExtractor.seekTo(0, 0);
        }
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(integer);
        long j3 = -1;
        while (!z2) {
            boolean z3 = false;
            int sampleTrackIndex = mediaExtractor.getSampleTrackIndex();
            if (sampleTrackIndex == a) {
                bufferInfo.size = mediaExtractor.readSampleData(allocateDirect, 0);
                if (bufferInfo.size < 0) {
                    bufferInfo.size = 0;
                    z3 = true;
                } else {
                    bufferInfo.presentationTimeUs = mediaExtractor.getSampleTime();
                    if (j > 0 && j3 == -1) {
                        j3 = bufferInfo.presentationTimeUs;
                    }
                    if (j2 < 0 || bufferInfo.presentationTimeUs < j2) {
                        bufferInfo.offset = 0;
                        bufferInfo.flags = mediaExtractor.getSampleFlags();
                        if (koVar.mo14492a(a2, allocateDirect, bufferInfo, z)) {
                        }
                        mediaExtractor.advance();
                    } else {
                        z3 = true;
                    }
                }
            } else if (sampleTrackIndex == -1) {
                z3 = true;
            }
            if (z3) {
                z2 = true;
            }
        }
        mediaExtractor.unselectTrack(a);
        return j3;
    }

    /* renamed from: a */
    public static MediaCodecInfo m10075a(String str) {
        int codecCount = MediaCodecList.getCodecCount();
        MediaCodecInfo mediaCodecInfo = null;
        for (int i = 0; i < codecCount; i++) {
            MediaCodecInfo codecInfoAt = MediaCodecList.getCodecInfoAt(i);
            if (codecInfoAt.isEncoder()) {
                for (String equalsIgnoreCase : codecInfoAt.getSupportedTypes()) {
                    if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                        mediaCodecInfo = codecInfoAt;
                        if (!mediaCodecInfo.getName().equals("OMX.SEC.avc.enc")) {
                            return mediaCodecInfo;
                        }
                        if (mediaCodecInfo.getName().equals("OMX.SEC.AVC.Encoder")) {
                            return mediaCodecInfo;
                        }
                    }
                }
                continue;
            }
        }
        return mediaCodecInfo;
    }

    /* renamed from: a */
    public static MediaController m10076a() {
        MediaController mediaController = f7635a;
        if (mediaController == null) {
            synchronized (MediaController.class) {
                try {
                    mediaController = f7635a;
                    if (mediaController == null) {
                        MediaController mediaController2 = new MediaController();
                        try {
                            f7635a = mediaController2;
                            mediaController = mediaController2;
                        } catch (Throwable th) {
                            th = th;
                            MediaController mediaController3 = mediaController2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return mediaController;
    }

    /* renamed from: a */
    private void m10077a(boolean z, boolean z2) {
        if (this.f7636b) {
            this.f7636b = false;
        }
    }

    /* renamed from: a */
    private static boolean m10078a(int i) {
        switch (i) {
            case 19:
            case 20:
            case 21:
            case 39:
            case 2130706688:
                return true;
            default:
                return false;
        }
    }

    public static native int convertVideoFrame(ByteBuffer byteBuffer, ByteBuffer byteBuffer2, int i, int i2, int i3, int i4, int i5);

    /* JADX INFO: used method not loaded: ko.a(boolean):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x02b4, code lost:
        if (r58.equals("nokia") != false) goto L_0x02b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x0508, code lost:
        r6 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0509, code lost:
        r47 = r48;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:?, code lost:
        r47.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:?, code lost:
        r59.mo14491a(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:350:?, code lost:
        r47.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:354:?, code lost:
        r59.mo14491a(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:358:0x08cb, code lost:
        r35 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:0x08cc, code lost:
        android.util.Log.e("tmessages", r35.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:360:0x08d6, code lost:
        r35 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:361:0x08d7, code lost:
        android.util.Log.e("tmessages", r35.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:0x08fa, code lost:
        r35 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:370:0x08fb, code lost:
        r47 = r48;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:373:0x0904, code lost:
        r35 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:374:0x0905, code lost:
        r56 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x020d, code lost:
        r35 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        android.util.Log.e("tmessages", r35.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0217, code lost:
        r45 = true;
        r14 = -1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x0508 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:55:0x01b4] */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x050f A[SYNTHETIC, Splitter:B:200:0x050f] */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0514  */
    /* JADX WARNING: Removed duplicated region for block: B:349:0x08a0 A[SYNTHETIC, Splitter:B:349:0x08a0] */
    /* JADX WARNING: Removed duplicated region for block: B:352:0x08a5  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0224 A[Catch:{ Exception -> 0x0900, all -> 0x08f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0229 A[Catch:{ Exception -> 0x0900, all -> 0x08f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x022e A[Catch:{ Exception -> 0x0900, all -> 0x08f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0236 A[Catch:{ Exception -> 0x0900, all -> 0x08f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x023e A[Catch:{ Exception -> 0x0900, all -> 0x08f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x024f A[SYNTHETIC, Splitter:B:89:0x024f] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0254  */
    @android.annotation.TargetApi(16)
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.net.Uri mo11053b(java.lang.String r93) {
        /*
            r92 = this;
            android.media.MediaMetadataRetriever r74 = new android.media.MediaMetadataRetriever
            r74.<init>()
            r0 = r74
            r1 = r93
            r0.setDataSource(r1)
            r63 = 0
            r62 = 0
            android.graphics.Bitmap r22 = r74.getFrameAtTime()
            r6 = 24
            r0 = r74
            java.lang.String r76 = r0.extractMetadata(r6)
            if (r22 == 0) goto L_0x002f
            int r63 = r22.getWidth()
            int r62 = r22.getHeight()
        L_0x0026:
            if (r63 == 0) goto L_0x002c
            if (r62 == 0) goto L_0x002c
            if (r76 != 0) goto L_0x0050
        L_0x002c:
            r84 = 0
        L_0x002e:
            return r84
        L_0x002f:
            r6 = 18
            r0 = r74
            java.lang.String r6 = r0.extractMetadata(r6)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            int r63 = r6.intValue()
            r6 = 19
            r0 = r74
            java.lang.String r6 = r0.extractMetadata(r6)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            int r62 = r6.intValue()
            goto L_0x0026
        L_0x0050:
            r80 = -1
            r42 = -1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r76)
            int r77 = r6.intValue()
            r73 = r63
            r71 = r62
            r0 = r63
            r1 = r62
            if (r0 <= r1) goto L_0x0133
            r6 = 1280(0x500, float:1.794E-42)
            r0 = r63
            if (r0 <= r6) goto L_0x0133
            r73 = 1280(0x500, float:1.794E-42)
            int r6 = r73 * r62
            int r71 = r6 / r63
        L_0x0072:
            r23 = 1500000(0x16e360, float:2.101948E-39)
            r75 = 0
            java.io.File r25 = new java.io.File
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.io.File r10 = android.os.Environment.getExternalStorageDirectory()
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r10 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r10 = android.os.Environment.DIRECTORY_MOVIES
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r10 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r10 = "Candid"
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r10 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r6 = r6.toString()
            r0 = r25
            r0.<init>(r6)
            boolean r6 = r25.exists()
            if (r6 != 0) goto L_0x00b6
            r25.mkdir()
        L_0x00b6:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r10 = "COMPRESSED_VIDEO_"
            java.lang.StringBuilder r6 = r6.append(r10)
            java.text.SimpleDateFormat r10 = new java.text.SimpleDateFormat
            java.lang.String r11 = "yyyyMMdd_HHmmss"
            java.util.Locale r12 = java.util.Locale.US
            r10.<init>(r11, r12)
            java.util.Date r11 = new java.util.Date
            r11.<init>()
            java.lang.String r10 = r10.format(r11)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r10 = ".mp4"
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r49 = r6.toString()
            java.io.File r18 = new java.io.File
            r0 = r18
            r1 = r25
            r2 = r49
            r0.<init>(r1, r2)
            boolean r6 = r18.exists()
            if (r6 != 0) goto L_0x0288
            r18.createNewFile()     // Catch:{ IOException -> 0x0284 }
            java.lang.String r6 = r18.getAbsolutePath()     // Catch:{ IOException -> 0x0284 }
            android.net.Uri r84 = android.net.Uri.parse(r6)     // Catch:{ IOException -> 0x0284 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ IOException -> 0x0284 }
            r10 = 18
            if (r6 >= r10) goto L_0x0147
            r0 = r71
            r1 = r73
            if (r0 <= r1) goto L_0x0147
            r0 = r73
            r1 = r63
            if (r0 == r1) goto L_0x0147
            r0 = r71
            r1 = r62
            if (r0 == r1) goto L_0x0147
            r77 = 90
            r75 = 270(0x10e, float:3.78E-43)
        L_0x0119:
            java.io.File r54 = new java.io.File     // Catch:{ IOException -> 0x0284 }
            r0 = r54
            r1 = r93
            r0.<init>(r1)     // Catch:{ IOException -> 0x0284 }
            boolean r6 = r54.canRead()     // Catch:{ IOException -> 0x0284 }
            if (r6 != 0) goto L_0x016e
            r6 = 1
            r10 = 1
            r0 = r92
            r0.m10077a(r6, r10)     // Catch:{ IOException -> 0x0284 }
            r84 = 0
            goto L_0x002e
        L_0x0133:
            r0 = r62
            r1 = r63
            if (r0 <= r1) goto L_0x0072
            r6 = 1280(0x500, float:1.794E-42)
            r0 = r62
            if (r0 <= r6) goto L_0x0072
            r71 = 1280(0x500, float:1.794E-42)
            int r6 = r71 * r63
            int r73 = r6 / r62
            goto L_0x0072
        L_0x0147:
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ IOException -> 0x0284 }
            r10 = 20
            if (r6 <= r10) goto L_0x0119
            r6 = 90
            r0 = r77
            if (r0 != r6) goto L_0x0158
            r77 = 0
            r75 = 270(0x10e, float:3.78E-43)
            goto L_0x0119
        L_0x0158:
            r6 = 180(0xb4, float:2.52E-43)
            r0 = r77
            if (r0 != r6) goto L_0x0163
            r75 = 180(0xb4, float:2.52E-43)
            r77 = 0
            goto L_0x0119
        L_0x0163:
            r6 = 270(0x10e, float:3.78E-43)
            r0 = r77
            if (r0 != r6) goto L_0x0119
            r77 = 0
            r75 = 90
            goto L_0x0119
        L_0x016e:
            r6 = 1
            r0 = r92
            r0.f7636b = r6     // Catch:{ IOException -> 0x0284 }
            r45 = 0
            r88 = r80
            long r82 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0284 }
            if (r73 == 0) goto L_0x08e2
            if (r71 == 0) goto L_0x08e2
            r59 = 0
            r47 = 0
            android.media.MediaCodec$BufferInfo r51 = new android.media.MediaCodec$BufferInfo     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r51.<init>()     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            kp r60 = new kp     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r60.<init>()     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r0 = r60
            r1 = r18
            r0.mo14509a(r1)     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r0 = r60
            r1 = r77
            r0.mo14506a(r1)     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r0 = r60
            r1 = r73
            r2 = r71
            r0.mo14507a(r1, r2)     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            ko r6 = new ko     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r6.<init>()     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r0 = r60
            ko r59 = r6.mo14486a(r0)     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            android.media.MediaExtractor r48 = new android.media.MediaExtractor     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            r48.<init>()     // Catch:{ Exception -> 0x0890, all -> 0x08ed }
            java.lang.String r6 = r54.toString()     // Catch:{ Exception -> 0x08fa, all -> 0x0508 }
            r0 = r48
            r0.setDataSource(r6)     // Catch:{ Exception -> 0x08fa, all -> 0x0508 }
            r0 = r73
            r1 = r63
            if (r0 != r1) goto L_0x01c9
            r0 = r71
            r1 = r62
            if (r0 == r1) goto L_0x0868
        L_0x01c9:
            r6 = 0
            r0 = r92
            r1 = r48
            int r86 = r0.m10073a(r1, r6)     // Catch:{ Exception -> 0x08fa, all -> 0x0508 }
            if (r86 < 0) goto L_0x0909
            r4 = 0
            r37 = 0
            r56 = 0
            r66 = 0
            r90 = -1
            r64 = 0
            r53 = 0
            r30 = 0
            r79 = 0
            r87 = -5
            r70 = 0
            java.lang.String r6 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r58 = r6.toLowerCase()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 >= r10) goto L_0x04b2
            java.lang.String r6 = "video/avc"
            android.media.MediaCodecInfo r26 = m10075a(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "video/avc"
            r0 = r26
            int r28 = m10072a(r0, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r28 != 0) goto L_0x028c
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = "no supported color format"
            r6.<init>(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            throw r6     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x020d:
            r35 = move-exception
        L_0x020e:
            java.lang.String r6 = "tmessages"
            java.lang.String r10 = r35.getMessage()     // Catch:{ Exception -> 0x08fa, all -> 0x0508 }
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x08fa, all -> 0x0508 }
            r45 = 1
            r14 = r88
        L_0x021b:
            r0 = r48
            r1 = r86
            r0.unselectTrack(r1)     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
            if (r66 == 0) goto L_0x0227
            r66.mo14512a()     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
        L_0x0227:
            if (r56 == 0) goto L_0x022c
            r56.mo14481a()     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
        L_0x022c:
            if (r4 == 0) goto L_0x0234
            r4.stop()     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
            r4.release()     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
        L_0x0234:
            if (r37 == 0) goto L_0x023c
            r37.stop()     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
            r37.release()     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
        L_0x023c:
            if (r45 != 0) goto L_0x024d
            r19 = 1
            r10 = r92
            r11 = r48
            r12 = r59
            r13 = r51
            r16 = r42
            r10.m10074a(r11, r12, r13, r14, r16, r18, r19)     // Catch:{ Exception -> 0x0900, all -> 0x08f2 }
        L_0x024d:
            if (r48 == 0) goto L_0x0252
            r48.release()     // Catch:{ IOException -> 0x0284 }
        L_0x0252:
            if (r59 == 0) goto L_0x025a
            r6 = 0
            r0 = r59
            r0.mo14491a(r6)     // Catch:{ Exception -> 0x0884 }
        L_0x025a:
            java.lang.String r6 = "tmessages"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0284 }
            r10.<init>()     // Catch:{ IOException -> 0x0284 }
            java.lang.String r11 = "time = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ IOException -> 0x0284 }
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0284 }
            long r12 = r12 - r82
            java.lang.StringBuilder r10 = r10.append(r12)     // Catch:{ IOException -> 0x0284 }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x0284 }
            android.util.Log.e(r6, r10)     // Catch:{ IOException -> 0x0284 }
            r47 = r48
        L_0x027a:
            r6 = 1
            r0 = r92
            r1 = r45
            r0.m10077a(r6, r1)     // Catch:{ IOException -> 0x0284 }
            goto L_0x002e
        L_0x0284:
            r35 = move-exception
            r35.printStackTrace()
        L_0x0288:
            r84 = 0
            goto L_0x002e
        L_0x028c:
            java.lang.String r27 = r26.getName()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "OMX.qcom."
            r0 = r27
            boolean r6 = r0.contains(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x0478
            r70 = 1
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 16
            if (r6 != r10) goto L_0x02b8
            java.lang.String r6 = "lge"
            r0 = r58
            boolean r6 = r0.equals(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 != 0) goto L_0x02b6
            java.lang.String r6 = "nokia"
            r0 = r58
            boolean r6 = r0.equals(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x02b8
        L_0x02b6:
            r79 = 1
        L_0x02b8:
            java.lang.String r6 = "tmessages"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "codec = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = r26.getName()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = " manufacturer = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r58
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "device = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = android.os.Build.MODEL     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x02ec:
            java.lang.String r6 = "tmessages"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "colorFormat = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r28
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r72 = r71
            r68 = 0
            int r6 = r73 * r71
            int r6 = r6 * 3
            int r24 = r6 / 2
            if (r70 != 0) goto L_0x04b7
            int r6 = r71 % 16
            if (r6 == 0) goto L_0x0326
            int r6 = r71 % 16
            int r6 = 16 - r6
            int r72 = r72 + r6
            int r6 = r72 - r71
            int r68 = r73 * r6
            int r6 = r68 * 5
            int r6 = r6 / 4
            int r24 = r24 + r6
        L_0x0326:
            r0 = r48
            r1 = r86
            r0.selectTrack(r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 0
            int r6 = (r80 > r10 ? 1 : (r80 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x04fe
            r6 = 0
            r0 = r48
            r1 = r80
            r0.seekTo(r1, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x033b:
            r0 = r48
            r1 = r86
            android.media.MediaFormat r55 = r0.getTrackFormat(r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "video/avc"
            r0 = r73
            r1 = r71
            android.media.MediaFormat r65 = android.media.MediaFormat.createVideoFormat(r6, r0, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "color-format"
            r0 = r65
            r1 = r28
            r0.setInteger(r6, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "bitrate"
            if (r23 == 0) goto L_0x0539
        L_0x035a:
            r0 = r65
            r1 = r23
            r0.setInteger(r6, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "frame-rate"
            r10 = 30
            r0 = r65
            r0.setInteger(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "i-frame-interval"
            r10 = 10
            r0 = r65
            r0.setInteger(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 >= r10) goto L_0x038b
            java.lang.String r6 = "stride"
            int r10 = r73 + 32
            r0 = r65
            r0.setInteger(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "slice-height"
            r0 = r65
            r1 = r71
            r0.setInteger(r6, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x038b:
            java.lang.String r6 = "video/avc"
            android.media.MediaCodec r37 = android.media.MediaCodec.createEncoderByType(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = 0
            r10 = 0
            r11 = 1
            r0 = r37
            r1 = r65
            r0.configure(r1, r6, r10, r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 < r10) goto L_0x03b1
            kn r57 = new kn     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            android.view.Surface r6 = r37.createInputSurface()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r57
            r0.<init>(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r57.mo14483b()     // Catch:{ Exception -> 0x0904, all -> 0x0508 }
            r56 = r57
        L_0x03b1:
            r37.start()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "mime"
            r0 = r55
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            android.media.MediaCodec r4 = android.media.MediaCodec.createDecoderByType(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 < r10) goto L_0x053e
            kq r67 = new kq     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r67.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r66 = r67
        L_0x03cd:
            android.view.Surface r6 = r66.mo14515c()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 0
            r11 = 0
            r0 = r55
            r4.configure(r0, r6, r10, r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r4.start()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r20 = 2500(0x9c4, float:3.503E-42)
            r31 = 0
            r40 = 0
            r38 = 0
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 21
            if (r6 >= r10) goto L_0x03fb
            java.nio.ByteBuffer[] r31 = r4.getInputBuffers()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.nio.ByteBuffer[] r40 = r37.getOutputBuffers()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 >= r10) goto L_0x03fb
            java.nio.ByteBuffer[] r38 = r37.getInputBuffers()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x03fb:
            if (r64 != 0) goto L_0x085e
            if (r53 != 0) goto L_0x0446
            r44 = 0
            int r50 = r48.getSampleTrackIndex()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r50
            r1 = r86
            if (r0 != r1) goto L_0x0563
            r10 = 2500(0x9c4, double:1.235E-320)
            int r5 = r4.dequeueInputBuffer(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r5 < 0) goto L_0x0430
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 21
            if (r6 >= r10) goto L_0x054f
            r52 = r31[r5]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x041b:
            r6 = 0
            r0 = r48
            r1 = r52
            int r7 = r0.readSampleData(r1, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r7 >= 0) goto L_0x0555
            r6 = 0
            r7 = 0
            r8 = 0
            r10 = 4
            r4.queueInputBuffer(r5, r6, r7, r8, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r53 = 1
        L_0x0430:
            if (r44 == 0) goto L_0x0446
            r10 = 2500(0x9c4, double:1.235E-320)
            int r5 = r4.dequeueInputBuffer(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r5 < 0) goto L_0x0446
            r10 = 0
            r11 = 0
            r12 = 0
            r14 = 4
            r8 = r4
            r9 = r5
            r8.queueInputBuffer(r9, r10, r11, r12, r14)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r53 = 1
        L_0x0446:
            if (r30 != 0) goto L_0x056c
            r32 = 1
        L_0x044a:
            r39 = 1
        L_0x044c:
            if (r32 != 0) goto L_0x0450
            if (r39 == 0) goto L_0x03fb
        L_0x0450:
            r10 = 2500(0x9c4, double:1.235E-320)
            r0 = r37
            r1 = r51
            int r41 = r0.dequeueOutputBuffer(r1, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = -1
            r0 = r41
            if (r0 != r6) goto L_0x0570
            r39 = 0
        L_0x0461:
            r6 = -1
            r0 = r41
            if (r0 != r6) goto L_0x044c
            if (r30 != 0) goto L_0x044c
            r10 = 2500(0x9c4, double:1.235E-320)
            r0 = r51
            int r33 = r4.dequeueOutputBuffer(r0, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = -1
            r0 = r33
            if (r0 != r6) goto L_0x06de
            r32 = 0
            goto L_0x044c
        L_0x0478:
            java.lang.String r6 = "OMX.Intel."
            r0 = r27
            boolean r6 = r0.contains(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x0486
            r70 = 2
            goto L_0x02b8
        L_0x0486:
            java.lang.String r6 = "OMX.MTK.VIDEO.ENCODER.AVC"
            r0 = r27
            boolean r6 = r0.equals(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x0494
            r70 = 3
            goto L_0x02b8
        L_0x0494:
            java.lang.String r6 = "OMX.SEC.AVC.Encoder"
            r0 = r27
            boolean r6 = r0.equals(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x04a4
            r70 = 4
            r79 = 1
            goto L_0x02b8
        L_0x04a4:
            java.lang.String r6 = "OMX.TI.DUCATI1.VIDEO.H264E"
            r0 = r27
            boolean r6 = r0.equals(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x02b8
            r70 = 5
            goto L_0x02b8
        L_0x04b2:
            r28 = 2130708361(0x7f000789, float:1.701803E38)
            goto L_0x02ec
        L_0x04b7:
            r6 = 1
            r0 = r70
            if (r0 != r6) goto L_0x04d8
            java.lang.String r6 = r58.toLowerCase()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = "lge"
            boolean r6 = r6.equals(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 != 0) goto L_0x0326
            int r6 = r73 * r71
            int r6 = r6 + 2047
            r0 = r6 & -2048(0xfffffffffffff800, float:NaN)
            r85 = r0
            int r6 = r73 * r71
            int r68 = r85 - r6
            int r24 = r24 + r68
            goto L_0x0326
        L_0x04d8:
            r6 = 5
            r0 = r70
            if (r0 == r6) goto L_0x0326
            r6 = 3
            r0 = r70
            if (r0 != r6) goto L_0x0326
            java.lang.String r6 = "baidu"
            r0 = r58
            boolean r6 = r0.equals(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x0326
            int r6 = r71 % 16
            int r6 = 16 - r6
            int r72 = r72 + r6
            int r6 = r72 - r71
            int r68 = r73 * r6
            int r6 = r68 * 5
            int r6 = r6 / 4
            int r24 = r24 + r6
            goto L_0x0326
        L_0x04fe:
            r10 = 0
            r6 = 0
            r0 = r48
            r0.seekTo(r10, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x033b
        L_0x0508:
            r6 = move-exception
            r47 = r48
            r14 = r88
        L_0x050d:
            if (r47 == 0) goto L_0x0512
            r47.release()     // Catch:{ IOException -> 0x0284 }
        L_0x0512:
            if (r59 == 0) goto L_0x051a
            r10 = 0
            r0 = r59
            r0.mo14491a(r10)     // Catch:{ Exception -> 0x08d6 }
        L_0x051a:
            java.lang.String r10 = "tmessages"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0284 }
            r11.<init>()     // Catch:{ IOException -> 0x0284 }
            java.lang.String r12 = "time = "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x0284 }
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0284 }
            long r12 = r12 - r82
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x0284 }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x0284 }
            android.util.Log.e(r10, r11)     // Catch:{ IOException -> 0x0284 }
            throw r6     // Catch:{ IOException -> 0x0284 }
        L_0x0539:
            r23 = 921600(0xe1000, float:1.291437E-39)
            goto L_0x035a
        L_0x053e:
            kq r67 = new kq     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r67
            r1 = r73
            r2 = r71
            r3 = r75
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r66 = r67
            goto L_0x03cd
        L_0x054f:
            java.nio.ByteBuffer r52 = r4.getInputBuffer(r5)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x041b
        L_0x0555:
            r6 = 0
            long r8 = r48.getSampleTime()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 0
            r4.queueInputBuffer(r5, r6, r7, r8, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r48.advance()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x0430
        L_0x0563:
            r6 = -1
            r0 = r50
            if (r0 != r6) goto L_0x0430
            r44 = 1
            goto L_0x0430
        L_0x056c:
            r32 = 0
            goto L_0x044a
        L_0x0570:
            r6 = -3
            r0 = r41
            if (r0 != r6) goto L_0x0581
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 21
            if (r6 >= r10) goto L_0x0461
            java.nio.ByteBuffer[] r40 = r37.getOutputBuffers()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x0461
        L_0x0581:
            r6 = -2
            r0 = r41
            if (r0 != r6) goto L_0x059a
            android.media.MediaFormat r61 = r37.getOutputFormat()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = -5
            r0 = r87
            if (r0 != r6) goto L_0x0461
            r6 = 0
            r0 = r59
            r1 = r61
            int r87 = r0.mo14485a(r1, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x0461
        L_0x059a:
            if (r41 >= 0) goto L_0x05b7
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "unexpected result from encoder.dequeueOutputBuffer: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r41
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6.<init>(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            throw r6     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x05b7:
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 21
            if (r6 >= r10) goto L_0x05e2
            r36 = r40[r41]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x05bf:
            if (r36 != 0) goto L_0x05eb
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "encoderOutputBuffer "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r41
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = " was null"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6.<init>(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            throw r6     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x05e2:
            r0 = r37
            r1 = r41
            java.nio.ByteBuffer r36 = r0.getOutputBuffer(r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x05bf
        L_0x05eb:
            r0 = r51
            int r6 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 1
            if (r6 <= r10) goto L_0x0610
            r0 = r51
            int r6 = r0.flags     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = r6 & 2
            if (r6 != 0) goto L_0x0624
            r6 = 0
            r0 = r59
            r1 = r87
            r2 = r36
            r3 = r51
            boolean r6 = r0.mo14492a(r1, r2, r3, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x0610
            r6 = 0
            r10 = 0
            r0 = r92
            r0.m10077a(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x0610:
            r0 = r51
            int r6 = r0.flags     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = r6 & 4
            if (r6 == 0) goto L_0x06da
            r64 = 1
        L_0x061a:
            r6 = 0
            r0 = r37
            r1 = r41
            r0.releaseOutputBuffer(r1, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x0461
        L_0x0624:
            r6 = -5
            r0 = r87
            if (r0 != r6) goto L_0x0610
            r0 = r51
            int r6 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            byte[] r0 = new byte[r6]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r29 = r0
            r0 = r51
            int r6 = r0.offset     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r51
            int r10 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = r6 + r10
            r0 = r36
            r0.limit(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r51
            int r6 = r0.offset     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r36
            r0.position(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r36
            r1 = r29
            r0.get(r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r78 = 0
            r69 = 0
            r0 = r51
            int r6 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r21 = r6 + -1
        L_0x0659:
            if (r21 < 0) goto L_0x06ac
            r6 = 3
            r0 = r21
            if (r0 <= r6) goto L_0x06ac
            byte r6 = r29[r21]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 1
            if (r6 != r10) goto L_0x06d7
            int r6 = r21 + -1
            byte r6 = r29[r6]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 != 0) goto L_0x06d7
            int r6 = r21 + -2
            byte r6 = r29[r6]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 != 0) goto L_0x06d7
            int r6 = r21 + -3
            byte r6 = r29[r6]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 != 0) goto L_0x06d7
            int r6 = r21 + -3
            java.nio.ByteBuffer r78 = java.nio.ByteBuffer.allocate(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r51
            int r6 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r10 = r21 + -3
            int r6 = r6 - r10
            java.nio.ByteBuffer r69 = java.nio.ByteBuffer.allocate(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = 0
            int r10 = r21 + -3
            r0 = r78
            r1 = r29
            java.nio.ByteBuffer r6 = r0.put(r1, r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 0
            r6.position(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = r21 + -3
            r0 = r51
            int r10 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r11 = r21 + -3
            int r10 = r10 - r11
            r0 = r69
            r1 = r29
            java.nio.ByteBuffer r6 = r0.put(r1, r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 0
            r6.position(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x06ac:
            java.lang.String r6 = "video/avc"
            r0 = r73
            r1 = r71
            android.media.MediaFormat r61 = android.media.MediaFormat.createVideoFormat(r6, r0, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r78 == 0) goto L_0x06cc
            if (r69 == 0) goto L_0x06cc
            java.lang.String r6 = "csd-0"
            r0 = r61
            r1 = r78
            r0.setByteBuffer(r6, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "csd-1"
            r0 = r61
            r1 = r69
            r0.setByteBuffer(r6, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x06cc:
            r6 = 0
            r0 = r59
            r1 = r61
            int r87 = r0.mo14485a(r1, r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x0610
        L_0x06d7:
            int r21 = r21 + -1
            goto L_0x0659
        L_0x06da:
            r64 = 0
            goto L_0x061a
        L_0x06de:
            r6 = -3
            r0 = r33
            if (r0 == r6) goto L_0x044c
            r6 = -2
            r0 = r33
            if (r0 != r6) goto L_0x0708
            android.media.MediaFormat r61 = r4.getOutputFormat()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r6 = "tmessages"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "newFormat = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r61
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x044c
        L_0x0708:
            if (r33 >= 0) goto L_0x0725
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "unexpected result from decoder.dequeueOutputBuffer: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r33
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6.<init>(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            throw r6     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x0725:
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 < r10) goto L_0x07da
            r0 = r51
            int r6 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 == 0) goto L_0x07d6
            r34 = 1
        L_0x0733:
            r10 = 0
            int r6 = (r42 > r10 ? 1 : (r42 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x0751
            r0 = r51
            long r10 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = (r10 > r42 ? 1 : (r10 == r42 ? 0 : -1))
            if (r6 < 0) goto L_0x0751
            r53 = 1
            r30 = 1
            r34 = 0
            r0 = r51
            int r6 = r0.flags     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = r6 | 4
            r0 = r51
            r0.flags = r6     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x0751:
            r10 = 0
            int r6 = (r80 > r10 ? 1 : (r80 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x078f
            r10 = -1
            int r6 = (r90 > r10 ? 1 : (r90 == r10 ? 0 : -1))
            if (r6 != 0) goto L_0x078f
            r0 = r51
            long r10 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = (r10 > r80 ? 1 : (r10 == r80 ? 0 : -1))
            if (r6 >= 0) goto L_0x07f1
            r34 = 0
            java.lang.String r6 = "tmessages"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10.<init>()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = "drop frame startTime = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r80
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r11 = " present time = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r51
            long r12 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.StringBuilder r10 = r10.append(r12)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x078f:
            r0 = r33
            r1 = r34
            r4.releaseOutputBuffer(r0, r1)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r34 == 0) goto L_0x07ba
            r46 = 0
            r66.mo14516d()     // Catch:{ Exception -> 0x07f8, all -> 0x0508 }
        L_0x079d:
            if (r46 != 0) goto L_0x07ba
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 < r10) goto L_0x0805
            r6 = 0
            r0 = r66
            r0.mo14513a(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r0 = r51
            long r10 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r12 = 1000(0x3e8, double:4.94E-321)
            long r10 = r10 * r12
            r0 = r56
            r0.mo14482a(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r56.mo14484c()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
        L_0x07ba:
            r0 = r51
            int r6 = r0.flags     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r6 = r6 & 4
            if (r6 == 0) goto L_0x044c
            r32 = 0
            java.lang.String r6 = "tmessages"
            java.lang.String r10 = "decoder stream end"
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = 18
            if (r6 < r10) goto L_0x0844
            r37.signalEndOfInputStream()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x044c
        L_0x07d6:
            r34 = 0
            goto L_0x0733
        L_0x07da:
            r0 = r51
            int r6 = r0.size     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r6 != 0) goto L_0x07ea
            r0 = r51
            long r10 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r12 = 0
            int r6 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r6 == 0) goto L_0x07ee
        L_0x07ea:
            r34 = 1
        L_0x07ec:
            goto L_0x0733
        L_0x07ee:
            r34 = 0
            goto L_0x07ec
        L_0x07f1:
            r0 = r51
            long r0 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r90 = r0
            goto L_0x078f
        L_0x07f8:
            r35 = move-exception
            r46 = 1
            java.lang.String r6 = "tmessages"
            java.lang.String r10 = r35.getMessage()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x079d
        L_0x0805:
            r10 = 2500(0x9c4, double:1.235E-320)
            r0 = r37
            int r5 = r0.dequeueInputBuffer(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r5 < 0) goto L_0x083b
            r6 = 1
            r0 = r66
            r0.mo14513a(r6)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            java.nio.ByteBuffer r8 = r66.mo14517e()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r9 = r38[r5]     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r9.clear()     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r10 = r28
            r11 = r73
            r12 = r71
            r13 = r68
            r14 = r79
            convertVideoFrame(r8, r9, r10, r11, r12, r13, r14)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r12 = 0
            r0 = r51
            long r14 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r16 = 0
            r10 = r37
            r11 = r5
            r13 = r24
            r10.queueInputBuffer(r11, r12, r13, r14, r16)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x07ba
        L_0x083b:
            java.lang.String r6 = "tmessages"
            java.lang.String r10 = "input buffer not available"
            android.util.Log.e(r6, r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x07ba
        L_0x0844:
            r10 = 2500(0x9c4, double:1.235E-320)
            r0 = r37
            int r5 = r0.dequeueInputBuffer(r10)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            if (r5 < 0) goto L_0x044c
            r12 = 0
            r13 = 1
            r0 = r51
            long r14 = r0.presentationTimeUs     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            r16 = 4
            r10 = r37
            r11 = r5
            r10.queueInputBuffer(r11, r12, r13, r14, r16)     // Catch:{ Exception -> 0x020d, all -> 0x0508 }
            goto L_0x044c
        L_0x085e:
            r10 = -1
            int r6 = (r90 > r10 ? 1 : (r90 == r10 ? 0 : -1))
            if (r6 == 0) goto L_0x090d
            r14 = r90
            goto L_0x021b
        L_0x0868:
            r19 = 0
            r10 = r92
            r11 = r48
            r12 = r59
            r13 = r51
            r14 = r80
            r16 = r42
            long r90 = r10.m10074a(r11, r12, r13, r14, r16, r18, r19)     // Catch:{ Exception -> 0x08fa, all -> 0x0508 }
            r10 = -1
            int r6 = (r90 > r10 ? 1 : (r90 == r10 ? 0 : -1))
            if (r6 == 0) goto L_0x0909
            r14 = r90
            goto L_0x023c
        L_0x0884:
            r35 = move-exception
            java.lang.String r6 = "tmessages"
            java.lang.String r10 = r35.getMessage()     // Catch:{ IOException -> 0x0284 }
            android.util.Log.e(r6, r10)     // Catch:{ IOException -> 0x0284 }
            goto L_0x025a
        L_0x0890:
            r35 = move-exception
            r14 = r88
        L_0x0893:
            r45 = 1
            java.lang.String r6 = "tmessages"
            java.lang.String r10 = r35.getMessage()     // Catch:{ all -> 0x08f7 }
            android.util.Log.e(r6, r10)     // Catch:{ all -> 0x08f7 }
            if (r47 == 0) goto L_0x08a3
            r47.release()     // Catch:{ IOException -> 0x0284 }
        L_0x08a3:
            if (r59 == 0) goto L_0x08ab
            r6 = 0
            r0 = r59
            r0.mo14491a(r6)     // Catch:{ Exception -> 0x08cb }
        L_0x08ab:
            java.lang.String r6 = "tmessages"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0284 }
            r10.<init>()     // Catch:{ IOException -> 0x0284 }
            java.lang.String r11 = "time = "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ IOException -> 0x0284 }
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0284 }
            long r12 = r12 - r82
            java.lang.StringBuilder r10 = r10.append(r12)     // Catch:{ IOException -> 0x0284 }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x0284 }
            android.util.Log.e(r6, r10)     // Catch:{ IOException -> 0x0284 }
            goto L_0x027a
        L_0x08cb:
            r35 = move-exception
            java.lang.String r6 = "tmessages"
            java.lang.String r10 = r35.getMessage()     // Catch:{ IOException -> 0x0284 }
            android.util.Log.e(r6, r10)     // Catch:{ IOException -> 0x0284 }
            goto L_0x08ab
        L_0x08d6:
            r35 = move-exception
            java.lang.String r10 = "tmessages"
            java.lang.String r11 = r35.getMessage()     // Catch:{ IOException -> 0x0284 }
            android.util.Log.e(r10, r11)     // Catch:{ IOException -> 0x0284 }
            goto L_0x051a
        L_0x08e2:
            r6 = 1
            r10 = 1
            r0 = r92
            r0.m10077a(r6, r10)     // Catch:{ IOException -> 0x0284 }
            r84 = 0
            goto L_0x002e
        L_0x08ed:
            r6 = move-exception
            r14 = r88
            goto L_0x050d
        L_0x08f2:
            r6 = move-exception
            r47 = r48
            goto L_0x050d
        L_0x08f7:
            r6 = move-exception
            goto L_0x050d
        L_0x08fa:
            r35 = move-exception
            r47 = r48
            r14 = r88
            goto L_0x0893
        L_0x0900:
            r35 = move-exception
            r47 = r48
            goto L_0x0893
        L_0x0904:
            r35 = move-exception
            r56 = r57
            goto L_0x020e
        L_0x0909:
            r14 = r88
            goto L_0x023c
        L_0x090d:
            r14 = r88
            goto L_0x021b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.becandid.thirdparty.video_compression.MediaController.mo11053b(java.lang.String):android.net.Uri");
    }
}
