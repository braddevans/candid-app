package com.crashlytics.android.core;

public class CrashlyticsMissingDependencyException extends RuntimeException {
    public CrashlyticsMissingDependencyException(String str) {
        super(m10208a(str));
    }

    /* renamed from: a */
    private static String m10208a(String str) {
        return "\n" + str + "\n";
    }
}
