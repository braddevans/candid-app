package com.crashlytics.android.core;

import android.support.p001v4.app.NotificationCompat;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class CodedOutputStream implements Flushable {

    /* renamed from: a */
    private final byte[] f7754a;

    /* renamed from: b */
    private final int f7755b;

    /* renamed from: c */
    private int f7756c = 0;

    /* renamed from: d */
    private final OutputStream f7757d;

    static class OutOfSpaceException extends IOException {
        OutOfSpaceException() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
    }

    private CodedOutputStream(OutputStream outputStream, byte[] bArr) {
        this.f7757d = outputStream;
        this.f7754a = bArr;
        this.f7755b = bArr.length;
    }

    /* renamed from: a */
    public static CodedOutputStream m10161a(OutputStream outputStream) {
        return m10162a(outputStream, 4096);
    }

    /* renamed from: a */
    public static CodedOutputStream m10162a(OutputStream outputStream, int i) {
        return new CodedOutputStream(outputStream, new byte[i]);
    }

    /* renamed from: a */
    private void m10163a() throws IOException {
        if (this.f7757d == null) {
            throw new OutOfSpaceException();
        }
        this.f7757d.write(this.f7754a, 0, this.f7756c);
        this.f7756c = 0;
    }

    /* renamed from: b */
    public static int m10164b(float f) {
        return 4;
    }

    /* renamed from: b */
    public static int m10165b(int i, float f) {
        return m10180j(i) + m10164b(f);
    }

    /* renamed from: b */
    public static int m10166b(int i, long j) {
        return m10180j(i) + m10169b(j);
    }

    /* renamed from: b */
    public static int m10167b(int i, ByteString vlVar) {
        return m10180j(i) + m10170b(vlVar);
    }

    /* renamed from: b */
    public static int m10168b(int i, boolean z) {
        return m10180j(i) + m10171b(z);
    }

    /* renamed from: b */
    public static int m10169b(long j) {
        return m10173d(j);
    }

    /* renamed from: b */
    public static int m10170b(ByteString vlVar) {
        return m10181l(vlVar.mo16584a()) + vlVar.mo16584a();
    }

    /* renamed from: b */
    public static int m10171b(boolean z) {
        return 1;
    }

    /* renamed from: d */
    public static int m10172d(int i, int i2) {
        return m10180j(i) + m10176f(i2);
    }

    /* renamed from: d */
    public static int m10173d(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (Long.MIN_VALUE & j) == 0 ? 9 : 10;
    }

    /* renamed from: e */
    public static int m10174e(int i) {
        if (i >= 0) {
            return m10181l(i);
        }
        return 10;
    }

    /* renamed from: e */
    public static int m10175e(int i, int i2) {
        return m10180j(i) + m10178g(i2);
    }

    /* renamed from: f */
    public static int m10176f(int i) {
        return m10181l(i);
    }

    /* renamed from: f */
    public static int m10177f(int i, int i2) {
        return m10180j(i) + m10179h(i2);
    }

    /* renamed from: g */
    public static int m10178g(int i) {
        return m10174e(i);
    }

    /* renamed from: h */
    public static int m10179h(int i) {
        return m10181l(m10182n(i));
    }

    /* renamed from: j */
    public static int m10180j(int i) {
        return m10181l(WireFormat.m16902a(i, 0));
    }

    /* renamed from: l */
    public static int m10181l(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    /* renamed from: n */
    public static int m10182n(int i) {
        return (i << 1) ^ (i >> 31);
    }

    /* renamed from: a */
    public void mo11124a(byte b) throws IOException {
        if (this.f7756c == this.f7755b) {
            m10163a();
        }
        byte[] bArr = this.f7754a;
        int i = this.f7756c;
        this.f7756c = i + 1;
        bArr[i] = b;
    }

    /* renamed from: a */
    public void mo11125a(float f) throws IOException {
        mo11149m(Float.floatToRawIntBits(f));
    }

    /* renamed from: a */
    public void mo11126a(int i) throws IOException {
        if (i >= 0) {
            mo11148k(i);
        } else {
            mo11142c((long) i);
        }
    }

    /* renamed from: a */
    public void mo11127a(int i, float f) throws IOException {
        mo11146g(i, 5);
        mo11125a(f);
    }

    /* renamed from: a */
    public void mo11128a(int i, int i2) throws IOException {
        mo11146g(i, 0);
        mo11138b(i2);
    }

    /* renamed from: a */
    public void mo11129a(int i, long j) throws IOException {
        mo11146g(i, 0);
        mo11132a(j);
    }

    /* renamed from: a */
    public void mo11130a(int i, ByteString vlVar) throws IOException {
        mo11146g(i, 2);
        mo11133a(vlVar);
    }

    /* renamed from: a */
    public void mo11131a(int i, boolean z) throws IOException {
        mo11146g(i, 0);
        mo11135a(z);
    }

    /* renamed from: a */
    public void mo11132a(long j) throws IOException {
        mo11142c(j);
    }

    /* renamed from: a */
    public void mo11133a(ByteString vlVar) throws IOException {
        mo11148k(vlVar.mo16584a());
        mo11143c(vlVar);
    }

    /* renamed from: a */
    public void mo11134a(ByteString vlVar, int i, int i2) throws IOException {
        if (this.f7755b - this.f7756c >= i2) {
            vlVar.mo16585a(this.f7754a, i, this.f7756c, i2);
            this.f7756c += i2;
            return;
        }
        int i3 = this.f7755b - this.f7756c;
        vlVar.mo16585a(this.f7754a, i, this.f7756c, i3);
        int i4 = i + i3;
        int i5 = i2 - i3;
        this.f7756c = this.f7755b;
        m10163a();
        if (i5 <= this.f7755b) {
            vlVar.mo16585a(this.f7754a, i4, 0, i5);
            this.f7756c = i5;
            return;
        }
        InputStream b = vlVar.mo16586b();
        if (((long) i4) != b.skip((long) i4)) {
            throw new IllegalStateException("Skip failed.");
        }
        while (i5 > 0) {
            int min = Math.min(i5, this.f7755b);
            int read = b.read(this.f7754a, 0, min);
            if (read != min) {
                throw new IllegalStateException("Read failed.");
            }
            this.f7757d.write(this.f7754a, 0, read);
            i5 -= read;
        }
    }

    /* renamed from: a */
    public void mo11135a(boolean z) throws IOException {
        mo11147i(z ? 1 : 0);
    }

    /* renamed from: a */
    public void mo11136a(byte[] bArr) throws IOException {
        mo11137a(bArr, 0, bArr.length);
    }

    /* renamed from: a */
    public void mo11137a(byte[] bArr, int i, int i2) throws IOException {
        if (this.f7755b - this.f7756c >= i2) {
            System.arraycopy(bArr, i, this.f7754a, this.f7756c, i2);
            this.f7756c += i2;
            return;
        }
        int i3 = this.f7755b - this.f7756c;
        System.arraycopy(bArr, i, this.f7754a, this.f7756c, i3);
        int i4 = i + i3;
        int i5 = i2 - i3;
        this.f7756c = this.f7755b;
        m10163a();
        if (i5 <= this.f7755b) {
            System.arraycopy(bArr, i4, this.f7754a, 0, i5);
            this.f7756c = i5;
            return;
        }
        this.f7757d.write(bArr, i4, i5);
    }

    /* renamed from: b */
    public void mo11138b(int i) throws IOException {
        mo11148k(i);
    }

    /* renamed from: b */
    public void mo11139b(int i, int i2) throws IOException {
        mo11146g(i, 0);
        mo11140c(i2);
    }

    /* renamed from: c */
    public void mo11140c(int i) throws IOException {
        mo11126a(i);
    }

    /* renamed from: c */
    public void mo11141c(int i, int i2) throws IOException {
        mo11146g(i, 0);
        mo11144d(i2);
    }

    /* renamed from: c */
    public void mo11142c(long j) throws IOException {
        while ((-128 & j) != 0) {
            mo11147i((((int) j) & 127) | NotificationCompat.FLAG_HIGH_PRIORITY);
            j >>>= 7;
        }
        mo11147i((int) j);
    }

    /* renamed from: c */
    public void mo11143c(ByteString vlVar) throws IOException {
        mo11134a(vlVar, 0, vlVar.mo16584a());
    }

    /* renamed from: d */
    public void mo11144d(int i) throws IOException {
        mo11148k(m10182n(i));
    }

    public void flush() throws IOException {
        if (this.f7757d != null) {
            m10163a();
        }
    }

    /* renamed from: g */
    public void mo11146g(int i, int i2) throws IOException {
        mo11148k(WireFormat.m16902a(i, i2));
    }

    /* renamed from: i */
    public void mo11147i(int i) throws IOException {
        mo11124a((byte) i);
    }

    /* renamed from: k */
    public void mo11148k(int i) throws IOException {
        while ((i & -128) != 0) {
            mo11147i((i & 127) | NotificationCompat.FLAG_HIGH_PRIORITY);
            i >>>= 7;
        }
        mo11147i(i);
    }

    /* renamed from: m */
    public void mo11149m(int i) throws IOException {
        mo11147i(i & 255);
        mo11147i((i >> 8) & 255);
        mo11147i((i >> 16) & 255);
        mo11147i((i >> 24) & 255);
    }
}
