package com.crashlytics.android.answers;

import android.app.Activity;
import java.util.Collections;
import java.util.Map;

public final class SessionEvent {

    /* renamed from: a */
    public final SessionEventMetadata f7729a;

    /* renamed from: b */
    public final long f7730b;

    /* renamed from: c */
    public final Type f7731c;

    /* renamed from: d */
    public final Map<String, String> f7732d;

    /* renamed from: e */
    public final String f7733e;

    /* renamed from: f */
    public final Map<String, Object> f7734f;

    /* renamed from: g */
    public final String f7735g;

    /* renamed from: h */
    public final Map<String, Object> f7736h;

    /* renamed from: i */
    private String f7737i;

    public enum Type {
        START,
        RESUME,
        PAUSE,
        STOP,
        CRASH,
        INSTALL,
        CUSTOM,
        PREDEFINED
    }

    /* renamed from: com.crashlytics.android.answers.SessionEvent$a */
    public static class C1955a {

        /* renamed from: a */
        final Type f7747a;

        /* renamed from: b */
        final long f7748b = System.currentTimeMillis();

        /* renamed from: c */
        Map<String, String> f7749c = null;

        /* renamed from: d */
        String f7750d = null;

        /* renamed from: e */
        Map<String, Object> f7751e = null;

        /* renamed from: f */
        String f7752f = null;

        /* renamed from: g */
        Map<String, Object> f7753g = null;

        public C1955a(Type type) {
            this.f7747a = type;
        }

        /* renamed from: a */
        public C1955a mo11118a(String str) {
            this.f7750d = str;
            return this;
        }

        /* renamed from: a */
        public C1955a mo11119a(Map<String, String> map) {
            this.f7749c = map;
            return this;
        }

        /* renamed from: a */
        public SessionEvent mo11120a(SessionEventMetadata uwVar) {
            return new SessionEvent(uwVar, this.f7748b, this.f7747a, this.f7749c, this.f7750d, this.f7751e, this.f7752f, this.f7753g);
        }

        /* renamed from: b */
        public C1955a mo11121b(String str) {
            this.f7752f = str;
            return this;
        }

        /* renamed from: b */
        public C1955a mo11122b(Map<String, Object> map) {
            this.f7751e = map;
            return this;
        }

        /* renamed from: c */
        public C1955a mo11123c(Map<String, Object> map) {
            this.f7753g = map;
            return this;
        }
    }

    private SessionEvent(SessionEventMetadata uwVar, long j, Type type, Map<String, String> map, String str, Map<String, Object> map2, String str2, Map<String, Object> map3) {
        this.f7729a = uwVar;
        this.f7730b = j;
        this.f7731c = type;
        this.f7732d = map;
        this.f7733e = str;
        this.f7734f = map2;
        this.f7735g = str2;
        this.f7736h = map3;
    }

    /* renamed from: a */
    public static C1955a m10149a() {
        return new C1955a(Type.INSTALL);
    }

    /* renamed from: a */
    public static C1955a m10150a(Type type, Activity activity) {
        return new C1955a(type).mo11119a(Collections.singletonMap("activity", activity.getClass().getName()));
    }

    /* renamed from: a */
    public static C1955a m10151a(String str) {
        return new C1955a(Type.CRASH).mo11119a(Collections.singletonMap("sessionId", str));
    }

    /* renamed from: a */
    public static C1955a m10152a(String str, String str2) {
        return m10151a(str).mo11122b(Collections.singletonMap("exceptionName", str2));
    }

    /* renamed from: a */
    public static C1955a m10153a(CustomEvent ujVar) {
        return new C1955a(Type.CUSTOM).mo11118a(ujVar.mo16488b()).mo11122b(ujVar.mo16460a());
    }

    /* renamed from: a */
    public static C1955a m10154a(PredefinedEvent<?> uoVar) {
        return new C1955a(Type.PREDEFINED).mo11121b(uoVar.mo16541b()).mo11123c(uoVar.mo16542c()).mo11122b(uoVar.mo16460a());
    }

    public String toString() {
        if (this.f7737i == null) {
            this.f7737i = "[" + getClass().getSimpleName() + ": " + "timestamp=" + this.f7730b + ", type=" + this.f7731c + ", details=" + this.f7732d + ", customType=" + this.f7733e + ", customAttributes=" + this.f7734f + ", predefinedType=" + this.f7735g + ", predefinedAttributes=" + this.f7736h + ", metadata=[" + this.f7729a + "]]";
        }
        return this.f7737i;
    }
}
