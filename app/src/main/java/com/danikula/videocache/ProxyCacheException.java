package com.danikula.videocache;

public class ProxyCacheException extends Exception {
    public ProxyCacheException(String str) {
        super(str);
    }

    public ProxyCacheException(String str, Throwable th) {
        super(str, th);
    }
}
