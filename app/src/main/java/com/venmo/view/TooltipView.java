package com.venmo.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.widget.TextView;

public class TooltipView extends TextView {

    /* renamed from: a */
    private int f9743a;

    /* renamed from: b */
    private int f9744b;

    /* renamed from: c */
    private int f9745c;

    /* renamed from: d */
    private int f9746d;

    /* renamed from: e */
    private int f9747e;

    /* renamed from: f */
    private bcm f9748f;

    /* renamed from: g */
    private ArrowAlignment f9749g;

    /* renamed from: h */
    private int f9750h;

    /* renamed from: i */
    private int f9751i;

    /* renamed from: j */
    private Paint f9752j;

    /* renamed from: k */
    private Path f9753k;

    public TooltipView(Context context) {
        super(context);
        m12408a(null, 0);
    }

    public TooltipView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m12408a(attributeSet, 0);
    }

    public TooltipView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m12408a(attributeSet, i);
    }

    /* renamed from: a */
    private int m12407a(TypedArray typedArray, int i, int i2) {
        int dimensionPixelSize = typedArray.getDimensionPixelSize(i, Integer.MIN_VALUE);
        return dimensionPixelSize == Integer.MIN_VALUE ? getResources().getDimensionPixelSize(i2) : dimensionPixelSize;
    }

    /* renamed from: a */
    private void m12408a(AttributeSet attributeSet, int i) {
        Resources resources = getResources();
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, C1100c.TooltipView, i, 0);
        try {
            this.f9746d = obtainStyledAttributes.getResourceId(C1100c.TooltipView_anchoredView, -1);
            this.f9747e = obtainStyledAttributes.getColor(C1100c.TooltipView_tooltipColor, 0);
            this.f9745c = m12407a(obtainStyledAttributes, C1100c.TooltipView_cornerRadius, C1098a.tooltip_default_corner_radius);
            this.f9743a = m12407a(obtainStyledAttributes, C1100c.TooltipView_arrowHeight, C1098a.tooltip_default_arrow_height);
            this.f9744b = m12407a(obtainStyledAttributes, C1100c.TooltipView_arrowWidth, C1098a.tooltip_default_arrow_width);
            this.f9751i = obtainStyledAttributes.getInteger(C1100c.TooltipView_arrowLocation, resources.getInteger(C1099b.tooltip_default_arrow_location));
            this.f9748f = this.f9751i == 0 ? new bcp() : new bcn();
            this.f9749g = ArrowAlignment.m12405a(obtainStyledAttributes.getInteger(C1100c.TooltipView_arrowAlignment, resources.getInteger(C1099b.tooltip_default_arrow_alignment)));
            this.f9750h = m12407a(obtainStyledAttributes, C1100c.TooltipView_arrowAlignmentOffset, C1098a.tooltip_default_offset);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public int getAlignmentOffset() {
        return this.f9750h;
    }

    public int getAnchoredViewId() {
        return this.f9746d;
    }

    public ArrowAlignment getArrowAlignment() {
        return this.f9749g;
    }

    public int getArrowHeight() {
        return this.f9743a;
    }

    public int getArrowWidth() {
        return this.f9744b;
    }

    public int getCornerRadius() {
        return this.f9745c;
    }

    public int getTooltipColor() {
        return this.f9747e;
    }

    public Paint getTooltipPaint() {
        return this.f9752j;
    }

    public Path getTooltipPath() {
        return this.f9753k;
    }

    public void invalidate() {
        super.invalidate();
        this.f9753k = null;
        this.f9752j = null;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.f9753k == null || this.f9752j == null) {
            this.f9748f.mo8795a(this, canvas);
        }
        canvas.drawPath(this.f9753k, this.f9752j);
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight() + this.f9743a);
    }

    public void setAlignmentOffset(int i) {
        this.f9750h = i;
        invalidate();
    }

    public void setAlignmentOffsetResource(int i) {
        this.f9750h = getResources().getDimensionPixelSize(i);
        invalidate();
    }

    public void setAnchoredViewId(int i) {
        this.f9746d = i;
        invalidate();
    }

    public void setArrowAlignment(ArrowAlignment arrowAlignment) {
        this.f9749g = arrowAlignment;
        invalidate();
    }

    public void setArrowHeight(int i) {
        this.f9743a = i;
        invalidate();
    }

    public void setArrowHeightResource(int i) {
        this.f9743a = getResources().getDimensionPixelSize(i);
        invalidate();
    }

    public void setArrowPositioning(int i) {
        this.f9751i = i;
        invalidate();
    }

    public void setArrowWidth(int i) {
        this.f9744b = i;
        invalidate();
    }

    public void setArrowWidthResource(int i) {
        this.f9744b = getResources().getDimensionPixelSize(i);
        invalidate();
    }

    public void setCornerRadius(int i) {
        this.f9745c = i;
        invalidate();
    }

    public void setCornerRadiusResource(int i) {
        this.f9745c = getResources().getDimensionPixelSize(i);
        invalidate();
    }

    public void setPaint(Paint paint) {
        this.f9752j = paint;
    }

    public void setTooltipColor(int i) {
        this.f9747e = i;
        invalidate();
    }

    public void setTooltipPath(Path path) {
        this.f9753k = path;
    }
}
