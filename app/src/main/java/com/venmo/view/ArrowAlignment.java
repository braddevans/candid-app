package com.venmo.view;

public enum ArrowAlignment {
    START(0),
    CENTER(1),
    END(2),
    ANCHORED_VIEW(3);
    

    /* renamed from: e */
    private final int f9742e;

    private ArrowAlignment(int i) {
        this.f9742e = i;
    }

    /* renamed from: a */
    public static ArrowAlignment m12405a(int i) {
        ArrowAlignment[] values;
        for (ArrowAlignment arrowAlignment : values()) {
            if (i == arrowAlignment.mo13048a()) {
                return arrowAlignment;
            }
        }
        throw new IllegalArgumentException("No matching ArrowAlignment with value: " + i);
    }

    /* renamed from: a */
    public int mo13048a() {
        return this.f9742e;
    }
}
