package com.google.android.gms.maps.model;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.util.AttributeSet;
import com.google.android.gms.C2137R;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class LatLngBounds extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final auk CREATOR = new auk();

    /* renamed from: a */
    public final LatLng f9254a;

    /* renamed from: b */
    public final LatLng f9255b;

    /* renamed from: c */
    private final int f9256c;

    public LatLngBounds(int i, LatLng latLng, LatLng latLng2) {
        alw.m2724a(latLng, (Object) "null southwest");
        alw.m2724a(latLng2, (Object) "null northeast");
        alw.m2729a(latLng2.f9251a >= latLng.f9251a, "southern latitude exceeds northern latitude (%s > %s)", Double.valueOf(latLng.f9251a), Double.valueOf(latLng2.f9251a));
        this.f9256c = i;
        this.f9254a = latLng;
        this.f9255b = latLng2;
    }

    public LatLngBounds(LatLng latLng, LatLng latLng2) {
        this(1, latLng, latLng2);
    }

    /* renamed from: a */
    public static LatLngBounds m11929a(Context context, AttributeSet attributeSet) {
        if (context == null || attributeSet == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, C2137R.styleable.MapAttrs);
        Float f = obtainAttributes.hasValue(C2137R.styleable.MapAttrs_latLngBoundsSouthWestLatitude) ? Float.valueOf(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_latLngBoundsSouthWestLatitude, 0.0f)) : null;
        Float f2 = obtainAttributes.hasValue(C2137R.styleable.MapAttrs_latLngBoundsSouthWestLongitude) ? Float.valueOf(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_latLngBoundsSouthWestLongitude, 0.0f)) : null;
        Float f3 = obtainAttributes.hasValue(C2137R.styleable.MapAttrs_latLngBoundsNorthEastLatitude) ? Float.valueOf(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_latLngBoundsNorthEastLatitude, 0.0f)) : null;
        Float f4 = obtainAttributes.hasValue(C2137R.styleable.MapAttrs_latLngBoundsNorthEastLongitude) ? Float.valueOf(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_latLngBoundsNorthEastLongitude, 0.0f)) : null;
        if (f == null || f2 == null || f3 == null || f4 == null) {
            return null;
        }
        return new LatLngBounds(new LatLng((double) f.floatValue(), (double) f2.floatValue()), new LatLng((double) f3.floatValue(), (double) f4.floatValue()));
    }

    /* renamed from: a */
    public int mo12630a() {
        return this.f9256c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLngBounds)) {
            return false;
        }
        LatLngBounds latLngBounds = (LatLngBounds) obj;
        return this.f9254a.equals(latLngBounds.f9254a) && this.f9255b.equals(latLngBounds.f9255b);
    }

    public int hashCode() {
        return alv.m2719a(this.f9254a, this.f9255b);
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("southwest", this.f9254a).mo1191a("northeast", this.f9255b).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        auk.m5684a(this, parcel, i);
    }
}
