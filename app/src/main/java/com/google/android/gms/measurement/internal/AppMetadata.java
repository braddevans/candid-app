package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class AppMetadata extends AbstractSafeParcelable {
    public static final avn CREATOR = new avn();

    /* renamed from: a */
    public final int f9340a;

    /* renamed from: b */
    public final String f9341b;

    /* renamed from: c */
    public final String f9342c;

    /* renamed from: d */
    public final String f9343d;

    /* renamed from: e */
    public final String f9344e;

    /* renamed from: f */
    public final long f9345f;

    /* renamed from: g */
    public final long f9346g;

    /* renamed from: h */
    public final String f9347h;

    /* renamed from: i */
    public final boolean f9348i;

    /* renamed from: j */
    public final boolean f9349j;

    /* renamed from: k */
    public final long f9350k;

    /* renamed from: l */
    public final String f9351l;

    public AppMetadata(int i, String str, String str2, String str3, String str4, long j, long j2, String str5, boolean z, boolean z2, long j3, String str6) {
        this.f9340a = i;
        this.f9341b = str;
        this.f9342c = str2;
        this.f9343d = str3;
        if (i < 5) {
            j3 = -2147483648L;
        }
        this.f9350k = j3;
        this.f9344e = str4;
        this.f9345f = j;
        this.f9346g = j2;
        this.f9347h = str5;
        if (i >= 3) {
            this.f9348i = z;
        } else {
            this.f9348i = true;
        }
        this.f9349j = z2;
        this.f9351l = str6;
    }

    public AppMetadata(String str, String str2, String str3, long j, String str4, long j2, long j3, String str5, boolean z, boolean z2, String str6) {
        alw.m2725a(str);
        this.f9340a = 6;
        this.f9341b = str;
        if (TextUtils.isEmpty(str2)) {
            str2 = null;
        }
        this.f9342c = str2;
        this.f9343d = str3;
        this.f9350k = j;
        this.f9344e = str4;
        this.f9345f = j2;
        this.f9346g = j3;
        this.f9347h = str5;
        this.f9348i = z;
        this.f9349j = z2;
        this.f9351l = str6;
    }

    public void writeToParcel(Parcel parcel, int i) {
        avn.m6041a(this, parcel, i);
    }
}
