package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class GroundOverlayOptions extends AbstractSafeParcelable {
    public static final auj CREATOR = new auj();

    /* renamed from: a */
    private final int f9238a;

    /* renamed from: b */
    private atv f9239b;

    /* renamed from: c */
    private LatLng f9240c;

    /* renamed from: d */
    private float f9241d;

    /* renamed from: e */
    private float f9242e;

    /* renamed from: f */
    private LatLngBounds f9243f;

    /* renamed from: g */
    private float f9244g;

    /* renamed from: h */
    private float f9245h;

    /* renamed from: i */
    private boolean f9246i;

    /* renamed from: j */
    private float f9247j;

    /* renamed from: k */
    private float f9248k;

    /* renamed from: l */
    private float f9249l;

    /* renamed from: m */
    private boolean f9250m;

    public GroundOverlayOptions() {
        this.f9246i = true;
        this.f9247j = 0.0f;
        this.f9248k = 0.5f;
        this.f9249l = 0.5f;
        this.f9250m = false;
        this.f9238a = 1;
    }

    public GroundOverlayOptions(int i, IBinder iBinder, LatLng latLng, float f, float f2, LatLngBounds latLngBounds, float f3, float f4, boolean z, float f5, float f6, float f7, boolean z2) {
        this.f9246i = true;
        this.f9247j = 0.0f;
        this.f9248k = 0.5f;
        this.f9249l = 0.5f;
        this.f9250m = false;
        this.f9238a = i;
        this.f9239b = new atv(C0670a.m4132a(iBinder));
        this.f9240c = latLng;
        this.f9241d = f;
        this.f9242e = f2;
        this.f9243f = latLngBounds;
        this.f9244g = f3;
        this.f9245h = f4;
        this.f9246i = z;
        this.f9247j = f5;
        this.f9248k = f6;
        this.f9249l = f7;
        this.f9250m = z2;
    }

    /* renamed from: a */
    public IBinder mo12611a() {
        return this.f9239b.mo7374a().asBinder();
    }

    /* renamed from: b */
    public int mo12612b() {
        return this.f9238a;
    }

    /* renamed from: c */
    public LatLng mo12613c() {
        return this.f9240c;
    }

    /* renamed from: d */
    public float mo12614d() {
        return this.f9241d;
    }

    /* renamed from: e */
    public float mo12615e() {
        return this.f9242e;
    }

    /* renamed from: f */
    public LatLngBounds mo12616f() {
        return this.f9243f;
    }

    /* renamed from: g */
    public float mo12617g() {
        return this.f9244g;
    }

    /* renamed from: h */
    public float mo12618h() {
        return this.f9245h;
    }

    /* renamed from: i */
    public float mo12619i() {
        return this.f9247j;
    }

    /* renamed from: j */
    public float mo12620j() {
        return this.f9248k;
    }

    /* renamed from: k */
    public float mo12621k() {
        return this.f9249l;
    }

    /* renamed from: l */
    public boolean mo12622l() {
        return this.f9246i;
    }

    /* renamed from: m */
    public boolean mo12623m() {
        return this.f9250m;
    }

    public void writeToParcel(Parcel parcel, int i) {
        auj.m5681a(this, parcel, i);
    }
}
