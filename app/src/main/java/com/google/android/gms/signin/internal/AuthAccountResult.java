package com.google.android.gms.signin.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class AuthAccountResult extends AbstractSafeParcelable implements alf {
    public static final Creator<AuthAccountResult> CREATOR = new awl();

    /* renamed from: a */
    public final int f9388a;

    /* renamed from: b */
    private int f9389b;

    /* renamed from: c */
    private Intent f9390c;

    public AuthAccountResult() {
        this(0, null);
    }

    public AuthAccountResult(int i, int i2, Intent intent) {
        this.f9388a = i;
        this.f9389b = i2;
        this.f9390c = intent;
    }

    public AuthAccountResult(int i, Intent intent) {
        this(2, i, intent);
    }

    /* renamed from: a */
    public Status mo1166a() {
        return this.f9389b == 0 ? Status.f8890a : Status.f8894e;
    }

    /* renamed from: b */
    public int mo12756b() {
        return this.f9389b;
    }

    /* renamed from: c */
    public Intent mo12757c() {
        return this.f9390c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        awl.m6544a(this, parcel, i);
    }
}
