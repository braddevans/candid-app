package com.google.android.gms.auth.api.signin;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInAccount extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Creator<GoogleSignInAccount> CREATOR = new akx();

    /* renamed from: a */
    public static ane f8858a = ang.m4032d();

    /* renamed from: n */
    private static Comparator<Scope> f8859n = new Comparator<Scope>() {
        /* renamed from: a */
        public int compare(Scope scope, Scope scope2) {
            return scope.mo12216a().compareTo(scope2.mo12216a());
        }
    };

    /* renamed from: b */
    public final int f8860b;

    /* renamed from: c */
    public List<Scope> f8861c;

    /* renamed from: d */
    private String f8862d;

    /* renamed from: e */
    private String f8863e;

    /* renamed from: f */
    private String f8864f;

    /* renamed from: g */
    private String f8865g;

    /* renamed from: h */
    private Uri f8866h;

    /* renamed from: i */
    private String f8867i;

    /* renamed from: j */
    private long f8868j;

    /* renamed from: k */
    private String f8869k;

    /* renamed from: l */
    private String f8870l;

    /* renamed from: m */
    private String f8871m;

    public GoogleSignInAccount(int i, String str, String str2, String str3, String str4, Uri uri, String str5, long j, String str6, List<Scope> list, String str7, String str8) {
        this.f8860b = i;
        this.f8862d = str;
        this.f8863e = str2;
        this.f8864f = str3;
        this.f8865g = str4;
        this.f8866h = uri;
        this.f8867i = str5;
        this.f8868j = j;
        this.f8869k = str6;
        this.f8861c = list;
        this.f8870l = str7;
        this.f8871m = str8;
    }

    /* renamed from: l */
    private JSONObject m11442l() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (mo12173a() != null) {
                jSONObject.put("id", mo12173a());
            }
            if (mo12174b() != null) {
                jSONObject.put("tokenId", mo12174b());
            }
            if (mo12175c() != null) {
                jSONObject.put("email", mo12175c());
            }
            if (mo12176d() != null) {
                jSONObject.put("displayName", mo12176d());
            }
            if (mo12177e() != null) {
                jSONObject.put("givenName", mo12177e());
            }
            if (mo12179f() != null) {
                jSONObject.put("familyName", mo12179f());
            }
            if (mo12180g() != null) {
                jSONObject.put("photoUrl", mo12180g().toString());
            }
            if (mo12181h() != null) {
                jSONObject.put("serverAuthCode", mo12181h());
            }
            jSONObject.put("expirationTime", this.f8868j);
            jSONObject.put("obfuscatedIdentifier", mo12184j());
            JSONArray jSONArray = new JSONArray();
            Collections.sort(this.f8861c, f8859n);
            for (Scope a : this.f8861c) {
                jSONArray.put(a.mo12216a());
            }
            jSONObject.put("grantedScopes", jSONArray);
            return jSONObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public String mo12173a() {
        return this.f8862d;
    }

    /* renamed from: b */
    public String mo12174b() {
        return this.f8863e;
    }

    /* renamed from: c */
    public String mo12175c() {
        return this.f8864f;
    }

    /* renamed from: d */
    public String mo12176d() {
        return this.f8865g;
    }

    /* renamed from: e */
    public String mo12177e() {
        return this.f8870l;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof GoogleSignInAccount)) {
            return false;
        }
        return ((GoogleSignInAccount) obj).mo12185k().equals(mo12185k());
    }

    /* renamed from: f */
    public String mo12179f() {
        return this.f8871m;
    }

    /* renamed from: g */
    public Uri mo12180g() {
        return this.f8866h;
    }

    /* renamed from: h */
    public String mo12181h() {
        return this.f8867i;
    }

    public int hashCode() {
        return mo12185k().hashCode();
    }

    /* renamed from: i */
    public long mo12183i() {
        return this.f8868j;
    }

    /* renamed from: j */
    public String mo12184j() {
        return this.f8869k;
    }

    /* renamed from: k */
    public String mo12185k() {
        return m11442l().toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        akx.m2596a(this, parcel, i);
    }
}
