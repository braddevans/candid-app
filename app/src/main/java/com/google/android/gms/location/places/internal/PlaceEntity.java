package com.google.android.gms.location.places.internal;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public final class PlaceEntity extends AbstractSafeParcelable implements arc, ReflectedParcelable {
    public static final ard CREATOR = new ard();

    /* renamed from: a */
    public final int f9114a;

    /* renamed from: b */
    private final String f9115b;

    /* renamed from: c */
    private final Bundle f9116c;
    @Deprecated

    /* renamed from: d */
    private final PlaceLocalization f9117d;

    /* renamed from: e */
    private final LatLng f9118e;

    /* renamed from: f */
    private final float f9119f;

    /* renamed from: g */
    private final LatLngBounds f9120g;

    /* renamed from: h */
    private final String f9121h;

    /* renamed from: i */
    private final Uri f9122i;

    /* renamed from: j */
    private final boolean f9123j;

    /* renamed from: k */
    private final float f9124k;

    /* renamed from: l */
    private final int f9125l;

    /* renamed from: m */
    private final long f9126m;

    /* renamed from: n */
    private final List<Integer> f9127n;

    /* renamed from: o */
    private final List<Integer> f9128o;

    /* renamed from: p */
    private final String f9129p;

    /* renamed from: q */
    private final String f9130q;

    /* renamed from: r */
    private final String f9131r;

    /* renamed from: s */
    private final String f9132s;

    /* renamed from: t */
    private final List<String> f9133t;

    /* renamed from: u */
    private final Map<Integer, String> f9134u;

    /* renamed from: v */
    private final TimeZone f9135v;

    /* renamed from: w */
    private Locale f9136w;

    public PlaceEntity(int i, String str, List<Integer> list, List<Integer> list2, Bundle bundle, String str2, String str3, String str4, String str5, List<String> list3, LatLng latLng, float f, LatLngBounds latLngBounds, String str6, Uri uri, boolean z, float f2, int i2, long j, PlaceLocalization placeLocalization) {
        this.f9114a = i;
        this.f9115b = str;
        this.f9128o = Collections.unmodifiableList(list);
        this.f9127n = list2;
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.f9116c = bundle;
        this.f9129p = str2;
        this.f9130q = str3;
        this.f9131r = str4;
        this.f9132s = str5;
        if (list3 == null) {
            list3 = Collections.emptyList();
        }
        this.f9133t = list3;
        this.f9118e = latLng;
        this.f9119f = f;
        this.f9120g = latLngBounds;
        if (str6 == null) {
            str6 = "UTC";
        }
        this.f9121h = str6;
        this.f9122i = uri;
        this.f9123j = z;
        this.f9124k = f2;
        this.f9125l = i2;
        this.f9126m = j;
        this.f9134u = Collections.unmodifiableMap(new HashMap());
        this.f9135v = null;
        this.f9136w = null;
        this.f9117d = placeLocalization;
    }

    /* renamed from: b */
    public String mo12475b() {
        return this.f9115b;
    }

    /* renamed from: c */
    public List<Integer> mo12476c() {
        return this.f9128o;
    }

    /* renamed from: d */
    public List<Integer> mo12477d() {
        return this.f9127n;
    }

    /* renamed from: e */
    public String mo6961a() {
        return this.f9129p;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PlaceEntity)) {
            return false;
        }
        PlaceEntity placeEntity = (PlaceEntity) obj;
        return this.f9115b.equals(placeEntity.f9115b) && alv.m2721a(this.f9136w, placeEntity.f9136w) && this.f9126m == placeEntity.f9126m;
    }

    /* renamed from: f */
    public String mo12498v() {
        return this.f9130q;
    }

    /* renamed from: g */
    public LatLng mo12481g() {
        return this.f9118e;
    }

    /* renamed from: h */
    public float mo12482h() {
        return this.f9119f;
    }

    public int hashCode() {
        return alv.m2719a(this.f9115b, this.f9136w, Long.valueOf(this.f9126m));
    }

    /* renamed from: i */
    public LatLngBounds mo12484i() {
        return this.f9120g;
    }

    /* renamed from: j */
    public Uri mo12485j() {
        return this.f9122i;
    }

    /* renamed from: k */
    public String mo12497u() {
        return this.f9131r;
    }

    /* renamed from: l */
    public String mo12487l() {
        return this.f9132s;
    }

    /* renamed from: m */
    public List<String> mo12488m() {
        return this.f9133t;
    }

    /* renamed from: n */
    public boolean mo12489n() {
        return this.f9123j;
    }

    /* renamed from: o */
    public float mo12490o() {
        return this.f9124k;
    }

    /* renamed from: p */
    public int mo12491p() {
        return this.f9125l;
    }

    /* renamed from: q */
    public long mo12492q() {
        return this.f9126m;
    }

    /* renamed from: r */
    public Bundle mo12493r() {
        return this.f9116c;
    }

    /* renamed from: s */
    public String mo12494s() {
        return this.f9121h;
    }

    @Deprecated
    /* renamed from: t */
    public PlaceLocalization mo12495t() {
        return this.f9117d;
    }

    @SuppressLint({"DefaultLocale"})
    public String toString() {
        return alv.m2720a((Object) this).mo1191a("id", this.f9115b).mo1191a("placeTypes", this.f9128o).mo1191a("locale", this.f9136w).mo1191a("name", this.f9129p).mo1191a("address", this.f9130q).mo1191a("phoneNumber", this.f9131r).mo1191a("latlng", this.f9118e).mo1191a("viewport", this.f9120g).mo1191a("websiteUri", this.f9122i).mo1191a("isPermanentlyClosed", Boolean.valueOf(this.f9123j)).mo1191a("priceLevel", Integer.valueOf(this.f9125l)).mo1191a("timestampSecs", Long.valueOf(this.f9126m)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        ard.m4800a(this, parcel, i);
    }
}
