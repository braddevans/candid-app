package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class SignInResponse extends AbstractSafeParcelable {
    public static final Creator<SignInResponse> CREATOR = new aws();

    /* renamed from: a */
    public final int f9400a;

    /* renamed from: b */
    private final ConnectionResult f9401b;

    /* renamed from: c */
    private final ResolveAccountResponse f9402c;

    public SignInResponse(int i, ConnectionResult connectionResult, ResolveAccountResponse resolveAccountResponse) {
        this.f9400a = i;
        this.f9401b = connectionResult;
        this.f9402c = resolveAccountResponse;
    }

    /* renamed from: a */
    public ConnectionResult mo12766a() {
        return this.f9401b;
    }

    /* renamed from: b */
    public ResolveAccountResponse mo12767b() {
        return this.f9402c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        aws.m6595a(this, parcel, i);
    }
}
