package com.google.android.gms.internal;

import android.support.p001v4.app.NotificationCompat;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class zzard {

    /* renamed from: a */
    private final ByteBuffer f8993a;

    public static class zza extends IOException {
        zza(int i, int i2) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space (pos " + i + " limit " + i2 + ").");
        }
    }

    private zzard(ByteBuffer byteBuffer) {
        this.f8993a = byteBuffer;
        this.f8993a.order(ByteOrder.LITTLE_ENDIAN);
    }

    private zzard(byte[] bArr, int i, int i2) {
        this(ByteBuffer.wrap(bArr, i, i2));
    }

    /* renamed from: a */
    private static int m11610a(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        while (i < length && charSequence.charAt(i) < 128) {
            i++;
        }
        int i2 = i;
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt >= 2048) {
                i3 += m11611a(charSequence, i2);
                break;
            }
            i2++;
            i3 = ((127 - charAt) >>> 31) + i3;
        }
        if (i3 >= length) {
            return i3;
        }
        throw new IllegalArgumentException("UTF-8 length does not fit in int: " + (((long) i3) + 4294967296L));
    }

    /* renamed from: a */
    private static int m11611a(CharSequence charSequence, int i) {
        int length = charSequence.length();
        int i2 = 0;
        int i3 = i;
        while (i3 < length) {
            char charAt = charSequence.charAt(i3);
            if (charAt < 2048) {
                i2 += (127 - charAt) >>> 31;
            } else {
                i2 += 2;
                if (55296 <= charAt && charAt <= 57343) {
                    if (Character.codePointAt(charSequence, i3) < 65536) {
                        throw new IllegalArgumentException("Unpaired surrogate at index " + i3);
                    }
                    i3++;
                }
            }
            i3++;
        }
        return i2;
    }

    /* renamed from: a */
    private static int m11612a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        int i3;
        int length = charSequence.length();
        int i4 = 0;
        int i5 = i + i2;
        while (i4 < length && i4 + i < i5) {
            char charAt = charSequence.charAt(i4);
            if (charAt >= 128) {
                break;
            }
            bArr[i + i4] = (byte) charAt;
            i4++;
        }
        if (i4 == length) {
            return i + length;
        }
        int i6 = i + i4;
        while (i4 < length) {
            char charAt2 = charSequence.charAt(i4);
            if (charAt2 < 128 && i6 < i5) {
                i3 = i6 + 1;
                bArr[i6] = (byte) charAt2;
            } else if (charAt2 < 2048 && i6 <= i5 - 2) {
                int i7 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 6) | 960);
                i3 = i7 + 1;
                bArr[i7] = (byte) ((charAt2 & '?') | 128);
            } else if ((charAt2 < 55296 || 57343 < charAt2) && i6 <= i5 - 3) {
                int i8 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 12) | 480);
                int i9 = i8 + 1;
                bArr[i8] = (byte) (((charAt2 >>> 6) & 63) | NotificationCompat.FLAG_HIGH_PRIORITY);
                i3 = i9 + 1;
                bArr[i9] = (byte) ((charAt2 & '?') | 128);
            } else if (i6 <= i5 - 4) {
                if (i4 + 1 != charSequence.length()) {
                    i4++;
                    char charAt3 = charSequence.charAt(i4);
                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                        int i10 = i6 + 1;
                        bArr[i6] = (byte) ((codePoint >>> 18) | 240);
                        int i11 = i10 + 1;
                        bArr[i10] = (byte) (((codePoint >>> 12) & 63) | NotificationCompat.FLAG_HIGH_PRIORITY);
                        int i12 = i11 + 1;
                        bArr[i11] = (byte) (((codePoint >>> 6) & 63) | NotificationCompat.FLAG_HIGH_PRIORITY);
                        i3 = i12 + 1;
                        bArr[i12] = (byte) ((codePoint & 63) | NotificationCompat.FLAG_HIGH_PRIORITY);
                    }
                }
                throw new IllegalArgumentException("Unpaired surrogate at index " + (i4 - 1));
            } else {
                throw new ArrayIndexOutOfBoundsException("Failed writing " + charAt2 + " at index " + i6);
            }
            i4++;
            i6 = i3;
        }
        return i6;
    }

    /* renamed from: a */
    public static zzard m11613a(byte[] bArr) {
        return m11614a(bArr, 0, bArr.length);
    }

    /* renamed from: a */
    public static zzard m11614a(byte[] bArr, int i, int i2) {
        return new zzard(bArr, i, i2);
    }

    /* renamed from: a */
    private static void m11615a(CharSequence charSequence, ByteBuffer byteBuffer) {
        if (byteBuffer.isReadOnly()) {
            throw new ReadOnlyBufferException();
        } else if (byteBuffer.hasArray()) {
            try {
                byteBuffer.position(m11612a(charSequence, byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining()) - byteBuffer.arrayOffset());
            } catch (ArrayIndexOutOfBoundsException e) {
                BufferOverflowException bufferOverflowException = new BufferOverflowException();
                bufferOverflowException.initCause(e);
                throw bufferOverflowException;
            }
        } else {
            m11628b(charSequence, byteBuffer);
        }
    }

    /* renamed from: b */
    public static int m11616b(double d) {
        return 8;
    }

    /* renamed from: b */
    public static int m11617b(float f) {
        return 4;
    }

    /* renamed from: b */
    public static int m11618b(int i) {
        if (i >= 0) {
            return m11633f(i);
        }
        return 10;
    }

    /* renamed from: b */
    public static int m11619b(int i, double d) {
        return m11631d(i) + m11616b(d);
    }

    /* renamed from: b */
    public static int m11620b(int i, float f) {
        return m11631d(i) + m11617b(f);
    }

    /* renamed from: b */
    public static int m11621b(int i, int i2) {
        return m11631d(i) + m11618b(i2);
    }

    /* renamed from: b */
    public static int m11622b(int i, aol aol) {
        return m11631d(i) + m11625b(aol);
    }

    /* renamed from: b */
    public static int m11623b(int i, String str) {
        return m11631d(i) + m11626b(str);
    }

    /* renamed from: b */
    public static int m11624b(int i, boolean z) {
        return m11631d(i) + m11627b(z);
    }

    /* renamed from: b */
    public static int m11625b(aol aol) {
        int b = aol.mo6651b();
        return b + m11633f(b);
    }

    /* renamed from: b */
    public static int m11626b(String str) {
        int a = m11610a((CharSequence) str);
        return a + m11633f(a);
    }

    /* renamed from: b */
    public static int m11627b(boolean z) {
        return 1;
    }

    /* renamed from: b */
    private static void m11628b(CharSequence charSequence, ByteBuffer byteBuffer) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            char charAt = charSequence.charAt(i);
            if (charAt < 128) {
                byteBuffer.put((byte) charAt);
            } else if (charAt < 2048) {
                byteBuffer.put((byte) ((charAt >>> 6) | 960));
                byteBuffer.put((byte) ((charAt & '?') | 128));
            } else if (charAt < 55296 || 57343 < charAt) {
                byteBuffer.put((byte) ((charAt >>> 12) | 480));
                byteBuffer.put((byte) (((charAt >>> 6) & 63) | NotificationCompat.FLAG_HIGH_PRIORITY));
                byteBuffer.put((byte) ((charAt & '?') | 128));
            } else {
                if (i + 1 != charSequence.length()) {
                    i++;
                    char charAt2 = charSequence.charAt(i);
                    if (Character.isSurrogatePair(charAt, charAt2)) {
                        int codePoint = Character.toCodePoint(charAt, charAt2);
                        byteBuffer.put((byte) ((codePoint >>> 18) | 240));
                        byteBuffer.put((byte) (((codePoint >>> 12) & 63) | NotificationCompat.FLAG_HIGH_PRIORITY));
                        byteBuffer.put((byte) (((codePoint >>> 6) & 63) | NotificationCompat.FLAG_HIGH_PRIORITY));
                        byteBuffer.put((byte) ((codePoint & 63) | NotificationCompat.FLAG_HIGH_PRIORITY));
                    }
                }
                throw new IllegalArgumentException("Unpaired surrogate at index " + (i - 1));
            }
            i++;
        }
    }

    /* renamed from: c */
    public static int m11629c(int i, long j) {
        return m11631d(i) + m11632d(j);
    }

    /* renamed from: c */
    public static int m11630c(long j) {
        return m11634f(j);
    }

    /* renamed from: d */
    public static int m11631d(int i) {
        return m11633f(aon.m4291a(i, 0));
    }

    /* renamed from: d */
    public static int m11632d(long j) {
        return m11634f(j);
    }

    /* renamed from: f */
    public static int m11633f(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    /* renamed from: f */
    public static int m11634f(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (Long.MIN_VALUE & j) == 0 ? 9 : 10;
    }

    /* renamed from: a */
    public int mo12318a() {
        return this.f8993a.remaining();
    }

    /* renamed from: a */
    public void mo12319a(byte b) throws IOException {
        if (!this.f8993a.hasRemaining()) {
            throw new zza(this.f8993a.position(), this.f8993a.limit());
        }
        this.f8993a.put(b);
    }

    /* renamed from: a */
    public void mo12320a(double d) throws IOException {
        mo12342g(Double.doubleToLongBits(d));
    }

    /* renamed from: a */
    public void mo12321a(float f) throws IOException {
        mo12341g(Float.floatToIntBits(f));
    }

    /* renamed from: a */
    public void mo12322a(int i) throws IOException {
        if (i >= 0) {
            mo12339e(i);
        } else {
            mo12340e((long) i);
        }
    }

    /* renamed from: a */
    public void mo12323a(int i, double d) throws IOException {
        mo12338c(i, 1);
        mo12320a(d);
    }

    /* renamed from: a */
    public void mo12324a(int i, float f) throws IOException {
        mo12338c(i, 5);
        mo12321a(f);
    }

    /* renamed from: a */
    public void mo12325a(int i, int i2) throws IOException {
        mo12338c(i, 0);
        mo12322a(i2);
    }

    /* renamed from: a */
    public void mo12326a(int i, long j) throws IOException {
        mo12338c(i, 0);
        mo12330a(j);
    }

    /* renamed from: a */
    public void mo12327a(int i, aol aol) throws IOException {
        mo12338c(i, 2);
        mo12331a(aol);
    }

    /* renamed from: a */
    public void mo12328a(int i, String str) throws IOException {
        mo12338c(i, 2);
        mo12332a(str);
    }

    /* renamed from: a */
    public void mo12329a(int i, boolean z) throws IOException {
        mo12338c(i, 0);
        mo12333a(z);
    }

    /* renamed from: a */
    public void mo12330a(long j) throws IOException {
        mo12340e(j);
    }

    /* renamed from: a */
    public void mo12331a(aol aol) throws IOException {
        mo12339e(aol.mo6648a());
        aol.mo6650a(this);
    }

    /* renamed from: a */
    public void mo12332a(String str) throws IOException {
        try {
            int f = m11633f(str.length());
            if (f == m11633f(str.length() * 3)) {
                int position = this.f8993a.position();
                if (this.f8993a.remaining() < f) {
                    throw new zza(f + position, this.f8993a.limit());
                }
                this.f8993a.position(position + f);
                m11615a((CharSequence) str, this.f8993a);
                int position2 = this.f8993a.position();
                this.f8993a.position(position);
                mo12339e((position2 - position) - f);
                this.f8993a.position(position2);
                return;
            }
            mo12339e(m11610a((CharSequence) str));
            m11615a((CharSequence) str, this.f8993a);
        } catch (BufferOverflowException e) {
            zza zza2 = new zza(this.f8993a.position(), this.f8993a.limit());
            zza2.initCause(e);
            throw zza2;
        }
    }

    /* renamed from: a */
    public void mo12333a(boolean z) throws IOException {
        mo12337c(z ? 1 : 0);
    }

    /* renamed from: b */
    public void mo12334b() {
        if (mo12318a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    /* renamed from: b */
    public void mo12335b(int i, long j) throws IOException {
        mo12338c(i, 0);
        mo12336b(j);
    }

    /* renamed from: b */
    public void mo12336b(long j) throws IOException {
        mo12340e(j);
    }

    /* renamed from: c */
    public void mo12337c(int i) throws IOException {
        mo12319a((byte) i);
    }

    /* renamed from: c */
    public void mo12338c(int i, int i2) throws IOException {
        mo12339e(aon.m4291a(i, i2));
    }

    /* renamed from: e */
    public void mo12339e(int i) throws IOException {
        while ((i & -128) != 0) {
            mo12337c((i & 127) | NotificationCompat.FLAG_HIGH_PRIORITY);
            i >>>= 7;
        }
        mo12337c(i);
    }

    /* renamed from: e */
    public void mo12340e(long j) throws IOException {
        while ((-128 & j) != 0) {
            mo12337c((((int) j) & 127) | NotificationCompat.FLAG_HIGH_PRIORITY);
            j >>>= 7;
        }
        mo12337c((int) j);
    }

    /* renamed from: g */
    public void mo12341g(int i) throws IOException {
        if (this.f8993a.remaining() < 4) {
            throw new zza(this.f8993a.position(), this.f8993a.limit());
        }
        this.f8993a.putInt(i);
    }

    /* renamed from: g */
    public void mo12342g(long j) throws IOException {
        if (this.f8993a.remaining() < 8) {
            throw new zza(this.f8993a.position(), this.f8993a.limit());
        }
        this.f8993a.putLong(j);
    }
}
