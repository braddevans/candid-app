package com.google.android.gms.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import com.google.android.gms.C2137R;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzah;
import com.google.android.gms.dynamic.zzg.zza;

public final class SignInButton extends FrameLayout implements OnClickListener {

    /* renamed from: a */
    private int f8882a;

    /* renamed from: b */
    private int f8883b;

    /* renamed from: c */
    private View f8884c;

    /* renamed from: d */
    private OnClickListener f8885d;

    public SignInButton(Context context) {
        this(context, null);
    }

    public SignInButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SignInButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f8885d = null;
        m11465a(context, attributeSet);
        setStyle(this.f8882a, this.f8883b);
    }

    /* renamed from: a */
    private static Button m11463a(Context context, int i, int i2) {
        zzah zzah = new zzah(context);
        zzah.mo12269a(context.getResources(), i, i2);
        return zzah;
    }

    /* renamed from: a */
    private void m11464a(Context context) {
        if (this.f8884c != null) {
            removeView(this.f8884c);
        }
        try {
            this.f8884c = ama.m2748a(context, this.f8882a, this.f8883b);
        } catch (zza e) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            this.f8884c = m11463a(context, this.f8882a, this.f8883b);
        }
        addView(this.f8884c);
        this.f8884c.setEnabled(isEnabled());
        this.f8884c.setOnClickListener(this);
    }

    /* renamed from: a */
    private void m11465a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, C2137R.styleable.SignInButton, 0, 0);
        try {
            this.f8882a = obtainStyledAttributes.getInt(C2137R.styleable.SignInButton_buttonSize, 0);
            this.f8883b = obtainStyledAttributes.getInt(C2137R.styleable.SignInButton_colorScheme, 2);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public void onClick(View view) {
        if (this.f8885d != null && view == this.f8884c) {
            this.f8885d.onClick(this);
        }
    }

    public void setColorScheme(int i) {
        setStyle(this.f8882a, i);
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.f8884c.setEnabled(z);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.f8885d = onClickListener;
        if (this.f8884c != null) {
            this.f8884c.setOnClickListener(this);
        }
    }

    @Deprecated
    public void setScopes(Scope[] scopeArr) {
        setStyle(this.f8882a, this.f8883b);
    }

    public void setSize(int i) {
        setStyle(i, this.f8883b);
    }

    public void setStyle(int i, int i2) {
        this.f8882a = i;
        this.f8883b = i2;
        m11464a(getContext());
    }

    @Deprecated
    public void setStyle(int i, int i2, Scope[] scopeArr) {
        setStyle(i, i2);
    }
}
