package com.google.android.gms.gcm;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import java.util.Iterator;

public abstract class GcmListenerService extends Service {

    /* renamed from: a */
    private final Object f8968a = new Object();

    /* renamed from: b */
    private int f8969b;

    /* renamed from: c */
    private int f8970c = 0;

    @TargetApi(11)
    /* renamed from: a */
    private void m11563a(final Intent intent) {
        if (VERSION.SDK_INT >= 11) {
            AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
                public void run() {
                    GcmListenerService.this.m11567b(intent);
                }
            });
        } else {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public Void doInBackground(Void... voidArr) {
                    GcmListenerService.this.m11567b(intent);
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    /* renamed from: a */
    static void m11564a(Bundle bundle) {
        Iterator it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (str != null && str.startsWith("google.c.")) {
                it.remove();
            }
        }
    }

    /* renamed from: b */
    private void m11566b() {
        synchronized (this.f8968a) {
            this.f8970c--;
            if (this.f8970c == 0) {
                mo12288a(this.f8969b);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m11567b(android.content.Intent r5) {
        /*
            r4 = this;
            java.lang.String r1 = r5.getAction()     // Catch:{ all -> 0x003d }
            r0 = -1
            int r2 = r1.hashCode()     // Catch:{ all -> 0x003d }
            switch(r2) {
                case 366519424: goto L_0x002f;
                default: goto L_0x000c;
            }     // Catch:{ all -> 0x003d }
        L_0x000c:
            switch(r0) {
                case 0: goto L_0x0039;
                default: goto L_0x000f;
            }     // Catch:{ all -> 0x003d }
        L_0x000f:
            java.lang.String r1 = "GcmListenerService"
            java.lang.String r2 = "Unknown intent action: "
            java.lang.String r0 = r5.getAction()     // Catch:{ all -> 0x003d }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x003d }
            int r3 = r0.length()     // Catch:{ all -> 0x003d }
            if (r3 == 0) goto L_0x0042
            java.lang.String r0 = r2.concat(r0)     // Catch:{ all -> 0x003d }
        L_0x0025:
            android.util.Log.d(r1, r0)     // Catch:{ all -> 0x003d }
        L_0x0028:
            r4.m11566b()     // Catch:{ all -> 0x003d }
            com.google.android.gms.gcm.GcmReceiver.m3705a(r5)
            return
        L_0x002f:
            java.lang.String r2 = "com.google.android.c2dm.intent.RECEIVE"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x000c
            r0 = 0
            goto L_0x000c
        L_0x0039:
            r4.m11568c(r5)     // Catch:{ all -> 0x003d }
            goto L_0x0028
        L_0x003d:
            r0 = move-exception
            com.google.android.gms.gcm.GcmReceiver.m3705a(r5)
            throw r0
        L_0x0042:
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x003d }
            r0.<init>(r2)     // Catch:{ all -> 0x003d }
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.gcm.GcmListenerService.m11567b(android.content.Intent):void");
    }

    /* renamed from: c */
    private void m11568c(Intent intent) {
        String stringExtra = intent.getStringExtra("message_type");
        if (stringExtra == null) {
            stringExtra = "gcm";
        }
        char c = 65535;
        switch (stringExtra.hashCode()) {
            case -2062414158:
                if (stringExtra.equals("deleted_messages")) {
                    c = 1;
                    break;
                }
                break;
            case 102161:
                if (stringExtra.equals("gcm")) {
                    c = 0;
                    break;
                }
                break;
            case 814694033:
                if (stringExtra.equals("send_error")) {
                    c = 3;
                    break;
                }
                break;
            case 814800675:
                if (stringExtra.equals("send_event")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                m11569d(intent);
                return;
            case 1:
                mo12285a();
                return;
            case 2:
                mo12286a(intent.getStringExtra("google.message_id"));
                return;
            case 3:
                mo12287a(m11570e(intent), intent.getStringExtra("error"));
                return;
            default:
                String str = "GcmListenerService";
                String str2 = "Received message with unknown type: ";
                String valueOf = String.valueOf(stringExtra);
                Log.w(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                return;
        }
    }

    /* renamed from: d */
    private void m11569d(Intent intent) {
        Bundle extras = intent.getExtras();
        extras.remove("message_type");
        extras.remove("android.support.content.wakelockid");
        if (zza.m11585a(extras)) {
            if (!zza.m11589b((Context) this)) {
                zza.m11581a((Context) this).mo12296c(extras);
                return;
            }
            zza.m11588b(extras);
        }
        String string = extras.getString("from");
        extras.remove("from");
        m11564a(extras);
        mo10692a(string, extras);
    }

    /* renamed from: e */
    private String m11570e(Intent intent) {
        String stringExtra = intent.getStringExtra("google.message_id");
        return stringExtra == null ? intent.getStringExtra("message_id") : stringExtra;
    }

    /* renamed from: a */
    public void mo12285a() {
    }

    /* renamed from: a */
    public void mo12286a(String str) {
    }

    /* renamed from: a */
    public void mo10692a(String str, Bundle bundle) {
    }

    /* renamed from: a */
    public void mo12287a(String str, String str2) {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo12288a(int i) {
        return stopSelfResult(i);
    }

    public final IBinder onBind(Intent intent) {
        return null;
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.f8968a) {
            this.f8969b = i2;
            this.f8970c++;
        }
        if (intent == null) {
            m11566b();
            return 2;
        }
        m11563a(intent);
        return 3;
    }
}
