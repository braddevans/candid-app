package com.google.android.gms.maps;

import android.os.Parcel;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

public final class StreetViewPanoramaOptions extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final auz CREATOR = new auz();

    /* renamed from: a */
    private final int f9198a;

    /* renamed from: b */
    private StreetViewPanoramaCamera f9199b;

    /* renamed from: c */
    private String f9200c;

    /* renamed from: d */
    private LatLng f9201d;

    /* renamed from: e */
    private Integer f9202e;

    /* renamed from: f */
    private Boolean f9203f;

    /* renamed from: g */
    private Boolean f9204g;

    /* renamed from: h */
    private Boolean f9205h;

    /* renamed from: i */
    private Boolean f9206i;

    /* renamed from: j */
    private Boolean f9207j;

    public StreetViewPanoramaOptions() {
        this.f9203f = Boolean.valueOf(true);
        this.f9204g = Boolean.valueOf(true);
        this.f9205h = Boolean.valueOf(true);
        this.f9206i = Boolean.valueOf(true);
        this.f9198a = 1;
    }

    public StreetViewPanoramaOptions(int i, StreetViewPanoramaCamera streetViewPanoramaCamera, String str, LatLng latLng, Integer num, byte b, byte b2, byte b3, byte b4, byte b5) {
        this.f9203f = Boolean.valueOf(true);
        this.f9204g = Boolean.valueOf(true);
        this.f9205h = Boolean.valueOf(true);
        this.f9206i = Boolean.valueOf(true);
        this.f9198a = i;
        this.f9199b = streetViewPanoramaCamera;
        this.f9201d = latLng;
        this.f9202e = num;
        this.f9200c = str;
        this.f9203f = asm.m5248a(b);
        this.f9204g = asm.m5248a(b2);
        this.f9205h = asm.m5248a(b3);
        this.f9206i = asm.m5248a(b4);
        this.f9207j = asm.m5248a(b5);
    }

    /* renamed from: a */
    public int mo12576a() {
        return this.f9198a;
    }

    /* renamed from: b */
    public byte mo12577b() {
        return asm.m5247a(this.f9203f);
    }

    /* renamed from: c */
    public byte mo12578c() {
        return asm.m5247a(this.f9204g);
    }

    /* renamed from: d */
    public byte mo12579d() {
        return asm.m5247a(this.f9205h);
    }

    /* renamed from: e */
    public byte mo12580e() {
        return asm.m5247a(this.f9206i);
    }

    /* renamed from: f */
    public byte mo12581f() {
        return asm.m5247a(this.f9207j);
    }

    /* renamed from: g */
    public StreetViewPanoramaCamera mo12582g() {
        return this.f9199b;
    }

    /* renamed from: h */
    public LatLng mo12583h() {
        return this.f9201d;
    }

    /* renamed from: i */
    public Integer mo12584i() {
        return this.f9202e;
    }

    /* renamed from: j */
    public String mo12585j() {
        return this.f9200c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        auz.m5729a(this, parcel, i);
    }
}
