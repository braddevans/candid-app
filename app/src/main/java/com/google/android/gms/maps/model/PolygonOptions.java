package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.List;

public final class PolygonOptions extends AbstractSafeParcelable {
    public static final aup CREATOR = new aup();

    /* renamed from: a */
    private final int f9279a;

    /* renamed from: b */
    private final List<LatLng> f9280b;

    /* renamed from: c */
    private final List<List<LatLng>> f9281c;

    /* renamed from: d */
    private float f9282d;

    /* renamed from: e */
    private int f9283e;

    /* renamed from: f */
    private int f9284f;

    /* renamed from: g */
    private float f9285g;

    /* renamed from: h */
    private boolean f9286h;

    /* renamed from: i */
    private boolean f9287i;

    /* renamed from: j */
    private boolean f9288j;

    public PolygonOptions() {
        this.f9282d = 10.0f;
        this.f9283e = -16777216;
        this.f9284f = 0;
        this.f9285g = 0.0f;
        this.f9286h = true;
        this.f9287i = false;
        this.f9288j = false;
        this.f9279a = 1;
        this.f9280b = new ArrayList();
        this.f9281c = new ArrayList();
    }

    public PolygonOptions(int i, List<LatLng> list, List list2, float f, int i2, int i3, float f2, boolean z, boolean z2, boolean z3) {
        this.f9282d = 10.0f;
        this.f9283e = -16777216;
        this.f9284f = 0;
        this.f9285g = 0.0f;
        this.f9286h = true;
        this.f9287i = false;
        this.f9288j = false;
        this.f9279a = i;
        this.f9280b = list;
        this.f9281c = list2;
        this.f9282d = f;
        this.f9283e = i2;
        this.f9284f = i3;
        this.f9285g = f2;
        this.f9286h = z;
        this.f9287i = z2;
        this.f9288j = z3;
    }

    /* renamed from: a */
    public int mo12656a() {
        return this.f9279a;
    }

    /* renamed from: b */
    public List mo12657b() {
        return this.f9281c;
    }

    /* renamed from: c */
    public List<LatLng> mo12658c() {
        return this.f9280b;
    }

    /* renamed from: d */
    public float mo12659d() {
        return this.f9282d;
    }

    /* renamed from: e */
    public int mo12660e() {
        return this.f9283e;
    }

    /* renamed from: f */
    public int mo12661f() {
        return this.f9284f;
    }

    /* renamed from: g */
    public float mo12662g() {
        return this.f9285g;
    }

    /* renamed from: h */
    public boolean mo12663h() {
        return this.f9286h;
    }

    /* renamed from: i */
    public boolean mo12664i() {
        return this.f9287i;
    }

    /* renamed from: j */
    public boolean mo12665j() {
        return this.f9288j;
    }

    public void writeToParcel(Parcel parcel, int i) {
        aup.m5699a(this, parcel, i);
    }
}
