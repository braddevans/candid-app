package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class VisibleRegion extends AbstractSafeParcelable {
    public static final aux CREATOR = new aux();

    /* renamed from: a */
    public final LatLng f9327a;

    /* renamed from: b */
    public final LatLng f9328b;

    /* renamed from: c */
    public final LatLng f9329c;

    /* renamed from: d */
    public final LatLng f9330d;

    /* renamed from: e */
    public final LatLngBounds f9331e;

    /* renamed from: f */
    private final int f9332f;

    public VisibleRegion(int i, LatLng latLng, LatLng latLng2, LatLng latLng3, LatLng latLng4, LatLngBounds latLngBounds) {
        this.f9332f = i;
        this.f9327a = latLng;
        this.f9328b = latLng2;
        this.f9329c = latLng3;
        this.f9330d = latLng4;
        this.f9331e = latLngBounds;
    }

    /* renamed from: a */
    public int mo12708a() {
        return this.f9332f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VisibleRegion)) {
            return false;
        }
        VisibleRegion visibleRegion = (VisibleRegion) obj;
        return this.f9327a.equals(visibleRegion.f9327a) && this.f9328b.equals(visibleRegion.f9328b) && this.f9329c.equals(visibleRegion.f9329c) && this.f9330d.equals(visibleRegion.f9330d) && this.f9331e.equals(visibleRegion.f9331e);
    }

    public int hashCode() {
        return alv.m2719a(this.f9327a, this.f9328b, this.f9329c, this.f9330d, this.f9331e);
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("nearLeft", this.f9327a).mo1191a("nearRight", this.f9328b).mo1191a("farLeft", this.f9329c).mo1191a("farRight", this.f9330d).mo1191a("latLngBounds", this.f9331e).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aux.m5723a(this, parcel, i);
    }
}
