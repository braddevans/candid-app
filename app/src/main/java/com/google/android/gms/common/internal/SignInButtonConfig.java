package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class SignInButtonConfig extends AbstractSafeParcelable {
    public static final Creator<SignInButtonConfig> CREATOR = new alz();

    /* renamed from: a */
    public final int f8942a;

    /* renamed from: b */
    private final int f8943b;

    /* renamed from: c */
    private final int f8944c;
    @Deprecated

    /* renamed from: d */
    private final Scope[] f8945d;

    public SignInButtonConfig(int i, int i2, int i3, Scope[] scopeArr) {
        this.f8942a = i;
        this.f8943b = i2;
        this.f8944c = i3;
        this.f8945d = scopeArr;
    }

    public SignInButtonConfig(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, null);
    }

    /* renamed from: a */
    public int mo12259a() {
        return this.f8943b;
    }

    /* renamed from: b */
    public int mo12260b() {
        return this.f8944c;
    }

    @Deprecated
    /* renamed from: c */
    public Scope[] mo12261c() {
        return this.f8945d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        alz.m2740a(this, parcel, i);
    }
}
