package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.location.internal.ClientIdentity;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class ActivityTransitionRequest extends AbstractSafeParcelable {
    public static final arl CREATOR = new arl();

    /* renamed from: a */
    public static final Comparator<ActivityTransition> f9025a = new Comparator<ActivityTransition>() {
        /* renamed from: a */
        public int compare(ActivityTransition activityTransition, ActivityTransition activityTransition2) {
            int b = activityTransition.mo12372b();
            int b2 = activityTransition2.mo12372b();
            if (b != b2) {
                return b < b2 ? -1 : 1;
            }
            int c = activityTransition.mo12373c();
            int c2 = activityTransition2.mo12373c();
            if (c == c2) {
                return 0;
            }
            return c >= c2 ? 1 : -1;
        }
    };

    /* renamed from: b */
    private final int f9026b;

    /* renamed from: c */
    private final List<ActivityTransition> f9027c;

    /* renamed from: d */
    private final String f9028d;

    /* renamed from: e */
    private final List<ClientIdentity> f9029e;

    public ActivityTransitionRequest(int i, List<ActivityTransition> list, String str, List<ClientIdentity> list2) {
        alw.m2724a(list, (Object) "transitions can't be null");
        alw.m2732b(list.size() > 0, "transitions can't be empty.");
        m11692a(list);
        this.f9026b = i;
        this.f9027c = Collections.unmodifiableList(list);
        this.f9028d = str;
        this.f9029e = list2 == null ? Collections.emptyList() : Collections.unmodifiableList(list2);
    }

    /* renamed from: a */
    private static void m11692a(List<ActivityTransition> list) {
        TreeSet treeSet = new TreeSet(f9025a);
        for (ActivityTransition activityTransition : list) {
            alw.m2732b(treeSet.add(activityTransition), String.format("Found duplicated transition: %s.", new Object[]{activityTransition}));
        }
    }

    /* renamed from: a */
    public int mo12378a() {
        return this.f9026b;
    }

    /* renamed from: b */
    public List<ActivityTransition> mo12379b() {
        return this.f9027c;
    }

    /* renamed from: c */
    public String mo12380c() {
        return this.f9028d;
    }

    /* renamed from: d */
    public List<ClientIdentity> mo12381d() {
        return this.f9029e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ActivityTransitionRequest activityTransitionRequest = (ActivityTransitionRequest) obj;
        return alv.m2721a(this.f9027c, activityTransitionRequest.f9027c) && alv.m2721a(this.f9028d, activityTransitionRequest.f9028d) && alv.m2721a(this.f9029e, activityTransitionRequest.f9029e);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.f9028d != null ? this.f9028d.hashCode() : 0) + (this.f9027c.hashCode() * 31)) * 31;
        if (this.f9029e != null) {
            i = this.f9029e.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        String valueOf = String.valueOf(this.f9027c);
        String str = this.f9028d;
        String valueOf2 = String.valueOf(this.f9029e);
        return new StringBuilder(String.valueOf(valueOf).length() + 61 + String.valueOf(str).length() + String.valueOf(valueOf2).length()).append("ActivityTransitionRequest [mTransitions=").append(valueOf).append(", mTag='").append(str).append("'").append(", mClients=").append(valueOf2).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        arl.m4827a(this, parcel, i);
    }
}
