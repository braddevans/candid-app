package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

@TargetApi(11)
public final class zzrc extends Fragment implements apc {

    /* renamed from: a */
    private static WeakHashMap<Activity, WeakReference<zzrc>> f8997a = new WeakHashMap<>();

    /* renamed from: b */
    private Map<String, apb> f8998b = new ArrayMap();

    /* renamed from: c */
    private int f8999c = 0;

    /* renamed from: d */
    private Bundle f9000d;

    /* renamed from: a */
    public Activity mo6730a() {
        return getActivity();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (apb a : this.f8998b.values()) {
            a.mo6727a(str, fileDescriptor, printWriter, strArr);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (apb a : this.f8998b.values()) {
            a.mo6675a(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f8999c = 1;
        this.f9000d = bundle;
        for (Entry entry : this.f8998b.entrySet()) {
            ((apb) entry.getValue()).mo6676a(bundle != null ? bundle.getBundle((String) entry.getKey()) : null);
        }
    }

    public void onDestroy() {
        super.onStop();
        this.f8999c = 4;
        for (apb f : this.f8998b.values()) {
            f.mo6729f();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Entry entry : this.f8998b.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((apb) entry.getValue()).mo6679b(bundle2);
                bundle.putBundle((String) entry.getKey(), bundle2);
            }
        }
    }

    public void onStart() {
        super.onStop();
        this.f8999c = 2;
        for (apb a : this.f8998b.values()) {
            a.mo6674a();
        }
    }

    public void onStop() {
        super.onStop();
        this.f8999c = 3;
        for (apb b : this.f8998b.values()) {
            b.mo6678b();
        }
    }
}
