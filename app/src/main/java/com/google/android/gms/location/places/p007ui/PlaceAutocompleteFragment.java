package com.google.android.gms.location.places.p007ui;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import com.google.android.gms.C2137R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.maps.model.LatLngBounds;

@TargetApi(12)
/* renamed from: com.google.android.gms.location.places.ui.PlaceAutocompleteFragment */
public class PlaceAutocompleteFragment extends Fragment {

    /* renamed from: a */
    private View f9143a;

    /* renamed from: b */
    private View f9144b;

    /* renamed from: c */
    private EditText f9145c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f9146d;

    /* renamed from: e */
    private LatLngBounds f9147e;

    /* renamed from: f */
    private AutocompleteFilter f9148f;

    /* renamed from: g */
    private arg f9149g;

    /* renamed from: a */
    private void m11783a() {
        int i = 0;
        boolean z = !this.f9145c.getText().toString().isEmpty();
        View view = this.f9144b;
        if (!z) {
            i = 8;
        }
        view.setVisibility(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m11785b() {
        int i;
        try {
            Intent a = new C0772a(2).mo6973a(this.f9147e).mo6972a(this.f9148f).mo6974a(this.f9145c.getText().toString()).mo6971a(1).mo6970a(getActivity());
            this.f9146d = true;
            startActivityForResult(a, 30421);
            i = -1;
        } catch (GooglePlayServicesRepairableException e) {
            int a2 = e.mo12201a();
            Log.e("Places", "Could not open autocomplete activity", e);
            i = a2;
        } catch (GooglePlayServicesNotAvailableException e2) {
            int i2 = e2.f8879a;
            Log.e("Places", "Could not open autocomplete activity", e2);
            i = i2;
        }
        if (i != -1) {
            aky.m2599a().mo1130b(getActivity(), i, 30422);
        }
    }

    /* renamed from: a */
    public void mo12504a(CharSequence charSequence) {
        this.f9145c.setText(charSequence);
        m11783a();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        this.f9146d = false;
        if (i == 30421) {
            if (i2 == -1) {
                arc a = arf.m4806a(getActivity(), intent);
                if (this.f9149g != null) {
                    this.f9149g.mo6975a(a);
                }
                mo12504a((CharSequence) a.mo6961a().toString());
            } else if (i2 == 2) {
                Status b = arf.m4807b(getActivity(), intent);
                if (this.f9149g != null) {
                    this.f9149g.mo6976a(b);
                }
            }
        }
        super.onActivityResult(i, i2, intent);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(C2137R.layout.place_autocomplete_fragment, viewGroup, false);
        this.f9143a = inflate.findViewById(C2137R.C2139id.place_autocomplete_search_button);
        this.f9144b = inflate.findViewById(C2137R.C2139id.place_autocomplete_clear_button);
        this.f9145c = (EditText) inflate.findViewById(C2137R.C2139id.place_autocomplete_search_input);
        C21641 r0 = new OnClickListener() {
            public void onClick(View view) {
                if (!PlaceAutocompleteFragment.this.f9146d) {
                    PlaceAutocompleteFragment.this.m11785b();
                }
            }
        };
        this.f9143a.setOnClickListener(r0);
        this.f9145c.setOnClickListener(r0);
        this.f9144b.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PlaceAutocompleteFragment.this.mo12504a((CharSequence) "");
            }
        });
        m11783a();
        return inflate;
    }

    public void onDestroyView() {
        this.f9143a = null;
        this.f9144b = null;
        this.f9145c = null;
        super.onDestroyView();
    }
}
