package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class LocationRequestUpdateData extends AbstractSafeParcelable {
    public static final aqy CREATOR = new aqy();

    /* renamed from: a */
    public int f9092a;

    /* renamed from: b */
    public LocationRequestInternal f9093b;

    /* renamed from: c */
    arp f9094c;

    /* renamed from: d */
    public PendingIntent f9095d;

    /* renamed from: e */
    aro f9096e;

    /* renamed from: f */
    aqr f9097f;

    /* renamed from: g */
    private final int f9098g;

    public LocationRequestUpdateData(int i, int i2, LocationRequestInternal locationRequestInternal, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        aqr aqr = null;
        this.f9098g = i;
        this.f9092a = i2;
        this.f9093b = locationRequestInternal;
        this.f9094c = iBinder == null ? null : C0776a.m4842a(iBinder);
        this.f9095d = pendingIntent;
        this.f9096e = iBinder2 == null ? null : C0774a.m4838a(iBinder2);
        if (iBinder3 != null) {
            aqr = C0761a.m4696a(iBinder3);
        }
        this.f9097f = aqr;
    }

    /* renamed from: a */
    public static LocationRequestUpdateData m11738a(aro aro, aqr aqr) {
        return new LocationRequestUpdateData(1, 2, null, null, null, aro.asBinder(), aqr != null ? aqr.asBinder() : null);
    }

    /* renamed from: a */
    public static LocationRequestUpdateData m11739a(arp arp, aqr aqr) {
        return new LocationRequestUpdateData(1, 2, null, arp.asBinder(), null, null, aqr != null ? aqr.asBinder() : null);
    }

    /* renamed from: a */
    public int mo12453a() {
        return this.f9098g;
    }

    /* renamed from: b */
    public IBinder mo12454b() {
        if (this.f9094c == null) {
            return null;
        }
        return this.f9094c.asBinder();
    }

    /* renamed from: c */
    public IBinder mo12455c() {
        if (this.f9096e == null) {
            return null;
        }
        return this.f9096e.asBinder();
    }

    /* renamed from: d */
    public IBinder mo12456d() {
        if (this.f9097f == null) {
            return null;
        }
        return this.f9097f.asBinder();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqy.m4788a(this, parcel, i);
    }
}
