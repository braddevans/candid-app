package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class LatLng extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final aul CREATOR = new aul();

    /* renamed from: a */
    public final double f9251a;

    /* renamed from: b */
    public final double f9252b;

    /* renamed from: c */
    private final int f9253c;

    public LatLng(double d, double d2) {
        this(1, d, d2);
    }

    public LatLng(int i, double d, double d2) {
        this.f9253c = i;
        if (-180.0d > d2 || d2 >= 180.0d) {
            this.f9252b = ((((d2 - 180.0d) % 360.0d) + 360.0d) % 360.0d) - 180.0d;
        } else {
            this.f9252b = d2;
        }
        this.f9251a = Math.max(-90.0d, Math.min(90.0d, d));
    }

    /* renamed from: a */
    public int mo12625a() {
        return this.f9253c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLng)) {
            return false;
        }
        LatLng latLng = (LatLng) obj;
        return Double.doubleToLongBits(this.f9251a) == Double.doubleToLongBits(latLng.f9251a) && Double.doubleToLongBits(this.f9252b) == Double.doubleToLongBits(latLng.f9252b);
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.f9251a);
        int i = ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31;
        long doubleToLongBits2 = Double.doubleToLongBits(this.f9252b);
        return (i * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
    }

    public String toString() {
        double d = this.f9251a;
        return "lat/lng: (" + d + "," + this.f9252b + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        aul.m5687a(this, parcel, i);
    }
}
