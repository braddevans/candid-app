package com.google.android.gms.maps.model;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.util.AttributeSet;
import com.google.android.gms.C2137R;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class CameraPosition extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final auh CREATOR = new auh();

    /* renamed from: a */
    public final LatLng f9220a;

    /* renamed from: b */
    public final float f9221b;

    /* renamed from: c */
    public final float f9222c;

    /* renamed from: d */
    public final float f9223d;

    /* renamed from: e */
    private final int f9224e;

    /* renamed from: com.google.android.gms.maps.model.CameraPosition$a */
    public static final class C2178a {

        /* renamed from: a */
        private LatLng f9225a;

        /* renamed from: b */
        private float f9226b;

        /* renamed from: c */
        private float f9227c;

        /* renamed from: d */
        private float f9228d;

        /* renamed from: a */
        public C2178a mo12596a(float f) {
            this.f9226b = f;
            return this;
        }

        /* renamed from: a */
        public C2178a mo12597a(LatLng latLng) {
            this.f9225a = latLng;
            return this;
        }

        /* renamed from: a */
        public CameraPosition mo12598a() {
            return new CameraPosition(this.f9225a, this.f9226b, this.f9227c, this.f9228d);
        }

        /* renamed from: b */
        public C2178a mo12599b(float f) {
            this.f9227c = f;
            return this;
        }

        /* renamed from: c */
        public C2178a mo12600c(float f) {
            this.f9228d = f;
            return this;
        }
    }

    public CameraPosition(int i, LatLng latLng, float f, float f2, float f3) {
        alw.m2724a(latLng, (Object) "null camera target");
        alw.m2729a(0.0f <= f2 && f2 <= 90.0f, "Tilt needs to be between 0 and 90 inclusive: %s", Float.valueOf(f2));
        this.f9224e = i;
        this.f9220a = latLng;
        this.f9221b = f;
        this.f9222c = f2 + 0.0f;
        if (((double) f3) <= 0.0d) {
            f3 = (f3 % 360.0f) + 360.0f;
        }
        this.f9223d = f3 % 360.0f;
    }

    public CameraPosition(LatLng latLng, float f, float f2, float f3) {
        this(1, latLng, f, f2, f3);
    }

    /* renamed from: a */
    public static CameraPosition m11898a(Context context, AttributeSet attributeSet) {
        if (attributeSet == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, C2137R.styleable.MapAttrs);
        LatLng latLng = new LatLng((double) (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_cameraTargetLat) ? obtainAttributes.getFloat(C2137R.styleable.MapAttrs_cameraTargetLat, 0.0f) : 0.0f), (double) (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_cameraTargetLng) ? obtainAttributes.getFloat(C2137R.styleable.MapAttrs_cameraTargetLng, 0.0f) : 0.0f));
        C2178a b = m11899b();
        b.mo12597a(latLng);
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_cameraZoom)) {
            b.mo12596a(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_cameraZoom, 0.0f));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_cameraBearing)) {
            b.mo12600c(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_cameraBearing, 0.0f));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_cameraTilt)) {
            b.mo12599b(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_cameraTilt, 0.0f));
        }
        return b.mo12598a();
    }

    /* renamed from: b */
    public static C2178a m11899b() {
        return new C2178a();
    }

    /* renamed from: a */
    public int mo12591a() {
        return this.f9224e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CameraPosition)) {
            return false;
        }
        CameraPosition cameraPosition = (CameraPosition) obj;
        return this.f9220a.equals(cameraPosition.f9220a) && Float.floatToIntBits(this.f9221b) == Float.floatToIntBits(cameraPosition.f9221b) && Float.floatToIntBits(this.f9222c) == Float.floatToIntBits(cameraPosition.f9222c) && Float.floatToIntBits(this.f9223d) == Float.floatToIntBits(cameraPosition.f9223d);
    }

    public int hashCode() {
        return alv.m2719a(this.f9220a, Float.valueOf(this.f9221b), Float.valueOf(this.f9222c), Float.valueOf(this.f9223d));
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("target", this.f9220a).mo1191a("zoom", Float.valueOf(this.f9221b)).mo1191a("tilt", Float.valueOf(this.f9222c)).mo1191a("bearing", Float.valueOf(this.f9223d)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        auh.m5675a(this, parcel, i);
    }
}
