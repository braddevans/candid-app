package com.google.android.gms.common.internal.safeparcel;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class zza {

    /* renamed from: com.google.android.gms.common.internal.safeparcel.zza$zza reason: collision with other inner class name */
    public static class C3143zza extends RuntimeException {
        public C3143zza(String str, Parcel parcel) {
            int dataPosition = parcel.dataPosition();
            super(new StringBuilder(String.valueOf(str).length() + 41).append(str).append(" Parcel: pos=").append(dataPosition).append(" size=").append(parcel.dataSize()).toString());
        }
    }

    /* renamed from: a */
    public static int m11509a(int i) {
        return 65535 & i;
    }

    /* renamed from: a */
    public static int m11510a(Parcel parcel) {
        return parcel.readInt();
    }

    /* renamed from: a */
    public static int m11511a(Parcel parcel, int i) {
        return (i & -65536) != -65536 ? (i >> 16) & 65535 : parcel.readInt();
    }

    /* renamed from: a */
    public static <T extends Parcelable> T m11512a(Parcel parcel, int i, Creator<T> creator) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        T t = (Parcelable) creator.createFromParcel(parcel);
        parcel.setDataPosition(a + dataPosition);
        return t;
    }

    /* renamed from: a */
    private static void m11513a(Parcel parcel, int i, int i2) {
        int a = m11511a(parcel, i);
        if (a != i2) {
            String valueOf = String.valueOf(Integer.toHexString(a));
            throw new C3143zza(new StringBuilder(String.valueOf(valueOf).length() + 46).append("Expected size ").append(i2).append(" got ").append(a).append(" (0x").append(valueOf).append(")").toString(), parcel);
        }
    }

    /* renamed from: a */
    private static void m11514a(Parcel parcel, int i, int i2, int i3) {
        if (i2 != i3) {
            String valueOf = String.valueOf(Integer.toHexString(i2));
            throw new C3143zza(new StringBuilder(String.valueOf(valueOf).length() + 46).append("Expected size ").append(i3).append(" got ").append(i2).append(" (0x").append(valueOf).append(")").toString(), parcel);
        }
    }

    /* renamed from: a */
    public static void m11515a(Parcel parcel, int i, List list, ClassLoader classLoader) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a != 0) {
            parcel.readList(list, classLoader);
            parcel.setDataPosition(a + dataPosition);
        }
    }

    /* renamed from: b */
    public static int m11516b(Parcel parcel) {
        int a = m11510a(parcel);
        int a2 = m11511a(parcel, a);
        int dataPosition = parcel.dataPosition();
        if (m11509a(a) != 20293) {
            String str = "Expected object header. Got 0x";
            String valueOf = String.valueOf(Integer.toHexString(a));
            throw new C3143zza(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), parcel);
        }
        int i = dataPosition + a2;
        if (i >= dataPosition && i <= parcel.dataSize()) {
            return i;
        }
        throw new C3143zza("Size read is invalid start=" + dataPosition + " end=" + i, parcel);
    }

    /* renamed from: b */
    public static void m11517b(Parcel parcel, int i) {
        parcel.setDataPosition(m11511a(parcel, i) + parcel.dataPosition());
    }

    /* renamed from: b */
    public static <T> T[] m11518b(Parcel parcel, int i, Creator<T> creator) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        T[] createTypedArray = parcel.createTypedArray(creator);
        parcel.setDataPosition(a + dataPosition);
        return createTypedArray;
    }

    /* renamed from: c */
    public static <T> ArrayList<T> m11519c(Parcel parcel, int i, Creator<T> creator) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        ArrayList<T> createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(a + dataPosition);
        return createTypedArrayList;
    }

    /* renamed from: c */
    public static boolean m11520c(Parcel parcel, int i) {
        m11513a(parcel, i, 4);
        return parcel.readInt() != 0;
    }

    /* renamed from: d */
    public static byte m11521d(Parcel parcel, int i) {
        m11513a(parcel, i, 4);
        return (byte) parcel.readInt();
    }

    /* renamed from: e */
    public static short m11522e(Parcel parcel, int i) {
        m11513a(parcel, i, 4);
        return (short) parcel.readInt();
    }

    /* renamed from: f */
    public static int m11523f(Parcel parcel, int i) {
        m11513a(parcel, i, 4);
        return parcel.readInt();
    }

    /* renamed from: g */
    public static Integer m11524g(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        if (a == 0) {
            return null;
        }
        m11514a(parcel, i, a, 4);
        return Integer.valueOf(parcel.readInt());
    }

    /* renamed from: h */
    public static long m11525h(Parcel parcel, int i) {
        m11513a(parcel, i, 8);
        return parcel.readLong();
    }

    /* renamed from: i */
    public static Long m11526i(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        if (a == 0) {
            return null;
        }
        m11514a(parcel, i, a, 8);
        return Long.valueOf(parcel.readLong());
    }

    /* renamed from: j */
    public static float m11527j(Parcel parcel, int i) {
        m11513a(parcel, i, 4);
        return parcel.readFloat();
    }

    /* renamed from: k */
    public static Float m11528k(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        if (a == 0) {
            return null;
        }
        m11514a(parcel, i, a, 4);
        return Float.valueOf(parcel.readFloat());
    }

    /* renamed from: l */
    public static double m11529l(Parcel parcel, int i) {
        m11513a(parcel, i, 8);
        return parcel.readDouble();
    }

    /* renamed from: m */
    public static Double m11530m(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        if (a == 0) {
            return null;
        }
        m11514a(parcel, i, a, 8);
        return Double.valueOf(parcel.readDouble());
    }

    /* renamed from: n */
    public static String m11531n(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(a + dataPosition);
        return readString;
    }

    /* renamed from: o */
    public static IBinder m11532o(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(a + dataPosition);
        return readStrongBinder;
    }

    /* renamed from: p */
    public static Bundle m11533p(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(a + dataPosition);
        return readBundle;
    }

    /* renamed from: q */
    public static byte[] m11534q(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(a + dataPosition);
        return createByteArray;
    }

    /* renamed from: r */
    public static int[] m11535r(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(a + dataPosition);
        return createIntArray;
    }

    /* renamed from: s */
    public static String[] m11536s(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(a + dataPosition);
        return createStringArray;
    }

    /* renamed from: t */
    public static ArrayList<Integer> m11537t(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        int readInt = parcel.readInt();
        for (int i2 = 0; i2 < readInt; i2++) {
            arrayList.add(Integer.valueOf(parcel.readInt()));
        }
        parcel.setDataPosition(dataPosition + a);
        return arrayList;
    }

    /* renamed from: u */
    public static ArrayList<String> m11538u(Parcel parcel, int i) {
        int a = m11511a(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (a == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(a + dataPosition);
        return createStringArrayList;
    }
}
