package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class CircleOptions extends AbstractSafeParcelable {
    public static final aui CREATOR = new aui();

    /* renamed from: a */
    private final int f9229a;

    /* renamed from: b */
    private LatLng f9230b;

    /* renamed from: c */
    private double f9231c;

    /* renamed from: d */
    private float f9232d;

    /* renamed from: e */
    private int f9233e;

    /* renamed from: f */
    private int f9234f;

    /* renamed from: g */
    private float f9235g;

    /* renamed from: h */
    private boolean f9236h;

    /* renamed from: i */
    private boolean f9237i;

    public CircleOptions() {
        this.f9230b = null;
        this.f9231c = 0.0d;
        this.f9232d = 10.0f;
        this.f9233e = -16777216;
        this.f9234f = 0;
        this.f9235g = 0.0f;
        this.f9236h = true;
        this.f9237i = false;
        this.f9229a = 1;
    }

    public CircleOptions(int i, LatLng latLng, double d, float f, int i2, int i3, float f2, boolean z, boolean z2) {
        this.f9230b = null;
        this.f9231c = 0.0d;
        this.f9232d = 10.0f;
        this.f9233e = -16777216;
        this.f9234f = 0;
        this.f9235g = 0.0f;
        this.f9236h = true;
        this.f9237i = false;
        this.f9229a = i;
        this.f9230b = latLng;
        this.f9231c = d;
        this.f9232d = f;
        this.f9233e = i2;
        this.f9234f = i3;
        this.f9235g = f2;
        this.f9236h = z;
        this.f9237i = z2;
    }

    /* renamed from: a */
    public int mo12601a() {
        return this.f9229a;
    }

    /* renamed from: b */
    public LatLng mo12602b() {
        return this.f9230b;
    }

    /* renamed from: c */
    public double mo12603c() {
        return this.f9231c;
    }

    /* renamed from: d */
    public float mo12604d() {
        return this.f9232d;
    }

    /* renamed from: e */
    public int mo12605e() {
        return this.f9233e;
    }

    /* renamed from: f */
    public int mo12606f() {
        return this.f9234f;
    }

    /* renamed from: g */
    public float mo12607g() {
        return this.f9235g;
    }

    /* renamed from: h */
    public boolean mo12608h() {
        return this.f9236h;
    }

    /* renamed from: i */
    public boolean mo12609i() {
        return this.f9237i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        aui.m5678a(this, parcel, i);
    }
}
