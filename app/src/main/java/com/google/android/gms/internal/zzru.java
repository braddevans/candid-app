package com.google.android.gms.internal;

import android.graphics.Canvas;
import android.widget.ImageView;

public final class zzru extends ImageView {
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }
}
