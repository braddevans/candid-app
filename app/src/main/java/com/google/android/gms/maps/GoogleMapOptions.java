package com.google.android.gms.maps;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.util.AttributeSet;
import com.google.android.gms.C2137R;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;

public final class GoogleMapOptions extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final auy CREATOR = new auy();

    /* renamed from: a */
    private final int f9152a;

    /* renamed from: b */
    private Boolean f9153b;

    /* renamed from: c */
    private Boolean f9154c;

    /* renamed from: d */
    private int f9155d;

    /* renamed from: e */
    private CameraPosition f9156e;

    /* renamed from: f */
    private Boolean f9157f;

    /* renamed from: g */
    private Boolean f9158g;

    /* renamed from: h */
    private Boolean f9159h;

    /* renamed from: i */
    private Boolean f9160i;

    /* renamed from: j */
    private Boolean f9161j;

    /* renamed from: k */
    private Boolean f9162k;

    /* renamed from: l */
    private Boolean f9163l;

    /* renamed from: m */
    private Boolean f9164m;

    /* renamed from: n */
    private Boolean f9165n;

    /* renamed from: o */
    private Float f9166o;

    /* renamed from: p */
    private Float f9167p;

    /* renamed from: q */
    private LatLngBounds f9168q;

    public GoogleMapOptions() {
        this.f9155d = -1;
        this.f9166o = null;
        this.f9167p = null;
        this.f9168q = null;
        this.f9152a = 1;
    }

    public GoogleMapOptions(int i, byte b, byte b2, int i2, CameraPosition cameraPosition, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8, byte b9, byte b10, byte b11, Float f, Float f2, LatLngBounds latLngBounds) {
        this.f9155d = -1;
        this.f9166o = null;
        this.f9167p = null;
        this.f9168q = null;
        this.f9152a = i;
        this.f9153b = asm.m5248a(b);
        this.f9154c = asm.m5248a(b2);
        this.f9155d = i2;
        this.f9156e = cameraPosition;
        this.f9157f = asm.m5248a(b3);
        this.f9158g = asm.m5248a(b4);
        this.f9159h = asm.m5248a(b5);
        this.f9160i = asm.m5248a(b6);
        this.f9161j = asm.m5248a(b7);
        this.f9162k = asm.m5248a(b8);
        this.f9163l = asm.m5248a(b9);
        this.f9164m = asm.m5248a(b10);
        this.f9165n = asm.m5248a(b11);
        this.f9166o = f;
        this.f9167p = f2;
        this.f9168q = latLngBounds;
    }

    /* renamed from: a */
    public static GoogleMapOptions m11788a(Context context, AttributeSet attributeSet) {
        if (attributeSet == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, C2137R.styleable.MapAttrs);
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_mapType)) {
            googleMapOptions.mo12512a(obtainAttributes.getInt(C2137R.styleable.MapAttrs_mapType, -1));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_zOrderOnTop)) {
            googleMapOptions.mo12515a(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_zOrderOnTop, false));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_useViewLifecycle)) {
            googleMapOptions.mo12518b(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_useViewLifecycle, false));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_uiCompass)) {
            googleMapOptions.mo12522d(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_uiCompass, true));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_uiRotateGestures)) {
            googleMapOptions.mo12530h(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_uiRotateGestures, true));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_uiScrollGestures)) {
            googleMapOptions.mo12524e(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_uiScrollGestures, true));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_uiTiltGestures)) {
            googleMapOptions.mo12528g(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_uiTiltGestures, true));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_uiZoomGestures)) {
            googleMapOptions.mo12526f(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_uiZoomGestures, true));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_uiZoomControls)) {
            googleMapOptions.mo12520c(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_uiZoomControls, true));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_liteMode)) {
            googleMapOptions.mo12532i(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_liteMode, false));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_uiMapToolbar)) {
            googleMapOptions.mo12534j(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_uiMapToolbar, true));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_ambientEnabled)) {
            googleMapOptions.mo12536k(obtainAttributes.getBoolean(C2137R.styleable.MapAttrs_ambientEnabled, false));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_cameraMinZoomPreference)) {
            googleMapOptions.mo12511a(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_cameraMinZoomPreference, Float.NEGATIVE_INFINITY));
        }
        if (obtainAttributes.hasValue(C2137R.styleable.MapAttrs_cameraMinZoomPreference)) {
            googleMapOptions.mo12517b(obtainAttributes.getFloat(C2137R.styleable.MapAttrs_cameraMaxZoomPreference, Float.POSITIVE_INFINITY));
        }
        googleMapOptions.mo12514a(LatLngBounds.m11929a(context, attributeSet));
        googleMapOptions.mo12513a(CameraPosition.m11898a(context, attributeSet));
        obtainAttributes.recycle();
        return googleMapOptions;
    }

    /* renamed from: a */
    public int mo12510a() {
        return this.f9152a;
    }

    /* renamed from: a */
    public GoogleMapOptions mo12511a(float f) {
        this.f9166o = Float.valueOf(f);
        return this;
    }

    /* renamed from: a */
    public GoogleMapOptions mo12512a(int i) {
        this.f9155d = i;
        return this;
    }

    /* renamed from: a */
    public GoogleMapOptions mo12513a(CameraPosition cameraPosition) {
        this.f9156e = cameraPosition;
        return this;
    }

    /* renamed from: a */
    public GoogleMapOptions mo12514a(LatLngBounds latLngBounds) {
        this.f9168q = latLngBounds;
        return this;
    }

    /* renamed from: a */
    public GoogleMapOptions mo12515a(boolean z) {
        this.f9153b = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: b */
    public byte mo12516b() {
        return asm.m5247a(this.f9153b);
    }

    /* renamed from: b */
    public GoogleMapOptions mo12517b(float f) {
        this.f9167p = Float.valueOf(f);
        return this;
    }

    /* renamed from: b */
    public GoogleMapOptions mo12518b(boolean z) {
        this.f9154c = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: c */
    public byte mo12519c() {
        return asm.m5247a(this.f9154c);
    }

    /* renamed from: c */
    public GoogleMapOptions mo12520c(boolean z) {
        this.f9157f = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: d */
    public byte mo12521d() {
        return asm.m5247a(this.f9157f);
    }

    /* renamed from: d */
    public GoogleMapOptions mo12522d(boolean z) {
        this.f9158g = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: e */
    public byte mo12523e() {
        return asm.m5247a(this.f9158g);
    }

    /* renamed from: e */
    public GoogleMapOptions mo12524e(boolean z) {
        this.f9159h = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: f */
    public byte mo12525f() {
        return asm.m5247a(this.f9159h);
    }

    /* renamed from: f */
    public GoogleMapOptions mo12526f(boolean z) {
        this.f9160i = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: g */
    public byte mo12527g() {
        return asm.m5247a(this.f9160i);
    }

    /* renamed from: g */
    public GoogleMapOptions mo12528g(boolean z) {
        this.f9161j = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: h */
    public byte mo12529h() {
        return asm.m5247a(this.f9161j);
    }

    /* renamed from: h */
    public GoogleMapOptions mo12530h(boolean z) {
        this.f9162k = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: i */
    public byte mo12531i() {
        return asm.m5247a(this.f9162k);
    }

    /* renamed from: i */
    public GoogleMapOptions mo12532i(boolean z) {
        this.f9163l = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: j */
    public byte mo12533j() {
        return asm.m5247a(this.f9163l);
    }

    /* renamed from: j */
    public GoogleMapOptions mo12534j(boolean z) {
        this.f9164m = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: k */
    public byte mo12535k() {
        return asm.m5247a(this.f9164m);
    }

    /* renamed from: k */
    public GoogleMapOptions mo12536k(boolean z) {
        this.f9165n = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: l */
    public byte mo12537l() {
        return asm.m5247a(this.f9165n);
    }

    /* renamed from: m */
    public int mo12538m() {
        return this.f9155d;
    }

    /* renamed from: n */
    public CameraPosition mo12539n() {
        return this.f9156e;
    }

    /* renamed from: o */
    public Float mo12540o() {
        return this.f9166o;
    }

    /* renamed from: p */
    public Float mo12541p() {
        return this.f9167p;
    }

    /* renamed from: q */
    public LatLngBounds mo12542q() {
        return this.f9168q;
    }

    public void writeToParcel(Parcel parcel, int i) {
        auy.m5726a(this, parcel, i);
    }
}
