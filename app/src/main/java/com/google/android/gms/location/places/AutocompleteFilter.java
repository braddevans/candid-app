package com.google.android.gms.location.places;

import android.os.Parcel;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Collection;
import java.util.List;

public class AutocompleteFilter extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final ari CREATOR = new ari();

    /* renamed from: a */
    public final int f9109a;

    /* renamed from: b */
    public final boolean f9110b;

    /* renamed from: c */
    public final List<Integer> f9111c;

    /* renamed from: d */
    public final String f9112d;

    /* renamed from: e */
    final int f9113e;

    public AutocompleteFilter(int i, boolean z, List<Integer> list, String str) {
        boolean z2 = true;
        this.f9109a = i;
        this.f9111c = list;
        this.f9113e = m11760a(list);
        this.f9112d = str;
        if (this.f9109a < 1) {
            if (z) {
                z2 = false;
            }
            this.f9110b = z2;
            return;
        }
        this.f9110b = z;
    }

    /* renamed from: a */
    private static int m11760a(Collection<Integer> collection) {
        if (collection == null || collection.isEmpty()) {
            return 0;
        }
        return ((Integer) collection.iterator().next()).intValue();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AutocompleteFilter)) {
            return false;
        }
        AutocompleteFilter autocompleteFilter = (AutocompleteFilter) obj;
        return this.f9113e == autocompleteFilter.f9113e && this.f9110b == autocompleteFilter.f9110b && this.f9112d == autocompleteFilter.f9112d;
    }

    public int hashCode() {
        return alv.m2719a(Boolean.valueOf(this.f9110b), Integer.valueOf(this.f9113e), this.f9112d);
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("includeQueryPredictions", Boolean.valueOf(this.f9110b)).mo1191a("typeFilter", Integer.valueOf(this.f9113e)).mo1191a("country", this.f9112d).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        ari.m4818a(this, parcel, i);
    }
}
