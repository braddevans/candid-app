package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class Tile extends AbstractSafeParcelable {
    public static final auv CREATOR = new auv();

    /* renamed from: a */
    public final int f9314a;

    /* renamed from: b */
    public final int f9315b;

    /* renamed from: c */
    public final byte[] f9316c;

    /* renamed from: d */
    private final int f9317d;

    public Tile(int i, int i2, int i3, byte[] bArr) {
        this.f9317d = i;
        this.f9314a = i2;
        this.f9315b = i3;
        this.f9316c = bArr;
    }

    public Tile(int i, int i2, byte[] bArr) {
        this(1, i, i2, bArr);
    }

    /* renamed from: a */
    public int mo12699a() {
        return this.f9317d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        auv.m5717a(this, parcel, i);
    }
}
