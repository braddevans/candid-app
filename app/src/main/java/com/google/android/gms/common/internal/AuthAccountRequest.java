package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class AuthAccountRequest extends AbstractSafeParcelable {
    public static final Creator<AuthAccountRequest> CREATOR = new ame();

    /* renamed from: a */
    public final int f8919a;

    /* renamed from: b */
    public final IBinder f8920b;

    /* renamed from: c */
    public final Scope[] f8921c;

    /* renamed from: d */
    public Integer f8922d;

    /* renamed from: e */
    public Integer f8923e;

    public AuthAccountRequest(int i, IBinder iBinder, Scope[] scopeArr, Integer num, Integer num2) {
        this.f8919a = i;
        this.f8920b = iBinder;
        this.f8921c = scopeArr;
        this.f8922d = num;
        this.f8923e = num2;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ame.m2761a(this, parcel, i);
    }
}
