package com.google.android.gms.location.internal;

import android.annotation.SuppressLint;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Locale;

public class ParcelableGeofence extends AbstractSafeParcelable implements aqe {
    public static final aqz CREATOR = new aqz();

    /* renamed from: a */
    private final int f9099a;

    /* renamed from: b */
    private final String f9100b;

    /* renamed from: c */
    private final long f9101c;

    /* renamed from: d */
    private final short f9102d;

    /* renamed from: e */
    private final double f9103e;

    /* renamed from: f */
    private final double f9104f;

    /* renamed from: g */
    private final float f9105g;

    /* renamed from: h */
    private final int f9106h;

    /* renamed from: i */
    private final int f9107i;

    /* renamed from: j */
    private final int f9108j;

    public ParcelableGeofence(int i, String str, int i2, short s, double d, double d2, float f, long j, int i3, int i4) {
        m11748a(str);
        m11747a(f);
        m11746a(d, d2);
        int a = m11744a(i2);
        this.f9099a = i;
        this.f9102d = s;
        this.f9100b = str;
        this.f9103e = d;
        this.f9104f = d2;
        this.f9105g = f;
        this.f9101c = j;
        this.f9106h = a;
        this.f9107i = i3;
        this.f9108j = i4;
    }

    /* renamed from: a */
    private static int m11744a(int i) {
        int i2 = i & 7;
        if (i2 != 0) {
            return i2;
        }
        throw new IllegalArgumentException("No supported transition specified: " + i);
    }

    /* renamed from: a */
    public static ParcelableGeofence m11745a(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        ParcelableGeofence parcelableGeofence = (ParcelableGeofence) CREATOR.createFromParcel(obtain);
        obtain.recycle();
        return parcelableGeofence;
    }

    /* renamed from: a */
    private static void m11746a(double d, double d2) {
        if (d > 90.0d || d < -90.0d) {
            throw new IllegalArgumentException("invalid latitude: " + d);
        } else if (d2 > 180.0d || d2 < -180.0d) {
            throw new IllegalArgumentException("invalid longitude: " + d2);
        }
    }

    /* renamed from: a */
    private static void m11747a(float f) {
        if (f <= 0.0f) {
            throw new IllegalArgumentException("invalid radius: " + f);
        }
    }

    /* renamed from: a */
    private static void m11748a(String str) {
        if (str == null || str.length() > 100) {
            String str2 = "requestId is null or too long: ";
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
    }

    @SuppressLint({"DefaultLocale"})
    /* renamed from: b */
    private static String m11749b(int i) {
        switch (i) {
            case 1:
                return "CIRCLE";
            default:
                return null;
        }
    }

    /* renamed from: a */
    public String mo6862a() {
        return this.f9100b;
    }

    /* renamed from: b */
    public int mo12458b() {
        return this.f9099a;
    }

    /* renamed from: c */
    public short mo12459c() {
        return this.f9102d;
    }

    /* renamed from: d */
    public double mo12460d() {
        return this.f9103e;
    }

    /* renamed from: e */
    public double mo12461e() {
        return this.f9104f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ParcelableGeofence)) {
            return false;
        }
        ParcelableGeofence parcelableGeofence = (ParcelableGeofence) obj;
        if (this.f9105g != parcelableGeofence.f9105g) {
            return false;
        }
        if (this.f9103e != parcelableGeofence.f9103e) {
            return false;
        }
        if (this.f9104f != parcelableGeofence.f9104f) {
            return false;
        }
        return this.f9102d == parcelableGeofence.f9102d;
    }

    /* renamed from: f */
    public float mo12463f() {
        return this.f9105g;
    }

    /* renamed from: g */
    public long mo12464g() {
        return this.f9101c;
    }

    /* renamed from: h */
    public int mo12465h() {
        return this.f9106h;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.f9103e);
        int i = ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31;
        long doubleToLongBits2 = Double.doubleToLongBits(this.f9104f);
        return (((((((i * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + Float.floatToIntBits(this.f9105g)) * 31) + this.f9102d) * 31) + this.f9106h;
    }

    /* renamed from: i */
    public int mo12467i() {
        return this.f9107i;
    }

    /* renamed from: j */
    public int mo12468j() {
        return this.f9108j;
    }

    public String toString() {
        return String.format(Locale.US, "Geofence[%s id:%s transitions:%d %.6f, %.6f %.0fm, resp=%ds, dwell=%dms, @%d]", new Object[]{m11749b(this.f9102d), this.f9100b, Integer.valueOf(this.f9106h), Double.valueOf(this.f9103e), Double.valueOf(this.f9104f), Float.valueOf(this.f9105g), Integer.valueOf(this.f9107i / 1000), Integer.valueOf(this.f9108j), Long.valueOf(this.f9101c)});
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqz aqz = CREATOR;
        aqz.m4791a(this, parcel, i);
    }
}
