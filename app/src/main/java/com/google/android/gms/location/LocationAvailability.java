package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class LocationAvailability extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final aqh CREATOR = new aqh();

    /* renamed from: a */
    public int f9047a;

    /* renamed from: b */
    public int f9048b;

    /* renamed from: c */
    public long f9049c;

    /* renamed from: d */
    public int f9050d;

    /* renamed from: e */
    private final int f9051e;

    public LocationAvailability(int i, int i2, int i3, int i4, long j) {
        this.f9051e = i;
        this.f9050d = i2;
        this.f9047a = i3;
        this.f9048b = i4;
        this.f9049c = j;
    }

    /* renamed from: a */
    public boolean mo12408a() {
        return this.f9050d < 1000;
    }

    /* renamed from: b */
    public int mo12409b() {
        return this.f9051e;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof LocationAvailability)) {
            return false;
        }
        LocationAvailability locationAvailability = (LocationAvailability) obj;
        return this.f9050d == locationAvailability.f9050d && this.f9047a == locationAvailability.f9047a && this.f9048b == locationAvailability.f9048b && this.f9049c == locationAvailability.f9049c;
    }

    public int hashCode() {
        return alv.m2719a(Integer.valueOf(this.f9050d), Integer.valueOf(this.f9047a), Integer.valueOf(this.f9048b), Long.valueOf(this.f9049c));
    }

    public String toString() {
        return "LocationAvailability[isLocationAvailable: " + mo12408a() + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqh.m4664a(this, parcel, i);
    }
}
