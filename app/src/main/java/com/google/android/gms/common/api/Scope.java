package com.google.android.gms.common.api;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class Scope extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Creator<Scope> CREATOR = new alo();

    /* renamed from: a */
    public final int f8888a;

    /* renamed from: b */
    private final String f8889b;

    public Scope(int i, String str) {
        alw.m2726a(str, (Object) "scopeUri must not be null or empty");
        this.f8888a = i;
        this.f8889b = str;
    }

    public Scope(String str) {
        this(1, str);
    }

    /* renamed from: a */
    public String mo12216a() {
        return this.f8889b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Scope)) {
            return false;
        }
        return this.f8889b.equals(((Scope) obj).f8889b);
    }

    public int hashCode() {
        return this.f8889b.hashCode();
    }

    public String toString() {
        return this.f8889b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        alo.m2673a(this, parcel, i);
    }
}
