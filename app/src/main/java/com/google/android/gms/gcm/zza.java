package com.google.android.gms.gcm;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.os.SystemClock;
import android.support.p001v4.app.NotificationCompat.Builder;
import android.text.TextUtils;
import android.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.MissingFormatArgumentException;
import org.json.JSONArray;
import org.json.JSONException;

class zza {

    /* renamed from: a */
    static zza f8977a;

    /* renamed from: b */
    private final Context f8978b;

    /* renamed from: com.google.android.gms.gcm.zza$zza reason: collision with other inner class name */
    class C3144zza extends IllegalArgumentException {
    }

    private zza(Context context) {
        this.f8978b = context.getApplicationContext();
    }

    /* renamed from: a */
    private int m11580a() {
        return (int) SystemClock.uptimeMillis();
    }

    /* renamed from: a */
    static synchronized zza m11581a(Context context) {
        zza zza;
        synchronized (zza.class) {
            if (f8977a == null) {
                f8977a = new zza(context);
            }
            zza = f8977a;
        }
        return zza;
    }

    /* renamed from: a */
    static String m11582a(Bundle bundle, String str) {
        String string = bundle.getString(str);
        return string == null ? bundle.getString(str.replace("gcm.n.", "gcm.notification.")) : string;
    }

    /* renamed from: a */
    private String m11583a(String str) {
        return str.substring("gcm.n.".length());
    }

    /* renamed from: a */
    private void m11584a(String str, Notification notification) {
        if (Log.isLoggable("GcmNotification", 3)) {
            Log.d("GcmNotification", "Showing notification");
        }
        NotificationManager notificationManager = (NotificationManager) this.f8978b.getSystemService("notification");
        if (TextUtils.isEmpty(str)) {
            str = "GCM-Notification:" + SystemClock.uptimeMillis();
        }
        notificationManager.notify(str, 0, notification);
    }

    /* renamed from: a */
    static boolean m11585a(Bundle bundle) {
        return "1".equals(m11582a(bundle, "gcm.n.e")) || m11582a(bundle, "gcm.n.icon") != null;
    }

    /* renamed from: b */
    private int m11586b(String str) {
        if (!TextUtils.isEmpty(str)) {
            Resources resources = this.f8978b.getResources();
            int identifier = resources.getIdentifier(str, "drawable", this.f8978b.getPackageName());
            if (identifier != 0) {
                return identifier;
            }
            int identifier2 = resources.getIdentifier(str, "mipmap", this.f8978b.getPackageName());
            if (identifier2 != 0) {
                return identifier2;
            }
            Log.w("GcmNotification", new StringBuilder(String.valueOf(str).length() + 57).append("Icon resource ").append(str).append(" not found. Notification will use app icon.").toString());
        }
        int i = this.f8978b.getApplicationInfo().icon;
        if (i == 0) {
            return 17301651;
        }
        return i;
    }

    /* renamed from: b */
    private String m11587b(Bundle bundle, String str) {
        String a = m11582a(bundle, str);
        if (!TextUtils.isEmpty(a)) {
            return a;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf("_loc_key");
        String a2 = m11582a(bundle, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        Resources resources = this.f8978b.getResources();
        int identifier = resources.getIdentifier(a2, "string", this.f8978b.getPackageName());
        if (identifier == 0) {
            String str2 = "GcmNotification";
            String valueOf3 = String.valueOf(str);
            String valueOf4 = String.valueOf("_loc_key");
            String valueOf5 = String.valueOf(m11583a(valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3)));
            Log.w(str2, new StringBuilder(String.valueOf(valueOf5).length() + 49 + String.valueOf(a2).length()).append(valueOf5).append(" resource not found: ").append(a2).append(" Default value will be used.").toString());
            return null;
        }
        String valueOf6 = String.valueOf(str);
        String valueOf7 = String.valueOf("_loc_args");
        String a3 = m11582a(bundle, valueOf7.length() != 0 ? valueOf6.concat(valueOf7) : new String(valueOf6));
        if (TextUtils.isEmpty(a3)) {
            return resources.getString(identifier);
        }
        try {
            JSONArray jSONArray = new JSONArray(a3);
            Object[] objArr = new String[jSONArray.length()];
            for (int i = 0; i < objArr.length; i++) {
                objArr[i] = jSONArray.opt(i);
            }
            return resources.getString(identifier, objArr);
        } catch (JSONException e) {
            String str3 = "GcmNotification";
            String valueOf8 = String.valueOf(str);
            String valueOf9 = String.valueOf("_loc_args");
            String valueOf10 = String.valueOf(m11583a(valueOf9.length() != 0 ? valueOf8.concat(valueOf9) : new String(valueOf8)));
            Log.w(str3, new StringBuilder(String.valueOf(valueOf10).length() + 41 + String.valueOf(a3).length()).append("Malformed ").append(valueOf10).append(": ").append(a3).append("  Default value will be used.").toString());
            return null;
        } catch (MissingFormatArgumentException e2) {
            Log.w("GcmNotification", new StringBuilder(String.valueOf(a2).length() + 58 + String.valueOf(a3).length()).append("Missing format argument for ").append(a2).append(": ").append(a3).append(" Default value will be used.").toString(), e2);
            return null;
        }
    }

    /* renamed from: b */
    static void m11588b(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        Iterator it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            String string = bundle.getString(str);
            if (str.startsWith("gcm.notification.")) {
                str = str.replace("gcm.notification.", "gcm.n.");
            }
            if (str.startsWith("gcm.n.")) {
                if (!"gcm.n.e".equals(str)) {
                    bundle2.putString(str.substring("gcm.n.".length()), string);
                }
                it.remove();
            }
        }
        String string2 = bundle2.getString("sound2");
        if (string2 != null) {
            bundle2.remove("sound2");
            bundle2.putString("sound", string2);
        }
        if (!bundle2.isEmpty()) {
            bundle.putBundle("notification", bundle2);
        }
    }

    /* renamed from: b */
    static boolean m11589b(Context context) {
        if (((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            return false;
        }
        int myPid = Process.myPid();
        List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            if (runningAppProcessInfo.pid == myPid) {
                return runningAppProcessInfo.importance == 100;
            }
        }
        return false;
    }

    /* renamed from: c */
    private Uri m11590c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if ("default".equals(str) || this.f8978b.getResources().getIdentifier(str, "raw", this.f8978b.getPackageName()) == 0) {
            return RingtoneManager.getDefaultUri(2);
        }
        String valueOf = String.valueOf("android.resource://");
        String valueOf2 = String.valueOf(this.f8978b.getPackageName());
        return Uri.parse(new StringBuilder(String.valueOf(valueOf).length() + 5 + String.valueOf(valueOf2).length() + String.valueOf(str).length()).append(valueOf).append(valueOf2).append("/raw/").append(str).toString());
    }

    /* renamed from: d */
    private Notification m11591d(Bundle bundle) {
        String b = m11587b(bundle, "gcm.n.title");
        String b2 = m11587b(bundle, "gcm.n.body");
        int b3 = m11586b(m11582a(bundle, "gcm.n.icon"));
        String a = m11582a(bundle, "gcm.n.color");
        Uri c = m11590c(m11582a(bundle, "gcm.n.sound2"));
        PendingIntent e = m11592e(bundle);
        Builder smallIcon = new Builder(this.f8978b).setAutoCancel(true).setSmallIcon(b3);
        if (!TextUtils.isEmpty(b)) {
            smallIcon.setContentTitle(b);
        } else {
            smallIcon.setContentTitle(this.f8978b.getApplicationInfo().loadLabel(this.f8978b.getPackageManager()));
        }
        if (!TextUtils.isEmpty(b2)) {
            smallIcon.setContentText(b2);
        }
        if (!TextUtils.isEmpty(a)) {
            smallIcon.setColor(Color.parseColor(a));
        }
        if (c != null) {
            smallIcon.setSound(c);
        }
        if (e != null) {
            smallIcon.setContentIntent(e);
        }
        return smallIcon.build();
    }

    /* renamed from: e */
    private PendingIntent m11592e(Bundle bundle) {
        Intent intent;
        String a = m11582a(bundle, "gcm.n.click_action");
        if (!TextUtils.isEmpty(a)) {
            Intent intent2 = new Intent(a);
            intent2.setPackage(this.f8978b.getPackageName());
            intent2.setFlags(268435456);
            intent = intent2;
        } else {
            Intent launchIntentForPackage = this.f8978b.getPackageManager().getLaunchIntentForPackage(this.f8978b.getPackageName());
            if (launchIntentForPackage == null) {
                Log.w("GcmNotification", "No activity found to launch app");
                return null;
            }
            intent = launchIntentForPackage;
        }
        Bundle bundle2 = new Bundle(bundle);
        GcmListenerService.m11564a(bundle2);
        intent.putExtras(bundle2);
        for (String str : bundle2.keySet()) {
            if (str.startsWith("gcm.n.") || str.startsWith("gcm.notification.")) {
                intent.removeExtra(str);
            }
        }
        return PendingIntent.getActivity(this.f8978b, m11580a(), intent, 1073741824);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public boolean mo12296c(Bundle bundle) {
        try {
            m11584a(m11582a(bundle, "gcm.n.tag"), m11591d(bundle));
            return true;
        } catch (C3144zza e) {
            String str = "GcmNotification";
            String str2 = "Failed to show notification: ";
            String valueOf = String.valueOf(e.getMessage());
            Log.e(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            return false;
        }
    }
}
