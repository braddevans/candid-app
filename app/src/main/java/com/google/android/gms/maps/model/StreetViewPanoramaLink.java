package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class StreetViewPanoramaLink extends AbstractSafeParcelable {
    public static final aus CREATOR = new aus();

    /* renamed from: a */
    public final String f9302a;

    /* renamed from: b */
    public final float f9303b;

    /* renamed from: c */
    private final int f9304c;

    public StreetViewPanoramaLink(int i, String str, float f) {
        this.f9304c = i;
        this.f9302a = str;
        if (((double) f) <= 0.0d) {
            f = (f % 360.0f) + 360.0f;
        }
        this.f9303b = f % 360.0f;
    }

    /* renamed from: a */
    public int mo12681a() {
        return this.f9304c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaLink)) {
            return false;
        }
        StreetViewPanoramaLink streetViewPanoramaLink = (StreetViewPanoramaLink) obj;
        return this.f9302a.equals(streetViewPanoramaLink.f9302a) && Float.floatToIntBits(this.f9303b) == Float.floatToIntBits(streetViewPanoramaLink.f9303b);
    }

    public int hashCode() {
        return alv.m2719a(this.f9302a, Float.valueOf(this.f9303b));
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("panoId", this.f9302a).mo1191a("bearing", Float.valueOf(this.f9303b)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aus.m5708a(this, parcel, i);
    }
}
