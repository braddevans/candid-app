package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Keep;
import java.util.Map;

@Deprecated
public class AppMeasurement {

    /* renamed from: a */
    private final awi f9333a;

    /* renamed from: com.google.android.gms.measurement.AppMeasurement$a */
    public static final class C2181a extends C0982a {

        /* renamed from: a */
        public static final Map<String, String> f9334a = anf.m4030a((K[]) new String[]{"app_clear_data", "app_exception", "app_remove", "app_update", "firebase_campaign", "error", "first_open", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement"}, (V[]) new String[]{"_cd", "_ae", "_ui", "_au", "_cmp", "_err", "_f", "_iap", "_nd", "_nf", "_no", "_nr", "_ou", "_s", "_e"});
    }

    /* renamed from: com.google.android.gms.measurement.AppMeasurement$b */
    public interface C2182b {
        /* renamed from: a */
        void mo12714a(String str, String str2, Bundle bundle, long j);
    }

    /* renamed from: com.google.android.gms.measurement.AppMeasurement$c */
    public interface C2183c {
        /* renamed from: a */
        void mo12715a(String str, String str2, Bundle bundle, long j);
    }

    /* renamed from: com.google.android.gms.measurement.AppMeasurement$d */
    public static final class C2184d extends C0983b {

        /* renamed from: a */
        public static final Map<String, String> f9335a = anf.m4030a((K[]) new String[]{"firebase_conversion", "engagement_time_msec", "firebase_error", "firebase_error_value", "firebase_error_length", "debug", "realtime", "firebase_event_origin", "message_device_time", "message_id", "message_name", "message_time", "previous_app_version", "previous_os_version", "topic", "update_with_analytics", "previous_first_open_count", "system_app", "system_app_update"}, (V[]) new String[]{"_c", "_et", "_err", "_ev", "_el", "_dbg", "_r", "_o", "_ndt", "_nmid", "_nmn", "_nmt", "_pv", "_po", "_nt", "_uwa", "_pfo", "_sys", "_sysu"});
    }

    /* renamed from: com.google.android.gms.measurement.AppMeasurement$e */
    public static final class C2185e extends C0984c {

        /* renamed from: a */
        public static final Map<String, String> f9336a = anf.m4030a((K[]) new String[]{"firebase_last_notification", "first_open_time", "last_deep_link_referrer", "user_id"}, (V[]) new String[]{"_ln", "_fot", "_ldl", "_id"});
    }

    public AppMeasurement(awi awi) {
        alw.m2723a(awi);
        this.f9333a = awi;
    }

    @Keep
    @Deprecated
    public static AppMeasurement getInstance(Context context) {
        return awi.m6440a(context).mo8067m();
    }

    /* renamed from: a */
    public void mo12713a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.f9333a.mo8066l().mo7684a(str, str2, bundle);
    }
}
