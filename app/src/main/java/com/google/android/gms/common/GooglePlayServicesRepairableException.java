package com.google.android.gms.common;

import android.content.Intent;

public class GooglePlayServicesRepairableException extends UserRecoverableException {

    /* renamed from: a */
    private final int f8880a;

    public GooglePlayServicesRepairableException(int i, String str, Intent intent) {
        super(str, intent);
        this.f8880a = i;
    }

    /* renamed from: a */
    public int mo12201a() {
        return this.f8880a;
    }
}
