package com.google.android.gms.location;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class LocationResult extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Creator<LocationResult> CREATOR = new arq();

    /* renamed from: a */
    public static final List<Location> f9061a = Collections.emptyList();

    /* renamed from: b */
    private final int f9062b;

    /* renamed from: c */
    private final List<Location> f9063c;

    public LocationResult(int i, List<Location> list) {
        this.f9062b = i;
        this.f9063c = list;
    }

    /* renamed from: a */
    public List<Location> mo12419a() {
        return this.f9063c;
    }

    /* renamed from: b */
    public int mo12420b() {
        return this.f9062b;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof LocationResult)) {
            return false;
        }
        LocationResult locationResult = (LocationResult) obj;
        if (locationResult.f9063c.size() != this.f9063c.size()) {
            return false;
        }
        Iterator it = this.f9063c.iterator();
        for (Location time : locationResult.f9063c) {
            if (((Location) it.next()).getTime() != time.getTime()) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 17;
        Iterator it = this.f9063c.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            long time = ((Location) it.next()).getTime();
            i = ((int) (time ^ (time >>> 32))) + (i2 * 31);
        }
    }

    public String toString() {
        String valueOf = String.valueOf(this.f9063c);
        return new StringBuilder(String.valueOf(valueOf).length() + 27).append("LocationResult[locations: ").append(valueOf).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        arq.m4844a(this, parcel, i);
    }
}
