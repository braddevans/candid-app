package com.google.android.gms.measurement;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

public final class AppMeasurementService extends Service implements C0901a {

    /* renamed from: a */
    private avf f9339a;

    /* renamed from: b */
    private avf m11990b() {
        if (this.f9339a == null) {
            this.f9339a = new avf(this);
        }
        return this.f9339a;
    }

    /* renamed from: a */
    public Context mo7745a() {
        return this;
    }

    /* renamed from: a */
    public boolean mo7746a(int i) {
        return stopSelfResult(i);
    }

    public IBinder onBind(Intent intent) {
        return m11990b().mo7738a(intent);
    }

    public void onCreate() {
        super.onCreate();
        m11990b().mo7739a();
    }

    public void onDestroy() {
        m11990b().mo7740b();
        super.onDestroy();
    }

    public void onRebind(Intent intent) {
        m11990b().mo7742c(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int a = m11990b().mo7737a(intent, i, i2);
        AppMeasurementReceiver.m3705a(intent);
        return a;
    }

    public boolean onUnbind(Intent intent) {
        return m11990b().mo7741b(intent);
    }
}
