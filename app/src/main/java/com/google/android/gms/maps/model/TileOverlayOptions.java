package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class TileOverlayOptions extends AbstractSafeParcelable {
    public static final auw CREATOR = new auw();

    /* renamed from: a */
    private final int f9318a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public aug f9319b;

    /* renamed from: c */
    private atx f9320c;

    /* renamed from: d */
    private boolean f9321d;

    /* renamed from: e */
    private float f9322e;

    /* renamed from: f */
    private boolean f9323f;

    /* renamed from: g */
    private float f9324g;

    public TileOverlayOptions() {
        this.f9321d = true;
        this.f9323f = true;
        this.f9324g = 0.0f;
        this.f9318a = 1;
    }

    public TileOverlayOptions(int i, IBinder iBinder, boolean z, float f, boolean z2, float f2) {
        this.f9321d = true;
        this.f9323f = true;
        this.f9324g = 0.0f;
        this.f9318a = i;
        this.f9319b = C0882a.m5673a(iBinder);
        this.f9320c = this.f9319b == null ? null : new atx() {

            /* renamed from: c */
            private final aug f9326c = TileOverlayOptions.this.f9319b;
        };
        this.f9321d = z;
        this.f9322e = f;
        this.f9323f = z2;
        this.f9324g = f2;
    }

    /* renamed from: a */
    public int mo12701a() {
        return this.f9318a;
    }

    /* renamed from: b */
    public IBinder mo12702b() {
        return this.f9319b.asBinder();
    }

    /* renamed from: c */
    public float mo12703c() {
        return this.f9322e;
    }

    /* renamed from: d */
    public boolean mo12704d() {
        return this.f9321d;
    }

    /* renamed from: e */
    public boolean mo12705e() {
        return this.f9323f;
    }

    /* renamed from: f */
    public float mo12706f() {
        return this.f9324g;
    }

    public void writeToParcel(Parcel parcel, int i) {
        auw.m5720a(this, parcel, i);
    }
}
