package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class EventParcel extends AbstractSafeParcelable {
    public static final avw CREATOR = new avw();

    /* renamed from: a */
    public final int f9356a;

    /* renamed from: b */
    public final String f9357b;

    /* renamed from: c */
    public final EventParams f9358c;

    /* renamed from: d */
    public final String f9359d;

    /* renamed from: e */
    public final long f9360e;

    public EventParcel(int i, String str, EventParams eventParams, String str2, long j) {
        this.f9356a = i;
        this.f9357b = str;
        this.f9358c = eventParams;
        this.f9359d = str2;
        this.f9360e = j;
    }

    public EventParcel(String str, EventParams eventParams, String str2, long j) {
        this.f9356a = 1;
        this.f9357b = str;
        this.f9358c = eventParams;
        this.f9359d = str2;
        this.f9360e = j;
    }

    public String toString() {
        String str = this.f9359d;
        String str2 = this.f9357b;
        String valueOf = String.valueOf(this.f9358c);
        return new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length()).append("origin=").append(str).append(",name=").append(str2).append(",params=").append(valueOf).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        avw.m6231a(this, parcel, i);
    }
}
