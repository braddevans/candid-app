package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class GestureRequest extends AbstractSafeParcelable {
    public static final arn CREATOR = new arn();

    /* renamed from: a */
    private static final List<Integer> f9041a = Collections.unmodifiableList(Arrays.asList(new Integer[]{Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(5), Integer.valueOf(6), Integer.valueOf(7), Integer.valueOf(8), Integer.valueOf(9), Integer.valueOf(10), Integer.valueOf(11), Integer.valueOf(12), Integer.valueOf(13), Integer.valueOf(14), Integer.valueOf(15), Integer.valueOf(16), Integer.valueOf(17), Integer.valueOf(18), Integer.valueOf(19)}));

    /* renamed from: b */
    private static final List<Integer> f9042b = Collections.unmodifiableList(Arrays.asList(new Integer[]{Integer.valueOf(1)}));

    /* renamed from: c */
    private static final List<Integer> f9043c = Collections.unmodifiableList(Arrays.asList(new Integer[]{Integer.valueOf(2), Integer.valueOf(4), Integer.valueOf(6), Integer.valueOf(8), Integer.valueOf(10), Integer.valueOf(12), Integer.valueOf(14), Integer.valueOf(16), Integer.valueOf(18), Integer.valueOf(19)}));

    /* renamed from: d */
    private static final List<Integer> f9044d = Collections.unmodifiableList(Arrays.asList(new Integer[]{Integer.valueOf(3), Integer.valueOf(5), Integer.valueOf(7), Integer.valueOf(9), Integer.valueOf(11), Integer.valueOf(13), Integer.valueOf(15), Integer.valueOf(17)}));

    /* renamed from: e */
    private final int f9045e;

    /* renamed from: f */
    private final List<Integer> f9046f;

    public GestureRequest(int i, List<Integer> list) {
        this.f9045e = i;
        this.f9046f = list;
    }

    /* renamed from: a */
    public int mo12405a() {
        return this.f9045e;
    }

    /* renamed from: b */
    public List<Integer> mo12406b() {
        return this.f9046f;
    }

    public void writeToParcel(Parcel parcel, int i) {
        arn.m4833a(this, parcel, i);
    }
}
