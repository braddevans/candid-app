package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.List;

public final class PolylineOptions extends AbstractSafeParcelable {
    public static final auq CREATOR = new auq();

    /* renamed from: a */
    private final int f9289a;

    /* renamed from: b */
    private final List<LatLng> f9290b;

    /* renamed from: c */
    private float f9291c;

    /* renamed from: d */
    private int f9292d;

    /* renamed from: e */
    private float f9293e;

    /* renamed from: f */
    private boolean f9294f;

    /* renamed from: g */
    private boolean f9295g;

    /* renamed from: h */
    private boolean f9296h;

    public PolylineOptions() {
        this.f9291c = 10.0f;
        this.f9292d = -16777216;
        this.f9293e = 0.0f;
        this.f9294f = true;
        this.f9295g = false;
        this.f9296h = false;
        this.f9289a = 1;
        this.f9290b = new ArrayList();
    }

    public PolylineOptions(int i, List list, float f, int i2, float f2, boolean z, boolean z2, boolean z3) {
        this.f9291c = 10.0f;
        this.f9292d = -16777216;
        this.f9293e = 0.0f;
        this.f9294f = true;
        this.f9295g = false;
        this.f9296h = false;
        this.f9289a = i;
        this.f9290b = list;
        this.f9291c = f;
        this.f9292d = i2;
        this.f9293e = f2;
        this.f9294f = z;
        this.f9295g = z2;
        this.f9296h = z3;
    }

    /* renamed from: a */
    public int mo12667a() {
        return this.f9289a;
    }

    /* renamed from: b */
    public List<LatLng> mo12668b() {
        return this.f9290b;
    }

    /* renamed from: c */
    public float mo12669c() {
        return this.f9291c;
    }

    /* renamed from: d */
    public int mo12670d() {
        return this.f9292d;
    }

    /* renamed from: e */
    public float mo12671e() {
        return this.f9293e;
    }

    /* renamed from: f */
    public boolean mo12672f() {
        return this.f9294f;
    }

    /* renamed from: g */
    public boolean mo12673g() {
        return this.f9295g;
    }

    /* renamed from: h */
    public boolean mo12674h() {
        return this.f9296h;
    }

    public void writeToParcel(Parcel parcel, int i) {
        auq.m5702a(this, parcel, i);
    }
}
