package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Iterator;

public class EventParams extends AbstractSafeParcelable implements Iterable<String> {
    public static final avv CREATOR = new avv();

    /* renamed from: a */
    public final int f9352a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Bundle f9353b;

    public EventParams(int i, Bundle bundle) {
        this.f9352a = i;
        this.f9353b = bundle;
    }

    public EventParams(Bundle bundle) {
        alw.m2723a(bundle);
        this.f9353b = bundle;
        this.f9352a = 1;
    }

    /* renamed from: a */
    public int mo12732a() {
        return this.f9353b.size();
    }

    /* renamed from: a */
    public Object mo12733a(String str) {
        return this.f9353b.get(str);
    }

    /* renamed from: b */
    public Bundle mo12734b() {
        return new Bundle(this.f9353b);
    }

    public Iterator<String> iterator() {
        return new Iterator<String>() {

            /* renamed from: a */
            Iterator<String> f9354a = EventParams.this.f9353b.keySet().iterator();

            /* renamed from: a */
            public String next() {
                return (String) this.f9354a.next();
            }

            public boolean hasNext() {
                return this.f9354a.hasNext();
            }

            public void remove() {
                throw new UnsupportedOperationException("Remove not supported");
            }
        };
    }

    public String toString() {
        return this.f9353b.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        avv.m6228a(this, parcel, i);
    }
}
