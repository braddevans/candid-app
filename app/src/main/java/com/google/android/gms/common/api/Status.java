package com.google.android.gms.common.api;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.IntentSender.SendIntentException;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class Status extends AbstractSafeParcelable implements alf, ReflectedParcelable {
    public static final Creator<Status> CREATOR = new alp();

    /* renamed from: a */
    public static final Status f8890a = new Status(0);

    /* renamed from: b */
    public static final Status f8891b = new Status(14);

    /* renamed from: c */
    public static final Status f8892c = new Status(8);

    /* renamed from: d */
    public static final Status f8893d = new Status(15);

    /* renamed from: e */
    public static final Status f8894e = new Status(16);

    /* renamed from: f */
    public static final Status f8895f = new Status(17);

    /* renamed from: g */
    public static final Status f8896g = new Status(18);

    /* renamed from: h */
    private final int f8897h;

    /* renamed from: i */
    private final int f8898i;

    /* renamed from: j */
    private final String f8899j;

    /* renamed from: k */
    private final PendingIntent f8900k;

    public Status(int i) {
        this(i, null);
    }

    public Status(int i, int i2, String str, PendingIntent pendingIntent) {
        this.f8897h = i;
        this.f8898i = i2;
        this.f8899j = str;
        this.f8900k = pendingIntent;
    }

    public Status(int i, String str) {
        this(1, i, str, null);
    }

    public Status(int i, String str, PendingIntent pendingIntent) {
        this(1, i, str, pendingIntent);
    }

    /* renamed from: h */
    private String m11473h() {
        return this.f8899j != null ? this.f8899j : alb.m2647a(this.f8898i);
    }

    /* renamed from: a */
    public Status mo1166a() {
        return this;
    }

    /* renamed from: a */
    public void mo12221a(Activity activity, int i) throws SendIntentException {
        if (mo12225e()) {
            activity.startIntentSenderForResult(this.f8900k.getIntentSender(), i, null, 0, 0, 0);
        }
    }

    /* renamed from: b */
    public PendingIntent mo12222b() {
        return this.f8900k;
    }

    /* renamed from: c */
    public String mo12223c() {
        return this.f8899j;
    }

    /* renamed from: d */
    public int mo12224d() {
        return this.f8897h;
    }

    /* renamed from: e */
    public boolean mo12225e() {
        return this.f8900k != null;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        return this.f8897h == status.f8897h && this.f8898i == status.f8898i && alv.m2721a(this.f8899j, status.f8899j) && alv.m2721a(this.f8900k, status.f8900k);
    }

    /* renamed from: f */
    public boolean mo12227f() {
        return this.f8898i <= 0;
    }

    /* renamed from: g */
    public int mo12228g() {
        return this.f8898i;
    }

    public int hashCode() {
        return alv.m2719a(Integer.valueOf(this.f8897h), Integer.valueOf(this.f8898i), this.f8899j, this.f8900k);
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("statusCode", m11473h()).mo1191a("resolution", this.f8900k).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        alp.m2676a(this, parcel, i);
    }
}
