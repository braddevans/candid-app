package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class MarkerOptions extends AbstractSafeParcelable {
    public static final aun CREATOR = new aun();

    /* renamed from: a */
    private final int f9260a;

    /* renamed from: b */
    private LatLng f9261b;

    /* renamed from: c */
    private String f9262c;

    /* renamed from: d */
    private String f9263d;

    /* renamed from: e */
    private atv f9264e;

    /* renamed from: f */
    private float f9265f;

    /* renamed from: g */
    private float f9266g;

    /* renamed from: h */
    private boolean f9267h;

    /* renamed from: i */
    private boolean f9268i;

    /* renamed from: j */
    private boolean f9269j;

    /* renamed from: k */
    private float f9270k;

    /* renamed from: l */
    private float f9271l;

    /* renamed from: m */
    private float f9272m;

    /* renamed from: n */
    private float f9273n;

    /* renamed from: o */
    private float f9274o;

    public MarkerOptions() {
        this.f9265f = 0.5f;
        this.f9266g = 1.0f;
        this.f9268i = true;
        this.f9269j = false;
        this.f9270k = 0.0f;
        this.f9271l = 0.5f;
        this.f9272m = 0.0f;
        this.f9273n = 1.0f;
        this.f9260a = 1;
    }

    public MarkerOptions(int i, LatLng latLng, String str, String str2, IBinder iBinder, float f, float f2, boolean z, boolean z2, boolean z3, float f3, float f4, float f5, float f6, float f7) {
        this.f9265f = 0.5f;
        this.f9266g = 1.0f;
        this.f9268i = true;
        this.f9269j = false;
        this.f9270k = 0.0f;
        this.f9271l = 0.5f;
        this.f9272m = 0.0f;
        this.f9273n = 1.0f;
        this.f9260a = i;
        this.f9261b = latLng;
        this.f9262c = str;
        this.f9263d = str2;
        this.f9264e = iBinder == null ? null : new atv(C0670a.m4132a(iBinder));
        this.f9265f = f;
        this.f9266g = f2;
        this.f9267h = z;
        this.f9268i = z2;
        this.f9269j = z3;
        this.f9270k = f3;
        this.f9271l = f4;
        this.f9272m = f5;
        this.f9273n = f6;
        this.f9274o = f7;
    }

    /* renamed from: a */
    public int mo12638a() {
        return this.f9260a;
    }

    /* renamed from: b */
    public IBinder mo12639b() {
        if (this.f9264e == null) {
            return null;
        }
        return this.f9264e.mo7374a().asBinder();
    }

    /* renamed from: c */
    public LatLng mo12640c() {
        return this.f9261b;
    }

    /* renamed from: d */
    public String mo12641d() {
        return this.f9262c;
    }

    /* renamed from: e */
    public String mo12642e() {
        return this.f9263d;
    }

    /* renamed from: f */
    public float mo12643f() {
        return this.f9265f;
    }

    /* renamed from: g */
    public float mo12644g() {
        return this.f9266g;
    }

    /* renamed from: h */
    public boolean mo12645h() {
        return this.f9267h;
    }

    /* renamed from: i */
    public boolean mo12646i() {
        return this.f9268i;
    }

    /* renamed from: j */
    public boolean mo12647j() {
        return this.f9269j;
    }

    /* renamed from: k */
    public float mo12648k() {
        return this.f9270k;
    }

    /* renamed from: l */
    public float mo12649l() {
        return this.f9271l;
    }

    /* renamed from: m */
    public float mo12650m() {
        return this.f9272m;
    }

    /* renamed from: n */
    public float mo12651n() {
        return this.f9273n;
    }

    /* renamed from: o */
    public float mo12652o() {
        return this.f9274o;
    }

    public void writeToParcel(Parcel parcel, int i) {
        aun.m5693a(this, parcel, i);
    }
}
