package com.google.android.gms.measurement;

import android.content.Context;
import android.content.Intent;
import android.support.p001v4.content.WakefulBroadcastReceiver;

public final class AppMeasurementReceiver extends WakefulBroadcastReceiver implements C0928a {

    /* renamed from: a */
    private awg f9338a;

    /* renamed from: a */
    private awg m11988a() {
        if (this.f9338a == null) {
            this.f9338a = new awg(this);
        }
        return this.f9338a;
    }

    /* renamed from: a */
    public void mo8017a(Context context, Intent intent) {
        m3706a_(context, intent);
    }

    public void onReceive(Context context, Intent intent) {
        m11988a().mo8015a(context, intent);
    }
}
