package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class PointOfInterest extends AbstractSafeParcelable {
    public static final auo CREATOR = new auo();

    /* renamed from: a */
    public final LatLng f9275a;

    /* renamed from: b */
    public final String f9276b;

    /* renamed from: c */
    public final String f9277c;

    /* renamed from: d */
    private final int f9278d;

    public PointOfInterest(int i, LatLng latLng, String str, String str2) {
        this.f9278d = i;
        this.f9275a = latLng;
        this.f9276b = str;
        this.f9277c = str2;
    }

    /* renamed from: a */
    public int mo12654a() {
        return this.f9278d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        auo.m5696a(this, parcel, i);
    }
}
