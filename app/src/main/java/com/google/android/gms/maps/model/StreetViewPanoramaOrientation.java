package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class StreetViewPanoramaOrientation extends AbstractSafeParcelable {
    public static final auu CREATOR = new auu();

    /* renamed from: a */
    public final float f9309a;

    /* renamed from: b */
    public final float f9310b;

    /* renamed from: c */
    private final int f9311c;

    /* renamed from: com.google.android.gms.maps.model.StreetViewPanoramaOrientation$a */
    public static final class C2179a {

        /* renamed from: a */
        public float f9312a;

        /* renamed from: b */
        public float f9313b;

        /* renamed from: a */
        public C2179a mo12696a(float f) {
            this.f9313b = f;
            return this;
        }

        /* renamed from: a */
        public StreetViewPanoramaOrientation mo12697a() {
            return new StreetViewPanoramaOrientation(this.f9313b, this.f9312a);
        }

        /* renamed from: b */
        public C2179a mo12698b(float f) {
            this.f9312a = f;
            return this;
        }
    }

    public StreetViewPanoramaOrientation(float f, float f2) {
        this(1, f, f2);
    }

    public StreetViewPanoramaOrientation(int i, float f, float f2) {
        alw.m2732b(-90.0f <= f && f <= 90.0f, "Tilt needs to be between -90 and 90 inclusive");
        this.f9311c = i;
        this.f9309a = 0.0f + f;
        if (((double) f2) <= 0.0d) {
            f2 = (f2 % 360.0f) + 360.0f;
        }
        this.f9310b = f2 % 360.0f;
    }

    /* renamed from: a */
    public int mo12691a() {
        return this.f9311c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaOrientation)) {
            return false;
        }
        StreetViewPanoramaOrientation streetViewPanoramaOrientation = (StreetViewPanoramaOrientation) obj;
        return Float.floatToIntBits(this.f9309a) == Float.floatToIntBits(streetViewPanoramaOrientation.f9309a) && Float.floatToIntBits(this.f9310b) == Float.floatToIntBits(streetViewPanoramaOrientation.f9310b);
    }

    public int hashCode() {
        return alv.m2719a(Float.valueOf(this.f9309a), Float.valueOf(this.f9310b));
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("tilt", Float.valueOf(this.f9309a)).mo1191a("bearing", Float.valueOf(this.f9310b)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        auu.m5714a(this, parcel, i);
    }
}
