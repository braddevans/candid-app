package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class zzqv extends BroadcastReceiver {

    /* renamed from: a */
    protected Context f8994a;

    /* renamed from: b */
    private final C2151a f8995b;

    /* renamed from: com.google.android.gms.internal.zzqv$a */
    public static abstract class C2151a {
        /* renamed from: a */
        public abstract void mo6685a();
    }

    public zzqv(C2151a aVar) {
        this.f8995b = aVar;
    }

    /* renamed from: a */
    public synchronized void mo12343a() {
        if (this.f8994a != null) {
            this.f8994a.unregisterReceiver(this);
        }
        this.f8994a = null;
    }

    /* renamed from: a */
    public void mo12344a(Context context) {
        this.f8994a = context;
    }

    public void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        String str = null;
        if (data != null) {
            str = data.getSchemeSpecificPart();
        }
        if ("com.google.android.gms".equals(str)) {
            this.f8995b.mo6685a();
            mo12343a();
        }
    }
}
