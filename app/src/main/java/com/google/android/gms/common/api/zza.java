package com.google.android.gms.common.api;

public class zza extends Exception {

    /* renamed from: a */
    protected final Status f8901a;

    public zza(Status status) {
        super(status.mo12223c());
        this.f8901a = status;
    }
}
