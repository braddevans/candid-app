package com.google.android.gms.maps;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import java.util.ArrayList;
import java.util.List;

@TargetApi(11)
public class StreetViewPanoramaFragment extends Fragment {

    /* renamed from: a */
    private final C2174b f9189a = new C2174b(this);

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaFragment$a */
    static class C2172a implements asl {

        /* renamed from: a */
        private final Fragment f9190a;

        /* renamed from: b */
        private final ash f9191b;

        public C2172a(Fragment fragment, ash ash) {
            this.f9191b = (ash) alw.m2723a(ash);
            this.f9190a = (Fragment) alw.m2723a(fragment);
        }

        /* renamed from: a */
        public View mo6484a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            try {
                return (View) any.m4134a(this.f9191b.mo7188a(any.m4133a(layoutInflater), any.m4133a(viewGroup), bundle));
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo6485a() {
        }

        /* renamed from: a */
        public void mo6486a(Activity activity, Bundle bundle, Bundle bundle2) {
            try {
                this.f9191b.mo7191a(any.m4133a(activity), (StreetViewPanoramaOptions) null, bundle2);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo6487a(Bundle bundle) {
            if (bundle == null) {
                try {
                    bundle = new Bundle();
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                }
            }
            Bundle arguments = this.f9190a.getArguments();
            if (arguments != null && arguments.containsKey("StreetViewPanoramaOptions")) {
                asu.m5272a(bundle, "StreetViewPanoramaOptions", arguments.getParcelable("StreetViewPanoramaOptions"));
            }
            this.f9191b.mo7190a(bundle);
        }

        /* renamed from: a */
        public void mo12574a(final ary ary) {
            try {
                this.f9191b.mo7192a((ass) new C0811a() {
                    /* renamed from: a */
                    public void mo7254a(asg asg) throws RemoteException {
                        ary.mo7024a(new arz(asg));
                    }
                });
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6488b() {
            try {
                this.f9191b.mo7193b();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6489b(Bundle bundle) {
            try {
                this.f9191b.mo7194b(bundle);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: c */
        public void mo6490c() {
            try {
                this.f9191b.mo7195c();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: d */
        public void mo6491d() {
        }

        /* renamed from: e */
        public void mo6492e() {
            try {
                this.f9191b.mo7196d();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: f */
        public void mo6493f() {
            try {
                this.f9191b.mo7197e();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: g */
        public void mo6494g() {
            try {
                this.f9191b.mo7198f();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
    }

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaFragment$b */
    static class C2174b extends anw<C2172a> {

        /* renamed from: a */
        protected anz<C2172a> f9194a;

        /* renamed from: b */
        private final Fragment f9195b;

        /* renamed from: c */
        private Activity f9196c;

        /* renamed from: d */
        private final List<ary> f9197d = new ArrayList();

        C2174b(Fragment fragment) {
            this.f9195b = fragment;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m11868a(Activity activity) {
            this.f9196c = activity;
            mo12575i();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo6500a(anz<C2172a> anz) {
            this.f9194a = anz;
            mo12575i();
        }

        /* renamed from: i */
        public void mo12575i() {
            if (this.f9196c != null && this.f9194a != null && mo6496a() == null) {
                try {
                    arw.m4857a((Context) this.f9196c);
                    this.f9194a.mo6509a(new C2172a(this.f9195b, asv.m5273a((Context) this.f9196c).mo7275c(any.m4133a(this.f9196c))));
                    for (ary a : this.f9197d) {
                        ((C2172a) mo6496a()).mo12574a(a);
                    }
                    this.f9197d.clear();
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public void onActivityCreated(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(StreetViewPanoramaFragment.class.getClassLoader());
        }
        super.onActivityCreated(bundle);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f9189a.m11868a(activity);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f9189a.mo6498a(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.f9189a.mo6495a(layoutInflater, viewGroup, bundle);
    }

    public void onDestroy() {
        this.f9189a.mo6507g();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.f9189a.mo6506f();
        super.onDestroyView();
    }

    @SuppressLint({"NewApi"})
    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(activity, attributeSet, bundle);
        this.f9189a.m11868a(activity);
        this.f9189a.mo6497a(activity, new Bundle(), bundle);
    }

    public void onLowMemory() {
        this.f9189a.mo6508h();
        super.onLowMemory();
    }

    public void onPause() {
        this.f9189a.mo6504d();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f9189a.mo6503c();
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(StreetViewPanoramaFragment.class.getClassLoader());
        }
        super.onSaveInstanceState(bundle);
        this.f9189a.mo6502b(bundle);
    }

    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
    }
}
