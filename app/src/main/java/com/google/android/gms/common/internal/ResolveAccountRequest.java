package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ResolveAccountRequest extends AbstractSafeParcelable {
    public static final Creator<ResolveAccountRequest> CREATOR = new alx();

    /* renamed from: a */
    public final int f8933a;

    /* renamed from: b */
    private final Account f8934b;

    /* renamed from: c */
    private final int f8935c;

    /* renamed from: d */
    private final GoogleSignInAccount f8936d;

    public ResolveAccountRequest(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.f8933a = i;
        this.f8934b = account;
        this.f8935c = i2;
        this.f8936d = googleSignInAccount;
    }

    /* renamed from: a */
    public Account mo12249a() {
        return this.f8934b;
    }

    /* renamed from: b */
    public int mo12250b() {
        return this.f8935c;
    }

    /* renamed from: c */
    public GoogleSignInAccount mo12251c() {
        return this.f8936d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        alx.m2734a(this, parcel, i);
    }
}
