package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ActivityTransition extends AbstractSafeParcelable {
    public static final ark CREATOR = new ark();

    /* renamed from: a */
    private final int f9022a;

    /* renamed from: b */
    private final int f9023b;

    /* renamed from: c */
    private final int f9024c;

    public ActivityTransition(int i, int i2, int i3) {
        this.f9022a = i;
        this.f9023b = i2;
        this.f9024c = i3;
    }

    /* renamed from: a */
    public int mo12371a() {
        return this.f9022a;
    }

    /* renamed from: b */
    public int mo12372b() {
        return this.f9023b;
    }

    /* renamed from: c */
    public int mo12373c() {
        return this.f9024c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityTransition)) {
            return false;
        }
        ActivityTransition activityTransition = (ActivityTransition) obj;
        return this.f9023b == activityTransition.f9023b && this.f9024c == activityTransition.f9024c;
    }

    public int hashCode() {
        return alv.m2719a(Integer.valueOf(this.f9023b), Integer.valueOf(this.f9024c));
    }

    public String toString() {
        int i = this.f9023b;
        return "ActivityTransition [mActivityType=" + i + ", mTransitionType=" + this.f9024c + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        ark.m4824a(this, parcel, i);
    }
}
