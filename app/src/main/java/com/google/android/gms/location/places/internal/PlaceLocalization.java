package com.google.android.gms.location.places.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

@Deprecated
public final class PlaceLocalization extends AbstractSafeParcelable {
    public static final are CREATOR = new are();

    /* renamed from: a */
    public final int f9137a;

    /* renamed from: b */
    public final String f9138b;

    /* renamed from: c */
    public final String f9139c;

    /* renamed from: d */
    public final String f9140d;

    /* renamed from: e */
    public final String f9141e;

    /* renamed from: f */
    public final List<String> f9142f;

    public PlaceLocalization(int i, String str, String str2, String str3, String str4, List<String> list) {
        this.f9137a = i;
        this.f9138b = str;
        this.f9139c = str2;
        this.f9140d = str3;
        this.f9141e = str4;
        this.f9142f = list;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PlaceLocalization)) {
            return false;
        }
        PlaceLocalization placeLocalization = (PlaceLocalization) obj;
        return alv.m2721a(this.f9138b, placeLocalization.f9138b) && alv.m2721a(this.f9139c, placeLocalization.f9139c) && alv.m2721a(this.f9140d, placeLocalization.f9140d) && alv.m2721a(this.f9141e, placeLocalization.f9141e) && alv.m2721a(this.f9142f, placeLocalization.f9142f);
    }

    public int hashCode() {
        return alv.m2719a(this.f9138b, this.f9139c, this.f9140d, this.f9141e);
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("name", this.f9138b).mo1191a("address", this.f9139c).mo1191a("internationalPhoneNumber", this.f9140d).mo1191a("regularOpenHours", this.f9141e).mo1191a("attributions", this.f9142f).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        are are = CREATOR;
        are.m4803a(this, parcel, i);
    }
}
