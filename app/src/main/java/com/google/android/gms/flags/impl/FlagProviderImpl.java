package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.gms.common.util.DynamiteApi;

@DynamiteApi
public class FlagProviderImpl extends C0727a {

    /* renamed from: a */
    private boolean f8966a = false;

    /* renamed from: b */
    private SharedPreferences f8967b;

    public boolean getBooleanFlagValue(String str, boolean z, int i) {
        return !this.f8966a ? z : C0679a.m4183a(this.f8967b, str, Boolean.valueOf(z)).booleanValue();
    }

    public int getIntFlagValue(String str, int i, int i2) {
        return !this.f8966a ? i : C0681b.m4185a(this.f8967b, str, Integer.valueOf(i)).intValue();
    }

    public long getLongFlagValue(String str, long j, int i) {
        return !this.f8966a ? j : C0683c.m4187a(this.f8967b, str, Long.valueOf(j)).longValue();
    }

    public String getStringFlagValue(String str, String str2, int i) {
        return !this.f8966a ? str2 : C0685d.m4189a(this.f8967b, str, str2);
    }

    public void init(anx anx) {
        Context context = (Context) any.m4134a(anx);
        if (!this.f8966a) {
            try {
                this.f8967b = aob.m4191a(context.createPackageContext("com.google.android.gms", 0));
                this.f8966a = true;
            } catch (NameNotFoundException e) {
            }
        }
    }
}
