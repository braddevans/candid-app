package com.google.android.gms.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import java.util.ArrayList;
import java.util.List;

public class StreetViewPanoramaView extends FrameLayout {

    /* renamed from: a */
    private final C2177b f9208a;

    /* renamed from: b */
    private arz f9209b;

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaView$a */
    static class C2175a implements asl {

        /* renamed from: a */
        private final ViewGroup f9210a;

        /* renamed from: b */
        private final asi f9211b;

        /* renamed from: c */
        private View f9212c;

        public C2175a(ViewGroup viewGroup, asi asi) {
            this.f9211b = (asi) alw.m2723a(asi);
            this.f9210a = (ViewGroup) alw.m2723a(viewGroup);
        }

        /* renamed from: a */
        public View mo6484a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            throw new UnsupportedOperationException("onCreateView not allowed on StreetViewPanoramaViewDelegate");
        }

        /* renamed from: a */
        public void mo6485a() {
        }

        /* renamed from: a */
        public void mo6486a(Activity activity, Bundle bundle, Bundle bundle2) {
            throw new UnsupportedOperationException("onInflate not allowed on StreetViewPanoramaViewDelegate");
        }

        /* renamed from: a */
        public void mo6487a(Bundle bundle) {
            try {
                this.f9211b.mo7203a(bundle);
                this.f9212c = (View) any.m4134a(this.f9211b.mo7210f());
                this.f9210a.removeAllViews();
                this.f9210a.addView(this.f9212c);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo12588a(final ary ary) {
            try {
                this.f9211b.mo7204a((ass) new C0811a() {
                    /* renamed from: a */
                    public void mo7254a(asg asg) throws RemoteException {
                        ary.mo7024a(new arz(asg));
                    }
                });
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6488b() {
            try {
                this.f9211b.mo7205b();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6489b(Bundle bundle) {
            try {
                this.f9211b.mo7206b(bundle);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: c */
        public void mo6490c() {
            try {
                this.f9211b.mo7207c();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: d */
        public void mo6491d() {
        }

        /* renamed from: e */
        public void mo6492e() {
            throw new UnsupportedOperationException("onDestroyView not allowed on StreetViewPanoramaViewDelegate");
        }

        /* renamed from: f */
        public void mo6493f() {
            try {
                this.f9211b.mo7208d();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: g */
        public void mo6494g() {
            try {
                this.f9211b.mo7209e();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: h */
        public asi mo12589h() {
            return this.f9211b;
        }
    }

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaView$b */
    static class C2177b extends anw<C2175a> {

        /* renamed from: a */
        protected anz<C2175a> f9215a;

        /* renamed from: b */
        private final ViewGroup f9216b;

        /* renamed from: c */
        private final Context f9217c;

        /* renamed from: d */
        private final StreetViewPanoramaOptions f9218d;

        /* renamed from: e */
        private final List<ary> f9219e = new ArrayList();

        C2177b(ViewGroup viewGroup, Context context, StreetViewPanoramaOptions streetViewPanoramaOptions) {
            this.f9216b = viewGroup;
            this.f9217c = context;
            this.f9218d = streetViewPanoramaOptions;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo6500a(anz<C2175a> anz) {
            this.f9215a = anz;
            mo12590i();
        }

        /* renamed from: i */
        public void mo12590i() {
            if (this.f9215a != null && mo6496a() == null) {
                try {
                    this.f9215a.mo6509a(new C2175a(this.f9216b, asv.m5273a(this.f9217c).mo7270a(any.m4133a(this.f9217c), this.f9218d)));
                    for (ary a : this.f9219e) {
                        ((C2175a) mo6496a()).mo12588a(a);
                    }
                    this.f9219e.clear();
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public StreetViewPanoramaView(Context context) {
        super(context);
        this.f9208a = new C2177b(this, context, null);
    }

    public StreetViewPanoramaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f9208a = new C2177b(this, context, null);
    }

    public StreetViewPanoramaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9208a = new C2177b(this, context, null);
    }

    @Deprecated
    public final arz getStreetViewPanorama() {
        if (this.f9209b != null) {
            return this.f9209b;
        }
        this.f9208a.mo12590i();
        if (this.f9208a.mo6496a() == null) {
            return null;
        }
        try {
            this.f9209b = new arz(((C2175a) this.f9208a.mo6496a()).mo12589h().mo7202a());
            return this.f9209b;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
