package com.google.android.gms.common.stats;

import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class StatsEvent extends AbstractSafeParcelable implements ReflectedParcelable {
    /* renamed from: a */
    public abstract long mo12270a();

    /* renamed from: b */
    public abstract int mo12271b();

    /* renamed from: i */
    public abstract long mo12278i();

    /* renamed from: l */
    public abstract String mo12281l();

    public String toString() {
        long a = mo12270a();
        String valueOf = String.valueOf("\t");
        int b = mo12271b();
        String valueOf2 = String.valueOf("\t");
        long i = mo12278i();
        String valueOf3 = String.valueOf(mo12281l());
        return new StringBuilder(String.valueOf(valueOf).length() + 51 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length()).append(a).append(valueOf).append(b).append(valueOf2).append(i).append(valueOf3).toString();
    }
}
