package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class UserAttributeParcel extends AbstractSafeParcelable {
    public static final avk CREATOR = new avk();

    /* renamed from: a */
    public final int f9361a;

    /* renamed from: b */
    public final String f9362b;

    /* renamed from: c */
    public final long f9363c;

    /* renamed from: d */
    public final Long f9364d;

    /* renamed from: e */
    public final Float f9365e;

    /* renamed from: f */
    public final String f9366f;

    /* renamed from: g */
    public final String f9367g;

    /* renamed from: h */
    public final Double f9368h;

    public UserAttributeParcel(int i, String str, long j, Long l, Float f, String str2, String str3, Double d) {
        Double d2 = null;
        this.f9361a = i;
        this.f9362b = str;
        this.f9363c = j;
        this.f9364d = l;
        this.f9365e = null;
        if (i == 1) {
            if (f != null) {
                d2 = Double.valueOf(f.doubleValue());
            }
            this.f9368h = d2;
        } else {
            this.f9368h = d;
        }
        this.f9366f = str2;
        this.f9367g = str3;
    }

    public UserAttributeParcel(avl avl) {
        this(avl.f4276b, avl.f4277c, avl.f4278d, avl.f4275a);
    }

    public UserAttributeParcel(String str, long j, Object obj, String str2) {
        alw.m2725a(str);
        this.f9361a = 2;
        this.f9362b = str;
        this.f9363c = j;
        this.f9367g = str2;
        if (obj == null) {
            this.f9364d = null;
            this.f9365e = null;
            this.f9368h = null;
            this.f9366f = null;
        } else if (obj instanceof Long) {
            this.f9364d = (Long) obj;
            this.f9365e = null;
            this.f9368h = null;
            this.f9366f = null;
        } else if (obj instanceof String) {
            this.f9364d = null;
            this.f9365e = null;
            this.f9368h = null;
            this.f9366f = (String) obj;
        } else if (obj instanceof Double) {
            this.f9364d = null;
            this.f9365e = null;
            this.f9368h = (Double) obj;
            this.f9366f = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    /* renamed from: a */
    public Object mo12744a() {
        if (this.f9364d != null) {
            return this.f9364d;
        }
        if (this.f9368h != null) {
            return this.f9368h;
        }
        if (this.f9366f != null) {
            return this.f9366f;
        }
        return null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        avk.m5963a(this, parcel, i);
    }
}
