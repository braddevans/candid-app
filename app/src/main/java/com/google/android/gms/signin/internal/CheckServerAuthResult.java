package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class CheckServerAuthResult extends AbstractSafeParcelable {
    public static final Creator<CheckServerAuthResult> CREATOR = new awm();

    /* renamed from: a */
    public final int f9391a;

    /* renamed from: b */
    public final boolean f9392b;

    /* renamed from: c */
    public final List<Scope> f9393c;

    public CheckServerAuthResult(int i, boolean z, List<Scope> list) {
        this.f9391a = i;
        this.f9392b = z;
        this.f9393c = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        awm.m6547a(this, parcel, i);
    }
}
