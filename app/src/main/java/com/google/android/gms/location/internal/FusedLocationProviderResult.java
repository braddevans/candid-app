package com.google.android.gms.location.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class FusedLocationProviderResult extends AbstractSafeParcelable implements alf {
    public static final Creator<FusedLocationProviderResult> CREATOR = new aqp();

    /* renamed from: a */
    public static final FusedLocationProviderResult f9081a = new FusedLocationProviderResult(Status.f8890a);

    /* renamed from: b */
    private final int f9082b;

    /* renamed from: c */
    private final Status f9083c;

    public FusedLocationProviderResult(int i, Status status) {
        this.f9082b = i;
        this.f9083c = status;
    }

    public FusedLocationProviderResult(Status status) {
        this(1, status);
    }

    /* renamed from: a */
    public Status mo1166a() {
        return this.f9083c;
    }

    /* renamed from: b */
    public int mo12446b() {
        return this.f9082b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqp.m4687a(this, parcel, i);
    }
}
