package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ResolveAccountResponse extends AbstractSafeParcelable {
    public static final Creator<ResolveAccountResponse> CREATOR = new aly();

    /* renamed from: a */
    public final int f8937a;

    /* renamed from: b */
    public IBinder f8938b;

    /* renamed from: c */
    private ConnectionResult f8939c;

    /* renamed from: d */
    private boolean f8940d;

    /* renamed from: e */
    private boolean f8941e;

    public ResolveAccountResponse(int i, IBinder iBinder, ConnectionResult connectionResult, boolean z, boolean z2) {
        this.f8937a = i;
        this.f8938b = iBinder;
        this.f8939c = connectionResult;
        this.f8940d = z;
        this.f8941e = z2;
    }

    /* renamed from: a */
    public amq mo12253a() {
        return C0349a.m2919a(this.f8938b);
    }

    /* renamed from: b */
    public ConnectionResult mo12254b() {
        return this.f8939c;
    }

    /* renamed from: c */
    public boolean mo12255c() {
        return this.f8940d;
    }

    /* renamed from: d */
    public boolean mo12256d() {
        return this.f8941e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ResolveAccountResponse)) {
            return false;
        }
        ResolveAccountResponse resolveAccountResponse = (ResolveAccountResponse) obj;
        return this.f8939c.equals(resolveAccountResponse.f8939c) && mo12253a().equals(resolveAccountResponse.mo12253a());
    }

    public void writeToParcel(Parcel parcel, int i) {
        aly.m2737a(this, parcel, i);
    }
}
