package com.google.android.gms.measurement.internal;

import android.content.Context;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

public class zzw extends avb {
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static final AtomicLong f9369j = new AtomicLong(Long.MIN_VALUE);
    /* access modifiers changed from: private */

    /* renamed from: a */
    public C2189c f9370a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C2189c f9371b;

    /* renamed from: c */
    private final PriorityBlockingQueue<FutureTask<?>> f9372c = new PriorityBlockingQueue<>();

    /* renamed from: d */
    private final BlockingQueue<FutureTask<?>> f9373d = new LinkedBlockingQueue();

    /* renamed from: e */
    private final UncaughtExceptionHandler f9374e = new C2187a("Thread death: Uncaught exception on worker thread");

    /* renamed from: f */
    private final UncaughtExceptionHandler f9375f = new C2187a("Thread death: Uncaught exception on network thread");
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final Object f9376g = new Object();
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final Semaphore f9377h = new Semaphore(2);
    /* access modifiers changed from: private */

    /* renamed from: i */
    public volatile boolean f9378i;

    /* renamed from: com.google.android.gms.measurement.internal.zzw$a */
    final class C2187a implements UncaughtExceptionHandler {

        /* renamed from: b */
        private final String f9380b;

        public C2187a(String str) {
            alw.m2723a(str);
            this.f9380b = str;
        }

        public synchronized void uncaughtException(Thread thread, Throwable th) {
            zzw.this.mo7703w().mo7970f().mo7975a(this.f9380b, th);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzw$b */
    final class C2188b<V> extends FutureTask<V> implements Comparable<C2188b> {

        /* renamed from: b */
        private final long f9382b = zzw.f9369j.getAndIncrement();

        /* renamed from: c */
        private final boolean f9383c;

        /* renamed from: d */
        private final String f9384d;

        C2188b(Runnable runnable, boolean z, String str) {
            super(runnable, null);
            alw.m2723a(str);
            this.f9384d = str;
            this.f9383c = z;
            if (this.f9382b == Long.MAX_VALUE) {
                zzw.this.mo7703w().mo7970f().mo7974a("Tasks index overflow");
            }
        }

        C2188b(Callable<V> callable, boolean z, String str) {
            super(callable);
            alw.m2723a(str);
            this.f9384d = str;
            this.f9383c = z;
            if (this.f9382b == Long.MAX_VALUE) {
                zzw.this.mo7703w().mo7970f().mo7974a("Tasks index overflow");
            }
        }

        /* renamed from: a */
        public int compareTo(C2188b bVar) {
            if (this.f9383c != bVar.f9383c) {
                return this.f9383c ? -1 : 1;
            }
            if (this.f9382b < bVar.f9382b) {
                return -1;
            }
            if (this.f9382b > bVar.f9382b) {
                return 1;
            }
            zzw.this.mo7703w().mo7971g().mo7975a("Two tasks share the same index. index", Long.valueOf(this.f9382b));
            return 0;
        }

        /* access modifiers changed from: protected */
        public void setException(Throwable th) {
            zzw.this.mo7703w().mo7970f().mo7975a(this.f9384d, th);
            if (th instanceof zza) {
                Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
            }
            super.setException(th);
        }
    }

    /* renamed from: com.google.android.gms.measurement.internal.zzw$c */
    final class C2189c extends Thread {

        /* renamed from: b */
        private final Object f9386b = new Object();

        /* renamed from: c */
        private final BlockingQueue<FutureTask<?>> f9387c;

        public C2189c(String str, BlockingQueue<FutureTask<?>> blockingQueue) {
            alw.m2723a(str);
            alw.m2723a(blockingQueue);
            this.f9387c = blockingQueue;
            setName(str);
        }

        /* renamed from: a */
        private void m12033a(InterruptedException interruptedException) {
            zzw.this.mo7703w().mo7972z().mo7975a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
        }

        /* renamed from: a */
        public void mo12754a() {
            synchronized (this.f9386b) {
                this.f9386b.notifyAll();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0078, code lost:
            r1 = com.google.android.gms.measurement.internal.zzw.m12005c(r4.f9385a);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x007e, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            com.google.android.gms.measurement.internal.zzw.m12000a(r4.f9385a).release();
            com.google.android.gms.measurement.internal.zzw.m12005c(r4.f9385a).notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0097, code lost:
            if (r4 != com.google.android.gms.measurement.internal.zzw.m12006d(r4.f9385a)) goto L_0x00a9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0099, code lost:
            com.google.android.gms.measurement.internal.zzw.m11999a(r4.f9385a, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x009f, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a0, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x00af, code lost:
            if (r4 != com.google.android.gms.measurement.internal.zzw.m12007e(r4.f9385a)) goto L_0x00bb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b1, code lost:
            com.google.android.gms.measurement.internal.zzw.m12003b(r4.f9385a, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
            r4.f9385a.mo7703w().mo7970f().mo7974a("Current scheduler thread is neither worker nor network");
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r0 = 0
                r1 = r0
            L_0x0002:
                if (r1 != 0) goto L_0x0015
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ InterruptedException -> 0x0010 }
                java.util.concurrent.Semaphore r0 = r0.f9377h     // Catch:{ InterruptedException -> 0x0010 }
                r0.acquire()     // Catch:{ InterruptedException -> 0x0010 }
                r0 = 1
                r1 = r0
                goto L_0x0002
            L_0x0010:
                r0 = move-exception
                r4.m12033a(r0)
                goto L_0x0002
            L_0x0015:
                java.util.concurrent.BlockingQueue<java.util.concurrent.FutureTask<?>> r0 = r4.f9387c     // Catch:{ all -> 0x0023 }
                java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0023 }
                java.util.concurrent.FutureTask r0 = (java.util.concurrent.FutureTask) r0     // Catch:{ all -> 0x0023 }
                if (r0 == 0) goto L_0x004d
                r0.run()     // Catch:{ all -> 0x0023 }
                goto L_0x0015
            L_0x0023:
                r0 = move-exception
                com.google.android.gms.measurement.internal.zzw r1 = com.google.android.gms.measurement.internal.zzw.this
                java.lang.Object r1 = r1.f9376g
                monitor-enter(r1)
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                java.util.concurrent.Semaphore r2 = r2.f9377h     // Catch:{ all -> 0x00e1 }
                r2.release()     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                java.lang.Object r2 = r2.f9376g     // Catch:{ all -> 0x00e1 }
                r2.notifyAll()     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw$c r2 = r2.f9370a     // Catch:{ all -> 0x00e1 }
                if (r4 != r2) goto L_0x00d1
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                r3 = 0
                r2.f9370a = r3     // Catch:{ all -> 0x00e1 }
            L_0x004b:
                monitor-exit(r1)     // Catch:{ all -> 0x00e1 }
                throw r0
            L_0x004d:
                java.lang.Object r1 = r4.f9386b     // Catch:{ all -> 0x0023 }
                monitor-enter(r1)     // Catch:{ all -> 0x0023 }
                java.util.concurrent.BlockingQueue<java.util.concurrent.FutureTask<?>> r0 = r4.f9387c     // Catch:{ all -> 0x00a6 }
                java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00a6 }
                if (r0 != 0) goto L_0x0067
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00a6 }
                boolean r0 = r0.f9378i     // Catch:{ all -> 0x00a6 }
                if (r0 != 0) goto L_0x0067
                java.lang.Object r0 = r4.f9386b     // Catch:{ InterruptedException -> 0x00a1 }
                r2 = 30000(0x7530, double:1.4822E-319)
                r0.wait(r2)     // Catch:{ InterruptedException -> 0x00a1 }
            L_0x0067:
                monitor-exit(r1)     // Catch:{ all -> 0x00a6 }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x0023 }
                java.lang.Object r1 = r0.f9376g     // Catch:{ all -> 0x0023 }
                monitor-enter(r1)     // Catch:{ all -> 0x0023 }
                java.util.concurrent.BlockingQueue<java.util.concurrent.FutureTask<?>> r0 = r4.f9387c     // Catch:{ all -> 0x00ce }
                java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00ce }
                if (r0 != 0) goto L_0x00cb
                monitor-exit(r1)     // Catch:{ all -> 0x00ce }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this
                java.lang.Object r1 = r0.f9376g
                monitor-enter(r1)
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                java.util.concurrent.Semaphore r0 = r0.f9377h     // Catch:{ all -> 0x00b8 }
                r0.release()     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                java.lang.Object r0 = r0.f9376g     // Catch:{ all -> 0x00b8 }
                r0.notifyAll()     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw$c r0 = r0.f9370a     // Catch:{ all -> 0x00b8 }
                if (r4 != r0) goto L_0x00a9
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                r2 = 0
                r0.f9370a = r2     // Catch:{ all -> 0x00b8 }
            L_0x009f:
                monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
                return
            L_0x00a1:
                r0 = move-exception
                r4.m12033a(r0)     // Catch:{ all -> 0x00a6 }
                goto L_0x0067
            L_0x00a6:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00a6 }
                throw r0     // Catch:{ all -> 0x0023 }
            L_0x00a9:
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw$c r0 = r0.f9371b     // Catch:{ all -> 0x00b8 }
                if (r4 != r0) goto L_0x00bb
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                r2 = 0
                r0.f9371b = r2     // Catch:{ all -> 0x00b8 }
                goto L_0x009f
            L_0x00b8:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
                throw r0
            L_0x00bb:
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                awb r0 = r0.mo7703w()     // Catch:{ all -> 0x00b8 }
                awb$a r0 = r0.mo7970f()     // Catch:{ all -> 0x00b8 }
                java.lang.String r2 = "Current scheduler thread is neither worker nor network"
                r0.mo7974a(r2)     // Catch:{ all -> 0x00b8 }
                goto L_0x009f
            L_0x00cb:
                monitor-exit(r1)     // Catch:{ all -> 0x00ce }
                goto L_0x0015
            L_0x00ce:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00ce }
                throw r0     // Catch:{ all -> 0x0023 }
            L_0x00d1:
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw$c r2 = r2.f9371b     // Catch:{ all -> 0x00e1 }
                if (r4 != r2) goto L_0x00e4
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                r3 = 0
                r2.f9371b = r3     // Catch:{ all -> 0x00e1 }
                goto L_0x004b
            L_0x00e1:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00e1 }
                throw r0
            L_0x00e4:
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                awb r2 = r2.mo7703w()     // Catch:{ all -> 0x00e1 }
                awb$a r2 = r2.mo7970f()     // Catch:{ all -> 0x00e1 }
                java.lang.String r3 = "Current scheduler thread is neither worker nor network"
                r2.mo7974a(r3)     // Catch:{ all -> 0x00e1 }
                goto L_0x004b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzw.C2189c.run():void");
        }
    }

    static class zza extends RuntimeException {
    }

    public zzw(awi awi) {
        super(awi);
    }

    /* renamed from: a */
    private void m12001a(C2188b<?> bVar) {
        synchronized (this.f9376g) {
            this.f9372c.add(bVar);
            if (this.f9370a == null) {
                this.f9370a = new C2189c("Measurement Worker", this.f9372c);
                this.f9370a.setUncaughtExceptionHandler(this.f9374e);
                this.f9370a.start();
            } else {
                this.f9370a.mo12754a();
            }
        }
    }

    /* renamed from: a */
    private void m12002a(FutureTask<?> futureTask) {
        synchronized (this.f9376g) {
            this.f9373d.add(futureTask);
            if (this.f9371b == null) {
                this.f9371b = new C2189c("Measurement Network", this.f9373d);
                this.f9371b.setUncaughtExceptionHandler(this.f9375f);
                this.f9371b.start();
            } else {
                this.f9371b.mo12754a();
            }
        }
    }

    /* renamed from: a */
    public <V> Future<V> mo12746a(Callable<V> callable) throws IllegalStateException {
        mo7657c();
        alw.m2723a(callable);
        C2188b bVar = new C2188b(callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.f9370a) {
            bVar.run();
        } else {
            m12001a(bVar);
        }
        return bVar;
    }

    /* renamed from: a */
    public void mo12747a(Runnable runnable) throws IllegalStateException {
        mo7657c();
        alw.m2723a(runnable);
        m12001a(new C2188b<>(runnable, false, "Task exception on worker thread"));
    }

    /* renamed from: b */
    public <V> Future<V> mo12748b(Callable<V> callable) throws IllegalStateException {
        mo7657c();
        alw.m2723a(callable);
        C2188b bVar = new C2188b(callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.f9370a) {
            bVar.run();
        } else {
            m12001a(bVar);
        }
        return bVar;
    }

    /* renamed from: b */
    public void mo12749b(Runnable runnable) throws IllegalStateException {
        mo7657c();
        alw.m2723a(runnable);
        m12002a((FutureTask<?>) new C2188b<Object>(runnable, false, "Task exception on network thread"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo7659e() {
    }

    /* renamed from: h */
    public /* bridge */ /* synthetic */ void mo7688h() {
        super.mo7688h();
    }

    /* renamed from: i */
    public void mo7689i() {
        if (Thread.currentThread() != this.f9371b) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    /* renamed from: j */
    public void mo7690j() {
        if (Thread.currentThread() != this.f9370a) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    /* renamed from: k */
    public /* bridge */ /* synthetic */ avo mo7691k() {
        return super.mo7691k();
    }

    /* renamed from: l */
    public /* bridge */ /* synthetic */ avd mo7692l() {
        return super.mo7692l();
    }

    /* renamed from: m */
    public /* bridge */ /* synthetic */ avz mo7693m() {
        return super.mo7693m();
    }

    /* renamed from: n */
    public /* bridge */ /* synthetic */ avs mo7694n() {
        return super.mo7694n();
    }

    /* renamed from: o */
    public /* bridge */ /* synthetic */ ave mo7695o() {
        return super.mo7695o();
    }

    /* renamed from: p */
    public /* bridge */ /* synthetic */ ane mo7696p() {
        return super.mo7696p();
    }

    /* renamed from: q */
    public /* bridge */ /* synthetic */ Context mo7697q() {
        return super.mo7697q();
    }

    /* renamed from: r */
    public /* bridge */ /* synthetic */ avq mo7698r() {
        return super.mo7698r();
    }

    /* renamed from: s */
    public /* bridge */ /* synthetic */ avm mo7699s() {
        return super.mo7699s();
    }

    /* renamed from: t */
    public /* bridge */ /* synthetic */ awh mo7700t() {
        return super.mo7700t();
    }

    /* renamed from: u */
    public /* bridge */ /* synthetic */ avg mo7701u() {
        return super.mo7701u();
    }

    /* renamed from: v */
    public /* bridge */ /* synthetic */ zzw mo7702v() {
        return super.mo7702v();
    }

    /* renamed from: w */
    public /* bridge */ /* synthetic */ awb mo7703w() {
        return super.mo7703w();
    }

    /* renamed from: x */
    public /* bridge */ /* synthetic */ awf mo7704x() {
        return super.mo7704x();
    }

    /* renamed from: y */
    public /* bridge */ /* synthetic */ avp mo7705y() {
        return super.mo7705y();
    }
}
