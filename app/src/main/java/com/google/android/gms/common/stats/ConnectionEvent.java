package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class ConnectionEvent extends StatsEvent {
    public static final Creator<ConnectionEvent> CREATOR = new amx();

    /* renamed from: a */
    public final int f8952a;

    /* renamed from: b */
    private final long f8953b;

    /* renamed from: c */
    private int f8954c;

    /* renamed from: d */
    private final String f8955d;

    /* renamed from: e */
    private final String f8956e;

    /* renamed from: f */
    private final String f8957f;

    /* renamed from: g */
    private final String f8958g;

    /* renamed from: h */
    private final String f8959h;

    /* renamed from: i */
    private final String f8960i;

    /* renamed from: j */
    private final long f8961j;

    /* renamed from: k */
    private final long f8962k;

    /* renamed from: l */
    private long f8963l;

    public ConnectionEvent(int i, long j, int i2, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3) {
        this.f8952a = i;
        this.f8953b = j;
        this.f8954c = i2;
        this.f8955d = str;
        this.f8956e = str2;
        this.f8957f = str3;
        this.f8958g = str4;
        this.f8963l = -1;
        this.f8959h = str5;
        this.f8960i = str6;
        this.f8961j = j2;
        this.f8962k = j3;
    }

    public ConnectionEvent(long j, int i, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3) {
        this(1, j, i, str, str2, str3, str4, str5, str6, j2, j3);
    }

    /* renamed from: a */
    public long mo12270a() {
        return this.f8953b;
    }

    /* renamed from: b */
    public int mo12271b() {
        return this.f8954c;
    }

    /* renamed from: c */
    public String mo12272c() {
        return this.f8955d;
    }

    /* renamed from: d */
    public String mo12273d() {
        return this.f8956e;
    }

    /* renamed from: e */
    public String mo12274e() {
        return this.f8957f;
    }

    /* renamed from: f */
    public String mo12275f() {
        return this.f8958g;
    }

    /* renamed from: g */
    public String mo12276g() {
        return this.f8959h;
    }

    /* renamed from: h */
    public String mo12277h() {
        return this.f8960i;
    }

    /* renamed from: i */
    public long mo12278i() {
        return this.f8963l;
    }

    /* renamed from: j */
    public long mo12279j() {
        return this.f8962k;
    }

    /* renamed from: k */
    public long mo12280k() {
        return this.f8961j;
    }

    /* renamed from: l */
    public String mo12281l() {
        String valueOf = String.valueOf("\t");
        String valueOf2 = String.valueOf(mo12272c());
        String valueOf3 = String.valueOf(mo12273d());
        String valueOf4 = String.valueOf("\t");
        String valueOf5 = String.valueOf(mo12274e());
        String valueOf6 = String.valueOf(mo12275f());
        String valueOf7 = String.valueOf("\t");
        String str = this.f8959h == null ? "" : this.f8959h;
        String valueOf8 = String.valueOf("\t");
        return new StringBuilder(String.valueOf(valueOf).length() + 22 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length() + String.valueOf(valueOf5).length() + String.valueOf(valueOf6).length() + String.valueOf(valueOf7).length() + String.valueOf(str).length() + String.valueOf(valueOf8).length()).append(valueOf).append(valueOf2).append("/").append(valueOf3).append(valueOf4).append(valueOf5).append("/").append(valueOf6).append(valueOf7).append(str).append(valueOf8).append(mo12279j()).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        amx.m3028a(this, parcel, i);
    }
}
