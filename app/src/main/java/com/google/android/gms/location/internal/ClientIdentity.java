package com.google.android.gms.location.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ClientIdentity extends AbstractSafeParcelable {
    public static final aqn CREATOR = new aqn();

    /* renamed from: a */
    public final int f9078a;

    /* renamed from: b */
    public final String f9079b;

    /* renamed from: c */
    private final int f9080c;

    public ClientIdentity(int i, int i2, String str) {
        this.f9080c = i;
        this.f9078a = i2;
        this.f9079b = str;
    }

    /* renamed from: a */
    public int mo12441a() {
        return this.f9080c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof ClientIdentity)) {
            return false;
        }
        ClientIdentity clientIdentity = (ClientIdentity) obj;
        return clientIdentity.f9078a == this.f9078a && alv.m2721a(clientIdentity.f9079b, this.f9079b);
    }

    public int hashCode() {
        return this.f9078a;
    }

    public String toString() {
        return String.format("%d:%s", new Object[]{Integer.valueOf(this.f9078a), this.f9079b});
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqn.m4684a(this, parcel, i);
    }
}
