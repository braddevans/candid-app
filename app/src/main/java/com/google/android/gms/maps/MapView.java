package com.google.android.gms.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import java.util.ArrayList;
import java.util.List;

public class MapView extends FrameLayout {

    /* renamed from: a */
    private final C2171b f9178a;

    /* renamed from: com.google.android.gms.maps.MapView$a */
    static class C2169a implements ask {

        /* renamed from: a */
        private final ViewGroup f9179a;

        /* renamed from: b */
        private final ase f9180b;

        /* renamed from: c */
        private View f9181c;

        public C2169a(ViewGroup viewGroup, ase ase) {
            this.f9180b = (ase) alw.m2723a(ase);
            this.f9179a = (ViewGroup) alw.m2723a(viewGroup);
        }

        /* renamed from: a */
        public View mo6484a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            throw new UnsupportedOperationException("onCreateView not allowed on MapViewDelegate");
        }

        /* renamed from: a */
        public void mo6485a() {
            try {
                this.f9180b.mo7157h();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo6486a(Activity activity, Bundle bundle, Bundle bundle2) {
            throw new UnsupportedOperationException("onInflate not allowed on MapViewDelegate");
        }

        /* renamed from: a */
        public void mo6487a(Bundle bundle) {
            try {
                this.f9180b.mo7147a(bundle);
                this.f9181c = (View) any.m4134a(this.f9180b.mo7155f());
                this.f9179a.removeAllViews();
                this.f9179a.addView(this.f9181c);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo12560a(final arx arx) {
            try {
                this.f9180b.mo7148a((ato) new C0852a() {
                    /* renamed from: a */
                    public void mo7344a(asb asb) throws RemoteException {
                        arx.mo7023a(new arv(asb));
                    }
                });
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6488b() {
            try {
                this.f9180b.mo7149b();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6489b(Bundle bundle) {
            try {
                this.f9180b.mo7150b(bundle);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: c */
        public void mo6490c() {
            try {
                this.f9180b.mo7151c();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: d */
        public void mo6491d() {
            try {
                this.f9180b.mo7158i();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: e */
        public void mo6492e() {
            throw new UnsupportedOperationException("onDestroyView not allowed on MapViewDelegate");
        }

        /* renamed from: f */
        public void mo6493f() {
            try {
                this.f9180b.mo7153d();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: g */
        public void mo6494g() {
            try {
                this.f9180b.mo7154e();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
    }

    /* renamed from: com.google.android.gms.maps.MapView$b */
    static class C2171b extends anw<C2169a> {

        /* renamed from: a */
        protected anz<C2169a> f9184a;

        /* renamed from: b */
        private final ViewGroup f9185b;

        /* renamed from: c */
        private final Context f9186c;

        /* renamed from: d */
        private final GoogleMapOptions f9187d;

        /* renamed from: e */
        private final List<arx> f9188e = new ArrayList();

        C2171b(ViewGroup viewGroup, Context context, GoogleMapOptions googleMapOptions) {
            this.f9185b = viewGroup;
            this.f9186c = context;
            this.f9187d = googleMapOptions;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo6500a(anz<C2169a> anz) {
            this.f9184a = anz;
            mo12561i();
        }

        /* renamed from: i */
        public void mo12561i() {
            if (this.f9184a != null && mo6496a() == null) {
                try {
                    arw.m4857a(this.f9186c);
                    ase a = asv.m5273a(this.f9186c).mo7269a(any.m4133a(this.f9186c), this.f9187d);
                    if (a != null) {
                        this.f9184a.mo6509a(new C2169a(this.f9185b, a));
                        for (arx a2 : this.f9188e) {
                            ((C2169a) mo6496a()).mo12560a(a2);
                        }
                        this.f9188e.clear();
                    }
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public MapView(Context context) {
        super(context);
        this.f9178a = new C2171b(this, context, null);
        m11839a();
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f9178a = new C2171b(this, context, GoogleMapOptions.m11788a(context, attributeSet));
        m11839a();
    }

    public MapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f9178a = new C2171b(this, context, GoogleMapOptions.m11788a(context, attributeSet));
        m11839a();
    }

    /* renamed from: a */
    private void m11839a() {
        setClickable(true);
    }
}
