package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class ConnectionResult extends AbstractSafeParcelable {
    public static final Creator<ConnectionResult> CREATOR = new anq();

    /* renamed from: a */
    public static final ConnectionResult f8872a = new ConnectionResult(0);

    /* renamed from: b */
    public final int f8873b;

    /* renamed from: c */
    private final int f8874c;

    /* renamed from: d */
    private final PendingIntent f8875d;

    /* renamed from: e */
    private final String f8876e;

    public ConnectionResult(int i) {
        this(i, null, null);
    }

    public ConnectionResult(int i, int i2, PendingIntent pendingIntent, String str) {
        this.f8873b = i;
        this.f8874c = i2;
        this.f8875d = pendingIntent;
        this.f8876e = str;
    }

    public ConnectionResult(int i, PendingIntent pendingIntent) {
        this(i, pendingIntent, null);
    }

    public ConnectionResult(int i, PendingIntent pendingIntent, String str) {
        this(1, i, pendingIntent, str);
    }

    /* renamed from: a */
    public static String m11455a(int i) {
        switch (i) {
            case -1:
                return "UNKNOWN";
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            case 11:
                return "LICENSE_CHECK_FAILED";
            case 13:
                return "CANCELED";
            case 14:
                return "TIMEOUT";
            case 15:
                return "INTERRUPTED";
            case 16:
                return "API_UNAVAILABLE";
            case 17:
                return "SIGN_IN_FAILED";
            case 18:
                return "SERVICE_UPDATING";
            case 19:
                return "SERVICE_MISSING_PERMISSION";
            case 20:
                return "RESTRICTED_PROFILE";
            case 21:
                return "API_VERSION_UPDATE_REQUIRED";
            case 42:
                return "UPDATE_ANDROID_WEAR";
            case 99:
                return "UNFINISHED";
            case 1500:
                return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
            default:
                return "UNKNOWN_ERROR_CODE(" + i + ")";
        }
    }

    /* renamed from: a */
    public boolean mo12189a() {
        return (this.f8874c == 0 || this.f8875d == null) ? false : true;
    }

    /* renamed from: b */
    public boolean mo12190b() {
        return this.f8874c == 0;
    }

    /* renamed from: c */
    public int mo12191c() {
        return this.f8874c;
    }

    /* renamed from: d */
    public PendingIntent mo12192d() {
        return this.f8875d;
    }

    /* renamed from: e */
    public String mo12193e() {
        return this.f8876e;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ConnectionResult)) {
            return false;
        }
        ConnectionResult connectionResult = (ConnectionResult) obj;
        return this.f8874c == connectionResult.f8874c && alv.m2721a(this.f8875d, connectionResult.f8875d) && alv.m2721a(this.f8876e, connectionResult.f8876e);
    }

    public int hashCode() {
        return alv.m2719a(Integer.valueOf(this.f8874c), this.f8875d, this.f8876e);
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("statusCode", m11455a(this.f8874c)).mo1191a("resolution", this.f8875d).mo1191a("message", this.f8876e).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        anq.m4061a(this, parcel, i);
    }
}
