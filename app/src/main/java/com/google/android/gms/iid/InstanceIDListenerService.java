package com.google.android.gms.iid;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.p001v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class InstanceIDListenerService extends Service {

    /* renamed from: a */
    static String f8979a = "action";

    /* renamed from: f */
    private static String f8980f = "google.com/iid";

    /* renamed from: g */
    private static String f8981g = "CMD";

    /* renamed from: h */
    private static String f8982h = "gcm.googleapis.com/refresh";

    /* renamed from: b */
    MessengerCompat f8983b = new MessengerCompat((Handler) new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            InstanceIDListenerService.this.m11596a(message, MessengerCompat.m11603a(message));
        }
    });

    /* renamed from: c */
    BroadcastReceiver f8984c = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (Log.isLoggable("InstanceID", 3)) {
                intent.getStringExtra("registration_id");
                String valueOf = String.valueOf(intent.getExtras());
                Log.d("InstanceID", new StringBuilder(String.valueOf(valueOf).length() + 46).append("Received GSF callback using dynamic receiver: ").append(valueOf).toString());
            }
            InstanceIDListenerService.this.mo12298a(intent);
            InstanceIDListenerService.this.mo12300b();
        }
    };

    /* renamed from: d */
    int f8985d;

    /* renamed from: e */
    int f8986e;

    /* renamed from: a */
    public static void m11594a(Context context) {
        Intent intent = new Intent("com.google.android.gms.iid.InstanceID");
        intent.setPackage(context.getPackageName());
        intent.putExtra(f8981g, "SYNC");
        context.startService(intent);
    }

    /* renamed from: a */
    public static void m11595a(Context context, aog aog) {
        aog.mo6607b();
        Intent intent = new Intent("com.google.android.gms.iid.InstanceID");
        intent.putExtra(f8981g, "RST");
        intent.setPackage(context.getPackageName());
        context.startService(intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m11596a(Message message, int i) {
        aof.m4211a((Context) this);
        getPackageManager();
        if (i == aof.f3748c || i == aof.f3747b) {
            mo12298a((Intent) message.obj);
            return;
        }
        int i2 = aof.f3747b;
        Log.w("InstanceID", "Message from unexpected caller " + i + " mine=" + i2 + " appid=" + aof.f3748c);
    }

    /* renamed from: a */
    public void mo10691a() {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo12297a(int i) {
        synchronized (this) {
            this.f8985d++;
            if (i > this.f8986e) {
                this.f8986e = i;
            }
        }
    }

    /* renamed from: a */
    public void mo12298a(Intent intent) {
        aoc a;
        String stringExtra = intent.getStringExtra("subtype");
        if (stringExtra == null) {
            a = aoc.m4198c(this);
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("subtype", stringExtra);
            a = aoc.m4194a((Context) this, bundle);
        }
        String stringExtra2 = intent.getStringExtra(f8981g);
        if (intent.getStringExtra("error") == null && intent.getStringExtra("registration_id") == null) {
            if (Log.isLoggable("InstanceID", 3)) {
                String valueOf = String.valueOf(intent.getExtras());
                Log.d("InstanceID", new StringBuilder(String.valueOf(stringExtra).length() + 18 + String.valueOf(stringExtra2).length() + String.valueOf(valueOf).length()).append("Service command ").append(stringExtra).append(" ").append(stringExtra2).append(" ").append(valueOf).toString());
            }
            if (intent.getStringExtra("unregistered") != null) {
                aog c = a.mo6583c();
                if (stringExtra == null) {
                    stringExtra = "";
                }
                c.mo6611e(stringExtra);
                a.mo6584d().mo6598d(intent);
            } else if (f8982h.equals(intent.getStringExtra("from"))) {
                a.mo6583c().mo6611e(stringExtra);
                mo12299a(false);
            } else if ("RST".equals(stringExtra2)) {
                a.mo6582b();
                mo12299a(true);
            } else if ("RST_FULL".equals(stringExtra2)) {
                if (!a.mo6583c().mo6606a()) {
                    a.mo6583c().mo6607b();
                    mo12299a(true);
                }
            } else if ("SYNC".equals(stringExtra2)) {
                a.mo6583c().mo6611e(stringExtra);
                mo12299a(false);
            } else {
                if ("PING".equals(stringExtra2)) {
                }
            }
        } else {
            if (Log.isLoggable("InstanceID", 3)) {
                String str = "InstanceID";
                String str2 = "Register result in service ";
                String valueOf2 = String.valueOf(stringExtra);
                Log.d(str, valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
            }
            a.mo6584d().mo6598d(intent);
        }
    }

    /* renamed from: a */
    public void mo12299a(boolean z) {
        mo10691a();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo12300b() {
        synchronized (this) {
            this.f8985d--;
            if (this.f8985d == 0) {
                stopSelf(this.f8986e);
            }
            if (Log.isLoggable("InstanceID", 3)) {
                int i = this.f8985d;
                Log.d("InstanceID", "Stop " + i + " " + this.f8986e);
            }
        }
    }

    public IBinder onBind(Intent intent) {
        if (intent == null || !"com.google.android.gms.iid.InstanceID".equals(intent.getAction())) {
            return null;
        }
        return this.f8983b.mo12307a();
    }

    public void onCreate() {
        IntentFilter intentFilter = new IntentFilter("com.google.android.c2dm.intent.REGISTRATION");
        intentFilter.addCategory(getPackageName());
        registerReceiver(this.f8984c, intentFilter, "com.google.android.c2dm.permission.RECEIVE", null);
    }

    public void onDestroy() {
        unregisterReceiver(this.f8984c);
    }

    /* JADX INFO: finally extract failed */
    public int onStartCommand(Intent intent, int i, int i2) {
        mo12297a(i2);
        if (intent == null) {
            mo12300b();
            return 2;
        }
        try {
            if ("com.google.android.gms.iid.InstanceID".equals(intent.getAction())) {
                if (VERSION.SDK_INT <= 18) {
                    Intent intent2 = (Intent) intent.getParcelableExtra("GSF");
                    if (intent2 != null) {
                        startService(intent2);
                        mo12300b();
                        return 1;
                    }
                }
                mo12298a(intent);
            }
            mo12300b();
            if (intent.getStringExtra("from") != null) {
                WakefulBroadcastReceiver.m3705a(intent);
            }
            return 2;
        } catch (Throwable th) {
            mo12300b();
            throw th;
        }
    }
}
