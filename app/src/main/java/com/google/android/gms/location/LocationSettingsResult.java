package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class LocationSettingsResult extends AbstractSafeParcelable implements alf {
    public static final Creator<LocationSettingsResult> CREATOR = new ars();

    /* renamed from: a */
    private final int f9068a;

    /* renamed from: b */
    private final Status f9069b;

    /* renamed from: c */
    private final LocationSettingsStates f9070c;

    public LocationSettingsResult(int i, Status status, LocationSettingsStates locationSettingsStates) {
        this.f9068a = i;
        this.f9069b = status;
        this.f9070c = locationSettingsStates;
    }

    /* renamed from: a */
    public Status mo1166a() {
        return this.f9069b;
    }

    /* renamed from: b */
    public int mo12430b() {
        return this.f9068a;
    }

    /* renamed from: c */
    public LocationSettingsStates mo12431c() {
        return this.f9070c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ars.m4850a(this, parcel, i);
    }
}
