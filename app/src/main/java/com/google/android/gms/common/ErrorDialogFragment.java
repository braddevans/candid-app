package com.google.android.gms.common;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;

@TargetApi(11)
public class ErrorDialogFragment extends DialogFragment {

    /* renamed from: a */
    private Dialog f8877a = null;

    /* renamed from: b */
    private OnCancelListener f8878b = null;

    /* renamed from: a */
    public static ErrorDialogFragment m11461a(Dialog dialog, OnCancelListener onCancelListener) {
        ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        Dialog dialog2 = (Dialog) alw.m2724a(dialog, (Object) "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        errorDialogFragment.f8877a = dialog2;
        if (onCancelListener != null) {
            errorDialogFragment.f8878b = onCancelListener;
        }
        return errorDialogFragment;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.f8878b != null) {
            this.f8878b.onCancel(dialogInterface);
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f8877a == null) {
            setShowsDialog(false);
        }
        return this.f8877a;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
