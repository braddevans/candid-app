package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public final class AppMeasurementInstallReferrerReceiver extends BroadcastReceiver implements C0928a {

    /* renamed from: a */
    private awg f9337a;

    /* renamed from: a */
    private awg m11986a() {
        if (this.f9337a == null) {
            this.f9337a = new awg(this);
        }
        return this.f9337a;
    }

    /* renamed from: a */
    public void mo8017a(Context context, Intent intent) {
    }

    public void onReceive(Context context, Intent intent) {
        m11986a().mo8015a(context, intent);
    }
}
