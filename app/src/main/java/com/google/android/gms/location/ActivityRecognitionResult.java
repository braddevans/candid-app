package com.google.android.gms.location;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class ActivityRecognitionResult extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final aqb CREATOR = new aqb();

    /* renamed from: a */
    public List<DetectedActivity> f9016a;

    /* renamed from: b */
    public long f9017b;

    /* renamed from: c */
    public long f9018c;

    /* renamed from: d */
    public int f9019d;

    /* renamed from: e */
    public Bundle f9020e;

    /* renamed from: f */
    private final int f9021f;

    public ActivityRecognitionResult(int i, List<DetectedActivity> list, long j, long j2, int i2, Bundle bundle) {
        this.f9021f = i;
        this.f9016a = list;
        this.f9017b = j;
        this.f9018c = j2;
        this.f9019d = i2;
        this.f9020e = bundle;
    }

    /* renamed from: a */
    public static boolean m11680a(Intent intent) {
        if (intent == null) {
            return false;
        }
        if (m11685e(intent)) {
            return true;
        }
        List d = m11684d(intent);
        return d != null && !d.isEmpty();
    }

    /* renamed from: a */
    private static boolean m11681a(Bundle bundle, Bundle bundle2) {
        if (bundle == null && bundle2 == null) {
            return true;
        }
        if ((bundle == null && bundle2 != null) || (bundle != null && bundle2 == null)) {
            return false;
        }
        if (bundle.size() != bundle2.size()) {
            return false;
        }
        for (String str : bundle.keySet()) {
            if (!bundle2.containsKey(str)) {
                return false;
            }
            if (bundle.get(str) == null) {
                if (bundle2.get(str) != null) {
                    return false;
                }
            } else if (bundle.get(str) instanceof Bundle) {
                if (!m11681a(bundle.getBundle(str), bundle2.getBundle(str))) {
                    return false;
                }
            } else if (!bundle.get(str).equals(bundle2.get(str))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    public static ActivityRecognitionResult m11682b(Intent intent) {
        ActivityRecognitionResult f = m11686f(intent);
        if (f != null) {
            return f;
        }
        List d = m11684d(intent);
        if (d == null || d.isEmpty()) {
            return null;
        }
        return (ActivityRecognitionResult) d.get(d.size() - 1);
    }

    /* renamed from: c */
    public static boolean m11683c(Intent intent) {
        if (intent == null) {
            return false;
        }
        return intent.hasExtra("com.google.android.location.internal.EXTRA_ACTIVITY_RESULT_LIST");
    }

    /* renamed from: d */
    public static List<ActivityRecognitionResult> m11684d(Intent intent) {
        if (!m11683c(intent)) {
            return null;
        }
        return als.m2713b(intent, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT_LIST", CREATOR);
    }

    /* renamed from: e */
    private static boolean m11685e(Intent intent) {
        if (intent == null) {
            return false;
        }
        return intent.hasExtra("com.google.android.location.internal.EXTRA_ACTIVITY_RESULT");
    }

    /* renamed from: f */
    private static ActivityRecognitionResult m11686f(Intent intent) {
        if (!m11680a(intent)) {
            return null;
        }
        Object obj = intent.getExtras().get("com.google.android.location.internal.EXTRA_ACTIVITY_RESULT");
        if (obj instanceof byte[]) {
            return (ActivityRecognitionResult) als.m2712a((byte[]) obj, CREATOR);
        }
        if (obj instanceof ActivityRecognitionResult) {
            return (ActivityRecognitionResult) obj;
        }
        return null;
    }

    /* renamed from: a */
    public DetectedActivity mo12365a() {
        return (DetectedActivity) this.f9016a.get(0);
    }

    /* renamed from: b */
    public int mo12366b() {
        return this.f9021f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ActivityRecognitionResult activityRecognitionResult = (ActivityRecognitionResult) obj;
        return this.f9017b == activityRecognitionResult.f9017b && this.f9018c == activityRecognitionResult.f9018c && this.f9019d == activityRecognitionResult.f9019d && alv.m2721a(this.f9016a, activityRecognitionResult.f9016a) && m11681a(this.f9020e, activityRecognitionResult.f9020e);
    }

    public int hashCode() {
        return alv.m2719a(Long.valueOf(this.f9017b), Long.valueOf(this.f9018c), Integer.valueOf(this.f9019d), this.f9016a, this.f9020e);
    }

    public String toString() {
        String valueOf = String.valueOf(this.f9016a);
        long j = this.f9017b;
        return new StringBuilder(String.valueOf(valueOf).length() + 124).append("ActivityRecognitionResult [probableActivities=").append(valueOf).append(", timeMillis=").append(j).append(", elapsedRealtimeMillis=").append(this.f9018c).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqb.m4648a(this, parcel, i);
    }
}
