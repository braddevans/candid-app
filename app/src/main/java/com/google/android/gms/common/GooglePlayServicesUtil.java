package com.google.android.gms.common;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.NotificationCompat;
import android.support.p001v4.app.NotificationCompatExtras;
import android.util.Log;
import com.google.android.gms.C2137R;

public final class GooglePlayServicesUtil extends ant {
    public static final String GMS_ERROR_DIALOG = "GooglePlayServicesErrorDialog";
    @Deprecated
    public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
    @Deprecated
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE = ant.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";

    /* renamed from: com.google.android.gms.common.GooglePlayServicesUtil$a */
    static class C2142a extends Handler {

        /* renamed from: a */
        private final Context f8881a;

        C2142a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.f8881a = context.getApplicationContext();
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.f8881a);
                    if (GooglePlayServicesUtil.isUserRecoverableError(isGooglePlayServicesAvailable)) {
                        GooglePlayServicesUtil.zza(isGooglePlayServicesAvailable, this.f8881a);
                        return;
                    }
                    return;
                default:
                    Log.w("GooglePlayServicesUtil", "Don't know how to handle this message: " + message.what);
                    return;
            }
        }
    }

    private GooglePlayServicesUtil() {
    }

    @Deprecated
    public static Dialog getErrorDialog(int i, Activity activity, int i2) {
        return getErrorDialog(i, activity, i2, null);
    }

    @Deprecated
    public static Dialog getErrorDialog(int i, Activity activity, int i2, OnCancelListener onCancelListener) {
        if (zzd(activity, i)) {
            i = 18;
        }
        return aky.m2599a().mo1118a(activity, i, i2, onCancelListener);
    }

    @Deprecated
    public static PendingIntent getErrorPendingIntent(int i, Context context, int i2) {
        return ant.getErrorPendingIntent(i, context, i2);
    }

    @Deprecated
    public static String getErrorString(int i) {
        return ant.getErrorString(i);
    }

    @Deprecated
    public static String getOpenSourceSoftwareLicenseInfo(Context context) {
        return ant.getOpenSourceSoftwareLicenseInfo(context);
    }

    public static Context getRemoteContext(Context context) {
        return ant.getRemoteContext(context);
    }

    public static Resources getRemoteResource(Context context) {
        return ant.getRemoteResource(context);
    }

    @Deprecated
    public static int isGooglePlayServicesAvailable(Context context) {
        return ant.isGooglePlayServicesAvailable(context);
    }

    @Deprecated
    public static boolean isUserRecoverableError(int i) {
        return ant.isUserRecoverableError(i);
    }

    @Deprecated
    public static boolean showErrorDialogFragment(int i, Activity activity, int i2) {
        return showErrorDialogFragment(i, activity, i2, null);
    }

    @Deprecated
    public static boolean showErrorDialogFragment(int i, Activity activity, int i2, OnCancelListener onCancelListener) {
        return showErrorDialogFragment(i, activity, null, i2, onCancelListener);
    }

    public static boolean showErrorDialogFragment(int i, Activity activity, Fragment fragment, int i2, OnCancelListener onCancelListener) {
        if (zzd(activity, i)) {
            i = 18;
        }
        aky a = aky.m2599a();
        if (fragment == null) {
            return a.mo1131b(activity, i, i2, onCancelListener);
        }
        Dialog a2 = a.mo1120a((Context) activity, i, amk.m2870a(fragment, aky.m2599a().mo1124a((Context) activity, i, "d"), i2), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a.mo1126a(activity, a2, GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    @Deprecated
    public static void showErrorNotification(int i, Context context) {
        if (anh.m4036a(context) && i == 2) {
            i = 42;
        }
        if (zzd(context, i) || zze(context, i)) {
            zzbs(context);
        } else {
            zza(i, context);
        }
    }

    /* access modifiers changed from: private */
    public static void zza(int i, Context context) {
        zza(i, context, (String) null);
    }

    public static void zza(int i, Context context, PendingIntent pendingIntent) {
        zza(i, context, null, pendingIntent);
    }

    private static void zza(int i, Context context, String str) {
        zza(i, context, str, aky.m2599a().mo1122a(context, i, 0, "n"));
    }

    @TargetApi(20)
    private static void zza(int i, Context context, String str, PendingIntent pendingIntent) {
        Notification build;
        Notification notification;
        int i2;
        Resources resources = context.getResources();
        String b = amj.m2865b(context, i);
        String d = amj.m2867d(context, i);
        if (anh.m4036a(context)) {
            alw.m2727a(ank.m4046e());
            build = new Builder(context).setSmallIcon(C2137R.C2138drawable.common_ic_googleplayservices).setPriority(2).setAutoCancel(true).setStyle(new BigTextStyle().bigText(new StringBuilder(String.valueOf(b).length() + 1 + String.valueOf(d).length()).append(b).append(" ").append(d).toString())).addAction(C2137R.C2138drawable.common_full_open_on_phone, resources.getString(C2137R.string.common_open_on_phone), pendingIntent).build();
        } else {
            String string = resources.getString(C2137R.string.common_google_play_services_notification_ticker);
            if (ank.m4041a()) {
                Builder autoCancel = new Builder(context).setSmallIcon(17301642).setContentTitle(b).setContentText(d).setContentIntent(pendingIntent).setTicker(string).setAutoCancel(true);
                if (ank.m4049h()) {
                    autoCancel.setLocalOnly(true);
                }
                if (ank.m4046e()) {
                    autoCancel.setStyle(new BigTextStyle().bigText(d));
                    notification = autoCancel.build();
                } else {
                    notification = autoCancel.getNotification();
                }
                if (VERSION.SDK_INT == 19) {
                    notification.extras.putBoolean(NotificationCompatExtras.EXTRA_LOCAL_ONLY, true);
                }
                build = notification;
            } else {
                build = new NotificationCompat.Builder(context).setSmallIcon(17301642).setTicker(string).setWhen(System.currentTimeMillis()).setAutoCancel(true).setContentIntent(pendingIntent).setContentTitle(b).setContentText(d).build();
            }
        }
        if (zzfn(i)) {
            f3629vd.set(false);
            i2 = 10436;
        } else {
            i2 = 39789;
        }
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        if (str != null) {
            notificationManager.notify(str, i2, build);
        } else {
            notificationManager.notify(i2, build);
        }
    }

    private static void zzbs(Context context) {
        C2142a aVar = new C2142a(context);
        aVar.sendMessageDelayed(aVar.obtainMessage(1), 120000);
    }

    @Deprecated
    public static boolean zzd(Context context, int i) {
        return ant.zzd(context, i);
    }

    @Deprecated
    public static boolean zze(Context context, int i) {
        return ant.zze(context, i);
    }

    @Deprecated
    public static Intent zzfm(int i) {
        return ant.zzfm(i);
    }
}
