package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.WorkSource;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ActivityRecognitionRequest extends AbstractSafeParcelable {
    public static final Creator<ActivityRecognitionRequest> CREATOR = new arj();

    /* renamed from: a */
    private final int f9007a;

    /* renamed from: b */
    private long f9008b;

    /* renamed from: c */
    private boolean f9009c;

    /* renamed from: d */
    private WorkSource f9010d;

    /* renamed from: e */
    private String f9011e;

    /* renamed from: f */
    private int[] f9012f;

    /* renamed from: g */
    private boolean f9013g;

    /* renamed from: h */
    private String f9014h;

    /* renamed from: i */
    private final long f9015i;

    public ActivityRecognitionRequest(int i, long j, boolean z, WorkSource workSource, String str, int[] iArr, boolean z2, String str2, long j2) {
        this.f9007a = i;
        this.f9008b = j;
        this.f9009c = z;
        this.f9010d = workSource;
        this.f9011e = str;
        this.f9012f = iArr;
        this.f9013g = z2;
        this.f9014h = str2;
        this.f9015i = j2;
    }

    /* renamed from: a */
    public long mo12355a() {
        return this.f9008b;
    }

    /* renamed from: b */
    public boolean mo12356b() {
        return this.f9009c;
    }

    /* renamed from: c */
    public WorkSource mo12357c() {
        return this.f9010d;
    }

    /* renamed from: d */
    public String mo12358d() {
        return this.f9011e;
    }

    /* renamed from: e */
    public int[] mo12359e() {
        return this.f9012f;
    }

    /* renamed from: f */
    public boolean mo12360f() {
        return this.f9013g;
    }

    /* renamed from: g */
    public String mo12361g() {
        return this.f9014h;
    }

    /* renamed from: h */
    public long mo12362h() {
        return this.f9015i;
    }

    /* renamed from: i */
    public int mo12363i() {
        return this.f9007a;
    }

    public void writeToParcel(Parcel parcel, int i) {
        arj.m4821a(this, parcel, i);
    }
}
