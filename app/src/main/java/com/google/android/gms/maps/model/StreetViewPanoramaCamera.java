package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation.C2179a;

public class StreetViewPanoramaCamera extends AbstractSafeParcelable {
    public static final aur CREATOR = new aur();

    /* renamed from: a */
    public final float f9297a;

    /* renamed from: b */
    public final float f9298b;

    /* renamed from: c */
    public final float f9299c;

    /* renamed from: d */
    private final int f9300d;

    /* renamed from: e */
    private StreetViewPanoramaOrientation f9301e;

    public StreetViewPanoramaCamera(int i, float f, float f2, float f3) {
        alw.m2732b(-90.0f <= f2 && f2 <= 90.0f, "Tilt needs to be between -90 and 90 inclusive");
        this.f9300d = i;
        if (((double) f) <= 0.0d) {
            f = 0.0f;
        }
        this.f9297a = f;
        this.f9298b = f2 + 0.0f;
        this.f9299c = (((double) f3) <= 0.0d ? (f3 % 360.0f) + 360.0f : f3) % 360.0f;
        this.f9301e = new C2179a().mo12696a(f2).mo12698b(f3).mo12697a();
    }

    /* renamed from: a */
    public int mo12676a() {
        return this.f9300d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaCamera)) {
            return false;
        }
        StreetViewPanoramaCamera streetViewPanoramaCamera = (StreetViewPanoramaCamera) obj;
        return Float.floatToIntBits(this.f9297a) == Float.floatToIntBits(streetViewPanoramaCamera.f9297a) && Float.floatToIntBits(this.f9298b) == Float.floatToIntBits(streetViewPanoramaCamera.f9298b) && Float.floatToIntBits(this.f9299c) == Float.floatToIntBits(streetViewPanoramaCamera.f9299c);
    }

    public int hashCode() {
        return alv.m2719a(Float.valueOf(this.f9297a), Float.valueOf(this.f9298b), Float.valueOf(this.f9299c));
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("zoom", Float.valueOf(this.f9297a)).mo1191a("tilt", Float.valueOf(this.f9298b)).mo1191a("bearing", Float.valueOf(this.f9299c)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aur.m5705a(this, parcel, i);
    }
}
