package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;

@KeepName
public final class DataHolder extends AbstractSafeParcelable implements Closeable {
    public static final Creator<DataHolder> CREATOR = new alq();

    /* renamed from: k */
    private static final C2144a f8902k = new C2144a(new String[0], null) {
    };

    /* renamed from: a */
    Bundle f8903a;

    /* renamed from: b */
    int[] f8904b;

    /* renamed from: c */
    int f8905c;

    /* renamed from: d */
    boolean f8906d = false;

    /* renamed from: e */
    private final int f8907e;

    /* renamed from: f */
    private final String[] f8908f;

    /* renamed from: g */
    private final CursorWindow[] f8909g;

    /* renamed from: h */
    private final int f8910h;

    /* renamed from: i */
    private final Bundle f8911i;

    /* renamed from: j */
    private boolean f8912j = true;

    /* renamed from: com.google.android.gms.common.data.DataHolder$a */
    public static class C2144a {

        /* renamed from: a */
        private final String[] f8913a;

        /* renamed from: b */
        private final ArrayList<HashMap<String, Object>> f8914b;

        /* renamed from: c */
        private final String f8915c;

        /* renamed from: d */
        private final HashMap<Object, Integer> f8916d;

        /* renamed from: e */
        private boolean f8917e;

        /* renamed from: f */
        private String f8918f;

        private C2144a(String[] strArr, String str) {
            this.f8913a = (String[]) alw.m2723a(strArr);
            this.f8914b = new ArrayList<>();
            this.f8915c = str;
            this.f8916d = new HashMap<>();
            this.f8917e = false;
            this.f8918f = null;
        }
    }

    public static class zzb extends RuntimeException {
    }

    public DataHolder(int i, String[] strArr, CursorWindow[] cursorWindowArr, int i2, Bundle bundle) {
        this.f8907e = i;
        this.f8908f = strArr;
        this.f8909g = cursorWindowArr;
        this.f8910h = i2;
        this.f8911i = bundle;
    }

    /* renamed from: a */
    public void mo12232a() {
        this.f8903a = new Bundle();
        for (int i = 0; i < this.f8908f.length; i++) {
            this.f8903a.putInt(this.f8908f[i], i);
        }
        this.f8904b = new int[this.f8909g.length];
        int i2 = 0;
        for (int i3 = 0; i3 < this.f8909g.length; i3++) {
            this.f8904b[i3] = i2;
            i2 += this.f8909g[i3].getNumRows() - (i2 - this.f8909g[i3].getStartPosition());
        }
        this.f8905c = i2;
    }

    /* renamed from: b */
    public int mo12233b() {
        return this.f8907e;
    }

    /* renamed from: c */
    public String[] mo12234c() {
        return this.f8908f;
    }

    public void close() {
        synchronized (this) {
            if (!this.f8906d) {
                this.f8906d = true;
                for (CursorWindow close : this.f8909g) {
                    close.close();
                }
            }
        }
    }

    /* renamed from: d */
    public CursorWindow[] mo12236d() {
        return this.f8909g;
    }

    /* renamed from: e */
    public int mo12237e() {
        return this.f8910h;
    }

    /* renamed from: f */
    public Bundle mo12238f() {
        return this.f8911i;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            if (this.f8912j && this.f8909g.length > 0 && !mo12240g()) {
                close();
                String valueOf = String.valueOf(toString());
                Log.e("DataBuffer", new StringBuilder(String.valueOf(valueOf).length() + 178).append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ").append(valueOf).append(")").toString());
            }
        } finally {
            super.finalize();
        }
    }

    /* renamed from: g */
    public boolean mo12240g() {
        boolean z;
        synchronized (this) {
            z = this.f8906d;
        }
        return z;
    }

    public void writeToParcel(Parcel parcel, int i) {
        alq.m2679a(this, parcel, i);
    }
}
