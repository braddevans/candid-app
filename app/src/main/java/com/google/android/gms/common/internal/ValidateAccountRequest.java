package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
public class ValidateAccountRequest extends AbstractSafeParcelable {
    public static final Creator<ValidateAccountRequest> CREATOR = new amd();

    /* renamed from: a */
    public final int f8946a;

    /* renamed from: b */
    public final IBinder f8947b;

    /* renamed from: c */
    private final int f8948c;

    /* renamed from: d */
    private final Scope[] f8949d;

    /* renamed from: e */
    private final Bundle f8950e;

    /* renamed from: f */
    private final String f8951f;

    public ValidateAccountRequest(int i, int i2, IBinder iBinder, Scope[] scopeArr, Bundle bundle, String str) {
        this.f8946a = i;
        this.f8948c = i2;
        this.f8947b = iBinder;
        this.f8949d = scopeArr;
        this.f8950e = bundle;
        this.f8951f = str;
    }

    /* renamed from: a */
    public int mo12263a() {
        return this.f8948c;
    }

    /* renamed from: b */
    public Scope[] mo12264b() {
        return this.f8949d;
    }

    /* renamed from: c */
    public String mo12265c() {
        return this.f8951f;
    }

    /* renamed from: d */
    public Bundle mo12266d() {
        return this.f8950e;
    }

    public void writeToParcel(Parcel parcel, int i) {
        amd.m2758a(this, parcel, i);
    }
}
