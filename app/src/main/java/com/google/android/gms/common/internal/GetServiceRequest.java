package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Collection;

public class GetServiceRequest extends AbstractSafeParcelable {
    public static final Creator<GetServiceRequest> CREATOR = new aml();

    /* renamed from: a */
    public final int f8924a;

    /* renamed from: b */
    public final int f8925b;

    /* renamed from: c */
    public int f8926c;

    /* renamed from: d */
    public String f8927d;

    /* renamed from: e */
    public IBinder f8928e;

    /* renamed from: f */
    public Scope[] f8929f;

    /* renamed from: g */
    public Bundle f8930g;

    /* renamed from: h */
    public Account f8931h;

    /* renamed from: i */
    public long f8932i;

    public GetServiceRequest(int i) {
        this.f8924a = 3;
        this.f8926c = anr.f3616b;
        this.f8925b = i;
    }

    public GetServiceRequest(int i, int i2, int i3, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, long j) {
        this.f8924a = i;
        this.f8925b = i2;
        this.f8926c = i3;
        this.f8927d = str;
        if (i < 2) {
            this.f8931h = m11489a(iBinder);
        } else {
            this.f8928e = iBinder;
            this.f8931h = account;
        }
        this.f8929f = scopeArr;
        this.f8930g = bundle;
        this.f8932i = j;
    }

    /* renamed from: a */
    private Account m11489a(IBinder iBinder) {
        if (iBinder != null) {
            return alt.m2714a(C0349a.m2919a(iBinder));
        }
        return null;
    }

    /* renamed from: a */
    public GetServiceRequest mo12243a(amq amq) {
        if (amq != null) {
            this.f8928e = amq.asBinder();
        }
        return this;
    }

    /* renamed from: a */
    public GetServiceRequest mo12244a(Account account) {
        this.f8931h = account;
        return this;
    }

    /* renamed from: a */
    public GetServiceRequest mo12245a(Bundle bundle) {
        this.f8930g = bundle;
        return this;
    }

    /* renamed from: a */
    public GetServiceRequest mo12246a(String str) {
        this.f8927d = str;
        return this;
    }

    /* renamed from: a */
    public GetServiceRequest mo12247a(Collection<Scope> collection) {
        this.f8929f = (Scope[]) collection.toArray(new Scope[collection.size()]);
        return this;
    }

    public void writeToParcel(Parcel parcel, int i) {
        aml.m2876a(this, parcel, i);
    }
}
