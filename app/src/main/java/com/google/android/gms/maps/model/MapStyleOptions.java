package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class MapStyleOptions extends AbstractSafeParcelable {
    public static final aum CREATOR = new aum();

    /* renamed from: a */
    private static final String f9257a = MapStyleOptions.class.getSimpleName();

    /* renamed from: b */
    private final int f9258b;

    /* renamed from: c */
    private String f9259c;

    public MapStyleOptions(int i, String str) {
        this.f9258b = i;
        this.f9259c = str;
    }

    /* renamed from: a */
    public int mo12635a() {
        return this.f9258b;
    }

    /* renamed from: b */
    public String mo12636b() {
        return this.f9259c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        aum.m5690a(this, parcel, i);
    }
}
