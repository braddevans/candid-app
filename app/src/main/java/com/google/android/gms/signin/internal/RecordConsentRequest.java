package com.google.android.gms.signin.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class RecordConsentRequest extends AbstractSafeParcelable {
    public static final Creator<RecordConsentRequest> CREATOR = new awp();

    /* renamed from: a */
    public final int f9394a;

    /* renamed from: b */
    private final Account f9395b;

    /* renamed from: c */
    private final Scope[] f9396c;

    /* renamed from: d */
    private final String f9397d;

    public RecordConsentRequest(int i, Account account, Scope[] scopeArr, String str) {
        this.f9394a = i;
        this.f9395b = account;
        this.f9396c = scopeArr;
        this.f9397d = str;
    }

    /* renamed from: a */
    public Account mo12760a() {
        return this.f9395b;
    }

    /* renamed from: b */
    public Scope[] mo12761b() {
        return this.f9396c;
    }

    /* renamed from: c */
    public String mo12762c() {
        return this.f9397d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        awp.m6582a(this, parcel, i);
    }
}
