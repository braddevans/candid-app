package com.google.android.gms.common;

public final class GooglePlayServicesNotAvailableException extends Exception {

    /* renamed from: a */
    public final int f8879a;

    public GooglePlayServicesNotAvailableException(int i) {
        this.f8879a = i;
    }
}
