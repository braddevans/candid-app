package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class LocationSettingsStates extends AbstractSafeParcelable {
    public static final Creator<LocationSettingsStates> CREATOR = new art();

    /* renamed from: a */
    private final int f9071a;

    /* renamed from: b */
    private final boolean f9072b;

    /* renamed from: c */
    private final boolean f9073c;

    /* renamed from: d */
    private final boolean f9074d;

    /* renamed from: e */
    private final boolean f9075e;

    /* renamed from: f */
    private final boolean f9076f;

    /* renamed from: g */
    private final boolean f9077g;

    public LocationSettingsStates(int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.f9071a = i;
        this.f9072b = z;
        this.f9073c = z2;
        this.f9074d = z3;
        this.f9075e = z4;
        this.f9076f = z5;
        this.f9077g = z6;
    }

    /* renamed from: a */
    public int mo12433a() {
        return this.f9071a;
    }

    /* renamed from: b */
    public boolean mo12434b() {
        return this.f9072b;
    }

    /* renamed from: c */
    public boolean mo12435c() {
        return this.f9075e;
    }

    /* renamed from: d */
    public boolean mo12436d() {
        return this.f9073c;
    }

    /* renamed from: e */
    public boolean mo12437e() {
        return this.f9076f;
    }

    /* renamed from: f */
    public boolean mo12438f() {
        return this.f9074d;
    }

    /* renamed from: g */
    public boolean mo12439g() {
        return this.f9077g;
    }

    public void writeToParcel(Parcel parcel, int i) {
        art.m4853a(this, parcel, i);
    }
}
