package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.location.internal.ParcelableGeofence;
import java.util.ArrayList;
import java.util.List;

public class GeofencingRequest extends AbstractSafeParcelable {
    public static final Creator<GeofencingRequest> CREATOR = new arm();

    /* renamed from: a */
    private final int f9036a;

    /* renamed from: b */
    private final List<ParcelableGeofence> f9037b;

    /* renamed from: c */
    private final int f9038c;

    /* renamed from: com.google.android.gms.location.GeofencingRequest$a */
    public static final class C2163a {

        /* renamed from: a */
        private final List<ParcelableGeofence> f9039a = new ArrayList();

        /* renamed from: b */
        private int f9040b = 5;

        /* renamed from: b */
        public static int m11707b(int i) {
            return i & 7;
        }

        /* renamed from: a */
        public C2163a mo12401a(int i) {
            this.f9040b = m11707b(i);
            return this;
        }

        /* renamed from: a */
        public C2163a mo12402a(aqe aqe) {
            alw.m2724a(aqe, (Object) "geofence can't be null.");
            alw.m2732b(aqe instanceof ParcelableGeofence, "Geofence must be created using Geofence.Builder.");
            this.f9039a.add((ParcelableGeofence) aqe);
            return this;
        }

        /* renamed from: a */
        public C2163a mo12403a(List<aqe> list) {
            if (list != null && !list.isEmpty()) {
                for (aqe aqe : list) {
                    if (aqe != null) {
                        mo12402a(aqe);
                    }
                }
            }
            return this;
        }

        /* renamed from: a */
        public GeofencingRequest mo12404a() {
            alw.m2732b(!this.f9039a.isEmpty(), "No geofence has been added to this request.");
            return new GeofencingRequest((List) this.f9039a, this.f9040b);
        }
    }

    public GeofencingRequest(int i, List<ParcelableGeofence> list, int i2) {
        this.f9036a = i;
        this.f9037b = list;
        this.f9038c = i2;
    }

    private GeofencingRequest(List<ParcelableGeofence> list, int i) {
        this(1, list, i);
    }

    /* renamed from: a */
    public int mo12397a() {
        return this.f9036a;
    }

    /* renamed from: b */
    public List<ParcelableGeofence> mo12398b() {
        return this.f9037b;
    }

    /* renamed from: c */
    public int mo12399c() {
        return this.f9038c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        arm.m4830a(this, parcel, i);
    }
}
