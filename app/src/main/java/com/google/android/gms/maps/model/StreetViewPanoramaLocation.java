package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class StreetViewPanoramaLocation extends AbstractSafeParcelable {
    public static final aut CREATOR = new aut();

    /* renamed from: a */
    public final StreetViewPanoramaLink[] f9305a;

    /* renamed from: b */
    public final LatLng f9306b;

    /* renamed from: c */
    public final String f9307c;

    /* renamed from: d */
    private final int f9308d;

    public StreetViewPanoramaLocation(int i, StreetViewPanoramaLink[] streetViewPanoramaLinkArr, LatLng latLng, String str) {
        this.f9308d = i;
        this.f9305a = streetViewPanoramaLinkArr;
        this.f9306b = latLng;
        this.f9307c = str;
    }

    /* renamed from: a */
    public int mo12686a() {
        return this.f9308d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaLocation)) {
            return false;
        }
        StreetViewPanoramaLocation streetViewPanoramaLocation = (StreetViewPanoramaLocation) obj;
        return this.f9307c.equals(streetViewPanoramaLocation.f9307c) && this.f9306b.equals(streetViewPanoramaLocation.f9306b);
    }

    public int hashCode() {
        return alv.m2719a(this.f9306b, this.f9307c);
    }

    public String toString() {
        return alv.m2720a((Object) this).mo1191a("panoId", this.f9307c).mo1191a("position", this.f9306b.toString()).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aut.m5711a(this, parcel, i);
    }
}
