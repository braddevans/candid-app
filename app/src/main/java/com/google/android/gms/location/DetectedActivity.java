package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Comparator;

public class DetectedActivity extends AbstractSafeParcelable {
    public static final aqc CREATOR = new aqc();

    /* renamed from: a */
    public static final Comparator<DetectedActivity> f9030a = new Comparator<DetectedActivity>() {
        /* renamed from: a */
        public int compare(DetectedActivity detectedActivity, DetectedActivity detectedActivity2) {
            int compareTo = Integer.valueOf(detectedActivity2.mo12389b()).compareTo(Integer.valueOf(detectedActivity.mo12389b()));
            return compareTo == 0 ? Integer.valueOf(detectedActivity.mo12388a()).compareTo(Integer.valueOf(detectedActivity2.mo12388a())) : compareTo;
        }
    };

    /* renamed from: b */
    public static final int[] f9031b = {9, 10};

    /* renamed from: c */
    public static final int[] f9032c = {0, 1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14};

    /* renamed from: d */
    public int f9033d;

    /* renamed from: e */
    public int f9034e;

    /* renamed from: f */
    private final int f9035f;

    public DetectedActivity(int i, int i2, int i3) {
        this.f9035f = i;
        this.f9033d = i2;
        this.f9034e = i3;
    }

    /* renamed from: a */
    public static String m11698a(int i) {
        switch (i) {
            case 0:
                return "IN_VEHICLE";
            case 1:
                return "ON_BICYCLE";
            case 2:
                return "ON_FOOT";
            case 3:
                return "STILL";
            case 4:
                return "UNKNOWN";
            case 5:
                return "TILTING";
            case 7:
                return "WALKING";
            case 8:
                return "RUNNING";
            default:
                return Integer.toString(i);
        }
    }

    /* renamed from: b */
    private int m11699b(int i) {
        if (i > 15) {
            return 4;
        }
        return i;
    }

    /* renamed from: a */
    public int mo12388a() {
        return m11699b(this.f9033d);
    }

    /* renamed from: b */
    public int mo12389b() {
        return this.f9034e;
    }

    /* renamed from: c */
    public int mo12390c() {
        return this.f9035f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DetectedActivity detectedActivity = (DetectedActivity) obj;
        return this.f9033d == detectedActivity.f9033d && this.f9034e == detectedActivity.f9034e;
    }

    public int hashCode() {
        return alv.m2719a(Integer.valueOf(this.f9033d), Integer.valueOf(this.f9034e));
    }

    public String toString() {
        String valueOf = String.valueOf(m11698a(mo12388a()));
        return new StringBuilder(String.valueOf(valueOf).length() + 48).append("DetectedActivity [type=").append(valueOf).append(", confidence=").append(this.f9034e).append("]").toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqc.m4651a(this, parcel, i);
    }
}
