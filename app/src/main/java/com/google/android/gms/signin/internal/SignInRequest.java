package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class SignInRequest extends AbstractSafeParcelable {
    public static final Creator<SignInRequest> CREATOR = new awr();

    /* renamed from: a */
    public final int f9398a;

    /* renamed from: b */
    final ResolveAccountRequest f9399b;

    public SignInRequest(int i, ResolveAccountRequest resolveAccountRequest) {
        this.f9398a = i;
        this.f9399b = resolveAccountRequest;
    }

    /* renamed from: a */
    public ResolveAccountRequest mo12764a() {
        return this.f9399b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        awr.m6592a(this, parcel, i);
    }
}
