package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import com.google.android.gms.C2137R;

public final class zzah extends Button {
    public zzah(Context context) {
        this(context, null);
    }

    public zzah(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842824);
    }

    /* renamed from: a */
    private int m11539a(int i, int i2, int i3) {
        switch (i) {
            case 0:
            case 1:
                return i3;
            case 2:
                return i2;
            default:
                throw new IllegalStateException("Unknown button size: " + i);
        }
    }

    /* renamed from: a */
    private int m11540a(int i, int i2, int i3, int i4) {
        switch (i) {
            case 0:
                return i2;
            case 1:
                return i3;
            case 2:
                return i4;
            default:
                throw new IllegalStateException("Unknown color scheme: " + i);
        }
    }

    /* renamed from: a */
    private void m11541a(Resources resources) {
        setTypeface(Typeface.DEFAULT_BOLD);
        setTextSize(14.0f);
        float f = resources.getDisplayMetrics().density;
        setMinHeight((int) ((f * 48.0f) + 0.5f));
        setMinWidth((int) ((f * 48.0f) + 0.5f));
    }

    /* renamed from: b */
    private void m11542b(Resources resources, int i, int i2) {
        setBackgroundDrawable(resources.getDrawable(m11539a(i, m11540a(i2, C2137R.C2138drawable.common_google_signin_btn_icon_dark, C2137R.C2138drawable.common_google_signin_btn_icon_light, C2137R.C2138drawable.common_google_signin_btn_icon_light), m11540a(i2, C2137R.C2138drawable.common_google_signin_btn_text_dark, C2137R.C2138drawable.common_google_signin_btn_text_light, C2137R.C2138drawable.common_google_signin_btn_text_light))));
    }

    /* renamed from: c */
    private void m11543c(Resources resources, int i, int i2) {
        setTextColor((ColorStateList) alw.m2723a(resources.getColorStateList(m11540a(i2, C2137R.color.common_google_signin_btn_text_dark, C2137R.color.common_google_signin_btn_text_light, C2137R.color.common_google_signin_btn_text_light))));
        switch (i) {
            case 0:
                setText(resources.getString(C2137R.string.common_signin_button_text));
                break;
            case 1:
                setText(resources.getString(C2137R.string.common_signin_button_text_long));
                break;
            case 2:
                setText(null);
                break;
            default:
                throw new IllegalStateException("Unknown button size: " + i);
        }
        setTransformationMethod(null);
    }

    /* renamed from: a */
    public void mo12269a(Resources resources, int i, int i2) {
        m11541a(resources);
        m11542b(resources, i, i2);
        m11543c(resources, i, i2);
    }
}
