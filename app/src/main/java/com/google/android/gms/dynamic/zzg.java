package com.google.android.gms.dynamic;

import android.content.Context;
import android.os.IBinder;

public abstract class zzg<T> {

    /* renamed from: a */
    private final String f8964a;

    /* renamed from: b */
    private T f8965b;

    public static class zza extends Exception {
        public zza(String str) {
            super(str);
        }

        public zza(String str, Throwable th) {
            super(str, th);
        }
    }

    protected zzg(String str) {
        this.f8964a = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final T mo12284a(Context context) throws zza {
        if (this.f8965b == null) {
            alw.m2723a(context);
            Context remoteContext = ant.getRemoteContext(context);
            if (remoteContext == null) {
                throw new zza("Could not get remote context.");
            }
            try {
                this.f8965b = mo1206b((IBinder) remoteContext.getClassLoader().loadClass(this.f8964a).newInstance());
            } catch (ClassNotFoundException e) {
                throw new zza("Could not load creator class.", e);
            } catch (InstantiationException e2) {
                throw new zza("Could not instantiate creator.", e2);
            } catch (IllegalAccessException e3) {
                throw new zza("Could not access creator.", e3);
            }
        }
        return this.f8965b;
    }

    /* renamed from: b */
    public abstract T mo1206b(IBinder iBinder);
}
