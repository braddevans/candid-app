package com.google.android.gms.location.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;

public class LocationRequestInternal extends AbstractSafeParcelable {
    public static final aqx CREATOR = new aqx();

    /* renamed from: a */
    public static final List<ClientIdentity> f9084a = Collections.emptyList();

    /* renamed from: b */
    public LocationRequest f9085b;

    /* renamed from: c */
    public boolean f9086c;

    /* renamed from: d */
    public List<ClientIdentity> f9087d;

    /* renamed from: e */
    public String f9088e;

    /* renamed from: f */
    public boolean f9089f;

    /* renamed from: g */
    public boolean f9090g;

    /* renamed from: h */
    private final int f9091h;

    public LocationRequestInternal(int i, LocationRequest locationRequest, boolean z, List<ClientIdentity> list, String str, boolean z2, boolean z3) {
        this.f9091h = i;
        this.f9085b = locationRequest;
        this.f9086c = z;
        this.f9087d = list;
        this.f9088e = str;
        this.f9089f = z2;
        this.f9090g = z3;
    }

    /* renamed from: a */
    public int mo12448a() {
        return this.f9091h;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof LocationRequestInternal)) {
            return false;
        }
        LocationRequestInternal locationRequestInternal = (LocationRequestInternal) obj;
        return alv.m2721a(this.f9085b, locationRequestInternal.f9085b) && this.f9086c == locationRequestInternal.f9086c && this.f9089f == locationRequestInternal.f9089f && alv.m2721a(this.f9087d, locationRequestInternal.f9087d) && this.f9090g == locationRequestInternal.f9090g;
    }

    public int hashCode() {
        return this.f9085b.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f9085b.toString());
        if (this.f9088e != null) {
            sb.append(" tag=").append(this.f9088e);
        }
        sb.append(" trigger=").append(this.f9086c);
        sb.append(" hideAppOps=").append(this.f9089f);
        sb.append(" clients=").append(this.f9087d);
        sb.append(" forceCoarseLocation=").append(this.f9090g);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqx.m4785a(this, parcel, i);
    }
}
