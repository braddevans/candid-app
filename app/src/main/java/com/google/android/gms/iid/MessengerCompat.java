package com.google.android.gms.iid;

import android.annotation.TargetApi;
import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public class MessengerCompat implements Parcelable {
    public static final Creator<MessengerCompat> CREATOR = new Creator<MessengerCompat>() {
        /* renamed from: a */
        public MessengerCompat createFromParcel(Parcel parcel) {
            IBinder readStrongBinder = parcel.readStrongBinder();
            if (readStrongBinder != null) {
                return new MessengerCompat(readStrongBinder);
            }
            return null;
        }

        /* renamed from: a */
        public MessengerCompat[] newArray(int i) {
            return new MessengerCompat[i];
        }
    };

    /* renamed from: a */
    Messenger f8989a;

    /* renamed from: b */
    aoe f8990b;

    /* renamed from: com.google.android.gms.iid.MessengerCompat$a */
    final class C2150a extends C0688a {

        /* renamed from: a */
        Handler f8991a;

        C2150a(Handler handler) {
            this.f8991a = handler;
        }

        /* renamed from: a */
        public void mo6586a(Message message) throws RemoteException {
            message.arg2 = Binder.getCallingUid();
            this.f8991a.dispatchMessage(message);
        }
    }

    public MessengerCompat(Handler handler) {
        if (VERSION.SDK_INT >= 21) {
            this.f8989a = new Messenger(handler);
        } else {
            this.f8990b = new C2150a(handler);
        }
    }

    public MessengerCompat(IBinder iBinder) {
        if (VERSION.SDK_INT >= 21) {
            this.f8989a = new Messenger(iBinder);
        } else {
            this.f8990b = C0688a.m4209a(iBinder);
        }
    }

    /* renamed from: a */
    public static int m11603a(Message message) {
        return VERSION.SDK_INT >= 21 ? m11604c(message) : message.arg2;
    }

    @TargetApi(21)
    /* renamed from: c */
    private static int m11604c(Message message) {
        return message.sendingUid;
    }

    /* renamed from: a */
    public IBinder mo12307a() {
        return this.f8989a != null ? this.f8989a.getBinder() : this.f8990b.asBinder();
    }

    /* renamed from: b */
    public void mo12308b(Message message) throws RemoteException {
        if (this.f8989a != null) {
            this.f8989a.send(message);
        } else {
            this.f8990b.mo6586a(message);
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null) {
            return z;
        }
        try {
            return mo12307a().equals(((MessengerCompat) obj).mo12307a());
        } catch (ClassCastException e) {
            return z;
        }
    }

    public int hashCode() {
        return mo12307a().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (this.f8989a != null) {
            parcel.writeStrongBinder(this.f8989a.getBinder());
        } else {
            parcel.writeStrongBinder(this.f8990b.asBinder());
        }
    }
}
