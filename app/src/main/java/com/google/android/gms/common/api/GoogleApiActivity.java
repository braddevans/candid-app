package com.google.android.gms.common.api;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;

public class GoogleApiActivity extends Activity implements OnCancelListener {

    /* renamed from: a */
    protected int f8887a = 0;

    /* renamed from: a */
    public static PendingIntent m11466a(Context context, PendingIntent pendingIntent, int i) {
        return m11467a(context, pendingIntent, i, true);
    }

    /* renamed from: a */
    public static PendingIntent m11467a(Context context, PendingIntent pendingIntent, int i, boolean z) {
        return PendingIntent.getActivity(context, 0, m11470b(context, pendingIntent, i, z), 134217728);
    }

    /* renamed from: a */
    private void m11468a() {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Log.e("GoogleApiActivity", "Activity started without extras");
            finish();
            return;
        }
        PendingIntent pendingIntent = (PendingIntent) extras.get("pending_intent");
        Integer num = (Integer) extras.get("error_code");
        if (pendingIntent == null && num == null) {
            Log.e("GoogleApiActivity", "Activity started without resolution");
            finish();
        } else if (pendingIntent != null) {
            try {
                startIntentSenderForResult(pendingIntent.getIntentSender(), 1, null, 0, 0, 0);
                this.f8887a = 1;
            } catch (SendIntentException e) {
                Log.e("GoogleApiActivity", "Failed to launch pendingIntent", e);
                finish();
            }
        } else {
            aky.m2599a().mo1131b(this, num.intValue(), 2, this);
            this.f8887a = 1;
        }
    }

    /* renamed from: a */
    private void m11469a(int i, aoy aoy) {
        switch (i) {
            case -1:
                aoy.mo6706b();
                return;
            case 0:
                aoy.mo6707b(new ConnectionResult(13, null), getIntent().getIntExtra("failing_client_id", -1));
                return;
            default:
                return;
        }
    }

    /* renamed from: b */
    public static Intent m11470b(Context context, PendingIntent pendingIntent, int i, boolean z) {
        Intent intent = new Intent(context, GoogleApiActivity.class);
        intent.putExtra("pending_intent", pendingIntent);
        intent.putExtra("failing_client_id", i);
        intent.putExtra("notify_manager", z);
        return intent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12211a(int i) {
        setResult(i);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1) {
            boolean booleanExtra = getIntent().getBooleanExtra("notify_manager", true);
            this.f8887a = 0;
            aoy a = aoy.m4367a();
            mo12211a(i2);
            if (booleanExtra) {
                m11469a(i2, a);
            }
        } else if (i == 2) {
            this.f8887a = 0;
            mo12211a(i2);
        }
        finish();
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f8887a = 0;
        setResult(0);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.f8887a = bundle.getInt("resolution");
        }
        if (this.f8887a != 1) {
            m11468a();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("resolution", this.f8887a);
        super.onSaveInstanceState(bundle);
    }
}
