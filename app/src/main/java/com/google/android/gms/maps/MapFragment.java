package com.google.android.gms.maps;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import java.util.ArrayList;
import java.util.List;

@TargetApi(11)
public class MapFragment extends Fragment {

    /* renamed from: a */
    private final C2168b f9169a = new C2168b(this);

    /* renamed from: com.google.android.gms.maps.MapFragment$a */
    static class C2166a implements ask {

        /* renamed from: a */
        private final Fragment f9170a;

        /* renamed from: b */
        private final asd f9171b;

        public C2166a(Fragment fragment, asd asd) {
            this.f9171b = (asd) alw.m2723a(asd);
            this.f9170a = (Fragment) alw.m2723a(fragment);
        }

        /* renamed from: a */
        public View mo6484a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            try {
                return (View) any.m4134a(this.f9171b.mo7128a(any.m4133a(layoutInflater), any.m4133a(viewGroup), bundle));
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo6485a() {
            try {
                this.f9171b.mo7142i();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo6486a(Activity activity, Bundle bundle, Bundle bundle2) {
            try {
                this.f9171b.mo7131a(any.m4133a(activity), (GoogleMapOptions) bundle.getParcelable("MapOptions"), bundle2);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: a */
        public void mo6487a(Bundle bundle) {
            if (bundle == null) {
                try {
                    bundle = new Bundle();
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                }
            }
            Bundle arguments = this.f9170a.getArguments();
            if (arguments != null && arguments.containsKey("MapOptions")) {
                asu.m5272a(bundle, "MapOptions", arguments.getParcelable("MapOptions"));
            }
            this.f9171b.mo7130a(bundle);
        }

        /* renamed from: a */
        public void mo12558a(final arx arx) {
            try {
                this.f9171b.mo7132a((ato) new C0852a() {
                    /* renamed from: a */
                    public void mo7344a(asb asb) throws RemoteException {
                        arx.mo7023a(new arv(asb));
                    }
                });
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6488b() {
            try {
                this.f9171b.mo7133b();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: b */
        public void mo6489b(Bundle bundle) {
            try {
                this.f9171b.mo7134b(bundle);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: c */
        public void mo6490c() {
            try {
                this.f9171b.mo7135c();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: d */
        public void mo6491d() {
            try {
                this.f9171b.mo7143j();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: e */
        public void mo6492e() {
            try {
                this.f9171b.mo7137d();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: f */
        public void mo6493f() {
            try {
                this.f9171b.mo7138e();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        /* renamed from: g */
        public void mo6494g() {
            try {
                this.f9171b.mo7139f();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
    }

    /* renamed from: com.google.android.gms.maps.MapFragment$b */
    static class C2168b extends anw<C2166a> {

        /* renamed from: a */
        protected anz<C2166a> f9174a;

        /* renamed from: b */
        private final Fragment f9175b;

        /* renamed from: c */
        private Activity f9176c;

        /* renamed from: d */
        private final List<arx> f9177d = new ArrayList();

        C2168b(Fragment fragment) {
            this.f9175b = fragment;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m11835a(Activity activity) {
            this.f9176c = activity;
            mo12559i();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo6500a(anz<C2166a> anz) {
            this.f9174a = anz;
            mo12559i();
        }

        /* renamed from: i */
        public void mo12559i() {
            if (this.f9176c != null && this.f9174a != null && mo6496a() == null) {
                try {
                    arw.m4857a((Context) this.f9176c);
                    asd b = asv.m5273a((Context) this.f9176c).mo7273b(any.m4133a(this.f9176c));
                    if (b != null) {
                        this.f9174a.mo6509a(new C2166a(this.f9175b, b));
                        for (arx a : this.f9177d) {
                            ((C2166a) mo6496a()).mo12558a(a);
                        }
                        this.f9177d.clear();
                    }
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public void onActivityCreated(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(MapFragment.class.getClassLoader());
        }
        super.onActivityCreated(bundle);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f9169a.m11835a(activity);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f9169a.mo6498a(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View a = this.f9169a.mo6495a(layoutInflater, viewGroup, bundle);
        a.setClickable(true);
        return a;
    }

    public void onDestroy() {
        this.f9169a.mo6507g();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.f9169a.mo6506f();
        super.onDestroyView();
    }

    @SuppressLint({"NewApi"})
    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(activity, attributeSet, bundle);
        this.f9169a.m11835a(activity);
        GoogleMapOptions a = GoogleMapOptions.m11788a(activity, attributeSet);
        Bundle bundle2 = new Bundle();
        bundle2.putParcelable("MapOptions", a);
        this.f9169a.mo6497a(activity, bundle2, bundle);
    }

    public void onLowMemory() {
        this.f9169a.mo6508h();
        super.onLowMemory();
    }

    public void onPause() {
        this.f9169a.mo6504d();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f9169a.mo6503c();
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(MapFragment.class.getClassLoader());
        }
        super.onSaveInstanceState(bundle);
        this.f9169a.mo6502b(bundle);
    }

    public void onStart() {
        super.onStart();
        this.f9169a.mo6501b();
    }

    public void onStop() {
        this.f9169a.mo6505e();
        super.onStop();
    }

    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
    }
}
