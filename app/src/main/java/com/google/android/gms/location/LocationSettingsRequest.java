package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Collections;
import java.util.List;

public final class LocationSettingsRequest extends AbstractSafeParcelable {
    public static final Creator<LocationSettingsRequest> CREATOR = new arr();

    /* renamed from: a */
    private final int f9064a;

    /* renamed from: b */
    private final List<LocationRequest> f9065b;

    /* renamed from: c */
    private final boolean f9066c;

    /* renamed from: d */
    private final boolean f9067d;

    public LocationSettingsRequest(int i, List<LocationRequest> list, boolean z, boolean z2) {
        this.f9064a = i;
        this.f9065b = list;
        this.f9066c = z;
        this.f9067d = z2;
    }

    /* renamed from: a */
    public int mo12425a() {
        return this.f9064a;
    }

    /* renamed from: b */
    public List<LocationRequest> mo12426b() {
        return Collections.unmodifiableList(this.f9065b);
    }

    /* renamed from: c */
    public boolean mo12427c() {
        return this.f9066c;
    }

    /* renamed from: d */
    public boolean mo12428d() {
        return this.f9067d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        arr.m4847a(this, parcel, i);
    }
}
