package com.google.android.gms.location;

import android.os.Parcel;
import android.os.SystemClock;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class LocationRequest extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final aqi CREATOR = new aqi();

    /* renamed from: a */
    public int f9052a;

    /* renamed from: b */
    public long f9053b;

    /* renamed from: c */
    public long f9054c;

    /* renamed from: d */
    public boolean f9055d;

    /* renamed from: e */
    public long f9056e;

    /* renamed from: f */
    public int f9057f;

    /* renamed from: g */
    public float f9058g;

    /* renamed from: h */
    public long f9059h;

    /* renamed from: i */
    private final int f9060i;

    public LocationRequest() {
        this.f9060i = 1;
        this.f9052a = 102;
        this.f9053b = 3600000;
        this.f9054c = 600000;
        this.f9055d = false;
        this.f9056e = Long.MAX_VALUE;
        this.f9057f = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        this.f9058g = 0.0f;
        this.f9059h = 0;
    }

    public LocationRequest(int i, int i2, long j, long j2, boolean z, long j3, int i3, float f, long j4) {
        this.f9060i = i;
        this.f9052a = i2;
        this.f9053b = j;
        this.f9054c = j2;
        this.f9055d = z;
        this.f9056e = j3;
        this.f9057f = i3;
        this.f9058g = f;
        this.f9059h = j4;
    }

    /* renamed from: a */
    public static String m11716a(int i) {
        switch (i) {
            case 100:
                return "PRIORITY_HIGH_ACCURACY";
            case 102:
                return "PRIORITY_BALANCED_POWER_ACCURACY";
            case 104:
                return "PRIORITY_LOW_POWER";
            case 105:
                return "PRIORITY_NO_POWER";
            default:
                return "???";
        }
    }

    /* renamed from: a */
    public int mo12414a() {
        return this.f9060i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocationRequest)) {
            return false;
        }
        LocationRequest locationRequest = (LocationRequest) obj;
        return this.f9052a == locationRequest.f9052a && this.f9053b == locationRequest.f9053b && this.f9054c == locationRequest.f9054c && this.f9055d == locationRequest.f9055d && this.f9056e == locationRequest.f9056e && this.f9057f == locationRequest.f9057f && this.f9058g == locationRequest.f9058g;
    }

    public int hashCode() {
        return alv.m2719a(Integer.valueOf(this.f9052a), Long.valueOf(this.f9053b), Long.valueOf(this.f9054c), Boolean.valueOf(this.f9055d), Long.valueOf(this.f9056e), Integer.valueOf(this.f9057f), Float.valueOf(this.f9058g));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request[").append(m11716a(this.f9052a));
        if (this.f9052a != 105) {
            sb.append(" requested=");
            sb.append(this.f9053b).append("ms");
        }
        sb.append(" fastest=");
        sb.append(this.f9054c).append("ms");
        if (this.f9059h > this.f9053b) {
            sb.append(" maxWait=");
            sb.append(this.f9059h).append("ms");
        }
        if (this.f9056e != Long.MAX_VALUE) {
            long elapsedRealtime = this.f9056e - SystemClock.elapsedRealtime();
            sb.append(" expireIn=");
            sb.append(elapsedRealtime).append("ms");
        }
        if (this.f9057f != Integer.MAX_VALUE) {
            sb.append(" num=").append(this.f9057f);
        }
        sb.append(']');
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        aqi.m4667a(this, parcel, i);
    }
}
