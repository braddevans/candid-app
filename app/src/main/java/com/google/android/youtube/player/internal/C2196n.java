package com.google.android.youtube.player.internal;

import android.content.Context;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;

/* renamed from: com.google.android.youtube.player.internal.n */
public final class C2196n extends FrameLayout {

    /* renamed from: a */
    private final ProgressBar f9445a;

    /* renamed from: b */
    private final TextView f9446b;

    public C2196n(Context context) {
        super(context, null, axz.m6839c(context));
        axq axq = new axq(context);
        setBackgroundColor(-16777216);
        this.f9445a = new ProgressBar(context);
        this.f9445a.setVisibility(8);
        addView(this.f9445a, new LayoutParams(-2, -2, 17));
        int i = (int) ((10.0f * context.getResources().getDisplayMetrics().density) + 0.5f);
        this.f9446b = new TextView(context);
        this.f9446b.setTextAppearance(context, 16973894);
        this.f9446b.setTextColor(-1);
        this.f9446b.setVisibility(8);
        this.f9446b.setPadding(i, i, i, i);
        this.f9446b.setGravity(17);
        this.f9446b.setText(axq.f4536a);
        addView(this.f9446b, new LayoutParams(-2, -2, 17));
    }

    /* renamed from: a */
    public final void mo12819a() {
        this.f9445a.setVisibility(8);
        this.f9446b.setVisibility(8);
    }

    /* renamed from: b */
    public final void mo12820b() {
        this.f9445a.setVisibility(0);
        this.f9446b.setVisibility(8);
    }

    /* renamed from: c */
    public final void mo12821c() {
        this.f9445a.setVisibility(8);
        this.f9446b.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3 = 0;
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            i3 = size;
        } else if (mode == 1073741824 || (mode == Integer.MIN_VALUE && mode2 == 0)) {
            size2 = (int) (((float) size) / 1.777f);
            i3 = size;
        } else if (mode2 == 1073741824 || (mode2 == Integer.MIN_VALUE && mode == 0)) {
            i3 = (int) (((float) size2) * 1.777f);
        } else if (mode != Integer.MIN_VALUE || mode2 != Integer.MIN_VALUE) {
            size2 = 0;
        } else if (((float) size2) < ((float) size) / 1.777f) {
            i3 = (int) (((float) size2) * 1.777f);
        } else {
            size2 = (int) (((float) size) / 1.777f);
            i3 = size;
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(resolveSize(i3, i), 1073741824), MeasureSpec.makeMeasureSpec(resolveSize(size2, i2), 1073741824));
    }
}
