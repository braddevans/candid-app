package com.google.android.youtube.player.internal;

import android.os.RemoteException;

/* renamed from: com.google.android.youtube.player.internal.q */
public final class C2197q extends RuntimeException {
    public C2197q(RemoteException remoteException) {
        super(remoteException);
    }
}
