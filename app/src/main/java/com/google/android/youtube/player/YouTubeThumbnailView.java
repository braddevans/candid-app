package com.google.android.youtube.player;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public final class YouTubeThumbnailView extends ImageView {

    /* renamed from: a */
    private axb f9444a;

    public YouTubeThumbnailView(Context context) {
        this(context, null);
    }

    public YouTubeThumbnailView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public YouTubeThumbnailView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        if (this.f9444a != null) {
            this.f9444a.mo8147c();
            this.f9444a = null;
        }
        super.finalize();
    }
}
