package com.google.android.youtube.player;

import android.app.Activity;
import android.os.Bundle;

public class YouTubeBaseActivity extends Activity {

    /* renamed from: a */
    private C2190a f9403a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public YouTubePlayerView f9404b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f9405c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Bundle f9406d;

    /* renamed from: com.google.android.youtube.player.YouTubeBaseActivity$a */
    final class C2190a implements C2195b {
        private C2190a() {
        }

        /* synthetic */ C2190a(YouTubeBaseActivity youTubeBaseActivity, byte b) {
            this();
        }

        /* renamed from: a */
        public final void mo12776a(YouTubePlayerView youTubePlayerView) {
            if (!(YouTubeBaseActivity.this.f9404b == null || YouTubeBaseActivity.this.f9404b == youTubePlayerView)) {
                YouTubeBaseActivity.this.f9404b.mo12802c(true);
            }
            YouTubeBaseActivity.this.f9404b = youTubePlayerView;
            if (YouTubeBaseActivity.this.f9405c > 0) {
                youTubePlayerView.mo12788a();
            }
            if (YouTubeBaseActivity.this.f9405c >= 2) {
                youTubePlayerView.mo12799b();
            }
        }

        /* renamed from: a */
        public final void mo12777a(YouTubePlayerView youTubePlayerView, String str, C0945a aVar) {
            youTubePlayerView.mo12789a(YouTubeBaseActivity.this, youTubePlayerView, str, aVar, YouTubeBaseActivity.this.f9406d);
            YouTubeBaseActivity.this.f9406d = null;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public final C2195b mo12769a() {
        return this.f9403a;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f9403a = new C2190a(this, 0);
        this.f9406d = bundle != null ? bundle.getBundle("YouTubeBaseActivity.KEY_PLAYER_VIEW_STATE") : null;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f9404b != null) {
            this.f9404b.mo12800b(isFinishing());
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.f9405c = 1;
        if (this.f9404b != null) {
            this.f9404b.mo12801c();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f9405c = 2;
        if (this.f9404b != null) {
            this.f9404b.mo12799b();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBundle("YouTubeBaseActivity.KEY_PLAYER_VIEW_STATE", this.f9404b != null ? this.f9404b.mo12806e() : this.f9406d);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f9405c = 1;
        if (this.f9404b != null) {
            this.f9404b.mo12788a();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.f9405c = 0;
        if (this.f9404b != null) {
            this.f9404b.mo12804d();
        }
        super.onStop();
    }
}
