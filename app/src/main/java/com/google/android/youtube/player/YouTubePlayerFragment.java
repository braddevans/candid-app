package com.google.android.youtube.player;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class YouTubePlayerFragment extends Fragment implements C0946b {

    /* renamed from: a */
    private final C2191a f9421a = new C2191a(this, 0);

    /* renamed from: b */
    private Bundle f9422b;

    /* renamed from: c */
    private YouTubePlayerView f9423c;

    /* renamed from: d */
    private String f9424d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C0945a f9425e;

    /* renamed from: f */
    private boolean f9426f;

    /* renamed from: com.google.android.youtube.player.YouTubePlayerFragment$a */
    final class C2191a implements C2195b {
        private C2191a() {
        }

        /* synthetic */ C2191a(YouTubePlayerFragment youTubePlayerFragment, byte b) {
            this();
        }

        /* renamed from: a */
        public final void mo12776a(YouTubePlayerView youTubePlayerView) {
        }

        /* renamed from: a */
        public final void mo12777a(YouTubePlayerView youTubePlayerView, String str, C0945a aVar) {
            YouTubePlayerFragment.this.mo12778a(str, YouTubePlayerFragment.this.f9425e);
        }
    }

    /* renamed from: a */
    private void m12053a() {
        if (this.f9423c != null && this.f9425e != null) {
            this.f9423c.mo12791a(this.f9426f);
            this.f9423c.mo12789a(getActivity(), this, this.f9424d, this.f9425e, this.f9422b);
            this.f9422b = null;
            this.f9425e = null;
        }
    }

    /* renamed from: a */
    public void mo12778a(String str, C0945a aVar) {
        this.f9424d = axd.m6618a(str, (Object) "Developer key cannot be null or empty");
        this.f9425e = aVar;
        m12053a();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f9422b = bundle != null ? bundle.getBundle("YouTubePlayerFragment.KEY_PLAYER_VIEW_STATE") : null;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f9423c = new YouTubePlayerView(getActivity(), null, 0, this.f9421a);
        m12053a();
        return this.f9423c;
    }

    public void onDestroy() {
        if (this.f9423c != null) {
            Activity activity = getActivity();
            this.f9423c.mo12800b(activity == null || activity.isFinishing());
        }
        super.onDestroy();
    }

    public void onDestroyView() {
        this.f9423c.mo12802c(getActivity().isFinishing());
        this.f9423c = null;
        super.onDestroyView();
    }

    public void onPause() {
        this.f9423c.mo12801c();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f9423c.mo12799b();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBundle("YouTubePlayerFragment.KEY_PLAYER_VIEW_STATE", this.f9423c != null ? this.f9423c.mo12806e() : this.f9422b);
    }

    public void onStart() {
        super.onStart();
        this.f9423c.mo12788a();
    }

    public void onStop() {
        this.f9423c.mo12804d();
        super.onStop();
    }
}
