package com.google.android.youtube.player;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalFocusChangeListener;
import com.google.android.youtube.player.internal.C2196n;
import com.google.android.youtube.player.internal.C2198w.C2199a;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public final class YouTubePlayerView extends ViewGroup implements C0946b {

    /* renamed from: a */
    private final C2194a f9428a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Set<View> f9429b;

    /* renamed from: c */
    private final C2195b f9430c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public axf f9431d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public axt f9432e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public View f9433f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public C2196n f9434g;

    /* renamed from: h */
    private C0946b f9435h;

    /* renamed from: i */
    private Bundle f9436i;

    /* renamed from: j */
    private C0945a f9437j;

    /* renamed from: k */
    private boolean f9438k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public boolean f9439l;

    /* renamed from: com.google.android.youtube.player.YouTubePlayerView$a */
    final class C2194a implements OnGlobalFocusChangeListener {
        private C2194a() {
        }

        /* synthetic */ C2194a(YouTubePlayerView youTubePlayerView, byte b) {
            this();
        }

        public final void onGlobalFocusChanged(View view, View view2) {
            if (YouTubePlayerView.this.f9432e != null && YouTubePlayerView.this.f9429b.contains(view2) && !YouTubePlayerView.this.f9429b.contains(view)) {
                YouTubePlayerView.this.f9432e.mo8283g();
            }
        }
    }

    /* renamed from: com.google.android.youtube.player.YouTubePlayerView$b */
    interface C2195b {
        /* renamed from: a */
        void mo12776a(YouTubePlayerView youTubePlayerView);

        /* renamed from: a */
        void mo12777a(YouTubePlayerView youTubePlayerView, String str, C0945a aVar);
    }

    public YouTubePlayerView(Context context) {
        this(context, null);
    }

    public YouTubePlayerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public YouTubePlayerView(Context context, AttributeSet attributeSet, int i) {
        if (!(context instanceof YouTubeBaseActivity)) {
            throw new IllegalStateException("A YouTubePlayerView can only be created with an Activity  which extends YouTubeBaseActivity as its context.");
        }
        this(context, attributeSet, i, ((YouTubeBaseActivity) context).mo12769a());
    }

    YouTubePlayerView(Context context, AttributeSet attributeSet, int i, C2195b bVar) {
        super((Context) axd.m6617a(context, (Object) "context cannot be null"), attributeSet, i);
        this.f9430c = (C2195b) axd.m6617a(bVar, (Object) "listener cannot be null");
        if (getBackground() == null) {
            setBackgroundColor(-16777216);
        }
        setClipToPadding(false);
        this.f9434g = new C2196n(context);
        requestTransparentRegion(this.f9434g);
        addView(this.f9434g);
        this.f9429b = new HashSet();
        this.f9428a = new C2194a(this, 0);
    }

    /* renamed from: a */
    private void m12058a(View view) {
        if (!(view == this.f9434g || (this.f9432e != null && view == this.f9433f))) {
            throw new UnsupportedOperationException("No views can be added on top of the player");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m12059a(YouTubeInitializationResult youTubeInitializationResult) {
        this.f9432e = null;
        this.f9434g.mo12821c();
        if (this.f9437j != null) {
            this.f9437j.onInitializationFailure(this.f9435h, youTubeInitializationResult);
            this.f9437j = null;
        }
    }

    /* renamed from: a */
    static /* synthetic */ void m12060a(YouTubePlayerView youTubePlayerView, Activity activity) {
        try {
            youTubePlayerView.f9432e = new axt(youTubePlayerView.f9431d, axc.m6612a().mo8150a(activity, youTubePlayerView.f9431d, youTubePlayerView.f9438k));
            youTubePlayerView.f9433f = youTubePlayerView.f9432e.mo8269a();
            youTubePlayerView.addView(youTubePlayerView.f9433f);
            youTubePlayerView.removeView(youTubePlayerView.f9434g);
            youTubePlayerView.f9430c.mo12776a(youTubePlayerView);
            if (youTubePlayerView.f9437j != null) {
                boolean z = false;
                if (youTubePlayerView.f9436i != null) {
                    z = youTubePlayerView.f9432e.mo8274a(youTubePlayerView.f9436i);
                    youTubePlayerView.f9436i = null;
                }
                youTubePlayerView.f9437j.onInitializationSuccess(youTubePlayerView.f9435h, youTubePlayerView.f9432e, z);
                youTubePlayerView.f9437j = null;
            }
        } catch (C2199a e) {
            axy.m6834a("Error creating YouTubePlayerView", (Throwable) e);
            youTubePlayerView.m12059a(YouTubeInitializationResult.INTERNAL_ERROR);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public final void mo12788a() {
        if (this.f9432e != null) {
            this.f9432e.mo8275b();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public final void mo12789a(final Activity activity, C0946b bVar, String str, C0945a aVar, Bundle bundle) {
        if (this.f9432e == null && this.f9437j == null) {
            axd.m6617a(activity, (Object) "activity cannot be null");
            this.f9435h = (C0946b) axd.m6617a(bVar, (Object) "provider cannot be null");
            this.f9437j = (C0945a) axd.m6617a(aVar, (Object) "listener cannot be null");
            this.f9436i = bundle;
            this.f9434g.mo12820b();
            this.f9431d = axc.m6612a().mo8149a(getContext(), str, new C0974a() {
                /* renamed from: a */
                public final void mo8285a() {
                    if (YouTubePlayerView.this.f9431d != null) {
                        YouTubePlayerView.m12060a(YouTubePlayerView.this, activity);
                    }
                    YouTubePlayerView.this.f9431d = null;
                }

                /* renamed from: b */
                public final void mo8286b() {
                    if (!YouTubePlayerView.this.f9439l && YouTubePlayerView.this.f9432e != null) {
                        YouTubePlayerView.this.f9432e.mo8282f();
                    }
                    YouTubePlayerView.this.f9434g.mo12819a();
                    if (YouTubePlayerView.this.indexOfChild(YouTubePlayerView.this.f9434g) < 0) {
                        YouTubePlayerView.this.addView(YouTubePlayerView.this.f9434g);
                        YouTubePlayerView.this.removeView(YouTubePlayerView.this.f9433f);
                    }
                    YouTubePlayerView.this.f9433f = null;
                    YouTubePlayerView.this.f9432e = null;
                    YouTubePlayerView.this.f9431d = null;
                }
            }, new C0975b() {
                /* renamed from: a */
                public final void mo8287a(YouTubeInitializationResult youTubeInitializationResult) {
                    YouTubePlayerView.this.m12059a(youTubeInitializationResult);
                    YouTubePlayerView.this.f9431d = null;
                }
            });
            this.f9431d.mo8257e();
        }
    }

    /* renamed from: a */
    public final void mo12790a(String str, C0945a aVar) {
        axd.m6618a(str, (Object) "Developer key cannot be null or empty");
        this.f9430c.mo12777a(this, str, aVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public final void mo12791a(boolean z) {
        if (!z || VERSION.SDK_INT >= 14) {
            this.f9438k = z;
            return;
        }
        axy.m6835a("Could not enable TextureView because API level is lower than 14", new Object[0]);
        this.f9438k = false;
    }

    public final void addFocusables(ArrayList<View> arrayList, int i) {
        ArrayList arrayList2 = new ArrayList();
        super.addFocusables(arrayList2, i);
        arrayList.addAll(arrayList2);
        this.f9429b.clear();
        this.f9429b.addAll(arrayList2);
    }

    public final void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        ArrayList arrayList2 = new ArrayList();
        super.addFocusables(arrayList2, i, i2);
        arrayList.addAll(arrayList2);
        this.f9429b.clear();
        this.f9429b.addAll(arrayList2);
    }

    public final void addView(View view) {
        m12058a(view);
        super.addView(view);
    }

    public final void addView(View view, int i) {
        m12058a(view);
        super.addView(view, i);
    }

    public final void addView(View view, int i, int i2) {
        m12058a(view);
        super.addView(view, i, i2);
    }

    public final void addView(View view, int i, LayoutParams layoutParams) {
        m12058a(view);
        super.addView(view, i, layoutParams);
    }

    public final void addView(View view, LayoutParams layoutParams) {
        m12058a(view);
        super.addView(view, layoutParams);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public final void mo12799b() {
        if (this.f9432e != null) {
            this.f9432e.mo8279c();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public final void mo12800b(boolean z) {
        if (this.f9432e != null) {
            this.f9432e.mo8277b(z);
            mo12802c(z);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public final void mo12801c() {
        if (this.f9432e != null) {
            this.f9432e.mo8280d();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public final void mo12802c(boolean z) {
        this.f9439l = true;
        if (this.f9432e != null) {
            this.f9432e.mo8272a(z);
        }
    }

    public final void clearChildFocus(View view) {
        if (hasFocusable()) {
            requestFocus();
        } else {
            super.clearChildFocus(view);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public final void mo12804d() {
        if (this.f9432e != null) {
            this.f9432e.mo8281e();
        }
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (this.f9432e != null) {
            if (keyEvent.getAction() == 0) {
                return this.f9432e.mo8273a(keyEvent.getKeyCode(), keyEvent) || super.dispatchKeyEvent(keyEvent);
            }
            if (keyEvent.getAction() == 1) {
                return this.f9432e.mo8278b(keyEvent.getKeyCode(), keyEvent) || super.dispatchKeyEvent(keyEvent);
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public final Bundle mo12806e() {
        return this.f9432e == null ? this.f9436i : this.f9432e.mo8284h();
    }

    public final void focusableViewAvailable(View view) {
        super.focusableViewAvailable(view);
        this.f9429b.add(view);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnGlobalFocusChangeListener(this.f9428a);
    }

    public final void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f9432e != null) {
            this.f9432e.mo8270a(configuration);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getViewTreeObserver().removeOnGlobalFocusChangeListener(this.f9428a);
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (getChildCount() > 0) {
            getChildAt(0).layout(0, 0, i3 - i, i4 - i2);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            childAt.measure(i, i2);
            setMeasuredDimension(childAt.getMeasuredWidth(), childAt.getMeasuredHeight());
            return;
        }
        setMeasuredDimension(0, 0);
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }

    public final void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        this.f9429b.add(view2);
    }

    public final void setClipToPadding(boolean z) {
    }

    public final void setPadding(int i, int i2, int i3, int i4) {
    }
}
