package com.google.android.youtube.player.internal;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import java.lang.reflect.InvocationTargetException;

/* renamed from: com.google.android.youtube.player.internal.w */
public final class C2198w {

    /* renamed from: com.google.android.youtube.player.internal.w$a */
    public static final class C2199a extends Exception {
        public C2199a(String str) {
            super(str);
        }

        public C2199a(String str, Throwable th) {
            super(str, th);
        }
    }

    /* renamed from: a */
    private static IBinder m12088a(Class<?> cls, IBinder iBinder, IBinder iBinder2, IBinder iBinder3, boolean z) throws C2199a {
        try {
            return (IBinder) cls.getConstructor(new Class[]{IBinder.class, IBinder.class, IBinder.class, Boolean.TYPE}).newInstance(new Object[]{iBinder, iBinder2, iBinder3, Boolean.valueOf(z)});
        } catch (NoSuchMethodException e) {
            NoSuchMethodException noSuchMethodException = e;
            String str = "Could not find the right constructor for ";
            String valueOf = String.valueOf(cls.getName());
            throw new C2199a(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), noSuchMethodException);
        } catch (InvocationTargetException e2) {
            InvocationTargetException invocationTargetException = e2;
            String str2 = "Exception thrown by invoked constructor in ";
            String valueOf2 = String.valueOf(cls.getName());
            throw new C2199a(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), invocationTargetException);
        } catch (InstantiationException e3) {
            InstantiationException instantiationException = e3;
            String str3 = "Unable to instantiate the dynamic class ";
            String valueOf3 = String.valueOf(cls.getName());
            throw new C2199a(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3), instantiationException);
        } catch (IllegalAccessException e4) {
            IllegalAccessException illegalAccessException = e4;
            String str4 = "Unable to call the default constructor of ";
            String valueOf4 = String.valueOf(cls.getName());
            throw new C2199a(valueOf4.length() != 0 ? str4.concat(valueOf4) : new String(str4), illegalAccessException);
        }
    }

    /* renamed from: a */
    private static IBinder m12089a(ClassLoader classLoader, String str, IBinder iBinder, IBinder iBinder2, IBinder iBinder3, boolean z) throws C2199a {
        try {
            return m12088a(classLoader.loadClass(str), iBinder, iBinder2, iBinder3, z);
        } catch (ClassNotFoundException e) {
            ClassNotFoundException classNotFoundException = e;
            String str2 = "Unable to find dynamic class ";
            String valueOf = String.valueOf(str);
            throw new C2199a(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2), classNotFoundException);
        }
    }

    /* renamed from: a */
    public static axh m12090a(Activity activity, IBinder iBinder, boolean z) throws C2199a {
        axd.m6616a(activity);
        axd.m6616a(iBinder);
        Context b = axz.m6838b(activity);
        if (b != null) {
            return C0950a.m6669a(m12089a(b.getClassLoader(), "com.google.android.youtube.api.jar.client.RemoteEmbeddedPlayer", axw.m6830a(b).asBinder(), axw.m6830a(activity).asBinder(), iBinder, z));
        }
        throw new C2199a("Could not create remote context");
    }
}
