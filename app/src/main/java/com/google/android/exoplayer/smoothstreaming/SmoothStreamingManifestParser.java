package com.google.android.exoplayer.smoothstreaming;

import android.util.Base64;
import android.util.Pair;
import com.google.android.exoplayer.ParserException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class SmoothStreamingManifestParser implements C0287a<aif> {

    /* renamed from: a */
    private final XmlPullParserFactory f8735a;

    public static class MissingFieldException extends ParserException {
        public MissingFieldException(String str) {
            super("Missing required field: " + str);
        }
    }

    /* renamed from: com.google.android.exoplayer.smoothstreaming.SmoothStreamingManifestParser$a */
    static abstract class C2121a {

        /* renamed from: a */
        private final String f8736a;

        /* renamed from: b */
        private final String f8737b;

        /* renamed from: c */
        private final C2121a f8738c;

        /* renamed from: d */
        private final List<Pair<String, Object>> f8739d = new LinkedList();

        public C2121a(C2121a aVar, String str, String str2) {
            this.f8738c = aVar;
            this.f8736a = str;
            this.f8737b = str2;
        }

        /* renamed from: a */
        private C2121a m11339a(C2121a aVar, String str, String str2) {
            if ("QualityLevel".equals(str)) {
                return new C2125e(aVar, str2);
            }
            if ("Protection".equals(str)) {
                return new C2122b(aVar, str2);
            }
            if ("StreamIndex".equals(str)) {
                return new C2124d(aVar, str2);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final int mo12109a(XmlPullParser xmlPullParser, String str, int i) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue(null, str);
            if (attributeValue == null) {
                return i;
            }
            try {
                return Integer.parseInt(attributeValue);
            } catch (NumberFormatException e) {
                throw new ParserException((Throwable) e);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final long mo12110a(XmlPullParser xmlPullParser, String str, long j) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue(null, str);
            if (attributeValue == null) {
                return j;
            }
            try {
                return Long.parseLong(attributeValue);
            } catch (NumberFormatException e) {
                throw new ParserException((Throwable) e);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abstract Object mo12111a();

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final Object mo12112a(String str) {
            for (int i = 0; i < this.f8739d.size(); i++) {
                Pair pair = (Pair) this.f8739d.get(i);
                if (((String) pair.first).equals(str)) {
                    return pair.second;
                }
            }
            if (this.f8738c == null) {
                return null;
            }
            return this.f8738c.mo12112a(str);
        }

        /* renamed from: a */
        public final Object mo12113a(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException, ParserException {
            boolean z = false;
            int i = 0;
            while (true) {
                switch (xmlPullParser.getEventType()) {
                    case 1:
                        return null;
                    case 2:
                        String name = xmlPullParser.getName();
                        if (!this.f8737b.equals(name)) {
                            if (z) {
                                if (i <= 0) {
                                    if (!mo12120b(name)) {
                                        C2121a a = m11339a(this, name, this.f8736a);
                                        if (a != null) {
                                            mo12115a(a.mo12113a(xmlPullParser));
                                            break;
                                        } else {
                                            i = 1;
                                            break;
                                        }
                                    } else {
                                        mo12119b(xmlPullParser);
                                        break;
                                    }
                                } else {
                                    i++;
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            z = true;
                            mo12119b(xmlPullParser);
                            break;
                        }
                    case 3:
                        if (z) {
                            if (i <= 0) {
                                String name2 = xmlPullParser.getName();
                                mo12123d(xmlPullParser);
                                if (mo12120b(name2)) {
                                    break;
                                } else {
                                    return mo12111a();
                                }
                            } else {
                                i--;
                                break;
                            }
                        } else {
                            continue;
                        }
                    case 4:
                        if (z && i == 0) {
                            mo12122c(xmlPullParser);
                            break;
                        }
                }
                xmlPullParser.next();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final String mo12114a(XmlPullParser xmlPullParser, String str) throws MissingFieldException {
            String attributeValue = xmlPullParser.getAttributeValue(null, str);
            if (attributeValue != null) {
                return attributeValue;
            }
            throw new MissingFieldException(str);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo12115a(Object obj) {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo12116a(String str, Object obj) {
            this.f8739d.add(Pair.create(str, obj));
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final boolean mo12117a(XmlPullParser xmlPullParser, String str, boolean z) {
            String attributeValue = xmlPullParser.getAttributeValue(null, str);
            return attributeValue != null ? Boolean.parseBoolean(attributeValue) : z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public final int mo12118b(XmlPullParser xmlPullParser, String str) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue(null, str);
            if (attributeValue != null) {
                try {
                    return Integer.parseInt(attributeValue);
                } catch (NumberFormatException e) {
                    throw new ParserException((Throwable) e);
                }
            } else {
                throw new MissingFieldException(str);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void mo12119b(XmlPullParser xmlPullParser) throws ParserException {
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public boolean mo12120b(String str) {
            return false;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public final long mo12121c(XmlPullParser xmlPullParser, String str) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue(null, str);
            if (attributeValue != null) {
                try {
                    return Long.parseLong(attributeValue);
                } catch (NumberFormatException e) {
                    throw new ParserException((Throwable) e);
                }
            } else {
                throw new MissingFieldException(str);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void mo12122c(XmlPullParser xmlPullParser) throws ParserException {
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public void mo12123d(XmlPullParser xmlPullParser) throws ParserException {
        }
    }

    /* renamed from: com.google.android.exoplayer.smoothstreaming.SmoothStreamingManifestParser$b */
    static class C2122b extends C2121a {

        /* renamed from: a */
        private boolean f8740a;

        /* renamed from: b */
        private UUID f8741b;

        /* renamed from: c */
        private byte[] f8742c;

        public C2122b(C2121a aVar, String str) {
            super(aVar, str, "Protection");
        }

        /* renamed from: c */
        private static String m11355c(String str) {
            return (str.charAt(0) == '{' && str.charAt(str.length() + -1) == '}') ? str.substring(1, str.length() - 1) : str;
        }

        /* renamed from: a */
        public Object mo12111a() {
            return new C0274a(this.f8741b, afr.m1690a(this.f8741b, this.f8742c));
        }

        /* renamed from: b */
        public void mo12119b(XmlPullParser xmlPullParser) {
            if ("ProtectionHeader".equals(xmlPullParser.getName())) {
                this.f8740a = true;
                this.f8741b = UUID.fromString(m11355c(xmlPullParser.getAttributeValue(null, "SystemID")));
            }
        }

        /* renamed from: b */
        public boolean mo12120b(String str) {
            return "ProtectionHeader".equals(str);
        }

        /* renamed from: c */
        public void mo12122c(XmlPullParser xmlPullParser) {
            if (this.f8740a) {
                this.f8742c = Base64.decode(xmlPullParser.getText(), 0);
            }
        }

        /* renamed from: d */
        public void mo12123d(XmlPullParser xmlPullParser) {
            if ("ProtectionHeader".equals(xmlPullParser.getName())) {
                this.f8740a = false;
            }
        }
    }

    /* renamed from: com.google.android.exoplayer.smoothstreaming.SmoothStreamingManifestParser$c */
    static class C2123c extends C2121a {

        /* renamed from: a */
        private int f8743a;

        /* renamed from: b */
        private int f8744b;

        /* renamed from: c */
        private long f8745c;

        /* renamed from: d */
        private long f8746d;

        /* renamed from: e */
        private long f8747e;

        /* renamed from: f */
        private int f8748f = -1;

        /* renamed from: g */
        private boolean f8749g;

        /* renamed from: h */
        private C0274a f8750h = null;

        /* renamed from: i */
        private List<C0275b> f8751i = new LinkedList();

        public C2123c(C2121a aVar, String str) {
            super(aVar, str, "SmoothStreamingMedia");
        }

        /* renamed from: a */
        public Object mo12111a() {
            C0275b[] bVarArr = new C0275b[this.f8751i.size()];
            this.f8751i.toArray(bVarArr);
            return new aif(this.f8743a, this.f8744b, this.f8745c, this.f8746d, this.f8747e, this.f8748f, this.f8749g, this.f8750h, bVarArr);
        }

        /* renamed from: a */
        public void mo12115a(Object obj) {
            if (obj instanceof C0275b) {
                this.f8751i.add((C0275b) obj);
            } else if (obj instanceof C0274a) {
                akc.m2451b(this.f8750h == null);
                this.f8750h = (C0274a) obj;
            }
        }

        /* renamed from: b */
        public void mo12119b(XmlPullParser xmlPullParser) throws ParserException {
            this.f8743a = mo12118b(xmlPullParser, "MajorVersion");
            this.f8744b = mo12118b(xmlPullParser, "MinorVersion");
            this.f8745c = mo12110a(xmlPullParser, "TimeScale", 10000000);
            this.f8746d = mo12121c(xmlPullParser, "Duration");
            this.f8747e = mo12110a(xmlPullParser, "DVRWindowLength", 0);
            this.f8748f = mo12109a(xmlPullParser, "LookaheadCount", -1);
            this.f8749g = mo12117a(xmlPullParser, "IsLive", false);
            mo12116a("TimeScale", (Object) Long.valueOf(this.f8745c));
        }
    }

    /* renamed from: com.google.android.exoplayer.smoothstreaming.SmoothStreamingManifestParser$d */
    static class C2124d extends C2121a {

        /* renamed from: a */
        private final String f8752a;

        /* renamed from: b */
        private final List<C0276c> f8753b = new LinkedList();

        /* renamed from: c */
        private int f8754c;

        /* renamed from: d */
        private String f8755d;

        /* renamed from: e */
        private long f8756e;

        /* renamed from: f */
        private String f8757f;

        /* renamed from: g */
        private int f8758g;

        /* renamed from: h */
        private String f8759h;

        /* renamed from: i */
        private int f8760i;

        /* renamed from: j */
        private int f8761j;

        /* renamed from: k */
        private int f8762k;

        /* renamed from: l */
        private int f8763l;

        /* renamed from: m */
        private String f8764m;

        /* renamed from: n */
        private ArrayList<Long> f8765n;

        /* renamed from: o */
        private long f8766o;

        public C2124d(C2121a aVar, String str) {
            super(aVar, str, "StreamIndex");
            this.f8752a = str;
        }

        /* renamed from: e */
        private void m11364e(XmlPullParser xmlPullParser) throws ParserException {
            int size = this.f8765n.size();
            long a = mo12110a(xmlPullParser, "t", -1);
            if (a == -1) {
                if (size == 0) {
                    a = 0;
                } else if (this.f8766o != -1) {
                    a = ((Long) this.f8765n.get(size - 1)).longValue() + this.f8766o;
                } else {
                    throw new ParserException("Unable to infer start time");
                }
            }
            int i = size + 1;
            this.f8765n.add(Long.valueOf(a));
            this.f8766o = mo12110a(xmlPullParser, "d", -1);
            long a2 = mo12110a(xmlPullParser, "r", 1);
            if (a2 <= 1 || this.f8766o != -1) {
                for (int i2 = 1; ((long) i2) < a2; i2++) {
                    i++;
                    this.f8765n.add(Long.valueOf((this.f8766o * ((long) i2)) + a));
                }
                return;
            }
            throw new ParserException("Repeated chunk with unspecified duration");
        }

        /* renamed from: f */
        private void m11365f(XmlPullParser xmlPullParser) throws ParserException {
            this.f8754c = m11366g(xmlPullParser);
            mo12116a("Type", (Object) Integer.valueOf(this.f8754c));
            if (this.f8754c == 2) {
                this.f8755d = mo12114a(xmlPullParser, "Subtype");
            } else {
                this.f8755d = xmlPullParser.getAttributeValue(null, "Subtype");
            }
            this.f8757f = xmlPullParser.getAttributeValue(null, "Name");
            this.f8758g = mo12109a(xmlPullParser, "QualityLevels", -1);
            this.f8759h = mo12114a(xmlPullParser, "Url");
            this.f8760i = mo12109a(xmlPullParser, "MaxWidth", -1);
            this.f8761j = mo12109a(xmlPullParser, "MaxHeight", -1);
            this.f8762k = mo12109a(xmlPullParser, "DisplayWidth", -1);
            this.f8763l = mo12109a(xmlPullParser, "DisplayHeight", -1);
            this.f8764m = xmlPullParser.getAttributeValue(null, "Language");
            mo12116a("Language", (Object) this.f8764m);
            this.f8756e = (long) mo12109a(xmlPullParser, "TimeScale", -1);
            if (this.f8756e == -1) {
                this.f8756e = ((Long) mo12112a("TimeScale")).longValue();
            }
            this.f8765n = new ArrayList<>();
        }

        /* renamed from: g */
        private int m11366g(XmlPullParser xmlPullParser) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue(null, "Type");
            if (attributeValue == null) {
                throw new MissingFieldException("Type");
            } else if ("audio".equalsIgnoreCase(attributeValue)) {
                return 0;
            } else {
                if ("video".equalsIgnoreCase(attributeValue)) {
                    return 1;
                }
                if ("text".equalsIgnoreCase(attributeValue)) {
                    return 2;
                }
                throw new ParserException("Invalid key value[" + attributeValue + "]");
            }
        }

        /* renamed from: a */
        public Object mo12111a() {
            C0276c[] cVarArr = new C0276c[this.f8753b.size()];
            this.f8753b.toArray(cVarArr);
            return new C0275b(this.f8752a, this.f8759h, this.f8754c, this.f8755d, this.f8756e, this.f8757f, this.f8758g, this.f8760i, this.f8761j, this.f8762k, this.f8763l, this.f8764m, cVarArr, this.f8765n, this.f8766o);
        }

        /* renamed from: a */
        public void mo12115a(Object obj) {
            if (obj instanceof C0276c) {
                this.f8753b.add((C0276c) obj);
            }
        }

        /* renamed from: b */
        public void mo12119b(XmlPullParser xmlPullParser) throws ParserException {
            if ("c".equals(xmlPullParser.getName())) {
                m11364e(xmlPullParser);
            } else {
                m11365f(xmlPullParser);
            }
        }

        /* renamed from: b */
        public boolean mo12120b(String str) {
            return "c".equals(str);
        }
    }

    /* renamed from: com.google.android.exoplayer.smoothstreaming.SmoothStreamingManifestParser$e */
    static class C2125e extends C2121a {

        /* renamed from: a */
        private final List<byte[]> f8767a = new LinkedList();

        /* renamed from: b */
        private int f8768b;

        /* renamed from: c */
        private int f8769c;

        /* renamed from: d */
        private String f8770d;

        /* renamed from: e */
        private int f8771e;

        /* renamed from: f */
        private int f8772f;

        /* renamed from: g */
        private int f8773g;

        /* renamed from: h */
        private int f8774h;

        /* renamed from: i */
        private String f8775i;

        public C2125e(C2121a aVar, String str) {
            super(aVar, str, "QualityLevel");
        }

        /* renamed from: c */
        private static String m11371c(String str) {
            if (str.equalsIgnoreCase("H264") || str.equalsIgnoreCase("X264") || str.equalsIgnoreCase("AVC1") || str.equalsIgnoreCase("DAVC")) {
                return "video/avc";
            }
            if (str.equalsIgnoreCase("AAC") || str.equalsIgnoreCase("AACL") || str.equalsIgnoreCase("AACH") || str.equalsIgnoreCase("AACP")) {
                return "audio/mp4a-latm";
            }
            if (str.equalsIgnoreCase("TTML")) {
                return "application/ttml+xml";
            }
            if (str.equalsIgnoreCase("ac-3") || str.equalsIgnoreCase("dac3")) {
                return "audio/ac3";
            }
            if (str.equalsIgnoreCase("ec-3") || str.equalsIgnoreCase("dec3")) {
                return "audio/eac3";
            }
            if (str.equalsIgnoreCase("dtsc")) {
                return "audio/vnd.dts";
            }
            if (str.equalsIgnoreCase("dtsh") || str.equalsIgnoreCase("dtsl")) {
                return "audio/vnd.dts.hd";
            }
            if (str.equalsIgnoreCase("dtse")) {
                return "audio/vnd.dts.hd;profile=lbr";
            }
            if (str.equalsIgnoreCase("opus")) {
                return "audio/opus";
            }
            return null;
        }

        /* renamed from: a */
        public Object mo12111a() {
            byte[][] bArr = null;
            if (!this.f8767a.isEmpty()) {
                bArr = new byte[this.f8767a.size()][];
                this.f8767a.toArray(bArr);
            }
            return new C0276c(this.f8768b, this.f8769c, this.f8770d, bArr, this.f8771e, this.f8772f, this.f8773g, this.f8774h, this.f8775i);
        }

        /* renamed from: b */
        public void mo12119b(XmlPullParser xmlPullParser) throws ParserException {
            int intValue = ((Integer) mo12112a("Type")).intValue();
            this.f8768b = mo12109a(xmlPullParser, "Index", -1);
            this.f8769c = mo12118b(xmlPullParser, "Bitrate");
            this.f8775i = (String) mo12112a("Language");
            if (intValue == 1) {
                this.f8772f = mo12118b(xmlPullParser, "MaxHeight");
                this.f8771e = mo12118b(xmlPullParser, "MaxWidth");
                this.f8770d = m11371c(mo12114a(xmlPullParser, "FourCC"));
            } else {
                this.f8772f = -1;
                this.f8771e = -1;
                String attributeValue = xmlPullParser.getAttributeValue(null, "FourCC");
                String str = attributeValue != null ? m11371c(attributeValue) : intValue == 0 ? "audio/mp4a-latm" : null;
                this.f8770d = str;
            }
            if (intValue == 0) {
                this.f8773g = mo12118b(xmlPullParser, "SamplingRate");
                this.f8774h = mo12118b(xmlPullParser, "Channels");
            } else {
                this.f8773g = -1;
                this.f8774h = -1;
            }
            String attributeValue2 = xmlPullParser.getAttributeValue(null, "CodecPrivateData");
            if (attributeValue2 != null && attributeValue2.length() > 0) {
                byte[] f = akw.m2595f(attributeValue2);
                byte[][] b = ake.m2460b(f);
                if (b == null) {
                    this.f8767a.add(f);
                    return;
                }
                for (byte[] add : b) {
                    this.f8767a.add(add);
                }
            }
        }
    }

    public SmoothStreamingManifestParser() {
        try {
            this.f8735a = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    /* renamed from: a */
    public aif mo592b(String str, InputStream inputStream) throws IOException, ParserException {
        try {
            XmlPullParser newPullParser = this.f8735a.newPullParser();
            newPullParser.setInput(inputStream, null);
            return (aif) new C2123c(null, str).mo12113a(newPullParser);
        } catch (XmlPullParserException e) {
            throw new ParserException((Throwable) e);
        }
    }
}
