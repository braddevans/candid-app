package com.google.android.exoplayer.extractor.flv;

import com.google.android.exoplayer.ParserException;

public abstract class TagPayloadReader {

    /* renamed from: a */
    protected final afb f8733a;

    /* renamed from: b */
    private long f8734b = -1;

    public static final class UnsupportedFormatException extends ParserException {
        public UnsupportedFormatException(String str) {
            super(str);
        }
    }

    protected TagPayloadReader(afb afb) {
        this.f8733a = afb;
    }

    /* renamed from: a */
    public final long mo12105a() {
        return this.f8734b;
    }

    /* renamed from: a */
    public final void mo12106a(long j) {
        this.f8734b = j;
    }

    /* renamed from: a */
    public abstract void mo722a(ako ako, long j) throws ParserException;

    /* renamed from: a */
    public abstract boolean mo723a(ako ako) throws ParserException;

    /* renamed from: b */
    public final void mo12107b(ako ako, long j) throws ParserException {
        if (mo723a(ako)) {
            mo722a(ako, j);
        }
    }
}
