package com.google.android.exoplayer.upstream;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ContentDataSource implements ajz {

    /* renamed from: a */
    private final ContentResolver f8789a;

    /* renamed from: b */
    private final ajy f8790b;

    /* renamed from: c */
    private AssetFileDescriptor f8791c;

    /* renamed from: d */
    private InputStream f8792d;

    /* renamed from: e */
    private String f8793e;

    /* renamed from: f */
    private long f8794f;

    /* renamed from: g */
    private boolean f8795g;

    public static class ContentDataSourceException extends IOException {
        public ContentDataSourceException(IOException iOException) {
            super(iOException);
        }
    }

    public ContentDataSource(Context context, ajy ajy) {
        this.f8789a = context.getContentResolver();
        this.f8790b = ajy;
    }

    /* renamed from: a */
    public int mo836a(byte[] bArr, int i, int i2) throws ContentDataSourceException {
        if (this.f8794f == 0) {
            return -1;
        }
        try {
            int read = this.f8792d.read(bArr, i, this.f8794f == -1 ? i2 : (int) Math.min(this.f8794f, (long) i2));
            if (read <= 0) {
                return read;
            }
            if (this.f8794f != -1) {
                this.f8794f -= (long) read;
            }
            if (this.f8790b == null) {
                return read;
            }
            this.f8790b.mo992a(read);
            return read;
        } catch (IOException e) {
            throw new ContentDataSourceException(e);
        }
    }

    /* renamed from: a */
    public long mo837a(ajt ajt) throws ContentDataSourceException {
        try {
            this.f8793e = ajt.f1968a.toString();
            this.f8791c = this.f8789a.openAssetFileDescriptor(ajt.f1968a, "r");
            this.f8792d = new FileInputStream(this.f8791c.getFileDescriptor());
            if (this.f8792d.skip(ajt.f1971d) < ajt.f1971d) {
                throw new EOFException();
            }
            if (ajt.f1972e != -1) {
                this.f8794f = ajt.f1972e;
            } else {
                this.f8794f = (long) this.f8792d.available();
                if (this.f8794f == 0) {
                    this.f8794f = -1;
                }
            }
            this.f8795g = true;
            if (this.f8790b != null) {
                this.f8790b.mo993b();
            }
            return this.f8794f;
        } catch (IOException e) {
            throw new ContentDataSourceException(e);
        }
    }

    /* renamed from: a */
    public void mo838a() throws ContentDataSourceException {
        this.f8793e = null;
        try {
            if (this.f8792d != null) {
                this.f8792d.close();
            }
            this.f8792d = null;
            try {
                if (this.f8791c != null) {
                    this.f8791c.close();
                }
                this.f8791c = null;
                if (this.f8795g) {
                    this.f8795g = false;
                    if (this.f8790b != null) {
                        this.f8790b.mo994c();
                    }
                }
            } catch (IOException e) {
                throw new ContentDataSourceException(e);
            } catch (Throwable th) {
                this.f8791c = null;
                if (this.f8795g) {
                    this.f8795g = false;
                    if (this.f8790b != null) {
                        this.f8790b.mo994c();
                    }
                }
                throw th;
            }
        } catch (IOException e2) {
            throw new ContentDataSourceException(e2);
        } catch (Throwable th2) {
            this.f8792d = null;
            try {
                if (this.f8791c != null) {
                    this.f8791c.close();
                }
                this.f8791c = null;
                if (this.f8795g) {
                    this.f8795g = false;
                    if (this.f8790b != null) {
                        this.f8790b.mo994c();
                    }
                }
                throw th2;
            } catch (IOException e3) {
                throw new ContentDataSourceException(e3);
            } catch (Throwable th3) {
                this.f8791c = null;
                if (this.f8795g) {
                    this.f8795g = false;
                    if (this.f8790b != null) {
                        this.f8790b.mo994c();
                    }
                }
                throw th3;
            }
        }
    }

    /* renamed from: b */
    public String mo996b() {
        return this.f8793e;
    }
}
