package com.google.android.exoplayer;

public final class ExoPlaybackException extends Exception {

    /* renamed from: a */
    public final boolean f8490a;

    public ExoPlaybackException(String str) {
        super(str);
        this.f8490a = false;
    }

    public ExoPlaybackException(Throwable th) {
        super(th);
        this.f8490a = false;
    }

    public ExoPlaybackException(Throwable th, boolean z) {
        super(th);
        this.f8490a = z;
    }
}
