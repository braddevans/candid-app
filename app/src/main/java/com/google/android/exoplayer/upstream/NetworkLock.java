package com.google.android.exoplayer.upstream;

import java.io.IOException;
import java.util.PriorityQueue;

public final class NetworkLock {

    /* renamed from: a */
    public static final NetworkLock f8814a = new NetworkLock();

    /* renamed from: b */
    private final Object f8815b = new Object();

    /* renamed from: c */
    private final PriorityQueue<Integer> f8816c = new PriorityQueue<>();

    /* renamed from: d */
    private int f8817d = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;

    public static class PriorityTooLowException extends IOException {
    }

    private NetworkLock() {
    }

    /* renamed from: a */
    public void mo12142a(int i) {
        synchronized (this.f8815b) {
            this.f8816c.add(Integer.valueOf(i));
            this.f8817d = Math.min(this.f8817d, i);
        }
    }

    /* renamed from: b */
    public void mo12143b(int i) {
        synchronized (this.f8815b) {
            this.f8816c.remove(Integer.valueOf(i));
            this.f8817d = this.f8816c.isEmpty() ? ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED : ((Integer) this.f8816c.peek()).intValue();
            this.f8815b.notifyAll();
        }
    }
}
