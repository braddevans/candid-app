package com.google.android.exoplayer.extractor;

import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.util.SparseArray;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.ParserException;
import com.google.android.exoplayer.upstream.Loader;
import com.google.android.exoplayer.upstream.Loader.C2127a;
import com.google.android.exoplayer.upstream.Loader.C2129c;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class ExtractorSampleSource implements acu, C0157a, aew, C2127a {

    /* renamed from: a */
    private static final List<Class<? extends aeu>> f8682a = new ArrayList();

    /* renamed from: A */
    private long f8683A;

    /* renamed from: B */
    private long f8684B;

    /* renamed from: C */
    private Loader f8685C;

    /* renamed from: D */
    private C2118b f8686D;

    /* renamed from: E */
    private IOException f8687E;

    /* renamed from: F */
    private int f8688F;

    /* renamed from: G */
    private long f8689G;

    /* renamed from: H */
    private boolean f8690H;
    /* access modifiers changed from: private */

    /* renamed from: I */
    public int f8691I;

    /* renamed from: J */
    private int f8692J;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final C2119c f8693b;

    /* renamed from: c */
    private final ajp f8694c;

    /* renamed from: d */
    private final int f8695d;

    /* renamed from: e */
    private final SparseArray<C2120d> f8696e;

    /* renamed from: f */
    private final int f8697f;

    /* renamed from: g */
    private final Uri f8698g;

    /* renamed from: h */
    private final ajr f8699h;

    /* renamed from: i */
    private final Handler f8700i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public final C2117a f8701j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public final int f8702k;

    /* renamed from: l */
    private volatile boolean f8703l;

    /* renamed from: m */
    private volatile afa f8704m;

    /* renamed from: n */
    private volatile aei f8705n;

    /* renamed from: o */
    private boolean f8706o;

    /* renamed from: p */
    private int f8707p;

    /* renamed from: q */
    private MediaFormat[] f8708q;

    /* renamed from: r */
    private long f8709r;

    /* renamed from: s */
    private boolean[] f8710s;

    /* renamed from: t */
    private boolean[] f8711t;

    /* renamed from: u */
    private boolean[] f8712u;

    /* renamed from: v */
    private int f8713v;

    /* renamed from: w */
    private long f8714w;

    /* renamed from: x */
    private long f8715x;

    /* renamed from: y */
    private long f8716y;

    /* renamed from: z */
    private boolean f8717z;

    public static final class UnrecognizedInputFormatException extends ParserException {
        public UnrecognizedInputFormatException(aeu[] aeuArr) {
            super("None of the available extractors (" + akw.m2573a((T[]) aeuArr) + ") could read the stream.");
        }
    }

    /* renamed from: com.google.android.exoplayer.extractor.ExtractorSampleSource$a */
    public interface C2117a {
        /* renamed from: a */
        void mo484a(int i, IOException iOException);
    }

    /* renamed from: com.google.android.exoplayer.extractor.ExtractorSampleSource$b */
    static class C2118b implements C2129c {

        /* renamed from: a */
        private final Uri f8721a;

        /* renamed from: b */
        private final ajr f8722b;

        /* renamed from: c */
        private final C2119c f8723c;

        /* renamed from: d */
        private final ajp f8724d;

        /* renamed from: e */
        private final int f8725e;

        /* renamed from: f */
        private final aey f8726f = new aey();

        /* renamed from: g */
        private volatile boolean f8727g;

        /* renamed from: h */
        private boolean f8728h;

        public C2118b(Uri uri, ajr ajr, C2119c cVar, ajp ajp, int i, long j) {
            this.f8721a = (Uri) akc.m2447a(uri);
            this.f8722b = (ajr) akc.m2447a(ajr);
            this.f8723c = (C2119c) akc.m2447a(cVar);
            this.f8724d = (ajp) akc.m2447a(ajp);
            this.f8725e = i;
            this.f8726f.f824a = j;
            this.f8728h = true;
        }

        /* renamed from: f */
        public void mo527f() {
            this.f8727g = true;
        }

        /* renamed from: g */
        public boolean mo528g() {
            return this.f8727g;
        }

        /* JADX WARNING: Removed duplicated region for block: B:28:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x006f  */
        /* renamed from: h */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo529h() throws java.io.IOException, java.lang.InterruptedException {
            /*
                r12 = this;
                r9 = 0
            L_0x0001:
                if (r9 != 0) goto L_0x007a
                boolean r1 = r12.f8727g
                if (r1 != 0) goto L_0x007a
                r8 = 0
                aey r1 = r12.f8726f     // Catch:{ all -> 0x0063 }
                long r2 = r1.f824a     // Catch:{ all -> 0x0063 }
                ajr r10 = r12.f8722b     // Catch:{ all -> 0x0063 }
                ajt r0 = new ajt     // Catch:{ all -> 0x0063 }
                android.net.Uri r1 = r12.f8721a     // Catch:{ all -> 0x0063 }
                r4 = -1
                r6 = 0
                r0.<init>(r1, r2, r4, r6)     // Catch:{ all -> 0x0063 }
                long r4 = r10.mo837a(r0)     // Catch:{ all -> 0x0063 }
                r10 = -1
                int r1 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
                if (r1 == 0) goto L_0x0023
                long r4 = r4 + r2
            L_0x0023:
                aer r0 = new aer     // Catch:{ all -> 0x0063 }
                ajr r1 = r12.f8722b     // Catch:{ all -> 0x0063 }
                r0.<init>(r1, r2, r4)     // Catch:{ all -> 0x0063 }
                com.google.android.exoplayer.extractor.ExtractorSampleSource$c r1 = r12.f8723c     // Catch:{ all -> 0x007b }
                aeu r7 = r1.mo12103a(r0)     // Catch:{ all -> 0x007b }
                boolean r1 = r12.f8728h     // Catch:{ all -> 0x007b }
                if (r1 == 0) goto L_0x003a
                r7.mo699b()     // Catch:{ all -> 0x007b }
                r1 = 0
                r12.f8728h = r1     // Catch:{ all -> 0x007b }
            L_0x003a:
                if (r9 != 0) goto L_0x004e
                boolean r1 = r12.f8727g     // Catch:{ all -> 0x007b }
                if (r1 != 0) goto L_0x004e
                ajp r1 = r12.f8724d     // Catch:{ all -> 0x007b }
                int r6 = r12.f8725e     // Catch:{ all -> 0x007b }
                r1.mo981b(r6)     // Catch:{ all -> 0x007b }
                aey r1 = r12.f8726f     // Catch:{ all -> 0x007b }
                int r9 = r7.mo696a(r0, r1)     // Catch:{ all -> 0x007b }
                goto L_0x003a
            L_0x004e:
                r1 = 1
                if (r9 != r1) goto L_0x0058
                r9 = 0
            L_0x0052:
                ajr r1 = r12.f8722b
                r1.mo838a()
                goto L_0x0001
            L_0x0058:
                if (r0 == 0) goto L_0x0052
                aey r1 = r12.f8726f
                long r10 = r0.mo679c()
                r1.f824a = r10
                goto L_0x0052
            L_0x0063:
                r1 = move-exception
                r0 = r8
            L_0x0065:
                r6 = 1
                if (r9 != r6) goto L_0x006f
                r9 = 0
            L_0x0069:
                ajr r6 = r12.f8722b
                r6.mo838a()
                throw r1
            L_0x006f:
                if (r0 == 0) goto L_0x0069
                aey r6 = r12.f8726f
                long r10 = r0.mo679c()
                r6.f824a = r10
                goto L_0x0069
            L_0x007a:
                return
            L_0x007b:
                r1 = move-exception
                goto L_0x0065
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer.extractor.ExtractorSampleSource.C2118b.mo529h():void");
        }
    }

    /* renamed from: com.google.android.exoplayer.extractor.ExtractorSampleSource$c */
    static final class C2119c {

        /* renamed from: a */
        private final aeu[] f8729a;

        /* renamed from: b */
        private final aew f8730b;

        /* renamed from: c */
        private aeu f8731c;

        public C2119c(aeu[] aeuArr, aew aew) {
            this.f8729a = aeuArr;
            this.f8730b = aew;
        }

        /* renamed from: a */
        public aeu mo12103a(aev aev) throws UnrecognizedInputFormatException, IOException, InterruptedException {
            if (this.f8731c != null) {
                return this.f8731c;
            }
            aeu[] aeuArr = this.f8729a;
            int length = aeuArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                aeu aeu = aeuArr[i];
                try {
                    if (aeu.mo698a(aev)) {
                        this.f8731c = aeu;
                        aev.mo671a();
                        break;
                    }
                    i++;
                } catch (EOFException e) {
                } finally {
                    aev.mo671a();
                }
            }
            if (this.f8731c == null) {
                throw new UnrecognizedInputFormatException(this.f8729a);
            }
            this.f8731c.mo697a(this.f8730b);
            return this.f8731c;
        }

        /* renamed from: a */
        public void mo12104a() {
            if (this.f8731c != null) {
                this.f8731c.mo700c();
                this.f8731c = null;
            }
        }
    }

    /* renamed from: com.google.android.exoplayer.extractor.ExtractorSampleSource$d */
    class C2120d extends aes {
        public C2120d(ajp ajp) {
            super(ajp);
        }

        /* renamed from: a */
        public void mo496a(long j, int i, int i2, int i3, byte[] bArr) {
            super.mo496a(j, i, i2, i3, bArr);
            ExtractorSampleSource.this.f8691I = ExtractorSampleSource.this.f8691I + 1;
        }
    }

    static {
        try {
            f8682a.add(Class.forName("ahd").asSubclass(aeu.class));
        } catch (ClassNotFoundException e) {
        }
        try {
            f8682a.add(Class.forName("afp").asSubclass(aeu.class));
        } catch (ClassNotFoundException e2) {
        }
        try {
            f8682a.add(Class.forName("afq").asSubclass(aeu.class));
        } catch (ClassNotFoundException e3) {
        }
        try {
            f8682a.add(Class.forName("afi").asSubclass(aeu.class));
        } catch (ClassNotFoundException e4) {
        }
        try {
            f8682a.add(Class.forName("agh").asSubclass(aeu.class));
        } catch (ClassNotFoundException e5) {
        }
        try {
            f8682a.add(Class.forName("agu").asSubclass(aeu.class));
        } catch (ClassNotFoundException e6) {
        }
        try {
            f8682a.add(Class.forName("afd").asSubclass(aeu.class));
        } catch (ClassNotFoundException e7) {
        }
        try {
            f8682a.add(Class.forName("afy").asSubclass(aeu.class));
        } catch (ClassNotFoundException e8) {
        }
        try {
            f8682a.add(Class.forName("agr").asSubclass(aeu.class));
        } catch (ClassNotFoundException e9) {
        }
        try {
            f8682a.add(Class.forName("agv").asSubclass(aeu.class));
        } catch (ClassNotFoundException e10) {
        }
        try {
            f8682a.add(Class.forName("com.google.android.exoplayer.ext.flac.FlacExtractor").asSubclass(aeu.class));
        } catch (ClassNotFoundException e11) {
        }
    }

    public ExtractorSampleSource(Uri uri, ajr ajr, ajp ajp, int i, int i2, Handler handler, C2117a aVar, int i3, aeu... aeuArr) {
        this.f8698g = uri;
        this.f8699h = ajr;
        this.f8701j = aVar;
        this.f8700i = handler;
        this.f8702k = i3;
        this.f8694c = ajp;
        this.f8695d = i;
        this.f8697f = i2;
        if (aeuArr == null || aeuArr.length == 0) {
            aeuArr = new aeu[f8682a.size()];
            int i4 = 0;
            while (i4 < aeuArr.length) {
                try {
                    aeuArr[i4] = (aeu) ((Class) f8682a.get(i4)).newInstance();
                    i4++;
                } catch (InstantiationException e) {
                    throw new IllegalStateException("Unexpected error creating default extractor", e);
                } catch (IllegalAccessException e2) {
                    throw new IllegalStateException("Unexpected error creating default extractor", e2);
                }
            }
        }
        this.f8693b = new C2119c(aeuArr, this);
        this.f8696e = new SparseArray<>();
        this.f8716y = Long.MIN_VALUE;
    }

    public ExtractorSampleSource(Uri uri, ajr ajr, ajp ajp, int i, Handler handler, C2117a aVar, int i2, aeu... aeuArr) {
        this(uri, ajr, ajp, i, -1, handler, aVar, i2, aeuArr);
    }

    /* renamed from: a */
    private void m11291a(final IOException iOException) {
        if (this.f8700i != null && this.f8701j != null) {
            this.f8700i.post(new Runnable() {
                public void run() {
                    ExtractorSampleSource.this.f8701j.mo484a(ExtractorSampleSource.this.f8702k, iOException);
                }
            });
        }
    }

    /* renamed from: c */
    private void m11294c(long j) {
        this.f8716y = j;
        this.f8690H = false;
        if (this.f8685C.mo12136a()) {
            this.f8685C.mo12137b();
            return;
        }
        m11302i();
        m11299f();
    }

    /* renamed from: d */
    private C2118b m11296d(long j) {
        return new C2118b(this.f8698g, this.f8699h, this.f8693b, this.f8694c, this.f8695d, this.f8704m.mo668b(j));
    }

    /* renamed from: e */
    private void m11297e(long j) {
        for (int i = 0; i < this.f8712u.length; i++) {
            if (!this.f8712u[i]) {
                ((C2120d) this.f8696e.valueAt(i)).mo686a(j);
            }
        }
    }

    /* renamed from: f */
    private long m11298f(long j) {
        return Math.min((j - 1) * 1000, 5000);
    }

    /* renamed from: f */
    private void m11299f() {
        boolean z = false;
        if (!this.f8690H && !this.f8685C.mo12136a()) {
            if (this.f8687E == null) {
                this.f8684B = 0;
                this.f8717z = false;
                if (!this.f8706o) {
                    this.f8686D = m11300g();
                } else {
                    akc.m2451b(m11303j());
                    if (this.f8709r == -1 || this.f8716y < this.f8709r) {
                        this.f8686D = m11296d(this.f8716y);
                        this.f8716y = Long.MIN_VALUE;
                    } else {
                        this.f8690H = true;
                        this.f8716y = Long.MIN_VALUE;
                        return;
                    }
                }
                this.f8692J = this.f8691I;
                this.f8685C.mo12134a((C2129c) this.f8686D, (C2127a) this);
            } else if (!m11304k()) {
                if (this.f8686D != null) {
                    z = true;
                }
                akc.m2451b(z);
                if (SystemClock.elapsedRealtime() - this.f8689G >= m11298f((long) this.f8688F)) {
                    this.f8687E = null;
                    if (!this.f8706o) {
                        for (int i = 0; i < this.f8696e.size(); i++) {
                            ((C2120d) this.f8696e.valueAt(i)).mo684a();
                        }
                        this.f8686D = m11300g();
                    } else if (!this.f8704m.mo667a() && this.f8709r == -1) {
                        for (int i2 = 0; i2 < this.f8696e.size(); i2++) {
                            ((C2120d) this.f8696e.valueAt(i2)).mo684a();
                        }
                        this.f8686D = m11300g();
                        this.f8683A = this.f8714w;
                        this.f8717z = true;
                    }
                    this.f8692J = this.f8691I;
                    this.f8685C.mo12134a((C2129c) this.f8686D, (C2127a) this);
                }
            }
        }
    }

    /* renamed from: g */
    private C2118b m11300g() {
        return new C2118b(this.f8698g, this.f8699h, this.f8693b, this.f8694c, this.f8695d, 0);
    }

    /* renamed from: h */
    private boolean m11301h() {
        for (int i = 0; i < this.f8696e.size(); i++) {
            if (!((C2120d) this.f8696e.valueAt(i)).mo692d()) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: i */
    private void m11302i() {
        for (int i = 0; i < this.f8696e.size(); i++) {
            ((C2120d) this.f8696e.valueAt(i)).mo684a();
        }
        this.f8686D = null;
        this.f8687E = null;
        this.f8688F = 0;
    }

    /* renamed from: j */
    private boolean m11303j() {
        return this.f8716y != Long.MIN_VALUE;
    }

    /* renamed from: k */
    private boolean m11304k() {
        return this.f8687E instanceof UnrecognizedInputFormatException;
    }

    /* renamed from: a */
    public int mo429a(int i, long j, acs acs, act act) {
        this.f8714w = j;
        if (this.f8711t[i] || m11303j()) {
            return -2;
        }
        C2120d dVar = (C2120d) this.f8696e.valueAt(i);
        if (this.f8710s[i]) {
            acs.f518a = dVar.mo693e();
            acs.f519b = this.f8705n;
            this.f8710s[i] = false;
            return -4;
        } else if (!dVar.mo687a(act)) {
            return this.f8690H ? -1 : -2;
        } else {
            act.f523d = ((act.f524e > this.f8715x ? 1 : (act.f524e == this.f8715x ? 0 : -1)) < 0 ? 134217728 : 0) | act.f523d;
            if (this.f8717z) {
                this.f8684B = this.f8683A - act.f524e;
                this.f8717z = false;
            }
            act.f524e += this.f8684B;
            return -3;
        }
    }

    /* renamed from: a */
    public MediaFormat mo430a(int i) {
        akc.m2451b(this.f8706o);
        return this.f8708q[i];
    }

    /* renamed from: a */
    public void mo495a() {
        this.f8703l = true;
    }

    /* renamed from: a */
    public void mo431a(int i, long j) {
        akc.m2451b(this.f8706o);
        akc.m2451b(!this.f8712u[i]);
        this.f8707p++;
        this.f8712u[i] = true;
        this.f8710s[i] = true;
        this.f8711t[i] = false;
        if (this.f8707p == 1) {
            if (!this.f8704m.mo667a()) {
                j = 0;
            }
            this.f8714w = j;
            this.f8715x = j;
            m11294c(j);
        }
    }

    /* renamed from: a */
    public void mo498a(aei aei) {
        this.f8705n = aei;
    }

    /* renamed from: a */
    public void mo499a(afa afa) {
        this.f8704m = afa;
    }

    /* renamed from: a */
    public void mo507a(C2129c cVar) {
        this.f8690H = true;
    }

    /* renamed from: a */
    public void mo508a(C2129c cVar, IOException iOException) {
        this.f8687E = iOException;
        this.f8688F = this.f8691I > this.f8692J ? 1 : this.f8688F + 1;
        this.f8689G = SystemClock.elapsedRealtime();
        m11291a(iOException);
        m11299f();
    }

    /* renamed from: a */
    public boolean mo432a(long j) {
        if (this.f8706o) {
            return true;
        }
        if (this.f8685C == null) {
            this.f8685C = new Loader("Loader:ExtractorSampleSource");
        }
        m11299f();
        if (this.f8704m == null || !this.f8703l || !m11301h()) {
            return false;
        }
        int size = this.f8696e.size();
        this.f8712u = new boolean[size];
        this.f8711t = new boolean[size];
        this.f8710s = new boolean[size];
        this.f8708q = new MediaFormat[size];
        this.f8709r = -1;
        for (int i = 0; i < size; i++) {
            MediaFormat e = ((C2120d) this.f8696e.valueAt(i)).mo693e();
            this.f8708q[i] = e;
            if (e.f8554e != -1 && e.f8554e > this.f8709r) {
                this.f8709r = e.f8554e;
            }
        }
        this.f8706o = true;
        return true;
    }

    /* renamed from: b */
    public long mo433b(int i) {
        if (!this.f8711t[i]) {
            return Long.MIN_VALUE;
        }
        this.f8711t[i] = false;
        return this.f8715x;
    }

    /* renamed from: b */
    public void mo434b() throws IOException {
        if (this.f8687E != null) {
            if (m11304k()) {
                throw this.f8687E;
            }
            int i = this.f8697f != -1 ? this.f8697f : (this.f8704m == null || this.f8704m.mo667a()) ? 3 : 6;
            if (this.f8688F > i) {
                throw this.f8687E;
            }
        }
    }

    /* renamed from: b */
    public void mo435b(long j) {
        akc.m2451b(this.f8706o);
        akc.m2451b(this.f8707p > 0);
        if (!this.f8704m.mo667a()) {
            j = 0;
        }
        long j2 = m11303j() ? this.f8716y : this.f8714w;
        this.f8714w = j;
        this.f8715x = j;
        if (j2 != j) {
            boolean z = !m11303j();
            int i = 0;
            while (z && i < this.f8696e.size()) {
                z &= ((C2120d) this.f8696e.valueAt(i)).mo690b(j);
                i++;
            }
            if (!z) {
                m11294c(j);
            }
            for (int i2 = 0; i2 < this.f8711t.length; i2++) {
                this.f8711t[i2] = true;
            }
        }
    }

    /* renamed from: b */
    public void mo509b(C2129c cVar) {
        if (this.f8707p > 0) {
            m11294c(this.f8716y);
            return;
        }
        m11302i();
        this.f8694c.mo977a(0);
    }

    /* renamed from: b */
    public boolean mo436b(int i, long j) {
        akc.m2451b(this.f8706o);
        akc.m2451b(this.f8712u[i]);
        this.f8714w = j;
        m11297e(this.f8714w);
        if (this.f8690H) {
            return true;
        }
        m11299f();
        if (m11303j()) {
            return false;
        }
        return !((C2120d) this.f8696e.valueAt(i)).mo695g();
    }

    /* renamed from: b_ */
    public afb mo502b_(int i) {
        C2120d dVar = (C2120d) this.f8696e.get(i);
        if (dVar != null) {
            return dVar;
        }
        C2120d dVar2 = new C2120d(this.f8694c);
        this.f8696e.put(i, dVar2);
        return dVar2;
    }

    /* renamed from: c */
    public int mo437c() {
        return this.f8696e.size();
    }

    /* renamed from: c */
    public void mo438c(int i) {
        akc.m2451b(this.f8706o);
        akc.m2451b(this.f8712u[i]);
        this.f8707p--;
        this.f8712u[i] = false;
        if (this.f8707p == 0) {
            this.f8714w = Long.MIN_VALUE;
            if (this.f8685C.mo12136a()) {
                this.f8685C.mo12137b();
                return;
            }
            m11302i();
            this.f8694c.mo977a(0);
        }
    }

    /* renamed from: d */
    public long mo439d() {
        if (this.f8690H) {
            return -3;
        }
        if (m11303j()) {
            return this.f8716y;
        }
        long j = Long.MIN_VALUE;
        for (int i = 0; i < this.f8696e.size(); i++) {
            j = Math.max(j, ((C2120d) this.f8696e.valueAt(i)).mo694f());
        }
        return j == Long.MIN_VALUE ? this.f8714w : j;
    }

    /* renamed from: e */
    public void mo440e() {
        akc.m2451b(this.f8713v > 0);
        int i = this.f8713v - 1;
        this.f8713v = i;
        if (i == 0 && this.f8685C != null) {
            this.f8685C.mo12135a(new Runnable() {
                public void run() {
                    ExtractorSampleSource.this.f8693b.mo12104a();
                }
            });
            this.f8685C = null;
        }
    }

    /* renamed from: g_ */
    public C0157a mo428g_() {
        this.f8713v++;
        return this;
    }
}
