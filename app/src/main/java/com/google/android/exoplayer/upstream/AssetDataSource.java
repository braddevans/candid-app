package com.google.android.exoplayer.upstream;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public final class AssetDataSource implements ajz {

    /* renamed from: a */
    private final AssetManager f8783a;

    /* renamed from: b */
    private final ajy f8784b;

    /* renamed from: c */
    private String f8785c;

    /* renamed from: d */
    private InputStream f8786d;

    /* renamed from: e */
    private long f8787e;

    /* renamed from: f */
    private boolean f8788f;

    public static final class AssetDataSourceException extends IOException {
        public AssetDataSourceException(IOException iOException) {
            super(iOException);
        }
    }

    public AssetDataSource(Context context, ajy ajy) {
        this.f8783a = context.getAssets();
        this.f8784b = ajy;
    }

    /* renamed from: a */
    public int mo836a(byte[] bArr, int i, int i2) throws AssetDataSourceException {
        if (this.f8787e == 0) {
            return -1;
        }
        try {
            int read = this.f8786d.read(bArr, i, this.f8787e == -1 ? i2 : (int) Math.min(this.f8787e, (long) i2));
            if (read <= 0) {
                return read;
            }
            if (this.f8787e != -1) {
                this.f8787e -= (long) read;
            }
            if (this.f8784b == null) {
                return read;
            }
            this.f8784b.mo992a(read);
            return read;
        } catch (IOException e) {
            throw new AssetDataSourceException(e);
        }
    }

    /* renamed from: a */
    public long mo837a(ajt ajt) throws AssetDataSourceException {
        try {
            this.f8785c = ajt.f1968a.toString();
            String path = ajt.f1968a.getPath();
            if (path.startsWith("/android_asset/")) {
                path = path.substring(15);
            } else if (path.startsWith("/")) {
                path = path.substring(1);
            }
            this.f8785c = ajt.f1968a.toString();
            this.f8786d = this.f8783a.open(path, 1);
            if (this.f8786d.skip(ajt.f1971d) < ajt.f1971d) {
                throw new EOFException();
            }
            if (ajt.f1972e != -1) {
                this.f8787e = ajt.f1972e;
            } else {
                this.f8787e = (long) this.f8786d.available();
                if (this.f8787e == 2147483647L) {
                    this.f8787e = -1;
                }
            }
            this.f8788f = true;
            if (this.f8784b != null) {
                this.f8784b.mo993b();
            }
            return this.f8787e;
        } catch (IOException e) {
            throw new AssetDataSourceException(e);
        }
    }

    /* renamed from: a */
    public void mo838a() throws AssetDataSourceException {
        this.f8785c = null;
        if (this.f8786d != null) {
            try {
                this.f8786d.close();
                this.f8786d = null;
                if (this.f8788f) {
                    this.f8788f = false;
                    if (this.f8784b != null) {
                        this.f8784b.mo994c();
                    }
                }
            } catch (IOException e) {
                throw new AssetDataSourceException(e);
            } catch (Throwable th) {
                this.f8786d = null;
                if (this.f8788f) {
                    this.f8788f = false;
                    if (this.f8784b != null) {
                        this.f8784b.mo994c();
                    }
                }
                throw th;
            }
        }
    }

    /* renamed from: b */
    public String mo996b() {
        return this.f8785c;
    }
}
