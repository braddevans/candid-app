package com.google.android.exoplayer.audio;

import android.annotation.TargetApi;
import android.media.AudioTimestamp;
import android.media.PlaybackParams;
import android.os.ConditionVariable;
import android.os.SystemClock;
import android.util.Log;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

public final class AudioTrack {

    /* renamed from: a */
    public static boolean f8574a = false;

    /* renamed from: b */
    public static boolean f8575b = false;

    /* renamed from: A */
    private int f8576A;

    /* renamed from: B */
    private int f8577B;

    /* renamed from: C */
    private long f8578C;

    /* renamed from: D */
    private long f8579D;

    /* renamed from: E */
    private long f8580E;

    /* renamed from: F */
    private float f8581F;

    /* renamed from: G */
    private byte[] f8582G;

    /* renamed from: H */
    private int f8583H;

    /* renamed from: I */
    private int f8584I;

    /* renamed from: J */
    private ByteBuffer f8585J;

    /* renamed from: K */
    private boolean f8586K;

    /* renamed from: c */
    private final ada f8587c;

    /* renamed from: d */
    private final int f8588d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final ConditionVariable f8589e;

    /* renamed from: f */
    private final long[] f8590f;

    /* renamed from: g */
    private final C2107a f8591g;

    /* renamed from: h */
    private android.media.AudioTrack f8592h;

    /* renamed from: i */
    private android.media.AudioTrack f8593i;

    /* renamed from: j */
    private int f8594j;

    /* renamed from: k */
    private int f8595k;

    /* renamed from: l */
    private int f8596l;

    /* renamed from: m */
    private int f8597m;

    /* renamed from: n */
    private boolean f8598n;

    /* renamed from: o */
    private int f8599o;

    /* renamed from: p */
    private int f8600p;

    /* renamed from: q */
    private long f8601q;

    /* renamed from: r */
    private int f8602r;

    /* renamed from: s */
    private int f8603s;

    /* renamed from: t */
    private long f8604t;

    /* renamed from: u */
    private long f8605u;

    /* renamed from: v */
    private boolean f8606v;

    /* renamed from: w */
    private long f8607w;

    /* renamed from: x */
    private Method f8608x;

    /* renamed from: y */
    private long f8609y;

    /* renamed from: z */
    private long f8610z;

    public static final class InitializationException extends Exception {

        /* renamed from: a */
        public final int f8615a;

        public InitializationException(int i, int i2, int i3, int i4) {
            super("AudioTrack init failed: " + i + ", Config(" + i2 + ", " + i3 + ", " + i4 + ")");
            this.f8615a = i;
        }
    }

    public static final class InvalidAudioTrackTimestampException extends RuntimeException {
        public InvalidAudioTrackTimestampException(String str) {
            super(str);
        }
    }

    public static final class WriteException extends Exception {

        /* renamed from: a */
        public final int f8616a;

        public WriteException(int i) {
            super("AudioTrack write failed: " + i);
            this.f8616a = i;
        }
    }

    /* renamed from: com.google.android.exoplayer.audio.AudioTrack$a */
    static class C2107a {

        /* renamed from: a */
        protected android.media.AudioTrack f8617a;

        /* renamed from: b */
        private boolean f8618b;

        /* renamed from: c */
        private int f8619c;

        /* renamed from: d */
        private long f8620d;

        /* renamed from: e */
        private long f8621e;

        /* renamed from: f */
        private long f8622f;

        /* renamed from: g */
        private long f8623g;

        /* renamed from: h */
        private long f8624h;

        /* renamed from: i */
        private long f8625i;

        private C2107a() {
        }

        /* renamed from: a */
        public void mo12075a() {
            if (this.f8623g == -1) {
                this.f8617a.pause();
            }
        }

        /* renamed from: a */
        public void mo12076a(long j) {
            this.f8624h = mo12079b();
            this.f8623g = SystemClock.elapsedRealtime() * 1000;
            this.f8625i = j;
            this.f8617a.stop();
        }

        /* renamed from: a */
        public void mo12077a(android.media.AudioTrack audioTrack, boolean z) {
            this.f8617a = audioTrack;
            this.f8618b = z;
            this.f8623g = -1;
            this.f8620d = 0;
            this.f8621e = 0;
            this.f8622f = 0;
            if (audioTrack != null) {
                this.f8619c = audioTrack.getSampleRate();
            }
        }

        /* renamed from: a */
        public void mo12078a(PlaybackParams playbackParams) {
            throw new UnsupportedOperationException();
        }

        /* renamed from: b */
        public long mo12079b() {
            if (this.f8623g != -1) {
                return Math.min(this.f8625i, this.f8624h + ((((long) this.f8619c) * ((SystemClock.elapsedRealtime() * 1000) - this.f8623g)) / 1000000));
            }
            int playState = this.f8617a.getPlayState();
            if (playState == 1) {
                return 0;
            }
            long playbackHeadPosition = 4294967295L & ((long) this.f8617a.getPlaybackHeadPosition());
            if (this.f8618b) {
                if (playState == 2 && playbackHeadPosition == 0) {
                    this.f8622f = this.f8620d;
                }
                playbackHeadPosition += this.f8622f;
            }
            if (this.f8620d > playbackHeadPosition) {
                this.f8621e++;
            }
            this.f8620d = playbackHeadPosition;
            return (this.f8621e << 32) + playbackHeadPosition;
        }

        /* renamed from: c */
        public long mo12080c() {
            return (mo12079b() * 1000000) / ((long) this.f8619c);
        }

        /* renamed from: d */
        public boolean mo12081d() {
            return false;
        }

        /* renamed from: e */
        public long mo12082e() {
            throw new UnsupportedOperationException();
        }

        /* renamed from: f */
        public long mo12083f() {
            throw new UnsupportedOperationException();
        }

        /* renamed from: g */
        public float mo12084g() {
            return 1.0f;
        }
    }

    @TargetApi(19)
    /* renamed from: com.google.android.exoplayer.audio.AudioTrack$b */
    static class C2108b extends C2107a {

        /* renamed from: b */
        private final AudioTimestamp f8626b = new AudioTimestamp();

        /* renamed from: c */
        private long f8627c;

        /* renamed from: d */
        private long f8628d;

        /* renamed from: e */
        private long f8629e;

        public C2108b() {
            super();
        }

        /* renamed from: a */
        public void mo12077a(android.media.AudioTrack audioTrack, boolean z) {
            super.mo12077a(audioTrack, z);
            this.f8627c = 0;
            this.f8628d = 0;
            this.f8629e = 0;
        }

        /* renamed from: d */
        public boolean mo12081d() {
            boolean timestamp = this.f8617a.getTimestamp(this.f8626b);
            if (timestamp) {
                long j = this.f8626b.framePosition;
                if (this.f8628d > j) {
                    this.f8627c++;
                }
                this.f8628d = j;
                this.f8629e = (this.f8627c << 32) + j;
            }
            return timestamp;
        }

        /* renamed from: e */
        public long mo12082e() {
            return this.f8626b.nanoTime;
        }

        /* renamed from: f */
        public long mo12083f() {
            return this.f8629e;
        }
    }

    @TargetApi(23)
    /* renamed from: com.google.android.exoplayer.audio.AudioTrack$c */
    static class C2109c extends C2108b {

        /* renamed from: b */
        private PlaybackParams f8630b;

        /* renamed from: c */
        private float f8631c = 1.0f;

        /* renamed from: h */
        private void m11237h() {
            if (this.f8617a != null && this.f8630b != null) {
                this.f8617a.setPlaybackParams(this.f8630b);
            }
        }

        /* renamed from: a */
        public void mo12077a(android.media.AudioTrack audioTrack, boolean z) {
            super.mo12077a(audioTrack, z);
            m11237h();
        }

        /* renamed from: a */
        public void mo12078a(PlaybackParams playbackParams) {
            if (playbackParams == null) {
                playbackParams = new PlaybackParams();
            }
            PlaybackParams allowDefaults = playbackParams.allowDefaults();
            this.f8630b = allowDefaults;
            this.f8631c = allowDefaults.getSpeed();
            m11237h();
        }

        /* renamed from: g */
        public float mo12084g() {
            return this.f8631c;
        }
    }

    public AudioTrack() {
        this(null, 3);
    }

    public AudioTrack(ada ada, int i) {
        this.f8587c = ada;
        this.f8588d = i;
        this.f8589e = new ConditionVariable(true);
        if (akw.f2108a >= 18) {
            try {
                this.f8608x = android.media.AudioTrack.class.getMethod("getLatency", null);
            } catch (NoSuchMethodException e) {
            }
        }
        if (akw.f2108a >= 23) {
            this.f8591g = new C2109c();
        } else if (akw.f2108a >= 19) {
            this.f8591g = new C2108b();
        } else {
            this.f8591g = new C2107a();
        }
        this.f8590f = new long[10];
        this.f8581F = 1.0f;
        this.f8577B = 0;
    }

    /* renamed from: a */
    private static int m11185a(int i, ByteBuffer byteBuffer) {
        if (i == 7 || i == 8) {
            return akf.m2461a(byteBuffer);
        }
        if (i == 5) {
            return akb.m2437a();
        }
        if (i == 6) {
            return akb.m2439a(byteBuffer);
        }
        throw new IllegalStateException("Unexpected audio encoding: " + i);
    }

    @TargetApi(21)
    /* renamed from: a */
    private static int m11186a(android.media.AudioTrack audioTrack, ByteBuffer byteBuffer, int i) {
        return audioTrack.write(byteBuffer, i, 1);
    }

    /* renamed from: a */
    private long m11187a(long j) {
        return j / ((long) this.f8599o);
    }

    /* renamed from: a */
    private static ByteBuffer m11189a(ByteBuffer byteBuffer, int i, int i2, int i3, ByteBuffer byteBuffer2) {
        int i4;
        switch (i3) {
            case Integer.MIN_VALUE:
                i4 = (i2 / 3) * 2;
                break;
            case 3:
                i4 = i2 * 2;
                break;
            case 1073741824:
                i4 = i2 / 2;
                break;
            default:
                throw new IllegalStateException();
        }
        ByteBuffer byteBuffer3 = byteBuffer2;
        if (byteBuffer3 == null || byteBuffer3.capacity() < i4) {
            byteBuffer3 = ByteBuffer.allocateDirect(i4);
        }
        byteBuffer3.position(0);
        byteBuffer3.limit(i4);
        int i5 = i + i2;
        switch (i3) {
            case Integer.MIN_VALUE:
                for (int i6 = i; i6 < i5; i6 += 3) {
                    byteBuffer3.put(byteBuffer.get(i6 + 1));
                    byteBuffer3.put(byteBuffer.get(i6 + 2));
                }
                break;
            case 3:
                for (int i7 = i; i7 < i5; i7++) {
                    byteBuffer3.put(0);
                    byteBuffer3.put((byte) ((byteBuffer.get(i7) & 255) - 128));
                }
                break;
            case 1073741824:
                for (int i8 = i; i8 < i5; i8 += 4) {
                    byteBuffer3.put(byteBuffer.get(i8 + 2));
                    byteBuffer3.put(byteBuffer.get(i8 + 3));
                }
                break;
            default:
                throw new IllegalStateException();
        }
        byteBuffer3.position(0);
        return byteBuffer3;
    }

    @TargetApi(21)
    /* renamed from: a */
    private static void m11190a(android.media.AudioTrack audioTrack, float f) {
        audioTrack.setVolume(f);
    }

    /* renamed from: b */
    private static int m11191b(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case -1095064472:
                if (str.equals("audio/vnd.dts")) {
                    c = 2;
                    break;
                }
                break;
            case 187078296:
                if (str.equals("audio/ac3")) {
                    c = 0;
                    break;
                }
                break;
            case 1504578661:
                if (str.equals("audio/eac3")) {
                    c = 1;
                    break;
                }
                break;
            case 1505942594:
                if (str.equals("audio/vnd.dts.hd")) {
                    c = 3;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return 5;
            case 1:
                return 6;
            case 2:
                return 7;
            case 3:
                return 8;
            default:
                return 0;
        }
    }

    /* renamed from: b */
    private long m11192b(long j) {
        return (1000000 * j) / ((long) this.f8594j);
    }

    /* renamed from: b */
    private static void m11193b(android.media.AudioTrack audioTrack, float f) {
        audioTrack.setStereoVolume(f, f);
    }

    /* renamed from: c */
    private long m11194c(long j) {
        return (((long) this.f8594j) * j) / 1000000;
    }

    /* renamed from: l */
    private void m11195l() {
        if (mo12061a()) {
            if (akw.f2108a >= 21) {
                m11190a(this.f8593i, this.f8581F);
            } else {
                m11193b(this.f8593i, this.f8581F);
            }
        }
    }

    /* renamed from: m */
    private void m11196m() {
        if (this.f8592h != null) {
            final android.media.AudioTrack audioTrack = this.f8592h;
            this.f8592h = null;
            new Thread() {
                public void run() {
                    audioTrack.release();
                }
            }.start();
        }
    }

    /* renamed from: n */
    private boolean m11197n() {
        return mo12061a() && this.f8577B != 0;
    }

    /* renamed from: o */
    private void m11198o() {
        long c = this.f8591g.mo12080c();
        if (c != 0) {
            long nanoTime = System.nanoTime() / 1000;
            if (nanoTime - this.f8605u >= 30000) {
                this.f8590f[this.f8602r] = c - nanoTime;
                this.f8602r = (this.f8602r + 1) % 10;
                if (this.f8603s < 10) {
                    this.f8603s++;
                }
                this.f8605u = nanoTime;
                this.f8604t = 0;
                for (int i = 0; i < this.f8603s; i++) {
                    this.f8604t += this.f8590f[i] / ((long) this.f8603s);
                }
            }
            if (!m11202s() && nanoTime - this.f8607w >= 500000) {
                this.f8606v = this.f8591g.mo12081d();
                if (this.f8606v) {
                    long e = this.f8591g.mo12082e() / 1000;
                    long f = this.f8591g.mo12083f();
                    if (e < this.f8579D) {
                        this.f8606v = false;
                    } else if (Math.abs(e - nanoTime) > 5000000) {
                        String str = "Spurious audio timestamp (system clock mismatch): " + f + ", " + e + ", " + nanoTime + ", " + c;
                        if (f8575b) {
                            throw new InvalidAudioTrackTimestampException(str);
                        }
                        Log.w("AudioTrack", str);
                        this.f8606v = false;
                    } else if (Math.abs(m11192b(f) - c) > 5000000) {
                        String str2 = "Spurious audio timestamp (frame position mismatch): " + f + ", " + e + ", " + nanoTime + ", " + c;
                        if (f8575b) {
                            throw new InvalidAudioTrackTimestampException(str2);
                        }
                        Log.w("AudioTrack", str2);
                        this.f8606v = false;
                    }
                }
                if (this.f8608x != null && !this.f8598n) {
                    try {
                        this.f8580E = (((long) ((Integer) this.f8608x.invoke(this.f8593i, null)).intValue()) * 1000) - this.f8601q;
                        this.f8580E = Math.max(this.f8580E, 0);
                        if (this.f8580E > 5000000) {
                            Log.w("AudioTrack", "Ignoring impossibly large audio latency: " + this.f8580E);
                            this.f8580E = 0;
                        }
                    } catch (Exception e2) {
                        this.f8608x = null;
                    }
                }
                this.f8607w = nanoTime;
            }
        }
    }

    /* renamed from: p */
    private void m11199p() throws InitializationException {
        int state = this.f8593i.getState();
        if (state != 1) {
            try {
                this.f8593i.release();
            } catch (Exception e) {
            } finally {
                this.f8593i = null;
            }
            throw new InitializationException(state, this.f8594j, this.f8595k, this.f8600p);
        }
    }

    /* renamed from: q */
    private long m11200q() {
        return this.f8598n ? this.f8610z : m11187a(this.f8609y);
    }

    /* renamed from: r */
    private void m11201r() {
        this.f8604t = 0;
        this.f8603s = 0;
        this.f8602r = 0;
        this.f8605u = 0;
        this.f8606v = false;
        this.f8607w = 0;
    }

    /* renamed from: s */
    private boolean m11202s() {
        return akw.f2108a < 23 && (this.f8597m == 5 || this.f8597m == 6);
    }

    /* renamed from: t */
    private boolean m11203t() {
        return m11202s() && this.f8593i.getPlayState() == 2 && this.f8593i.getPlaybackHeadPosition() == 0;
    }

    /* renamed from: a */
    public int mo12054a(int i) throws InitializationException {
        this.f8589e.block();
        if (i == 0) {
            this.f8593i = new android.media.AudioTrack(this.f8588d, this.f8594j, this.f8595k, this.f8597m, this.f8600p, 1);
        } else {
            this.f8593i = new android.media.AudioTrack(this.f8588d, this.f8594j, this.f8595k, this.f8597m, this.f8600p, 1, i);
        }
        m11199p();
        int audioSessionId = this.f8593i.getAudioSessionId();
        if (f8574a && akw.f2108a < 21) {
            if (!(this.f8592h == null || audioSessionId == this.f8592h.getAudioSessionId())) {
                m11196m();
            }
            if (this.f8592h == null) {
                this.f8592h = new android.media.AudioTrack(this.f8588d, 4000, 4, 2, 2, 0, audioSessionId);
            }
        }
        this.f8591g.mo12077a(this.f8593i, m11202s());
        m11195l();
        return audioSessionId;
    }

    /* renamed from: a */
    public int mo12055a(ByteBuffer byteBuffer, int i, int i2, long j) throws WriteException {
        if (m11202s()) {
            if (this.f8593i.getPlayState() == 2) {
                return 0;
            }
            if (this.f8593i.getPlayState() == 1 && this.f8591g.mo12079b() != 0) {
                return 0;
            }
        }
        int i3 = 0;
        if (this.f8584I == 0) {
            if (i2 == 0) {
                return 2;
            }
            this.f8586K = this.f8597m != this.f8596l;
            if (this.f8586K) {
                akc.m2451b(this.f8597m == 2);
                this.f8585J = m11189a(byteBuffer, i, i2, this.f8596l, this.f8585J);
                byteBuffer = this.f8585J;
                i = this.f8585J.position();
                i2 = this.f8585J.limit();
            }
            this.f8584I = i2;
            byteBuffer.position(i);
            if (this.f8598n && this.f8576A == 0) {
                this.f8576A = m11185a(this.f8597m, byteBuffer);
            }
            if (this.f8577B == 0) {
                this.f8578C = Math.max(0, j);
                this.f8577B = 1;
            } else {
                long b = this.f8578C + m11192b(m11200q());
                if (this.f8577B == 1 && Math.abs(b - j) > 200000) {
                    Log.e("AudioTrack", "Discontinuity detected [expected " + b + ", got " + j + "]");
                    this.f8577B = 2;
                }
                if (this.f8577B == 2) {
                    this.f8578C += j - b;
                    this.f8577B = 1;
                    i3 = 0 | 1;
                }
            }
            if (akw.f2108a < 21) {
                if (this.f8582G == null || this.f8582G.length < i2) {
                    this.f8582G = new byte[i2];
                }
                byteBuffer.get(this.f8582G, 0, i2);
                this.f8583H = 0;
            }
        }
        int i4 = 0;
        if (akw.f2108a < 21) {
            int b2 = this.f8600p - ((int) (this.f8609y - (this.f8591g.mo12079b() * ((long) this.f8599o))));
            if (b2 > 0) {
                i4 = this.f8593i.write(this.f8582G, this.f8583H, Math.min(this.f8584I, b2));
                if (i4 >= 0) {
                    this.f8583H += i4;
                }
            }
        } else {
            i4 = m11186a(this.f8593i, this.f8586K ? this.f8585J : byteBuffer, this.f8584I);
        }
        if (i4 < 0) {
            throw new WriteException(i4);
        }
        this.f8584I -= i4;
        if (!this.f8598n) {
            this.f8609y += (long) i4;
        }
        if (this.f8584I != 0) {
            return i3;
        }
        if (this.f8598n) {
            this.f8610z += (long) this.f8576A;
        }
        return i3 | 2;
    }

    /* renamed from: a */
    public long mo12056a(boolean z) {
        if (!m11197n()) {
            return Long.MIN_VALUE;
        }
        if (this.f8593i.getPlayState() == 3) {
            m11198o();
        }
        long nanoTime = System.nanoTime() / 1000;
        if (this.f8606v) {
            return m11192b(this.f8591g.mo12083f() + m11194c((long) (((float) (nanoTime - (this.f8591g.mo12082e() / 1000))) * this.f8591g.mo12084g()))) + this.f8578C;
        }
        long j = this.f8603s == 0 ? this.f8591g.mo12080c() + this.f8578C : this.f8604t + nanoTime + this.f8578C;
        return !z ? j - this.f8580E : j;
    }

    /* renamed from: a */
    public void mo12057a(float f) {
        if (this.f8581F != f) {
            this.f8581F = f;
            m11195l();
        }
    }

    /* renamed from: a */
    public void mo12058a(PlaybackParams playbackParams) {
        this.f8591g.mo12078a(playbackParams);
    }

    /* renamed from: a */
    public void mo12059a(String str, int i, int i2, int i3) {
        mo12060a(str, i, i2, i3, 0);
    }

    /* renamed from: a */
    public void mo12060a(String str, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        switch (i) {
            case 1:
                i5 = 4;
                break;
            case 2:
                i5 = 12;
                break;
            case 3:
                i5 = 28;
                break;
            case 4:
                i5 = 204;
                break;
            case 5:
                i5 = 220;
                break;
            case 6:
                i5 = 252;
                break;
            case 7:
                i5 = 1276;
                break;
            case 8:
                i5 = ace.f394a;
                break;
            default:
                throw new IllegalArgumentException("Unsupported channel count: " + i);
        }
        boolean z = !"audio/raw".equals(str);
        if (z) {
            i6 = m11191b(str);
        } else if (i3 == 3 || i3 == 2 || i3 == Integer.MIN_VALUE || i3 == 1073741824) {
            i6 = i3;
        } else {
            throw new IllegalArgumentException("Unsupported PCM encoding: " + i3);
        }
        if (!mo12061a() || this.f8596l != i6 || this.f8594j != i2 || this.f8595k != i5) {
            mo12071j();
            this.f8596l = i6;
            this.f8598n = z;
            this.f8594j = i2;
            this.f8595k = i5;
            if (!z) {
                i6 = 2;
            }
            this.f8597m = i6;
            this.f8599o = i * 2;
            if (i4 != 0) {
                this.f8600p = i4;
            } else if (!z) {
                int minBufferSize = android.media.AudioTrack.getMinBufferSize(i2, i5, this.f8597m);
                akc.m2451b(minBufferSize != -2);
                int i7 = minBufferSize * 4;
                int c = ((int) m11194c(250000)) * this.f8599o;
                int max = (int) Math.max((long) minBufferSize, m11194c(750000) * ((long) this.f8599o));
                if (i7 >= c) {
                    c = i7 > max ? max : i7;
                }
                this.f8600p = c;
            } else if (this.f8597m == 5 || this.f8597m == 6) {
                this.f8600p = 20480;
            } else {
                this.f8600p = 49152;
            }
            this.f8601q = z ? -1 : m11192b(m11187a((long) this.f8600p));
        }
    }

    /* renamed from: a */
    public boolean mo12061a() {
        return this.f8593i != null;
    }

    /* renamed from: a */
    public boolean mo12062a(String str) {
        return this.f8587c != null && this.f8587c.mo473a(m11191b(str));
    }

    /* renamed from: b */
    public int mo12063b() throws InitializationException {
        return mo12054a(0);
    }

    /* renamed from: c */
    public int mo12064c() {
        return this.f8600p;
    }

    /* renamed from: d */
    public long mo12065d() {
        return this.f8601q;
    }

    /* renamed from: e */
    public void mo12066e() {
        if (mo12061a()) {
            this.f8579D = System.nanoTime() / 1000;
            this.f8593i.play();
        }
    }

    /* renamed from: f */
    public void mo12067f() {
        if (this.f8577B == 1) {
            this.f8577B = 2;
        }
    }

    /* renamed from: g */
    public void mo12068g() {
        if (mo12061a()) {
            this.f8591g.mo12076a(m11200q());
        }
    }

    /* renamed from: h */
    public boolean mo12069h() {
        return mo12061a() && (m11200q() > this.f8591g.mo12079b() || m11203t());
    }

    /* renamed from: i */
    public void mo12070i() {
        if (mo12061a()) {
            m11201r();
            this.f8591g.mo12075a();
        }
    }

    /* renamed from: j */
    public void mo12071j() {
        if (mo12061a()) {
            this.f8609y = 0;
            this.f8610z = 0;
            this.f8576A = 0;
            this.f8584I = 0;
            this.f8577B = 0;
            this.f8580E = 0;
            m11201r();
            if (this.f8593i.getPlayState() == 3) {
                this.f8593i.pause();
            }
            final android.media.AudioTrack audioTrack = this.f8593i;
            this.f8593i = null;
            this.f8591g.mo12077a(null, false);
            this.f8589e.close();
            new Thread() {
                public void run() {
                    try {
                        audioTrack.flush();
                        audioTrack.release();
                    } finally {
                        AudioTrack.this.f8589e.open();
                    }
                }
            }.start();
        }
    }

    /* renamed from: k */
    public void mo12072k() {
        mo12071j();
        m11196m();
    }
}
