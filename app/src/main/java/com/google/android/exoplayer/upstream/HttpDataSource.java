package com.google.android.exoplayer.upstream;

import android.text.TextUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface HttpDataSource extends ajz {

    /* renamed from: a */
    public static final akq<String> f8801a = new akq<String>() {
        /* renamed from: a */
        public boolean mo1103a(String str) {
            String b = akw.m2588b(str);
            return !TextUtils.isEmpty(b) && (!b.contains("text") || b.contains("text/vtt")) && !b.contains("html") && !b.contains("xml");
        }
    };

    public static class HttpDataSourceException extends IOException {

        /* renamed from: a */
        public final int f8802a;

        /* renamed from: b */
        public final ajt f8803b;

        public HttpDataSourceException(IOException iOException, ajt ajt, int i) {
            super(iOException);
            this.f8803b = ajt;
            this.f8802a = i;
        }

        public HttpDataSourceException(String str, ajt ajt, int i) {
            super(str);
            this.f8803b = ajt;
            this.f8802a = i;
        }

        public HttpDataSourceException(String str, IOException iOException, ajt ajt, int i) {
            super(str, iOException);
            this.f8803b = ajt;
            this.f8802a = i;
        }
    }

    public static final class InvalidContentTypeException extends HttpDataSourceException {

        /* renamed from: c */
        public final String f8804c;

        public InvalidContentTypeException(String str, ajt ajt) {
            super("Invalid content type: " + str, ajt, 1);
            this.f8804c = str;
        }
    }

    public static final class InvalidResponseCodeException extends HttpDataSourceException {

        /* renamed from: c */
        public final int f8805c;

        /* renamed from: d */
        public final Map<String, List<String>> f8806d;

        public InvalidResponseCodeException(int i, Map<String, List<String>> map, ajt ajt) {
            super("Response code: " + i, ajt, 1);
            this.f8805c = i;
            this.f8806d = map;
        }
    }
}
