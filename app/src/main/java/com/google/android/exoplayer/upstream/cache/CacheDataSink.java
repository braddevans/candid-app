package com.google.android.exoplayer.upstream.cache;

import java.io.IOException;

public final class CacheDataSink {

    public static class CacheDataSinkException extends IOException {
    }
}
