package com.google.android.exoplayer.dash;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.exoplayer.BehindLiveWindowException;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.util.ManifestFetcher;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DashChunkSource implements adi, C0177a {

    /* renamed from: a */
    private final Handler f8632a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final C2111a f8633b;

    /* renamed from: c */
    private final ajr f8634c;

    /* renamed from: d */
    private final adm f8635d;

    /* renamed from: e */
    private final C0176b f8636e;

    /* renamed from: f */
    private final ManifestFetcher<adz> f8637f;

    /* renamed from: g */
    private final adt f8638g;

    /* renamed from: h */
    private final ArrayList<C2112b> f8639h;

    /* renamed from: i */
    private final SparseArray<C2113c> f8640i;

    /* renamed from: j */
    private final akd f8641j;

    /* renamed from: k */
    private final long f8642k;

    /* renamed from: l */
    private final long f8643l;

    /* renamed from: m */
    private final long[] f8644m;

    /* renamed from: n */
    private final boolean f8645n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public final int f8646o;

    /* renamed from: p */
    private adz f8647p;

    /* renamed from: q */
    private adz f8648q;

    /* renamed from: r */
    private C2112b f8649r;

    /* renamed from: s */
    private int f8650s;

    /* renamed from: t */
    private acx f8651t;

    /* renamed from: u */
    private boolean f8652u;

    /* renamed from: v */
    private boolean f8653v;

    /* renamed from: w */
    private boolean f8654w;

    /* renamed from: x */
    private IOException f8655x;

    public static class NoAdaptationSetException extends IOException {
    }

    /* renamed from: com.google.android.exoplayer.dash.DashChunkSource$a */
    public interface C2111a {
        /* renamed from: a */
        void mo11153a(int i, acx acx);
    }

    /* renamed from: com.google.android.exoplayer.dash.DashChunkSource$b */
    public static final class C2112b {

        /* renamed from: a */
        public final MediaFormat f8658a;

        /* renamed from: b */
        public final int f8659b;

        /* renamed from: c */
        public final int f8660c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public final int f8661d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public final adl f8662e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public final adl[] f8663f;

        public C2112b(MediaFormat mediaFormat, int i, adl adl) {
            this.f8658a = mediaFormat;
            this.f8661d = i;
            this.f8662e = adl;
            this.f8663f = null;
            this.f8659b = -1;
            this.f8660c = -1;
        }

        public C2112b(MediaFormat mediaFormat, int i, adl[] adlArr, int i2, int i3) {
            this.f8658a = mediaFormat;
            this.f8661d = i;
            this.f8663f = adlArr;
            this.f8659b = i2;
            this.f8660c = i3;
            this.f8662e = null;
        }

        /* renamed from: a */
        public boolean mo12087a() {
            return this.f8663f != null;
        }
    }

    /* renamed from: com.google.android.exoplayer.dash.DashChunkSource$c */
    public static final class C2113c {

        /* renamed from: a */
        public final int f8664a;

        /* renamed from: b */
        public final long f8665b;

        /* renamed from: c */
        public final HashMap<String, C2114d> f8666c;

        /* renamed from: d */
        private final int[] f8667d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public aei f8668e;

        /* renamed from: f */
        private boolean f8669f;

        /* renamed from: g */
        private boolean f8670g;

        /* renamed from: h */
        private long f8671h;

        /* renamed from: i */
        private long f8672i;

        public C2113c(int i, adz adz, int i2, C2112b bVar) {
            this.f8664a = i;
            aeb a = adz.mo561a(i2);
            long a2 = m11272a(adz, i2);
            adw adw = (adw) a.f720c.get(bVar.f8661d);
            List<aed> list = adw.f695c;
            this.f8665b = a.f719b * 1000;
            this.f8668e = m11273a(adw);
            if (!bVar.mo12087a()) {
                this.f8667d = new int[]{m11271a(list, bVar.f8662e.f655a)};
            } else {
                this.f8667d = new int[bVar.f8663f.length];
                for (int i3 = 0; i3 < bVar.f8663f.length; i3++) {
                    this.f8667d[i3] = m11271a(list, bVar.f8663f[i3].f655a);
                }
            }
            this.f8666c = new HashMap<>();
            for (int i4 : this.f8667d) {
                aed aed = (aed) list.get(i4);
                this.f8666c.put(aed.f728c.f655a, new C2114d(this.f8665b, a2, aed));
            }
            m11276a(a2, (aed) list.get(this.f8667d[0]));
        }

        /* renamed from: a */
        private static int m11271a(List<aed> list, String str) {
            for (int i = 0; i < list.size(); i++) {
                if (str.equals(((aed) list.get(i)).f728c.f655a)) {
                    return i;
                }
            }
            throw new IllegalStateException("Missing format id: " + str);
        }

        /* renamed from: a */
        private static long m11272a(adz adz, int i) {
            long b = adz.mo564b(i);
            if (b == -1) {
                return -1;
            }
            return 1000 * b;
        }

        /* renamed from: a */
        private static aei m11273a(adw adw) {
            if (adw.f696d.isEmpty()) {
                return null;
            }
            C0195a aVar = null;
            for (int i = 0; i < adw.f696d.size(); i++) {
                adx adx = (adx) adw.f696d.get(i);
                if (!(adx.f698b == null || adx.f699c == null)) {
                    if (aVar == null) {
                        aVar = new C0195a();
                    }
                    aVar.mo629a(adx.f698b, adx.f699c);
                }
            }
            return aVar;
        }

        /* renamed from: a */
        private void m11276a(long j, aed aed) {
            boolean z = true;
            DashSegmentIndex e = aed.mo610e();
            if (e != null) {
                int a = e.mo548a();
                int a2 = e.mo549a(j);
                if (a2 != -1) {
                    z = false;
                }
                this.f8669f = z;
                this.f8670g = e.mo554b();
                this.f8671h = this.f8665b + e.mo551a(a);
                if (!this.f8669f) {
                    this.f8672i = this.f8665b + e.mo551a(a2) + e.mo552a(a2, j);
                    return;
                }
                return;
            }
            this.f8669f = false;
            this.f8670g = true;
            this.f8671h = this.f8665b;
            this.f8672i = this.f8665b + j;
        }

        /* renamed from: a */
        public long mo12088a() {
            return this.f8671h;
        }

        /* renamed from: a */
        public void mo12089a(adz adz, int i, C2112b bVar) throws BehindLiveWindowException {
            aeb a = adz.mo561a(i);
            long a2 = m11272a(adz, i);
            List<aed> list = ((adw) a.f720c.get(bVar.f8661d)).f695c;
            for (int i2 : this.f8667d) {
                aed aed = (aed) list.get(i2);
                ((C2114d) this.f8666c.get(aed.f728c.f655a)).mo12096a(a2, aed);
            }
            m11276a(a2, (aed) list.get(this.f8667d[0]));
        }

        /* renamed from: b */
        public long mo12090b() {
            if (!mo12091c()) {
                return this.f8672i;
            }
            throw new IllegalStateException("Period has unbounded index");
        }

        /* renamed from: c */
        public boolean mo12091c() {
            return this.f8669f;
        }

        /* renamed from: d */
        public boolean mo12092d() {
            return this.f8670g;
        }
    }

    /* renamed from: com.google.android.exoplayer.dash.DashChunkSource$d */
    public static final class C2114d {

        /* renamed from: a */
        public final boolean f8673a;

        /* renamed from: b */
        public final adf f8674b;

        /* renamed from: c */
        public aed f8675c;

        /* renamed from: d */
        public DashSegmentIndex f8676d;

        /* renamed from: e */
        public MediaFormat f8677e;

        /* renamed from: f */
        private final long f8678f;

        /* renamed from: g */
        private long f8679g;

        /* renamed from: h */
        private int f8680h;

        public C2114d(long j, long j2, aed aed) {
            adf adf;
            this.f8678f = j;
            this.f8679g = j2;
            this.f8675c = aed;
            String str = aed.f728c.f656b;
            this.f8673a = DashChunkSource.m11250b(str);
            if (this.f8673a) {
                adf = null;
            } else {
                adf = new adf(DashChunkSource.m11247a(str) ? new ahd() : new afp());
            }
            this.f8674b = adf;
            this.f8676d = aed.mo610e();
        }

        /* renamed from: a */
        public int mo12093a() {
            return this.f8676d.mo549a(this.f8679g);
        }

        /* renamed from: a */
        public int mo12094a(long j) {
            return this.f8676d.mo550a(j - this.f8678f, this.f8679g) + this.f8680h;
        }

        /* renamed from: a */
        public long mo12095a(int i) {
            return this.f8676d.mo551a(i - this.f8680h) + this.f8678f;
        }

        /* renamed from: a */
        public void mo12096a(long j, aed aed) throws BehindLiveWindowException {
            DashSegmentIndex e = this.f8675c.mo610e();
            DashSegmentIndex e2 = aed.mo610e();
            this.f8679g = j;
            this.f8675c = aed;
            if (e != null) {
                this.f8676d = e2;
                if (e.mo554b()) {
                    int a = e.mo549a(this.f8679g);
                    long a2 = e.mo551a(a) + e.mo552a(a, this.f8679g);
                    int a3 = e2.mo548a();
                    long a4 = e2.mo551a(a3);
                    if (a2 == a4) {
                        this.f8680h += (e.mo549a(this.f8679g) + 1) - a3;
                    } else if (a2 < a4) {
                        throw new BehindLiveWindowException();
                    } else {
                        this.f8680h += e.mo550a(a4, this.f8679g) - a3;
                    }
                }
            }
        }

        /* renamed from: b */
        public int mo12097b() {
            return this.f8676d.mo548a() + this.f8680h;
        }

        /* renamed from: b */
        public long mo12098b(int i) {
            return mo12095a(i) + this.f8676d.mo552a(i - this.f8680h, this.f8679g);
        }

        /* renamed from: c */
        public boolean mo12099c(int i) {
            int a = mo12093a();
            return a != -1 && i > this.f8680h + a;
        }

        /* renamed from: d */
        public aec mo12100d(int i) {
            return this.f8676d.mo553b(i - this.f8680h);
        }
    }

    public DashChunkSource(ManifestFetcher<adz> manifestFetcher, adt adt, ajr ajr, adm adm, long j, long j2, Handler handler, C2111a aVar, int i) {
        this(manifestFetcher, (adz) manifestFetcher.mo12144a(), adt, ajr, adm, new akt(), j * 1000, j2 * 1000, true, handler, aVar, i);
    }

    DashChunkSource(ManifestFetcher<adz> manifestFetcher, adz adz, adt adt, ajr ajr, adm adm, akd akd, long j, long j2, boolean z, Handler handler, C2111a aVar, int i) {
        this.f8637f = manifestFetcher;
        this.f8647p = adz;
        this.f8638g = adt;
        this.f8634c = ajr;
        this.f8635d = adm;
        this.f8641j = akd;
        this.f8642k = j;
        this.f8643l = j2;
        this.f8653v = z;
        this.f8632a = handler;
        this.f8633b = aVar;
        this.f8646o = i;
        this.f8636e = new C0176b();
        this.f8644m = new long[2];
        this.f8640i = new SparseArray<>();
        this.f8639h = new ArrayList<>();
        this.f8645n = adz.f704d;
    }

    /* renamed from: a */
    private ade m11242a(aec aec, aec aec2, aed aed, adf adf, ajr ajr, int i, int i2) {
        aec aec3;
        if (aec != null) {
            aec3 = aec.mo603a(aec2);
            if (aec3 == null) {
                aec3 = aec;
            }
        } else {
            aec3 = aec2;
        }
        return new ado(ajr, new ajt(aec3.mo604a(), aec3.f721a, aec3.f722b, aed.mo611f()), i2, aed.f728c, adf, i);
    }

    /* renamed from: a */
    private static MediaFormat m11243a(int i, adl adl, String str, long j) {
        switch (i) {
            case 0:
                return MediaFormat.m11163a(adl.f655a, str, adl.f657c, -1, j, adl.f658d, adl.f659e, null);
            case 1:
                return MediaFormat.m11166a(adl.f655a, str, adl.f657c, -1, j, adl.f661g, adl.f662h, null, adl.f664j);
            case 2:
                return MediaFormat.m11169a(adl.f655a, str, adl.f657c, j, adl.f664j);
            default:
                return null;
        }
    }

    /* renamed from: a */
    private static String m11244a(adl adl) {
        String str = adl.f656b;
        if (akk.m2478a(str)) {
            return akk.m2482e(adl.f663i);
        }
        if (akk.m2479b(str)) {
            return akk.m2481d(adl.f663i);
        }
        if (m11250b(str)) {
            return str;
        }
        if ("application/mp4".equals(str)) {
            if ("stpp".equals(adl.f663i)) {
                return "application/ttml+xml";
            }
            if ("wvtt".equals(adl.f663i)) {
                return "application/x-mp4vtt";
            }
        }
        return null;
    }

    /* renamed from: a */
    private void m11245a(final acx acx) {
        if (this.f8632a != null && this.f8633b != null) {
            this.f8632a.post(new Runnable() {
                public void run() {
                    DashChunkSource.this.f8633b.mo11153a(DashChunkSource.this.f8646o, acx);
                }
            });
        }
    }

    /* renamed from: a */
    private void m11246a(adz adz) {
        aeb a = adz.mo561a(0);
        while (this.f8640i.size() > 0 && ((C2113c) this.f8640i.valueAt(0)).f8665b < a.f719b * 1000) {
            this.f8640i.remove(((C2113c) this.f8640i.valueAt(0)).f8664a);
        }
        if (this.f8640i.size() <= adz.mo563b()) {
            try {
                int size = this.f8640i.size();
                if (size > 0) {
                    ((C2113c) this.f8640i.valueAt(0)).mo12089a(adz, 0, this.f8649r);
                    if (size > 1) {
                        int i = size - 1;
                        ((C2113c) this.f8640i.valueAt(i)).mo12089a(adz, i, this.f8649r);
                    }
                }
                for (int size2 = this.f8640i.size(); size2 < adz.mo563b(); size2++) {
                    this.f8640i.put(this.f8650s, new C2113c(this.f8650s, adz, size2, this.f8649r));
                    this.f8650s++;
                }
                acx c = m11251c(m11252d());
                if (this.f8651t == null || !this.f8651t.equals(c)) {
                    this.f8651t = c;
                    m11245a(this.f8651t);
                }
                this.f8647p = adz;
            } catch (BehindLiveWindowException e) {
                this.f8655x = e;
            }
        }
    }

    /* renamed from: a */
    static boolean m11247a(String str) {
        return str.startsWith("video/webm") || str.startsWith("audio/webm") || str.startsWith("application/webm");
    }

    /* renamed from: b */
    private C2113c m11249b(long j) {
        if (j < ((C2113c) this.f8640i.valueAt(0)).mo12088a()) {
            return (C2113c) this.f8640i.valueAt(0);
        }
        for (int i = 0; i < this.f8640i.size() - 1; i++) {
            C2113c cVar = (C2113c) this.f8640i.valueAt(i);
            if (j < cVar.mo12090b()) {
                return cVar;
            }
        }
        return (C2113c) this.f8640i.valueAt(this.f8640i.size() - 1);
    }

    /* renamed from: b */
    static boolean m11250b(String str) {
        return "text/vtt".equals(str) || "application/ttml+xml".equals(str);
    }

    /* renamed from: c */
    private acx m11251c(long j) {
        C2113c cVar = (C2113c) this.f8640i.valueAt(0);
        C2113c cVar2 = (C2113c) this.f8640i.valueAt(this.f8640i.size() - 1);
        if (!this.f8647p.f704d || cVar2.mo12092d()) {
            return new C0159b(cVar.mo12088a(), cVar2.mo12090b());
        }
        return new C0158a(cVar.mo12088a(), cVar2.mo12091c() ? Long.MAX_VALUE : cVar2.mo12090b(), (this.f8641j.mo1052a() * 1000) - (j - (this.f8647p.f701a * 1000)), this.f8647p.f706f == -1 ? -1 : this.f8647p.f706f * 1000, this.f8641j);
    }

    /* renamed from: d */
    private long m11252d() {
        return this.f8643l != 0 ? (this.f8641j.mo1052a() * 1000) + this.f8643l : System.currentTimeMillis() * 1000;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ade mo12085a(C2113c cVar, C2114d dVar, ajr ajr, MediaFormat mediaFormat, C2112b bVar, int i, int i2, boolean z) {
        aed aed = dVar.f8675c;
        adl adl = aed.f728c;
        long a = dVar.mo12095a(i);
        long b = dVar.mo12098b(i);
        aec d = dVar.mo12100d(i);
        ajt ajt = new ajt(d.mo604a(), d.f721a, d.f722b, aed.mo611f());
        long j = cVar.f8665b - aed.f729d;
        if (m11250b(adl.f656b)) {
            return new adq(ajr, ajt, 1, adl, a, b, i, bVar.f8658a, null, cVar.f8664a);
        }
        return new adj(ajr, ajt, i2, adl, a, b, i, j, dVar.f8674b, mediaFormat, bVar.f8659b, bVar.f8660c, cVar.f8668e, z, cVar.f8664a);
    }

    /* renamed from: a */
    public final MediaFormat mo517a(int i) {
        return ((C2112b) this.f8639h.get(i)).f8658a;
    }

    /* renamed from: a */
    public void mo518a() throws IOException {
        if (this.f8655x != null) {
            throw this.f8655x;
        } else if (this.f8637f != null) {
            this.f8637f.mo12149d();
        }
    }

    /* renamed from: a */
    public void mo519a(long j) {
        if (this.f8637f != null && this.f8647p.f704d && this.f8655x == null) {
            adz adz = (adz) this.f8637f.mo12144a();
            if (!(adz == null || adz == this.f8648q)) {
                m11246a(adz);
                this.f8648q = adz;
            }
            long j2 = this.f8647p.f705e;
            if (j2 == 0) {
                j2 = 5000;
            }
            if (SystemClock.elapsedRealtime() > this.f8637f.mo12147b() + j2) {
                this.f8637f.mo12152g();
            }
        }
    }

    /* renamed from: a */
    public void mo520a(ade ade) {
        if (ade instanceof ado) {
            ado ado = (ado) ade;
            String str = ado.f578d.f655a;
            C2113c cVar = (C2113c) this.f8640i.get(ado.f580f);
            if (cVar != null) {
                C2114d dVar = (C2114d) cVar.f8666c.get(str);
                if (ado.mo540a()) {
                    dVar.f8677e = ado.mo541b();
                }
                if (dVar.f8676d == null && ado.mo544i()) {
                    dVar.f8676d = new adu((aeq) ado.mo545j(), ado.f579e.f1968a.toString());
                }
                if (cVar.f8668e == null && ado.mo542c()) {
                    cVar.f8668e = ado.mo543d();
                }
            }
        }
    }

    /* renamed from: a */
    public void mo521a(ade ade, Exception exc) {
    }

    /* renamed from: a */
    public void mo556a(adz adz, int i, int i2, int i3) {
        adw adw = (adw) adz.mo561a(i).f720c.get(i2);
        adl adl = ((aed) adw.f695c.get(i3)).f728c;
        String a = m11244a(adl);
        if (a == null) {
            Log.w("DashChunkSource", "Skipped track " + adl.f655a + " (unknown media mime type)");
            return;
        }
        MediaFormat a2 = m11243a(adw.f694b, adl, a, adz.f704d ? -1 : adz.f702b * 1000);
        if (a2 == null) {
            Log.w("DashChunkSource", "Skipped track " + adl.f655a + " (unknown media format)");
        } else {
            this.f8639h.add(new C2112b(a2, i2, adl));
        }
    }

    /* renamed from: a */
    public void mo557a(adz adz, int i, int i2, int[] iArr) {
        if (this.f8635d == null) {
            Log.w("DashChunkSource", "Skipping adaptive track (missing format evaluator)");
            return;
        }
        adw adw = (adw) adz.mo561a(i).f720c.get(i2);
        int i3 = 0;
        int i4 = 0;
        adl adl = null;
        adl[] adlArr = new adl[iArr.length];
        for (int i5 = 0; i5 < adlArr.length; i5++) {
            adl adl2 = ((aed) adw.f695c.get(iArr[i5])).f728c;
            if (adl == null || adl2.f659e > i4) {
                adl = adl2;
            }
            i3 = Math.max(i3, adl2.f658d);
            i4 = Math.max(i4, adl2.f659e);
            adlArr[i5] = adl2;
        }
        Arrays.sort(adlArr, new C0174a());
        long j = this.f8645n ? -1 : adz.f702b * 1000;
        String a = m11244a(adl);
        if (a == null) {
            Log.w("DashChunkSource", "Skipped adaptive track (unknown media mime type)");
            return;
        }
        MediaFormat a2 = m11243a(adw.f694b, adl, a, j);
        if (a2 == null) {
            Log.w("DashChunkSource", "Skipped adaptive track (unknown media format)");
        } else {
            this.f8639h.add(new C2112b(a2.mo12044b((String) null), i2, adlArr, i3, i4));
        }
    }

    /* renamed from: a */
    public void mo522a(List<? extends adp> list) {
        if (this.f8649r.mo12087a()) {
            this.f8635d.mo538b();
        }
        if (this.f8637f != null) {
            this.f8637f.mo12151f();
        }
        this.f8640i.clear();
        this.f8636e.f673c = null;
        this.f8651t = null;
        this.f8655x = null;
        this.f8649r = null;
    }

    /* renamed from: a */
    public final void mo523a(List<? extends adp> list, long j, adg adg) {
        boolean z;
        C2113c cVar;
        if (this.f8655x != null) {
            adg.f588b = null;
            return;
        }
        this.f8636e.f671a = list.size();
        if (this.f8636e.f673c == null || !this.f8654w) {
            if (this.f8649r.mo12087a()) {
                this.f8635d.mo537a(list, j, this.f8649r.f8663f, this.f8636e);
            } else {
                this.f8636e.f673c = this.f8649r.f8662e;
                this.f8636e.f672b = 2;
            }
        }
        adl adl = this.f8636e.f673c;
        adg.f587a = this.f8636e.f671a;
        if (adl == null) {
            adg.f588b = null;
        } else if (adg.f587a != list.size() || adg.f588b == null || !adg.f588b.f578d.equals(adl)) {
            adg.f588b = null;
            this.f8651t.mo449a(this.f8644m);
            if (list.isEmpty()) {
                if (this.f8645n) {
                    if (j != 0) {
                        this.f8653v = false;
                    }
                    j = this.f8653v ? Math.max(this.f8644m[0], this.f8644m[1] - this.f8642k) : Math.max(Math.min(j, this.f8644m[1] - 1), this.f8644m[0]);
                }
                cVar = m11249b(j);
                z = true;
            } else {
                if (this.f8653v) {
                    this.f8653v = false;
                }
                adp adp = (adp) list.get(adg.f587a - 1);
                long j2 = adp.f681i;
                if (this.f8645n && j2 < this.f8644m[0]) {
                    this.f8655x = new BehindLiveWindowException();
                    return;
                } else if (!this.f8647p.f704d || j2 < this.f8644m[1]) {
                    C2113c cVar2 = (C2113c) this.f8640i.valueAt(this.f8640i.size() - 1);
                    if (adp.f580f != cVar2.f8664a || !((C2114d) cVar2.f8666c.get(adp.f578d.f655a)).mo12099c(adp.mo546i())) {
                        z = false;
                        cVar = (C2113c) this.f8640i.get(adp.f580f);
                        if (cVar == null) {
                            cVar = (C2113c) this.f8640i.valueAt(0);
                            z = true;
                        } else if (!cVar.mo12091c() && ((C2114d) cVar.f8666c.get(adp.f578d.f655a)).mo12099c(adp.mo546i())) {
                            cVar = (C2113c) this.f8640i.get(adp.f580f + 1);
                            z = true;
                        }
                    } else if (!this.f8647p.f704d) {
                        adg.f589c = true;
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            C2114d dVar = (C2114d) cVar.f8666c.get(adl.f655a);
            aed aed = dVar.f8675c;
            aec aec = null;
            aec aec2 = null;
            MediaFormat mediaFormat = dVar.f8677e;
            if (mediaFormat == null) {
                aec = aed.mo608c();
            }
            if (dVar.f8676d == null) {
                aec2 = aed.mo609d();
            }
            if (aec == null && aec2 == null) {
                int i = list.isEmpty() ? dVar.mo12094a(j) : z ? dVar.mo12097b() : ((adp) list.get(adg.f587a - 1)).mo546i();
                ade a = mo12085a(cVar, dVar, this.f8634c, mediaFormat, this.f8649r, i, this.f8636e.f672b, mediaFormat != null);
                this.f8654w = false;
                adg.f588b = a;
                return;
            }
            ade a2 = m11242a(aec, aec2, aed, dVar.f8674b, this.f8634c, cVar.f8664a, this.f8636e.f672b);
            this.f8654w = true;
            adg.f588b = a2;
        }
    }

    /* renamed from: b */
    public void mo524b(int i) {
        this.f8649r = (C2112b) this.f8639h.get(i);
        if (this.f8649r.mo12087a()) {
            this.f8635d.mo536a();
        }
        if (this.f8637f != null) {
            this.f8637f.mo12150e();
            m11246a((adz) this.f8637f.mo12144a());
            return;
        }
        m11246a(this.f8647p);
    }

    /* renamed from: b */
    public boolean mo525b() {
        if (!this.f8652u) {
            this.f8652u = true;
            try {
                this.f8638g.mo555a(this.f8647p, 0, this);
            } catch (IOException e) {
                this.f8655x = e;
            }
        }
        return this.f8655x == null;
    }

    /* renamed from: c */
    public int mo526c() {
        return this.f8639h.size();
    }
}
