package com.google.android.exoplayer.drm;

public final class UnsupportedDrmException extends Exception {

    /* renamed from: a */
    public final int f8681a;

    public UnsupportedDrmException(int i) {
        this.f8681a = i;
    }

    public UnsupportedDrmException(int i, Exception exc) {
        super(exc);
        this.f8681a = i;
    }
}
