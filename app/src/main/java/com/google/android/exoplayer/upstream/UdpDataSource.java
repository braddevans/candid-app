package com.google.android.exoplayer.upstream;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketException;

public final class UdpDataSource implements ajz {

    /* renamed from: a */
    private final ajy f8818a;

    /* renamed from: b */
    private final DatagramPacket f8819b;

    /* renamed from: c */
    private final int f8820c;

    /* renamed from: d */
    private ajt f8821d;

    /* renamed from: e */
    private DatagramSocket f8822e;

    /* renamed from: f */
    private MulticastSocket f8823f;

    /* renamed from: g */
    private InetAddress f8824g;

    /* renamed from: h */
    private InetSocketAddress f8825h;

    /* renamed from: i */
    private boolean f8826i;

    /* renamed from: j */
    private byte[] f8827j;

    /* renamed from: k */
    private int f8828k;

    public static final class UdpDataSourceException extends IOException {
        public UdpDataSourceException(IOException iOException) {
            super(iOException);
        }
    }

    /* renamed from: a */
    public int mo836a(byte[] bArr, int i, int i2) throws UdpDataSourceException {
        if (this.f8828k == 0) {
            try {
                this.f8822e.receive(this.f8819b);
                this.f8828k = this.f8819b.getLength();
                if (this.f8818a != null) {
                    this.f8818a.mo992a(this.f8828k);
                }
            } catch (IOException e) {
                throw new UdpDataSourceException(e);
            }
        }
        int length = this.f8819b.getLength() - this.f8828k;
        int min = Math.min(this.f8828k, i2);
        System.arraycopy(this.f8827j, length, bArr, i, min);
        this.f8828k -= min;
        return min;
    }

    /* renamed from: a */
    public long mo837a(ajt ajt) throws UdpDataSourceException {
        this.f8821d = ajt;
        String host = ajt.f1968a.getHost();
        int port = ajt.f1968a.getPort();
        try {
            this.f8824g = InetAddress.getByName(host);
            this.f8825h = new InetSocketAddress(this.f8824g, port);
            if (this.f8824g.isMulticastAddress()) {
                this.f8823f = new MulticastSocket(this.f8825h);
                this.f8823f.joinGroup(this.f8824g);
                this.f8822e = this.f8823f;
            } else {
                this.f8822e = new DatagramSocket(this.f8825h);
            }
            try {
                this.f8822e.setSoTimeout(this.f8820c);
                this.f8826i = true;
                if (this.f8818a != null) {
                    this.f8818a.mo993b();
                }
                return -1;
            } catch (SocketException e) {
                throw new UdpDataSourceException(e);
            }
        } catch (IOException e2) {
            throw new UdpDataSourceException(e2);
        }
    }

    /* renamed from: a */
    public void mo838a() {
        if (this.f8823f != null) {
            try {
                this.f8823f.leaveGroup(this.f8824g);
            } catch (IOException e) {
            }
            this.f8823f = null;
        }
        if (this.f8822e != null) {
            this.f8822e.close();
            this.f8822e = null;
        }
        this.f8824g = null;
        this.f8825h = null;
        this.f8828k = 0;
        if (this.f8826i) {
            this.f8826i = false;
            if (this.f8818a != null) {
                this.f8818a.mo994c();
            }
        }
    }

    /* renamed from: b */
    public String mo996b() {
        if (this.f8821d == null) {
            return null;
        }
        return this.f8821d.f1968a.toString();
    }
}
