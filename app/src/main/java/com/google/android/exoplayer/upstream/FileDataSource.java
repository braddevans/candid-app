package com.google.android.exoplayer.upstream;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

public final class FileDataSource implements ajz {

    /* renamed from: a */
    private final ajy f8796a;

    /* renamed from: b */
    private RandomAccessFile f8797b;

    /* renamed from: c */
    private String f8798c;

    /* renamed from: d */
    private long f8799d;

    /* renamed from: e */
    private boolean f8800e;

    public static class FileDataSourceException extends IOException {
        public FileDataSourceException(IOException iOException) {
            super(iOException);
        }
    }

    public FileDataSource() {
        this(null);
    }

    public FileDataSource(ajy ajy) {
        this.f8796a = ajy;
    }

    /* renamed from: a */
    public int mo836a(byte[] bArr, int i, int i2) throws FileDataSourceException {
        if (this.f8799d == 0) {
            return -1;
        }
        try {
            int read = this.f8797b.read(bArr, i, (int) Math.min(this.f8799d, (long) i2));
            if (read <= 0) {
                return read;
            }
            this.f8799d -= (long) read;
            if (this.f8796a == null) {
                return read;
            }
            this.f8796a.mo992a(read);
            return read;
        } catch (IOException e) {
            throw new FileDataSourceException(e);
        }
    }

    /* renamed from: a */
    public long mo837a(ajt ajt) throws FileDataSourceException {
        try {
            this.f8798c = ajt.f1968a.toString();
            this.f8797b = new RandomAccessFile(ajt.f1968a.getPath(), "r");
            this.f8797b.seek(ajt.f1971d);
            this.f8799d = ajt.f1972e == -1 ? this.f8797b.length() - ajt.f1971d : ajt.f1972e;
            if (this.f8799d < 0) {
                throw new EOFException();
            }
            this.f8800e = true;
            if (this.f8796a != null) {
                this.f8796a.mo993b();
            }
            return this.f8799d;
        } catch (IOException e) {
            throw new FileDataSourceException(e);
        }
    }

    /* renamed from: a */
    public void mo838a() throws FileDataSourceException {
        this.f8798c = null;
        if (this.f8797b != null) {
            try {
                this.f8797b.close();
                this.f8797b = null;
                if (this.f8800e) {
                    this.f8800e = false;
                    if (this.f8796a != null) {
                        this.f8796a.mo994c();
                    }
                }
            } catch (IOException e) {
                throw new FileDataSourceException(e);
            } catch (Throwable th) {
                this.f8797b = null;
                if (this.f8800e) {
                    this.f8800e = false;
                    if (this.f8796a != null) {
                        this.f8796a.mo994c();
                    }
                }
                throw th;
            }
        }
    }

    /* renamed from: b */
    public String mo996b() {
        return this.f8798c;
    }
}
