package com.google.android.exoplayer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class MediaFormat implements Parcelable {
    public static final Creator<MediaFormat> CREATOR = new Creator<MediaFormat>() {
        /* renamed from: a */
        public MediaFormat createFromParcel(Parcel parcel) {
            return new MediaFormat(parcel);
        }

        /* renamed from: a */
        public MediaFormat[] newArray(int i) {
            return new MediaFormat[i];
        }
    };

    /* renamed from: a */
    public final String f8550a;

    /* renamed from: b */
    public final String f8551b;

    /* renamed from: c */
    public final int f8552c;

    /* renamed from: d */
    public final int f8553d;

    /* renamed from: e */
    public final long f8554e;

    /* renamed from: f */
    public final List<byte[]> f8555f;

    /* renamed from: g */
    public final boolean f8556g;

    /* renamed from: h */
    public final int f8557h;

    /* renamed from: i */
    public final int f8558i;

    /* renamed from: j */
    public final int f8559j;

    /* renamed from: k */
    public final int f8560k;

    /* renamed from: l */
    public final int f8561l;

    /* renamed from: m */
    public final float f8562m;

    /* renamed from: n */
    public final int f8563n;

    /* renamed from: o */
    public final byte[] f8564o;

    /* renamed from: p */
    public final int f8565p;

    /* renamed from: q */
    public final int f8566q;

    /* renamed from: r */
    public final int f8567r;

    /* renamed from: s */
    public final int f8568s;

    /* renamed from: t */
    public final int f8569t;

    /* renamed from: u */
    public final String f8570u;

    /* renamed from: v */
    public final long f8571v;

    /* renamed from: w */
    private int f8572w;

    /* renamed from: x */
    private android.media.MediaFormat f8573x;

    MediaFormat(Parcel parcel) {
        this.f8550a = parcel.readString();
        this.f8551b = parcel.readString();
        this.f8552c = parcel.readInt();
        this.f8553d = parcel.readInt();
        this.f8554e = parcel.readLong();
        this.f8557h = parcel.readInt();
        this.f8558i = parcel.readInt();
        this.f8561l = parcel.readInt();
        this.f8562m = parcel.readFloat();
        this.f8565p = parcel.readInt();
        this.f8566q = parcel.readInt();
        this.f8570u = parcel.readString();
        this.f8571v = parcel.readLong();
        this.f8555f = new ArrayList();
        parcel.readList(this.f8555f, null);
        this.f8556g = parcel.readInt() == 1;
        this.f8559j = parcel.readInt();
        this.f8560k = parcel.readInt();
        this.f8567r = parcel.readInt();
        this.f8568s = parcel.readInt();
        this.f8569t = parcel.readInt();
        this.f8564o = parcel.readInt() != 0 ? parcel.createByteArray() : null;
        this.f8563n = parcel.readInt();
    }

    MediaFormat(String str, String str2, int i, int i2, long j, int i3, int i4, int i5, float f, int i6, int i7, String str3, long j2, List<byte[]> list, boolean z, int i8, int i9, int i10, int i11, int i12, byte[] bArr, int i13) {
        this.f8550a = str;
        this.f8551b = akc.m2448a(str2);
        this.f8552c = i;
        this.f8553d = i2;
        this.f8554e = j;
        this.f8557h = i3;
        this.f8558i = i4;
        this.f8561l = i5;
        this.f8562m = f;
        this.f8565p = i6;
        this.f8566q = i7;
        this.f8570u = str3;
        this.f8571v = j2;
        if (list == null) {
            list = Collections.emptyList();
        }
        this.f8555f = list;
        this.f8556g = z;
        this.f8559j = i8;
        this.f8560k = i9;
        this.f8567r = i10;
        this.f8568s = i11;
        this.f8569t = i12;
        this.f8564o = bArr;
        this.f8563n = i13;
    }

    /* renamed from: a */
    public static MediaFormat m11162a() {
        return m11168a(null, "application/id3", -1, -1);
    }

    /* renamed from: a */
    public static MediaFormat m11163a(String str, String str2, int i, int i2, long j, int i3, int i4, List<byte[]> list) {
        return m11165a(str, str2, i, i2, j, i3, i4, list, -1, -1.0f, null, -1);
    }

    /* renamed from: a */
    public static MediaFormat m11164a(String str, String str2, int i, int i2, long j, int i3, int i4, List<byte[]> list, int i5, float f) {
        return new MediaFormat(str, str2, i, i2, j, i3, i4, i5, f, -1, -1, null, Long.MAX_VALUE, list, false, -1, -1, -1, -1, -1, null, -1);
    }

    /* renamed from: a */
    public static MediaFormat m11165a(String str, String str2, int i, int i2, long j, int i3, int i4, List<byte[]> list, int i5, float f, byte[] bArr, int i6) {
        return new MediaFormat(str, str2, i, i2, j, i3, i4, i5, f, -1, -1, null, Long.MAX_VALUE, list, false, -1, -1, -1, -1, -1, bArr, i6);
    }

    /* renamed from: a */
    public static MediaFormat m11166a(String str, String str2, int i, int i2, long j, int i3, int i4, List<byte[]> list, String str3) {
        return m11167a(str, str2, i, i2, j, i3, i4, list, str3, -1);
    }

    /* renamed from: a */
    public static MediaFormat m11167a(String str, String str2, int i, int i2, long j, int i3, int i4, List<byte[]> list, String str3, int i5) {
        return new MediaFormat(str, str2, i, i2, j, -1, -1, -1, -1.0f, i3, i4, str3, Long.MAX_VALUE, list, false, -1, -1, i5, -1, -1, null, -1);
    }

    /* renamed from: a */
    public static MediaFormat m11168a(String str, String str2, int i, long j) {
        return new MediaFormat(str, str2, i, -1, j, -1, -1, -1, -1.0f, -1, -1, null, Long.MAX_VALUE, null, false, -1, -1, -1, -1, -1, null, -1);
    }

    /* renamed from: a */
    public static MediaFormat m11169a(String str, String str2, int i, long j, String str3) {
        return m11170a(str, str2, i, j, str3, Long.MAX_VALUE);
    }

    /* renamed from: a */
    public static MediaFormat m11170a(String str, String str2, int i, long j, String str3, long j2) {
        return new MediaFormat(str, str2, i, -1, j, -1, -1, -1, -1.0f, -1, -1, str3, j2, null, false, -1, -1, -1, -1, -1, null, -1);
    }

    /* renamed from: a */
    public static MediaFormat m11171a(String str, String str2, int i, long j, List<byte[]> list, String str3) {
        return new MediaFormat(str, str2, i, -1, j, -1, -1, -1, -1.0f, -1, -1, str3, Long.MAX_VALUE, list, false, -1, -1, -1, -1, -1, null, -1);
    }

    @TargetApi(16)
    /* renamed from: a */
    private static final void m11172a(android.media.MediaFormat mediaFormat, String str, int i) {
        if (i != -1) {
            mediaFormat.setInteger(str, i);
        }
    }

    @TargetApi(16)
    /* renamed from: a */
    private static final void m11173a(android.media.MediaFormat mediaFormat, String str, String str2) {
        if (str2 != null) {
            mediaFormat.setString(str, str2);
        }
    }

    /* renamed from: a */
    public MediaFormat mo12036a(int i) {
        return new MediaFormat(this.f8550a, this.f8551b, this.f8552c, i, this.f8554e, this.f8557h, this.f8558i, this.f8561l, this.f8562m, this.f8565p, this.f8566q, this.f8570u, this.f8571v, this.f8555f, this.f8556g, this.f8559j, this.f8560k, this.f8567r, this.f8568s, this.f8569t, this.f8564o, this.f8563n);
    }

    /* renamed from: a */
    public MediaFormat mo12037a(int i, int i2) {
        return new MediaFormat(this.f8550a, this.f8551b, this.f8552c, this.f8553d, this.f8554e, this.f8557h, this.f8558i, this.f8561l, this.f8562m, this.f8565p, this.f8566q, this.f8570u, this.f8571v, this.f8555f, this.f8556g, i, i2, this.f8567r, this.f8568s, this.f8569t, this.f8564o, this.f8563n);
    }

    /* renamed from: a */
    public MediaFormat mo12038a(long j) {
        return new MediaFormat(this.f8550a, this.f8551b, this.f8552c, this.f8553d, this.f8554e, this.f8557h, this.f8558i, this.f8561l, this.f8562m, this.f8565p, this.f8566q, this.f8570u, j, this.f8555f, this.f8556g, this.f8559j, this.f8560k, this.f8567r, this.f8568s, this.f8569t, this.f8564o, this.f8563n);
    }

    /* renamed from: a */
    public MediaFormat mo12039a(String str) {
        return new MediaFormat(this.f8550a, this.f8551b, this.f8552c, this.f8553d, this.f8554e, this.f8557h, this.f8558i, this.f8561l, this.f8562m, this.f8565p, this.f8566q, str, this.f8571v, this.f8555f, this.f8556g, this.f8559j, this.f8560k, this.f8567r, this.f8568s, this.f8569t, this.f8564o, this.f8563n);
    }

    /* renamed from: a */
    public MediaFormat mo12040a(String str, int i, int i2, int i3, String str2) {
        return new MediaFormat(str, this.f8551b, i, this.f8553d, this.f8554e, i2, i3, this.f8561l, this.f8562m, this.f8565p, this.f8566q, str2, this.f8571v, this.f8555f, this.f8556g, -1, -1, this.f8567r, this.f8568s, this.f8569t, this.f8564o, this.f8563n);
    }

    @SuppressLint({"InlinedApi"})
    @TargetApi(16)
    /* renamed from: b */
    public final android.media.MediaFormat mo12041b() {
        if (this.f8573x == null) {
            android.media.MediaFormat mediaFormat = new android.media.MediaFormat();
            mediaFormat.setString("mime", this.f8551b);
            m11173a(mediaFormat, "language", this.f8570u);
            m11172a(mediaFormat, "max-input-size", this.f8553d);
            m11172a(mediaFormat, "width", this.f8557h);
            m11172a(mediaFormat, "height", this.f8558i);
            m11172a(mediaFormat, "rotation-degrees", this.f8561l);
            m11172a(mediaFormat, "max-width", this.f8559j);
            m11172a(mediaFormat, "max-height", this.f8560k);
            m11172a(mediaFormat, "channel-count", this.f8565p);
            m11172a(mediaFormat, "sample-rate", this.f8566q);
            m11172a(mediaFormat, "encoder-delay", this.f8568s);
            m11172a(mediaFormat, "encoder-padding", this.f8569t);
            for (int i = 0; i < this.f8555f.size(); i++) {
                mediaFormat.setByteBuffer("csd-" + i, ByteBuffer.wrap((byte[]) this.f8555f.get(i)));
            }
            if (this.f8554e != -1) {
                mediaFormat.setLong("durationUs", this.f8554e);
            }
            this.f8573x = mediaFormat;
        }
        return this.f8573x;
    }

    /* renamed from: b */
    public MediaFormat mo12042b(int i, int i2) {
        return new MediaFormat(this.f8550a, this.f8551b, this.f8552c, this.f8553d, this.f8554e, this.f8557h, this.f8558i, this.f8561l, this.f8562m, this.f8565p, this.f8566q, this.f8570u, this.f8571v, this.f8555f, this.f8556g, this.f8559j, this.f8560k, this.f8567r, i, i2, this.f8564o, this.f8563n);
    }

    /* renamed from: b */
    public MediaFormat mo12043b(long j) {
        return new MediaFormat(this.f8550a, this.f8551b, this.f8552c, this.f8553d, j, this.f8557h, this.f8558i, this.f8561l, this.f8562m, this.f8565p, this.f8566q, this.f8570u, this.f8571v, this.f8555f, this.f8556g, this.f8559j, this.f8560k, this.f8567r, this.f8568s, this.f8569t, this.f8564o, this.f8563n);
    }

    /* renamed from: b */
    public MediaFormat mo12044b(String str) {
        return new MediaFormat(str, this.f8551b, -1, -1, this.f8554e, -1, -1, -1, -1.0f, -1, -1, null, Long.MAX_VALUE, null, true, this.f8559j, this.f8560k, -1, -1, -1, null, this.f8563n);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MediaFormat mediaFormat = (MediaFormat) obj;
        if (this.f8556g != mediaFormat.f8556g || this.f8552c != mediaFormat.f8552c || this.f8553d != mediaFormat.f8553d || this.f8554e != mediaFormat.f8554e || this.f8557h != mediaFormat.f8557h || this.f8558i != mediaFormat.f8558i || this.f8561l != mediaFormat.f8561l || this.f8562m != mediaFormat.f8562m || this.f8559j != mediaFormat.f8559j || this.f8560k != mediaFormat.f8560k || this.f8565p != mediaFormat.f8565p || this.f8566q != mediaFormat.f8566q || this.f8567r != mediaFormat.f8567r || this.f8568s != mediaFormat.f8568s || this.f8569t != mediaFormat.f8569t || this.f8571v != mediaFormat.f8571v || !akw.m2579a((Object) this.f8550a, (Object) mediaFormat.f8550a) || !akw.m2579a((Object) this.f8570u, (Object) mediaFormat.f8570u) || !akw.m2579a((Object) this.f8551b, (Object) mediaFormat.f8551b) || this.f8555f.size() != mediaFormat.f8555f.size() || !Arrays.equals(this.f8564o, mediaFormat.f8564o) || this.f8563n != mediaFormat.f8563n) {
            return false;
        }
        for (int i = 0; i < this.f8555f.size(); i++) {
            if (!Arrays.equals((byte[]) this.f8555f.get(i), (byte[]) mediaFormat.f8555f.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        if (this.f8572w == 0) {
            int hashCode = ((((((((((((((((((((((((((((((((((this.f8550a == null ? 0 : this.f8550a.hashCode()) + 527) * 31) + (this.f8551b == null ? 0 : this.f8551b.hashCode())) * 31) + this.f8552c) * 31) + this.f8553d) * 31) + this.f8557h) * 31) + this.f8558i) * 31) + this.f8561l) * 31) + Float.floatToRawIntBits(this.f8562m)) * 31) + ((int) this.f8554e)) * 31) + (this.f8556g ? 1231 : 1237)) * 31) + this.f8559j) * 31) + this.f8560k) * 31) + this.f8565p) * 31) + this.f8566q) * 31) + this.f8567r) * 31) + this.f8568s) * 31) + this.f8569t) * 31;
            if (this.f8570u != null) {
                i = this.f8570u.hashCode();
            }
            int i2 = ((hashCode + i) * 31) + ((int) this.f8571v);
            for (int i3 = 0; i3 < this.f8555f.size(); i3++) {
                i2 = (i2 * 31) + Arrays.hashCode((byte[]) this.f8555f.get(i3));
            }
            this.f8572w = (((i2 * 31) + Arrays.hashCode(this.f8564o)) * 31) + this.f8563n;
        }
        return this.f8572w;
    }

    public String toString() {
        return "MediaFormat(" + this.f8550a + ", " + this.f8551b + ", " + this.f8552c + ", " + this.f8553d + ", " + this.f8557h + ", " + this.f8558i + ", " + this.f8561l + ", " + this.f8562m + ", " + this.f8565p + ", " + this.f8566q + ", " + this.f8570u + ", " + this.f8554e + ", " + this.f8556g + ", " + this.f8559j + ", " + this.f8560k + ", " + this.f8567r + ", " + this.f8568s + ", " + this.f8569t + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.f8550a);
        parcel.writeString(this.f8551b);
        parcel.writeInt(this.f8552c);
        parcel.writeInt(this.f8553d);
        parcel.writeLong(this.f8554e);
        parcel.writeInt(this.f8557h);
        parcel.writeInt(this.f8558i);
        parcel.writeInt(this.f8561l);
        parcel.writeFloat(this.f8562m);
        parcel.writeInt(this.f8565p);
        parcel.writeInt(this.f8566q);
        parcel.writeString(this.f8570u);
        parcel.writeLong(this.f8571v);
        parcel.writeList(this.f8555f);
        parcel.writeInt(this.f8556g ? 1 : 0);
        parcel.writeInt(this.f8559j);
        parcel.writeInt(this.f8560k);
        parcel.writeInt(this.f8567r);
        parcel.writeInt(this.f8568s);
        parcel.writeInt(this.f8569t);
        if (this.f8564o == null) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        if (this.f8564o != null) {
            parcel.writeByteArray(this.f8564o);
        }
        parcel.writeInt(this.f8563n);
    }
}
