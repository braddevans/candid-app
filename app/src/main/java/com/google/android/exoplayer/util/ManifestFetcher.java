package com.google.android.exoplayer.util;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.android.exoplayer.upstream.Loader;
import com.google.android.exoplayer.upstream.Loader.C2127a;
import com.google.android.exoplayer.upstream.Loader.C2129c;
import java.io.IOException;
import java.util.concurrent.CancellationException;

public class ManifestFetcher<T> implements C2127a {

    /* renamed from: a */
    volatile String f8829a;

    /* renamed from: b */
    private final C0287a<T> f8830b;

    /* renamed from: c */
    private final ajz f8831c;

    /* renamed from: d */
    private final Handler f8832d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final C2133a f8833e;

    /* renamed from: f */
    private int f8834f;

    /* renamed from: g */
    private Loader f8835g;

    /* renamed from: h */
    private aka<T> f8836h;

    /* renamed from: i */
    private long f8837i;

    /* renamed from: j */
    private int f8838j;

    /* renamed from: k */
    private long f8839k;

    /* renamed from: l */
    private ManifestIOException f8840l;

    /* renamed from: m */
    private volatile T f8841m;

    /* renamed from: n */
    private volatile long f8842n;

    /* renamed from: o */
    private volatile long f8843o;

    public static final class ManifestIOException extends IOException {
        public ManifestIOException(Throwable th) {
            super(th);
        }
    }

    /* renamed from: com.google.android.exoplayer.util.ManifestFetcher$a */
    public interface C2133a {
        /* renamed from: a */
        void mo12156a();

        /* renamed from: a */
        void mo12157a(IOException iOException);

        /* renamed from: b */
        void mo12158b();
    }

    /* renamed from: com.google.android.exoplayer.util.ManifestFetcher$b */
    public interface C2134b<T> {
        /* renamed from: a */
        void mo12159a(IOException iOException);

        /* renamed from: a */
        void mo12160a(T t);
    }

    /* renamed from: com.google.android.exoplayer.util.ManifestFetcher$c */
    public interface C2135c {
        /* renamed from: a */
        String mo562a();
    }

    /* renamed from: com.google.android.exoplayer.util.ManifestFetcher$d */
    class C2136d implements C2127a {

        /* renamed from: b */
        private final aka<T> f8849b;

        /* renamed from: c */
        private final Looper f8850c;

        /* renamed from: d */
        private final C2134b<T> f8851d;

        /* renamed from: e */
        private final Loader f8852e = new Loader("manifestLoader:single");

        /* renamed from: f */
        private long f8853f;

        public C2136d(aka<T> aka, Looper looper, C2134b<T> bVar) {
            this.f8849b = aka;
            this.f8850c = looper;
            this.f8851d = bVar;
        }

        /* renamed from: b */
        private void m11434b() {
            this.f8852e.mo12138c();
        }

        /* renamed from: a */
        public void mo12161a() {
            this.f8853f = SystemClock.elapsedRealtime();
            this.f8852e.mo12133a(this.f8850c, this.f8849b, this);
        }

        /* renamed from: a */
        public void mo507a(C2129c cVar) {
            try {
                Object a = this.f8849b.mo1051a();
                ManifestFetcher.this.mo12146a(a, this.f8853f);
                this.f8851d.mo12160a(a);
            } finally {
                m11434b();
            }
        }

        /* renamed from: a */
        public void mo508a(C2129c cVar, IOException iOException) {
            try {
                this.f8851d.mo12159a(iOException);
            } finally {
                m11434b();
            }
        }

        /* renamed from: b */
        public void mo509b(C2129c cVar) {
            try {
                this.f8851d.mo12159a((IOException) new ManifestIOException(new CancellationException()));
            } finally {
                m11434b();
            }
        }
    }

    public ManifestFetcher(String str, ajz ajz, C0287a<T> aVar) {
        this(str, ajz, aVar, null, null);
    }

    public ManifestFetcher(String str, ajz ajz, C0287a<T> aVar, Handler handler, C2133a aVar2) {
        this.f8830b = aVar;
        this.f8829a = str;
        this.f8831c = ajz;
        this.f8832d = handler;
        this.f8833e = aVar2;
    }

    /* renamed from: a */
    private long m11411a(long j) {
        return Math.min((j - 1) * 1000, 5000);
    }

    /* renamed from: a */
    private void m11413a(final IOException iOException) {
        if (this.f8832d != null && this.f8833e != null) {
            this.f8832d.post(new Runnable() {
                public void run() {
                    ManifestFetcher.this.f8833e.mo12157a(iOException);
                }
            });
        }
    }

    /* renamed from: h */
    private void m11414h() {
        if (this.f8832d != null && this.f8833e != null) {
            this.f8832d.post(new Runnable() {
                public void run() {
                    ManifestFetcher.this.f8833e.mo12156a();
                }
            });
        }
    }

    /* renamed from: i */
    private void m11415i() {
        if (this.f8832d != null && this.f8833e != null) {
            this.f8832d.post(new Runnable() {
                public void run() {
                    ManifestFetcher.this.f8833e.mo12158b();
                }
            });
        }
    }

    /* renamed from: a */
    public T mo12144a() {
        return this.f8841m;
    }

    /* renamed from: a */
    public void mo12145a(Looper looper, C2134b<T> bVar) {
        new C2136d(new aka(this.f8829a, this.f8831c, this.f8830b), looper, bVar).mo12161a();
    }

    /* renamed from: a */
    public void mo507a(C2129c cVar) {
        if (this.f8836h == cVar) {
            this.f8841m = this.f8836h.mo1051a();
            this.f8842n = this.f8837i;
            this.f8843o = SystemClock.elapsedRealtime();
            this.f8838j = 0;
            this.f8840l = null;
            if (this.f8841m instanceof C2135c) {
                String a = ((C2135c) this.f8841m).mo562a();
                if (!TextUtils.isEmpty(a)) {
                    this.f8829a = a;
                }
            }
            m11415i();
        }
    }

    /* renamed from: a */
    public void mo508a(C2129c cVar, IOException iOException) {
        if (this.f8836h == cVar) {
            this.f8838j++;
            this.f8839k = SystemClock.elapsedRealtime();
            this.f8840l = new ManifestIOException(iOException);
            m11413a((IOException) this.f8840l);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo12146a(T t, long j) {
        this.f8841m = t;
        this.f8842n = j;
        this.f8843o = SystemClock.elapsedRealtime();
    }

    /* renamed from: b */
    public long mo12147b() {
        return this.f8842n;
    }

    /* renamed from: b */
    public void mo509b(C2129c cVar) {
    }

    /* renamed from: c */
    public long mo12148c() {
        return this.f8843o;
    }

    /* renamed from: d */
    public void mo12149d() throws ManifestIOException {
        if (this.f8840l != null && this.f8838j > 1) {
            throw this.f8840l;
        }
    }

    /* renamed from: e */
    public void mo12150e() {
        int i = this.f8834f;
        this.f8834f = i + 1;
        if (i == 0) {
            this.f8838j = 0;
            this.f8840l = null;
        }
    }

    /* renamed from: f */
    public void mo12151f() {
        int i = this.f8834f - 1;
        this.f8834f = i;
        if (i == 0 && this.f8835g != null) {
            this.f8835g.mo12138c();
            this.f8835g = null;
        }
    }

    /* renamed from: g */
    public void mo12152g() {
        if (this.f8840l == null || SystemClock.elapsedRealtime() >= this.f8839k + m11411a((long) this.f8838j)) {
            if (this.f8835g == null) {
                this.f8835g = new Loader("manifestLoader");
            }
            if (!this.f8835g.mo12136a()) {
                this.f8836h = new aka<>(this.f8829a, this.f8831c, this.f8830b);
                this.f8837i = SystemClock.elapsedRealtime();
                this.f8835g.mo12134a((C2129c) this.f8836h, (C2127a) this);
                m11414h();
            }
        }
    }
}
