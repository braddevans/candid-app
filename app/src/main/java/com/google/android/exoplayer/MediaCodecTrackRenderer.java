package com.google.android.exoplayer;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodec.CodecException;
import android.media.MediaCodec.CryptoException;
import android.media.MediaCodec.CryptoInfo;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.SystemClock;
import com.google.android.exoplayer.MediaCodecUtil.DecoderQueryException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

@TargetApi(16)
public abstract class MediaCodecTrackRenderer extends acv {

    /* renamed from: c */
    private static final byte[] f8491c = akw.m2595f("0000016742C00BDA259000000168CE0F13200000016588840DCE7118A0002FBF1C31C3275D78");

    /* renamed from: A */
    private long f8492A;

    /* renamed from: B */
    private int f8493B;

    /* renamed from: C */
    private int f8494C;

    /* renamed from: D */
    private boolean f8495D;

    /* renamed from: E */
    private boolean f8496E;

    /* renamed from: F */
    private int f8497F;

    /* renamed from: G */
    private int f8498G;

    /* renamed from: H */
    private boolean f8499H;

    /* renamed from: I */
    private boolean f8500I;

    /* renamed from: J */
    private int f8501J;

    /* renamed from: K */
    private boolean f8502K;

    /* renamed from: L */
    private boolean f8503L;

    /* renamed from: M */
    private boolean f8504M;

    /* renamed from: N */
    private boolean f8505N;

    /* renamed from: a */
    public final acf f8506a;

    /* renamed from: b */
    protected final Handler f8507b;

    /* renamed from: d */
    private final acq f8508d;

    /* renamed from: e */
    private final aej<aem> f8509e;

    /* renamed from: f */
    private final boolean f8510f;

    /* renamed from: g */
    private final act f8511g;

    /* renamed from: h */
    private final acs f8512h;

    /* renamed from: i */
    private final List<Long> f8513i;

    /* renamed from: j */
    private final BufferInfo f8514j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public final C2098a f8515k;

    /* renamed from: l */
    private final boolean f8516l;

    /* renamed from: m */
    private MediaFormat f8517m;

    /* renamed from: n */
    private aei f8518n;

    /* renamed from: o */
    private MediaCodec f8519o;

    /* renamed from: p */
    private boolean f8520p;

    /* renamed from: q */
    private boolean f8521q;

    /* renamed from: r */
    private boolean f8522r;

    /* renamed from: s */
    private boolean f8523s;

    /* renamed from: t */
    private boolean f8524t;

    /* renamed from: u */
    private boolean f8525u;

    /* renamed from: v */
    private boolean f8526v;

    /* renamed from: w */
    private boolean f8527w;

    /* renamed from: x */
    private boolean f8528x;

    /* renamed from: y */
    private ByteBuffer[] f8529y;

    /* renamed from: z */
    private ByteBuffer[] f8530z;

    public static class DecoderInitializationException extends Exception {

        /* renamed from: a */
        public final String f8539a;

        /* renamed from: b */
        public final boolean f8540b;

        /* renamed from: c */
        public final String f8541c;

        /* renamed from: d */
        public final String f8542d;

        public DecoderInitializationException(MediaFormat mediaFormat, Throwable th, boolean z, int i) {
            super("Decoder init failed: [" + i + "], " + mediaFormat, th);
            this.f8539a = mediaFormat.f8551b;
            this.f8540b = z;
            this.f8541c = null;
            this.f8542d = m11134a(i);
        }

        public DecoderInitializationException(MediaFormat mediaFormat, Throwable th, boolean z, String str) {
            super("Decoder init failed: " + str + ", " + mediaFormat, th);
            this.f8539a = mediaFormat.f8551b;
            this.f8540b = z;
            this.f8541c = str;
            this.f8542d = akw.f2108a >= 21 ? m11135a(th) : null;
        }

        /* renamed from: a */
        private static String m11134a(int i) {
            return "com.google.android.exoplayer.MediaCodecTrackRenderer_" + (i < 0 ? "neg_" : "") + Math.abs(i);
        }

        @TargetApi(21)
        /* renamed from: a */
        private static String m11135a(Throwable th) {
            if (th instanceof CodecException) {
                return ((CodecException) th).getDiagnosticInfo();
            }
            return null;
        }
    }

    /* renamed from: com.google.android.exoplayer.MediaCodecTrackRenderer$a */
    public interface C2098a {
        /* renamed from: a */
        void mo11155a(CryptoException cryptoException);

        /* renamed from: a */
        void mo11157a(DecoderInitializationException decoderInitializationException);

        /* renamed from: a */
        void mo11159a(String str, long j, long j2);
    }

    public MediaCodecTrackRenderer(acu acu, acq acq, aej<aem> aej, boolean z, Handler handler, C2098a aVar) {
        this(new acu[]{acu}, acq, aej, z, handler, aVar);
    }

    public MediaCodecTrackRenderer(acu[] acuArr, acq acq, aej<aem> aej, boolean z, Handler handler, C2098a aVar) {
        super(acuArr);
        akc.m2451b(akw.f2108a >= 16);
        this.f8508d = (acq) akc.m2447a(acq);
        this.f8509e = aej;
        this.f8510f = z;
        this.f8507b = handler;
        this.f8515k = aVar;
        this.f8516l = m11088B();
        this.f8506a = new acf();
        this.f8511g = new act(0);
        this.f8512h = new acs();
        this.f8513i = new ArrayList();
        this.f8514j = new BufferInfo();
        this.f8497F = 0;
        this.f8498G = 0;
    }

    /* renamed from: A */
    private void m11087A() throws ExoPlaybackException {
        if (this.f8498G == 2) {
            mo12023p();
            mo12021m();
            return;
        }
        this.f8503L = true;
        mo400k();
    }

    /* renamed from: B */
    private static boolean m11088B() {
        return akw.f2108a <= 22 && "foster".equals(akw.f2109b) && "NVIDIA".equals(akw.f2110c);
    }

    /* renamed from: a */
    private static CryptoInfo m11089a(act act, int i) {
        CryptoInfo a = act.f520a.mo329a();
        if (i != 0) {
            if (a.numBytesOfClearData == null) {
                a.numBytesOfClearData = new int[1];
            }
            int[] iArr = a.numBytesOfClearData;
            iArr[0] = iArr[0] + i;
        }
        return a;
    }

    /* renamed from: a */
    private void m11091a(final CryptoException cryptoException) {
        if (this.f8507b != null && this.f8515k != null) {
            this.f8507b.post(new Runnable() {
                public void run() {
                    MediaCodecTrackRenderer.this.f8515k.mo11155a(cryptoException);
                }
            });
        }
    }

    /* renamed from: a */
    private void m11092a(DecoderInitializationException decoderInitializationException) throws ExoPlaybackException {
        m11099b(decoderInitializationException);
        throw new ExoPlaybackException((Throwable) decoderInitializationException);
    }

    /* renamed from: a */
    private void m11093a(String str, long j, long j2) {
        if (this.f8507b != null && this.f8515k != null) {
            final String str2 = str;
            final long j3 = j;
            final long j4 = j2;
            this.f8507b.post(new Runnable() {
                public void run() {
                    MediaCodecTrackRenderer.this.f8515k.mo11159a(str2, j3, j4);
                }
            });
        }
    }

    /* renamed from: a */
    private boolean m11094a(long j, boolean z) throws ExoPlaybackException {
        int a;
        if (this.f8502K || this.f8498G == 2) {
            return false;
        }
        if (this.f8493B < 0) {
            this.f8493B = this.f8519o.dequeueInputBuffer(0);
            if (this.f8493B < 0) {
                return false;
            }
            this.f8511g.f521b = this.f8529y[this.f8493B];
            this.f8511g.mo427d();
        }
        if (this.f8498G == 1) {
            if (!this.f8524t) {
                this.f8500I = true;
                this.f8519o.queueInputBuffer(this.f8493B, 0, 0, 0, 4);
                this.f8493B = -1;
            }
            this.f8498G = 2;
            return false;
        } else if (this.f8527w) {
            this.f8527w = false;
            this.f8511g.f521b.put(f8491c);
            this.f8519o.queueInputBuffer(this.f8493B, 0, f8491c.length, 0, 0);
            this.f8493B = -1;
            this.f8499H = true;
            return true;
        } else {
            if (this.f8504M) {
                a = -3;
            } else {
                if (this.f8497F == 1) {
                    for (int i = 0; i < this.f8517m.f8555f.size(); i++) {
                        this.f8511g.f521b.put((byte[]) this.f8517m.f8555f.get(i));
                    }
                    this.f8497F = 2;
                }
                a = mo441a(j, this.f8512h, this.f8511g);
                if (z && this.f8501J == 1 && a == -2) {
                    this.f8501J = 2;
                }
            }
            if (a == -2) {
                return false;
            }
            if (a == -4) {
                if (this.f8497F == 2) {
                    this.f8511g.mo427d();
                    this.f8497F = 1;
                }
                mo388a(this.f8512h);
                return true;
            } else if (a == -1) {
                if (this.f8497F == 2) {
                    this.f8511g.mo427d();
                    this.f8497F = 1;
                }
                this.f8502K = true;
                if (!this.f8499H) {
                    m11087A();
                    return false;
                }
                try {
                    if (!this.f8524t) {
                        this.f8500I = true;
                        this.f8519o.queueInputBuffer(this.f8493B, 0, 0, 0, 4);
                        this.f8493B = -1;
                    }
                    return false;
                } catch (CryptoException e) {
                    m11091a(e);
                    throw new ExoPlaybackException((Throwable) e);
                }
            } else {
                if (this.f8505N) {
                    if (!this.f8511g.mo426c()) {
                        this.f8511g.mo427d();
                        if (this.f8497F == 2) {
                            this.f8497F = 1;
                        }
                        return true;
                    }
                    this.f8505N = false;
                }
                boolean a2 = this.f8511g.mo424a();
                this.f8504M = m11097a(a2);
                if (this.f8504M) {
                    return false;
                }
                if (this.f8521q && !a2) {
                    akm.m2492a(this.f8511g.f521b);
                    if (this.f8511g.f521b.position() == 0) {
                        return true;
                    }
                    this.f8521q = false;
                }
                try {
                    int position = this.f8511g.f521b.position();
                    int i2 = position - this.f8511g.f522c;
                    long j2 = this.f8511g.f524e;
                    if (this.f8511g.mo425b()) {
                        this.f8513i.add(Long.valueOf(j2));
                    }
                    mo12019a(j2, this.f8511g.f521b, position, a2);
                    if (a2) {
                        this.f8519o.queueSecureInputBuffer(this.f8493B, 0, m11089a(this.f8511g, i2), j2, 0);
                    } else {
                        this.f8519o.queueInputBuffer(this.f8493B, 0, position, j2, 0);
                    }
                    this.f8493B = -1;
                    this.f8499H = true;
                    this.f8497F = 0;
                    this.f8506a.f397c++;
                    return true;
                } catch (CryptoException e2) {
                    m11091a(e2);
                    throw new ExoPlaybackException((Throwable) e2);
                }
            }
        }
    }

    /* renamed from: a */
    private static boolean mo393a(String str) {
        return akw.f2108a < 18 || (akw.f2108a == 18 && ("OMX.SEC.avc.dec".equals(str) || "OMX.SEC.avc.dec.secure".equals(str))) || (akw.f2108a == 19 && akw.f2111d.startsWith("SM-G800") && ("OMX.Exynos.avc.dec".equals(str) || "OMX.Exynos.avc.dec.secure".equals(str)));
    }

    /* renamed from: a */
    private static boolean m11096a(String str, MediaFormat mediaFormat) {
        return akw.f2108a < 21 && mediaFormat.f8555f.isEmpty() && "OMX.MTK.VIDEO.DECODER.AVC".equals(str);
    }

    /* renamed from: a */
    private boolean m11097a(boolean z) throws ExoPlaybackException {
        if (!this.f8495D) {
            return false;
        }
        int b = this.f8509e.mo639b();
        if (b == 0) {
            throw new ExoPlaybackException((Throwable) this.f8509e.mo641d());
        } else if (b != 4) {
            return z || !this.f8510f;
        } else {
            return false;
        }
    }

    /* renamed from: b */
    private MediaFormat m11098b(MediaFormat mediaFormat) {
        MediaFormat b = mediaFormat.mo12041b();
        if (this.f8516l) {
            b.setInteger("auto-frc", 0);
        }
        return b;
    }

    /* renamed from: b */
    private void m11099b(final DecoderInitializationException decoderInitializationException) {
        if (this.f8507b != null && this.f8515k != null) {
            this.f8507b.post(new Runnable() {
                public void run() {
                    MediaCodecTrackRenderer.this.f8515k.mo11157a(decoderInitializationException);
                }
            });
        }
    }

    /* renamed from: b */
    private boolean m11100b(long j, long j2) throws ExoPlaybackException {
        if (this.f8503L) {
            return false;
        }
        if (this.f8494C < 0) {
            this.f8494C = this.f8519o.dequeueOutputBuffer(this.f8514j, mo12026s());
        }
        if (this.f8494C == -2) {
            m11108z();
            return true;
        } else if (this.f8494C == -3) {
            this.f8530z = this.f8519o.getOutputBuffers();
            this.f8506a.f399e++;
            return true;
        } else if (this.f8494C < 0) {
            if (!this.f8524t || (!this.f8502K && this.f8498G != 2)) {
                return false;
            }
            m11087A();
            return true;
        } else if (this.f8528x) {
            this.f8528x = false;
            this.f8519o.releaseOutputBuffer(this.f8494C, false);
            this.f8494C = -1;
            return true;
        } else if ((this.f8514j.flags & 4) != 0) {
            m11087A();
            return false;
        } else {
            int h = m11106h(this.f8514j.presentationTimeUs);
            if (!mo391a(j, j2, this.f8519o, this.f8530z[this.f8494C], this.f8514j, this.f8494C, h != -1)) {
                return false;
            }
            mo12020d(this.f8514j.presentationTimeUs);
            if (h != -1) {
                this.f8513i.remove(h);
            }
            this.f8494C = -1;
            return true;
        }
    }

    /* renamed from: b */
    private static boolean m11101b(String str) {
        return akw.f2108a < 24 && ("OMX.Nvidia.h264.decode".equals(str) || "OMX.Nvidia.h264.decode.secure".equals(str)) && (akw.f2109b.equals("flounder") || akw.f2109b.equals("flounder_lte") || akw.f2109b.equals("grouper") || akw.f2109b.equals("tilapia"));
    }

    /* renamed from: b */
    private static boolean m11102b(String str, MediaFormat mediaFormat) {
        return akw.f2108a <= 18 && mediaFormat.f8565p == 1 && "OMX.MTK.AUDIO.DECODER.MP3".equals(str);
    }

    /* renamed from: c */
    private static boolean m11103c(String str) {
        return akw.f2108a <= 17 && ("OMX.rk.video_decoder.avc".equals(str) || "OMX.allwinner.video.decoder.avc".equals(str));
    }

    /* renamed from: d */
    private static boolean m11104d(String str) {
        return akw.f2108a <= 23 && "OMX.google.vorbis.decoder".equals(str);
    }

    /* renamed from: g */
    private void m11105g(long j) throws ExoPlaybackException {
        if (mo441a(j, this.f8512h, (act) null) == -4) {
            mo388a(this.f8512h);
        }
    }

    /* renamed from: h */
    private int m11106h(long j) {
        int size = this.f8513i.size();
        for (int i = 0; i < size; i++) {
            if (((Long) this.f8513i.get(i)).longValue() == j) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: l */
    private boolean mo401l() {
        return SystemClock.elapsedRealtime() < this.f8492A + 1000;
    }

    /* renamed from: z */
    private void m11108z() throws ExoPlaybackException {
        MediaFormat outputFormat = this.f8519o.getOutputFormat();
        if (this.f8523s && outputFormat.getInteger("width") == 32 && outputFormat.getInteger("height") == 32) {
            this.f8528x = true;
            return;
        }
        if (this.f8526v) {
            outputFormat.setInteger("channel-count", 1);
        }
        mo389a(this.f8519o, outputFormat);
        this.f8506a.f398d++;
    }

    /* renamed from: a */
    public ach mo387a(acq acq, String str, boolean z) throws DecoderQueryException {
        return acq.mo409a(str, z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo442a(long j, long j2, boolean z) throws ExoPlaybackException {
        int i = z ? this.f8501J == 0 ? 1 : this.f8501J : 0;
        this.f8501J = i;
        if (this.f8517m == null) {
            m11105g(j);
        }
        mo12021m();
        if (this.f8519o != null) {
            aku.m2557a("drainAndFeed");
            do {
            } while (m11100b(j, j2));
            if (m11094a(j, true)) {
                do {
                } while (m11094a(j, false));
            }
            aku.m2556a();
        }
        this.f8506a.mo328a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12019a(long j, ByteBuffer byteBuffer, int i, boolean z) {
    }

    /* renamed from: a */
    public void mo388a(acs acs) throws ExoPlaybackException {
        boolean z = true;
        MediaFormat mediaFormat = this.f8517m;
        this.f8517m = acs.f518a;
        this.f8518n = acs.f519b;
        if (!akw.m2579a((Object) this.f8517m, (Object) mediaFormat)) {
            if (this.f8519o != null && mo413a(this.f8519o, this.f8520p, mediaFormat, this.f8517m)) {
                this.f8496E = true;
                this.f8497F = 1;
                if (!(this.f8523s && this.f8517m.f8557h == mediaFormat.f8557h && this.f8517m.f8558i == mediaFormat.f8558i)) {
                    z = false;
                }
                this.f8527w = z;
            } else if (this.f8499H) {
                this.f8498G = 1;
            } else {
                mo12023p();
                mo12021m();
            }
        }
    }

    /* renamed from: a */
    public void mo389a(MediaCodec mediaCodec, MediaFormat mediaFormat) throws ExoPlaybackException {
    }

    /* renamed from: a */
    public abstract void mo390a(MediaCodec mediaCodec, boolean z, MediaFormat mediaFormat, MediaCrypto mediaCrypto);

    /* renamed from: a */
    public abstract boolean mo391a(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, BufferInfo bufferInfo, int i, boolean z) throws ExoPlaybackException;

    /* renamed from: a */
    public abstract boolean mo392a(acq acq, MediaFormat mediaFormat) throws DecoderQueryException;

    /* renamed from: a */
    public boolean mo413a(MediaCodec mediaCodec, boolean z, MediaFormat mediaFormat, MediaFormat mediaFormat2) {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo443a(MediaFormat mediaFormat) throws DecoderQueryException {
        return mo392a(this.f8508d, mediaFormat);
    }

    /* renamed from: b */
    public boolean mo342b() {
        return this.f8503L;
    }

    /* renamed from: c */
    public void mo395c(long j) throws ExoPlaybackException {
        this.f8501J = 0;
        this.f8502K = false;
        this.f8503L = false;
        if (this.f8519o != null) {
            mo12024q();
        }
    }

    /* renamed from: c */
    public boolean mo343c() {
        return this.f8517m != null && !this.f8504M && (this.f8501J != 0 || this.f8494C >= 0 || mo401l());
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo12020d(long j) {
    }

    /* renamed from: h */
    public void mo397h() {
    }

    /* renamed from: i */
    public void mo398i() {
    }

    /* renamed from: j */
    public void mo399j() throws ExoPlaybackException {
        this.f8517m = null;
        this.f8518n = null;
        try {
            mo12023p();
            try {
                if (this.f8495D) {
                    this.f8509e.mo636a();
                    this.f8495D = false;
                }
            } finally {
                super.mo399j();
            }
        } catch (Throwable th) {
            if (this.f8495D) {
                this.f8509e.mo636a();
                this.f8495D = false;
            }
            throw th;
        } finally {
            super.mo399j();
        }
    }

    /* renamed from: k */
    public void mo400k() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: m */
    public final void mo12021m() throws ExoPlaybackException {
        if (mo416n()) {
            String str = this.f8517m.f8551b;
            MediaCrypto mediaCrypto = null;
            boolean z = false;
            if (this.f8518n != null) {
                if (this.f8509e == null) {
                    throw new ExoPlaybackException("Media requires a DrmSessionManager");
                }
                if (!this.f8495D) {
                    this.f8509e.mo637a(this.f8518n);
                    this.f8495D = true;
                }
                int b = this.f8509e.mo639b();
                if (b == 0) {
                    throw new ExoPlaybackException((Throwable) this.f8509e.mo641d());
                } else if (b == 3 || b == 4) {
                    mediaCrypto = ((aem) this.f8509e.mo640c()).mo653a();
                    z = this.f8509e.mo638a(str);
                } else {
                    return;
                }
            }
            ach ach = null;
            try {
                ach = mo387a(this.f8508d, str, z);
            } catch (DecoderQueryException e) {
                m11092a(new DecoderInitializationException(this.f8517m, (Throwable) e, z, -49998));
            }
            if (ach == null) {
                m11092a(new DecoderInitializationException(this.f8517m, (Throwable) null, z, -49999));
            }
            String str2 = ach.f411a;
            this.f8520p = ach.f413c;
            this.f8521q = m11096a(str2, this.f8517m);
            this.f8522r = mo393a(str2);
            this.f8523s = m11101b(str2);
            this.f8524t = m11103c(str2);
            this.f8525u = m11104d(str2);
            this.f8526v = m11102b(str2, this.f8517m);
            try {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                aku.m2557a("createByCodecName(" + str2 + ")");
                this.f8519o = MediaCodec.createByCodecName(str2);
                aku.m2556a();
                aku.m2557a("configureCodec");
                mo390a(this.f8519o, ach.f413c, m11098b(this.f8517m), mediaCrypto);
                aku.m2556a();
                aku.m2557a("codec.start()");
                this.f8519o.start();
                aku.m2556a();
                long elapsedRealtime2 = SystemClock.elapsedRealtime();
                m11093a(str2, elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
                this.f8529y = this.f8519o.getInputBuffers();
                this.f8530z = this.f8519o.getOutputBuffers();
            } catch (Exception e2) {
                m11092a(new DecoderInitializationException(this.f8517m, (Throwable) e2, z, str2));
            }
            this.f8492A = mo456u() == 3 ? SystemClock.elapsedRealtime() : -1;
            this.f8493B = -1;
            this.f8494C = -1;
            this.f8505N = true;
            this.f8506a.f395a++;
        }
    }

    /* renamed from: n */
    public boolean mo416n() {
        return this.f8519o == null && this.f8517m != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public final boolean mo12022o() {
        return this.f8519o != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public void mo12023p() {
        if (this.f8519o != null) {
            this.f8492A = -1;
            this.f8493B = -1;
            this.f8494C = -1;
            this.f8504M = false;
            this.f8513i.clear();
            this.f8529y = null;
            this.f8530z = null;
            this.f8496E = false;
            this.f8499H = false;
            this.f8520p = false;
            this.f8521q = false;
            this.f8522r = false;
            this.f8523s = false;
            this.f8524t = false;
            this.f8525u = false;
            this.f8526v = false;
            this.f8527w = false;
            this.f8528x = false;
            this.f8500I = false;
            this.f8497F = 0;
            this.f8498G = 0;
            this.f8506a.f396b++;
            try {
                this.f8519o.stop();
                try {
                    this.f8519o.release();
                } finally {
                    this.f8519o = null;
                }
            } catch (Throwable th) {
                this.f8519o.release();
                throw th;
            } finally {
                this.f8519o = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public void mo12024q() throws ExoPlaybackException {
        this.f8492A = -1;
        this.f8493B = -1;
        this.f8494C = -1;
        this.f8505N = true;
        this.f8504M = false;
        this.f8513i.clear();
        this.f8527w = false;
        this.f8528x = false;
        if (this.f8522r || (this.f8525u && this.f8500I)) {
            mo12023p();
            mo12021m();
        } else if (this.f8498G != 0) {
            mo12023p();
            mo12021m();
        } else {
            this.f8519o.flush();
            this.f8499H = false;
        }
        if (this.f8496E && this.f8517m != null) {
            this.f8497F = 1;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public final int mo12025r() {
        return this.f8501J;
    }

    /* access modifiers changed from: protected */
    /* renamed from: s */
    public long mo12026s() {
        return 0;
    }
}
