package com.google.android.exoplayer.upstream;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

public final class Loader {

    /* renamed from: a */
    private final ExecutorService f8807a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C2128b f8808b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public boolean f8809c;

    public static final class UnexpectedLoaderException extends IOException {
        public UnexpectedLoaderException(Exception exc) {
            super("Unexpected " + exc.getClass().getSimpleName() + ": " + exc.getMessage(), exc);
        }
    }

    /* renamed from: com.google.android.exoplayer.upstream.Loader$a */
    public interface C2127a {
        /* renamed from: a */
        void mo507a(C2129c cVar);

        /* renamed from: a */
        void mo508a(C2129c cVar, IOException iOException);

        /* renamed from: b */
        void mo509b(C2129c cVar);
    }

    @SuppressLint({"HandlerLeak"})
    /* renamed from: com.google.android.exoplayer.upstream.Loader$b */
    final class C2128b extends Handler implements Runnable {

        /* renamed from: b */
        private final C2129c f8811b;

        /* renamed from: c */
        private final C2127a f8812c;

        /* renamed from: d */
        private volatile Thread f8813d;

        public C2128b(Looper looper, C2129c cVar, C2127a aVar) {
            super(looper);
            this.f8811b = cVar;
            this.f8812c = aVar;
        }

        /* renamed from: b */
        private void m11400b() {
            Loader.this.f8809c = false;
            Loader.this.f8808b = null;
        }

        /* renamed from: a */
        public void mo12139a() {
            this.f8811b.mo527f();
            if (this.f8813d != null) {
                this.f8813d.interrupt();
            }
        }

        public void handleMessage(Message message) {
            if (message.what == 2) {
                throw ((Error) message.obj);
            }
            m11400b();
            if (this.f8811b.mo528g()) {
                this.f8812c.mo509b(this.f8811b);
                return;
            }
            switch (message.what) {
                case 0:
                    this.f8812c.mo507a(this.f8811b);
                    return;
                case 1:
                    this.f8812c.mo508a(this.f8811b, (IOException) message.obj);
                    return;
                default:
                    return;
            }
        }

        public void run() {
            try {
                this.f8813d = Thread.currentThread();
                if (!this.f8811b.mo528g()) {
                    aku.m2557a(this.f8811b.getClass().getSimpleName() + ".load()");
                    this.f8811b.mo529h();
                    aku.m2556a();
                }
                sendEmptyMessage(0);
            } catch (IOException e) {
                obtainMessage(1, e).sendToTarget();
            } catch (InterruptedException e2) {
                akc.m2451b(this.f8811b.mo528g());
                sendEmptyMessage(0);
            } catch (Exception e3) {
                Log.e("LoadTask", "Unexpected exception loading stream", e3);
                obtainMessage(1, new UnexpectedLoaderException(e3)).sendToTarget();
            } catch (Error e4) {
                Log.e("LoadTask", "Unexpected error loading stream", e4);
                obtainMessage(2, e4).sendToTarget();
                throw e4;
            }
        }
    }

    /* renamed from: com.google.android.exoplayer.upstream.Loader$c */
    public interface C2129c {
        /* renamed from: f */
        void mo527f();

        /* renamed from: g */
        boolean mo528g();

        /* renamed from: h */
        void mo529h() throws IOException, InterruptedException;
    }

    public Loader(String str) {
        this.f8807a = akw.m2574a(str);
    }

    /* renamed from: a */
    public void mo12133a(Looper looper, C2129c cVar, C2127a aVar) {
        akc.m2451b(!this.f8809c);
        this.f8809c = true;
        this.f8808b = new C2128b(looper, cVar, aVar);
        this.f8807a.submit(this.f8808b);
    }

    /* renamed from: a */
    public void mo12134a(C2129c cVar, C2127a aVar) {
        Looper myLooper = Looper.myLooper();
        akc.m2451b(myLooper != null);
        mo12133a(myLooper, cVar, aVar);
    }

    /* renamed from: a */
    public void mo12135a(Runnable runnable) {
        if (this.f8809c) {
            mo12137b();
        }
        if (runnable != null) {
            this.f8807a.submit(runnable);
        }
        this.f8807a.shutdown();
    }

    /* renamed from: a */
    public boolean mo12136a() {
        return this.f8809c;
    }

    /* renamed from: b */
    public void mo12137b() {
        akc.m2451b(this.f8809c);
        this.f8808b.mo12139a();
    }

    /* renamed from: c */
    public void mo12138c() {
        mo12135a(null);
    }
}
