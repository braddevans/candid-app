package com.google.android.exoplayer.text;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public final class SubtitleLayout extends View {

    /* renamed from: a */
    private final List<aij> f8776a;

    /* renamed from: b */
    private List<aii> f8777b;

    /* renamed from: c */
    private int f8778c;

    /* renamed from: d */
    private float f8779d;

    /* renamed from: e */
    private boolean f8780e;

    /* renamed from: f */
    private aih f8781f;

    /* renamed from: g */
    private float f8782g;

    public SubtitleLayout(Context context) {
        this(context, null);
    }

    public SubtitleLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f8776a = new ArrayList();
        this.f8778c = 0;
        this.f8779d = 0.0533f;
        this.f8780e = true;
        this.f8781f = aih.f1770a;
        this.f8782g = 0.08f;
    }

    /* renamed from: a */
    private void m11374a(int i, float f) {
        if (this.f8778c != i || this.f8779d != f) {
            this.f8778c = i;
            this.f8779d = f;
            invalidate();
        }
    }

    public void dispatchDraw(Canvas canvas) {
        float f;
        int size = this.f8777b == null ? 0 : this.f8777b.size();
        int top = getTop();
        int bottom = getBottom();
        int left = getLeft() + getPaddingLeft();
        int paddingTop = top + getPaddingTop();
        int right = getRight() + getPaddingRight();
        int paddingBottom = bottom - getPaddingBottom();
        if (paddingBottom > paddingTop && right > left) {
            if (this.f8778c == 2) {
                f = this.f8779d;
            } else {
                f = this.f8779d * ((float) (this.f8778c == 0 ? paddingBottom - paddingTop : bottom - top));
            }
            if (f > 0.0f) {
                for (int i = 0; i < size; i++) {
                    ((aij) this.f8776a.get(i)).mo903a((aii) this.f8777b.get(i), this.f8780e, this.f8781f, f, this.f8782g, canvas, left, paddingTop, right, paddingBottom);
                }
            }
        }
    }

    public void setApplyEmbeddedStyles(boolean z) {
        if (this.f8780e != z) {
            this.f8780e = z;
            invalidate();
        }
    }

    public void setBottomPaddingFraction(float f) {
        if (this.f8782g != f) {
            this.f8782g = f;
            invalidate();
        }
    }

    public void setCues(List<aii> list) {
        if (this.f8777b != list) {
            this.f8777b = list;
            int size = list == null ? 0 : list.size();
            while (this.f8776a.size() < size) {
                this.f8776a.add(new aij(getContext()));
            }
            invalidate();
        }
    }

    public void setFixedTextSize(int i, float f) {
        Context context = getContext();
        m11374a(2, TypedValue.applyDimension(i, f, (context == null ? Resources.getSystem() : context.getResources()).getDisplayMetrics()));
    }

    public void setFractionalTextSize(float f) {
        setFractionalTextSize(f, false);
    }

    public void setFractionalTextSize(float f, boolean z) {
        m11374a(z ? 1 : 0, f);
    }

    public void setStyle(aih aih) {
        if (this.f8781f != aih) {
            this.f8781f = aih;
            invalidate();
        }
    }
}
