package com.google.android.exoplayer;

import android.annotation.TargetApi;
import android.media.MediaCodecInfo;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecInfo.CodecProfileLevel;
import android.media.MediaCodecInfo.VideoCapabilities;
import android.media.MediaCodecList;
import android.support.p001v4.app.FragmentTransaction;
import android.support.p001v4.app.NotificationCompat;
import android.support.p003v7.widget.RecyclerView.ItemAnimator;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TargetApi(16)
public final class MediaCodecUtil {

    /* renamed from: a */
    private static final ach f8543a = new ach("OMX.google.raw.decoder", null);

    /* renamed from: b */
    private static final Map<C2100a, List<ach>> f8544b = new HashMap();

    /* renamed from: c */
    private static int f8545c = -1;

    public static class DecoderQueryException extends IOException {
        private DecoderQueryException(Throwable th) {
            super("Failed to query underlying media codecs", th);
        }
    }

    /* renamed from: com.google.android.exoplayer.MediaCodecUtil$a */
    static final class C2100a {

        /* renamed from: a */
        public final String f8546a;

        /* renamed from: b */
        public final boolean f8547b;

        public C2100a(String str, boolean z) {
            this.f8546a = str;
            this.f8547b = z;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || obj.getClass() != C2100a.class) {
                return false;
            }
            C2100a aVar = (C2100a) obj;
            return TextUtils.equals(this.f8546a, aVar.f8546a) && this.f8547b == aVar.f8547b;
        }

        public int hashCode() {
            return (((this.f8546a == null ? 0 : this.f8546a.hashCode()) + 31) * 31) + (this.f8547b ? 1231 : 1237);
        }
    }

    /* renamed from: com.google.android.exoplayer.MediaCodecUtil$b */
    interface C2101b {
        /* renamed from: a */
        int mo12032a();

        /* renamed from: a */
        MediaCodecInfo mo12033a(int i);

        /* renamed from: a */
        boolean mo12034a(String str, CodecCapabilities codecCapabilities);

        /* renamed from: b */
        boolean mo12035b();
    }

    /* renamed from: com.google.android.exoplayer.MediaCodecUtil$c */
    static final class C2102c implements C2101b {
        private C2102c() {
        }

        /* renamed from: a */
        public int mo12032a() {
            return MediaCodecList.getCodecCount();
        }

        /* renamed from: a */
        public MediaCodecInfo mo12033a(int i) {
            return MediaCodecList.getCodecInfoAt(i);
        }

        /* renamed from: a */
        public boolean mo12034a(String str, CodecCapabilities codecCapabilities) {
            return "video/avc".equals(str);
        }

        /* renamed from: b */
        public boolean mo12035b() {
            return false;
        }
    }

    @TargetApi(21)
    /* renamed from: com.google.android.exoplayer.MediaCodecUtil$d */
    static final class C2103d implements C2101b {

        /* renamed from: a */
        private final int f8548a;

        /* renamed from: b */
        private MediaCodecInfo[] f8549b;

        public C2103d(boolean z) {
            this.f8548a = z ? 1 : 0;
        }

        /* renamed from: c */
        private void m11157c() {
            if (this.f8549b == null) {
                this.f8549b = new MediaCodecList(this.f8548a).getCodecInfos();
            }
        }

        /* renamed from: a */
        public int mo12032a() {
            m11157c();
            return this.f8549b.length;
        }

        /* renamed from: a */
        public MediaCodecInfo mo12033a(int i) {
            m11157c();
            return this.f8549b[i];
        }

        /* renamed from: a */
        public boolean mo12034a(String str, CodecCapabilities codecCapabilities) {
            return codecCapabilities.isFeatureSupported("secure-playback");
        }

        /* renamed from: b */
        public boolean mo12035b() {
            return true;
        }
    }

    /* renamed from: a */
    private static int m11139a(int i) {
        switch (i) {
            case 1:
            case 2:
                return 25344;
            case 8:
                return 101376;
            case 16:
                return 101376;
            case 32:
                return 101376;
            case 64:
                return 202752;
            case NotificationCompat.FLAG_HIGH_PRIORITY /*128*/:
                return 414720;
            case NotificationCompat.FLAG_LOCAL_ONLY /*256*/:
                return 414720;
            case NotificationCompat.FLAG_GROUP_SUMMARY /*512*/:
                return 921600;
            case 1024:
                return 1310720;
            case ItemAnimator.FLAG_MOVED /*2048*/:
                return 2097152;
            case 4096:
                return 2097152;
            case FragmentTransaction.TRANSIT_EXIT_MASK /*8192*/:
                return 2228224;
            case 16384:
                return 5652480;
            case 32768:
                return 9437184;
            default:
                return -1;
        }
    }

    /* renamed from: a */
    public static ach m11140a() {
        return f8543a;
    }

    /* renamed from: a */
    public static ach m11141a(String str, boolean z) throws DecoderQueryException {
        List b = m11147b(str, z);
        if (b.isEmpty()) {
            return null;
        }
        return (ach) b.get(0);
    }

    /* renamed from: a */
    private static List<ach> m11142a(C2100a aVar, C2101b bVar) throws DecoderQueryException {
        ArrayList arrayList;
        String name;
        String str;
        try {
            arrayList = new ArrayList();
            String str2 = aVar.f8546a;
            int a = bVar.mo12032a();
            boolean b = bVar.mo12035b();
            int i = 0;
            loop0:
            while (true) {
                if (i >= a) {
                    break;
                }
                MediaCodecInfo a2 = bVar.mo12033a(i);
                name = a2.getName();
                if (m11143a(a2, name, b)) {
                    String[] supportedTypes = a2.getSupportedTypes();
                    int length = supportedTypes.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        str = supportedTypes[i2];
                        if (str.equalsIgnoreCase(str2)) {
                            CodecCapabilities capabilitiesForType = a2.getCapabilitiesForType(str);
                            boolean a3 = bVar.mo12034a(str2, capabilitiesForType);
                            if ((!b || aVar.f8547b != a3) && (b || aVar.f8547b)) {
                                if (!b && a3) {
                                    arrayList.add(new ach(name + ".secure", capabilitiesForType));
                                    break loop0;
                                }
                            } else {
                                arrayList.add(new ach(name, capabilitiesForType));
                            }
                        }
                    }
                    continue;
                }
                i++;
            }
            return arrayList;
        } catch (Exception e) {
            if (akw.f2108a > 23 || arrayList.isEmpty()) {
                Log.e("MediaCodecUtil", "Failed to query codec " + name + " (" + str + ")");
                throw e;
            }
            Log.e("MediaCodecUtil", "Skipping codec " + name + " (failed to query capabilities)");
        } catch (Exception e2) {
            throw new DecoderQueryException(e2);
        }
    }

    /* renamed from: a */
    private static boolean m11143a(MediaCodecInfo mediaCodecInfo, String str, boolean z) {
        if (mediaCodecInfo.isEncoder()) {
            return false;
        }
        if (!z && str.endsWith(".secure")) {
            return false;
        }
        if (akw.f2108a < 21 && ("CIPAACDecoder".equals(str) || "CIPMP3Decoder".equals(str) || "CIPVorbisDecoder".equals(str) || "AACDecoder".equals(str) || "MP3Decoder".equals(str))) {
            return false;
        }
        if (akw.f2108a < 18 && "OMX.SEC.MP3.Decoder".equals(str)) {
            return false;
        }
        if (akw.f2108a < 18 && "OMX.MTK.AUDIO.DECODER.AAC".equals(str) && "a70".equals(akw.f2109b)) {
            return false;
        }
        if (akw.f2108a == 16 && akw.f2109b != null && "OMX.qcom.audio.decoder.mp3".equals(str) && ("dlxu".equals(akw.f2109b) || "protou".equals(akw.f2109b) || "ville".equals(akw.f2109b) || "villeplus".equals(akw.f2109b) || "villec2".equals(akw.f2109b) || akw.f2109b.startsWith("gee") || "C6602".equals(akw.f2109b) || "C6603".equals(akw.f2109b) || "C6606".equals(akw.f2109b) || "C6616".equals(akw.f2109b) || "L36h".equals(akw.f2109b) || "SO-02E".equals(akw.f2109b))) {
            return false;
        }
        if (akw.f2108a == 16 && "OMX.qcom.audio.decoder.aac".equals(str) && ("C1504".equals(akw.f2109b) || "C1505".equals(akw.f2109b) || "C1604".equals(akw.f2109b) || "C1605".equals(akw.f2109b))) {
            return false;
        }
        if (akw.f2108a > 19 || akw.f2109b == null || ((!akw.f2109b.startsWith("d2") && !akw.f2109b.startsWith("serrano") && !akw.f2109b.startsWith("jflte") && !akw.f2109b.startsWith("santos")) || !"samsung".equals(akw.f2110c) || !str.equals("OMX.SEC.vp8.dec"))) {
            return akw.f2108a > 19 || akw.f2109b == null || !akw.f2109b.startsWith("jflte") || !"OMX.qcom.video.decoder.vp8".equals(str);
        }
        return false;
    }

    @TargetApi(21)
    /* renamed from: a */
    public static boolean m11144a(String str, boolean z, int i, int i2) throws DecoderQueryException {
        akc.m2451b(akw.f2108a >= 21);
        VideoCapabilities c = m11148c(str, z);
        return c != null && c.isSizeSupported(i, i2);
    }

    @TargetApi(21)
    /* renamed from: a */
    public static boolean m11145a(String str, boolean z, int i, int i2, double d) throws DecoderQueryException {
        akc.m2451b(akw.f2108a >= 21);
        VideoCapabilities c = m11148c(str, z);
        return c != null && c.areSizeAndRateSupported(i, i2, d);
    }

    /* renamed from: b */
    public static int m11146b() throws DecoderQueryException {
        if (f8545c == -1) {
            int i = 0;
            ach a = m11141a("video/avc", false);
            if (a != null) {
                for (CodecProfileLevel codecProfileLevel : a.f412b.profileLevels) {
                    i = Math.max(m11139a(codecProfileLevel.level), i);
                }
                i = Math.max(i, 172800);
            }
            f8545c = i;
        }
        return f8545c;
    }

    /* renamed from: b */
    public static synchronized List<ach> m11147b(String str, boolean z) throws DecoderQueryException {
        List list;
        synchronized (MediaCodecUtil.class) {
            C2100a aVar = new C2100a(str, z);
            List list2 = (List) f8544b.get(aVar);
            if (list2 != null) {
                list = list2;
            } else {
                List a = m11142a(aVar, akw.f2108a >= 21 ? new C2103d(z) : new C2102c());
                if (z && a.isEmpty() && 21 <= akw.f2108a && akw.f2108a <= 23) {
                    a = m11142a(aVar, (C2101b) new C2102c());
                    if (!a.isEmpty()) {
                        Log.w("MediaCodecUtil", "MediaCodecList API didn't list secure decoder for: " + str + ". Assuming: " + ((ach) a.get(0)).f411a);
                    }
                }
                List unmodifiableList = Collections.unmodifiableList(a);
                f8544b.put(aVar, unmodifiableList);
                list = unmodifiableList;
            }
        }
        return list;
    }

    @TargetApi(21)
    /* renamed from: c */
    private static VideoCapabilities m11148c(String str, boolean z) throws DecoderQueryException {
        ach a = m11141a(str, z);
        if (a == null) {
            return null;
        }
        return a.f412b.getVideoCapabilities();
    }
}
