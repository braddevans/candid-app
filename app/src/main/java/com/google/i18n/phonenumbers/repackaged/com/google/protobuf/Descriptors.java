package com.google.i18n.phonenumbers.repackaged.com.google.protobuf;

import java.util.logging.Logger;

public final class Descriptors {

    /* renamed from: a */
    private static final Logger f9613a = Logger.getLogger(Descriptors.class.getName());

    public static class DescriptorValidationException extends Exception {
    }
}
