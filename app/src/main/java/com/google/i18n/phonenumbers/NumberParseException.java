package com.google.i18n.phonenumbers;

public class NumberParseException extends Exception {

    /* renamed from: a */
    private ErrorType f9522a;

    /* renamed from: b */
    private String f9523b;

    public enum ErrorType {
        INVALID_COUNTRY_CODE,
        NOT_A_NUMBER,
        TOO_SHORT_AFTER_IDD,
        TOO_SHORT_NSN,
        TOO_LONG
    }

    public NumberParseException(ErrorType errorType, String str) {
        super(str);
        this.f9523b = str;
        this.f9522a = errorType;
    }

    /* renamed from: a */
    public ErrorType mo12913a() {
        return this.f9522a;
    }

    public String toString() {
        String valueOf = String.valueOf(String.valueOf(this.f9522a));
        String valueOf2 = String.valueOf(String.valueOf(this.f9523b));
        return new StringBuilder(valueOf.length() + 14 + valueOf2.length()).append("Error type: ").append(valueOf).append(". ").append(valueOf2).toString();
    }
}
