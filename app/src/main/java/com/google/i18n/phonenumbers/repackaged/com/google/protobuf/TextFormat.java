package com.google.i18n.phonenumbers.repackaged.com.google.protobuf;

import java.io.IOException;
import java.util.logging.Logger;

public final class TextFormat {

    /* renamed from: a */
    private static final Logger f9614a = Logger.getLogger(TextFormat.class.getName());

    /* renamed from: b */
    private static final C2221a f9615b = new C2221a(null);

    /* renamed from: c */
    private static final C2221a f9616c = new C2221a(null).m12253a(true);

    /* renamed from: d */
    private static final C2221a f9617d = new C2221a(null).m12255b(false);

    /* renamed from: e */
    private static final Parser f9618e = Parser.m12250a().mo12957a();

    /* renamed from: com.google.i18n.phonenumbers.repackaged.com.google.protobuf.TextFormat$1 */
    static class C22191 {
    }

    public static class ParseException extends IOException {
    }

    public static class Parser {

        /* renamed from: a */
        private final boolean f9619a;

        /* renamed from: b */
        private final SingularOverwritePolicy f9620b;

        public enum SingularOverwritePolicy {
            ALLOW_SINGULAR_OVERWRITES,
            FORBID_SINGULAR_OVERWRITES
        }

        /* renamed from: com.google.i18n.phonenumbers.repackaged.com.google.protobuf.TextFormat$Parser$a */
        public static class C2220a {

            /* renamed from: a */
            private boolean f9624a = false;

            /* renamed from: b */
            private SingularOverwritePolicy f9625b = SingularOverwritePolicy.ALLOW_SINGULAR_OVERWRITES;

            /* renamed from: a */
            public Parser mo12957a() {
                return new Parser(this.f9624a, this.f9625b, null);
            }
        }

        private Parser(boolean z, SingularOverwritePolicy singularOverwritePolicy) {
            this.f9619a = z;
            this.f9620b = singularOverwritePolicy;
        }

        /* synthetic */ Parser(boolean z, SingularOverwritePolicy singularOverwritePolicy, C22191 r3) {
            this(z, singularOverwritePolicy);
        }

        /* renamed from: a */
        public static C2220a m12250a() {
            return new C2220a();
        }
    }

    /* renamed from: com.google.i18n.phonenumbers.repackaged.com.google.protobuf.TextFormat$a */
    static final class C2221a {

        /* renamed from: a */
        boolean f9626a;

        /* renamed from: b */
        boolean f9627b;

        private C2221a() {
            this.f9626a = false;
            this.f9627b = true;
        }

        /* synthetic */ C2221a(C22191 r1) {
            this();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public C2221a m12253a(boolean z) {
            this.f9626a = z;
            return this;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public C2221a m12255b(boolean z) {
            this.f9627b = z;
            return this;
        }
    }

    private TextFormat() {
    }
}
