package com.google.i18n.phonenumbers;

import java.io.Serializable;

public final class Phonenumber {

    public static class PhoneNumber implements Serializable {

        /* renamed from: a */
        private boolean f9591a;

        /* renamed from: b */
        private int f9592b = 0;

        /* renamed from: c */
        private boolean f9593c;

        /* renamed from: d */
        private long f9594d = 0;

        /* renamed from: e */
        private boolean f9595e;

        /* renamed from: f */
        private String f9596f = "";

        /* renamed from: g */
        private boolean f9597g;

        /* renamed from: h */
        private boolean f9598h = false;

        /* renamed from: i */
        private boolean f9599i;

        /* renamed from: j */
        private int f9600j = 1;

        /* renamed from: k */
        private boolean f9601k;

        /* renamed from: l */
        private String f9602l = "";

        /* renamed from: m */
        private boolean f9603m;

        /* renamed from: n */
        private CountryCodeSource f9604n = CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN;

        /* renamed from: o */
        private boolean f9605o;

        /* renamed from: p */
        private String f9606p = "";

        public enum CountryCodeSource {
            FROM_NUMBER_WITH_PLUS_SIGN,
            FROM_NUMBER_WITH_IDD,
            FROM_NUMBER_WITHOUT_PLUS_SIGN,
            FROM_DEFAULT_COUNTRY
        }

        /* renamed from: a */
        public int mo12929a() {
            return this.f9592b;
        }

        /* renamed from: a */
        public PhoneNumber mo12930a(int i) {
            this.f9591a = true;
            this.f9592b = i;
            return this;
        }

        /* renamed from: a */
        public PhoneNumber mo12931a(long j) {
            this.f9593c = true;
            this.f9594d = j;
            return this;
        }

        /* renamed from: a */
        public PhoneNumber mo12932a(CountryCodeSource countryCodeSource) {
            if (countryCodeSource == null) {
                throw new NullPointerException();
            }
            this.f9603m = true;
            this.f9604n = countryCodeSource;
            return this;
        }

        /* renamed from: a */
        public PhoneNumber mo12933a(String str) {
            if (str == null) {
                throw new NullPointerException();
            }
            this.f9595e = true;
            this.f9596f = str;
            return this;
        }

        /* renamed from: a */
        public PhoneNumber mo12934a(boolean z) {
            this.f9597g = true;
            this.f9598h = z;
            return this;
        }

        /* renamed from: a */
        public boolean mo12935a(PhoneNumber phoneNumber) {
            if (phoneNumber == null) {
                return false;
            }
            if (this != phoneNumber) {
                return this.f9592b == phoneNumber.f9592b && this.f9594d == phoneNumber.f9594d && this.f9596f.equals(phoneNumber.f9596f) && this.f9598h == phoneNumber.f9598h && this.f9600j == phoneNumber.f9600j && this.f9602l.equals(phoneNumber.f9602l) && this.f9604n == phoneNumber.f9604n && this.f9606p.equals(phoneNumber.f9606p) && mo12953n() == phoneNumber.mo12953n();
            }
            return true;
        }

        /* renamed from: b */
        public long mo12936b() {
            return this.f9594d;
        }

        /* renamed from: b */
        public PhoneNumber mo12937b(int i) {
            this.f9599i = true;
            this.f9600j = i;
            return this;
        }

        /* renamed from: b */
        public PhoneNumber mo12938b(String str) {
            if (str == null) {
                throw new NullPointerException();
            }
            this.f9601k = true;
            this.f9602l = str;
            return this;
        }

        /* renamed from: c */
        public PhoneNumber mo12939c(String str) {
            if (str == null) {
                throw new NullPointerException();
            }
            this.f9605o = true;
            this.f9606p = str;
            return this;
        }

        /* renamed from: c */
        public boolean mo12940c() {
            return this.f9595e;
        }

        /* renamed from: d */
        public String mo12941d() {
            return this.f9596f;
        }

        /* renamed from: e */
        public boolean mo12942e() {
            return this.f9597g;
        }

        public boolean equals(Object obj) {
            return (obj instanceof PhoneNumber) && mo12935a((PhoneNumber) obj);
        }

        /* renamed from: f */
        public boolean mo12944f() {
            return this.f9598h;
        }

        /* renamed from: g */
        public boolean mo12945g() {
            return this.f9599i;
        }

        /* renamed from: h */
        public int mo12946h() {
            return this.f9600j;
        }

        public int hashCode() {
            int i = 1231;
            int a = (((((((((((((((mo12929a() + 2173) * 53) + Long.valueOf(mo12936b()).hashCode()) * 53) + mo12941d().hashCode()) * 53) + (mo12944f() ? 1231 : 1237)) * 53) + mo12946h()) * 53) + mo12949j().hashCode()) * 53) + mo12951l().hashCode()) * 53) + mo12954o().hashCode()) * 53;
            if (!mo12953n()) {
                i = 1237;
            }
            return a + i;
        }

        /* renamed from: i */
        public boolean mo12948i() {
            return this.f9601k;
        }

        /* renamed from: j */
        public String mo12949j() {
            return this.f9602l;
        }

        /* renamed from: k */
        public boolean mo12950k() {
            return this.f9603m;
        }

        /* renamed from: l */
        public CountryCodeSource mo12951l() {
            return this.f9604n;
        }

        /* renamed from: m */
        public PhoneNumber mo12952m() {
            this.f9603m = false;
            this.f9604n = CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN;
            return this;
        }

        /* renamed from: n */
        public boolean mo12953n() {
            return this.f9605o;
        }

        /* renamed from: o */
        public String mo12954o() {
            return this.f9606p;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Country Code: ").append(this.f9592b);
            sb.append(" National Number: ").append(this.f9594d);
            if (mo12942e() && mo12944f()) {
                sb.append(" Leading Zero(s): true");
            }
            if (mo12945g()) {
                sb.append(" Number of leading zeros: ").append(this.f9600j);
            }
            if (mo12940c()) {
                sb.append(" Extension: ").append(this.f9596f);
            }
            if (mo12950k()) {
                sb.append(" Country Code Source: ").append(this.f9604n);
            }
            if (mo12953n()) {
                sb.append(" Preferred Domestic Carrier Code: ").append(this.f9606p);
            }
            return sb.toString();
        }
    }
}
