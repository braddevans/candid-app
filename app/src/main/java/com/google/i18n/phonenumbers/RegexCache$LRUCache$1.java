package com.google.i18n.phonenumbers;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class RegexCache$LRUCache$1 extends LinkedHashMap<K, V> {

    /* renamed from: a */
    final /* synthetic */ C1075a f9612a;

    public RegexCache$LRUCache$1(C1075a aVar, int i, float f, boolean z) {
        this.f9612a = aVar;
        super(i, f, z);
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Entry<K, V> entry) {
        return size() > this.f9612a.f4885b;
    }
}
