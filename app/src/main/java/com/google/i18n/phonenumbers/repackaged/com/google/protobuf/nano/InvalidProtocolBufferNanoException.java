package com.google.i18n.phonenumbers.repackaged.com.google.protobuf.nano;

import java.io.IOException;

public class InvalidProtocolBufferNanoException extends IOException {
    public InvalidProtocolBufferNanoException(String str) {
        super(str);
    }

    /* renamed from: a */
    public static InvalidProtocolBufferNanoException m12256a() {
        return new InvalidProtocolBufferNanoException("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    /* renamed from: b */
    public static InvalidProtocolBufferNanoException m12257b() {
        return new InvalidProtocolBufferNanoException("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    /* renamed from: c */
    public static InvalidProtocolBufferNanoException m12258c() {
        return new InvalidProtocolBufferNanoException("CodedInputStream encountered a malformed varint.");
    }

    /* renamed from: d */
    public static InvalidProtocolBufferNanoException m12259d() {
        return new InvalidProtocolBufferNanoException("Protocol message contained an invalid tag (zero).");
    }

    /* renamed from: e */
    public static InvalidProtocolBufferNanoException m12260e() {
        return new InvalidProtocolBufferNanoException("Protocol message end-group tag did not match expected tag.");
    }

    /* renamed from: f */
    public static InvalidProtocolBufferNanoException m12261f() {
        return new InvalidProtocolBufferNanoException("Protocol message tag had invalid wire type.");
    }

    /* renamed from: g */
    public static InvalidProtocolBufferNanoException m12262g() {
        return new InvalidProtocolBufferNanoException("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }
}
