package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.NumberParseException.ErrorType;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber.CountryCodeSource;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberUtil {

    /* renamed from: A */
    private static final Pattern f9530A = Pattern.compile("\\$CC");

    /* renamed from: B */
    private static final Pattern f9531B = Pattern.compile("\\(?\\$1\\)?");

    /* renamed from: C */
    private static PhoneNumberUtil f9532C = null;

    /* renamed from: a */
    static final bae f9533a = new bae() {
        /* renamed from: a */
        public InputStream mo8592a(String str) {
            return PhoneNumberUtil.class.getResourceAsStream(str);
        }
    };

    /* renamed from: b */
    static final Pattern f9534b = Pattern.compile("[+＋]+");

    /* renamed from: c */
    static final Pattern f9535c = Pattern.compile("[\\\\/] *x");

    /* renamed from: d */
    static final Pattern f9536d = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");

    /* renamed from: e */
    static final String f9537e;

    /* renamed from: f */
    static final Pattern f9538f = Pattern.compile("(\\D+)");

    /* renamed from: g */
    private static final Logger f9539g = Logger.getLogger(PhoneNumberUtil.class.getName());

    /* renamed from: h */
    private static final Map<Integer, String> f9540h;

    /* renamed from: i */
    private static final Set<Integer> f9541i;

    /* renamed from: j */
    private static final Map<Character, Character> f9542j;

    /* renamed from: k */
    private static final Map<Character, Character> f9543k;

    /* renamed from: l */
    private static final Map<Character, Character> f9544l;

    /* renamed from: m */
    private static final Map<Character, Character> f9545m;

    /* renamed from: n */
    private static final Pattern f9546n = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");

    /* renamed from: o */
    private static final String f9547o;

    /* renamed from: p */
    private static final Pattern f9548p = Pattern.compile("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]+");

    /* renamed from: q */
    private static final Pattern f9549q = Pattern.compile("(\\p{Nd})");

    /* renamed from: r */
    private static final Pattern f9550r = Pattern.compile("[+＋\\p{Nd}]");

    /* renamed from: s */
    private static final Pattern f9551s = Pattern.compile("(?:.*?[A-Za-z]){3}.*");

    /* renamed from: t */
    private static final String f9552t;

    /* renamed from: u */
    private static final String f9553u;

    /* renamed from: v */
    private static final Pattern f9554v;

    /* renamed from: w */
    private static final Pattern f9555w;

    /* renamed from: x */
    private static final Pattern f9556x = Pattern.compile("(\\$\\d)");

    /* renamed from: y */
    private static final Pattern f9557y = Pattern.compile("\\$NP");

    /* renamed from: z */
    private static final Pattern f9558z = Pattern.compile("\\$FG");

    /* renamed from: D */
    private final bag f9559D;

    /* renamed from: E */
    private final Map<Integer, List<String>> f9560E;

    /* renamed from: F */
    private final Set<String> f9561F = new HashSet(35);

    /* renamed from: G */
    private final bai f9562G = new bai(100);

    /* renamed from: H */
    private final Set<String> f9563H = new HashSet(320);

    /* renamed from: I */
    private final Set<Integer> f9564I = new HashSet();

    /* renamed from: com.google.i18n.phonenumbers.PhoneNumberUtil$2 */
    static /* synthetic */ class C22182 {

        /* renamed from: a */
        static final /* synthetic */ int[] f9565a = new int[CountryCodeSource.values().length];

        /* renamed from: c */
        static final /* synthetic */ int[] f9567c = new int[PhoneNumberType.values().length];

        static {
            try {
                f9567c[PhoneNumberType.PREMIUM_RATE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f9567c[PhoneNumberType.TOLL_FREE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f9567c[PhoneNumberType.MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f9567c[PhoneNumberType.FIXED_LINE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f9567c[PhoneNumberType.FIXED_LINE_OR_MOBILE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f9567c[PhoneNumberType.SHARED_COST.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f9567c[PhoneNumberType.VOIP.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f9567c[PhoneNumberType.PERSONAL_NUMBER.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f9567c[PhoneNumberType.PAGER.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f9567c[PhoneNumberType.UAN.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f9567c[PhoneNumberType.VOICEMAIL.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            f9566b = new int[PhoneNumberFormat.values().length];
            try {
                f9566b[PhoneNumberFormat.E164.ordinal()] = 1;
            } catch (NoSuchFieldError e12) {
            }
            try {
                f9566b[PhoneNumberFormat.INTERNATIONAL.ordinal()] = 2;
            } catch (NoSuchFieldError e13) {
            }
            try {
                f9566b[PhoneNumberFormat.RFC3966.ordinal()] = 3;
            } catch (NoSuchFieldError e14) {
            }
            try {
                f9566b[PhoneNumberFormat.NATIONAL.ordinal()] = 4;
            } catch (NoSuchFieldError e15) {
            }
            try {
                f9565a[CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN.ordinal()] = 1;
            } catch (NoSuchFieldError e16) {
            }
            try {
                f9565a[CountryCodeSource.FROM_NUMBER_WITH_IDD.ordinal()] = 2;
            } catch (NoSuchFieldError e17) {
            }
            try {
                f9565a[CountryCodeSource.FROM_NUMBER_WITHOUT_PLUS_SIGN.ordinal()] = 3;
            } catch (NoSuchFieldError e18) {
            }
            try {
                f9565a[CountryCodeSource.FROM_DEFAULT_COUNTRY.ordinal()] = 4;
            } catch (NoSuchFieldError e19) {
            }
        }
    }

    public enum PhoneNumberFormat {
        E164,
        INTERNATIONAL,
        NATIONAL,
        RFC3966
    }

    public enum PhoneNumberType {
        FIXED_LINE,
        MOBILE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        VOICEMAIL,
        UNKNOWN
    }

    public enum ValidationResult {
        IS_POSSIBLE,
        INVALID_COUNTRY_CODE,
        TOO_SHORT,
        TOO_LONG
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(Integer.valueOf(52), "1");
        hashMap.put(Integer.valueOf(54), "9");
        f9540h = Collections.unmodifiableMap(hashMap);
        HashSet hashSet = new HashSet();
        hashSet.add(Integer.valueOf(52));
        hashSet.add(Integer.valueOf(54));
        hashSet.add(Integer.valueOf(55));
        f9541i = Collections.unmodifiableSet(hashSet);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(Character.valueOf('0'), Character.valueOf('0'));
        hashMap2.put(Character.valueOf('1'), Character.valueOf('1'));
        hashMap2.put(Character.valueOf('2'), Character.valueOf('2'));
        hashMap2.put(Character.valueOf('3'), Character.valueOf('3'));
        hashMap2.put(Character.valueOf('4'), Character.valueOf('4'));
        hashMap2.put(Character.valueOf('5'), Character.valueOf('5'));
        hashMap2.put(Character.valueOf('6'), Character.valueOf('6'));
        hashMap2.put(Character.valueOf('7'), Character.valueOf('7'));
        hashMap2.put(Character.valueOf('8'), Character.valueOf('8'));
        hashMap2.put(Character.valueOf('9'), Character.valueOf('9'));
        HashMap hashMap3 = new HashMap(40);
        hashMap3.put(Character.valueOf('A'), Character.valueOf('2'));
        hashMap3.put(Character.valueOf('B'), Character.valueOf('2'));
        hashMap3.put(Character.valueOf('C'), Character.valueOf('2'));
        hashMap3.put(Character.valueOf('D'), Character.valueOf('3'));
        hashMap3.put(Character.valueOf('E'), Character.valueOf('3'));
        hashMap3.put(Character.valueOf('F'), Character.valueOf('3'));
        hashMap3.put(Character.valueOf('G'), Character.valueOf('4'));
        hashMap3.put(Character.valueOf('H'), Character.valueOf('4'));
        hashMap3.put(Character.valueOf('I'), Character.valueOf('4'));
        hashMap3.put(Character.valueOf('J'), Character.valueOf('5'));
        hashMap3.put(Character.valueOf('K'), Character.valueOf('5'));
        hashMap3.put(Character.valueOf('L'), Character.valueOf('5'));
        hashMap3.put(Character.valueOf('M'), Character.valueOf('6'));
        hashMap3.put(Character.valueOf('N'), Character.valueOf('6'));
        hashMap3.put(Character.valueOf('O'), Character.valueOf('6'));
        hashMap3.put(Character.valueOf('P'), Character.valueOf('7'));
        hashMap3.put(Character.valueOf('Q'), Character.valueOf('7'));
        hashMap3.put(Character.valueOf('R'), Character.valueOf('7'));
        hashMap3.put(Character.valueOf('S'), Character.valueOf('7'));
        hashMap3.put(Character.valueOf('T'), Character.valueOf('8'));
        hashMap3.put(Character.valueOf('U'), Character.valueOf('8'));
        hashMap3.put(Character.valueOf('V'), Character.valueOf('8'));
        hashMap3.put(Character.valueOf('W'), Character.valueOf('9'));
        hashMap3.put(Character.valueOf('X'), Character.valueOf('9'));
        hashMap3.put(Character.valueOf('Y'), Character.valueOf('9'));
        hashMap3.put(Character.valueOf('Z'), Character.valueOf('9'));
        f9543k = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap(100);
        hashMap4.putAll(f9543k);
        hashMap4.putAll(hashMap2);
        f9544l = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.putAll(hashMap2);
        hashMap5.put(Character.valueOf('+'), Character.valueOf('+'));
        hashMap5.put(Character.valueOf('*'), Character.valueOf('*'));
        f9542j = Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        for (Character charValue : f9543k.keySet()) {
            char charValue2 = charValue.charValue();
            hashMap6.put(Character.valueOf(Character.toLowerCase(charValue2)), Character.valueOf(charValue2));
            hashMap6.put(Character.valueOf(charValue2), Character.valueOf(charValue2));
        }
        hashMap6.putAll(hashMap2);
        hashMap6.put(Character.valueOf('-'), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(65293), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(8208), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(8209), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(8210), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(8211), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(8212), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(8213), Character.valueOf('-'));
        hashMap6.put(Character.valueOf(8722), Character.valueOf('-'));
        hashMap6.put(Character.valueOf('/'), Character.valueOf('/'));
        hashMap6.put(Character.valueOf(65295), Character.valueOf('/'));
        hashMap6.put(Character.valueOf(' '), Character.valueOf(' '));
        hashMap6.put(Character.valueOf(12288), Character.valueOf(' '));
        hashMap6.put(Character.valueOf(8288), Character.valueOf(' '));
        hashMap6.put(Character.valueOf('.'), Character.valueOf('.'));
        hashMap6.put(Character.valueOf(65294), Character.valueOf('.'));
        f9545m = Collections.unmodifiableMap(hashMap6);
        String valueOf = String.valueOf(Arrays.toString(f9543k.keySet().toArray()).replaceAll("[, \\[\\]]", ""));
        String valueOf2 = String.valueOf(Arrays.toString(f9543k.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
        f9547o = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        String valueOf3 = String.valueOf(String.valueOf("\\p{Nd}{2}|[+＋]*+(?:[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*]*\\p{Nd}){3,}[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*"));
        String valueOf4 = String.valueOf(String.valueOf(f9547o));
        String valueOf5 = String.valueOf(String.valueOf("\\p{Nd}"));
        f9552t = new StringBuilder(valueOf3.length() + 2 + valueOf4.length() + valueOf5.length()).append(valueOf3).append(valueOf4).append(valueOf5).append("]*").toString();
        String str = "xｘ#＃~～";
        String str2 = ",";
        String valueOf6 = String.valueOf(str);
        f9553u = m12209f(valueOf6.length() != 0 ? str2.concat(valueOf6) : new String(str2));
        f9537e = m12209f(str);
        String valueOf7 = String.valueOf(String.valueOf(f9553u));
        f9554v = Pattern.compile(new StringBuilder(valueOf7.length() + 5).append("(?:").append(valueOf7).append(")$").toString(), 66);
        String valueOf8 = String.valueOf(String.valueOf(f9552t));
        String valueOf9 = String.valueOf(String.valueOf(f9553u));
        f9555w = Pattern.compile(new StringBuilder(valueOf8.length() + 5 + valueOf9.length()).append(valueOf8).append("(?:").append(valueOf9).append(")?").toString(), 66);
    }

    PhoneNumberUtil(bag bag, Map<Integer, List<String>> map) {
        this.f9559D = bag;
        this.f9560E = map;
        for (Entry entry : map.entrySet()) {
            List list = (List) entry.getValue();
            if (list.size() != 1 || !"001".equals(list.get(0))) {
                this.f9563H.addAll(list);
            } else {
                this.f9564I.add(entry.getKey());
            }
        }
        if (this.f9563H.remove("001")) {
            f9539g.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.f9561F.addAll((Collection) map.get(Integer.valueOf(1)));
    }

    /* renamed from: a */
    private C1077b m12184a(int i, String str) {
        return "001".equals(str) ? mo12918a(i) : mo12928e(str);
    }

    /* renamed from: a */
    private ValidationResult m12185a(Pattern pattern, String str) {
        Matcher matcher = pattern.matcher(str);
        return matcher.matches() ? ValidationResult.IS_POSSIBLE : matcher.lookingAt() ? ValidationResult.TOO_LONG : ValidationResult.TOO_SHORT;
    }

    /* renamed from: a */
    public static synchronized PhoneNumberUtil m12186a() {
        PhoneNumberUtil phoneNumberUtil;
        synchronized (PhoneNumberUtil.class) {
            if (f9532C == null) {
                m12196a(m12187a(f9533a));
            }
            phoneNumberUtil = f9532C;
        }
        return phoneNumberUtil;
    }

    /* renamed from: a */
    public static PhoneNumberUtil m12187a(bae bae) {
        if (bae != null) {
            return m12188a((bag) new bah(bae));
        }
        throw new IllegalArgumentException("metadataLoader could not be null.");
    }

    /* renamed from: a */
    public static PhoneNumberUtil m12188a(bag bag) {
        if (bag != null) {
            return new PhoneNumberUtil(bag, bad.m7404a());
        }
        throw new IllegalArgumentException("metadataSource could not be null.");
    }

    /* renamed from: a */
    static String m12189a(String str) {
        Matcher matcher = f9550r.matcher(str);
        if (!matcher.find()) {
            return "";
        }
        String substring = str.substring(matcher.start());
        Matcher matcher2 = f9536d.matcher(substring);
        if (matcher2.find()) {
            substring = substring.substring(0, matcher2.start());
            Logger logger = f9539g;
            Level level = Level.FINER;
            String str2 = "Stripped trailing characters: ";
            String valueOf = String.valueOf(substring);
            logger.log(level, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
        Matcher matcher3 = f9535c.matcher(substring);
        return matcher3.find() ? substring.substring(0, matcher3.start()) : substring;
    }

    /* renamed from: a */
    private String m12190a(String str, C1076a aVar, PhoneNumberFormat phoneNumberFormat, String str2) {
        String replaceAll;
        String str3 = aVar.f4888b;
        Matcher matcher = this.f9562G.mo8596a(aVar.f4887a).matcher(str);
        String str4 = "";
        if (phoneNumberFormat != PhoneNumberFormat.NATIONAL || str2 == null || str2.length() <= 0 || aVar.f4892f.length() <= 0) {
            String str5 = aVar.f4890d;
            replaceAll = (phoneNumberFormat != PhoneNumberFormat.NATIONAL || str5 == null || str5.length() <= 0) ? matcher.replaceAll(str3) : matcher.replaceAll(f9556x.matcher(str3).replaceFirst(str5));
        } else {
            replaceAll = matcher.replaceAll(f9556x.matcher(str3).replaceFirst(f9530A.matcher(aVar.f4892f).replaceFirst(str2)));
        }
        if (phoneNumberFormat != PhoneNumberFormat.RFC3966) {
            return replaceAll;
        }
        Matcher matcher2 = f9548p.matcher(replaceAll);
        if (matcher2.lookingAt()) {
            replaceAll = matcher2.replaceFirst("");
        }
        return matcher2.reset(replaceAll).replaceAll("-");
    }

    /* renamed from: a */
    private String m12191a(String str, C1077b bVar, PhoneNumberFormat phoneNumberFormat) {
        return m12192a(str, bVar, phoneNumberFormat, (String) null);
    }

    /* renamed from: a */
    private String m12192a(String str, C1077b bVar, PhoneNumberFormat phoneNumberFormat, String str2) {
        C1076a a = mo12917a((bVar.f4894A.length == 0 || phoneNumberFormat == PhoneNumberFormat.NATIONAL) ? bVar.f4924z : bVar.f4894A, str);
        return a == null ? str : m12190a(str, a, phoneNumberFormat, str2);
    }

    /* renamed from: a */
    private static String m12193a(String str, Map<Character, Character> map, boolean z) {
        StringBuilder sb = new StringBuilder(str.length());
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            Character ch = (Character) map.get(Character.valueOf(Character.toUpperCase(charAt)));
            if (ch != null) {
                sb.append(ch);
            } else if (!z) {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    static StringBuilder m12194a(String str, boolean z) {
        char[] charArray;
        StringBuilder sb = new StringBuilder(str.length());
        for (char c : str.toCharArray()) {
            int digit = Character.digit(c, 10);
            if (digit != -1) {
                sb.append(digit);
            } else if (z) {
                sb.append(c);
            }
        }
        return sb;
    }

    /* renamed from: a */
    private void m12195a(int i, PhoneNumberFormat phoneNumberFormat, StringBuilder sb) {
        switch (phoneNumberFormat) {
            case E164:
                sb.insert(0, i).insert(0, '+');
                return;
            case INTERNATIONAL:
                sb.insert(0, " ").insert(0, i).insert(0, '+');
                return;
            case RFC3966:
                sb.insert(0, "-").insert(0, i).insert(0, '+').insert(0, "tel:");
                return;
            default:
                return;
        }
    }

    /* renamed from: a */
    static synchronized void m12196a(PhoneNumberUtil phoneNumberUtil) {
        synchronized (PhoneNumberUtil.class) {
            f9532C = phoneNumberUtil;
        }
    }

    /* renamed from: a */
    private void m12197a(PhoneNumber phoneNumber, C1077b bVar, PhoneNumberFormat phoneNumberFormat, StringBuilder sb) {
        if (phoneNumber.mo12940c() && phoneNumber.mo12941d().length() > 0) {
            if (phoneNumberFormat == PhoneNumberFormat.RFC3966) {
                sb.append(";ext=").append(phoneNumber.mo12941d());
            } else if (!bVar.f4920v.equals("")) {
                sb.append(bVar.f4920v).append(phoneNumber.mo12941d());
            } else {
                sb.append(" ext. ").append(phoneNumber.mo12941d());
            }
        }
    }

    /* renamed from: a */
    static void m12198a(String str, PhoneNumber phoneNumber) {
        if (str.length() > 1 && str.charAt(0) == '0') {
            phoneNumber.mo12934a(true);
            int i = 1;
            while (i < str.length() - 1 && str.charAt(i) == '0') {
                i++;
            }
            if (i != 1) {
                phoneNumber.mo12937b(i);
            }
        }
    }

    /* renamed from: a */
    private void m12199a(String str, String str2, boolean z, boolean z2, PhoneNumber phoneNumber) throws NumberParseException {
        int a;
        if (str == null) {
            throw new NumberParseException(ErrorType.NOT_A_NUMBER, "The phone number supplied was null.");
        } else if (str.length() > 250) {
            throw new NumberParseException(ErrorType.TOO_LONG, "The string supplied was too long to parse.");
        } else {
            StringBuilder sb = new StringBuilder();
            m12200a(str, sb);
            if (!m12204b(sb.toString())) {
                throw new NumberParseException(ErrorType.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
            }
            if (z2) {
                if (!m12205b(sb.toString(), str2)) {
                    throw new NumberParseException(ErrorType.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
                }
            }
            if (z) {
                phoneNumber.mo12938b(str);
            }
            String b = mo12927b(sb);
            if (b.length() > 0) {
                phoneNumber.mo12933a(b);
            }
            C1077b e = mo12928e(str2);
            StringBuilder sb2 = new StringBuilder();
            try {
                a = mo12915a(sb.toString(), e, sb2, z, phoneNumber);
            } catch (NumberParseException e2) {
                Matcher matcher = f9534b.matcher(sb.toString());
                if (e2.mo12913a() != ErrorType.INVALID_COUNTRY_CODE || !matcher.lookingAt()) {
                    throw new NumberParseException(e2.mo12913a(), e2.getMessage());
                }
                a = mo12915a(sb.substring(matcher.end()), e, sb2, z, phoneNumber);
                if (a == 0) {
                    throw new NumberParseException(ErrorType.INVALID_COUNTRY_CODE, "Could not interpret numbers after plus-sign.");
                }
            }
            if (a != 0) {
                String b2 = mo12926b(a);
                if (!b2.equals(str2)) {
                    e = m12184a(a, b2);
                }
            } else {
                m12201a(sb);
                sb2.append(sb);
                if (str2 != null) {
                    phoneNumber.mo12930a(e.f4916r);
                } else if (z) {
                    phoneNumber.mo12952m();
                }
            }
            if (sb2.length() < 2) {
                throw new NumberParseException(ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
            }
            if (e != null) {
                StringBuilder sb3 = new StringBuilder();
                StringBuilder sb4 = new StringBuilder(sb2);
                mo12925a(sb4, e, sb3);
                if (!m12202a(e, sb4.toString())) {
                    sb2 = sb4;
                    if (z) {
                        phoneNumber.mo12939c(sb3.toString());
                    }
                }
            }
            int length = sb2.length();
            if (length < 2) {
                throw new NumberParseException(ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
            } else if (length > 17) {
                throw new NumberParseException(ErrorType.TOO_LONG, "The string supplied is too long to be a phone number.");
            } else {
                m12198a(sb2.toString(), phoneNumber);
                phoneNumber.mo12931a(Long.parseLong(sb2.toString()));
            }
        }
    }

    /* renamed from: a */
    private void m12200a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(";phone-context=");
        if (indexOf > 0) {
            int length = indexOf + ";phone-context=".length();
            if (str.charAt(length) == '+') {
                int indexOf2 = str.indexOf(59, length);
                if (indexOf2 > 0) {
                    sb.append(str.substring(length, indexOf2));
                } else {
                    sb.append(str.substring(length));
                }
            }
            int indexOf3 = str.indexOf("tel:");
            sb.append(str.substring(indexOf3 >= 0 ? indexOf3 + "tel:".length() : 0, indexOf));
        } else {
            sb.append(m12189a(str));
        }
        int indexOf4 = sb.indexOf(";isub=");
        if (indexOf4 > 0) {
            sb.delete(indexOf4, sb.length());
        }
    }

    /* renamed from: a */
    static void m12201a(StringBuilder sb) {
        sb.replace(0, sb.length(), m12206c(sb.toString()));
    }

    /* renamed from: a */
    private boolean m12202a(C1077b bVar, String str) {
        return m12185a(this.f9562G.mo8596a(bVar.f4899a.f4927b), str) == ValidationResult.TOO_SHORT;
    }

    /* renamed from: a */
    private boolean m12203a(Pattern pattern, StringBuilder sb) {
        Matcher matcher = pattern.matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        int end = matcher.end();
        Matcher matcher2 = f9549q.matcher(sb.substring(end));
        if (matcher2.find() && m12208d(matcher2.group(1)).equals("0")) {
            return false;
        }
        sb.delete(0, end);
        return true;
    }

    /* renamed from: b */
    static boolean m12204b(String str) {
        if (str.length() < 2) {
            return false;
        }
        return f9555w.matcher(str).matches();
    }

    /* renamed from: b */
    private boolean m12205b(String str, String str2) {
        return m12210g(str2) || !(str == null || str.length() == 0 || !f9534b.matcher(str).lookingAt());
    }

    /* renamed from: c */
    static String m12206c(String str) {
        return f9551s.matcher(str).matches() ? m12193a(str, f9544l, true) : m12208d(str);
    }

    /* renamed from: c */
    private boolean m12207c(int i) {
        return this.f9560E.containsKey(Integer.valueOf(i));
    }

    /* renamed from: d */
    public static String m12208d(String str) {
        return m12194a(str, false).toString();
    }

    /* renamed from: f */
    private static String m12209f(String str) {
        String valueOf = String.valueOf(String.valueOf(";ext=(\\p{Nd}{1,7})|[  \\t,]*(?:e?xt(?:ensi(?:ó?|ó))?n?|ｅ?ｘｔｎ?|["));
        String valueOf2 = String.valueOf(String.valueOf(str));
        String valueOf3 = String.valueOf(String.valueOf("(\\p{Nd}{1,7})"));
        String valueOf4 = String.valueOf(String.valueOf("\\p{Nd}"));
        return new StringBuilder(valueOf.length() + 48 + valueOf2.length() + valueOf3.length() + valueOf4.length()).append(valueOf).append(valueOf2).append("]|int|anexo|ｉｎｔ)").append("[:\\.．]?[  \\t,-]*").append(valueOf3).append("#?|").append("[- ]+(").append(valueOf4).append("{1,5})#").toString();
    }

    /* renamed from: g */
    private boolean m12210g(String str) {
        return str != null && this.f9563H.contains(str);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public int mo12915a(String str, C1077b bVar, StringBuilder sb, boolean z, PhoneNumber phoneNumber) throws NumberParseException {
        if (str.length() == 0) {
            return 0;
        }
        StringBuilder sb2 = new StringBuilder(str);
        String str2 = "NonMatch";
        if (bVar != null) {
            str2 = bVar.f4917s;
        }
        CountryCodeSource a = mo12919a(sb2, str2);
        if (z) {
            phoneNumber.mo12932a(a);
        }
        if (a == CountryCodeSource.FROM_DEFAULT_COUNTRY) {
            if (bVar != null) {
                int i = bVar.f4916r;
                String valueOf = String.valueOf(i);
                String sb3 = sb2.toString();
                if (sb3.startsWith(valueOf)) {
                    StringBuilder sb4 = new StringBuilder(sb3.substring(valueOf.length()));
                    C1079d dVar = bVar.f4899a;
                    Pattern a2 = this.f9562G.mo8596a(dVar.f4926a);
                    mo12925a(sb4, bVar, (StringBuilder) null);
                    Pattern a3 = this.f9562G.mo8596a(dVar.f4927b);
                    if ((!a2.matcher(sb2).matches() && a2.matcher(sb4).matches()) || m12185a(a3, sb2.toString()) == ValidationResult.TOO_LONG) {
                        sb.append(sb4);
                        if (z) {
                            phoneNumber.mo12932a(CountryCodeSource.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                        }
                        phoneNumber.mo12930a(i);
                        return i;
                    }
                }
            }
            phoneNumber.mo12930a(0);
            return 0;
        } else if (sb2.length() <= 2) {
            throw new NumberParseException(ErrorType.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        } else {
            int a4 = mo12916a(sb2, sb);
            if (a4 != 0) {
                phoneNumber.mo12930a(a4);
                return a4;
            }
            throw new NumberParseException(ErrorType.INVALID_COUNTRY_CODE, "Country calling code supplied was not recognised.");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public int mo12916a(StringBuilder sb, StringBuilder sb2) {
        if (sb.length() == 0 || sb.charAt(0) == '0') {
            return 0;
        }
        int length = sb.length();
        int i = 1;
        while (i <= 3 && i <= length) {
            int parseInt = Integer.parseInt(sb.substring(0, i));
            if (this.f9560E.containsKey(Integer.valueOf(parseInt))) {
                sb2.append(sb.substring(i));
                return parseInt;
            }
            i++;
        }
        return 0;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C1076a mo12917a(C1076a[] aVarArr, String str) {
        C1076a[] aVarArr2;
        for (C1076a aVar : aVarArr) {
            int length = aVar.f4889c.length;
            if ((length == 0 || this.f9562G.mo8596a(aVar.f4889c[length - 1]).matcher(str).lookingAt()) && this.f9562G.mo8596a(aVar.f4887a).matcher(str).matches()) {
                return aVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C1077b mo12918a(int i) {
        if (!this.f9560E.containsKey(Integer.valueOf(i))) {
            return null;
        }
        return this.f9559D.mo8593a(i);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public CountryCodeSource mo12919a(StringBuilder sb, String str) {
        if (sb.length() == 0) {
            return CountryCodeSource.FROM_DEFAULT_COUNTRY;
        }
        Matcher matcher = f9534b.matcher(sb);
        if (matcher.lookingAt()) {
            sb.delete(0, matcher.end());
            m12201a(sb);
            return CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern a = this.f9562G.mo8596a(str);
        m12201a(sb);
        return m12203a(a, sb) ? CountryCodeSource.FROM_NUMBER_WITH_IDD : CountryCodeSource.FROM_DEFAULT_COUNTRY;
    }

    /* renamed from: a */
    public PhoneNumber mo12920a(String str, String str2) throws NumberParseException {
        PhoneNumber phoneNumber = new PhoneNumber();
        mo12924a(str, str2, phoneNumber);
        return phoneNumber;
    }

    /* renamed from: a */
    public String mo12921a(PhoneNumber phoneNumber) {
        StringBuilder sb = new StringBuilder();
        if (phoneNumber.mo12944f()) {
            char[] cArr = new char[phoneNumber.mo12946h()];
            Arrays.fill(cArr, '0');
            sb.append(new String(cArr));
        }
        sb.append(phoneNumber.mo12936b());
        return sb.toString();
    }

    /* renamed from: a */
    public String mo12922a(PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat) {
        if (phoneNumber.mo12936b() == 0 && phoneNumber.mo12948i()) {
            String j = phoneNumber.mo12949j();
            if (j.length() > 0) {
                return j;
            }
        }
        StringBuilder sb = new StringBuilder(20);
        mo12923a(phoneNumber, phoneNumberFormat, sb);
        return sb.toString();
    }

    /* renamed from: a */
    public void mo12923a(PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat, StringBuilder sb) {
        sb.setLength(0);
        int a = phoneNumber.mo12929a();
        String a2 = mo12921a(phoneNumber);
        if (phoneNumberFormat == PhoneNumberFormat.E164) {
            sb.append(a2);
            m12195a(a, PhoneNumberFormat.E164, sb);
        } else if (!m12207c(a)) {
            sb.append(a2);
        } else {
            C1077b a3 = m12184a(a, mo12926b(a));
            sb.append(m12191a(a2, a3, phoneNumberFormat));
            m12197a(phoneNumber, a3, phoneNumberFormat, sb);
            m12195a(a, phoneNumberFormat, sb);
        }
    }

    /* renamed from: a */
    public void mo12924a(String str, String str2, PhoneNumber phoneNumber) throws NumberParseException {
        m12199a(str, str2, false, true, phoneNumber);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo12925a(StringBuilder sb, C1077b bVar, StringBuilder sb2) {
        int length = sb.length();
        String str = bVar.f4921w;
        if (length == 0 || str.length() == 0) {
            return false;
        }
        Matcher matcher = this.f9562G.mo8596a(str).matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        Pattern a = this.f9562G.mo8596a(bVar.f4899a.f4926a);
        boolean matches = a.matcher(sb).matches();
        int groupCount = matcher.groupCount();
        String str2 = bVar.f4922x;
        if (str2 != null && str2.length() != 0 && matcher.group(groupCount) != null) {
            StringBuilder sb3 = new StringBuilder(sb);
            sb3.replace(0, length, matcher.replaceFirst(str2));
            if (matches && !a.matcher(sb3.toString()).matches()) {
                return false;
            }
            if (sb2 != null && groupCount > 1) {
                sb2.append(matcher.group(1));
            }
            sb.replace(0, sb.length(), sb3.toString());
            return true;
        } else if (matches && !a.matcher(sb.substring(matcher.end())).matches()) {
            return false;
        } else {
            if (!(sb2 == null || groupCount <= 0 || matcher.group(groupCount) == null)) {
                sb2.append(matcher.group(1));
            }
            sb.delete(0, matcher.end());
            return true;
        }
    }

    /* renamed from: b */
    public String mo12926b(int i) {
        List list = (List) this.f9560E.get(Integer.valueOf(i));
        return list == null ? "ZZ" : (String) list.get(0);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public String mo12927b(StringBuilder sb) {
        Matcher matcher = f9554v.matcher(sb);
        if (matcher.find() && m12204b(sb.substring(0, matcher.start()))) {
            int groupCount = matcher.groupCount();
            for (int i = 1; i <= groupCount; i++) {
                if (matcher.group(i) != null) {
                    String group = matcher.group(i);
                    sb.delete(matcher.start(), sb.length());
                    return group;
                }
            }
        }
        return "";
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public C1077b mo12928e(String str) {
        if (!m12210g(str)) {
            return null;
        }
        return this.f9559D.mo8594a(str);
    }
}
