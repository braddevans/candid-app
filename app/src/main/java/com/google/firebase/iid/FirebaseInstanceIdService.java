package com.google.firebase.iid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

public class FirebaseInstanceIdService extends zzb {

    /* renamed from: c */
    private static BroadcastReceiver f9454c;

    /* renamed from: d */
    private static final Object f9455d = new Object();

    /* renamed from: e */
    private static boolean f9456e = false;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public boolean f9457f = false;

    /* renamed from: a */
    private ayf m12115a(String str) {
        if (str == null) {
            return ayf.m6890a(this, null);
        }
        Bundle bundle = new Bundle();
        bundle.putString("subtype", str);
        return ayf.m6890a(this, bundle);
    }

    /* renamed from: a */
    static void m12116a(Context context) {
        if (ayh.m6900a(context) != null) {
            synchronized (f9455d) {
                if (!f9456e) {
                    context.sendBroadcast(m12124c(0));
                    f9456e = true;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        if (r0.mo8344b(p000.ayf.f4612e) != false) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        if (r3.mo12831g().mo8322a() == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        m12116a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r3.mo12829e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 == null) goto L_0x0022;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void m12117a(android.content.Context r2, com.google.firebase.iid.FirebaseInstanceId r3) {
        /*
            java.lang.Object r1 = f9455d
            monitor-enter(r1)
            boolean r0 = f9456e     // Catch:{ all -> 0x0026 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
        L_0x0008:
            return
        L_0x0009:
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            ayi$a r0 = r3.mo12829e()
            if (r0 == 0) goto L_0x0022
            java.lang.String r1 = p000.ayf.f4612e
            boolean r0 = r0.mo8344b(r1)
            if (r0 != 0) goto L_0x0022
            ayg r0 = r3.mo12831g()
            java.lang.String r0 = r0.mo8322a()
            if (r0 == 0) goto L_0x0008
        L_0x0022:
            m12116a(r2)
            goto L_0x0008
        L_0x0026:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.FirebaseInstanceIdService.m12117a(android.content.Context, com.google.firebase.iid.FirebaseInstanceId):void");
    }

    /* renamed from: a */
    private void m12118a(Intent intent, String str) {
        boolean c = m12125c((Context) this);
        final int b = m12122b(intent, c);
        Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(str).length() + 47).append("background sync failed: ").append(str).append(", retry in ").append(b).append("s").toString());
        synchronized (f9455d) {
            m12126d(b);
            f9456e = true;
        }
        if (!c) {
            if (this.f9457f) {
                Log.d("FirebaseInstanceId", "device not connected. Connectivity change received registered");
            }
            if (f9454c == null) {
                f9454c = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        if (FirebaseInstanceIdService.m12125c(context)) {
                            if (FirebaseInstanceIdService.this.f9457f) {
                                Log.d("FirebaseInstanceId", "connectivity changed. starting background sync.");
                            }
                            FirebaseInstanceIdService.this.getApplicationContext().unregisterReceiver(this);
                            context.sendBroadcast(FirebaseInstanceIdService.m12124c(b));
                        }
                    }
                };
            }
            getApplicationContext().registerReceiver(f9454c, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x008e A[Catch:{ IOException -> 0x00a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ab A[SYNTHETIC, Splitter:B:51:0x00ab] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0071 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m12119a(android.content.Intent r9, boolean r10) {
        /*
            r8 = this;
            r2 = 1
            r1 = 0
            java.lang.Object r3 = f9455d
            monitor-enter(r3)
            r0 = 0
            f9456e = r0     // Catch:{ all -> 0x0010 }
            monitor-exit(r3)     // Catch:{ all -> 0x0010 }
            java.lang.String r0 = p000.ayh.m6900a(r8)
            if (r0 != 0) goto L_0x0013
        L_0x000f:
            return
        L_0x0010:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0010 }
            throw r0
        L_0x0013:
            com.google.firebase.iid.FirebaseInstanceId r0 = com.google.firebase.iid.FirebaseInstanceId.m12091a()
            ayi$a r3 = r0.mo12829e()
            if (r3 != 0) goto L_0x004d
            java.lang.String r1 = r0.mo12830f()     // Catch:{ IOException -> 0x0035, SecurityException -> 0x0044 }
            if (r1 == 0) goto L_0x003e
            boolean r1 = r8.f9457f     // Catch:{ IOException -> 0x0035, SecurityException -> 0x0044 }
            if (r1 == 0) goto L_0x002e
            java.lang.String r1 = "FirebaseInstanceId"
            java.lang.String r2 = "get master token succeeded"
            android.util.Log.d(r1, r2)     // Catch:{ IOException -> 0x0035, SecurityException -> 0x0044 }
        L_0x002e:
            m12117a(r8, r0)     // Catch:{ IOException -> 0x0035, SecurityException -> 0x0044 }
            r8.mo12834a()     // Catch:{ IOException -> 0x0035, SecurityException -> 0x0044 }
            goto L_0x000f
        L_0x0035:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            r8.m12118a(r9, r0)
            goto L_0x000f
        L_0x003e:
            java.lang.String r0 = "returned token is null"
            r8.m12118a(r9, r0)     // Catch:{ IOException -> 0x0035, SecurityException -> 0x0044 }
            goto L_0x000f
        L_0x0044:
            r0 = move-exception
            java.lang.String r1 = "FirebaseInstanceId"
            java.lang.String r2 = "Unable to get master token"
            android.util.Log.e(r1, r2, r0)
            goto L_0x000f
        L_0x004d:
            ayg r4 = r0.mo12831g()
            java.lang.String r0 = r4.mo8322a()
            r3 = r0
        L_0x0056:
            if (r3 == 0) goto L_0x00be
            java.lang.String r0 = "!"
            java.lang.String[] r0 = r3.split(r0)
            int r5 = r0.length
            r6 = 2
            if (r5 != r6) goto L_0x0071
            r5 = r0[r1]
            r6 = r0[r2]
            r0 = -1
            int r7 = r5.hashCode()     // Catch:{ IOException -> 0x00a1 }
            switch(r7) {
                case 83: goto L_0x007a;
                case 84: goto L_0x006e;
                case 85: goto L_0x0084;
                default: goto L_0x006e;
            }
        L_0x006e:
            switch(r0) {
                case 0: goto L_0x008e;
                case 1: goto L_0x00ab;
                default: goto L_0x0071;
            }
        L_0x0071:
            r4.mo8323a(r3)
            java.lang.String r0 = r4.mo8322a()
            r3 = r0
            goto L_0x0056
        L_0x007a:
            java.lang.String r7 = "S"
            boolean r5 = r5.equals(r7)     // Catch:{ IOException -> 0x00a1 }
            if (r5 == 0) goto L_0x006e
            r0 = r1
            goto L_0x006e
        L_0x0084:
            java.lang.String r7 = "U"
            boolean r5 = r5.equals(r7)     // Catch:{ IOException -> 0x00a1 }
            if (r5 == 0) goto L_0x006e
            r0 = r2
            goto L_0x006e
        L_0x008e:
            com.google.firebase.iid.FirebaseInstanceId r0 = com.google.firebase.iid.FirebaseInstanceId.m12091a()     // Catch:{ IOException -> 0x00a1 }
            r0.mo12824a(r6)     // Catch:{ IOException -> 0x00a1 }
            boolean r0 = r8.f9457f     // Catch:{ IOException -> 0x00a1 }
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = "FirebaseInstanceId"
            java.lang.String r5 = "subscribe operation succeeded"
            android.util.Log.d(r0, r5)     // Catch:{ IOException -> 0x00a1 }
            goto L_0x0071
        L_0x00a1:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            r8.m12118a(r9, r0)
            goto L_0x000f
        L_0x00ab:
            com.google.firebase.iid.FirebaseInstanceId r0 = com.google.firebase.iid.FirebaseInstanceId.m12091a()     // Catch:{ IOException -> 0x00a1 }
            r0.mo12826b(r6)     // Catch:{ IOException -> 0x00a1 }
            boolean r0 = r8.f9457f     // Catch:{ IOException -> 0x00a1 }
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = "FirebaseInstanceId"
            java.lang.String r5 = "unsubscribe operation succeeded"
            android.util.Log.d(r0, r5)     // Catch:{ IOException -> 0x00a1 }
            goto L_0x0071
        L_0x00be:
            java.lang.String r0 = "FirebaseInstanceId"
            java.lang.String r1 = "topic sync succeeded"
            android.util.Log.d(r0, r1)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.FirebaseInstanceIdService.m12119a(android.content.Intent, boolean):void");
    }

    /* renamed from: a */
    private void m12120a(ayh ayh, Bundle bundle) {
        String a = ayh.m6900a((Context) this);
        if (a == null) {
            Log.w("FirebaseInstanceId", "Unable to respond to ping due to missing target package");
            return;
        }
        Intent intent = new Intent("com.google.android.gcm.intent.SEND");
        intent.setPackage(a);
        intent.putExtras(bundle);
        ayh.mo8326a(intent);
        intent.putExtra("google.to", "google.com/iid");
        intent.putExtra("google.message_id", ayh.m6908b());
        sendOrderedBroadcast(intent, "com.google.android.gtalkservice.permission.GTALK_SERVICE");
    }

    /* renamed from: b */
    private int m12122b(Intent intent, boolean z) {
        int intExtra = intent == null ? 10 : intent.getIntExtra("next_retry_delay_in_seconds", 0);
        if (intExtra < 10 && !z) {
            return 30;
        }
        if (intExtra < 10) {
            return 10;
        }
        if (intExtra > 28800) {
            return 28800;
        }
        return intExtra;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static Intent m12124c(int i) {
        Context a = aya.m6872d().mo8299a();
        Intent intent = new Intent("ACTION_TOKEN_REFRESH_RETRY");
        intent.putExtra("next_retry_delay_in_seconds", i);
        return FirebaseInstanceIdInternalReceiver.m12110b(a, intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static boolean m12125c(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* renamed from: d */
    private void m12126d(int i) {
        ((AlarmManager) getSystemService("alarm")).set(3, SystemClock.elapsedRealtime() + ((long) (i * 1000)), PendingIntent.getBroadcast(this, 0, m12124c(i * 2), 268435456));
    }

    /* renamed from: e */
    private String m12127e(Intent intent) {
        String stringExtra = intent.getStringExtra("subtype");
        return stringExtra == null ? "" : stringExtra;
    }

    /* renamed from: a */
    public void mo12834a() {
    }

    /* renamed from: a */
    public boolean mo12835a(Intent intent) {
        this.f9457f = Log.isLoggable("FirebaseInstanceId", 3);
        if (intent.getStringExtra("error") == null && intent.getStringExtra("registration_id") == null) {
            return false;
        }
        String e = m12127e(intent);
        if (this.f9457f) {
            String str = "FirebaseInstanceId";
            String str2 = "Register result in service ";
            String valueOf = String.valueOf(e);
            Log.d(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
        m12115a(e).mo8321d().mo8332d(intent);
        return true;
    }

    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo12836b(android.content.Intent r5) {
        /*
            r4 = this;
            r1 = 0
            java.lang.String r0 = r5.getAction()
            if (r0 != 0) goto L_0x0009
            java.lang.String r0 = ""
        L_0x0009:
            r2 = -1
            int r3 = r0.hashCode()
            switch(r3) {
                case -1737547627: goto L_0x0019;
                default: goto L_0x0011;
            }
        L_0x0011:
            r0 = r2
        L_0x0012:
            switch(r0) {
                case 0: goto L_0x0023;
                default: goto L_0x0015;
            }
        L_0x0015:
            r4.mo12838d(r5)
        L_0x0018:
            return
        L_0x0019:
            java.lang.String r3 = "ACTION_TOKEN_REFRESH_RETRY"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x0011
            r0 = r1
            goto L_0x0012
        L_0x0023:
            r4.m12119a(r5, r1)
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.FirebaseInstanceIdService.mo12836b(android.content.Intent):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Intent mo12837c(Intent intent) {
        return FirebaseInstanceIdInternalReceiver.m12109a();
    }

    /* renamed from: d */
    public void mo12838d(Intent intent) {
        String e = m12127e(intent);
        ayf a = m12115a(e);
        String stringExtra = intent.getStringExtra("CMD");
        if (this.f9457f) {
            String valueOf = String.valueOf(intent.getExtras());
            Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(e).length() + 18 + String.valueOf(stringExtra).length() + String.valueOf(valueOf).length()).append("Service command ").append(e).append(" ").append(stringExtra).append(" ").append(valueOf).toString());
        }
        if (intent.getStringExtra("unregistered") != null) {
            ayi c = a.mo8319c();
            if (e == null) {
                e = "";
            }
            c.mo8342c(e);
            a.mo8321d().mo8332d(intent);
        } else if ("gcm.googleapis.com/refresh".equals(intent.getStringExtra("from"))) {
            a.mo8319c().mo8342c(e);
            m12119a(intent, false);
        } else if ("RST".equals(stringExtra)) {
            a.mo8318b();
            m12119a(intent, true);
        } else if ("RST_FULL".equals(stringExtra)) {
            if (!a.mo8319c().mo8340b()) {
                a.mo8318b();
                a.mo8319c().mo8341c();
                m12119a(intent, true);
            }
        } else if ("SYNC".equals(stringExtra)) {
            a.mo8319c().mo8342c(e);
            m12119a(intent, false);
        } else if ("PING".equals(stringExtra)) {
            m12120a(a.mo8321d(), intent.getExtras());
        }
    }
}
