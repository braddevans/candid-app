package com.google.firebase.iid;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.p001v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import com.google.android.gms.iid.MessengerCompat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class zzb extends Service {

    /* renamed from: a */
    MessengerCompat f9460a = new MessengerCompat((Handler) new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            int a = MessengerCompat.m11603a(message);
            ayh.m6900a((Context) zzb.this);
            zzb.this.getPackageManager();
            if (a == ayh.f4622c || a == ayh.f4621b) {
                zzb.this.mo12836b((Intent) message.obj);
                return;
            }
            int i = ayh.f4621b;
            Log.w("FirebaseInstanceId", "Message from unexpected caller " + a + " mine=" + i + " appid=" + ayh.f4622c);
        }
    });

    /* renamed from: b */
    final ExecutorService f9461b = Executors.newSingleThreadExecutor();

    /* renamed from: c */
    private final Object f9462c = new Object();

    /* renamed from: d */
    private int f9463d;

    /* renamed from: e */
    private int f9464e = 0;

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void mo12838d(Intent intent) {
        if (intent != null) {
            WakefulBroadcastReceiver.m3705a(intent);
        }
        synchronized (this.f9462c) {
            this.f9464e--;
            if (this.f9464e == 0) {
                mo12840b(this.f9463d);
            }
        }
    }

    /* renamed from: a */
    public boolean mo12835a(Intent intent) {
        return false;
    }

    /* renamed from: b */
    public abstract void mo12836b(Intent intent);

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public boolean mo12840b(int i) {
        return stopSelfResult(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public abstract Intent mo12837c(Intent intent);

    public final IBinder onBind(Intent intent) {
        if (intent == null || !"com.google.firebase.INSTANCE_ID_EVENT".equals(intent.getAction())) {
            return null;
        }
        return this.f9460a.mo12307a();
    }

    public final int onStartCommand(final Intent intent, int i, int i2) {
        synchronized (this.f9462c) {
            this.f9463d = i2;
            this.f9464e++;
        }
        final Intent c = mo12837c(intent);
        if (c == null) {
            mo12838d(intent);
            return 2;
        } else if (mo12835a(c)) {
            mo12838d(intent);
            return 2;
        } else {
            this.f9461b.execute(new Runnable() {
                public void run() {
                    zzb.this.mo12836b(c);
                    zzb.this.mo12838d(intent);
                }
            });
            return 3;
        }
    }
}
