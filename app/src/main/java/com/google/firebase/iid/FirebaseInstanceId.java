package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.annotation.Keep;
import android.util.Base64;
import android.util.Log;
import java.io.IOException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class FirebaseInstanceId {

    /* renamed from: a */
    private static Map<String, FirebaseInstanceId> f9447a = new ArrayMap();

    /* renamed from: b */
    private static ayg f9448b;

    /* renamed from: c */
    private final aya f9449c;

    /* renamed from: d */
    private final ayf f9450d;

    /* renamed from: e */
    private final String f9451e = mo12825b();

    private FirebaseInstanceId(aya aya, ayf ayf) {
        this.f9449c = aya;
        this.f9450d = ayf;
        if (this.f9451e == null) {
            throw new IllegalStateException("IID failing to initialize, FirebaseApp is missing project ID");
        }
        FirebaseInstanceIdService.m12117a(this.f9449c.mo8299a(), this);
    }

    /* renamed from: a */
    public static FirebaseInstanceId m12091a() {
        return getInstance(aya.m6872d());
    }

    /* renamed from: a */
    public static String m12092a(Context context) {
        return m12091a().f9449c.mo8301c().mo8309a();
    }

    /* renamed from: a */
    public static String m12093a(KeyPair keyPair) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(keyPair.getPublic().getEncoded());
            digest[0] = (byte) (((digest[0] & 15) + 112) & 255);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException e) {
            Log.w("FirebaseInstanceId", "Unexpected error, device missing required alghorithms");
            return null;
        }
    }

    /* renamed from: a */
    public static String m12094a(byte[] bArr) {
        return Base64.encodeToString(bArr, 11);
    }

    /* renamed from: a */
    public static void m12095a(Context context, ayi ayi) {
        ayi.mo8341c();
        Intent intent = new Intent();
        intent.putExtra("CMD", "RST");
        context.sendBroadcast(FirebaseInstanceIdInternalReceiver.m12110b(context, intent));
    }

    /* renamed from: b */
    public static int m12096b(Context context) {
        boolean z = false;
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 38).append("Never happens: can't find own package ").append(valueOf).toString());
            return z;
        }
    }

    /* renamed from: c */
    public static String m12097c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 38).append("Never happens: can't find own package ").append(valueOf).toString());
            return null;
        }
    }

    /* renamed from: d */
    public static void m12098d(Context context) {
        Intent intent = new Intent();
        intent.setPackage(context.getPackageName());
        intent.putExtra("CMD", "SYNC");
        context.sendBroadcast(FirebaseInstanceIdInternalReceiver.m12110b(context, intent));
    }

    @Keep
    public static synchronized FirebaseInstanceId getInstance(aya aya) {
        FirebaseInstanceId firebaseInstanceId;
        synchronized (FirebaseInstanceId.class) {
            firebaseInstanceId = (FirebaseInstanceId) f9447a.get(aya.mo8301c().mo8309a());
            if (firebaseInstanceId == null) {
                ayf a = ayf.m6890a(aya.mo8299a(), null);
                if (f9448b == null) {
                    f9448b = new ayg(a.mo8319c());
                }
                firebaseInstanceId = new FirebaseInstanceId(aya, a);
                f9447a.put(aya.mo8301c().mo8309a(), firebaseInstanceId);
            }
        }
        return firebaseInstanceId;
    }

    /* renamed from: a */
    public String mo12823a(String str, String str2) throws IOException {
        return this.f9450d.mo8317b(str, str2, null);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo12824a(String str) throws IOException {
        if (mo12828d() == null) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String str2 = "gcm.topic";
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString(str2, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        ayf ayf = this.f9450d;
        String d = mo12828d();
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        ayf.mo8317b(d, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public String mo12825b() {
        String b = this.f9449c.mo8301c().mo8310b();
        if (b != null) {
            return b;
        }
        String a = this.f9449c.mo8301c().mo8309a();
        if (!a.startsWith("1:")) {
            return a;
        }
        String[] split = a.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo12826b(String str) throws IOException {
        if (mo12828d() == null) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String str2 = "gcm.topic";
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString(str2, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        ayf ayf = this.f9450d;
        String d = mo12828d();
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        ayf.mo8316a(d, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    /* renamed from: c */
    public String mo12827c() {
        return m12093a(this.f9450d.mo8315a());
    }

    /* renamed from: d */
    public String mo12828d() {
        C0986a e = mo12829e();
        if (e == null || e.mo8344b(ayf.f4612e)) {
            FirebaseInstanceIdService.m12116a(this.f9449c.mo8299a());
        }
        if (e != null) {
            return e.f4639a;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public C0986a mo12829e() {
        return this.f9450d.mo8319c().mo8335a("", this.f9451e, "*");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public String mo12830f() throws IOException {
        return mo12823a(this.f9451e, "*");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public ayg mo12831g() {
        return f9448b;
    }
}
