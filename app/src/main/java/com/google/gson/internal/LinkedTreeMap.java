package com.google.gson.internal;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;

public final class LinkedTreeMap<K, V> extends AbstractMap<K, V> implements Serializable {

    /* renamed from: f */
    static final /* synthetic */ boolean f9486f = (!LinkedTreeMap.class.desiredAssertionStatus());

    /* renamed from: g */
    private static final Comparator<Comparable> f9487g = new Comparator<Comparable>() {
        /* renamed from: a */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    };

    /* renamed from: a */
    Comparator<? super K> f9488a;

    /* renamed from: b */
    C2216d<K, V> f9489b;

    /* renamed from: c */
    int f9490c;

    /* renamed from: d */
    int f9491d;

    /* renamed from: e */
    final C2216d<K, V> f9492e;

    /* renamed from: h */
    private C2211a f9493h;

    /* renamed from: i */
    private C2213b f9494i;

    /* renamed from: com.google.gson.internal.LinkedTreeMap$a */
    class C2211a extends AbstractSet<Entry<K, V>> {
        C2211a() {
        }

        public void clear() {
            LinkedTreeMap.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Entry) && LinkedTreeMap.this.mo12876a((Entry) obj) != null;
        }

        public Iterator<Entry<K, V>> iterator() {
            return new C2215c<Entry<K, V>>() {
                {
                    LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
                }

                /* renamed from: a */
                public Entry<K, V> next() {
                    return mo12902b();
                }
            };
        }

        public boolean remove(Object obj) {
            if (!(obj instanceof Entry)) {
                return false;
            }
            C2216d a = LinkedTreeMap.this.mo12876a((Entry) obj);
            if (a == null) {
                return false;
            }
            LinkedTreeMap.this.mo12877a(a, true);
            return true;
        }

        public int size() {
            return LinkedTreeMap.this.f9490c;
        }
    }

    /* renamed from: com.google.gson.internal.LinkedTreeMap$b */
    final class C2213b extends AbstractSet<K> {
        C2213b() {
        }

        public void clear() {
            LinkedTreeMap.this.clear();
        }

        public boolean contains(Object obj) {
            return LinkedTreeMap.this.containsKey(obj);
        }

        public Iterator<K> iterator() {
            return new C2215c<K>() {
                {
                    LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
                }

                public K next() {
                    return mo12902b().f9508f;
                }
            };
        }

        public boolean remove(Object obj) {
            return LinkedTreeMap.this.mo12878b(obj) != null;
        }

        public int size() {
            return LinkedTreeMap.this.f9490c;
        }
    }

    /* renamed from: com.google.gson.internal.LinkedTreeMap$c */
    abstract class C2215c<T> implements Iterator<T> {

        /* renamed from: b */
        C2216d<K, V> f9499b = LinkedTreeMap.this.f9492e.f9506d;

        /* renamed from: c */
        C2216d<K, V> f9500c = null;

        /* renamed from: d */
        int f9501d = LinkedTreeMap.this.f9491d;

        C2215c() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public final C2216d<K, V> mo12902b() {
            C2216d<K, V> dVar = this.f9499b;
            if (dVar == LinkedTreeMap.this.f9492e) {
                throw new NoSuchElementException();
            } else if (LinkedTreeMap.this.f9491d != this.f9501d) {
                throw new ConcurrentModificationException();
            } else {
                this.f9499b = dVar.f9506d;
                this.f9500c = dVar;
                return dVar;
            }
        }

        public final boolean hasNext() {
            return this.f9499b != LinkedTreeMap.this.f9492e;
        }

        public final void remove() {
            if (this.f9500c == null) {
                throw new IllegalStateException();
            }
            LinkedTreeMap.this.mo12877a(this.f9500c, true);
            this.f9500c = null;
            this.f9501d = LinkedTreeMap.this.f9491d;
        }
    }

    /* renamed from: com.google.gson.internal.LinkedTreeMap$d */
    static final class C2216d<K, V> implements Entry<K, V> {

        /* renamed from: a */
        C2216d<K, V> f9503a;

        /* renamed from: b */
        C2216d<K, V> f9504b;

        /* renamed from: c */
        C2216d<K, V> f9505c;

        /* renamed from: d */
        C2216d<K, V> f9506d;

        /* renamed from: e */
        C2216d<K, V> f9507e;

        /* renamed from: f */
        final K f9508f;

        /* renamed from: g */
        V f9509g;

        /* renamed from: h */
        int f9510h;

        C2216d() {
            this.f9508f = null;
            this.f9507e = this;
            this.f9506d = this;
        }

        C2216d(C2216d<K, V> dVar, K k, C2216d<K, V> dVar2, C2216d<K, V> dVar3) {
            this.f9503a = dVar;
            this.f9508f = k;
            this.f9510h = 1;
            this.f9506d = dVar2;
            this.f9507e = dVar3;
            dVar3.f9506d = this;
            dVar2.f9507e = this;
        }

        /* renamed from: a */
        public C2216d<K, V> mo12905a() {
            C2216d dVar = this;
            C2216d<K, V> dVar2 = dVar.f9504b;
            while (dVar2 != null) {
                dVar = dVar2;
                dVar2 = dVar.f9504b;
            }
            return dVar;
        }

        /* renamed from: b */
        public C2216d<K, V> mo12906b() {
            C2216d dVar = this;
            C2216d<K, V> dVar2 = dVar.f9505c;
            while (dVar2 != null) {
                dVar = dVar2;
                dVar2 = dVar.f9505c;
            }
            return dVar;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Entry)) {
                return false;
            }
            Entry entry = (Entry) obj;
            if (this.f9508f == null) {
                if (entry.getKey() != null) {
                    return false;
                }
            } else if (!this.f9508f.equals(entry.getKey())) {
                return false;
            }
            if (this.f9509g == null) {
                if (entry.getValue() != null) {
                    return false;
                }
            } else if (!this.f9509g.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        public K getKey() {
            return this.f9508f;
        }

        public V getValue() {
            return this.f9509g;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.f9508f == null ? 0 : this.f9508f.hashCode();
            if (this.f9509g != null) {
                i = this.f9509g.hashCode();
            }
            return hashCode ^ i;
        }

        public V setValue(V v) {
            V v2 = this.f9509g;
            this.f9509g = v;
            return v2;
        }

        public String toString() {
            return this.f9508f + "=" + this.f9509g;
        }
    }

    public LinkedTreeMap() {
        this(f9487g);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Comparator<? super K>, code=java.util.Comparator, for r2v0, types: [java.util.Comparator<? super K>, java.util.Comparator] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public LinkedTreeMap(java.util.Comparator r2) {
        /*
            r1 = this;
            r0 = 0
            r1.<init>()
            r1.f9490c = r0
            r1.f9491d = r0
            com.google.gson.internal.LinkedTreeMap$d r0 = new com.google.gson.internal.LinkedTreeMap$d
            r0.<init>()
            r1.f9492e = r0
            if (r2 == 0) goto L_0x0014
        L_0x0011:
            r1.f9488a = r2
            return
        L_0x0014:
            java.util.Comparator<java.lang.Comparable> r2 = f9487g
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.internal.LinkedTreeMap.<init>(java.util.Comparator):void");
    }

    /* renamed from: a */
    private void m12168a(C2216d<K, V> dVar) {
        int i = 0;
        C2216d<K, V> dVar2 = dVar.f9504b;
        C2216d<K, V> dVar3 = dVar.f9505c;
        C2216d<K, V> dVar4 = dVar3.f9504b;
        C2216d<K, V> dVar5 = dVar3.f9505c;
        dVar.f9505c = dVar4;
        if (dVar4 != null) {
            dVar4.f9503a = dVar;
        }
        m12169a(dVar, dVar3);
        dVar3.f9504b = dVar;
        dVar.f9503a = dVar3;
        dVar.f9510h = Math.max(dVar2 != null ? dVar2.f9510h : 0, dVar4 != null ? dVar4.f9510h : 0) + 1;
        int i2 = dVar.f9510h;
        if (dVar5 != null) {
            i = dVar5.f9510h;
        }
        dVar3.f9510h = Math.max(i2, i) + 1;
    }

    /* renamed from: a */
    private void m12169a(C2216d<K, V> dVar, C2216d<K, V> dVar2) {
        C2216d<K, V> dVar3 = dVar.f9503a;
        dVar.f9503a = null;
        if (dVar2 != null) {
            dVar2.f9503a = dVar3;
        }
        if (dVar3 == null) {
            this.f9489b = dVar2;
        } else if (dVar3.f9504b == dVar) {
            dVar3.f9504b = dVar2;
        } else if (f9486f || dVar3.f9505c == dVar) {
            dVar3.f9505c = dVar2;
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    private boolean m12170a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: b */
    private void m12171b(C2216d<K, V> dVar) {
        int i = 0;
        C2216d<K, V> dVar2 = dVar.f9504b;
        C2216d<K, V> dVar3 = dVar.f9505c;
        C2216d<K, V> dVar4 = dVar2.f9504b;
        C2216d<K, V> dVar5 = dVar2.f9505c;
        dVar.f9504b = dVar5;
        if (dVar5 != null) {
            dVar5.f9503a = dVar;
        }
        m12169a(dVar, dVar2);
        dVar2.f9505c = dVar;
        dVar.f9503a = dVar2;
        dVar.f9510h = Math.max(dVar3 != null ? dVar3.f9510h : 0, dVar5 != null ? dVar5.f9510h : 0) + 1;
        int i2 = dVar.f9510h;
        if (dVar4 != null) {
            i = dVar4.f9510h;
        }
        dVar2.f9510h = Math.max(i2, i) + 1;
    }

    /* renamed from: b */
    private void m12172b(C2216d<K, V> dVar, boolean z) {
        for (C2216d<K, V> dVar2 = dVar; dVar2 != null; dVar2 = dVar2.f9503a) {
            C2216d<K, V> dVar3 = dVar2.f9504b;
            C2216d<K, V> dVar4 = dVar2.f9505c;
            int i = dVar3 != null ? dVar3.f9510h : 0;
            int i2 = dVar4 != null ? dVar4.f9510h : 0;
            int i3 = i - i2;
            if (i3 == -2) {
                C2216d<K, V> dVar5 = dVar4.f9504b;
                C2216d<K, V> dVar6 = dVar4.f9505c;
                int i4 = (dVar5 != null ? dVar5.f9510h : 0) - (dVar6 != null ? dVar6.f9510h : 0);
                if (i4 == -1 || (i4 == 0 && !z)) {
                    m12168a(dVar2);
                } else if (f9486f || i4 == 1) {
                    m12171b(dVar4);
                    m12168a(dVar2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 2) {
                C2216d<K, V> dVar7 = dVar3.f9504b;
                C2216d<K, V> dVar8 = dVar3.f9505c;
                int i5 = (dVar7 != null ? dVar7.f9510h : 0) - (dVar8 != null ? dVar8.f9510h : 0);
                if (i5 == 1 || (i5 == 0 && !z)) {
                    m12171b(dVar2);
                } else if (f9486f || i5 == -1) {
                    m12168a(dVar3);
                    m12171b(dVar2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 0) {
                dVar2.f9510h = i + 1;
                if (z) {
                    return;
                }
            } else if (f9486f || i3 == -1 || i3 == 1) {
                dVar2.f9510h = Math.max(i, i2) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
        }
    }

    private Object writeReplace() throws ObjectStreamException {
        return new LinkedHashMap(this);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C2216d<K, V> mo12874a(Object obj) {
        C2216d<K, V> dVar = null;
        if (obj == null) {
            return dVar;
        }
        try {
            return mo12875a((K) obj, false);
        } catch (ClassCastException e) {
            return dVar;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C2216d<K, V> mo12875a(K k, boolean z) {
        C2216d<K, V> dVar;
        Comparator<? super K> comparator = this.f9488a;
        C2216d<K, V> dVar2 = this.f9489b;
        int i = 0;
        if (dVar2 != null) {
            Comparable comparable = comparator == f9487g ? (Comparable) k : null;
            while (true) {
                i = comparable != null ? comparable.compareTo(dVar2.f9508f) : comparator.compare(k, dVar2.f9508f);
                if (i == 0) {
                    return dVar2;
                }
                C2216d<K, V> dVar3 = i < 0 ? dVar2.f9504b : dVar2.f9505c;
                if (dVar3 == null) {
                    break;
                }
                dVar2 = dVar3;
            }
        }
        if (!z) {
            return null;
        }
        C2216d<K, V> dVar4 = this.f9492e;
        if (dVar2 != null) {
            dVar = new C2216d<>(dVar2, k, dVar4, dVar4.f9507e);
            if (i < 0) {
                dVar2.f9504b = dVar;
            } else {
                dVar2.f9505c = dVar;
            }
            m12172b(dVar2, true);
        } else if (comparator != f9487g || (k instanceof Comparable)) {
            dVar = new C2216d<>(dVar2, k, dVar4, dVar4.f9507e);
            this.f9489b = dVar;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.f9490c++;
        this.f9491d++;
        return dVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C2216d<K, V> mo12876a(Entry<?, ?> entry) {
        C2216d<K, V> a = mo12874a(entry.getKey());
        if (a != null && m12170a((Object) a.f9509g, entry.getValue())) {
            return a;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo12877a(C2216d<K, V> dVar, boolean z) {
        if (z) {
            dVar.f9507e.f9506d = dVar.f9506d;
            dVar.f9506d.f9507e = dVar.f9507e;
        }
        C2216d<K, V> dVar2 = dVar.f9504b;
        C2216d<K, V> dVar3 = dVar.f9505c;
        C2216d<K, V> dVar4 = dVar.f9503a;
        if (dVar2 == null || dVar3 == null) {
            if (dVar2 != null) {
                m12169a(dVar, dVar2);
                dVar.f9504b = null;
            } else if (dVar3 != null) {
                m12169a(dVar, dVar3);
                dVar.f9505c = null;
            } else {
                m12169a(dVar, null);
            }
            m12172b(dVar4, false);
            this.f9490c--;
            this.f9491d++;
            return;
        }
        C2216d<K, V> a = dVar2.f9510h > dVar3.f9510h ? dVar2.mo12906b() : dVar3.mo12905a();
        mo12877a(a, false);
        int i = 0;
        C2216d<K, V> dVar5 = dVar.f9504b;
        if (dVar5 != null) {
            i = dVar5.f9510h;
            a.f9504b = dVar5;
            dVar5.f9503a = a;
            dVar.f9504b = null;
        }
        int i2 = 0;
        C2216d<K, V> dVar6 = dVar.f9505c;
        if (dVar6 != null) {
            i2 = dVar6.f9510h;
            a.f9505c = dVar6;
            dVar6.f9503a = a;
            dVar.f9505c = null;
        }
        a.f9510h = Math.max(i, i2) + 1;
        m12169a(dVar, a);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public C2216d<K, V> mo12878b(Object obj) {
        C2216d<K, V> a = mo12874a(obj);
        if (a != null) {
            mo12877a(a, true);
        }
        return a;
    }

    public void clear() {
        this.f9489b = null;
        this.f9490c = 0;
        this.f9491d++;
        C2216d<K, V> dVar = this.f9492e;
        dVar.f9507e = dVar;
        dVar.f9506d = dVar;
    }

    public boolean containsKey(Object obj) {
        return mo12874a(obj) != null;
    }

    public Set<Entry<K, V>> entrySet() {
        C2211a aVar = this.f9493h;
        if (aVar != null) {
            return aVar;
        }
        C2211a aVar2 = new C2211a<>();
        this.f9493h = aVar2;
        return aVar2;
    }

    public V get(Object obj) {
        C2216d a = mo12874a(obj);
        if (a != null) {
            return a.f9509g;
        }
        return null;
    }

    public Set<K> keySet() {
        C2213b bVar = this.f9494i;
        if (bVar != null) {
            return bVar;
        }
        C2213b bVar2 = new C2213b<>();
        this.f9494i = bVar2;
        return bVar2;
    }

    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        C2216d a = mo12875a(k, true);
        V v2 = a.f9509g;
        a.f9509g = v;
        return v2;
    }

    public V remove(Object obj) {
        C2216d b = mo12878b(obj);
        if (b != null) {
            return b.f9509g;
        }
        return null;
    }

    public int size() {
        return this.f9490c;
    }
}
