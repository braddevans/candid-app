package com.google.gson.internal;

import java.io.ObjectStreamException;
import java.math.BigDecimal;

public final class LazilyParsedNumber extends Number {

    /* renamed from: a */
    private final String f9485a;

    public LazilyParsedNumber(String str) {
        this.f9485a = str;
    }

    private Object writeReplace() throws ObjectStreamException {
        return new BigDecimal(this.f9485a);
    }

    public double doubleValue() {
        return Double.parseDouble(this.f9485a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LazilyParsedNumber)) {
            return false;
        }
        LazilyParsedNumber lazilyParsedNumber = (LazilyParsedNumber) obj;
        return this.f9485a == lazilyParsedNumber.f9485a || this.f9485a.equals(lazilyParsedNumber.f9485a);
    }

    public float floatValue() {
        return Float.parseFloat(this.f9485a);
    }

    public int hashCode() {
        return this.f9485a.hashCode();
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.f9485a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.f9485a);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.f9485a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f9485a);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.f9485a).longValue();
        }
    }

    public String toString() {
        return this.f9485a;
    }
}
