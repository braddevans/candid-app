package com.google.gson.internal;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

/* renamed from: com.google.gson.internal.$Gson$Types reason: invalid class name */
public final class C$Gson$Types {

    /* renamed from: a */
    static final Type[] f9478a = new Type[0];

    /* renamed from: com.google.gson.internal.$Gson$Types$GenericArrayTypeImpl */
    /* compiled from: $Gson$Types */
    static final class GenericArrayTypeImpl implements Serializable, GenericArrayType {

        /* renamed from: a */
        private final Type f9479a;

        public GenericArrayTypeImpl(Type type) {
            this.f9479a = C$Gson$Types.m12163d(type);
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && C$Gson$Types.m12158a((Type) this, (Type) (GenericArrayType) obj);
        }

        public Type getGenericComponentType() {
            return this.f9479a;
        }

        public int hashCode() {
            return this.f9479a.hashCode();
        }

        public String toString() {
            return C$Gson$Types.m12165f(this.f9479a) + "[]";
        }
    }

    /* renamed from: com.google.gson.internal.$Gson$Types$ParameterizedTypeImpl */
    /* compiled from: $Gson$Types */
    static final class ParameterizedTypeImpl implements Serializable, ParameterizedType {

        /* renamed from: a */
        private final Type f9480a;

        /* renamed from: b */
        private final Type f9481b;

        /* renamed from: c */
        private final Type[] f9482c;

        public ParameterizedTypeImpl(Type type, Type type2, Type... typeArr) {
            boolean z = false;
            if (type2 instanceof Class) {
                Class cls = (Class) type2;
                boolean z2 = Modifier.isStatic(cls.getModifiers()) || cls.getEnclosingClass() == null;
                if (type != null || z2) {
                    z = true;
                }
                azd.m7062a(z);
            }
            this.f9480a = type == null ? null : C$Gson$Types.m12163d(type);
            this.f9481b = C$Gson$Types.m12163d(type2);
            this.f9482c = (Type[]) typeArr.clone();
            for (int i = 0; i < this.f9482c.length; i++) {
                azd.m7061a(this.f9482c[i]);
                C$Gson$Types.m12167h(this.f9482c[i]);
                this.f9482c[i] = C$Gson$Types.m12163d(this.f9482c[i]);
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && C$Gson$Types.m12158a((Type) this, (Type) (ParameterizedType) obj);
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.f9482c.clone();
        }

        public Type getOwnerType() {
            return this.f9480a;
        }

        public Type getRawType() {
            return this.f9481b;
        }

        public int hashCode() {
            return (Arrays.hashCode(this.f9482c) ^ this.f9481b.hashCode()) ^ C$Gson$Types.m12148a((Object) this.f9480a);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder((this.f9482c.length + 1) * 30);
            sb.append(C$Gson$Types.m12165f(this.f9481b));
            if (this.f9482c.length == 0) {
                return sb.toString();
            }
            sb.append("<").append(C$Gson$Types.m12165f(this.f9482c[0]));
            for (int i = 1; i < this.f9482c.length; i++) {
                sb.append(", ").append(C$Gson$Types.m12165f(this.f9482c[i]));
            }
            return sb.append(">").toString();
        }
    }

    /* renamed from: com.google.gson.internal.$Gson$Types$WildcardTypeImpl */
    /* compiled from: $Gson$Types */
    static final class WildcardTypeImpl implements Serializable, WildcardType {

        /* renamed from: a */
        private final Type f9483a;

        /* renamed from: b */
        private final Type f9484b;

        public WildcardTypeImpl(Type[] typeArr, Type[] typeArr2) {
            boolean z = true;
            azd.m7062a(typeArr2.length <= 1);
            azd.m7062a(typeArr.length == 1);
            if (typeArr2.length == 1) {
                azd.m7061a(typeArr2[0]);
                C$Gson$Types.m12167h(typeArr2[0]);
                if (typeArr[0] != Object.class) {
                    z = false;
                }
                azd.m7062a(z);
                this.f9484b = C$Gson$Types.m12163d(typeArr2[0]);
                this.f9483a = Object.class;
                return;
            }
            azd.m7061a(typeArr[0]);
            C$Gson$Types.m12167h(typeArr[0]);
            this.f9484b = null;
            this.f9483a = C$Gson$Types.m12163d(typeArr[0]);
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && C$Gson$Types.m12158a((Type) this, (Type) (WildcardType) obj);
        }

        public Type[] getLowerBounds() {
            if (this.f9484b == null) {
                return C$Gson$Types.f9478a;
            }
            return new Type[]{this.f9484b};
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.f9483a};
        }

        public int hashCode() {
            return (this.f9484b != null ? this.f9484b.hashCode() + 31 : 1) ^ (this.f9483a.hashCode() + 31);
        }

        public String toString() {
            return this.f9484b != null ? "? super " + C$Gson$Types.m12165f(this.f9484b) : this.f9483a == Object.class ? "?" : "? extends " + C$Gson$Types.m12165f(this.f9483a);
        }
    }

    /* renamed from: a */
    static int m12148a(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    /* renamed from: a */
    private static int m12149a(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    /* renamed from: a */
    private static Class<?> m12150a(TypeVariable<?> typeVariable) {
        GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    /* renamed from: a */
    public static GenericArrayType m12151a(Type type) {
        return new GenericArrayTypeImpl(type);
    }

    /* renamed from: a */
    public static ParameterizedType m12152a(Type type, Type type2, Type... typeArr) {
        return new ParameterizedTypeImpl(type, type2, typeArr);
    }

    /* renamed from: a */
    public static Type m12153a(Type type, Class<?> cls) {
        Type b = m12159b(type, cls, Collection.class);
        if (b instanceof WildcardType) {
            b = ((WildcardType) b).getUpperBounds()[0];
        }
        return b instanceof ParameterizedType ? ((ParameterizedType) b).getActualTypeArguments()[0] : Object.class;
    }

    /* renamed from: a */
    static Type m12154a(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return m12154a(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<?> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return m12154a(cls.getGenericSuperclass(), superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    /* JADX WARNING: type inference failed for: r11v2, types: [java.lang.reflect.GenericArrayType] */
    /* JADX WARNING: type inference failed for: r11v3 */
    /* JADX WARNING: type inference failed for: r10v11, types: [java.lang.reflect.Type] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.reflect.Type m12155a(java.lang.reflect.Type r21, java.lang.Class<?> r22, java.lang.reflect.Type r23) {
        /*
        L_0x0000:
            r0 = r23
            boolean r0 = r0 instanceof java.lang.reflect.TypeVariable
            r19 = r0
            if (r19 == 0) goto L_0x001f
            r17 = r23
            java.lang.reflect.TypeVariable r17 = (java.lang.reflect.TypeVariable) r17
            r0 = r21
            r1 = r22
            r2 = r17
            java.lang.reflect.Type r23 = m12156a(r0, r1, r2)
            r0 = r23
            r1 = r17
            if (r0 != r1) goto L_0x0000
            r10 = r23
        L_0x001e:
            return r10
        L_0x001f:
            r0 = r23
            boolean r0 = r0 instanceof java.lang.Class
            r19 = r0
            if (r19 == 0) goto L_0x004a
            r19 = r23
            java.lang.Class r19 = (java.lang.Class) r19
            boolean r19 = r19.isArray()
            if (r19 == 0) goto L_0x004a
            r11 = r23
            java.lang.Class r11 = (java.lang.Class) r11
            java.lang.Class r5 = r11.getComponentType()
            r0 = r21
            r1 = r22
            java.lang.reflect.Type r8 = m12155a(r0, r1, r5)
            if (r5 != r8) goto L_0x0045
        L_0x0043:
            r10 = r11
            goto L_0x001e
        L_0x0045:
            java.lang.reflect.GenericArrayType r11 = m12151a(r8)
            goto L_0x0043
        L_0x004a:
            r0 = r23
            boolean r0 = r0 instanceof java.lang.reflect.GenericArrayType
            r19 = r0
            if (r19 == 0) goto L_0x0069
            r10 = r23
            java.lang.reflect.GenericArrayType r10 = (java.lang.reflect.GenericArrayType) r10
            java.lang.reflect.Type r5 = r10.getGenericComponentType()
            r0 = r21
            r1 = r22
            java.lang.reflect.Type r8 = m12155a(r0, r1, r5)
            if (r5 == r8) goto L_0x001e
            java.lang.reflect.GenericArrayType r10 = m12151a(r8)
            goto L_0x001e
        L_0x0069:
            r0 = r23
            boolean r0 = r0 instanceof java.lang.reflect.ParameterizedType
            r19 = r0
            if (r19 == 0) goto L_0x00bf
            r10 = r23
            java.lang.reflect.ParameterizedType r10 = (java.lang.reflect.ParameterizedType) r10
            java.lang.reflect.Type r14 = r10.getOwnerType()
            r0 = r21
            r1 = r22
            java.lang.reflect.Type r9 = m12155a(r0, r1, r14)
            if (r9 == r14) goto L_0x00af
            r4 = 1
        L_0x0084:
            java.lang.reflect.Type[] r3 = r10.getActualTypeArguments()
            r16 = 0
            int r6 = r3.length
        L_0x008b:
            r0 = r16
            if (r0 >= r6) goto L_0x00b1
            r19 = r3[r16]
            r0 = r21
            r1 = r22
            r2 = r19
            java.lang.reflect.Type r15 = m12155a(r0, r1, r2)
            r19 = r3[r16]
            r0 = r19
            if (r15 == r0) goto L_0x00ac
            if (r4 != 0) goto L_0x00aa
            java.lang.Object r3 = r3.clone()
            java.lang.reflect.Type[] r3 = (java.lang.reflect.Type[]) r3
            r4 = 1
        L_0x00aa:
            r3[r16] = r15
        L_0x00ac:
            int r16 = r16 + 1
            goto L_0x008b
        L_0x00af:
            r4 = 0
            goto L_0x0084
        L_0x00b1:
            if (r4 == 0) goto L_0x001e
            java.lang.reflect.Type r19 = r10.getRawType()
            r0 = r19
            java.lang.reflect.ParameterizedType r10 = m12152a(r9, r0, r3)
            goto L_0x001e
        L_0x00bf:
            r0 = r23
            boolean r0 = r0 instanceof java.lang.reflect.WildcardType
            r19 = r0
            if (r19 == 0) goto L_0x0123
            r10 = r23
            java.lang.reflect.WildcardType r10 = (java.lang.reflect.WildcardType) r10
            java.lang.reflect.Type[] r12 = r10.getLowerBounds()
            java.lang.reflect.Type[] r13 = r10.getUpperBounds()
            int r0 = r12.length
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x00fa
            r19 = 0
            r19 = r12[r19]
            r0 = r21
            r1 = r22
            r2 = r19
            java.lang.reflect.Type r7 = m12155a(r0, r1, r2)
            r19 = 0
            r19 = r12[r19]
            r0 = r19
            if (r7 == r0) goto L_0x001e
            java.lang.reflect.WildcardType r10 = m12162c(r7)
            goto L_0x001e
        L_0x00fa:
            int r0 = r13.length
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x001e
            r19 = 0
            r19 = r13[r19]
            r0 = r21
            r1 = r22
            r2 = r19
            java.lang.reflect.Type r18 = m12155a(r0, r1, r2)
            r19 = 0
            r19 = r13[r19]
            r0 = r18
            r1 = r19
            if (r0 == r1) goto L_0x001e
            java.lang.reflect.WildcardType r10 = m12160b(r18)
            goto L_0x001e
        L_0x0123:
            r10 = r23
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.internal.C$Gson$Types.m12155a(java.lang.reflect.Type, java.lang.Class, java.lang.reflect.Type):java.lang.reflect.Type");
    }

    /* renamed from: a */
    static Type m12156a(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class a = m12150a(typeVariable);
        if (a == null) {
            return typeVariable;
        }
        Type a2 = m12154a(type, cls, a);
        if (!(a2 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) a2).getActualTypeArguments()[m12149a((Object[]) a.getTypeParameters(), (Object) typeVariable)];
    }

    /* renamed from: a */
    static boolean m12157a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: a */
    public static boolean m12158a(Type type, Type type2) {
        boolean z = true;
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            if (!m12157a((Object) parameterizedType.getOwnerType(), (Object) parameterizedType2.getOwnerType()) || !parameterizedType.getRawType().equals(parameterizedType2.getRawType()) || !Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                z = false;
            }
            return z;
        } else if (type instanceof GenericArrayType) {
            if (!(type2 instanceof GenericArrayType)) {
                return false;
            }
            return m12158a(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                z = false;
            }
            return z;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                z = false;
            }
            return z;
        }
    }

    /* renamed from: b */
    static Type m12159b(Type type, Class<?> cls, Class<?> cls2) {
        azd.m7062a(cls2.isAssignableFrom(cls));
        return m12155a(type, cls, m12154a(type, cls, cls2));
    }

    /* renamed from: b */
    public static WildcardType m12160b(Type type) {
        return new WildcardTypeImpl(new Type[]{type}, f9478a);
    }

    /* renamed from: b */
    public static Type[] m12161b(Type type, Class<?> cls) {
        if (type == Properties.class) {
            return new Type[]{String.class, String.class};
        }
        Type b = m12159b(type, cls, Map.class);
        if (b instanceof ParameterizedType) {
            return ((ParameterizedType) b).getActualTypeArguments();
        }
        return new Type[]{Object.class, Object.class};
    }

    /* renamed from: c */
    public static WildcardType m12162c(Type type) {
        return new WildcardTypeImpl(new Type[]{Object.class}, new Type[]{type});
    }

    /* renamed from: d */
    public static Type m12163d(Type type) {
        if (type instanceof Class) {
            Class cls = (Class) type;
            return (Type) (cls.isArray() ? new GenericArrayTypeImpl(m12163d(cls.getComponentType())) : cls);
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return new ParameterizedTypeImpl(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new GenericArrayTypeImpl(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType wildcardType = (WildcardType) type;
            return new WildcardTypeImpl(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
    }

    /* renamed from: e */
    public static Class<?> m12164e(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            azd.m7062a(rawType instanceof Class);
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(m12164e(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return m12164e(((WildcardType) type).getUpperBounds()[0]);
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + (type == null ? "null" : type.getClass().getName()));
        }
    }

    /* renamed from: f */
    public static String m12165f(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    /* renamed from: g */
    public static Type m12166g(Type type) {
        return type instanceof GenericArrayType ? ((GenericArrayType) type).getGenericComponentType() : ((Class) type).getComponentType();
    }

    /* renamed from: h */
    static void m12167h(Type type) {
        azd.m7062a(!(type instanceof Class) || !((Class) type).isPrimitive());
    }
}
