package com.facebook;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class AccessToken implements Parcelable {
    public static final Creator<AccessToken> CREATOR = new Creator() {
        /* renamed from: a */
        public AccessToken createFromParcel(Parcel parcel) {
            return new AccessToken(parcel);
        }

        /* renamed from: a */
        public AccessToken[] newArray(int i) {
            return new AccessToken[i];
        }
    };

    /* renamed from: a */
    private static final Date f7957a = new Date(Long.MAX_VALUE);

    /* renamed from: b */
    private static final Date f7958b = f7957a;

    /* renamed from: c */
    private static final Date f7959c = new Date();

    /* renamed from: d */
    private static final AccessTokenSource f7960d = AccessTokenSource.FACEBOOK_APPLICATION_WEB;

    /* renamed from: e */
    private final Date f7961e;

    /* renamed from: f */
    private final Set<String> f7962f;

    /* renamed from: g */
    private final Set<String> f7963g;

    /* renamed from: h */
    private final String f7964h;

    /* renamed from: i */
    private final AccessTokenSource f7965i;

    /* renamed from: j */
    private final Date f7966j;

    /* renamed from: k */
    private final String f7967k;

    /* renamed from: l */
    private final String f7968l;

    /* renamed from: com.facebook.AccessToken$a */
    public interface C1992a {
        /* renamed from: a */
        void mo11485a(AccessToken accessToken);

        /* renamed from: a */
        void mo11486a(FacebookException facebookException);
    }

    AccessToken(Parcel parcel) {
        this.f7961e = new Date(parcel.readLong());
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        this.f7962f = Collections.unmodifiableSet(new HashSet(arrayList));
        arrayList.clear();
        parcel.readStringList(arrayList);
        this.f7963g = Collections.unmodifiableSet(new HashSet(arrayList));
        this.f7964h = parcel.readString();
        this.f7965i = AccessTokenSource.valueOf(parcel.readString());
        this.f7966j = new Date(parcel.readLong());
        this.f7967k = parcel.readString();
        this.f7968l = parcel.readString();
    }

    public AccessToken(String str, String str2, String str3, Collection<String> collection, Collection<String> collection2, AccessTokenSource accessTokenSource, Date date, Date date2) {
        abh.m407a(str, "accessToken");
        abh.m407a(str2, "applicationId");
        abh.m407a(str3, "userId");
        if (date == null) {
            date = f7958b;
        }
        this.f7961e = date;
        this.f7962f = Collections.unmodifiableSet(collection != null ? new HashSet(collection) : new HashSet());
        this.f7963g = Collections.unmodifiableSet(collection2 != null ? new HashSet(collection2) : new HashSet());
        this.f7964h = str;
        if (accessTokenSource == null) {
            accessTokenSource = f7960d;
        }
        this.f7965i = accessTokenSource;
        if (date2 == null) {
            date2 = f7959c;
        }
        this.f7966j = date2;
        this.f7967k = str2;
        this.f7968l = str3;
    }

    /* renamed from: a */
    public static AccessToken m10408a() {
        return AccessTokenManager.m17270a().mo16932b();
    }

    /* renamed from: a */
    public static AccessToken m10409a(Bundle bundle) {
        List a = m10411a(bundle, "com.facebook.TokenCachingStrategy.Permissions");
        List a2 = m10411a(bundle, "com.facebook.TokenCachingStrategy.DeclinedPermissions");
        String d = LegacyTokenHelper.m17351d(bundle);
        if (abg.m374a(d)) {
            d = FacebookSdk.m17307j();
        }
        String b = LegacyTokenHelper.m17349b(bundle);
        try {
            return new AccessToken(b, d, abg.m390d(b).getString("id"), a, a2, LegacyTokenHelper.m17350c(bundle), LegacyTokenHelper.m17346a(bundle, "com.facebook.TokenCachingStrategy.ExpirationDate"), LegacyTokenHelper.m17346a(bundle, "com.facebook.TokenCachingStrategy.LastRefreshDate"));
        } catch (JSONException e) {
            return null;
        }
    }

    /* renamed from: a */
    public static AccessToken m10410a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.getInt("version") > 1) {
            throw new FacebookException("Unknown AccessToken serialization format.");
        }
        String string = jSONObject.getString("token");
        Date date = new Date(jSONObject.getLong("expires_at"));
        JSONArray jSONArray = jSONObject.getJSONArray("permissions");
        JSONArray jSONArray2 = jSONObject.getJSONArray("declined_permissions");
        Date date2 = new Date(jSONObject.getLong("last_refresh"));
        return new AccessToken(string, jSONObject.getString("application_id"), jSONObject.getString("user_id"), abg.m357a(jSONArray), abg.m357a(jSONArray2), AccessTokenSource.valueOf(jSONObject.getString("source")), date, date2);
    }

    /* renamed from: a */
    static List<String> m10411a(Bundle bundle, String str) {
        ArrayList stringArrayList = bundle.getStringArrayList(str);
        return stringArrayList == null ? Collections.emptyList() : Collections.unmodifiableList(new ArrayList(stringArrayList));
    }

    /* renamed from: a */
    public static void m10412a(AccessToken accessToken) {
        AccessTokenManager.m17270a().mo16931a(accessToken);
    }

    /* renamed from: a */
    private void m10413a(StringBuilder sb) {
        sb.append(" permissions:");
        if (this.f7962f == null) {
            sb.append("null");
            return;
        }
        sb.append("[");
        sb.append(TextUtils.join(", ", this.f7962f));
        sb.append("]");
    }

    /* renamed from: l */
    private String m10414l() {
        return this.f7964h == null ? "null" : FacebookSdk.m17294a(LoggingBehavior.INCLUDE_ACCESS_TOKENS) ? this.f7964h : "ACCESS_TOKEN_REMOVED";
    }

    /* renamed from: b */
    public String mo11466b() {
        return this.f7964h;
    }

    /* renamed from: c */
    public Date mo11467c() {
        return this.f7961e;
    }

    /* renamed from: d */
    public Set<String> mo11468d() {
        return this.f7962f;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public Set<String> mo11470e() {
        return this.f7963g;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AccessToken)) {
            return false;
        }
        AccessToken accessToken = (AccessToken) obj;
        return this.f7961e.equals(accessToken.f7961e) && this.f7962f.equals(accessToken.f7962f) && this.f7963g.equals(accessToken.f7963g) && this.f7964h.equals(accessToken.f7964h) && this.f7965i == accessToken.f7965i && this.f7966j.equals(accessToken.f7966j) && (this.f7967k != null ? this.f7967k.equals(accessToken.f7967k) : accessToken.f7967k == null) && this.f7968l.equals(accessToken.f7968l);
    }

    /* renamed from: f */
    public AccessTokenSource mo11472f() {
        return this.f7965i;
    }

    /* renamed from: g */
    public Date mo11473g() {
        return this.f7966j;
    }

    /* renamed from: h */
    public String mo11474h() {
        return this.f7967k;
    }

    public int hashCode() {
        return ((((((((((((((this.f7961e.hashCode() + 527) * 31) + this.f7962f.hashCode()) * 31) + this.f7963g.hashCode()) * 31) + this.f7964h.hashCode()) * 31) + this.f7965i.hashCode()) * 31) + this.f7966j.hashCode()) * 31) + (this.f7967k == null ? 0 : this.f7967k.hashCode())) * 31) + this.f7968l.hashCode();
    }

    /* renamed from: i */
    public String mo11476i() {
        return this.f7968l;
    }

    /* renamed from: j */
    public boolean mo11477j() {
        return new Date().after(this.f7961e);
    }

    /* renamed from: k */
    public JSONObject mo11478k() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("version", 1);
        jSONObject.put("token", this.f7964h);
        jSONObject.put("expires_at", this.f7961e.getTime());
        jSONObject.put("permissions", new JSONArray(this.f7962f));
        jSONObject.put("declined_permissions", new JSONArray(this.f7963g));
        jSONObject.put("last_refresh", this.f7966j.getTime());
        jSONObject.put("source", this.f7965i.name());
        jSONObject.put("application_id", this.f7967k);
        jSONObject.put("user_id", this.f7968l);
        return jSONObject;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{AccessToken");
        sb.append(" token:").append(m10414l());
        m10413a(sb);
        sb.append("}");
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f7961e.getTime());
        parcel.writeStringList(new ArrayList(this.f7962f));
        parcel.writeStringList(new ArrayList(this.f7963g));
        parcel.writeString(this.f7964h);
        parcel.writeString(this.f7965i.name());
        parcel.writeLong(this.f7966j.getTime());
        parcel.writeString(this.f7967k);
        parcel.writeString(this.f7968l);
    }
}
