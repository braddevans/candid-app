package com.facebook.internal;

import android.content.Intent;
import java.util.HashMap;
import java.util.Map;

public final class CallbackManagerImpl implements CallbackManager {

    /* renamed from: a */
    private static Map<Integer, C2016a> f8127a = new HashMap();

    /* renamed from: b */
    private Map<Integer, C2016a> f8128b = new HashMap();

    public enum RequestCodeOffset {
        Login(0),
        Share(1),
        Message(2),
        Like(3),
        GameRequest(4),
        AppGroupCreate(5),
        AppGroupJoin(6),
        AppInvite(7),
        DeviceShare(8);
        

        /* renamed from: j */
        private final int f8139j;

        private RequestCodeOffset(int i) {
            this.f8139j = i;
        }

        /* renamed from: a */
        public int mo11620a() {
            return FacebookSdk.m17312o() + this.f8139j;
        }
    }

    /* renamed from: com.facebook.internal.CallbackManagerImpl$a */
    public interface C2016a {
        /* renamed from: a */
        boolean mo271a(int i, Intent intent);
    }

    /* renamed from: a */
    private static synchronized C2016a m10586a(Integer num) {
        C2016a aVar;
        synchronized (CallbackManagerImpl.class) {
            aVar = (C2016a) f8127a.get(num);
        }
        return aVar;
    }

    /* renamed from: a */
    public static synchronized void m10587a(int i, C2016a aVar) {
        synchronized (CallbackManagerImpl.class) {
            abh.m406a((Object) aVar, "callback");
            if (!f8127a.containsKey(Integer.valueOf(i))) {
                f8127a.put(Integer.valueOf(i), aVar);
            }
        }
    }

    /* renamed from: b */
    private static boolean m10588b(int i, int i2, Intent intent) {
        C2016a a = m10586a(Integer.valueOf(i));
        if (a != null) {
            return a.mo271a(i2, intent);
        }
        return false;
    }

    /* renamed from: a */
    public boolean mo11618a(int i, int i2, Intent intent) {
        C2016a aVar = (C2016a) this.f8128b.get(Integer.valueOf(i));
        return aVar != null ? aVar.mo271a(i2, intent) : m10588b(i, i2, intent);
    }

    /* renamed from: b */
    public void mo11619b(int i, C2016a aVar) {
        abh.m406a((Object) aVar, "callback");
        this.f8128b.put(Integer.valueOf(i), aVar);
    }
}
