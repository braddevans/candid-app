package com.facebook.internal;

import java.util.EnumSet;
import java.util.Iterator;

public enum SmartLoginOption {
    None(0),
    Enabled(1),
    RequireConfirm(2);
    

    /* renamed from: d */
    public static final EnumSet<SmartLoginOption> f8146d = null;

    /* renamed from: e */
    private final long f8148e;

    static {
        f8146d = EnumSet.allOf(SmartLoginOption.class);
    }

    private SmartLoginOption(long j) {
        this.f8148e = j;
    }

    /* renamed from: a */
    public static EnumSet<SmartLoginOption> m10593a(long j) {
        EnumSet<SmartLoginOption> noneOf = EnumSet.noneOf(SmartLoginOption.class);
        Iterator it = f8146d.iterator();
        while (it.hasNext()) {
            SmartLoginOption smartLoginOption = (SmartLoginOption) it.next();
            if ((smartLoginOption.mo11621a() & j) != 0) {
                noneOf.add(smartLoginOption);
            }
        }
        return noneOf;
    }

    /* renamed from: a */
    public long mo11621a() {
        return this.f8148e;
    }
}
