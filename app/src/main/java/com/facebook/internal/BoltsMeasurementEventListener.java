package com.facebook.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.facebook.appevents.AppEventsLogger;

public class BoltsMeasurementEventListener extends BroadcastReceiver {

    /* renamed from: a */
    private static BoltsMeasurementEventListener f8125a;

    /* renamed from: b */
    private Context f8126b;

    private BoltsMeasurementEventListener(Context context) {
        this.f8126b = context.getApplicationContext();
    }

    /* renamed from: a */
    public static BoltsMeasurementEventListener m10583a(Context context) {
        if (f8125a != null) {
            return f8125a;
        }
        f8125a = new BoltsMeasurementEventListener(context);
        f8125a.m10584a();
        return f8125a;
    }

    /* renamed from: a */
    private void m10584a() {
        LocalBroadcastManager.m8652a(this.f8126b).mo9451a(this, new IntentFilter("com.parse.bolts.measurement_event"));
    }

    /* renamed from: b */
    private void m10585b() {
        LocalBroadcastManager.m8652a(this.f8126b).mo9450a((BroadcastReceiver) this);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            m10585b();
        } finally {
            super.finalize();
        }
    }

    public void onReceive(Context context, Intent intent) {
        AppEventsLogger a = AppEventsLogger.m10567a(context);
        String str = "bf_" + intent.getStringExtra("event_name");
        Bundle bundleExtra = intent.getBundleExtra("event_args");
        Bundle bundle = new Bundle();
        for (String str2 : bundleExtra.keySet()) {
            bundle.putString(str2.replaceAll("[^0-9a-zA-Z _-]", "-").replaceAll("^[ -]*", "").replaceAll("[ -]*$", ""), (String) bundleExtra.get(str2));
        }
        a.mo11608a(str, bundle);
    }
}
