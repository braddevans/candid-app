package com.facebook;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p003v7.widget.helper.ItemTouchHelper.Callback;
import java.net.HttpURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

public final class FacebookRequestError implements Parcelable {
    public static final Creator<FacebookRequestError> CREATOR = new Creator<FacebookRequestError>() {
        /* renamed from: a */
        public FacebookRequestError createFromParcel(Parcel parcel) {
            return new FacebookRequestError(parcel);
        }

        /* renamed from: a */
        public FacebookRequestError[] newArray(int i) {
            return new FacebookRequestError[i];
        }
    };

    /* renamed from: a */
    static final C1997a f8008a = new C1997a(Callback.DEFAULT_DRAG_ANIMATION_DURATION, 299);

    /* renamed from: b */
    private final Category f8009b;

    /* renamed from: c */
    private final int f8010c;

    /* renamed from: d */
    private final int f8011d;

    /* renamed from: e */
    private final int f8012e;

    /* renamed from: f */
    private final String f8013f;

    /* renamed from: g */
    private final String f8014g;

    /* renamed from: h */
    private final String f8015h;

    /* renamed from: i */
    private final String f8016i;

    /* renamed from: j */
    private final String f8017j;

    /* renamed from: k */
    private final JSONObject f8018k;

    /* renamed from: l */
    private final JSONObject f8019l;

    /* renamed from: m */
    private final Object f8020m;

    /* renamed from: n */
    private final HttpURLConnection f8021n;

    /* renamed from: o */
    private final FacebookException f8022o;

    public enum Category {
        LOGIN_RECOVERABLE,
        OTHER,
        TRANSIENT
    }

    /* renamed from: com.facebook.FacebookRequestError$a */
    static class C1997a {

        /* renamed from: a */
        private final int f8027a;

        /* renamed from: b */
        private final int f8028b;

        private C1997a(int i, int i2) {
            this.f8027a = i;
            this.f8028b = i2;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo11544a(int i) {
            return this.f8027a <= i && i <= this.f8028b;
        }
    }

    private FacebookRequestError(int i, int i2, int i3, String str, String str2, String str3, String str4, boolean z, JSONObject jSONObject, JSONObject jSONObject2, Object obj, HttpURLConnection httpURLConnection, FacebookException facebookException) {
        this.f8010c = i;
        this.f8011d = i2;
        this.f8012e = i3;
        this.f8013f = str;
        this.f8014g = str2;
        this.f8019l = jSONObject;
        this.f8018k = jSONObject2;
        this.f8020m = obj;
        this.f8021n = httpURLConnection;
        this.f8015h = str3;
        this.f8016i = str4;
        boolean z2 = false;
        if (facebookException != null) {
            this.f8022o = facebookException;
            z2 = true;
        } else {
            this.f8022o = new FacebookServiceException(this, str2);
        }
        aan h = m10455h();
        this.f8009b = z2 ? Category.OTHER : h.mo50a(i2, i3, z);
        this.f8017j = h.mo51a(this.f8009b);
    }

    public FacebookRequestError(int i, String str, String str2) {
        this(-1, i, -1, str, str2, null, null, false, null, null, null, null, null);
    }

    private FacebookRequestError(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), false, null, null, null, null, null);
    }

    public FacebookRequestError(HttpURLConnection httpURLConnection, Exception exc) {
        this(-1, -1, -1, null, null, null, null, false, null, null, null, httpURLConnection, exc instanceof FacebookException ? (FacebookException) exc : new FacebookException((Throwable) exc));
    }

    /* renamed from: a */
    public static FacebookRequestError m10454a(JSONObject jSONObject, Object obj, HttpURLConnection httpURLConnection) {
        JSONObject jSONObject2;
        try {
            if (jSONObject.has("code")) {
                int i = jSONObject.getInt("code");
                Object a = abg.m342a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT");
                if (a != null && (a instanceof JSONObject)) {
                    JSONObject jSONObject3 = (JSONObject) a;
                    String str = null;
                    String str2 = null;
                    String str3 = null;
                    String str4 = null;
                    boolean z = false;
                    int i2 = -1;
                    int i3 = -1;
                    boolean z2 = false;
                    if (jSONObject3.has("error")) {
                        JSONObject jSONObject4 = (JSONObject) abg.m342a(jSONObject3, "error", (String) null);
                        str = jSONObject4.optString("type", null);
                        str2 = jSONObject4.optString("message", null);
                        i2 = jSONObject4.optInt("code", -1);
                        i3 = jSONObject4.optInt("error_subcode", -1);
                        str3 = jSONObject4.optString("error_user_msg", null);
                        str4 = jSONObject4.optString("error_user_title", null);
                        z = jSONObject4.optBoolean("is_transient", false);
                        z2 = true;
                    } else if (jSONObject3.has("error_code") || jSONObject3.has("error_msg") || jSONObject3.has("error_reason")) {
                        str = jSONObject3.optString("error_reason", null);
                        str2 = jSONObject3.optString("error_msg", null);
                        i2 = jSONObject3.optInt("error_code", -1);
                        i3 = jSONObject3.optInt("error_subcode", -1);
                        z2 = true;
                    }
                    if (z2) {
                        return new FacebookRequestError(i, i2, i3, str, str2, str4, str3, z, jSONObject3, jSONObject, obj, httpURLConnection, null);
                    }
                }
                if (!f8008a.mo11544a(i)) {
                    if (jSONObject.has("body")) {
                        jSONObject2 = (JSONObject) abg.m342a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT");
                    } else {
                        jSONObject2 = null;
                    }
                    return new FacebookRequestError(i, -1, -1, null, null, null, null, false, jSONObject2, jSONObject, obj, httpURLConnection, null);
                }
            }
        } catch (JSONException e) {
        }
        return null;
    }

    /* renamed from: h */
    static synchronized aan m10455h() {
        aan g;
        synchronized (FacebookRequestError.class) {
            aap a = aaq.m115a(FacebookSdk.m17307j());
            g = a == null ? aan.m91a() : a.mo61g();
        }
        return g;
    }

    /* renamed from: a */
    public int mo11530a() {
        return this.f8010c;
    }

    /* renamed from: b */
    public int mo11531b() {
        return this.f8011d;
    }

    /* renamed from: c */
    public int mo11532c() {
        return this.f8012e;
    }

    /* renamed from: d */
    public String mo11533d() {
        return this.f8013f;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public String mo11535e() {
        return this.f8014g != null ? this.f8014g : this.f8022o.getLocalizedMessage();
    }

    /* renamed from: f */
    public JSONObject mo11536f() {
        return this.f8018k;
    }

    /* renamed from: g */
    public FacebookException mo11537g() {
        return this.f8022o;
    }

    public String toString() {
        return "{HttpStatus: " + this.f8010c + ", errorCode: " + this.f8011d + ", errorType: " + this.f8013f + ", errorMessage: " + mo11535e() + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f8010c);
        parcel.writeInt(this.f8011d);
        parcel.writeInt(this.f8012e);
        parcel.writeString(this.f8013f);
        parcel.writeString(this.f8014g);
        parcel.writeString(this.f8015h);
        parcel.writeString(this.f8016i);
    }
}
