package com.facebook;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.facebook.appevents.AppEventsLogger;

public abstract class FacebookButtonBase extends Button {

    /* renamed from: a */
    private String f7995a;

    /* renamed from: b */
    private String f7996b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public OnClickListener f7997c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public OnClickListener f7998d;

    /* renamed from: e */
    private boolean f7999e;

    /* renamed from: f */
    private int f8000f;

    /* renamed from: g */
    private int f8001g;

    /* renamed from: h */
    private aas f8002h;

    protected FacebookButtonBase(Context context, AttributeSet attributeSet, int i, int i2, String str, String str2) {
        super(context, attributeSet, 0);
        if (i2 == 0) {
            i2 = getDefaultStyleResource();
        }
        if (i2 == 0) {
            i2 = C3130g.com_facebook_button;
        }
        mo11502a(context, attributeSet, i, i2);
        this.f7995a = str;
        this.f7996b = str2;
        setClickable(true);
        setFocusable(true);
    }

    /* renamed from: a */
    private void mo11760a() {
        super.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FacebookButtonBase.this.m10442b(FacebookButtonBase.this.getContext());
                if (FacebookButtonBase.this.f7998d != null) {
                    FacebookButtonBase.this.f7998d.onClick(view);
                } else if (FacebookButtonBase.this.f7997c != null) {
                    FacebookButtonBase.this.f7997c.onClick(view);
                }
            }
        });
    }

    /* renamed from: a */
    private void m10439a(Context context) {
        AppEventsLogger.m10567a(context).mo11609a(this.f7995a, (Double) null, (Bundle) null);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m10442b(Context context) {
        AppEventsLogger.m10567a(context).mo11609a(this.f7996b, (Double) null, (Bundle) null);
    }

    /* renamed from: b */
    private void m10443b(Context context, AttributeSet attributeSet, int i, int i2) {
        if (!isInEditMode()) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, new int[]{16842964}, i, i2);
            try {
                if (obtainStyledAttributes.hasValue(0)) {
                    int resourceId = obtainStyledAttributes.getResourceId(0, 0);
                    if (resourceId != 0) {
                        setBackgroundResource(resourceId);
                    } else {
                        setBackgroundColor(obtainStyledAttributes.getColor(0, 0));
                    }
                } else {
                    setBackgroundColor(ContextCompat.getColor(context, C3124a.com_facebook_blue));
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
    }

    @SuppressLint({"ResourceType"})
    /* renamed from: c */
    private void m10444c(Context context, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, new int[]{16843119, 16843117, 16843120, 16843118, 16843121}, i, i2);
        try {
            setCompoundDrawablesWithIntrinsicBounds(obtainStyledAttributes.getResourceId(0, 0), obtainStyledAttributes.getResourceId(1, 0), obtainStyledAttributes.getResourceId(2, 0), obtainStyledAttributes.getResourceId(3, 0));
            setCompoundDrawablePadding(obtainStyledAttributes.getDimensionPixelSize(4, 0));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: d */
    private void m10445d(Context context, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, new int[]{16842966, 16842967, 16842968, 16842969}, i, i2);
        try {
            setPadding(obtainStyledAttributes.getDimensionPixelSize(0, 0), obtainStyledAttributes.getDimensionPixelSize(1, 0), obtainStyledAttributes.getDimensionPixelSize(2, 0), obtainStyledAttributes.getDimensionPixelSize(3, 0));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: e */
    private void m10446e(Context context, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, new int[]{16842904}, i, i2);
        try {
            setTextColor(obtainStyledAttributes.getColorStateList(0));
            obtainStyledAttributes.recycle();
            TypedArray obtainStyledAttributes2 = context.getTheme().obtainStyledAttributes(attributeSet, new int[]{16842927}, i, i2);
            try {
                setGravity(obtainStyledAttributes2.getInt(0, 17));
                obtainStyledAttributes2.recycle();
                TypedArray obtainStyledAttributes3 = context.getTheme().obtainStyledAttributes(attributeSet, new int[]{16842901, 16842903, 16843087}, i, i2);
                try {
                    setTextSize(0, (float) obtainStyledAttributes3.getDimensionPixelSize(0, 0));
                    setTypeface(Typeface.defaultFromStyle(obtainStyledAttributes3.getInt(1, 1)));
                    setText(obtainStyledAttributes3.getString(2));
                } finally {
                    obtainStyledAttributes3.recycle();
                }
            } catch (Throwable th) {
                obtainStyledAttributes2.recycle();
                throw th;
            }
        } catch (Throwable th2) {
            obtainStyledAttributes.recycle();
            throw th2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo11501a(String str) {
        return (int) Math.ceil((double) getPaint().measureText(str));
    }

    /* renamed from: a */
    public void mo11502a(Context context, AttributeSet attributeSet, int i, int i2) {
        m10443b(context, attributeSet, i, i2);
        m10444c(context, attributeSet, i, i2);
        m10445d(context, attributeSet, i, i2);
        m10446e(context, attributeSet, i, i2);
        mo11760a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11503a(View view) {
        if (this.f7997c != null) {
            this.f7997c.onClick(view);
        }
    }

    /* access modifiers changed from: protected */
    public Activity getActivity() {
        Context context = getContext();
        while (!(context instanceof Activity) && (context instanceof ContextWrapper)) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (context instanceof Activity) {
            return (Activity) context;
        }
        throw new FacebookException("Unable to get Activity.");
    }

    public int getCompoundPaddingLeft() {
        return this.f7999e ? this.f8000f : super.getCompoundPaddingLeft();
    }

    public int getCompoundPaddingRight() {
        return this.f7999e ? this.f8001g : super.getCompoundPaddingRight();
    }

    public abstract int getDefaultRequestCode();

    public int getDefaultStyleResource() {
        return 0;
    }

    public Fragment getFragment() {
        if (this.f8002h != null) {
            return this.f8002h.mo105b();
        }
        return null;
    }

    public android.app.Fragment getNativeFragment() {
        if (this.f8002h != null) {
            return this.f8002h.mo103a();
        }
        return null;
    }

    public int getRequestCode() {
        return getDefaultRequestCode();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            m10439a(getContext());
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if ((getGravity() & 1) != 0) {
            int compoundPaddingLeft = getCompoundPaddingLeft();
            int compoundPaddingRight = getCompoundPaddingRight();
            int min = Math.min((((getWidth() - (compoundPaddingLeft + getCompoundDrawablePadding())) - compoundPaddingRight) - mo11501a(getText().toString())) / 2, (compoundPaddingLeft - getPaddingLeft()) / 2);
            this.f8000f = compoundPaddingLeft - min;
            this.f8001g = compoundPaddingRight + min;
            this.f7999e = true;
        }
        super.onDraw(canvas);
        this.f7999e = false;
    }

    public void setFragment(android.app.Fragment fragment) {
        this.f8002h = new aas(fragment);
    }

    public void setFragment(Fragment fragment) {
        this.f8002h = new aas(fragment);
    }

    /* access modifiers changed from: protected */
    public void setInternalOnClickListener(OnClickListener onClickListener) {
        this.f7998d = onClickListener;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.f7997c = onClickListener;
    }
}
