package com.facebook;

public class FacebookDialogException extends FacebookException {

    /* renamed from: a */
    private int f8005a;

    /* renamed from: b */
    private String f8006b;

    public FacebookDialogException(String str, int i, String str2) {
        super(str);
        this.f8005a = i;
        this.f8006b = str2;
    }

    /* renamed from: a */
    public int mo11527a() {
        return this.f8005a;
    }

    /* renamed from: b */
    public String mo11528b() {
        return this.f8006b;
    }

    public final String toString() {
        return "{FacebookDialogException: " + "errorCode: " + mo11527a() + ", message: " + getMessage() + ", url: " + mo11528b() + "}";
    }
}
