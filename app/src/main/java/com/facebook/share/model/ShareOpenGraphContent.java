package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareOpenGraphAction.C2072a;

public final class ShareOpenGraphContent extends ShareContent<ShareOpenGraphContent, Object> {
    public static final Creator<ShareOpenGraphContent> CREATOR = new Creator<ShareOpenGraphContent>() {
        /* renamed from: a */
        public ShareOpenGraphContent createFromParcel(Parcel parcel) {
            return new ShareOpenGraphContent(parcel);
        }

        /* renamed from: a */
        public ShareOpenGraphContent[] newArray(int i) {
            return new ShareOpenGraphContent[i];
        }
    };

    /* renamed from: a */
    private final ShareOpenGraphAction f8393a;

    /* renamed from: b */
    private final String f8394b;

    ShareOpenGraphContent(Parcel parcel) {
        super(parcel);
        this.f8393a = new C2072a().mo11913a(parcel).mo11916a();
        this.f8394b = parcel.readString();
    }

    /* renamed from: a */
    public ShareOpenGraphAction mo11918a() {
        return this.f8393a;
    }

    /* renamed from: b */
    public String mo11919b() {
        return this.f8394b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f8393a, 0);
        parcel.writeString(this.f8394b);
    }
}
