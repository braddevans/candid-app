package com.facebook.share.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public abstract class ShareMedia implements ShareModel {

    /* renamed from: a */
    private final Bundle f8387a;

    public enum Type {
        PHOTO,
        VIDEO
    }

    /* renamed from: com.facebook.share.model.ShareMedia$a */
    public static abstract class C2069a<M extends ShareMedia, B extends C2069a> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Bundle f8391a = new Bundle();

        /* renamed from: a */
        static List<ShareMedia> m10933a(Parcel parcel) {
            Parcelable[] readParcelableArray = parcel.readParcelableArray(ShareMedia.class.getClassLoader());
            ArrayList arrayList = new ArrayList(readParcelableArray.length);
            for (Parcelable parcelable : readParcelableArray) {
                arrayList.add((ShareMedia) parcelable);
            }
            return arrayList;
        }

        @Deprecated
        /* renamed from: a */
        public B mo11901a(Bundle bundle) {
            this.f8391a.putAll(bundle);
            return this;
        }

        /* renamed from: a */
        public B mo11902a(M m) {
            return m == null ? this : mo11901a(m.mo11897a());
        }
    }

    ShareMedia(Parcel parcel) {
        this.f8387a = parcel.readBundle();
    }

    protected ShareMedia(C2069a aVar) {
        this.f8387a = new Bundle(aVar.f8391a);
    }

    @Deprecated
    /* renamed from: a */
    public Bundle mo11897a() {
        return new Bundle(this.f8387a);
    }

    /* renamed from: b */
    public abstract Type mo11898b();

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f8387a);
    }
}
