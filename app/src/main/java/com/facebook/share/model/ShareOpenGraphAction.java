package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareOpenGraphValueContainer.C2075a;

public final class ShareOpenGraphAction extends ShareOpenGraphValueContainer<ShareOpenGraphAction, C2072a> {
    public static final Creator<ShareOpenGraphAction> CREATOR = new Creator<ShareOpenGraphAction>() {
        /* renamed from: a */
        public ShareOpenGraphAction createFromParcel(Parcel parcel) {
            return new ShareOpenGraphAction(parcel);
        }

        /* renamed from: a */
        public ShareOpenGraphAction[] newArray(int i) {
            return new ShareOpenGraphAction[i];
        }
    };

    /* renamed from: com.facebook.share.model.ShareOpenGraphAction$a */
    public static final class C2072a extends C2075a<ShareOpenGraphAction, C2072a> {
        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public C2072a mo11913a(Parcel parcel) {
            return mo11917a((ShareOpenGraphAction) parcel.readParcelable(ShareOpenGraphAction.class.getClassLoader()));
        }

        /* renamed from: a */
        public C2072a mo11917a(ShareOpenGraphAction shareOpenGraphAction) {
            return shareOpenGraphAction == null ? this : ((C2072a) super.mo11917a(shareOpenGraphAction)).mo11915a(shareOpenGraphAction.mo11908a());
        }

        /* renamed from: a */
        public C2072a mo11915a(String str) {
            mo11934a("og:type", str);
            return this;
        }

        /* renamed from: a */
        public ShareOpenGraphAction mo11916a() {
            return new ShareOpenGraphAction(this);
        }
    }

    ShareOpenGraphAction(Parcel parcel) {
        super(parcel);
    }

    private ShareOpenGraphAction(C2072a aVar) {
        super((C2075a<P, E>) aVar);
    }

    /* renamed from: a */
    public String mo11908a() {
        return mo11930b("og:type");
    }
}
