package com.facebook.share.model;

import android.os.Bundle;
import android.os.Parcel;
import com.facebook.share.model.ShareOpenGraphValueContainer;
import com.facebook.share.model.ShareOpenGraphValueContainer.C2075a;
import java.util.Set;

public abstract class ShareOpenGraphValueContainer<P extends ShareOpenGraphValueContainer, E extends C2075a> implements ShareModel {

    /* renamed from: a */
    private final Bundle f8395a;

    /* renamed from: com.facebook.share.model.ShareOpenGraphValueContainer$a */
    public static abstract class C2075a<P extends ShareOpenGraphValueContainer, E extends C2075a> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Bundle f8396a = new Bundle();

        /* renamed from: a */
        public E mo11917a(P p) {
            if (p != null) {
                this.f8396a.putAll(p.mo11929b());
            }
            return this;
        }

        /* renamed from: a */
        public E mo11934a(String str, String str2) {
            this.f8396a.putString(str, str2);
            return this;
        }
    }

    ShareOpenGraphValueContainer(Parcel parcel) {
        this.f8395a = parcel.readBundle(C2075a.class.getClassLoader());
    }

    protected ShareOpenGraphValueContainer(C2075a<P, E> aVar) {
        this.f8395a = (Bundle) aVar.f8396a.clone();
    }

    /* renamed from: a */
    public Object mo11928a(String str) {
        return this.f8395a.get(str);
    }

    /* renamed from: b */
    public Bundle mo11929b() {
        return (Bundle) this.f8395a.clone();
    }

    /* renamed from: b */
    public String mo11930b(String str) {
        return this.f8395a.getString(str);
    }

    /* renamed from: c */
    public Set<String> mo11931c() {
        return this.f8395a.keySet();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f8395a);
    }
}
