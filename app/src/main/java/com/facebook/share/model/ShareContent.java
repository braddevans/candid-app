package com.facebook.share.model;

import android.net.Uri;
import android.os.Parcel;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareContent.C2064a;
import com.facebook.share.model.ShareHashtag.C2066a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ShareContent<P extends ShareContent, E extends C2064a> implements ShareModel {

    /* renamed from: a */
    private final Uri f8367a;

    /* renamed from: b */
    private final List<String> f8368b;

    /* renamed from: c */
    private final String f8369c;

    /* renamed from: d */
    private final String f8370d;

    /* renamed from: e */
    private final ShareHashtag f8371e;

    /* renamed from: com.facebook.share.model.ShareContent$a */
    public static abstract class C2064a<P extends ShareContent, E extends C2064a> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Uri f8372a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public List<String> f8373b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public String f8374c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public String f8375d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public ShareHashtag f8376e;

        /* renamed from: a */
        public E mo11867a(Uri uri) {
            this.f8372a = uri;
            return this;
        }

        /* renamed from: a */
        public E mo11868a(P p) {
            return p == null ? this : mo11867a(p.mo11862h()).mo11870a(p.mo11863i()).mo11869a(p.mo11864j()).mo11871b(p.mo11865k());
        }

        /* renamed from: a */
        public E mo11869a(String str) {
            this.f8374c = str;
            return this;
        }

        /* renamed from: a */
        public E mo11870a(List<String> list) {
            this.f8373b = list == null ? null : Collections.unmodifiableList(list);
            return this;
        }

        /* renamed from: b */
        public E mo11871b(String str) {
            this.f8375d = str;
            return this;
        }
    }

    protected ShareContent(Parcel parcel) {
        this.f8367a = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.f8368b = m10889a(parcel);
        this.f8369c = parcel.readString();
        this.f8370d = parcel.readString();
        this.f8371e = new C2066a().mo11879a(parcel).mo11882a();
    }

    protected ShareContent(C2064a aVar) {
        this.f8367a = aVar.f8372a;
        this.f8368b = aVar.f8373b;
        this.f8369c = aVar.f8374c;
        this.f8370d = aVar.f8375d;
        this.f8371e = aVar.f8376e;
    }

    /* renamed from: a */
    private List<String> m10889a(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        if (arrayList.size() == 0) {
            return null;
        }
        return Collections.unmodifiableList(arrayList);
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: h */
    public Uri mo11862h() {
        return this.f8367a;
    }

    /* renamed from: i */
    public List<String> mo11863i() {
        return this.f8368b;
    }

    /* renamed from: j */
    public String mo11864j() {
        return this.f8369c;
    }

    /* renamed from: k */
    public String mo11865k() {
        return this.f8370d;
    }

    /* renamed from: l */
    public ShareHashtag mo11866l() {
        return this.f8371e;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f8367a, 0);
        parcel.writeStringList(this.f8368b);
        parcel.writeString(this.f8369c);
        parcel.writeString(this.f8370d);
        parcel.writeParcelable(this.f8371e, 0);
    }
}
