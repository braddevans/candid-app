package com.facebook.share.model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareMedia.C2069a;
import com.facebook.share.model.ShareMedia.Type;
import java.util.ArrayList;
import java.util.List;

public final class SharePhoto extends ShareMedia {
    public static final Creator<SharePhoto> CREATOR = new Creator<SharePhoto>() {
        /* renamed from: a */
        public SharePhoto createFromParcel(Parcel parcel) {
            return new SharePhoto(parcel);
        }

        /* renamed from: a */
        public SharePhoto[] newArray(int i) {
            return new SharePhoto[i];
        }
    };

    /* renamed from: a */
    private final Bitmap f8397a;

    /* renamed from: b */
    private final Uri f8398b;

    /* renamed from: c */
    private final boolean f8399c;

    /* renamed from: d */
    private final String f8400d;

    /* renamed from: com.facebook.share.model.SharePhoto$a */
    public static final class C2077a extends C2069a<SharePhoto, C2077a> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Bitmap f8401a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public Uri f8402b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public boolean f8403c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public String f8404d;

        /* renamed from: a */
        static void m10968a(Parcel parcel, int i, List<SharePhoto> list) {
            ShareMedia[] shareMediaArr = new ShareMedia[list.size()];
            for (int i2 = 0; i2 < list.size(); i2++) {
                shareMediaArr[i2] = (ShareMedia) list.get(i2);
            }
            parcel.writeParcelableArray(shareMediaArr, i);
        }

        /* renamed from: c */
        static List<SharePhoto> m10970c(Parcel parcel) {
            List<ShareMedia> a = m10933a(parcel);
            ArrayList arrayList = new ArrayList();
            for (ShareMedia shareMedia : a) {
                if (shareMedia instanceof SharePhoto) {
                    arrayList.add((SharePhoto) shareMedia);
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public Uri mo11943a() {
            return this.f8402b;
        }

        /* renamed from: a */
        public C2077a mo11944a(Bitmap bitmap) {
            this.f8401a = bitmap;
            return this;
        }

        /* renamed from: a */
        public C2077a mo11945a(Uri uri) {
            this.f8402b = uri;
            return this;
        }

        /* renamed from: a */
        public C2077a mo11902a(SharePhoto sharePhoto) {
            return sharePhoto == null ? this : ((C2077a) super.mo11902a(sharePhoto)).mo11944a(sharePhoto.mo11935c()).mo11945a(sharePhoto.mo11936d()).mo11948a(sharePhoto.mo11937e()).mo11947a(sharePhoto.mo11938f());
        }

        /* renamed from: a */
        public C2077a mo11947a(String str) {
            this.f8404d = str;
            return this;
        }

        /* renamed from: a */
        public C2077a mo11948a(boolean z) {
            this.f8403c = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public Bitmap mo11949b() {
            return this.f8401a;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public C2077a mo11950b(Parcel parcel) {
            return mo11902a((SharePhoto) parcel.readParcelable(SharePhoto.class.getClassLoader()));
        }

        /* renamed from: c */
        public SharePhoto mo11951c() {
            return new SharePhoto(this);
        }
    }

    SharePhoto(Parcel parcel) {
        super(parcel);
        this.f8397a = (Bitmap) parcel.readParcelable(Bitmap.class.getClassLoader());
        this.f8398b = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.f8399c = parcel.readByte() != 0;
        this.f8400d = parcel.readString();
    }

    private SharePhoto(C2077a aVar) {
        super((C2069a) aVar);
        this.f8397a = aVar.f8401a;
        this.f8398b = aVar.f8402b;
        this.f8399c = aVar.f8403c;
        this.f8400d = aVar.f8404d;
    }

    /* renamed from: b */
    public Type mo11898b() {
        return Type.PHOTO;
    }

    /* renamed from: c */
    public Bitmap mo11935c() {
        return this.f8397a;
    }

    /* renamed from: d */
    public Uri mo11936d() {
        return this.f8398b;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public boolean mo11937e() {
        return this.f8399c;
    }

    /* renamed from: f */
    public String mo11938f() {
        return this.f8400d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 0;
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f8397a, 0);
        parcel.writeParcelable(this.f8398b, 0);
        if (this.f8399c) {
            i2 = 1;
        }
        parcel.writeByte((byte) i2);
        parcel.writeString(this.f8400d);
    }
}
