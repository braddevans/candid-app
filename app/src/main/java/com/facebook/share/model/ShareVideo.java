package com.facebook.share.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareMedia.C2069a;
import com.facebook.share.model.ShareMedia.Type;

public final class ShareVideo extends ShareMedia {
    public static final Creator<ShareVideo> CREATOR = new Creator<ShareVideo>() {
        /* renamed from: a */
        public ShareVideo createFromParcel(Parcel parcel) {
            return new ShareVideo(parcel);
        }

        /* renamed from: a */
        public ShareVideo[] newArray(int i) {
            return new ShareVideo[i];
        }
    };

    /* renamed from: a */
    private final Uri f8407a;

    /* renamed from: com.facebook.share.model.ShareVideo$a */
    public static final class C2081a extends C2069a<ShareVideo, C2081a> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Uri f8408a;

        /* renamed from: a */
        public C2081a mo11967a(Uri uri) {
            this.f8408a = uri;
            return this;
        }

        /* renamed from: a */
        public C2081a mo11902a(ShareVideo shareVideo) {
            return shareVideo == null ? this : ((C2081a) super.mo11902a(shareVideo)).mo11967a(shareVideo.mo11962c());
        }

        /* renamed from: a */
        public ShareVideo mo11969a() {
            return new ShareVideo(this);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public C2081a mo11970b(Parcel parcel) {
            return mo11902a((ShareVideo) parcel.readParcelable(ShareVideo.class.getClassLoader()));
        }
    }

    ShareVideo(Parcel parcel) {
        super(parcel);
        this.f8407a = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
    }

    private ShareVideo(C2081a aVar) {
        super((C2069a) aVar);
        this.f8407a = aVar.f8408a;
    }

    /* renamed from: b */
    public Type mo11898b() {
        return Type.VIDEO;
    }

    /* renamed from: c */
    public Uri mo11962c() {
        return this.f8407a;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f8407a, 0);
    }
}
