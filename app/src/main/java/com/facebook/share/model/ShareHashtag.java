package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ShareHashtag implements ShareModel {
    public static final Creator<ShareHashtag> CREATOR = new Creator<ShareHashtag>() {
        /* renamed from: a */
        public ShareHashtag createFromParcel(Parcel parcel) {
            return new ShareHashtag(parcel);
        }

        /* renamed from: a */
        public ShareHashtag[] newArray(int i) {
            return new ShareHashtag[i];
        }
    };

    /* renamed from: a */
    private final String f8377a;

    /* renamed from: com.facebook.share.model.ShareHashtag$a */
    public static class C2066a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public String f8378a;

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public C2066a mo11879a(Parcel parcel) {
            return mo11880a((ShareHashtag) parcel.readParcelable(ShareHashtag.class.getClassLoader()));
        }

        /* renamed from: a */
        public C2066a mo11880a(ShareHashtag shareHashtag) {
            return shareHashtag == null ? this : mo11881a(shareHashtag.mo11872a());
        }

        /* renamed from: a */
        public C2066a mo11881a(String str) {
            this.f8378a = str;
            return this;
        }

        /* renamed from: a */
        public ShareHashtag mo11882a() {
            return new ShareHashtag(this);
        }
    }

    ShareHashtag(Parcel parcel) {
        this.f8377a = parcel.readString();
    }

    private ShareHashtag(C2066a aVar) {
        this.f8377a = aVar.f8378a;
    }

    /* renamed from: a */
    public String mo11872a() {
        return this.f8377a;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f8377a);
    }
}
