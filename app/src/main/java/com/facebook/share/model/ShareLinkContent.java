package com.facebook.share.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareContent.C2064a;

public final class ShareLinkContent extends ShareContent<ShareLinkContent, C2068a> {
    public static final Creator<ShareLinkContent> CREATOR = new Creator<ShareLinkContent>() {
        /* renamed from: a */
        public ShareLinkContent createFromParcel(Parcel parcel) {
            return new ShareLinkContent(parcel);
        }

        /* renamed from: a */
        public ShareLinkContent[] newArray(int i) {
            return new ShareLinkContent[i];
        }
    };

    /* renamed from: a */
    private final String f8379a;

    /* renamed from: b */
    private final String f8380b;

    /* renamed from: c */
    private final Uri f8381c;

    /* renamed from: d */
    private final String f8382d;

    /* renamed from: com.facebook.share.model.ShareLinkContent$a */
    public static final class C2068a extends C2064a<ShareLinkContent, C2068a> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public String f8383a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public String f8384b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public Uri f8385c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public String f8386d;

        /* renamed from: a */
        public C2068a mo11868a(ShareLinkContent shareLinkContent) {
            return shareLinkContent == null ? this : ((C2068a) super.mo11868a(shareLinkContent)).mo11894c(shareLinkContent.mo11883a()).mo11893b(shareLinkContent.mo11885c()).mo11895d(shareLinkContent.mo11884b()).mo11896e(shareLinkContent.mo11886d());
        }

        /* renamed from: a */
        public ShareLinkContent mo11892a() {
            return new ShareLinkContent(this);
        }

        /* renamed from: b */
        public C2068a mo11893b(Uri uri) {
            this.f8385c = uri;
            return this;
        }

        /* renamed from: c */
        public C2068a mo11894c(String str) {
            this.f8383a = str;
            return this;
        }

        /* renamed from: d */
        public C2068a mo11895d(String str) {
            this.f8384b = str;
            return this;
        }

        /* renamed from: e */
        public C2068a mo11896e(String str) {
            this.f8386d = str;
            return this;
        }
    }

    ShareLinkContent(Parcel parcel) {
        super(parcel);
        this.f8379a = parcel.readString();
        this.f8380b = parcel.readString();
        this.f8381c = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.f8382d = parcel.readString();
    }

    private ShareLinkContent(C2068a aVar) {
        super((C2064a) aVar);
        this.f8379a = aVar.f8383a;
        this.f8380b = aVar.f8384b;
        this.f8381c = aVar.f8385c;
        this.f8382d = aVar.f8386d;
    }

    /* renamed from: a */
    public String mo11883a() {
        return this.f8379a;
    }

    /* renamed from: b */
    public String mo11884b() {
        return this.f8380b;
    }

    /* renamed from: c */
    public Uri mo11885c() {
        return this.f8381c;
    }

    /* renamed from: d */
    public String mo11886d() {
        return this.f8382d;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f8379a);
        parcel.writeString(this.f8380b);
        parcel.writeParcelable(this.f8381c, 0);
        parcel.writeString(this.f8382d);
    }
}
