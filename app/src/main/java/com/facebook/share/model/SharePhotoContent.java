package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareContent.C2064a;
import com.facebook.share.model.SharePhoto.C2077a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SharePhotoContent extends ShareContent<SharePhotoContent, C2079a> {
    public static final Creator<SharePhotoContent> CREATOR = new Creator<SharePhotoContent>() {
        /* renamed from: a */
        public SharePhotoContent createFromParcel(Parcel parcel) {
            return new SharePhotoContent(parcel);
        }

        /* renamed from: a */
        public SharePhotoContent[] newArray(int i) {
            return new SharePhotoContent[i];
        }
    };

    /* renamed from: a */
    private final List<SharePhoto> f8405a;

    /* renamed from: com.facebook.share.model.SharePhotoContent$a */
    public static class C2079a extends C2064a<SharePhotoContent, C2079a> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public final List<SharePhoto> f8406a = new ArrayList();

        /* renamed from: a */
        public C2079a mo11957a(SharePhoto sharePhoto) {
            if (sharePhoto != null) {
                this.f8406a.add(new C2077a().mo11902a(sharePhoto).mo11951c());
            }
            return this;
        }

        /* renamed from: a */
        public C2079a mo11868a(SharePhotoContent sharePhotoContent) {
            return sharePhotoContent == null ? this : ((C2079a) super.mo11868a(sharePhotoContent)).mo11960b(sharePhotoContent.mo11952a());
        }

        /* renamed from: a */
        public SharePhotoContent mo11959a() {
            return new SharePhotoContent(this);
        }

        /* renamed from: b */
        public C2079a mo11960b(List<SharePhoto> list) {
            if (list != null) {
                for (SharePhoto a : list) {
                    mo11957a(a);
                }
            }
            return this;
        }

        /* renamed from: c */
        public C2079a mo11961c(List<SharePhoto> list) {
            this.f8406a.clear();
            mo11960b(list);
            return this;
        }
    }

    SharePhotoContent(Parcel parcel) {
        super(parcel);
        this.f8405a = Collections.unmodifiableList(C2077a.m10970c(parcel));
    }

    private SharePhotoContent(C2079a aVar) {
        super((C2064a) aVar);
        this.f8405a = Collections.unmodifiableList(aVar.f8406a);
    }

    /* renamed from: a */
    public List<SharePhoto> mo11952a() {
        return this.f8405a;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        C2077a.m10968a(parcel, i, this.f8405a);
    }
}
