package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.SharePhoto.C2077a;
import com.facebook.share.model.ShareVideo.C2081a;

public final class ShareVideoContent extends ShareContent<ShareVideoContent, Object> implements ShareModel {
    public static final Creator<ShareVideoContent> CREATOR = new Creator<ShareVideoContent>() {
        /* renamed from: a */
        public ShareVideoContent createFromParcel(Parcel parcel) {
            return new ShareVideoContent(parcel);
        }

        /* renamed from: a */
        public ShareVideoContent[] newArray(int i) {
            return new ShareVideoContent[i];
        }
    };

    /* renamed from: a */
    private final String f8409a;

    /* renamed from: b */
    private final String f8410b;

    /* renamed from: c */
    private final SharePhoto f8411c;

    /* renamed from: d */
    private final ShareVideo f8412d;

    ShareVideoContent(Parcel parcel) {
        super(parcel);
        this.f8409a = parcel.readString();
        this.f8410b = parcel.readString();
        C2077a b = new C2077a().mo11950b(parcel);
        if (b.mo11943a() == null && b.mo11949b() == null) {
            this.f8411c = null;
        } else {
            this.f8411c = b.mo11951c();
        }
        this.f8412d = new C2081a().mo11970b(parcel).mo11969a();
    }

    /* renamed from: a */
    public String mo11971a() {
        return this.f8409a;
    }

    /* renamed from: b */
    public String mo11972b() {
        return this.f8410b;
    }

    /* renamed from: c */
    public SharePhoto mo11973c() {
        return this.f8411c;
    }

    /* renamed from: d */
    public ShareVideo mo11974d() {
        return this.f8412d;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f8409a);
        parcel.writeString(this.f8410b);
        parcel.writeParcelable(this.f8411c, 0);
        parcel.writeParcelable(this.f8412d, 0);
    }
}
