package com.facebook.share.widget;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.FacebookException;
import com.facebook.share.internal.LikeBoxCountView;
import com.facebook.share.internal.LikeBoxCountView.LikeBoxCountViewCaretPosition;
import com.facebook.share.internal.LikeButton;

public class LikeView extends FrameLayout {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f8418a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public ObjectType f8419b;

    /* renamed from: c */
    private LinearLayout f8420c;

    /* renamed from: d */
    private LikeButton f8421d;

    /* renamed from: e */
    private LikeBoxCountView f8422e;

    /* renamed from: f */
    private TextView f8423f;

    /* renamed from: g */
    private abu f8424g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public C2088c f8425h;

    /* renamed from: i */
    private BroadcastReceiver f8426i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public C2086a f8427j;

    /* renamed from: k */
    private Style f8428k = Style.f8462d;

    /* renamed from: l */
    private HorizontalAlignment f8429l = HorizontalAlignment.f8448d;

    /* renamed from: m */
    private AuxiliaryViewPosition f8430m = AuxiliaryViewPosition.f8441d;

    /* renamed from: n */
    private int f8431n = -1;

    /* renamed from: o */
    private int f8432o;

    /* renamed from: p */
    private int f8433p;

    /* renamed from: q */
    private aas f8434q;

    /* renamed from: r */
    private boolean f8435r;

    public enum AuxiliaryViewPosition {
        BOTTOM("bottom", 0),
        INLINE("inline", 1),
        TOP("top", 2);
        

        /* renamed from: d */
        static AuxiliaryViewPosition f8441d;

        /* renamed from: e */
        private String f8443e;

        /* renamed from: f */
        private int f8444f;

        static {
            f8441d = BOTTOM;
        }

        private AuxiliaryViewPosition(String str, int i) {
            this.f8443e = str;
            this.f8444f = i;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public int m11034a() {
            return this.f8444f;
        }

        /* renamed from: a */
        static AuxiliaryViewPosition m11036a(int i) {
            AuxiliaryViewPosition[] values;
            for (AuxiliaryViewPosition auxiliaryViewPosition : values()) {
                if (auxiliaryViewPosition.m11034a() == i) {
                    return auxiliaryViewPosition;
                }
            }
            return null;
        }

        public String toString() {
            return this.f8443e;
        }
    }

    public enum HorizontalAlignment {
        CENTER("center", 0),
        LEFT("left", 1),
        RIGHT("right", 2);
        

        /* renamed from: d */
        static HorizontalAlignment f8448d;

        /* renamed from: e */
        private String f8450e;

        /* renamed from: f */
        private int f8451f;

        static {
            f8448d = CENTER;
        }

        private HorizontalAlignment(String str, int i) {
            this.f8450e = str;
            this.f8451f = i;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public int m11037a() {
            return this.f8451f;
        }

        /* renamed from: a */
        static HorizontalAlignment m11039a(int i) {
            HorizontalAlignment[] values;
            for (HorizontalAlignment horizontalAlignment : values()) {
                if (horizontalAlignment.m11037a() == i) {
                    return horizontalAlignment;
                }
            }
            return null;
        }

        public String toString() {
            return this.f8450e;
        }
    }

    public enum ObjectType {
        UNKNOWN("unknown", 0),
        OPEN_GRAPH("open_graph", 1),
        PAGE("page", 2);
        

        /* renamed from: d */
        public static ObjectType f8455d;

        /* renamed from: e */
        private String f8457e;

        /* renamed from: f */
        private int f8458f;

        static {
            f8455d = UNKNOWN;
        }

        private ObjectType(String str, int i) {
            this.f8457e = str;
            this.f8458f = i;
        }

        /* renamed from: a */
        public static ObjectType m11040a(int i) {
            ObjectType[] values;
            for (ObjectType objectType : values()) {
                if (objectType.mo11998a() == i) {
                    return objectType;
                }
            }
            return null;
        }

        /* renamed from: a */
        public int mo11998a() {
            return this.f8458f;
        }

        public String toString() {
            return this.f8457e;
        }
    }

    public enum Style {
        STANDARD("standard", 0),
        BUTTON("button", 1),
        BOX_COUNT("box_count", 2);
        

        /* renamed from: d */
        static Style f8462d;

        /* renamed from: e */
        private String f8464e;

        /* renamed from: f */
        private int f8465f;

        static {
            f8462d = STANDARD;
        }

        private Style(String str, int i) {
            this.f8464e = str;
            this.f8465f = i;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public int m11042a() {
            return this.f8465f;
        }

        /* renamed from: a */
        static Style m11044a(int i) {
            Style[] values;
            for (Style style : values()) {
                if (style.m11042a() == i) {
                    return style;
                }
            }
            return null;
        }

        public String toString() {
            return this.f8464e;
        }
    }

    /* renamed from: com.facebook.share.widget.LikeView$a */
    class C2086a implements C0105c {

        /* renamed from: b */
        private boolean f8467b;

        private C2086a() {
        }

        /* renamed from: a */
        public void mo12001a() {
            this.f8467b = true;
        }

        /* renamed from: a */
        public void mo284a(abu abu, FacebookException facebookException) {
            if (!this.f8467b) {
                if (abu != null) {
                    if (!abu.mo283e()) {
                        facebookException = new FacebookException("Cannot use LikeView. The device may not be supported.");
                    }
                    LikeView.this.m11016a(abu);
                    LikeView.this.m11026c();
                }
                if (!(facebookException == null || LikeView.this.f8425h == null)) {
                    LikeView.this.f8425h.mo12003a(facebookException);
                }
                LikeView.this.f8427j = null;
            }
        }
    }

    /* renamed from: com.facebook.share.widget.LikeView$b */
    class C2087b extends BroadcastReceiver {
        private C2087b() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Bundle extras = intent.getExtras();
            boolean z = true;
            if (extras != null) {
                String string = extras.getString("com.facebook.sdk.LikeActionController.OBJECT_ID");
                z = abg.m374a(string) || abg.m373a(LikeView.this.f8418a, string);
            }
            if (z) {
                if ("com.facebook.sdk.LikeActionController.UPDATED".equals(action)) {
                    LikeView.this.m11026c();
                } else if ("com.facebook.sdk.LikeActionController.DID_ERROR".equals(action)) {
                    if (LikeView.this.f8425h != null) {
                        LikeView.this.f8425h.mo12003a(abb.m283a(extras));
                    }
                } else if ("com.facebook.sdk.LikeActionController.DID_RESET".equals(action)) {
                    LikeView.this.m11022a(LikeView.this.f8418a, LikeView.this.f8419b);
                    LikeView.this.m11026c();
                }
            }
        }
    }

    /* renamed from: com.facebook.share.widget.LikeView$c */
    public interface C2088c {
        /* renamed from: a */
        void mo12003a(FacebookException facebookException);
    }

    public LikeView(Context context) {
        super(context);
        m11017a(context);
    }

    public LikeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m11018a(attributeSet);
        m11017a(context);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m11015a() {
        if (this.f8424g != null) {
            Activity activity = null;
            if (this.f8434q == null) {
                activity = getActivity();
            }
            this.f8424g.mo279a(activity, this.f8434q, getAnalyticsParameters());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m11016a(abu abu) {
        this.f8424g = abu;
        this.f8426i = new C2087b();
        LocalBroadcastManager a = LocalBroadcastManager.m8652a(getContext());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.facebook.sdk.LikeActionController.UPDATED");
        intentFilter.addAction("com.facebook.sdk.LikeActionController.DID_ERROR");
        intentFilter.addAction("com.facebook.sdk.LikeActionController.DID_RESET");
        a.mo9451a(this.f8426i, intentFilter);
    }

    /* renamed from: a */
    private void m11017a(Context context) {
        this.f8432o = getResources().getDimensionPixelSize(C3125b.com_facebook_likeview_edge_padding);
        this.f8433p = getResources().getDimensionPixelSize(C3125b.com_facebook_likeview_internal_padding);
        if (this.f8431n == -1) {
            this.f8431n = getResources().getColor(C3124a.com_facebook_likeview_text_color);
        }
        setBackgroundColor(0);
        this.f8420c = new LinearLayout(context);
        this.f8420c.setLayoutParams(new LayoutParams(-2, -2));
        m11025b(context);
        m11027c(context);
        m11031d(context);
        this.f8420c.addView(this.f8421d);
        this.f8420c.addView(this.f8423f);
        this.f8420c.addView(this.f8422e);
        addView(this.f8420c);
        m11022a(this.f8418a, this.f8419b);
        m11026c();
    }

    /* renamed from: a */
    private void m11018a(AttributeSet attributeSet) {
        if (attributeSet != null && getContext() != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, C3131h.com_facebook_like_view);
            if (obtainStyledAttributes != null) {
                this.f8418a = abg.m347a(obtainStyledAttributes.getString(C3131h.com_facebook_like_view_com_facebook_object_id), (String) null);
                this.f8419b = ObjectType.m11040a(obtainStyledAttributes.getInt(C3131h.com_facebook_like_view_com_facebook_object_type, ObjectType.f8455d.mo11998a()));
                this.f8428k = Style.m11044a(obtainStyledAttributes.getInt(C3131h.com_facebook_like_view_com_facebook_style, Style.f8462d.m11042a()));
                if (this.f8428k == null) {
                    throw new IllegalArgumentException("Unsupported value for LikeView 'style'");
                }
                this.f8430m = AuxiliaryViewPosition.m11036a(obtainStyledAttributes.getInt(C3131h.com_facebook_like_view_com_facebook_auxiliary_view_position, AuxiliaryViewPosition.f8441d.m11034a()));
                if (this.f8430m == null) {
                    throw new IllegalArgumentException("Unsupported value for LikeView 'auxiliary_view_position'");
                }
                this.f8429l = HorizontalAlignment.m11039a(obtainStyledAttributes.getInt(C3131h.com_facebook_like_view_com_facebook_horizontal_alignment, HorizontalAlignment.f8448d.m11037a()));
                if (this.f8429l == null) {
                    throw new IllegalArgumentException("Unsupported value for LikeView 'horizontal_alignment'");
                }
                this.f8431n = obtainStyledAttributes.getColor(C3131h.com_facebook_like_view_com_facebook_foreground_color, -1);
                obtainStyledAttributes.recycle();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m11022a(String str, ObjectType objectType) {
        m11024b();
        this.f8418a = str;
        this.f8419b = objectType;
        if (!abg.m374a(str)) {
            this.f8427j = new C2086a();
            if (!isInEditMode()) {
                abu.m560a(str, objectType, (C0105c) this.f8427j);
            }
        }
    }

    /* renamed from: b */
    private void m11024b() {
        if (this.f8426i != null) {
            LocalBroadcastManager.m8652a(getContext()).mo9450a(this.f8426i);
            this.f8426i = null;
        }
        if (this.f8427j != null) {
            this.f8427j.mo12001a();
            this.f8427j = null;
        }
        this.f8424g = null;
    }

    /* renamed from: b */
    private void m11025b(Context context) {
        this.f8421d = new LikeButton(context, this.f8424g != null && this.f8424g.mo282d());
        this.f8421d.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                LikeView.this.m11015a();
            }
        });
        this.f8421d.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m11026c() {
        boolean z = !this.f8435r;
        if (this.f8424g == null) {
            this.f8421d.setSelected(false);
            this.f8423f.setText(null);
            this.f8422e.setText(null);
        } else {
            this.f8421d.setSelected(this.f8424g.mo282d());
            this.f8423f.setText(this.f8424g.mo281c());
            this.f8422e.setText(this.f8424g.mo280b());
            z &= this.f8424g.mo283e();
        }
        super.setEnabled(z);
        this.f8421d.setEnabled(z);
        m11030d();
    }

    /* renamed from: c */
    private void m11027c(Context context) {
        this.f8423f = new TextView(context);
        this.f8423f.setTextSize(0, getResources().getDimension(C3125b.com_facebook_likeview_text_size));
        this.f8423f.setMaxLines(2);
        this.f8423f.setTextColor(this.f8431n);
        this.f8423f.setGravity(17);
        this.f8423f.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
    }

    /* renamed from: d */
    private void m11030d() {
        View view;
        int i = 1;
        LayoutParams layoutParams = (LayoutParams) this.f8420c.getLayoutParams();
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.f8421d.getLayoutParams();
        int i2 = this.f8429l == HorizontalAlignment.LEFT ? 3 : this.f8429l == HorizontalAlignment.CENTER ? 1 : 5;
        layoutParams.gravity = i2 | 48;
        layoutParams2.gravity = i2;
        this.f8423f.setVisibility(8);
        this.f8422e.setVisibility(8);
        if (this.f8428k == Style.STANDARD && this.f8424g != null && !abg.m374a(this.f8424g.mo281c())) {
            view = this.f8423f;
        } else if (this.f8428k == Style.BOX_COUNT && this.f8424g != null && !abg.m374a(this.f8424g.mo280b())) {
            m11033e();
            view = this.f8422e;
        } else {
            return;
        }
        view.setVisibility(0);
        ((LinearLayout.LayoutParams) view.getLayoutParams()).gravity = i2;
        LinearLayout linearLayout = this.f8420c;
        if (this.f8430m == AuxiliaryViewPosition.INLINE) {
            i = 0;
        }
        linearLayout.setOrientation(i);
        if (this.f8430m == AuxiliaryViewPosition.TOP || (this.f8430m == AuxiliaryViewPosition.INLINE && this.f8429l == HorizontalAlignment.RIGHT)) {
            this.f8420c.removeView(this.f8421d);
            this.f8420c.addView(this.f8421d);
        } else {
            this.f8420c.removeView(view);
            this.f8420c.addView(view);
        }
        switch (this.f8430m) {
            case TOP:
                view.setPadding(this.f8432o, this.f8432o, this.f8432o, this.f8433p);
                return;
            case BOTTOM:
                view.setPadding(this.f8432o, this.f8433p, this.f8432o, this.f8432o);
                return;
            case INLINE:
                if (this.f8429l == HorizontalAlignment.RIGHT) {
                    view.setPadding(this.f8432o, this.f8432o, this.f8433p, this.f8432o);
                    return;
                } else {
                    view.setPadding(this.f8433p, this.f8432o, this.f8432o, this.f8432o);
                    return;
                }
            default:
                return;
        }
    }

    /* renamed from: d */
    private void m11031d(Context context) {
        this.f8422e = new LikeBoxCountView(context);
        this.f8422e.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
    }

    /* renamed from: e */
    private void m11033e() {
        switch (this.f8430m) {
            case TOP:
                this.f8422e.setCaretPosition(LikeBoxCountViewCaretPosition.BOTTOM);
                return;
            case BOTTOM:
                this.f8422e.setCaretPosition(LikeBoxCountViewCaretPosition.TOP);
                return;
            case INLINE:
                this.f8422e.setCaretPosition(this.f8429l == HorizontalAlignment.RIGHT ? LikeBoxCountViewCaretPosition.RIGHT : LikeBoxCountViewCaretPosition.LEFT);
                return;
            default:
                return;
        }
    }

    private Activity getActivity() {
        Context context = getContext();
        while (!(context instanceof Activity) && (context instanceof ContextWrapper)) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (context instanceof Activity) {
            return (Activity) context;
        }
        throw new FacebookException("Unable to get Activity.");
    }

    private Bundle getAnalyticsParameters() {
        Bundle bundle = new Bundle();
        bundle.putString("style", this.f8428k.toString());
        bundle.putString("auxiliary_position", this.f8430m.toString());
        bundle.putString("horizontal_alignment", this.f8429l.toString());
        bundle.putString("object_id", abg.m347a(this.f8418a, ""));
        bundle.putString("object_type", this.f8419b.toString());
        return bundle;
    }

    public C2088c getOnErrorListener() {
        return this.f8425h;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        setObjectIdAndType(null, ObjectType.UNKNOWN);
        super.onDetachedFromWindow();
    }

    public void setAuxiliaryViewPosition(AuxiliaryViewPosition auxiliaryViewPosition) {
        if (auxiliaryViewPosition == null) {
            auxiliaryViewPosition = AuxiliaryViewPosition.f8441d;
        }
        if (this.f8430m != auxiliaryViewPosition) {
            this.f8430m = auxiliaryViewPosition;
            m11030d();
        }
    }

    public void setEnabled(boolean z) {
        this.f8435r = !z;
        m11026c();
    }

    public void setForegroundColor(int i) {
        if (this.f8431n != i) {
            this.f8423f.setTextColor(i);
        }
    }

    public void setFragment(Fragment fragment) {
        this.f8434q = new aas(fragment);
    }

    public void setFragment(android.support.p001v4.app.Fragment fragment) {
        this.f8434q = new aas(fragment);
    }

    public void setHorizontalAlignment(HorizontalAlignment horizontalAlignment) {
        if (horizontalAlignment == null) {
            horizontalAlignment = HorizontalAlignment.f8448d;
        }
        if (this.f8429l != horizontalAlignment) {
            this.f8429l = horizontalAlignment;
            m11030d();
        }
    }

    public void setLikeViewStyle(Style style) {
        if (style == null) {
            style = Style.f8462d;
        }
        if (this.f8428k != style) {
            this.f8428k = style;
            m11030d();
        }
    }

    public void setObjectIdAndType(String str, ObjectType objectType) {
        String a = abg.m347a(str, (String) null);
        if (objectType == null) {
            objectType = ObjectType.f8455d;
        }
        if (!abg.m373a(a, this.f8418a) || objectType != this.f8419b) {
            m11022a(a, objectType);
            m11026c();
        }
    }

    public void setOnErrorListener(C2088c cVar) {
        this.f8425h = cVar;
    }
}
