package com.facebook.share.widget;

import android.content.Context;
import android.util.AttributeSet;
import com.facebook.internal.CallbackManagerImpl.RequestCodeOffset;
import com.facebook.share.model.ShareContent;

public final class SendButton extends ShareButtonBase {
    public SendButton(Context context) {
        super(context, null, 0, "fb_send_button_create", "fb_send_button_did_tap");
    }

    public SendButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0, "fb_send_button_create", "fb_send_button_did_tap");
    }

    public SendButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, "fb_send_button_create", "fb_send_button_did_tap");
    }

    /* access modifiers changed from: protected */
    public int getDefaultRequestCode() {
        return RequestCodeOffset.Message.mo11620a();
    }

    /* access modifiers changed from: protected */
    public int getDefaultStyleResource() {
        return C3130g.com_facebook_button_send;
    }

    /* access modifiers changed from: protected */
    public aal<ShareContent, C0087a> getDialog() {
        return getFragment() != null ? new acd(getFragment(), getRequestCode()) : getNativeFragment() != null ? new acd(getNativeFragment(), getRequestCode()) : new acd(getActivity(), getRequestCode());
    }
}
