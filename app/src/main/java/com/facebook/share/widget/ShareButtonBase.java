package com.facebook.share.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import com.facebook.FacebookButtonBase;
import com.facebook.share.model.ShareContent;

public abstract class ShareButtonBase extends FacebookButtonBase {

    /* renamed from: a */
    private ShareContent f8469a;

    /* renamed from: b */
    private int f8470b = 0;

    /* renamed from: c */
    private boolean f8471c = false;

    protected ShareButtonBase(Context context, AttributeSet attributeSet, int i, String str, String str2) {
        super(context, attributeSet, i, 0, str, str2);
        this.f8470b = isInEditMode() ? 0 : getDefaultRequestCode();
        m11049a(false);
    }

    /* renamed from: a */
    private void m11049a(boolean z) {
        setEnabled(z);
        this.f8471c = false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11502a(Context context, AttributeSet attributeSet, int i, int i2) {
        super.mo11502a(context, attributeSet, i, i2);
        setInternalOnClickListener(getShareOnClickListener());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo11760a() {
        return getDialog().mo33a(getShareContent());
    }

    /* access modifiers changed from: protected */
    public abstract aal<ShareContent, C0087a> getDialog();

    public int getRequestCode() {
        return this.f8470b;
    }

    public ShareContent getShareContent() {
        return this.f8469a;
    }

    /* access modifiers changed from: protected */
    public OnClickListener getShareOnClickListener() {
        return new OnClickListener() {
            public void onClick(View view) {
                ShareButtonBase.this.mo11503a(view);
                ShareButtonBase.this.getDialog().mo36b(ShareButtonBase.this.getShareContent());
            }
        };
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.f8471c = true;
    }

    /* access modifiers changed from: protected */
    public void setRequestCode(int i) {
        if (FacebookSdk.m17296b(i)) {
            throw new IllegalArgumentException("Request code " + i + " cannot be within the range reserved by the Facebook SDK.");
        }
        this.f8470b = i;
    }

    public void setShareContent(ShareContent shareContent) {
        this.f8469a = shareContent;
        if (!this.f8471c) {
            m11049a(mo11760a());
        }
    }
}
