package com.facebook.share.widget;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.internal.CallbackManagerImpl.RequestCodeOffset;
import com.facebook.share.internal.OpenGraphActionDialogFeature;
import com.facebook.share.internal.ShareDialogFeature;
import com.facebook.share.internal.ShareFeedContent;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhoto.C2077a;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.SharePhotoContent.C2079a;
import com.facebook.share.model.ShareVideoContent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public final class ShareDialog extends aal<ShareContent, C0087a> implements abs {

    /* renamed from: b */
    private static final int f8473b = RequestCodeOffset.Share.mo11620a();

    /* renamed from: c */
    private boolean f8474c;

    /* renamed from: d */
    private boolean f8475d;

    public enum Mode {
        AUTOMATIC,
        NATIVE,
        WEB,
        FEED
    }

    /* renamed from: com.facebook.share.widget.ShareDialog$a */
    class C2091a extends C0013a {
        private C2091a() {
            super();
        }

        /* renamed from: a */
        public aaf mo40a(ShareContent shareContent) {
            Bundle a;
            ShareDialog.this.m11053a(ShareDialog.this.mo35b(), shareContent, Mode.FEED);
            aaf d = ShareDialog.this.mo39d();
            if (shareContent instanceof ShareLinkContent) {
                ShareLinkContent shareLinkContent = (ShareLinkContent) shareContent;
                aca.m741c(shareLinkContent);
                a = acc.m802b(shareLinkContent);
            } else {
                a = acc.m797a((ShareFeedContent) shareContent);
            }
            aak.m59a(d, "feed", a);
            return d;
        }

        /* renamed from: a */
        public Object mo41a() {
            return Mode.FEED;
        }

        /* renamed from: a */
        public boolean mo42a(ShareContent shareContent, boolean z) {
            return (shareContent instanceof ShareLinkContent) || (shareContent instanceof ShareFeedContent);
        }
    }

    /* renamed from: com.facebook.share.widget.ShareDialog$b */
    class C2092b extends C0013a {
        private C2092b() {
            super();
        }

        /* renamed from: a */
        public aaf mo40a(final ShareContent shareContent) {
            ShareDialog.this.m11053a(ShareDialog.this.mo35b(), shareContent, Mode.NATIVE);
            aca.m730b(shareContent);
            final aaf d = ShareDialog.this.mo39d();
            final boolean e = ShareDialog.this.mo325e();
            aak.m54a(d, (C0012a) new C0012a() {
                /* renamed from: a */
                public Bundle mo27a() {
                    return abx.m688a(d.mo8c(), shareContent, e);
                }

                /* renamed from: b */
                public Bundle mo28b() {
                    return abt.m543a(d.mo8c(), shareContent, e);
                }
            }, ShareDialog.m11063g(shareContent.getClass()));
            return d;
        }

        /* renamed from: a */
        public Object mo41a() {
            return Mode.NATIVE;
        }

        /* renamed from: a */
        public boolean mo42a(ShareContent shareContent, boolean z) {
            if (shareContent == null) {
                return false;
            }
            boolean z2 = true;
            if (!z) {
                if (shareContent.mo11866l() != null) {
                    z2 = aak.m60a((aaj) ShareDialogFeature.HASHTAG);
                }
                if ((shareContent instanceof ShareLinkContent) && !abg.m374a(((ShareLinkContent) shareContent).mo11886d())) {
                    z2 &= aak.m60a((aaj) ShareDialogFeature.LINK_SHARE_QUOTES);
                }
            }
            return z2 && ShareDialog.m11061e(shareContent.getClass());
        }
    }

    /* renamed from: com.facebook.share.widget.ShareDialog$c */
    class C2094c extends C0013a {
        private C2094c() {
            super();
        }

        /* renamed from: a */
        private SharePhotoContent m11080a(SharePhotoContent sharePhotoContent, UUID uuid) {
            C2079a a = new C2079a().mo11868a(sharePhotoContent);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (int i = 0; i < sharePhotoContent.mo11952a().size(); i++) {
                SharePhoto sharePhoto = (SharePhoto) sharePhotoContent.mo11952a().get(i);
                Bitmap c = sharePhoto.mo11935c();
                if (c != null) {
                    C0052a a2 = aba.m251a(uuid, c);
                    sharePhoto = new C2077a().mo11902a(sharePhoto).mo11945a(Uri.parse(a2.mo170a())).mo11944a((Bitmap) null).mo11951c();
                    arrayList2.add(a2);
                }
                arrayList.add(sharePhoto);
            }
            a.mo11961c(arrayList);
            aba.m259a((Collection<C0052a>) arrayList2);
            return a.mo11959a();
        }

        /* renamed from: b */
        private String m11081b(ShareContent shareContent) {
            if ((shareContent instanceof ShareLinkContent) || (shareContent instanceof SharePhotoContent)) {
                return "share";
            }
            if (shareContent instanceof ShareOpenGraphContent) {
                return "share_open_graph";
            }
            return null;
        }

        /* renamed from: a */
        public aaf mo40a(ShareContent shareContent) {
            ShareDialog.this.m11053a(ShareDialog.this.mo35b(), shareContent, Mode.WEB);
            aaf d = ShareDialog.this.mo39d();
            aca.m741c(shareContent);
            Bundle a = shareContent instanceof ShareLinkContent ? acc.m799a((ShareLinkContent) shareContent) : shareContent instanceof SharePhotoContent ? acc.m801a(m11080a((SharePhotoContent) shareContent, d.mo8c())) : acc.m800a((ShareOpenGraphContent) shareContent);
            aak.m59a(d, m11081b(shareContent), a);
            return d;
        }

        /* renamed from: a */
        public Object mo41a() {
            return Mode.WEB;
        }

        /* renamed from: a */
        public boolean mo42a(ShareContent shareContent, boolean z) {
            return shareContent != null && ShareDialog.m11062f(shareContent.getClass());
        }
    }

    private ShareDialog(aas aas, int i) {
        super(aas, i);
        this.f8474c = false;
        this.f8475d = true;
        acb.m775a(i);
    }

    public ShareDialog(Activity activity) {
        super(activity, f8473b);
        this.f8474c = false;
        this.f8475d = true;
        acb.m775a(f8473b);
    }

    ShareDialog(Activity activity, int i) {
        super(activity, i);
        this.f8474c = false;
        this.f8475d = true;
        acb.m775a(i);
    }

    ShareDialog(Fragment fragment, int i) {
        this(new aas(fragment), i);
    }

    ShareDialog(android.support.p001v4.app.Fragment fragment, int i) {
        this(new aas(fragment), i);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m11053a(Context context, ShareContent shareContent, Mode mode) {
        String str;
        if (this.f8475d) {
            mode = Mode.AUTOMATIC;
        }
        switch (mode) {
            case AUTOMATIC:
                str = "automatic";
                break;
            case WEB:
                str = "web";
                break;
            case NATIVE:
                str = "native";
                break;
            default:
                str = "unknown";
                break;
        }
        aaj g = m11063g(shareContent.getClass());
        String str2 = g == ShareDialogFeature.SHARE_DIALOG ? "status" : g == ShareDialogFeature.PHOTOS ? "photo" : g == ShareDialogFeature.VIDEO ? "video" : g == OpenGraphActionDialogFeature.OG_ACTION_DIALOG ? "open_graph" : "unknown";
        AppEventsLogger a = AppEventsLogger.m10567a(context);
        Bundle bundle = new Bundle();
        bundle.putString("fb_share_dialog_show", str);
        bundle.putString("fb_share_dialog_content_type", str2);
        a.mo11609a("fb_share_dialog_show", (Double) null, bundle);
    }

    /* renamed from: a */
    public static boolean m11055a(Class<? extends ShareContent> cls) {
        return m11062f(cls) || m11061e(cls);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public static boolean m11061e(Class<? extends ShareContent> cls) {
        aaj g = m11063g(cls);
        return g != null && aak.m60a(g);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public static boolean m11062f(Class<? extends ShareContent> cls) {
        AccessToken a = AccessToken.m10408a();
        return ShareLinkContent.class.isAssignableFrom(cls) || ShareOpenGraphContent.class.isAssignableFrom(cls) || (SharePhotoContent.class.isAssignableFrom(cls) && (a != null && !a.mo11477j()));
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public static aaj m11063g(Class<? extends ShareContent> cls) {
        if (ShareLinkContent.class.isAssignableFrom(cls)) {
            return ShareDialogFeature.SHARE_DIALOG;
        }
        if (SharePhotoContent.class.isAssignableFrom(cls)) {
            return ShareDialogFeature.PHOTOS;
        }
        if (ShareVideoContent.class.isAssignableFrom(cls)) {
            return ShareDialogFeature.VIDEO;
        }
        if (ShareOpenGraphContent.class.isAssignableFrom(cls)) {
            return OpenGraphActionDialogFeature.OG_ACTION_DIALOG;
        }
        if (ShareMediaContent.class.isAssignableFrom(cls)) {
            return ShareDialogFeature.MULTIMEDIA;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo31a(CallbackManagerImpl callbackManagerImpl, FacebookCallback<C0087a> zhVar) {
        acb.m776a(mo29a(), (CallbackManager) callbackManagerImpl, zhVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public List<C0013a> mo38c() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C2092b());
        arrayList.add(new C2091a());
        arrayList.add(new C2094c());
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public aaf mo39d() {
        return new aaf(mo29a());
    }

    /* renamed from: e */
    public boolean mo325e() {
        return this.f8474c;
    }
}
