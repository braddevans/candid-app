package com.facebook.share.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import com.facebook.FacebookButtonBase;
import com.facebook.internal.CallbackManagerImpl.RequestCodeOffset;
import com.facebook.share.model.ShareContent;

public final class DeviceShareButton extends FacebookButtonBase {

    /* renamed from: a */
    private ShareContent f8413a;

    /* renamed from: b */
    private int f8414b;

    /* renamed from: c */
    private boolean f8415c;

    /* renamed from: d */
    private abr f8416d;

    public DeviceShareButton(Context context) {
        this(context, null, 0);
    }

    public DeviceShareButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    private DeviceShareButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, 0, "fb_device_share_button_create", "fb_device_share_button_did_tap");
        this.f8414b = 0;
        this.f8415c = false;
        this.f8416d = null;
        this.f8414b = isInEditMode() ? 0 : getDefaultRequestCode();
        m11011a(false);
    }

    /* renamed from: a */
    private void m11011a(boolean z) {
        setEnabled(z);
        this.f8415c = false;
    }

    /* renamed from: a */
    private boolean m11012a() {
        return new abr(getActivity()).mo33a(getShareContent());
    }

    /* access modifiers changed from: private */
    public abr getDialog() {
        if (this.f8416d != null) {
            return this.f8416d;
        }
        if (getFragment() != null) {
            this.f8416d = new abr(getFragment());
        } else if (getNativeFragment() != null) {
            this.f8416d = new abr(getNativeFragment());
        } else {
            this.f8416d = new abr(getActivity());
        }
        return this.f8416d;
    }

    private void setRequestCode(int i) {
        if (FacebookSdk.m17296b(i)) {
            throw new IllegalArgumentException("Request code " + i + " cannot be within the range reserved by the Facebook SDK.");
        }
        this.f8414b = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11502a(Context context, AttributeSet attributeSet, int i, int i2) {
        super.mo11502a(context, attributeSet, i, i2);
        setInternalOnClickListener(getShareOnClickListener());
    }

    /* access modifiers changed from: protected */
    public int getDefaultRequestCode() {
        return RequestCodeOffset.Share.mo11620a();
    }

    /* access modifiers changed from: protected */
    public int getDefaultStyleResource() {
        return C3130g.com_facebook_button_share;
    }

    public int getRequestCode() {
        return this.f8414b;
    }

    public ShareContent getShareContent() {
        return this.f8413a;
    }

    /* access modifiers changed from: protected */
    public OnClickListener getShareOnClickListener() {
        return new OnClickListener() {
            public void onClick(View view) {
                DeviceShareButton.this.mo11503a(view);
                DeviceShareButton.this.getDialog().mo36b(DeviceShareButton.this.getShareContent());
            }
        };
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.f8415c = true;
    }

    public void setShareContent(ShareContent shareContent) {
        this.f8413a = shareContent;
        if (!this.f8415c) {
            m11011a(m11012a());
        }
    }
}
