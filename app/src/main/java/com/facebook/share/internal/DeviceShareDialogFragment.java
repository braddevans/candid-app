package com.facebook.share.internal;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p001v4.app.DialogFragment;
import android.support.p001v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.C2004b;
import com.facebook.HttpMethod;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphContent;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

public class DeviceShareDialogFragment extends DialogFragment {

    /* renamed from: f */
    private static ScheduledThreadPoolExecutor f8308f;

    /* renamed from: a */
    private ProgressBar f8309a;

    /* renamed from: b */
    private TextView f8310b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public Dialog f8311c;

    /* renamed from: d */
    private volatile RequestState f8312d;

    /* renamed from: e */
    private volatile ScheduledFuture f8313e;

    /* renamed from: g */
    private ShareContent f8314g;

    static class RequestState implements Parcelable {
        public static final Creator<RequestState> CREATOR = new Creator<RequestState>() {
            /* renamed from: a */
            public RequestState createFromParcel(Parcel parcel) {
                return new RequestState(parcel);
            }

            /* renamed from: a */
            public RequestState[] newArray(int i) {
                return new RequestState[i];
            }
        };

        /* renamed from: a */
        private String f8318a;

        /* renamed from: b */
        private long f8319b;

        RequestState() {
        }

        protected RequestState(Parcel parcel) {
            this.f8318a = parcel.readString();
            this.f8319b = parcel.readLong();
        }

        /* renamed from: a */
        public String mo11824a() {
            return this.f8318a;
        }

        /* renamed from: a */
        public void mo11825a(long j) {
            this.f8319b = j;
        }

        /* renamed from: a */
        public void mo11826a(String str) {
            this.f8318a = str;
        }

        /* renamed from: b */
        public long mo11827b() {
            return this.f8319b;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f8318a);
            parcel.writeLong(this.f8319b);
        }
    }

    /* renamed from: a */
    private void m10839a() {
        if (isAdded()) {
            getFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    /* renamed from: a */
    private void m10840a(int i, Intent intent) {
        aae.m7b(this.f8312d.mo11824a());
        if (isAdded()) {
            FragmentActivity activity = getActivity();
            activity.setResult(i, intent);
            activity.finish();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10841a(FacebookRequestError facebookRequestError) {
        m10839a();
        Intent intent = new Intent();
        intent.putExtra("error", facebookRequestError);
        m10840a(-1, intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10842a(RequestState requestState) {
        this.f8312d = requestState;
        this.f8310b.setText(requestState.mo11824a());
        this.f8310b.setVisibility(0);
        this.f8309a.setVisibility(8);
        this.f8313e = m10847d().schedule(new Runnable() {
            public void run() {
                DeviceShareDialogFragment.this.f8311c.dismiss();
            }
        }, requestState.mo11827b(), TimeUnit.SECONDS);
    }

    /* renamed from: b */
    private Bundle m10845b() {
        ShareContent shareContent = this.f8314g;
        if (shareContent == null) {
            return null;
        }
        if (shareContent instanceof ShareLinkContent) {
            return acc.m799a((ShareLinkContent) shareContent);
        }
        if (shareContent instanceof ShareOpenGraphContent) {
            return acc.m800a((ShareOpenGraphContent) shareContent);
        }
        return null;
    }

    /* renamed from: c */
    private void m10846c() {
        Bundle b = m10845b();
        if (b == null || b.size() == 0) {
            m10841a(new FacebookRequestError(0, "", "Failed to get share content"));
        }
        b.putString("access_token", abh.m409b() + "|" + abh.m413c());
        b.putString("device_info", aae.m5a());
        new GraphRequest(null, "device/share", b, HttpMethod.POST, new C2004b() {
            public void onCompleted(GraphResponse zlVar) {
                FacebookRequestError a = zlVar.mo16977a();
                if (a != null) {
                    DeviceShareDialogFragment.this.m10841a(a);
                    return;
                }
                JSONObject b = zlVar.mo16978b();
                RequestState requestState = new RequestState();
                try {
                    requestState.mo11826a(b.getString("user_code"));
                    requestState.mo11825a(b.getLong("expires_in"));
                    DeviceShareDialogFragment.this.m10842a(requestState);
                } catch (JSONException e) {
                    DeviceShareDialogFragment.this.m10841a(new FacebookRequestError(0, "", "Malformed server response"));
                }
            }
        }).mo11562j();
    }

    /* renamed from: d */
    private static synchronized ScheduledThreadPoolExecutor m10847d() {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        synchronized (DeviceShareDialogFragment.class) {
            if (f8308f == null) {
                f8308f = new ScheduledThreadPoolExecutor(1);
            }
            scheduledThreadPoolExecutor = f8308f;
        }
        return scheduledThreadPoolExecutor;
    }

    /* renamed from: a */
    public void mo11821a(ShareContent shareContent) {
        this.f8314g = shareContent;
    }

    public Dialog onCreateDialog(Bundle bundle) {
        this.f8311c = new Dialog(getActivity(), C3130g.com_facebook_auth_dialog);
        View inflate = getActivity().getLayoutInflater().inflate(C3128e.com_facebook_device_auth_dialog_fragment, null);
        this.f8309a = (ProgressBar) inflate.findViewById(C3127d.progress_bar);
        this.f8310b = (TextView) inflate.findViewById(C3127d.confirmation_code);
        ((Button) inflate.findViewById(C3127d.cancel_button)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                DeviceShareDialogFragment.this.f8311c.dismiss();
            }
        });
        ((TextView) inflate.findViewById(C3127d.com_facebook_device_auth_instructions)).setText(Html.fromHtml(getString(C3129f.com_facebook_device_auth_instructions)));
        this.f8311c.setContentView(inflate);
        m10846c();
        return this.f8311c;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        if (bundle != null) {
            RequestState requestState = (RequestState) bundle.getParcelable("request_state");
            if (requestState != null) {
                m10842a(requestState);
            }
        }
        return onCreateView;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (this.f8313e != null) {
            this.f8313e.cancel(true);
        }
        m10840a(-1, new Intent());
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.f8312d != null) {
            bundle.putParcelable("request_state", this.f8312d);
        }
    }
}
