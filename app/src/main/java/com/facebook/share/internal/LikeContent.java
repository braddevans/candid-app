package com.facebook.share.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareModel;

public class LikeContent implements ShareModel {
    public static final Creator<LikeContent> CREATOR = new Creator<LikeContent>() {
        /* renamed from: a */
        public LikeContent createFromParcel(Parcel parcel) {
            return new LikeContent(parcel);
        }

        /* renamed from: a */
        public LikeContent[] newArray(int i) {
            return new LikeContent[i];
        }
    };

    /* renamed from: a */
    private final String f8334a;

    /* renamed from: b */
    private final String f8335b;

    /* renamed from: com.facebook.share.internal.LikeContent$a */
    public static class C2062a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public String f8336a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public String f8337b;

        /* renamed from: a */
        public C2062a mo11846a(String str) {
            this.f8336a = str;
            return this;
        }

        /* renamed from: a */
        public LikeContent mo11847a() {
            return new LikeContent(this);
        }

        /* renamed from: b */
        public C2062a mo11848b(String str) {
            this.f8337b = str;
            return this;
        }
    }

    LikeContent(Parcel parcel) {
        this.f8334a = parcel.readString();
        this.f8335b = parcel.readString();
    }

    private LikeContent(C2062a aVar) {
        this.f8334a = aVar.f8336a;
        this.f8335b = aVar.f8337b;
    }

    /* renamed from: a */
    public String mo11838a() {
        return this.f8334a;
    }

    /* renamed from: b */
    public String mo11839b() {
        return this.f8335b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f8334a);
        parcel.writeString(this.f8335b);
    }
}
