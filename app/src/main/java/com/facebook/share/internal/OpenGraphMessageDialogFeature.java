package com.facebook.share.internal;

public enum OpenGraphMessageDialogFeature implements aaj {
    OG_MESSAGE_DIALOG(20140204);
    

    /* renamed from: b */
    private int f8351b;

    private OpenGraphMessageDialogFeature(int i) {
        this.f8351b = i;
    }

    /* renamed from: a */
    public String mo24a() {
        return "com.facebook.platform.action.request.OGMESSAGEPUBLISH_DIALOG";
    }

    /* renamed from: b */
    public int mo25b() {
        return this.f8351b;
    }
}
