package com.facebook.share.internal;

public enum OpenGraphActionDialogFeature implements aaj {
    OG_ACTION_DIALOG(20130618);
    

    /* renamed from: b */
    private int f8348b;

    private OpenGraphActionDialogFeature(int i) {
        this.f8348b = i;
    }

    /* renamed from: a */
    public String mo24a() {
        return "com.facebook.platform.action.request.OGACTIONPUBLISH_DIALOG";
    }

    /* renamed from: b */
    public int mo25b() {
        return this.f8348b;
    }
}
