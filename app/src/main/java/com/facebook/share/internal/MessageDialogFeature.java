package com.facebook.share.internal;

public enum MessageDialogFeature implements aaj {
    MESSAGE_DIALOG(20140204),
    PHOTOS(20140324),
    VIDEO(20141218);
    

    /* renamed from: d */
    private int f8345d;

    private MessageDialogFeature(int i) {
        this.f8345d = i;
    }

    /* renamed from: a */
    public String mo24a() {
        return "com.facebook.platform.action.request.MESSAGE_DIALOG";
    }

    /* renamed from: b */
    public int mo25b() {
        return this.f8345d;
    }
}
