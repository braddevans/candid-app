package com.facebook.share.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.share.model.ShareContent;

public class ShareFeedContent extends ShareContent<ShareFeedContent, Object> {
    public static final Creator<ShareFeedContent> CREATOR = new Creator<ShareFeedContent>() {
        /* renamed from: a */
        public ShareFeedContent createFromParcel(Parcel parcel) {
            return new ShareFeedContent(parcel);
        }

        /* renamed from: a */
        public ShareFeedContent[] newArray(int i) {
            return new ShareFeedContent[i];
        }
    };

    /* renamed from: a */
    private final String f8360a;

    /* renamed from: b */
    private final String f8361b;

    /* renamed from: c */
    private final String f8362c;

    /* renamed from: d */
    private final String f8363d;

    /* renamed from: e */
    private final String f8364e;

    /* renamed from: f */
    private final String f8365f;

    /* renamed from: g */
    private final String f8366g;

    ShareFeedContent(Parcel parcel) {
        super(parcel);
        this.f8360a = parcel.readString();
        this.f8361b = parcel.readString();
        this.f8362c = parcel.readString();
        this.f8363d = parcel.readString();
        this.f8364e = parcel.readString();
        this.f8365f = parcel.readString();
        this.f8366g = parcel.readString();
    }

    /* renamed from: a */
    public String mo11849a() {
        return this.f8360a;
    }

    /* renamed from: b */
    public String mo11850b() {
        return this.f8361b;
    }

    /* renamed from: c */
    public String mo11851c() {
        return this.f8362c;
    }

    /* renamed from: d */
    public String mo11852d() {
        return this.f8363d;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public String mo11854e() {
        return this.f8364e;
    }

    /* renamed from: f */
    public String mo11855f() {
        return this.f8365f;
    }

    /* renamed from: g */
    public String mo11856g() {
        return this.f8366g;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f8360a);
        parcel.writeString(this.f8361b);
        parcel.writeString(this.f8362c);
        parcel.writeString(this.f8363d);
        parcel.writeString(this.f8364e);
        parcel.writeString(this.f8365f);
        parcel.writeString(this.f8366g);
    }
}
