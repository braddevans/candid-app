package com.facebook.share.internal;

import android.content.Context;
import android.util.AttributeSet;
import com.facebook.FacebookButtonBase;

public class LikeButton extends FacebookButtonBase {
    public LikeButton(Context context, boolean z) {
        super(context, null, 0, 0, "fb_like_button_create", "fb_like_button_did_tap");
        setSelected(z);
    }

    /* renamed from: a */
    private void m10859a() {
        if (isSelected()) {
            setCompoundDrawablesWithIntrinsicBounds(C3126c.com_facebook_button_like_icon_selected, 0, 0, 0);
            setText(getResources().getString(C3129f.com_facebook_like_button_liked));
            return;
        }
        setCompoundDrawablesWithIntrinsicBounds(C3126c.com_facebook_button_icon, 0, 0, 0);
        setText(getResources().getString(C3129f.com_facebook_like_button_not_liked));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11502a(Context context, AttributeSet attributeSet, int i, int i2) {
        super.mo11502a(context, attributeSet, i, i2);
        m10859a();
    }

    /* access modifiers changed from: protected */
    public int getDefaultRequestCode() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getDefaultStyleResource() {
        return C3130g.com_facebook_button_like;
    }

    public void setSelected(boolean z) {
        super.setSelected(z);
        m10859a();
    }
}
