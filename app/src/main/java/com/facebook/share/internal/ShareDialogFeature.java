package com.facebook.share.internal;

public enum ShareDialogFeature implements aaj {
    SHARE_DIALOG(20130618),
    PHOTOS(20140204),
    VIDEO(20141028),
    MULTIMEDIA(20160327),
    HASHTAG(20160327),
    LINK_SHARE_QUOTES(20160327);
    

    /* renamed from: g */
    private int f8359g;

    private ShareDialogFeature(int i) {
        this.f8359g = i;
    }

    /* renamed from: a */
    public String mo24a() {
        return "com.facebook.platform.action.request.FEED_DIALOG";
    }

    /* renamed from: b */
    public int mo25b() {
        return this.f8359g;
    }
}
