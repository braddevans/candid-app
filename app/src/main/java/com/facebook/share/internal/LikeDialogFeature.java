package com.facebook.share.internal;

public enum LikeDialogFeature implements aaj {
    LIKE_DIALOG(20140701);
    

    /* renamed from: b */
    private int f8340b;

    private LikeDialogFeature(int i) {
        this.f8340b = i;
    }

    /* renamed from: a */
    public String mo24a() {
        return "com.facebook.platform.action.request.LIKE_DIALOG";
    }

    /* renamed from: b */
    public int mo25b() {
        return this.f8340b;
    }
}
