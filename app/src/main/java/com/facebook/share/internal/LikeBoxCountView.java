package com.facebook.share.internal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

public class LikeBoxCountView extends FrameLayout {

    /* renamed from: a */
    private TextView f8320a;

    /* renamed from: b */
    private LikeBoxCountViewCaretPosition f8321b = LikeBoxCountViewCaretPosition.LEFT;

    /* renamed from: c */
    private float f8322c;

    /* renamed from: d */
    private float f8323d;

    /* renamed from: e */
    private float f8324e;

    /* renamed from: f */
    private Paint f8325f;

    /* renamed from: g */
    private int f8326g;

    /* renamed from: h */
    private int f8327h;

    public enum LikeBoxCountViewCaretPosition {
        LEFT,
        TOP,
        RIGHT,
        BOTTOM
    }

    public LikeBoxCountView(Context context) {
        super(context);
        m10856a(context);
    }

    /* renamed from: a */
    private void m10855a(int i, int i2, int i3, int i4) {
        this.f8320a.setPadding(this.f8326g + i, this.f8326g + i2, this.f8326g + i3, this.f8326g + i4);
    }

    /* renamed from: a */
    private void m10856a(Context context) {
        setWillNotDraw(false);
        this.f8322c = getResources().getDimension(C3125b.com_facebook_likeboxcountview_caret_height);
        this.f8323d = getResources().getDimension(C3125b.com_facebook_likeboxcountview_caret_width);
        this.f8324e = getResources().getDimension(C3125b.com_facebook_likeboxcountview_border_radius);
        this.f8325f = new Paint();
        this.f8325f.setColor(getResources().getColor(C3124a.com_facebook_likeboxcountview_border_color));
        this.f8325f.setStrokeWidth(getResources().getDimension(C3125b.com_facebook_likeboxcountview_border_width));
        this.f8325f.setStyle(Style.STROKE);
        m10858b(context);
        addView(this.f8320a);
        setCaretPosition(this.f8321b);
    }

    /* renamed from: a */
    private void m10857a(Canvas canvas, float f, float f2, float f3, float f4) {
        Path path = new Path();
        float f5 = 2.0f * this.f8324e;
        path.addArc(new RectF(f, f2, f + f5, f2 + f5), -180.0f, 90.0f);
        if (this.f8321b == LikeBoxCountViewCaretPosition.TOP) {
            path.lineTo((((f3 - f) - this.f8323d) / 2.0f) + f, f2);
            path.lineTo(((f3 - f) / 2.0f) + f, f2 - this.f8322c);
            path.lineTo((((f3 - f) + this.f8323d) / 2.0f) + f, f2);
        }
        path.lineTo(f3 - this.f8324e, f2);
        path.addArc(new RectF(f3 - f5, f2, f3, f2 + f5), -90.0f, 90.0f);
        if (this.f8321b == LikeBoxCountViewCaretPosition.RIGHT) {
            path.lineTo(f3, (((f4 - f2) - this.f8323d) / 2.0f) + f2);
            path.lineTo(this.f8322c + f3, ((f4 - f2) / 2.0f) + f2);
            path.lineTo(f3, (((f4 - f2) + this.f8323d) / 2.0f) + f2);
        }
        path.lineTo(f3, f4 - this.f8324e);
        path.addArc(new RectF(f3 - f5, f4 - f5, f3, f4), 0.0f, 90.0f);
        if (this.f8321b == LikeBoxCountViewCaretPosition.BOTTOM) {
            path.lineTo((((f3 - f) + this.f8323d) / 2.0f) + f, f4);
            path.lineTo(((f3 - f) / 2.0f) + f, this.f8322c + f4);
            path.lineTo((((f3 - f) - this.f8323d) / 2.0f) + f, f4);
        }
        path.lineTo(this.f8324e + f, f4);
        path.addArc(new RectF(f, f4 - f5, f + f5, f4), 90.0f, 90.0f);
        if (this.f8321b == LikeBoxCountViewCaretPosition.LEFT) {
            path.lineTo(f, (((f4 - f2) + this.f8323d) / 2.0f) + f2);
            path.lineTo(f - this.f8322c, ((f4 - f2) / 2.0f) + f2);
            path.lineTo(f, (((f4 - f2) - this.f8323d) / 2.0f) + f2);
        }
        path.lineTo(f, this.f8324e + f2);
        canvas.drawPath(path, this.f8325f);
    }

    /* renamed from: b */
    private void m10858b(Context context) {
        this.f8320a = new TextView(context);
        this.f8320a.setLayoutParams(new LayoutParams(-1, -1));
        this.f8320a.setGravity(17);
        this.f8320a.setTextSize(0, getResources().getDimension(C3125b.com_facebook_likeboxcountview_text_size));
        this.f8320a.setTextColor(getResources().getColor(C3124a.com_facebook_likeboxcountview_text_color));
        this.f8326g = getResources().getDimensionPixelSize(C3125b.com_facebook_likeboxcountview_text_padding);
        this.f8327h = getResources().getDimensionPixelSize(C3125b.com_facebook_likeboxcountview_caret_height);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int paddingTop = getPaddingTop();
        int paddingLeft = getPaddingLeft();
        int width = getWidth() - getPaddingRight();
        int height = getHeight() - getPaddingBottom();
        switch (this.f8321b) {
            case LEFT:
                paddingLeft = (int) (((float) paddingLeft) + this.f8322c);
                break;
            case TOP:
                paddingTop = (int) (((float) paddingTop) + this.f8322c);
                break;
            case RIGHT:
                width = (int) (((float) width) - this.f8322c);
                break;
            case BOTTOM:
                height = (int) (((float) height) - this.f8322c);
                break;
        }
        m10857a(canvas, (float) paddingLeft, (float) paddingTop, (float) width, (float) height);
    }

    public void setCaretPosition(LikeBoxCountViewCaretPosition likeBoxCountViewCaretPosition) {
        this.f8321b = likeBoxCountViewCaretPosition;
        switch (likeBoxCountViewCaretPosition) {
            case LEFT:
                m10855a(this.f8327h, 0, 0, 0);
                return;
            case TOP:
                m10855a(0, this.f8327h, 0, 0);
                return;
            case RIGHT:
                m10855a(0, 0, this.f8327h, 0);
                return;
            case BOTTOM:
                m10855a(0, 0, 0, this.f8327h);
                return;
            default:
                return;
        }
    }

    public void setText(String str) {
        this.f8320a.setText(str);
    }
}
