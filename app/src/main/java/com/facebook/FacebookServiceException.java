package com.facebook;

public class FacebookServiceException extends FacebookException {

    /* renamed from: a */
    private final FacebookRequestError f8029a;

    public FacebookServiceException(FacebookRequestError facebookRequestError, String str) {
        super(str);
        this.f8029a = facebookRequestError;
    }

    /* renamed from: a */
    public final FacebookRequestError mo11545a() {
        return this.f8029a;
    }

    public final String toString() {
        return "{FacebookServiceException: " + "httpResponseCode: " + this.f8029a.mo11530a() + ", facebookErrorCode: " + this.f8029a.mo11531b() + ", facebookErrorType: " + this.f8029a.mo11533d() + ", message: " + this.f8029a.mo11535e() + "}";
    }
}
