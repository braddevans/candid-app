package com.facebook.login;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.facebook.AccessToken;
import com.facebook.AccessTokenSource;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookServiceException;
import com.facebook.login.LoginClient.Request;
import com.facebook.login.LoginClient.Result;
import java.util.Collection;
import java.util.Locale;

abstract class WebLoginMethodHandler extends LoginMethodHandler {

    /* renamed from: c */
    private String f8235c;

    WebLoginMethodHandler(Parcel parcel) {
        super(parcel);
    }

    WebLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* renamed from: c */
    private void m10756c(String str) {
        this.f8234b.mo11695b().getSharedPreferences("com.facebook.login.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY", 0).edit().putString("TOKEN", str).apply();
    }

    /* renamed from: e */
    private static final String m10757e() {
        return "fb" + FacebookSdk.m17307j() + "://authorize";
    }

    /* renamed from: f */
    private String m10758f() {
        return this.f8234b.mo11695b().getSharedPreferences("com.facebook.login.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY", 0).getString("TOKEN", "");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bundle mo11745a(Bundle bundle, Request request) {
        bundle.putString("redirect_uri", m10757e());
        bundle.putString("client_id", request.mo11721d());
        LoginClient loginClient = this.f8234b;
        bundle.putString("e2e", LoginClient.m10686m());
        bundle.putString("response_type", "token,signed_request");
        bundle.putString("return_scopes", "true");
        bundle.putString("auth_type", "rerequest");
        if (mo11626c() != null) {
            bundle.putString("sso", mo11626c());
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11746a(Request request, Bundle bundle, FacebookException facebookException) {
        Result a;
        this.f8235c = null;
        if (bundle != null) {
            if (bundle.containsKey("e2e")) {
                this.f8235c = bundle.getString("e2e");
            }
            try {
                AccessToken a2 = m10737a(request.mo11715a(), bundle, mo11627d_(), request.mo11721d());
                a = Result.m10726a(this.f8234b.mo11698c(), a2);
                CookieSyncManager.createInstance(this.f8234b.mo11695b()).sync();
                m10756c(a2.mo11466b());
            } catch (FacebookException e) {
                a = Result.m10728a(this.f8234b.mo11698c(), null, e.getMessage());
            }
        } else if (facebookException instanceof FacebookOperationCanceledException) {
            a = Result.m10727a(this.f8234b.mo11698c(), "User canceled log in.");
        } else {
            this.f8235c = null;
            String str = null;
            String message = facebookException.getMessage();
            if (facebookException instanceof FacebookServiceException) {
                FacebookRequestError a3 = ((FacebookServiceException) facebookException).mo11545a();
                str = String.format(Locale.ROOT, "%d", new Object[]{Integer.valueOf(a3.mo11531b())});
                message = a3.toString();
            }
            a = Result.m10729a(this.f8234b.mo11698c(), null, message, str);
        }
        if (!abg.m374a(this.f8235c)) {
            mo11742b(this.f8235c);
        }
        this.f8234b.mo11691a(a);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Bundle mo11747b(Request request) {
        Bundle bundle = new Bundle();
        if (!abg.m375a((Collection<T>) request.mo11715a())) {
            String join = TextUtils.join(",", request.mo11715a());
            bundle.putString("scope", join);
            mo11741a("scope", join);
        }
        bundle.putString("default_audience", request.mo11720c().mo11634a());
        bundle.putString("state", mo11739a(request.mo11723e()));
        AccessToken a = AccessToken.m10408a();
        String str = a != null ? a.mo11466b() : null;
        if (str == null || !str.equals(m10758f())) {
            abg.m381b((Context) this.f8234b.mo11695b());
            mo11741a("access_token", "0");
        } else {
            bundle.putString("access_token", str);
            mo11741a("access_token", "1");
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public String mo11626c() {
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d_ */
    public abstract AccessTokenSource mo11627d_();
}
