package com.facebook.login.widget;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint.FontMetrics;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import com.facebook.AccessToken;
import com.facebook.FacebookButtonBase;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.CallbackManagerImpl.RequestCodeOffset;
import com.facebook.internal.LoginAuthorizationType;
import com.facebook.login.DefaultAudience;
import com.facebook.login.LoginBehavior;
import com.facebook.login.widget.ToolTipPopup.Style;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class LoginButton extends FacebookButtonBase {

    /* renamed from: a */
    private static final String f8244a = LoginButton.class.getName();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f8245b;

    /* renamed from: c */
    private String f8246c;

    /* renamed from: d */
    private String f8247d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C2047a f8248e = new C2047a();
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f8249f = "fb_login_view_usage";

    /* renamed from: g */
    private boolean f8250g;

    /* renamed from: h */
    private Style f8251h = Style.BLUE;

    /* renamed from: i */
    private ToolTipMode f8252i;

    /* renamed from: j */
    private long f8253j = 6000;

    /* renamed from: k */
    private ToolTipPopup f8254k;

    /* renamed from: l */
    private AccessTokenTracker f8255l;

    /* renamed from: m */
    private abo f8256m;

    public enum ToolTipMode {
        AUTOMATIC("automatic", 0),
        DISPLAY_ALWAYS("display_always", 1),
        NEVER_DISPLAY("never_display", 2);
        

        /* renamed from: d */
        public static ToolTipMode f8266d;

        /* renamed from: e */
        private String f8268e;

        /* renamed from: f */
        private int f8269f;

        static {
            f8266d = AUTOMATIC;
        }

        private ToolTipMode(String str, int i) {
            this.f8268e = str;
            this.f8269f = i;
        }

        /* renamed from: a */
        public static ToolTipMode m10795a(int i) {
            ToolTipMode[] values;
            for (ToolTipMode toolTipMode : values()) {
                if (toolTipMode.mo11784a() == i) {
                    return toolTipMode;
                }
            }
            return null;
        }

        /* renamed from: a */
        public int mo11784a() {
            return this.f8269f;
        }

        public String toString() {
            return this.f8268e;
        }
    }

    /* renamed from: com.facebook.login.widget.LoginButton$a */
    static class C2047a {

        /* renamed from: a */
        private DefaultAudience f8270a = DefaultAudience.FRIENDS;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public List<String> f8271b = Collections.emptyList();
        /* access modifiers changed from: private */

        /* renamed from: c */
        public LoginAuthorizationType f8272c = null;

        /* renamed from: d */
        private LoginBehavior f8273d = LoginBehavior.NATIVE_WITH_FALLBACK;

        C2047a() {
        }

        /* renamed from: a */
        public DefaultAudience mo11786a() {
            return this.f8270a;
        }

        /* renamed from: a */
        public void mo11787a(DefaultAudience defaultAudience) {
            this.f8270a = defaultAudience;
        }

        /* renamed from: a */
        public void mo11788a(LoginBehavior loginBehavior) {
            this.f8273d = loginBehavior;
        }

        /* renamed from: a */
        public void mo11789a(List<String> list) {
            if (LoginAuthorizationType.PUBLISH.equals(this.f8272c)) {
                throw new UnsupportedOperationException("Cannot call setReadPermissions after setPublishPermissions has been called.");
            }
            this.f8271b = list;
            this.f8272c = LoginAuthorizationType.READ;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public List<String> mo11790b() {
            return this.f8271b;
        }

        /* renamed from: b */
        public void mo11791b(List<String> list) {
            if (LoginAuthorizationType.READ.equals(this.f8272c)) {
                throw new UnsupportedOperationException("Cannot call setPublishPermissions after setReadPermissions has been called.");
            } else if (abg.m375a((Collection<T>) list)) {
                throw new IllegalArgumentException("Permissions for publish actions cannot be null or empty.");
            } else {
                this.f8271b = list;
                this.f8272c = LoginAuthorizationType.PUBLISH;
            }
        }

        /* renamed from: c */
        public LoginBehavior mo11792c() {
            return this.f8273d;
        }
    }

    /* renamed from: com.facebook.login.widget.LoginButton$b */
    public class C2048b implements OnClickListener {
        protected C2048b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abo mo11759a() {
            abo c = abo.m502c();
            c.mo258a(LoginButton.this.getDefaultAudience());
            c.mo259a(LoginButton.this.getLoginBehavior());
            return c;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo11793a(Context context) {
            String string;
            final abo a = mo11759a();
            if (LoginButton.this.f8245b) {
                String string2 = LoginButton.this.getResources().getString(C3129f.com_facebook_loginview_log_out_action);
                String string3 = LoginButton.this.getResources().getString(C3129f.com_facebook_loginview_cancel_action);
                Profile a2 = Profile.m10546a();
                if (a2 == null || a2.mo11589c() == null) {
                    string = LoginButton.this.getResources().getString(C3129f.com_facebook_loginview_logged_in_using_facebook);
                } else {
                    string = String.format(LoginButton.this.getResources().getString(C3129f.com_facebook_loginview_logged_in_as), new Object[]{a2.mo11589c()});
                }
                Builder builder = new Builder(context);
                builder.setMessage(string).setCancelable(true).setPositiveButton(string2, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        a.mo270d();
                    }
                }).setNegativeButton(string3, null);
                builder.create().show();
                return;
            }
            a.mo270d();
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void mo11794b() {
            abo a = mo11759a();
            if (LoginAuthorizationType.PUBLISH.equals(LoginButton.this.f8248e.f8272c)) {
                if (LoginButton.this.getFragment() != null) {
                    a.mo269b(LoginButton.this.getFragment(), (Collection<String>) LoginButton.this.f8248e.f8271b);
                } else if (LoginButton.this.getNativeFragment() != null) {
                    a.mo268b(LoginButton.this.getNativeFragment(), (Collection<String>) LoginButton.this.f8248e.f8271b);
                } else {
                    a.mo267b(LoginButton.this.getActivity(), (Collection<String>) LoginButton.this.f8248e.f8271b);
                }
            } else if (LoginButton.this.getFragment() != null) {
                a.mo263a(LoginButton.this.getFragment(), (Collection<String>) LoginButton.this.f8248e.f8271b);
            } else if (LoginButton.this.getNativeFragment() != null) {
                a.mo262a(LoginButton.this.getNativeFragment(), (Collection<String>) LoginButton.this.f8248e.f8271b);
            } else {
                a.mo261a(LoginButton.this.getActivity(), (Collection<String>) LoginButton.this.f8248e.f8271b);
            }
        }

        public void onClick(View view) {
            LoginButton.this.mo11503a(view);
            AccessToken a = AccessToken.m10408a();
            if (a != null) {
                mo11793a(LoginButton.this.getContext());
            } else {
                mo11794b();
            }
            AppEventsLogger a2 = AppEventsLogger.m10567a(LoginButton.this.getContext());
            Bundle bundle = new Bundle();
            bundle.putInt("logging_in", a != null ? 0 : 1);
            a2.mo11609a(LoginButton.this.f8249f, (Double) null, bundle);
        }
    }

    public LoginButton(Context context) {
        super(context, null, 0, 0, "fb_login_button_create", "fb_login_button_did_tap");
    }

    public LoginButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0, 0, "fb_login_button_create", "fb_login_button_did_tap");
    }

    public LoginButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, 0, "fb_login_button_create", "fb_login_button_did_tap");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10778a(aap aap) {
        if (aap != null && aap.mo57c() && getVisibility() == 0) {
            m10784b(aap.mo56b());
        }
    }

    /* renamed from: b */
    private void m10781b() {
        switch (this.f8252i) {
            case AUTOMATIC:
                final String a = abg.m344a(getContext());
                FacebookSdk.m17301d().execute(new Runnable() {
                    public void run() {
                        final aap a = aaq.m117a(a, false);
                        LoginButton.this.getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                LoginButton.this.m10778a(a);
                            }
                        });
                    }
                });
                return;
            case DISPLAY_ALWAYS:
                m10784b(getResources().getString(C3129f.com_facebook_tooltip_default));
                return;
            default:
                return;
        }
    }

    /* renamed from: b */
    private void m10782b(Context context, AttributeSet attributeSet, int i, int i2) {
        this.f8252i = ToolTipMode.f8266d;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, C3131h.com_facebook_login_view, i, i2);
        try {
            this.f8245b = obtainStyledAttributes.getBoolean(C3131h.com_facebook_login_view_com_facebook_confirm_logout, true);
            this.f8246c = obtainStyledAttributes.getString(C3131h.com_facebook_login_view_com_facebook_login_text);
            this.f8247d = obtainStyledAttributes.getString(C3131h.com_facebook_login_view_com_facebook_logout_text);
            this.f8252i = ToolTipMode.m10795a(obtainStyledAttributes.getInt(C3131h.com_facebook_login_view_com_facebook_tooltip_mode, ToolTipMode.f8266d.mo11784a()));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: b */
    private void m10784b(String str) {
        this.f8254k = new ToolTipPopup(str, this);
        this.f8254k.mo11814a(this.f8251h);
        this.f8254k.mo11813a(this.f8253j);
        this.f8254k.mo11812a();
    }

    /* renamed from: c */
    private int m10785c(String str) {
        return getCompoundPaddingLeft() + getCompoundDrawablePadding() + mo11501a(str) + getCompoundPaddingRight();
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m10787c() {
        Resources resources = getResources();
        if (!isInEditMode() && AccessToken.m10408a() != null) {
            setText(this.f8247d != null ? this.f8247d : resources.getString(C3129f.com_facebook_loginview_log_out_button));
        } else if (this.f8246c != null) {
            setText(this.f8246c);
        } else {
            String string = resources.getString(C3129f.com_facebook_loginview_log_in_button_long);
            int width = getWidth();
            if (width != 0 && m10785c(string) > width) {
                string = resources.getString(C3129f.com_facebook_loginview_log_in_button);
            }
            setText(string);
        }
    }

    /* renamed from: a */
    public void mo11760a() {
        if (this.f8254k != null) {
            this.f8254k.mo11815b();
            this.f8254k = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11502a(Context context, AttributeSet attributeSet, int i, int i2) {
        super.mo11502a(context, attributeSet, i, i2);
        setInternalOnClickListener(getNewLoginClickListener());
        m10782b(context, attributeSet, i, i2);
        if (isInEditMode()) {
            setBackgroundColor(getResources().getColor(C3124a.com_facebook_blue));
            this.f8246c = "Log in with Facebook";
        } else {
            this.f8255l = new AccessTokenTracker() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public void mo289a(AccessToken accessToken, AccessToken accessToken2) {
                    LoginButton.this.m10787c();
                }
            };
        }
        m10787c();
    }

    public DefaultAudience getDefaultAudience() {
        return this.f8248e.mo11786a();
    }

    /* access modifiers changed from: protected */
    public int getDefaultRequestCode() {
        return RequestCodeOffset.Login.mo11620a();
    }

    /* access modifiers changed from: protected */
    public int getDefaultStyleResource() {
        return C3130g.com_facebook_loginview_default_style;
    }

    public LoginBehavior getLoginBehavior() {
        return this.f8248e.mo11792c();
    }

    /* access modifiers changed from: 0000 */
    public abo getLoginManager() {
        if (this.f8256m == null) {
            this.f8256m = abo.m502c();
        }
        return this.f8256m;
    }

    /* access modifiers changed from: protected */
    public C2048b getNewLoginClickListener() {
        return new C2048b();
    }

    /* access modifiers changed from: 0000 */
    public List<String> getPermissions() {
        return this.f8248e.mo11790b();
    }

    public long getToolTipDisplayTime() {
        return this.f8253j;
    }

    public ToolTipMode getToolTipMode() {
        return this.f8252i;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f8255l != null && !this.f8255l.mo16938c()) {
            this.f8255l.mo16936a();
            m10787c();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f8255l != null) {
            this.f8255l.mo16937b();
        }
        mo11760a();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f8250g && !isInEditMode()) {
            this.f8250g = true;
            m10781b();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        m10787c();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        FontMetrics fontMetrics = getPaint().getFontMetrics();
        int compoundPaddingTop = getCompoundPaddingTop() + ((int) Math.ceil((double) (Math.abs(fontMetrics.top) + Math.abs(fontMetrics.bottom)))) + getCompoundPaddingBottom();
        Resources resources = getResources();
        String str = this.f8246c;
        if (str == null) {
            str = resources.getString(C3129f.com_facebook_loginview_log_in_button_long);
            int c = m10785c(str);
            if (resolveSize(c, i) < c) {
                str = resources.getString(C3129f.com_facebook_loginview_log_in_button);
            }
        }
        int c2 = m10785c(str);
        String str2 = this.f8247d;
        if (str2 == null) {
            str2 = resources.getString(C3129f.com_facebook_loginview_log_out_button);
        }
        setMeasuredDimension(resolveSize(Math.max(c2, m10785c(str2)), i), compoundPaddingTop);
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (i != 0) {
            mo11760a();
        }
    }

    public void setDefaultAudience(DefaultAudience defaultAudience) {
        this.f8248e.mo11787a(defaultAudience);
    }

    public void setLoginBehavior(LoginBehavior loginBehavior) {
        this.f8248e.mo11788a(loginBehavior);
    }

    /* access modifiers changed from: 0000 */
    public void setLoginManager(abo abo) {
        this.f8256m = abo;
    }

    /* access modifiers changed from: 0000 */
    public void setProperties(C2047a aVar) {
        this.f8248e = aVar;
    }

    public void setPublishPermissions(List<String> list) {
        this.f8248e.mo11791b(list);
    }

    public void setPublishPermissions(String... strArr) {
        this.f8248e.mo11791b(Arrays.asList(strArr));
    }

    public void setReadPermissions(List<String> list) {
        this.f8248e.mo11789a(list);
    }

    public void setReadPermissions(String... strArr) {
        this.f8248e.mo11789a(Arrays.asList(strArr));
    }

    public void setToolTipDisplayTime(long j) {
        this.f8253j = j;
    }

    public void setToolTipMode(ToolTipMode toolTipMode) {
        this.f8252i = toolTipMode;
    }

    public void setToolTipStyle(Style style) {
        this.f8251h = style;
    }
}
