package com.facebook.login.widget;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.lang.ref.WeakReference;

public class ToolTipPopup {

    /* renamed from: a */
    private final String f8289a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final WeakReference<View> f8290b;

    /* renamed from: c */
    private final Context f8291c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C2055a f8292d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public PopupWindow f8293e;

    /* renamed from: f */
    private Style f8294f = Style.BLUE;

    /* renamed from: g */
    private long f8295g = 6000;

    /* renamed from: h */
    private final OnScrollChangedListener f8296h = new OnScrollChangedListener() {
        public void onScrollChanged() {
            if (ToolTipPopup.this.f8290b.get() != null && ToolTipPopup.this.f8293e != null && ToolTipPopup.this.f8293e.isShowing()) {
                if (ToolTipPopup.this.f8293e.isAboveAnchor()) {
                    ToolTipPopup.this.f8292d.mo11820b();
                } else {
                    ToolTipPopup.this.f8292d.mo11819a();
                }
            }
        }
    };

    public enum Style {
        BLUE,
        BLACK
    }

    /* renamed from: com.facebook.login.widget.ToolTipPopup$a */
    class C2055a extends FrameLayout {
        /* access modifiers changed from: private */

        /* renamed from: b */
        public ImageView f8304b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public ImageView f8305c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public View f8306d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public ImageView f8307e;

        public C2055a(Context context) {
            super(context);
            m10834c();
        }

        /* renamed from: c */
        private void m10834c() {
            LayoutInflater.from(getContext()).inflate(C3128e.com_facebook_tooltip_bubble, this);
            this.f8304b = (ImageView) findViewById(C3127d.com_facebook_tooltip_bubble_view_top_pointer);
            this.f8305c = (ImageView) findViewById(C3127d.com_facebook_tooltip_bubble_view_bottom_pointer);
            this.f8306d = findViewById(C3127d.com_facebook_body_frame);
            this.f8307e = (ImageView) findViewById(C3127d.com_facebook_button_xout);
        }

        /* renamed from: a */
        public void mo11819a() {
            this.f8304b.setVisibility(0);
            this.f8305c.setVisibility(4);
        }

        /* renamed from: b */
        public void mo11820b() {
            this.f8304b.setVisibility(4);
            this.f8305c.setVisibility(0);
        }
    }

    public ToolTipPopup(String str, View view) {
        this.f8289a = str;
        this.f8290b = new WeakReference<>(view);
        this.f8291c = view.getContext();
    }

    /* renamed from: c */
    private void m10824c() {
        if (this.f8293e != null && this.f8293e.isShowing()) {
            if (this.f8293e.isAboveAnchor()) {
                this.f8292d.mo11820b();
            } else {
                this.f8292d.mo11819a();
            }
        }
    }

    /* renamed from: d */
    private void m10825d() {
        m10826e();
        if (this.f8290b.get() != null) {
            ((View) this.f8290b.get()).getViewTreeObserver().addOnScrollChangedListener(this.f8296h);
        }
    }

    /* renamed from: e */
    private void m10826e() {
        if (this.f8290b.get() != null) {
            ((View) this.f8290b.get()).getViewTreeObserver().removeOnScrollChangedListener(this.f8296h);
        }
    }

    /* renamed from: a */
    public void mo11812a() {
        if (this.f8290b.get() != null) {
            this.f8292d = new C2055a(this.f8291c);
            ((TextView) this.f8292d.findViewById(C3127d.com_facebook_tooltip_bubble_view_text_body)).setText(this.f8289a);
            if (this.f8294f == Style.BLUE) {
                this.f8292d.f8306d.setBackgroundResource(C3126c.com_facebook_tooltip_blue_background);
                this.f8292d.f8305c.setImageResource(C3126c.com_facebook_tooltip_blue_bottomnub);
                this.f8292d.f8304b.setImageResource(C3126c.com_facebook_tooltip_blue_topnub);
                this.f8292d.f8307e.setImageResource(C3126c.com_facebook_tooltip_blue_xout);
            } else {
                this.f8292d.f8306d.setBackgroundResource(C3126c.com_facebook_tooltip_black_background);
                this.f8292d.f8305c.setImageResource(C3126c.com_facebook_tooltip_black_bottomnub);
                this.f8292d.f8304b.setImageResource(C3126c.com_facebook_tooltip_black_topnub);
                this.f8292d.f8307e.setImageResource(C3126c.com_facebook_tooltip_black_xout);
            }
            View decorView = ((Activity) this.f8291c).getWindow().getDecorView();
            int width = decorView.getWidth();
            int height = decorView.getHeight();
            m10825d();
            this.f8292d.measure(MeasureSpec.makeMeasureSpec(width, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(height, Integer.MIN_VALUE));
            this.f8293e = new PopupWindow(this.f8292d, this.f8292d.getMeasuredWidth(), this.f8292d.getMeasuredHeight());
            this.f8293e.showAsDropDown((View) this.f8290b.get());
            m10824c();
            if (this.f8295g > 0) {
                this.f8292d.postDelayed(new Runnable() {
                    public void run() {
                        ToolTipPopup.this.mo11815b();
                    }
                }, this.f8295g);
            }
            this.f8293e.setTouchable(true);
            this.f8292d.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    ToolTipPopup.this.mo11815b();
                }
            });
        }
    }

    /* renamed from: a */
    public void mo11813a(long j) {
        this.f8295g = j;
    }

    /* renamed from: a */
    public void mo11814a(Style style) {
        this.f8294f = style;
    }

    /* renamed from: b */
    public void mo11815b() {
        m10826e();
        if (this.f8293e != null) {
            this.f8293e.dismiss();
        }
    }
}
