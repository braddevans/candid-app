package com.facebook.login.widget;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import com.facebook.login.LoginBehavior;
import com.facebook.login.widget.LoginButton.C2048b;

public class DeviceLoginButton extends LoginButton {

    /* renamed from: a */
    private Uri f8242a;

    /* renamed from: com.facebook.login.widget.DeviceLoginButton$a */
    class C2042a extends C2048b {
        private C2042a() {
            super();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abo mo11759a() {
            abk a = abk.m468a();
            a.mo258a(DeviceLoginButton.this.getDefaultAudience());
            a.mo259a(LoginBehavior.DEVICE_AUTH);
            a.mo238a(DeviceLoginButton.this.getDeviceRedirectUri());
            return a;
        }
    }

    public DeviceLoginButton(Context context) {
        super(context);
    }

    public DeviceLoginButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DeviceLoginButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public Uri getDeviceRedirectUri() {
        return this.f8242a;
    }

    /* access modifiers changed from: protected */
    public C2048b getNewLoginClickListener() {
        return new C2042a();
    }

    public void setDeviceRedirectUri(Uri uri) {
        this.f8242a = uri;
    }
}
