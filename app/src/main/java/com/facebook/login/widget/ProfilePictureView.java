package com.facebook.login.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.facebook.FacebookException;
import com.facebook.LoggingBehavior;

public class ProfilePictureView extends FrameLayout {

    /* renamed from: a */
    public static final String f8277a = ProfilePictureView.class.getSimpleName();

    /* renamed from: b */
    private String f8278b;

    /* renamed from: c */
    private int f8279c = 0;

    /* renamed from: d */
    private int f8280d = 0;

    /* renamed from: e */
    private boolean f8281e = true;

    /* renamed from: f */
    private Bitmap f8282f;

    /* renamed from: g */
    private ImageView f8283g;

    /* renamed from: h */
    private int f8284h = -1;

    /* renamed from: i */
    private aau f8285i;

    /* renamed from: j */
    private C2051a f8286j;

    /* renamed from: k */
    private Bitmap f8287k = null;

    /* renamed from: com.facebook.login.widget.ProfilePictureView$a */
    public interface C2051a {
        /* renamed from: a */
        void mo11811a(FacebookException facebookException);
    }

    public ProfilePictureView(Context context) {
        super(context);
        m10810a(context);
    }

    public ProfilePictureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m10810a(context);
        m10811a(attributeSet);
    }

    public ProfilePictureView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m10810a(context);
        m10811a(attributeSet);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10809a(aav aav) {
        if (aav.mo122a() == this.f8285i) {
            this.f8285i = null;
            Bitmap c = aav.mo124c();
            Exception b = aav.mo123b();
            if (b != null) {
                C2051a aVar = this.f8286j;
                if (aVar != null) {
                    aVar.mo11811a(new FacebookException("Error in downloading profile picture for profileId: " + getProfileId(), (Throwable) b));
                } else {
                    aaz.m196a(LoggingBehavior.REQUESTS, 6, f8277a, b.toString());
                }
            } else if (c != null) {
                setImageBitmap(c);
                if (aav.mo125d()) {
                    m10815b(false);
                }
            }
        }
    }

    /* renamed from: a */
    private void m10810a(Context context) {
        removeAllViews();
        this.f8283g = new ImageView(context);
        this.f8283g.setLayoutParams(new LayoutParams(-1, -1));
        this.f8283g.setScaleType(ScaleType.CENTER_INSIDE);
        addView(this.f8283g);
    }

    /* renamed from: a */
    private void m10811a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, C3131h.com_facebook_profile_picture_view);
        setPresetSize(obtainStyledAttributes.getInt(C3131h.com_facebook_profile_picture_view_com_facebook_preset_size, -1));
        this.f8281e = obtainStyledAttributes.getBoolean(C3131h.com_facebook_profile_picture_view_com_facebook_is_cropped, true);
        obtainStyledAttributes.recycle();
    }

    /* renamed from: a */
    private void m10813a(boolean z) {
        boolean c = m10817c();
        if (this.f8278b == null || this.f8278b.length() == 0 || (this.f8280d == 0 && this.f8279c == 0)) {
            m10814b();
        } else if (c || z) {
            m10815b(true);
        }
    }

    /* renamed from: b */
    private void m10814b() {
        if (this.f8285i != null) {
            aat.m165b(this.f8285i);
        }
        if (this.f8287k == null) {
            setImageBitmap(BitmapFactory.decodeResource(getResources(), mo11797a() ? C3126c.com_facebook_profile_picture_blank_square : C3126c.com_facebook_profile_picture_blank_portrait));
            return;
        }
        m10817c();
        setImageBitmap(Bitmap.createScaledBitmap(this.f8287k, this.f8280d, this.f8279c, false));
    }

    /* renamed from: b */
    private void m10815b(boolean z) {
        aau a = new C0038a(getContext(), aau.m166a(this.f8278b, this.f8280d, this.f8279c)).mo119a(z).mo118a((Object) this).mo117a((C0039b) new C0039b() {
            /* renamed from: a */
            public void mo121a(aav aav) {
                ProfilePictureView.this.m10809a(aav);
            }
        }).mo120a();
        if (this.f8285i != null) {
            aat.m165b(this.f8285i);
        }
        this.f8285i = a;
        aat.m159a(a);
    }

    /* renamed from: c */
    private int m10816c(boolean z) {
        int i;
        switch (this.f8284h) {
            case -4:
                i = C3125b.com_facebook_profilepictureview_preset_size_large;
                break;
            case -3:
                i = C3125b.com_facebook_profilepictureview_preset_size_normal;
                break;
            case -2:
                i = C3125b.com_facebook_profilepictureview_preset_size_small;
                break;
            case -1:
                if (z) {
                    i = C3125b.com_facebook_profilepictureview_preset_size_normal;
                    break;
                } else {
                    return 0;
                }
            default:
                return 0;
        }
        return getResources().getDimensionPixelSize(i);
    }

    /* renamed from: c */
    private boolean m10817c() {
        boolean z = true;
        int height = getHeight();
        int width = getWidth();
        if (width < 1 || height < 1) {
            return false;
        }
        int c = m10816c(false);
        if (c != 0) {
            width = c;
            height = c;
        }
        if (width <= height) {
            height = mo11797a() ? width : 0;
        } else {
            width = mo11797a() ? height : 0;
        }
        if (width == this.f8280d && height == this.f8279c) {
            z = false;
        }
        this.f8280d = width;
        this.f8279c = height;
        return z;
    }

    private void setImageBitmap(Bitmap bitmap) {
        if (this.f8283g != null && bitmap != null) {
            this.f8282f = bitmap;
            this.f8283g.setImageBitmap(bitmap);
        }
    }

    /* renamed from: a */
    public final boolean mo11797a() {
        return this.f8281e;
    }

    public final C2051a getOnErrorListener() {
        return this.f8286j;
    }

    public final int getPresetSize() {
        return this.f8284h;
    }

    public final String getProfileId() {
        return this.f8278b;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f8285i = null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        m10813a(false);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        boolean z = false;
        int size = MeasureSpec.getSize(i2);
        int size2 = MeasureSpec.getSize(i);
        if (MeasureSpec.getMode(i2) != 1073741824 && layoutParams.height == -2) {
            size = m10816c(true);
            i2 = MeasureSpec.makeMeasureSpec(size, 1073741824);
            z = true;
        }
        if (MeasureSpec.getMode(i) != 1073741824 && layoutParams.width == -2) {
            size2 = m10816c(true);
            i = MeasureSpec.makeMeasureSpec(size2, 1073741824);
            z = true;
        }
        if (z) {
            setMeasuredDimension(size2, size);
            measureChildren(i, i2);
            return;
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable.getClass() != Bundle.class) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        Bundle bundle = (Bundle) parcelable;
        super.onRestoreInstanceState(bundle.getParcelable("ProfilePictureView_superState"));
        this.f8278b = bundle.getString("ProfilePictureView_profileId");
        this.f8284h = bundle.getInt("ProfilePictureView_presetSize");
        this.f8281e = bundle.getBoolean("ProfilePictureView_isCropped");
        this.f8280d = bundle.getInt("ProfilePictureView_width");
        this.f8279c = bundle.getInt("ProfilePictureView_height");
        setImageBitmap((Bitmap) bundle.getParcelable("ProfilePictureView_bitmap"));
        if (bundle.getBoolean("ProfilePictureView_refresh")) {
            m10813a(true);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable("ProfilePictureView_superState", onSaveInstanceState);
        bundle.putString("ProfilePictureView_profileId", this.f8278b);
        bundle.putInt("ProfilePictureView_presetSize", this.f8284h);
        bundle.putBoolean("ProfilePictureView_isCropped", this.f8281e);
        bundle.putParcelable("ProfilePictureView_bitmap", this.f8282f);
        bundle.putInt("ProfilePictureView_width", this.f8280d);
        bundle.putInt("ProfilePictureView_height", this.f8279c);
        bundle.putBoolean("ProfilePictureView_refresh", this.f8285i != null);
        return bundle;
    }

    public final void setCropped(boolean z) {
        this.f8281e = z;
        m10813a(false);
    }

    public final void setDefaultProfilePicture(Bitmap bitmap) {
        this.f8287k = bitmap;
    }

    public final void setOnErrorListener(C2051a aVar) {
        this.f8286j = aVar;
    }

    public final void setPresetSize(int i) {
        switch (i) {
            case -4:
            case -3:
            case -2:
            case -1:
                this.f8284h = i;
                requestLayout();
                return;
            default:
                throw new IllegalArgumentException("Must use a predefined preset size");
        }
    }

    public final void setProfileId(String str) {
        boolean z = false;
        if (abg.m374a(this.f8278b) || !this.f8278b.equalsIgnoreCase(str)) {
            m10814b();
            z = true;
        }
        this.f8278b = str;
        m10813a(z);
    }
}
