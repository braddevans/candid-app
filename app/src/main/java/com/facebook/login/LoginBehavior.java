package com.facebook.login;

public enum LoginBehavior {
    NATIVE_WITH_FALLBACK(true, true, true, false, true, true),
    NATIVE_ONLY(true, true, false, false, false, true),
    KATANA_ONLY(false, true, false, false, false, false),
    WEB_ONLY(false, false, true, false, true, false),
    WEB_VIEW_ONLY(false, false, true, false, false, false),
    DEVICE_AUTH(false, false, false, true, false, false);
    

    /* renamed from: g */
    private final boolean f8200g;

    /* renamed from: h */
    private final boolean f8201h;

    /* renamed from: i */
    private final boolean f8202i;

    /* renamed from: j */
    private final boolean f8203j;

    /* renamed from: k */
    private final boolean f8204k;

    /* renamed from: l */
    private final boolean f8205l;

    private LoginBehavior(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.f8200g = z;
        this.f8201h = z2;
        this.f8202i = z3;
        this.f8203j = z4;
        this.f8204k = z5;
        this.f8205l = z6;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11681a() {
        return this.f8200g;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public boolean mo11682b() {
        return this.f8201h;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public boolean mo11683c() {
        return this.f8202i;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public boolean mo11684d() {
        return this.f8203j;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public boolean mo11685e() {
        return this.f8204k;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public boolean mo11686f() {
        return this.f8205l;
    }
}
