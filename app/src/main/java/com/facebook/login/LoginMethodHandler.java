package com.facebook.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.AccessTokenSource;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginClient.Request;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

abstract class LoginMethodHandler implements Parcelable {

    /* renamed from: a */
    Map<String, String> f8233a;

    /* renamed from: b */
    protected LoginClient f8234b;

    LoginMethodHandler(Parcel parcel) {
        this.f8233a = abg.m358a(parcel);
    }

    LoginMethodHandler(LoginClient loginClient) {
        this.f8234b = loginClient;
    }

    /* renamed from: a */
    static AccessToken m10736a(Bundle bundle, AccessTokenSource accessTokenSource, String str) {
        Date a = abg.m355a(bundle, "com.facebook.platform.extra.EXPIRES_SECONDS_SINCE_EPOCH", new Date(0));
        ArrayList stringArrayList = bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS");
        String string = bundle.getString("com.facebook.platform.extra.ACCESS_TOKEN");
        if (abg.m374a(string)) {
            return null;
        }
        return new AccessToken(string, str, bundle.getString("com.facebook.platform.extra.USER_ID"), stringArrayList, null, accessTokenSource, a, new Date());
    }

    /* renamed from: a */
    public static AccessToken m10737a(Collection<String> collection, Bundle bundle, AccessTokenSource accessTokenSource, String str) throws FacebookException {
        Date a = abg.m355a(bundle, "expires_in", new Date());
        String string = bundle.getString("access_token");
        String string2 = bundle.getString("granted_scopes");
        if (!abg.m374a(string2)) {
            collection = new ArrayList<>(Arrays.asList(string2.split(",")));
        }
        String string3 = bundle.getString("denied_scopes");
        ArrayList arrayList = null;
        if (!abg.m374a(string3)) {
            arrayList = new ArrayList(Arrays.asList(string3.split(",")));
        }
        if (abg.m374a(string)) {
            return null;
        }
        return new AccessToken(string, str, m10738c(bundle.getString("signed_request")), collection, arrayList, accessTokenSource, a, new Date());
    }

    /* renamed from: c */
    private static String m10738c(String str) throws FacebookException {
        if (str == null || str.isEmpty()) {
            throw new FacebookException("Authorization response does not contain the signed_request");
        }
        try {
            String[] split = str.split("\\.");
            if (split.length == 2) {
                return new JSONObject(new String(Base64.decode(split[1], 0), "UTF-8")).getString("user_id");
            }
        } catch (UnsupportedEncodingException | JSONException e) {
        }
        throw new FacebookException("Failed to retrieve user_id from signed_request");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public abstract String mo11622a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String mo11739a(String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("0_auth_logger_id", str);
            jSONObject.put("3_method", mo11622a());
            mo11623a(jSONObject);
        } catch (JSONException e) {
            Log.w("LoginMethodHandler", "Error creating client state json: " + e.getMessage());
        }
        return jSONObject.toString();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo11740a(LoginClient loginClient) {
        if (this.f8234b != null) {
            throw new FacebookException("Can't set LoginClient if it is already set.");
        }
        this.f8234b = loginClient;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11741a(String str, Object obj) {
        if (this.f8233a == null) {
            this.f8233a = new HashMap();
        }
        this.f8233a.put(str, obj == null ? null : obj.toString());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo11623a(JSONObject jSONObject) throws JSONException {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11624a(int i, int i2, Intent intent) {
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public abstract boolean mo11625a(Request request);

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo11668b() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo11742b(String str) {
        String d = this.f8234b.mo11698c().mo11721d();
        AppEventsLogger a = AppEventsLogger.m10568a((Context) this.f8234b.mo11695b(), d);
        Bundle bundle = new Bundle();
        bundle.putString("fb_web_login_e2e", str);
        bundle.putLong("fb_web_login_switchback_time", System.currentTimeMillis());
        bundle.putString("app_id", d);
        a.mo11609a("fb_dialogs_web_login_dialog_complete", (Double) null, bundle);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public boolean mo11743d() {
        return false;
    }

    public void writeToParcel(Parcel parcel, int i) {
        abg.m362a(parcel, this.f8233a);
    }
}
