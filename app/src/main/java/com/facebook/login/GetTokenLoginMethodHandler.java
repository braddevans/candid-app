package com.facebook.login;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.facebook.AccessTokenSource;
import com.facebook.FacebookException;
import com.facebook.login.LoginClient.Request;
import com.facebook.login.LoginClient.Result;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class GetTokenLoginMethodHandler extends LoginMethodHandler {
    public static final Creator<GetTokenLoginMethodHandler> CREATOR = new Creator() {
        /* renamed from: a */
        public GetTokenLoginMethodHandler createFromParcel(Parcel parcel) {
            return new GetTokenLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public GetTokenLoginMethodHandler[] newArray(int i) {
            return new GetTokenLoginMethodHandler[i];
        }
    };

    /* renamed from: c */
    private abl f8187c;

    GetTokenLoginMethodHandler(Parcel parcel) {
        super(parcel);
    }

    GetTokenLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo11622a() {
        return "get_token";
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo11667a(Request request, Bundle bundle) {
        if (this.f8187c != null) {
            this.f8187c.mo176a((C0060a) null);
        }
        this.f8187c = null;
        this.f8234b.mo11709l();
        if (bundle != null) {
            ArrayList stringArrayList = bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS");
            Set<String> a = request.mo11715a();
            if (stringArrayList == null || (a != null && !stringArrayList.containsAll(a))) {
                HashSet hashSet = new HashSet();
                for (String str : a) {
                    if (!stringArrayList.contains(str)) {
                        hashSet.add(str);
                    }
                }
                if (!hashSet.isEmpty()) {
                    mo11741a("new_permissions", TextUtils.join(",", hashSet));
                }
                request.mo11717a((Set<String>) hashSet);
            } else {
                mo11670c(request, bundle);
                return;
            }
        }
        this.f8234b.mo11706i();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11625a(final Request request) {
        this.f8187c = new abl(this.f8234b.mo11695b(), request.mo11721d());
        if (!this.f8187c.mo179a()) {
            return false;
        }
        this.f8234b.mo11708k();
        this.f8187c.mo176a((C0060a) new C0060a() {
            /* renamed from: a */
            public void mo184a(Bundle bundle) {
                GetTokenLoginMethodHandler.this.mo11667a(request, bundle);
            }
        });
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo11668b() {
        if (this.f8187c != null) {
            this.f8187c.mo180b();
            this.f8187c.mo176a((C0060a) null);
            this.f8187c = null;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo11669b(Request request, Bundle bundle) {
        this.f8234b.mo11691a(Result.m10726a(this.f8234b.mo11698c(), m10736a(bundle, AccessTokenSource.FACEBOOK_APPLICATION_SERVICE, request.mo11721d())));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo11670c(final Request request, final Bundle bundle) {
        String string = bundle.getString("com.facebook.platform.extra.USER_ID");
        if (string == null || string.isEmpty()) {
            this.f8234b.mo11708k();
            abg.m365a(bundle.getString("com.facebook.platform.extra.ACCESS_TOKEN"), (C0063a) new C0063a() {
                /* renamed from: a */
                public void mo187a(FacebookException facebookException) {
                    GetTokenLoginMethodHandler.this.f8234b.mo11697b(Result.m10728a(GetTokenLoginMethodHandler.this.f8234b.mo11698c(), "Caught exception", facebookException.getMessage()));
                }

                /* renamed from: a */
                public void mo188a(JSONObject jSONObject) {
                    try {
                        bundle.putString("com.facebook.platform.extra.USER_ID", jSONObject.getString("id"));
                        GetTokenLoginMethodHandler.this.mo11669b(request, bundle);
                    } catch (JSONException e) {
                        GetTokenLoginMethodHandler.this.f8234b.mo11697b(Result.m10728a(GetTokenLoginMethodHandler.this.f8234b.mo11698c(), "Caught exception", e.getMessage()));
                    }
                }
            });
            return;
        }
        mo11669b(request, bundle);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
