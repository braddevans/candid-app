package com.facebook.login;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p001v4.app.DialogFragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.AccessToken;
import com.facebook.AccessTokenSource;
import com.facebook.FacebookActivity;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.C2004b;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.SmartLoginOption;
import com.facebook.login.LoginClient.Request;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class DeviceAuthDialog extends DialogFragment {

    /* renamed from: a */
    private ProgressBar f8158a;

    /* renamed from: b */
    private TextView f8159b;

    /* renamed from: c */
    private DeviceAuthMethodHandler f8160c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public AtomicBoolean f8161d = new AtomicBoolean();

    /* renamed from: e */
    private volatile GraphRequestAsyncTask f8162e;

    /* renamed from: f */
    private volatile ScheduledFuture f8163f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public volatile RequestState f8164g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Dialog f8165h;

    /* renamed from: i */
    private boolean f8166i = false;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public boolean f8167j = false;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public Request f8168k = null;

    static class RequestState implements Parcelable {
        public static final Creator<RequestState> CREATOR = new Creator<RequestState>() {
            /* renamed from: a */
            public RequestState createFromParcel(Parcel parcel) {
                return new RequestState(parcel);
            }

            /* renamed from: a */
            public RequestState[] newArray(int i) {
                return new RequestState[i];
            }
        };

        /* renamed from: a */
        private String f8182a;

        /* renamed from: b */
        private String f8183b;

        /* renamed from: c */
        private long f8184c;

        /* renamed from: d */
        private long f8185d;

        RequestState() {
        }

        protected RequestState(Parcel parcel) {
            this.f8182a = parcel.readString();
            this.f8183b = parcel.readString();
            this.f8184c = parcel.readLong();
            this.f8185d = parcel.readLong();
        }

        /* renamed from: a */
        public String mo11640a() {
            return this.f8182a;
        }

        /* renamed from: a */
        public void mo11641a(long j) {
            this.f8184c = j;
        }

        /* renamed from: a */
        public void mo11642a(String str) {
            this.f8182a = str;
        }

        /* renamed from: b */
        public String mo11643b() {
            return this.f8183b;
        }

        /* renamed from: b */
        public void mo11644b(long j) {
            this.f8185d = j;
        }

        /* renamed from: b */
        public void mo11645b(String str) {
            this.f8183b = str;
        }

        /* renamed from: c */
        public long mo11646c() {
            return this.f8184c;
        }

        /* renamed from: d */
        public boolean mo11647d() {
            return this.f8185d != 0 && (new Date().getTime() - this.f8185d) - (this.f8184c * 1000) < 0;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f8182a);
            parcel.writeString(this.f8183b);
            parcel.writeLong(this.f8184c);
            parcel.writeLong(this.f8185d);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public View m10610a(boolean z) {
        View inflate;
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        if (z) {
            inflate = layoutInflater.inflate(C3128e.com_facebook_smart_device_dialog_fragment, null);
            aap a = aaq.m115a(FacebookSdk.m17307j());
            if (a.mo62h() != null) {
                m10612a((TextView) inflate.findViewById(C3127d.com_facebook_smart_instructions_2), a.mo62h());
            }
            if (a.mo63i() != null) {
                m10612a((TextView) inflate.findViewById(C3127d.com_facebook_smart_instructions_1), a.mo63i());
            }
        } else {
            inflate = layoutInflater.inflate(C3128e.com_facebook_device_auth_dialog_fragment, null);
        }
        this.f8158a = (ProgressBar) inflate.findViewById(C3127d.progress_bar);
        this.f8159b = (TextView) inflate.findViewById(C3127d.confirmation_code);
        ((Button) inflate.findViewById(C3127d.cancel_button)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                DeviceAuthDialog.this.m10629d();
            }
        });
        ((TextView) inflate.findViewById(C3127d.com_facebook_device_auth_instructions)).setText(Html.fromHtml(getString(C3129f.com_facebook_device_auth_instructions)));
        return inflate;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10611a() {
        this.f8164g.mo11644b(new Date().getTime());
        this.f8162e = m10627c().mo11562j();
    }

    /* renamed from: a */
    private void m10612a(final TextView textView, String str) {
        aat.m159a(new C0038a(getContext(), Uri.parse(str)).mo117a((C0039b) new C0039b() {
            /* renamed from: a */
            public void mo121a(aav aav) {
                if (aav.mo124c() != null) {
                    textView.setCompoundDrawablesWithIntrinsicBounds(null, null, new BitmapDrawable(DeviceAuthDialog.this.getResources(), Bitmap.createScaledBitmap(aav.mo124c(), 24, 24, false)), null);
                }
            }
        }).mo120a());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10613a(FacebookException facebookException) {
        if (this.f8161d.compareAndSet(false, true)) {
            if (this.f8164g != null) {
                aae.m7b(this.f8164g.mo11640a());
            }
            this.f8160c.mo11654a((Exception) facebookException);
            this.f8165h.dismiss();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10614a(RequestState requestState) {
        this.f8164g = requestState;
        this.f8159b.setText(requestState.mo11640a());
        this.f8159b.setVisibility(0);
        this.f8158a.setVisibility(8);
        if (!this.f8167j && aae.m6a(requestState.mo11640a())) {
            AppEventsLogger.m10567a(getContext()).mo11609a("fb_smart_login_service", (Double) null, (Bundle) null);
        }
        if (requestState.mo11647d()) {
            m10624b();
        } else {
            m10611a();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10621a(final String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id,permissions,name");
        new GraphRequest(new AccessToken(str, FacebookSdk.m17307j(), "0", null, null, null, null, null), "me", bundle, HttpMethod.GET, new C2004b() {
            public void onCompleted(GraphResponse zlVar) {
                if (!DeviceAuthDialog.this.f8161d.get()) {
                    if (zlVar.mo16977a() != null) {
                        DeviceAuthDialog.this.m10613a(zlVar.mo16977a().mo11537g());
                        return;
                    }
                    try {
                        JSONObject b = zlVar.mo16978b();
                        String string = b.getString("id");
                        C0065c a = abg.m339a(b);
                        String string2 = b.getString("name");
                        aae.m7b(DeviceAuthDialog.this.f8164g.mo11640a());
                        if (!aaq.m115a(FacebookSdk.m17307j()).mo59e().contains(SmartLoginOption.RequireConfirm) || DeviceAuthDialog.this.f8167j) {
                            DeviceAuthDialog.this.m10622a(string, a, str);
                            return;
                        }
                        DeviceAuthDialog.this.f8167j = true;
                        DeviceAuthDialog.this.m10623a(string, a, str, string2);
                    } catch (JSONException e) {
                        DeviceAuthDialog.this.m10613a(new FacebookException((Throwable) e));
                    }
                }
            }
        }).mo11562j();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10622a(String str, C0065c cVar, String str2) {
        this.f8160c.mo11655a(str2, FacebookSdk.m17307j(), str, cVar.mo190a(), cVar.mo191b(), AccessTokenSource.DEVICE_AUTH, null, null);
        this.f8165h.dismiss();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m10623a(final String str, final C0065c cVar, final String str2, String str3) {
        String string = getResources().getString(C3129f.com_facebook_smart_login_confirmation_title);
        String string2 = getResources().getString(C3129f.com_facebook_smart_login_confirmation_continue_as);
        String string3 = getResources().getString(C3129f.com_facebook_smart_login_confirmation_cancel);
        String format = String.format(string2, new Object[]{str3});
        Builder builder = new Builder(getContext());
        builder.setMessage(string).setCancelable(true).setNegativeButton(format, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                DeviceAuthDialog.this.m10622a(str, cVar, str2);
            }
        }).setPositiveButton(string3, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                DeviceAuthDialog.this.f8165h.setContentView(DeviceAuthDialog.this.m10610a(false));
                DeviceAuthDialog.this.mo11635a(DeviceAuthDialog.this.f8168k);
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m10624b() {
        this.f8163f = DeviceAuthMethodHandler.m10648c().schedule(new Runnable() {
            public void run() {
                DeviceAuthDialog.this.m10611a();
            }
        }, this.f8164g.mo11646c(), TimeUnit.SECONDS);
    }

    /* renamed from: c */
    private GraphRequest m10627c() {
        Bundle bundle = new Bundle();
        bundle.putString("code", this.f8164g.mo11643b());
        return new GraphRequest(null, "device/login_status", bundle, HttpMethod.POST, new C2004b() {
            public void onCompleted(GraphResponse zlVar) {
                if (!DeviceAuthDialog.this.f8161d.get()) {
                    FacebookRequestError a = zlVar.mo16977a();
                    if (a != null) {
                        switch (a.mo11532c()) {
                            case 1349152:
                            case 1349173:
                                DeviceAuthDialog.this.m10629d();
                                return;
                            case 1349172:
                            case 1349174:
                                DeviceAuthDialog.this.m10624b();
                                return;
                            default:
                                DeviceAuthDialog.this.m10613a(zlVar.mo16977a().mo11537g());
                                return;
                        }
                    } else {
                        try {
                            DeviceAuthDialog.this.m10621a(zlVar.mo16978b().getString("access_token"));
                        } catch (JSONException e) {
                            DeviceAuthDialog.this.m10613a(new FacebookException((Throwable) e));
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m10629d() {
        if (this.f8161d.compareAndSet(false, true)) {
            aae.m7b(this.f8164g.mo11640a());
            if (this.f8160c != null) {
                this.f8160c.mo11657e_();
            }
            this.f8165h.dismiss();
        }
    }

    /* renamed from: a */
    public void mo11635a(Request request) {
        this.f8168k = request;
        Bundle bundle = new Bundle();
        bundle.putString("scope", TextUtils.join(",", request.mo11715a()));
        String g = request.mo11725g();
        if (g != null) {
            bundle.putString("redirect_uri", g);
        }
        bundle.putString("access_token", abh.m409b() + "|" + abh.m413c());
        bundle.putString("device_info", aae.m5a());
        new GraphRequest(null, "device/login", bundle, HttpMethod.POST, new C2004b() {
            public void onCompleted(GraphResponse zlVar) {
                if (zlVar.mo16977a() != null) {
                    DeviceAuthDialog.this.m10613a(zlVar.mo16977a().mo11537g());
                    return;
                }
                JSONObject b = zlVar.mo16978b();
                RequestState requestState = new RequestState();
                try {
                    requestState.mo11642a(b.getString("user_code"));
                    requestState.mo11645b(b.getString("code"));
                    requestState.mo11641a(b.getLong("interval"));
                    DeviceAuthDialog.this.m10614a(requestState);
                } catch (JSONException e) {
                    DeviceAuthDialog.this.m10613a(new FacebookException((Throwable) e));
                }
            }
        }).mo11562j();
    }

    public Dialog onCreateDialog(Bundle bundle) {
        this.f8165h = new Dialog(getActivity(), C3130g.com_facebook_auth_dialog);
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        this.f8165h.setContentView(m10610a(aae.m8b() && !this.f8167j));
        return this.f8165h;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f8160c = (DeviceAuthMethodHandler) ((abm) ((FacebookActivity) getActivity()).mo11497b()).mo241b().mo11704g();
        if (bundle != null) {
            RequestState requestState = (RequestState) bundle.getParcelable("request_state");
            if (requestState != null) {
                m10614a(requestState);
            }
        }
        return onCreateView;
    }

    public void onDestroy() {
        this.f8166i = true;
        this.f8161d.set(true);
        super.onDestroy();
        if (this.f8162e != null) {
            this.f8162e.cancel(true);
        }
        if (this.f8163f != null) {
            this.f8163f.cancel(true);
        }
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (!this.f8166i) {
            m10629d();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.f8164g != null) {
            bundle.putParcelable("request_state", this.f8164g);
        }
    }
}
