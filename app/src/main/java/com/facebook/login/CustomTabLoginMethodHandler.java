package com.facebook.login;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.p001v4.app.FragmentActivity;
import com.facebook.AccessTokenSource;
import com.facebook.CustomTabMainActivity;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookServiceException;
import com.facebook.login.LoginClient.Request;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomTabLoginMethodHandler extends WebLoginMethodHandler {
    public static final Creator<CustomTabLoginMethodHandler> CREATOR = new Creator() {
        /* renamed from: a */
        public CustomTabLoginMethodHandler createFromParcel(Parcel parcel) {
            return new CustomTabLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public CustomTabLoginMethodHandler[] newArray(int i) {
            return new CustomTabLoginMethodHandler[i];
        }
    };

    /* renamed from: c */
    private static final String[] f8149c = {"com.android.chrome", "com.chrome.beta", "com.chrome.dev"};

    /* renamed from: d */
    private String f8150d;

    /* renamed from: e */
    private String f8151e;

    CustomTabLoginMethodHandler(Parcel parcel) {
        super(parcel);
        this.f8151e = parcel.readString();
    }

    CustomTabLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
        this.f8151e = abg.m343a(20);
    }

    /* renamed from: a */
    private void m10595a(String str, Request request) {
        if (str != null && str.startsWith(CustomTabMainActivity.m10430a())) {
            Uri parse = Uri.parse(str);
            Bundle c = abg.m384c(parse.getQuery());
            c.putAll(abg.m384c(parse.getFragment()));
            if (!m10596a(c)) {
                super.mo11746a(request, null, new FacebookException("Invalid state parameter"));
                return;
            }
            String string = c.getString("error");
            if (string == null) {
                string = c.getString("error_type");
            }
            String string2 = c.getString("error_msg");
            if (string2 == null) {
                string2 = c.getString("error_message");
            }
            if (string2 == null) {
                string2 = c.getString("error_description");
            }
            String string3 = c.getString("error_code");
            int i = -1;
            if (!abg.m374a(string3)) {
                try {
                    i = Integer.parseInt(string3);
                } catch (NumberFormatException e) {
                    i = -1;
                }
            }
            if (abg.m374a(string) && abg.m374a(string2) && i == -1) {
                super.mo11746a(request, c, null);
            } else if (string != null && (string.equals("access_denied") || string.equals("OAuthAccessDeniedException"))) {
                super.mo11746a(request, null, new FacebookOperationCanceledException());
            } else if (i == 4201) {
                super.mo11746a(request, null, new FacebookOperationCanceledException());
            } else {
                super.mo11746a(request, null, new FacebookServiceException(new FacebookRequestError(i, string, string2), string2));
            }
        }
    }

    /* renamed from: a */
    private boolean m10596a(Bundle bundle) {
        try {
            String string = bundle.getString("state");
            if (string == null) {
                return false;
            }
            return new JSONObject(string).getString("7_challenge").equals(this.f8151e);
        } catch (JSONException e) {
            return false;
        }
    }

    /* renamed from: e */
    private boolean m10597e() {
        return m10598f() && m10599g() != null && abh.m415c(FacebookSdk.m17303f());
    }

    /* renamed from: f */
    private boolean m10598f() {
        aap a = aaq.m115a(abg.m344a((Context) this.f8234b.mo11695b()));
        return a != null && a.mo58d();
    }

    /* renamed from: g */
    private String m10599g() {
        if (this.f8150d != null) {
            return this.f8150d;
        }
        FragmentActivity b = this.f8234b.mo11695b();
        List<ResolveInfo> queryIntentServices = b.getPackageManager().queryIntentServices(new Intent("android.support.customtabs.action.CustomTabsService"), 0);
        if (queryIntentServices != null) {
            HashSet hashSet = new HashSet(Arrays.asList(f8149c));
            for (ResolveInfo resolveInfo : queryIntentServices) {
                ServiceInfo serviceInfo = resolveInfo.serviceInfo;
                if (serviceInfo != null && hashSet.contains(serviceInfo.packageName)) {
                    this.f8150d = serviceInfo.packageName;
                    return this.f8150d;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo11622a() {
        return "custom_tab";
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo11623a(JSONObject jSONObject) throws JSONException {
        jSONObject.put("7_challenge", this.f8151e);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11624a(int i, int i2, Intent intent) {
        if (i != 1) {
            return super.mo11624a(i, i2, intent);
        }
        Request c = this.f8234b.mo11698c();
        if (i2 == -1) {
            m10595a(intent.getStringExtra(CustomTabMainActivity.f7986c), c);
            return true;
        }
        super.mo11746a(c, null, new FacebookOperationCanceledException());
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11625a(Request request) {
        if (!m10597e()) {
            return false;
        }
        Bundle a = mo11745a(mo11747b(request), request);
        Intent intent = new Intent(this.f8234b.mo11695b(), CustomTabMainActivity.class);
        intent.putExtra(CustomTabMainActivity.f7984a, a);
        intent.putExtra(CustomTabMainActivity.f7985b, m10599g());
        this.f8234b.mo11688a().startActivityForResult(intent, 1);
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public String mo11626c() {
        return "chrome_custom_tab";
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d_ */
    public AccessTokenSource mo11627d_() {
        return AccessTokenSource.CHROME_CUSTOM_TAB;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f8151e);
    }
}
