package com.facebook.login;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import com.facebook.login.LoginClient.Request;
import com.facebook.login.LoginClient.Result;

abstract class NativeAppLoginMethodHandler extends LoginMethodHandler {
    NativeAppLoginMethodHandler(Parcel parcel) {
        super(parcel);
    }

    NativeAppLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* JADX WARNING: type inference failed for: r7v0, types: [com.facebook.login.LoginClient$Result, java.lang.String] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r7v0, types: [com.facebook.login.LoginClient$Result, java.lang.String]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [com.facebook.login.LoginClient$Result, java.lang.String]
  mth insns count: 35
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.facebook.login.LoginClient.Result m10749a(com.facebook.login.LoginClient.Request r12, android.content.Intent r13) {
        /*
            r11 = this;
            r7 = 0
            android.os.Bundle r5 = r13.getExtras()
            java.lang.String r1 = r11.m10750a(r5)
            java.lang.String r8 = "error_code"
            java.lang.String r2 = r5.getString(r8)
            java.lang.String r3 = r11.m10752b(r5)
            java.lang.String r8 = "e2e"
            java.lang.String r0 = r5.getString(r8)
            boolean r8 = p000.abg.m374a(r0)
            if (r8 != 0) goto L_0x0022
            r11.mo11742b(r0)
        L_0x0022:
            if (r1 != 0) goto L_0x0045
            if (r2 != 0) goto L_0x0045
            if (r3 != 0) goto L_0x0045
            java.util.Set r8 = r12.mo11715a()     // Catch:{ FacebookException -> 0x003b }
            com.facebook.AccessTokenSource r9 = com.facebook.AccessTokenSource.FACEBOOK_APPLICATION_WEB     // Catch:{ FacebookException -> 0x003b }
            java.lang.String r10 = r12.mo11721d()     // Catch:{ FacebookException -> 0x003b }
            com.facebook.AccessToken r6 = m10737a(r8, r5, r9, r10)     // Catch:{ FacebookException -> 0x003b }
            com.facebook.login.LoginClient$Result r7 = com.facebook.login.LoginClient.Result.m10726a(r12, r6)     // Catch:{ FacebookException -> 0x003b }
        L_0x003a:
            return r7
        L_0x003b:
            r4 = move-exception
            java.lang.String r8 = r4.getMessage()
            com.facebook.login.LoginClient$Result r7 = com.facebook.login.LoginClient.Result.m10728a(r12, r7, r8)
            goto L_0x003a
        L_0x0045:
            java.util.Collection<java.lang.String> r8 = p000.abe.f173a
            boolean r8 = r8.contains(r1)
            if (r8 != 0) goto L_0x003a
            java.util.Collection<java.lang.String> r8 = p000.abe.f174b
            boolean r8 = r8.contains(r1)
            if (r8 == 0) goto L_0x005a
            com.facebook.login.LoginClient$Result r7 = com.facebook.login.LoginClient.Result.m10727a(r12, r7)
            goto L_0x003a
        L_0x005a:
            com.facebook.login.LoginClient$Result r7 = com.facebook.login.LoginClient.Result.m10729a(r12, r1, r3, r2)
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.login.NativeAppLoginMethodHandler.m10749a(com.facebook.login.LoginClient$Request, android.content.Intent):com.facebook.login.LoginClient$Result");
    }

    /* renamed from: a */
    private String m10750a(Bundle bundle) {
        String string = bundle.getString("error");
        return string == null ? bundle.getString("error_type") : string;
    }

    /* renamed from: b */
    private Result m10751b(Request request, Intent intent) {
        Bundle extras = intent.getExtras();
        String a = m10750a(extras);
        String string = extras.getString("error_code");
        return "CONNECTION_FAILURE".equals(string) ? Result.m10729a(request, a, m10752b(extras), string) : Result.m10727a(request, a);
    }

    /* renamed from: b */
    private String m10752b(Bundle bundle) {
        String string = bundle.getString("error_message");
        return string == null ? bundle.getString("error_description") : string;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11624a(int i, int i2, Intent intent) {
        Request c = this.f8234b.mo11698c();
        Result a = intent == null ? Result.m10727a(c, "Operation canceled") : i2 == 0 ? m10751b(c, intent) : i2 != -1 ? Result.m10728a(c, "Unexpected resultCode from authorization.", null) : m10749a(c, intent);
        if (a != null) {
            this.f8234b.mo11691a(a);
        } else {
            this.f8234b.mo11706i();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo11744a(Intent intent, int i) {
        if (intent == null) {
            return false;
        }
        try {
            this.f8234b.mo11688a().startActivityForResult(intent, i);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public abstract boolean mo11625a(Request request);
}
