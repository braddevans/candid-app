package com.facebook.login;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.login.LoginClient.Request;
import java.util.Collection;

class FacebookLiteLoginMethodHandler extends NativeAppLoginMethodHandler {
    public static final Creator<FacebookLiteLoginMethodHandler> CREATOR = new Creator() {
        /* renamed from: a */
        public FacebookLiteLoginMethodHandler createFromParcel(Parcel parcel) {
            return new FacebookLiteLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public FacebookLiteLoginMethodHandler[] newArray(int i) {
            return new FacebookLiteLoginMethodHandler[i];
        }
    };

    FacebookLiteLoginMethodHandler(Parcel parcel) {
        super(parcel);
    }

    FacebookLiteLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo11622a() {
        return "fb_lite_login";
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11625a(Request request) {
        String m = LoginClient.m10686m();
        Intent a = abb.m280a((Context) this.f8234b.mo11695b(), request.mo11721d(), (Collection<String>) request.mo11715a(), m, request.mo11724f(), request.mo11726h(), request.mo11720c(), mo11739a(request.mo11723e()));
        mo11741a("e2e", m);
        return mo11744a(a, LoginClient.m10684d());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
