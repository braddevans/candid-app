package com.facebook.login;

public enum DefaultAudience {
    NONE(null),
    ONLY_ME("only_me"),
    FRIENDS("friends"),
    EVERYONE("everyone");
    

    /* renamed from: e */
    private final String f8157e;

    private DefaultAudience(String str) {
        this.f8157e = str;
    }

    /* renamed from: a */
    public String mo11634a() {
        return this.f8157e;
    }
}
