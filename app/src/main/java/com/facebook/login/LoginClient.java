package com.facebook.login;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.facebook.internal.CallbackManagerImpl.RequestCodeOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginClient implements Parcelable {
    public static final Creator<LoginClient> CREATOR = new Creator() {
        /* renamed from: a */
        public LoginClient createFromParcel(Parcel parcel) {
            return new LoginClient(parcel);
        }

        /* renamed from: a */
        public LoginClient[] newArray(int i) {
            return new LoginClient[i];
        }
    };

    /* renamed from: a */
    LoginMethodHandler[] f8206a;

    /* renamed from: b */
    int f8207b = -1;

    /* renamed from: c */
    Fragment f8208c;

    /* renamed from: d */
    C2037b f8209d;

    /* renamed from: e */
    C2036a f8210e;

    /* renamed from: f */
    boolean f8211f;

    /* renamed from: g */
    Request f8212g;

    /* renamed from: h */
    Map<String, String> f8213h;

    /* renamed from: i */
    private abn f8214i;

    public static class Request implements Parcelable {
        public static final Creator<Request> CREATOR = new Creator() {
            /* renamed from: a */
            public Request createFromParcel(Parcel parcel) {
                return new Request(parcel);
            }

            /* renamed from: a */
            public Request[] newArray(int i) {
                return new Request[i];
            }
        };

        /* renamed from: a */
        private final LoginBehavior f8215a;

        /* renamed from: b */
        private Set<String> f8216b;

        /* renamed from: c */
        private final DefaultAudience f8217c;

        /* renamed from: d */
        private final String f8218d;

        /* renamed from: e */
        private final String f8219e;

        /* renamed from: f */
        private boolean f8220f;

        /* renamed from: g */
        private String f8221g;

        private Request(Parcel parcel) {
            DefaultAudience defaultAudience = null;
            this.f8220f = false;
            String readString = parcel.readString();
            this.f8215a = readString != null ? LoginBehavior.valueOf(readString) : null;
            ArrayList arrayList = new ArrayList();
            parcel.readStringList(arrayList);
            this.f8216b = new HashSet(arrayList);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                defaultAudience = DefaultAudience.valueOf(readString2);
            }
            this.f8217c = defaultAudience;
            this.f8218d = parcel.readString();
            this.f8219e = parcel.readString();
            this.f8220f = parcel.readByte() != 0;
            this.f8221g = parcel.readString();
        }

        public Request(LoginBehavior loginBehavior, Set<String> set, DefaultAudience defaultAudience, String str, String str2) {
            this.f8220f = false;
            this.f8215a = loginBehavior;
            if (set == null) {
                set = new HashSet<>();
            }
            this.f8216b = set;
            this.f8217c = defaultAudience;
            this.f8218d = str;
            this.f8219e = str2;
        }

        /* renamed from: a */
        public Set<String> mo11715a() {
            return this.f8216b;
        }

        /* renamed from: a */
        public void mo11716a(String str) {
            this.f8221g = str;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo11717a(Set<String> set) {
            abh.m406a((Object) set, "permissions");
            this.f8216b = set;
        }

        /* renamed from: a */
        public void mo11718a(boolean z) {
            this.f8220f = z;
        }

        /* renamed from: b */
        public LoginBehavior mo11719b() {
            return this.f8215a;
        }

        /* renamed from: c */
        public DefaultAudience mo11720c() {
            return this.f8217c;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public String mo11721d() {
            return this.f8218d;
        }

        public int describeContents() {
            return 0;
        }

        /* renamed from: e */
        public String mo11723e() {
            return this.f8219e;
        }

        /* renamed from: f */
        public boolean mo11724f() {
            return this.f8220f;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: g */
        public String mo11725g() {
            return this.f8221g;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: h */
        public boolean mo11726h() {
            for (String a : this.f8216b) {
                if (abo.m498a(a)) {
                    return true;
                }
            }
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            String str = null;
            parcel.writeString(this.f8215a != null ? this.f8215a.name() : null);
            parcel.writeStringList(new ArrayList(this.f8216b));
            if (this.f8217c != null) {
                str = this.f8217c.name();
            }
            parcel.writeString(str);
            parcel.writeString(this.f8218d);
            parcel.writeString(this.f8219e);
            parcel.writeByte((byte) (this.f8220f ? 1 : 0));
            parcel.writeString(this.f8221g);
        }
    }

    public static class Result implements Parcelable {
        public static final Creator<Result> CREATOR = new Creator() {
            /* renamed from: a */
            public Result createFromParcel(Parcel parcel) {
                return new Result(parcel);
            }

            /* renamed from: a */
            public Result[] newArray(int i) {
                return new Result[i];
            }
        };

        /* renamed from: a */
        public final Code f8222a;

        /* renamed from: b */
        public final AccessToken f8223b;

        /* renamed from: c */
        public final String f8224c;

        /* renamed from: d */
        final String f8225d;

        /* renamed from: e */
        public final Request f8226e;

        /* renamed from: f */
        public Map<String, String> f8227f;

        public enum Code {
            SUCCESS("success"),
            CANCEL("cancel"),
            ERROR("error");
            

            /* renamed from: d */
            private final String f8232d;

            private Code(String str) {
                this.f8232d = str;
            }

            /* renamed from: a */
            public String mo11738a() {
                return this.f8232d;
            }
        }

        private Result(Parcel parcel) {
            this.f8222a = Code.valueOf(parcel.readString());
            this.f8223b = (AccessToken) parcel.readParcelable(AccessToken.class.getClassLoader());
            this.f8224c = parcel.readString();
            this.f8225d = parcel.readString();
            this.f8226e = (Request) parcel.readParcelable(Request.class.getClassLoader());
            this.f8227f = abg.m358a(parcel);
        }

        Result(Request request, Code code, AccessToken accessToken, String str, String str2) {
            abh.m406a((Object) code, "code");
            this.f8226e = request;
            this.f8223b = accessToken;
            this.f8224c = str;
            this.f8222a = code;
            this.f8225d = str2;
        }

        /* renamed from: a */
        static Result m10726a(Request request, AccessToken accessToken) {
            return new Result(request, Code.SUCCESS, accessToken, null, null);
        }

        /* renamed from: a */
        static Result m10727a(Request request, String str) {
            return new Result(request, Code.CANCEL, null, str, null);
        }

        /* renamed from: a */
        static Result m10728a(Request request, String str, String str2) {
            return m10729a(request, str, str2, null);
        }

        /* renamed from: a */
        static Result m10729a(Request request, String str, String str2, String str3) {
            return new Result(request, Code.ERROR, null, TextUtils.join(": ", abg.m377b((T[]) new String[]{str, str2})), str3);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f8222a.name());
            parcel.writeParcelable(this.f8223b, i);
            parcel.writeString(this.f8224c);
            parcel.writeString(this.f8225d);
            parcel.writeParcelable(this.f8226e, i);
            abg.m362a(parcel, this.f8227f);
        }
    }

    /* renamed from: com.facebook.login.LoginClient$a */
    public interface C2036a {
        /* renamed from: a */
        void mo248a();

        /* renamed from: b */
        void mo249b();
    }

    /* renamed from: com.facebook.login.LoginClient$b */
    public interface C2037b {
        /* renamed from: a */
        void mo247a(Result result);
    }

    public LoginClient(Parcel parcel) {
        Parcelable[] readParcelableArray = parcel.readParcelableArray(LoginMethodHandler.class.getClassLoader());
        this.f8206a = new LoginMethodHandler[readParcelableArray.length];
        for (int i = 0; i < readParcelableArray.length; i++) {
            this.f8206a[i] = (LoginMethodHandler) readParcelableArray[i];
            this.f8206a[i].mo11740a(this);
        }
        this.f8207b = parcel.readInt();
        this.f8212g = (Request) parcel.readParcelable(Request.class.getClassLoader());
        this.f8213h = abg.m358a(parcel);
    }

    public LoginClient(Fragment fragment) {
        this.f8208c = fragment;
    }

    /* renamed from: a */
    private void m10681a(String str, Result result, Map<String, String> map) {
        m10682a(str, result.f8222a.mo11738a(), result.f8224c, result.f8225d, map);
    }

    /* renamed from: a */
    private void m10682a(String str, String str2, String str3, String str4, Map<String, String> map) {
        if (this.f8212g == null) {
            m10688o().mo253a("fb_mobile_login_method_complete", "Unexpected call to logCompleteLogin with null pendingAuthorizationRequest.", str);
        } else {
            m10688o().mo254a(this.f8212g.mo11723e(), str, str2, str3, str4, map);
        }
    }

    /* renamed from: a */
    private void m10683a(String str, String str2, boolean z) {
        if (this.f8213h == null) {
            this.f8213h = new HashMap();
        }
        if (this.f8213h.containsKey(str) && z) {
            str2 = ((String) this.f8213h.get(str)) + "," + str2;
        }
        this.f8213h.put(str, str2);
    }

    /* renamed from: d */
    public static int m10684d() {
        return RequestCodeOffset.Login.mo11620a();
    }

    /* renamed from: d */
    private void m10685d(Result result) {
        if (this.f8209d != null) {
            this.f8209d.mo247a(result);
        }
    }

    /* renamed from: m */
    static String m10686m() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("init", System.currentTimeMillis());
        } catch (JSONException e) {
        }
        return jSONObject.toString();
    }

    /* renamed from: n */
    private void m10687n() {
        mo11697b(Result.m10728a(this.f8212g, "Login attempt failed.", null));
    }

    /* renamed from: o */
    private abn m10688o() {
        if (this.f8214i == null || !this.f8214i.mo250a().equals(this.f8212g.mo11721d())) {
            this.f8214i = new abn(mo11695b(), this.f8212g.mo11721d());
        }
        return this.f8214i;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public int mo11687a(String str) {
        return mo11695b().checkCallingOrSelfPermission(str);
    }

    /* renamed from: a */
    public Fragment mo11688a() {
        return this.f8208c;
    }

    /* renamed from: a */
    public void mo11689a(Fragment fragment) {
        if (this.f8208c != null) {
            throw new FacebookException("Can't set fragment once it is already set.");
        }
        this.f8208c = fragment;
    }

    /* renamed from: a */
    public void mo11690a(Request request) {
        if (!mo11702e()) {
            mo11696b(request);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo11691a(Result result) {
        if (result.f8223b == null || AccessToken.m10408a() == null) {
            mo11697b(result);
        } else {
            mo11699c(result);
        }
    }

    /* renamed from: a */
    public void mo11692a(C2036a aVar) {
        this.f8210e = aVar;
    }

    /* renamed from: a */
    public void mo11693a(C2037b bVar) {
        this.f8209d = bVar;
    }

    /* renamed from: a */
    public boolean mo11694a(int i, int i2, Intent intent) {
        if (this.f8212g != null) {
            return mo11704g().mo11624a(i, i2, intent);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public FragmentActivity mo11695b() {
        return this.f8208c.getActivity();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo11696b(Request request) {
        if (request != null) {
            if (this.f8212g != null) {
                throw new FacebookException("Attempted to authorize while a request is pending.");
            } else if (AccessToken.m10408a() == null || mo11705h()) {
                this.f8212g = request;
                this.f8206a = mo11700c(request);
                mo11706i();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo11697b(Result result) {
        LoginMethodHandler g = mo11704g();
        if (g != null) {
            m10681a(g.mo11622a(), result, g.f8233a);
        }
        if (this.f8213h != null) {
            result.f8227f = this.f8213h;
        }
        this.f8206a = null;
        this.f8207b = -1;
        this.f8212g = null;
        this.f8213h = null;
        m10685d(result);
    }

    /* renamed from: c */
    public Request mo11698c() {
        return this.f8212g;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo11699c(Result result) {
        Result a;
        if (result.f8223b == null) {
            throw new FacebookException("Can't validate without a token");
        }
        AccessToken a2 = AccessToken.m10408a();
        AccessToken accessToken = result.f8223b;
        if (!(a2 == null || accessToken == null)) {
            try {
                if (a2.mo11476i().equals(accessToken.mo11476i())) {
                    a = Result.m10726a(this.f8212g, result.f8223b);
                    mo11697b(a);
                }
            } catch (Exception e) {
                mo11697b(Result.m10728a(this.f8212g, "Caught exception", e.getMessage()));
                return;
            }
        }
        a = Result.m10728a(this.f8212g, "User logged in as different Facebook user.", null);
        mo11697b(a);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public LoginMethodHandler[] mo11700c(Request request) {
        ArrayList arrayList = new ArrayList();
        LoginBehavior b = request.mo11719b();
        if (b.mo11681a()) {
            arrayList.add(new GetTokenLoginMethodHandler(this));
        }
        if (b.mo11682b()) {
            arrayList.add(new KatanaProxyLoginMethodHandler(this));
        }
        if (b.mo11686f()) {
            arrayList.add(new FacebookLiteLoginMethodHandler(this));
        }
        if (b.mo11685e()) {
            arrayList.add(new CustomTabLoginMethodHandler(this));
        }
        if (b.mo11683c()) {
            arrayList.add(new WebViewLoginMethodHandler(this));
        }
        if (b.mo11684d()) {
            arrayList.add(new DeviceAuthMethodHandler(this));
        }
        LoginMethodHandler[] loginMethodHandlerArr = new LoginMethodHandler[arrayList.size()];
        arrayList.toArray(loginMethodHandlerArr);
        return loginMethodHandlerArr;
    }

    public int describeContents() {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public boolean mo11702e() {
        return this.f8212g != null && this.f8207b >= 0;
    }

    /* renamed from: f */
    public void mo11703f() {
        if (this.f8207b >= 0) {
            mo11704g().mo11668b();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public LoginMethodHandler mo11704g() {
        if (this.f8207b >= 0) {
            return this.f8206a[this.f8207b];
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: h */
    public boolean mo11705h() {
        if (this.f8211f) {
            return true;
        }
        if (mo11687a("android.permission.INTERNET") != 0) {
            FragmentActivity b = mo11695b();
            mo11697b(Result.m10728a(this.f8212g, b.getString(C3129f.com_facebook_internet_permission_error_title), b.getString(C3129f.com_facebook_internet_permission_error_message)));
            return false;
        }
        this.f8211f = true;
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: i */
    public void mo11706i() {
        if (this.f8207b >= 0) {
            m10682a(mo11704g().mo11622a(), "skipped", null, null, mo11704g().f8233a);
        }
        while (this.f8206a != null && this.f8207b < this.f8206a.length - 1) {
            this.f8207b++;
            if (mo11707j()) {
                return;
            }
        }
        if (this.f8212g != null) {
            m10687n();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: j */
    public boolean mo11707j() {
        boolean z = false;
        LoginMethodHandler g = mo11704g();
        if (!g.mo11743d() || mo11705h()) {
            z = g.mo11625a(this.f8212g);
            if (z) {
                m10688o().mo252a(this.f8212g.mo11723e(), g.mo11622a());
            } else {
                m10688o().mo256b(this.f8212g.mo11723e(), g.mo11622a());
                m10683a("not_tried", g.mo11622a(), true);
            }
        } else {
            m10683a("no_internet_permission", "1", false);
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: k */
    public void mo11708k() {
        if (this.f8210e != null) {
            this.f8210e.mo248a();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: l */
    public void mo11709l() {
        if (this.f8210e != null) {
            this.f8210e.mo249b();
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelableArray(this.f8206a, i);
        parcel.writeInt(this.f8207b);
        parcel.writeParcelable(this.f8212g, i);
        abg.m362a(parcel, this.f8213h);
    }
}
