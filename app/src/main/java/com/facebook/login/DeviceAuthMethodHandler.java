package com.facebook.login;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.AccessToken;
import com.facebook.AccessTokenSource;
import com.facebook.login.LoginClient.Request;
import com.facebook.login.LoginClient.Result;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ScheduledThreadPoolExecutor;

class DeviceAuthMethodHandler extends LoginMethodHandler {
    public static final Creator<DeviceAuthMethodHandler> CREATOR = new Creator() {
        /* renamed from: a */
        public DeviceAuthMethodHandler createFromParcel(Parcel parcel) {
            return new DeviceAuthMethodHandler(parcel);
        }

        /* renamed from: a */
        public DeviceAuthMethodHandler[] newArray(int i) {
            return new DeviceAuthMethodHandler[i];
        }
    };

    /* renamed from: c */
    private static ScheduledThreadPoolExecutor f8186c;

    protected DeviceAuthMethodHandler(Parcel parcel) {
        super(parcel);
    }

    DeviceAuthMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* renamed from: b */
    private void m10647b(Request request) {
        DeviceAuthDialog deviceAuthDialog = new DeviceAuthDialog();
        deviceAuthDialog.show(this.f8234b.mo11695b().getSupportFragmentManager(), "login_with_facebook");
        deviceAuthDialog.mo11635a(request);
    }

    /* renamed from: c */
    public static synchronized ScheduledThreadPoolExecutor m10648c() {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        synchronized (DeviceAuthMethodHandler.class) {
            if (f8186c == null) {
                f8186c = new ScheduledThreadPoolExecutor(1);
            }
            scheduledThreadPoolExecutor = f8186c;
        }
        return scheduledThreadPoolExecutor;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo11622a() {
        return "device_auth";
    }

    /* renamed from: a */
    public void mo11654a(Exception exc) {
        this.f8234b.mo11691a(Result.m10728a(this.f8234b.mo11698c(), null, exc.getMessage()));
    }

    /* renamed from: a */
    public void mo11655a(String str, String str2, String str3, Collection<String> collection, Collection<String> collection2, AccessTokenSource accessTokenSource, Date date, Date date2) {
        this.f8234b.mo11691a(Result.m10726a(this.f8234b.mo11698c(), new AccessToken(str, str2, str3, collection, collection2, accessTokenSource, date, date2)));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11625a(Request request) {
        m10647b(request);
        return true;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e_ */
    public void mo11657e_() {
        this.f8234b.mo11691a(Result.m10727a(this.f8234b.mo11698c(), "User canceled log in."));
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
