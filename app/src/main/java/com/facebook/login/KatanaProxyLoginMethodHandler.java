package com.facebook.login;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.login.LoginClient.Request;

class KatanaProxyLoginMethodHandler extends NativeAppLoginMethodHandler {
    public static final Creator<KatanaProxyLoginMethodHandler> CREATOR = new Creator() {
        /* renamed from: a */
        public KatanaProxyLoginMethodHandler createFromParcel(Parcel parcel) {
            return new KatanaProxyLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public KatanaProxyLoginMethodHandler[] newArray(int i) {
            return new KatanaProxyLoginMethodHandler[i];
        }
    };

    KatanaProxyLoginMethodHandler(Parcel parcel) {
        super(parcel);
    }

    KatanaProxyLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo11622a() {
        return "katana_proxy_auth";
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11625a(Request request) {
        String m = LoginClient.m10686m();
        Intent b = abb.m289b(this.f8234b.mo11695b(), request.mo11721d(), request.mo11715a(), m, request.mo11724f(), request.mo11726h(), request.mo11720c(), mo11739a(request.mo11723e()));
        mo11741a("e2e", m);
        return mo11744a(b, LoginClient.m10684d());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
