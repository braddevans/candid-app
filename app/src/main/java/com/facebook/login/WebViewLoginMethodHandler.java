package com.facebook.login;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.p001v4.app.FragmentActivity;
import com.facebook.AccessTokenSource;
import com.facebook.FacebookException;
import com.facebook.login.LoginClient.Request;

class WebViewLoginMethodHandler extends WebLoginMethodHandler {
    public static final Creator<WebViewLoginMethodHandler> CREATOR = new Creator() {
        /* renamed from: a */
        public WebViewLoginMethodHandler createFromParcel(Parcel parcel) {
            return new WebViewLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public WebViewLoginMethodHandler[] newArray(int i) {
            return new WebViewLoginMethodHandler[i];
        }
    };

    /* renamed from: c */
    private abi f8236c;

    /* renamed from: d */
    private String f8237d;

    /* renamed from: com.facebook.login.WebViewLoginMethodHandler$a */
    static class C2040a extends C0070a {

        /* renamed from: a */
        private String f8240a;

        /* renamed from: b */
        private boolean f8241b;

        public C2040a(Context context, String str, Bundle bundle) {
            super(context, str, "oauth", bundle);
        }

        /* renamed from: a */
        public abi mo212a() {
            Bundle e = mo216e();
            e.putString("redirect_uri", "fbconnect://success");
            e.putString("client_id", mo213b());
            e.putString("e2e", this.f8240a);
            e.putString("response_type", "token,signed_request");
            e.putString("return_scopes", "true");
            e.putString("auth_type", "rerequest");
            return new abi(mo214c(), "oauth", e, mo215d(), mo217f());
        }

        /* renamed from: a */
        public C2040a mo11754a(String str) {
            this.f8240a = str;
            return this;
        }

        /* renamed from: a */
        public C2040a mo11755a(boolean z) {
            this.f8241b = z;
            return this;
        }
    }

    WebViewLoginMethodHandler(Parcel parcel) {
        super(parcel);
        this.f8237d = parcel.readString();
    }

    WebViewLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo11622a() {
        return "web_view";
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo11625a(final Request request) {
        Bundle b = mo11747b(request);
        C20381 r3 = new C0072c() {
            /* renamed from: a */
            public void mo49a(Bundle bundle, FacebookException facebookException) {
                WebViewLoginMethodHandler.this.mo11748b(request, bundle, facebookException);
            }
        };
        this.f8237d = LoginClient.m10686m();
        mo11741a("e2e", this.f8237d);
        FragmentActivity b2 = this.f8234b.mo11695b();
        this.f8236c = new C2040a(b2, request.mo11721d(), b).mo11754a(this.f8237d).mo11755a(request.mo11724f()).mo211a(r3).mo212a();
        aam aam = new aam();
        aam.setRetainInstance(true);
        aam.mo43a((Dialog) this.f8236c);
        aam.show(b2.getSupportFragmentManager(), "FacebookDialogFragment");
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo11668b() {
        if (this.f8236c != null) {
            this.f8236c.cancel();
            this.f8236c = null;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo11748b(Request request, Bundle bundle, FacebookException facebookException) {
        super.mo11746a(request, bundle, facebookException);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public boolean mo11743d() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d_ */
    public AccessTokenSource mo11627d_() {
        return AccessTokenSource.WEB_VIEW;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f8237d);
    }
}
