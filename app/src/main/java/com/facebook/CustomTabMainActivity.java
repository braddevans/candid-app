package com.facebook;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

public class CustomTabMainActivity extends Activity {

    /* renamed from: a */
    public static final String f7984a = (CustomTabMainActivity.class.getSimpleName() + ".extra_params");

    /* renamed from: b */
    public static final String f7985b = (CustomTabMainActivity.class.getSimpleName() + ".extra_chromePackage");

    /* renamed from: c */
    public static final String f7986c = (CustomTabMainActivity.class.getSimpleName() + ".extra_url");

    /* renamed from: d */
    public static final String f7987d = (CustomTabMainActivity.class.getSimpleName() + ".action_refresh");

    /* renamed from: e */
    private boolean f7988e = true;

    /* renamed from: f */
    private BroadcastReceiver f7989f;

    /* renamed from: a */
    public static final String m10430a() {
        return "fb" + FacebookSdk.m17307j() + "://authorize";
    }

    /* renamed from: a */
    private void m10431a(int i, Intent intent) {
        LocalBroadcastManager.m8652a((Context) this).mo9450a(this.f7989f);
        if (intent != null) {
            setResult(i, intent);
        } else {
            setResult(i);
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (CustomTabActivity.f7980a.equals(getIntent().getAction())) {
            setResult(0);
            finish();
        } else if (bundle == null) {
            Bundle bundleExtra = getIntent().getBundleExtra(f7984a);
            new aai("oauth", bundleExtra).mo23a(this, getIntent().getStringExtra(f7985b));
            this.f7988e = false;
            this.f7989f = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    Intent intent2 = new Intent(CustomTabMainActivity.this, CustomTabMainActivity.class);
                    intent2.setAction(CustomTabMainActivity.f7987d);
                    intent2.putExtra(CustomTabMainActivity.f7986c, intent.getStringExtra(CustomTabMainActivity.f7986c));
                    intent2.addFlags(603979776);
                    CustomTabMainActivity.this.startActivity(intent2);
                }
            };
            LocalBroadcastManager.m8652a((Context) this).mo9451a(this.f7989f, new IntentFilter(CustomTabActivity.f7980a));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (f7987d.equals(intent.getAction())) {
            LocalBroadcastManager.m8652a((Context) this).mo9452a(new Intent(CustomTabActivity.f7981b));
            m10431a(-1, intent);
        } else if (CustomTabActivity.f7980a.equals(intent.getAction())) {
            m10431a(-1, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f7988e) {
            m10431a(0, null);
        }
        this.f7988e = true;
    }
}
