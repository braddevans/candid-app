package com.facebook.appevents;

import com.facebook.AccessToken;
import java.io.Serializable;

public class AccessTokenAppIdPair implements Serializable {

    /* renamed from: a */
    private final String f8081a;

    /* renamed from: b */
    private final String f8082b;

    public static class SerializationProxyV1 implements Serializable {

        /* renamed from: a */
        private final String f8083a;

        /* renamed from: b */
        private final String f8084b;

        private SerializationProxyV1(String str, String str2) {
            this.f8083a = str;
            this.f8084b = str2;
        }

        private Object readResolve() {
            return new AccessTokenAppIdPair(this.f8083a, this.f8084b);
        }
    }

    public AccessTokenAppIdPair(AccessToken accessToken) {
        this(accessToken.mo11466b(), FacebookSdk.m17307j());
    }

    public AccessTokenAppIdPair(String str, String str2) {
        if (abg.m374a(str)) {
            str = null;
        }
        this.f8081a = str;
        this.f8082b = str2;
    }

    private Object writeReplace() {
        return new SerializationProxyV1(this.f8081a, this.f8082b);
    }

    /* renamed from: a */
    public String mo11599a() {
        return this.f8081a;
    }

    /* renamed from: b */
    public String mo11600b() {
        return this.f8082b;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AccessTokenAppIdPair)) {
            return false;
        }
        AccessTokenAppIdPair accessTokenAppIdPair = (AccessTokenAppIdPair) obj;
        return abg.m373a(accessTokenAppIdPair.f8081a, this.f8081a) && abg.m373a(accessTokenAppIdPair.f8082b, this.f8082b);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.f8081a == null ? 0 : this.f8081a.hashCode();
        if (this.f8082b != null) {
            i = this.f8082b.hashCode();
        }
        return hashCode ^ i;
    }
}
