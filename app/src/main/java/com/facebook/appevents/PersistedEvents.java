package com.facebook.appevents;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class PersistedEvents implements Serializable {

    /* renamed from: a */
    private HashMap<AccessTokenAppIdPair, List<AppEvent>> f8119a = new HashMap<>();

    static class SerializationProxyV1 implements Serializable {

        /* renamed from: a */
        private final HashMap<AccessTokenAppIdPair, List<AppEvent>> f8120a;

        private SerializationProxyV1(HashMap<AccessTokenAppIdPair, List<AppEvent>> hashMap) {
            this.f8120a = hashMap;
        }

        private Object readResolve() {
            return new PersistedEvents(this.f8120a);
        }
    }

    public PersistedEvents() {
    }

    public PersistedEvents(HashMap<AccessTokenAppIdPair, List<AppEvent>> hashMap) {
        this.f8119a.putAll(hashMap);
    }

    private Object writeReplace() {
        return new SerializationProxyV1(this.f8119a);
    }

    /* renamed from: a */
    public List<AppEvent> mo11612a(AccessTokenAppIdPair accessTokenAppIdPair) {
        return (List) this.f8119a.get(accessTokenAppIdPair);
    }

    /* renamed from: a */
    public Set<AccessTokenAppIdPair> mo11613a() {
        return this.f8119a.keySet();
    }

    /* renamed from: a */
    public void mo11614a(AccessTokenAppIdPair accessTokenAppIdPair, List<AppEvent> list) {
        if (!this.f8119a.containsKey(accessTokenAppIdPair)) {
            this.f8119a.put(accessTokenAppIdPair, list);
        } else {
            ((List) this.f8119a.get(accessTokenAppIdPair)).addAll(list);
        }
    }

    /* renamed from: b */
    public boolean mo11615b(AccessTokenAppIdPair accessTokenAppIdPair) {
        return this.f8119a.containsKey(accessTokenAppIdPair);
    }
}
