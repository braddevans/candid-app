package com.facebook.appevents;

import android.content.Context;
import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.facebook.LoggingBehavior;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;

public class AppEventsLogger {

    /* renamed from: a */
    private static final String f8095a = AppEventsLogger.class.getCanonicalName();

    /* renamed from: d */
    private static ScheduledThreadPoolExecutor f8096d;

    /* renamed from: e */
    private static FlushBehavior f8097e = FlushBehavior.AUTO;

    /* renamed from: f */
    private static Object f8098f = new Object();

    /* renamed from: g */
    private static String f8099g;

    /* renamed from: h */
    private static boolean f8100h;

    /* renamed from: i */
    private static String f8101i;

    /* renamed from: b */
    private final String f8102b;

    /* renamed from: c */
    private final AccessTokenAppIdPair f8103c;

    public enum FlushBehavior {
        AUTO,
        EXPLICIT_ONLY
    }

    private AppEventsLogger(Context context, String str, AccessToken accessToken) {
        this(abg.m385c(context), str, accessToken);
    }

    protected AppEventsLogger(String str, String str2, AccessToken accessToken) {
        abh.m403a();
        this.f8102b = str;
        if (accessToken == null) {
            accessToken = AccessToken.m10408a();
        }
        if (accessToken == null || (str2 != null && !str2.equals(accessToken.mo11474h()))) {
            if (str2 == null) {
                str2 = abg.m344a(FacebookSdk.m17303f());
            }
            this.f8103c = new AccessTokenAppIdPair(null, str2);
        } else {
            this.f8103c = new AccessTokenAppIdPair(accessToken);
        }
        m10574e();
    }

    /* renamed from: a */
    public static FlushBehavior m10566a() {
        FlushBehavior flushBehavior;
        synchronized (f8098f) {
            flushBehavior = f8097e;
        }
        return flushBehavior;
    }

    /* renamed from: a */
    public static AppEventsLogger m10567a(Context context) {
        return new AppEventsLogger(context, (String) null, (AccessToken) null);
    }

    /* renamed from: a */
    public static AppEventsLogger m10568a(Context context, String str) {
        return new AppEventsLogger(context, str, (AccessToken) null);
    }

    /* renamed from: a */
    private static void m10569a(Context context, AppEvent appEvent, AccessTokenAppIdPair accessTokenAppIdPair) {
        AppEventQueue.m17390a(accessTokenAppIdPair, appEvent);
        if (!appEvent.mo11604b() && !f8100h) {
            if (appEvent.mo11603a() == "fb_mobile_activate_app") {
                f8100h = true;
            } else {
                aaz.m198a(LoggingBehavior.APP_EVENTS, "AppEvents", "Warning: Please call AppEventsLogger.activateApp(...)from the long-lived activity's onResume() methodbefore logging other app events.");
            }
        }
    }

    /* renamed from: a */
    private void m10570a(String str, Double d, Bundle bundle, boolean z, UUID uuid) {
        try {
            m10569a(FacebookSdk.m17303f(), new AppEvent(this.f8102b, str, d, bundle, z, uuid), this.f8103c);
        } catch (JSONException e) {
            aaz.m199a(LoggingBehavior.APP_EVENTS, "AppEvents", "JSON encoding for app event failed: '%s'", e.toString());
        } catch (FacebookException e2) {
            aaz.m199a(LoggingBehavior.APP_EVENTS, "AppEvents", "Invalid app event: %s", e2.toString());
        }
    }

    /* renamed from: b */
    public static String m10571b(Context context) {
        if (f8099g == null) {
            synchronized (f8098f) {
                if (f8099g == null) {
                    f8099g = context.getSharedPreferences("com.facebook.sdk.appEventPreferences", 0).getString("anonymousAppDeviceGUID", null);
                    if (f8099g == null) {
                        f8099g = "XZ" + UUID.randomUUID().toString();
                        context.getSharedPreferences("com.facebook.sdk.appEventPreferences", 0).edit().putString("anonymousAppDeviceGUID", f8099g).apply();
                    }
                }
            }
        }
        return f8099g;
    }

    /* renamed from: c */
    public static String m10572c() {
        String str;
        synchronized (f8098f) {
            str = f8101i;
        }
        return str;
    }

    /* renamed from: d */
    public static String m10573d() {
        return AnalyticsUserIDStore.m17377a();
    }

    /* renamed from: e */
    private static void m10574e() {
        synchronized (f8098f) {
            if (f8096d == null) {
                f8096d = new ScheduledThreadPoolExecutor(1);
                f8096d.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        HashSet<String> hashSet = new HashSet<>();
                        for (AccessTokenAppIdPair b : AppEventQueue.m17386a()) {
                            hashSet.add(b.mo11600b());
                        }
                        for (String a : hashSet) {
                            aaq.m117a(a, true);
                        }
                    }
                }, 0, 86400, TimeUnit.SECONDS);
            }
        }
    }

    /* renamed from: a */
    public void mo11608a(String str, Bundle bundle) {
        m10570a(str, null, bundle, false, aaa.m1a());
    }

    /* renamed from: a */
    public void mo11609a(String str, Double d, Bundle bundle) {
        m10570a(str, d, bundle, true, aaa.m1a());
    }

    /* renamed from: b */
    public void mo11610b() {
        AppEventQueue.m17391a(FlushReason.EXPLICIT);
    }
}
