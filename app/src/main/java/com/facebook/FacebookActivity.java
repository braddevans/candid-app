package com.facebook;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.p001v4.app.Fragment;
import android.support.p001v4.app.FragmentActivity;
import android.support.p001v4.app.FragmentManager;
import android.util.Log;
import com.facebook.share.internal.DeviceShareDialogFragment;
import com.facebook.share.model.ShareContent;

public class FacebookActivity extends FragmentActivity {

    /* renamed from: a */
    public static String f7991a = "PassThrough";

    /* renamed from: b */
    private static String f7992b = "SingleFragment";

    /* renamed from: c */
    private static final String f7993c = FacebookActivity.class.getName();

    /* renamed from: d */
    private Fragment f7994d;

    /* renamed from: c */
    private void m10432c() {
        setResult(0, abb.m281a(getIntent(), (Bundle) null, abb.m283a(abb.m296d(getIntent()))));
        finish();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Fragment mo11496a() {
        Intent intent = getIntent();
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        Fragment findFragmentByTag = supportFragmentManager.findFragmentByTag(f7992b);
        if (findFragmentByTag != null) {
            return findFragmentByTag;
        }
        if ("FacebookDialogFragment".equals(intent.getAction())) {
            aam aam = new aam();
            aam.setRetainInstance(true);
            aam.show(supportFragmentManager, f7992b);
            return aam;
        } else if ("DeviceShareDialogFragment".equals(intent.getAction())) {
            DeviceShareDialogFragment deviceShareDialogFragment = new DeviceShareDialogFragment();
            deviceShareDialogFragment.setRetainInstance(true);
            deviceShareDialogFragment.mo11821a((ShareContent) intent.getParcelableExtra("content"));
            deviceShareDialogFragment.show(supportFragmentManager, f7992b);
            return deviceShareDialogFragment;
        } else {
            abm abm = new abm();
            abm.setRetainInstance(true);
            supportFragmentManager.beginTransaction().add(C3127d.com_facebook_fragment_container, abm, f7992b).commit();
            return abm;
        }
    }

    /* renamed from: b */
    public Fragment mo11497b() {
        return this.f7994d;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f7994d != null) {
            this.f7994d.onConfigurationChanged(configuration);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (!FacebookSdk.m17293a()) {
            Log.d(f7993c, "Facebook SDK not initialized. Make sure you call sdkInitialize inside your Application's onCreate method.");
            FacebookSdk.m17291a(getApplicationContext());
        }
        setContentView(C3128e.com_facebook_activity_layout);
        if (f7991a.equals(intent.getAction())) {
            m10432c();
        } else {
            this.f7994d = mo11496a();
        }
    }
}
