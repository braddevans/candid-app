package com.facebook;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

public class CustomTabActivity extends Activity {

    /* renamed from: a */
    public static final String f7980a = (CustomTabActivity.class.getSimpleName() + ".action_customTabRedirect");

    /* renamed from: b */
    public static final String f7981b = (CustomTabActivity.class.getSimpleName() + ".action_destroy");

    /* renamed from: c */
    private BroadcastReceiver f7982c;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == 0) {
            Intent intent2 = new Intent(f7980a);
            intent2.putExtra(CustomTabMainActivity.f7986c, getIntent().getDataString());
            LocalBroadcastManager.m8652a((Context) this).mo9452a(intent2);
            this.f7982c = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    CustomTabActivity.this.finish();
                }
            };
            LocalBroadcastManager.m8652a((Context) this).mo9451a(this.f7982c, new IntentFilter(f7981b));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = new Intent(this, CustomTabMainActivity.class);
        intent.setAction(f7980a);
        intent.putExtra(CustomTabMainActivity.f7986c, getIntent().getDataString());
        intent.addFlags(603979776);
        startActivityForResult(intent, 2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LocalBroadcastManager.m8652a((Context) this).mo9450a(this.f7982c);
        super.onDestroy();
    }
}
