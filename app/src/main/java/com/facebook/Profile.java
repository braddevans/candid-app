package com.facebook;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.json.JSONException;
import org.json.JSONObject;

public final class Profile implements Parcelable {
    public static final Creator<Profile> CREATOR = new Creator() {
        /* renamed from: a */
        public Profile createFromParcel(Parcel parcel) {
            return new Profile(parcel);
        }

        /* renamed from: a */
        public Profile[] newArray(int i) {
            return new Profile[i];
        }
    };

    /* renamed from: a */
    private final String f8075a;

    /* renamed from: b */
    private final String f8076b;

    /* renamed from: c */
    private final String f8077c;

    /* renamed from: d */
    private final String f8078d;

    /* renamed from: e */
    private final String f8079e;

    /* renamed from: f */
    private final Uri f8080f;

    private Profile(Parcel parcel) {
        this.f8075a = parcel.readString();
        this.f8076b = parcel.readString();
        this.f8077c = parcel.readString();
        this.f8078d = parcel.readString();
        this.f8079e = parcel.readString();
        String readString = parcel.readString();
        this.f8080f = readString == null ? null : Uri.parse(readString);
    }

    public Profile(String str, String str2, String str3, String str4, String str5, Uri uri) {
        abh.m407a(str, "id");
        this.f8075a = str;
        this.f8076b = str2;
        this.f8077c = str3;
        this.f8078d = str4;
        this.f8079e = str5;
        this.f8080f = uri;
    }

    public Profile(JSONObject jSONObject) {
        Uri uri = null;
        this.f8075a = jSONObject.optString("id", null);
        this.f8076b = jSONObject.optString("first_name", null);
        this.f8077c = jSONObject.optString("middle_name", null);
        this.f8078d = jSONObject.optString("last_name", null);
        this.f8079e = jSONObject.optString("name", null);
        String optString = jSONObject.optString("link_uri", null);
        if (optString != null) {
            uri = Uri.parse(optString);
        }
        this.f8080f = uri;
    }

    /* renamed from: a */
    public static Profile m10546a() {
        return ProfileManager.m17357a().mo16986b();
    }

    /* renamed from: a */
    public static void m10547a(Profile profile) {
        ProfileManager.m17357a().mo16985a(profile);
    }

    /* renamed from: b */
    public static void m10548b() {
        AccessToken a = AccessToken.m10408a();
        if (a == null) {
            m10547a(null);
        } else {
            abg.m365a(a.mo11466b(), (C0063a) new C0063a() {
                /* renamed from: a */
                public void mo187a(FacebookException facebookException) {
                }

                /* renamed from: a */
                public void mo188a(JSONObject jSONObject) {
                    String optString = jSONObject.optString("id");
                    if (optString != null) {
                        String optString2 = jSONObject.optString("link");
                        Profile.m10547a(new Profile(optString, jSONObject.optString("first_name"), jSONObject.optString("middle_name"), jSONObject.optString("last_name"), jSONObject.optString("name"), optString2 != null ? Uri.parse(optString2) : null));
                    }
                }
            });
        }
    }

    /* renamed from: c */
    public String mo11589c() {
        return this.f8079e;
    }

    /* renamed from: d */
    public JSONObject mo11590d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", this.f8075a);
            jSONObject.put("first_name", this.f8076b);
            jSONObject.put("middle_name", this.f8077c);
            jSONObject.put("last_name", this.f8078d);
            jSONObject.put("name", this.f8079e);
            if (this.f8080f == null) {
                return jSONObject;
            }
            jSONObject.put("link_uri", this.f8080f.toString());
            return jSONObject;
        } catch (JSONException e) {
            return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Profile)) {
            return false;
        }
        Profile profile = (Profile) obj;
        return (!this.f8075a.equals(profile.f8075a) || this.f8076b != null) ? (!this.f8076b.equals(profile.f8076b) || this.f8077c != null) ? (!this.f8077c.equals(profile.f8077c) || this.f8078d != null) ? (!this.f8078d.equals(profile.f8078d) || this.f8079e != null) ? (!this.f8079e.equals(profile.f8079e) || this.f8080f != null) ? this.f8080f.equals(profile.f8080f) : profile.f8080f == null : profile.f8079e == null : profile.f8078d == null : profile.f8077c == null : profile.f8076b == null;
    }

    public int hashCode() {
        int hashCode = this.f8075a.hashCode() + 527;
        if (this.f8076b != null) {
            hashCode = (hashCode * 31) + this.f8076b.hashCode();
        }
        if (this.f8077c != null) {
            hashCode = (hashCode * 31) + this.f8077c.hashCode();
        }
        if (this.f8078d != null) {
            hashCode = (hashCode * 31) + this.f8078d.hashCode();
        }
        if (this.f8079e != null) {
            hashCode = (hashCode * 31) + this.f8079e.hashCode();
        }
        return this.f8080f != null ? (hashCode * 31) + this.f8080f.hashCode() : hashCode;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f8075a);
        parcel.writeString(this.f8076b);
        parcel.writeString(this.f8077c);
        parcel.writeString(this.f8078d);
        parcel.writeString(this.f8079e);
        parcel.writeString(this.f8080f == null ? null : this.f8080f.toString());
    }
}
