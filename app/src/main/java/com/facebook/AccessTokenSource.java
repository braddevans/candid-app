package com.facebook;

public enum AccessTokenSource {
    NONE(false),
    FACEBOOK_APPLICATION_WEB(true),
    FACEBOOK_APPLICATION_NATIVE(true),
    FACEBOOK_APPLICATION_SERVICE(true),
    WEB_VIEW(true),
    CHROME_CUSTOM_TAB(true),
    TEST_USER(true),
    CLIENT_TOKEN(true),
    DEVICE_AUTH(true);
    

    /* renamed from: j */
    private final boolean f7979j;

    private AccessTokenSource(boolean z) {
        this.f7979j = z;
    }

    /* renamed from: a */
    public boolean mo11487a() {
        return this.f7979j;
    }
}
