package com.facebook;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GraphRequest {

    /* renamed from: a */
    public static final String f8030a = GraphRequest.class.getSimpleName();

    /* renamed from: b */
    private static String f8031b;

    /* renamed from: c */
    private static Pattern f8032c = Pattern.compile("^/?v\\d+\\.\\d+/(.*)");

    /* renamed from: q */
    private static volatile String f8033q;

    /* renamed from: d */
    private AccessToken f8034d;

    /* renamed from: e */
    private HttpMethod f8035e;

    /* renamed from: f */
    private String f8036f;

    /* renamed from: g */
    private JSONObject f8037g;

    /* renamed from: h */
    private String f8038h;

    /* renamed from: i */
    private String f8039i;

    /* renamed from: j */
    private boolean f8040j;

    /* renamed from: k */
    private Bundle f8041k;

    /* renamed from: l */
    private C2004b f8042l;

    /* renamed from: m */
    private String f8043m;

    /* renamed from: n */
    private Object f8044n;

    /* renamed from: o */
    private String f8045o;

    /* renamed from: p */
    private boolean f8046p;

    public static class ParcelableResourceWithMimeType<RESOURCE extends Parcelable> implements Parcelable {
        public static final Creator<ParcelableResourceWithMimeType> CREATOR = new Creator<ParcelableResourceWithMimeType>() {
            /* renamed from: a */
            public ParcelableResourceWithMimeType createFromParcel(Parcel parcel) {
                return new ParcelableResourceWithMimeType(parcel);
            }

            /* renamed from: a */
            public ParcelableResourceWithMimeType[] newArray(int i) {
                return new ParcelableResourceWithMimeType[i];
            }
        };

        /* renamed from: a */
        private final String f8054a;

        /* renamed from: b */
        private final RESOURCE f8055b;

        private ParcelableResourceWithMimeType(Parcel parcel) {
            this.f8054a = parcel.readString();
            this.f8055b = parcel.readParcelable(FacebookSdk.m17303f().getClassLoader());
        }

        public ParcelableResourceWithMimeType(RESOURCE resource, String str) {
            this.f8054a = str;
            this.f8055b = resource;
        }

        /* renamed from: a */
        public String mo11568a() {
            return this.f8054a;
        }

        /* renamed from: b */
        public RESOURCE mo11569b() {
            return this.f8055b;
        }

        public int describeContents() {
            return 1;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f8054a);
            parcel.writeParcelable(this.f8055b, i);
        }
    }

    /* renamed from: com.facebook.GraphRequest$a */
    static class C2003a {

        /* renamed from: a */
        private final GraphRequest f8056a;

        /* renamed from: b */
        private final Object f8057b;

        public C2003a(GraphRequest graphRequest, Object obj) {
            this.f8056a = graphRequest;
            this.f8057b = obj;
        }

        /* renamed from: a */
        public GraphRequest mo11576a() {
            return this.f8056a;
        }

        /* renamed from: b */
        public Object mo11577b() {
            return this.f8057b;
        }
    }

    /* renamed from: com.facebook.GraphRequest$b */
    public interface C2004b {
        void onCompleted(GraphResponse zlVar);
    }

    /* renamed from: com.facebook.GraphRequest$c */
    public interface C2005c {
        void onCompleted(JSONObject jSONObject, GraphResponse zlVar);
    }

    /* renamed from: com.facebook.GraphRequest$d */
    interface C2006d {
        /* renamed from: a */
        void mo11567a(String str, String str2) throws IOException;
    }

    /* renamed from: com.facebook.GraphRequest$e */
    public interface C2007e extends C2004b {
        /* renamed from: a */
        void mo11578a(long j, long j2);
    }

    /* renamed from: com.facebook.GraphRequest$f */
    static class C2008f implements C2006d {

        /* renamed from: a */
        private final OutputStream f8058a;

        /* renamed from: b */
        private final aaz f8059b;

        /* renamed from: c */
        private boolean f8060c = true;

        /* renamed from: d */
        private boolean f8061d = false;

        public C2008f(OutputStream outputStream, aaz aaz, boolean z) {
            this.f8058a = outputStream;
            this.f8059b = aaz;
            this.f8061d = z;
        }

        /* renamed from: b */
        private RuntimeException m10534b() {
            return new IllegalArgumentException("value is not a supported type.");
        }

        /* renamed from: a */
        public void mo11579a() throws IOException {
            if (!this.f8061d) {
                mo11588b("--%s", "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
                return;
            }
            this.f8058a.write("&".getBytes());
        }

        /* renamed from: a */
        public void mo11580a(String str, Bitmap bitmap) throws IOException {
            mo11584a(str, str, "image/png");
            bitmap.compress(CompressFormat.PNG, 100, this.f8058a);
            mo11588b("", new Object[0]);
            mo11579a();
            if (this.f8059b != null) {
                this.f8059b.mo131a("    " + str, (Object) "<Image>");
            }
        }

        /* renamed from: a */
        public void mo11581a(String str, Uri uri, String str2) throws IOException {
            if (str2 == null) {
                str2 = "content/unknown";
            }
            mo11584a(str, str, str2);
            int i = 0;
            if (this.f8058a instanceof ProgressNoopOutputStream) {
                ((ProgressNoopOutputStream) this.f8058a).mo16989a(abg.m394e(uri));
            } else {
                i = 0 + abg.m337a(FacebookSdk.m17303f().getContentResolver().openInputStream(uri), this.f8058a);
            }
            mo11588b("", new Object[0]);
            mo11579a();
            if (this.f8059b != null) {
                this.f8059b.mo131a("    " + str, (Object) String.format(Locale.ROOT, "<Data: %d>", new Object[]{Integer.valueOf(i)}));
            }
        }

        /* renamed from: a */
        public void mo11582a(String str, ParcelFileDescriptor parcelFileDescriptor, String str2) throws IOException {
            if (str2 == null) {
                str2 = "content/unknown";
            }
            mo11584a(str, str, str2);
            int i = 0;
            if (this.f8058a instanceof ProgressNoopOutputStream) {
                ((ProgressNoopOutputStream) this.f8058a).mo16989a(parcelFileDescriptor.getStatSize());
            } else {
                i = 0 + abg.m337a((InputStream) new AutoCloseInputStream(parcelFileDescriptor), this.f8058a);
            }
            mo11588b("", new Object[0]);
            mo11579a();
            if (this.f8059b != null) {
                this.f8059b.mo131a("    " + str, (Object) String.format(Locale.ROOT, "<Data: %d>", new Object[]{Integer.valueOf(i)}));
            }
        }

        /* renamed from: a */
        public void mo11583a(String str, Object obj, GraphRequest graphRequest) throws IOException {
            if (this.f8058a instanceof RequestOutputStream) {
                ((RequestOutputStream) this.f8058a).mo16990a(graphRequest);
            }
            if (GraphRequest.m10497e(obj)) {
                mo11567a(str, GraphRequest.m10499f(obj));
            } else if (obj instanceof Bitmap) {
                mo11580a(str, (Bitmap) obj);
            } else if (obj instanceof byte[]) {
                mo11586a(str, (byte[]) obj);
            } else if (obj instanceof Uri) {
                mo11581a(str, (Uri) obj, (String) null);
            } else if (obj instanceof ParcelFileDescriptor) {
                mo11582a(str, (ParcelFileDescriptor) obj, (String) null);
            } else if (obj instanceof ParcelableResourceWithMimeType) {
                ParcelableResourceWithMimeType parcelableResourceWithMimeType = (ParcelableResourceWithMimeType) obj;
                Parcelable b = parcelableResourceWithMimeType.mo11569b();
                String a = parcelableResourceWithMimeType.mo11568a();
                if (b instanceof ParcelFileDescriptor) {
                    mo11582a(str, (ParcelFileDescriptor) b, a);
                } else if (b instanceof Uri) {
                    mo11581a(str, (Uri) b, a);
                } else {
                    throw m10534b();
                }
            } else {
                throw m10534b();
            }
        }

        /* renamed from: a */
        public void mo11567a(String str, String str2) throws IOException {
            mo11584a(str, (String) null, (String) null);
            mo11588b("%s", str2);
            mo11579a();
            if (this.f8059b != null) {
                this.f8059b.mo131a("    " + str, (Object) str2);
            }
        }

        /* renamed from: a */
        public void mo11584a(String str, String str2, String str3) throws IOException {
            if (!this.f8061d) {
                mo11587a("Content-Disposition: form-data; name=\"%s\"", str);
                if (str2 != null) {
                    mo11587a("; filename=\"%s\"", str2);
                }
                mo11588b("", new Object[0]);
                if (str3 != null) {
                    mo11588b("%s: %s", "Content-Type", str3);
                }
                mo11588b("", new Object[0]);
                return;
            }
            this.f8058a.write(String.format("%s=", new Object[]{str}).getBytes());
        }

        /* renamed from: a */
        public void mo11585a(String str, JSONArray jSONArray, Collection<GraphRequest> collection) throws IOException, JSONException {
            if (!(this.f8058a instanceof RequestOutputStream)) {
                mo11567a(str, jSONArray.toString());
                return;
            }
            RequestOutputStream zsVar = (RequestOutputStream) this.f8058a;
            mo11584a(str, (String) null, (String) null);
            mo11587a("[", new Object[0]);
            int i = 0;
            for (GraphRequest graphRequest : collection) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                zsVar.mo16990a(graphRequest);
                if (i > 0) {
                    mo11587a(",%s", jSONObject.toString());
                } else {
                    mo11587a("%s", jSONObject.toString());
                }
                i++;
            }
            mo11587a("]", new Object[0]);
            if (this.f8059b != null) {
                this.f8059b.mo131a("    " + str, (Object) jSONArray.toString());
            }
        }

        /* renamed from: a */
        public void mo11586a(String str, byte[] bArr) throws IOException {
            mo11584a(str, str, "content/unknown");
            this.f8058a.write(bArr);
            mo11588b("", new Object[0]);
            mo11579a();
            if (this.f8059b != null) {
                this.f8059b.mo131a("    " + str, (Object) String.format(Locale.ROOT, "<Data: %d>", new Object[]{Integer.valueOf(bArr.length)}));
            }
        }

        /* renamed from: a */
        public void mo11587a(String str, Object... objArr) throws IOException {
            if (!this.f8061d) {
                if (this.f8060c) {
                    this.f8058a.write("--".getBytes());
                    this.f8058a.write("3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f".getBytes());
                    this.f8058a.write("\r\n".getBytes());
                    this.f8060c = false;
                }
                this.f8058a.write(String.format(str, objArr).getBytes());
                return;
            }
            this.f8058a.write(URLEncoder.encode(String.format(Locale.US, str, objArr), "UTF-8").getBytes());
        }

        /* renamed from: b */
        public void mo11588b(String str, Object... objArr) throws IOException {
            mo11587a(str, objArr);
            if (!this.f8061d) {
                mo11587a("\r\n", new Object[0]);
            }
        }
    }

    public GraphRequest() {
        this(null, null, null, null, null);
    }

    public GraphRequest(AccessToken accessToken, String str, Bundle bundle, HttpMethod httpMethod) {
        this(accessToken, str, bundle, httpMethod, null);
    }

    public GraphRequest(AccessToken accessToken, String str, Bundle bundle, HttpMethod httpMethod, C2004b bVar) {
        this(accessToken, str, bundle, httpMethod, bVar, null);
    }

    public GraphRequest(AccessToken accessToken, String str, Bundle bundle, HttpMethod httpMethod, C2004b bVar, String str2) {
        this.f8040j = true;
        this.f8046p = false;
        this.f8034d = accessToken;
        this.f8036f = str;
        this.f8045o = str2;
        mo11548a(bVar);
        mo11549a(httpMethod);
        if (bundle != null) {
            this.f8041k = new Bundle(bundle);
        } else {
            this.f8041k = new Bundle();
        }
        if (this.f8045o == null) {
            this.f8045o = FacebookSdk.m17304g();
        }
    }

    /* renamed from: a */
    public static GraphRequest m10467a(AccessToken accessToken, final C2005c cVar) {
        return new GraphRequest(accessToken, "me", null, null, new C2004b() {
            public void onCompleted(GraphResponse zlVar) {
                if (cVar != null) {
                    cVar.onCompleted(zlVar.mo16978b(), zlVar);
                }
            }
        });
    }

    /* renamed from: a */
    public static GraphRequest m10468a(AccessToken accessToken, String str, C2004b bVar) {
        return new GraphRequest(accessToken, str, null, null, bVar);
    }

    /* renamed from: a */
    public static GraphRequest m10469a(AccessToken accessToken, String str, JSONObject jSONObject, C2004b bVar) {
        GraphRequest graphRequest = new GraphRequest(accessToken, str, null, HttpMethod.POST, bVar);
        graphRequest.mo11552a(jSONObject);
        return graphRequest;
    }

    /* renamed from: a */
    private static HttpURLConnection m10470a(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty("User-Agent", m10505p());
        httpURLConnection.setRequestProperty("Accept-Language", Locale.getDefault().toString());
        httpURLConnection.setChunkedStreamingMode(0);
        return httpURLConnection;
    }

    /* renamed from: a */
    public static HttpURLConnection m10471a(GraphRequestBatch zkVar) {
        m10495d(zkVar);
        try {
            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = m10470a(zkVar.size() == 1 ? new URL(zkVar.get(0).mo11564l()) : new URL(abe.m330b()));
                m10484a(zkVar, httpURLConnection);
                return httpURLConnection;
            } catch (IOException | JSONException e) {
                abg.m368a((URLConnection) httpURLConnection);
                throw new FacebookException("could not construct request body", e);
            }
        } catch (MalformedURLException e2) {
            throw new FacebookException("could not construct URL for request", (Throwable) e2);
        }
    }

    /* renamed from: a */
    public static List<GraphResponse> m10472a(HttpURLConnection httpURLConnection, GraphRequestBatch zkVar) {
        List<GraphResponse> a = GraphResponse.m17341a(httpURLConnection, zkVar);
        abg.m368a((URLConnection) httpURLConnection);
        int size = zkVar.size();
        if (size != a.size()) {
            throw new FacebookException(String.format(Locale.US, "Received %d responses while expecting %d", new Object[]{Integer.valueOf(a.size()), Integer.valueOf(size)}));
        }
        m10485a(zkVar, a);
        AccessTokenManager.m17270a().mo16934d();
        return a;
    }

    /* renamed from: a */
    public static List<GraphResponse> m10473a(Collection<GraphRequest> collection) {
        return m10487b(new GraphRequestBatch(collection));
    }

    /* renamed from: a */
    public static List<GraphResponse> m10474a(GraphRequest... graphRequestArr) {
        abh.m406a((Object) graphRequestArr, "requests");
        return m10473a((Collection<GraphRequest>) Arrays.asList(graphRequestArr));
    }

    /* renamed from: a */
    public static GraphResponse m10475a(GraphRequest graphRequest) {
        List a = m10474a(graphRequest);
        if (a != null && a.size() == 1) {
            return (GraphResponse) a.get(0);
        }
        throw new FacebookException("invalid state: expected a single response");
    }

    /* renamed from: a */
    private static void m10476a(Bundle bundle, C2008f fVar, GraphRequest graphRequest) throws IOException {
        for (String str : bundle.keySet()) {
            Object obj = bundle.get(str);
            if (m10497e(obj)) {
                fVar.mo11583a(str, obj, graphRequest);
            }
        }
    }

    /* renamed from: a */
    private static void m10477a(C2008f fVar, Collection<GraphRequest> collection, Map<String, C2003a> map) throws JSONException, IOException {
        JSONArray jSONArray = new JSONArray();
        for (GraphRequest a : collection) {
            a.m10481a(jSONArray, map);
        }
        fVar.mo11585a("batch", jSONArray, collection);
    }

    /* renamed from: a */
    private static void m10478a(String str, Object obj, C2006d dVar, boolean z) throws IOException {
        Class cls = obj.getClass();
        if (JSONObject.class.isAssignableFrom(cls)) {
            JSONObject jSONObject = (JSONObject) obj;
            if (z) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str2 = (String) keys.next();
                    m10478a(String.format("%s[%s]", new Object[]{str, str2}), jSONObject.opt(str2), dVar, z);
                }
            } else if (jSONObject.has("id")) {
                m10478a(str, (Object) jSONObject.optString("id"), dVar, z);
            } else if (jSONObject.has("url")) {
                m10478a(str, (Object) jSONObject.optString("url"), dVar, z);
            } else if (jSONObject.has("fbsdk:create_object")) {
                m10478a(str, (Object) jSONObject.toString(), dVar, z);
            }
        } else if (JSONArray.class.isAssignableFrom(cls)) {
            JSONArray jSONArray = (JSONArray) obj;
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                m10478a(String.format(Locale.ROOT, "%s[%d]", new Object[]{str, Integer.valueOf(i)}), jSONArray.opt(i), dVar, z);
            }
        } else if (String.class.isAssignableFrom(cls) || Number.class.isAssignableFrom(cls) || Boolean.class.isAssignableFrom(cls)) {
            dVar.mo11567a(str, obj.toString());
        } else if (Date.class.isAssignableFrom(cls)) {
            C2006d dVar2 = dVar;
            String str3 = str;
            dVar2.mo11567a(str3, new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format((Date) obj));
        }
    }

    /* renamed from: a */
    private static void m10479a(HttpURLConnection httpURLConnection, boolean z) {
        if (z) {
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
            return;
        }
        httpURLConnection.setRequestProperty("Content-Type", m10504o());
    }

    /* renamed from: a */
    private static void m10480a(Map<String, C2003a> map, C2008f fVar) throws IOException {
        for (String str : map.keySet()) {
            C2003a aVar = (C2003a) map.get(str);
            if (m10496d(aVar.mo11577b())) {
                fVar.mo11583a(str, aVar.mo11577b(), aVar.mo11576a());
            }
        }
    }

    /* renamed from: a */
    private void m10481a(JSONArray jSONArray, Map<String, C2003a> map) throws JSONException, IOException {
        JSONObject jSONObject = new JSONObject();
        if (this.f8038h != null) {
            jSONObject.put("name", this.f8038h);
            jSONObject.put("omit_response_on_success", this.f8040j);
        }
        if (this.f8039i != null) {
            jSONObject.put("depends_on", this.f8039i);
        }
        String k = mo11563k();
        jSONObject.put("relative_url", k);
        jSONObject.put("method", this.f8035e);
        if (this.f8034d != null) {
            aaz.m200a(this.f8034d.mo11466b());
        }
        ArrayList arrayList = new ArrayList();
        for (String str : this.f8041k.keySet()) {
            Object obj = this.f8041k.get(str);
            if (m10496d(obj)) {
                String format = String.format(Locale.ROOT, "%s%d", new Object[]{"file", Integer.valueOf(map.size())});
                arrayList.add(format);
                map.put(format, new C2003a(this, obj));
            }
        }
        if (!arrayList.isEmpty()) {
            jSONObject.put("attached_files", TextUtils.join(",", arrayList));
        }
        if (this.f8037g != null) {
            final ArrayList arrayList2 = new ArrayList();
            m10482a(this.f8037g, k, (C2006d) new C2006d() {
                /* renamed from: a */
                public void mo11567a(String str, String str2) throws IOException {
                    arrayList2.add(String.format(Locale.US, "%s=%s", new Object[]{str, URLEncoder.encode(str2, "UTF-8")}));
                }
            });
            jSONObject.put("body", TextUtils.join("&", arrayList2));
        }
        jSONArray.put(jSONObject);
    }

    /* renamed from: a */
    private static void m10482a(JSONObject jSONObject, String str, C2006d dVar) throws IOException {
        boolean z = false;
        if (m10494c(str)) {
            int indexOf = str.indexOf(":");
            int indexOf2 = str.indexOf("?");
            z = indexOf > 3 && (indexOf2 == -1 || indexOf < indexOf2);
        }
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str2 = (String) keys.next();
            m10478a(str2, jSONObject.opt(str2), dVar, z && str2.equalsIgnoreCase("image"));
        }
    }

    /* renamed from: a */
    private static void m10483a(GraphRequestBatch zkVar, aaz aaz, int i, URL url, OutputStream outputStream, boolean z) throws IOException, JSONException {
        C2008f fVar = new C2008f(outputStream, aaz, z);
        if (i == 1) {
            GraphRequest a = zkVar.get(0);
            HashMap hashMap = new HashMap();
            for (String str : a.f8041k.keySet()) {
                Object obj = a.f8041k.get(str);
                if (m10496d(obj)) {
                    hashMap.put(str, new C2003a(a, obj));
                }
            }
            if (aaz != null) {
                aaz.mo134c("  Parameters:\n");
            }
            m10476a(a.f8041k, fVar, a);
            if (aaz != null) {
                aaz.mo134c("  Attachments:\n");
            }
            m10480a((Map<String, C2003a>) hashMap, fVar);
            if (a.f8037g != null) {
                m10482a(a.f8037g, url.getPath(), (C2006d) fVar);
                return;
            }
            return;
        }
        String g = m10501g(zkVar);
        if (abg.m374a(g)) {
            throw new FacebookException("App ID was not specified at the request or Settings.");
        }
        fVar.mo11567a("batch_app_id", g);
        HashMap hashMap2 = new HashMap();
        m10477a(fVar, (Collection<GraphRequest>) zkVar, (Map<String, C2003a>) hashMap2);
        if (aaz != null) {
            aaz.mo134c("  Attachments:\n");
        }
        m10480a((Map<String, C2003a>) hashMap2, fVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f4  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static final void m10484a(p000.GraphRequestBatch r22, java.net.HttpURLConnection r23) throws java.io.IOException, org.json.JSONException {
        /*
            aaz r20 = new aaz
            com.facebook.LoggingBehavior r2 = com.facebook.LoggingBehavior.REQUESTS
            java.lang.String r3 = "Request"
            r0 = r20
            r0.<init>(r2, r3)
            int r4 = r22.size()
            boolean r7 = m10500f(r22)
            r2 = 1
            if (r4 != r2) goto L_0x0095
            r2 = 0
            r0 = r22
            com.facebook.GraphRequest r2 = r0.get(r2)
            com.facebook.HttpMethod r0 = r2.f8035e
            r18 = r0
        L_0x0021:
            java.lang.String r2 = r18.name()
            r0 = r23
            r0.setRequestMethod(r2)
            r0 = r23
            m10479a(r0, r7)
            java.net.URL r5 = r23.getURL()
            java.lang.String r2 = "Request:\n"
            r0 = r20
            r0.mo134c(r2)
            java.lang.String r2 = "Id"
            java.lang.String r3 = r22.mo16962b()
            r0 = r20
            r0.mo131a(r2, r3)
            java.lang.String r2 = "URL"
            r0 = r20
            r0.mo131a(r2, r5)
            java.lang.String r2 = "Method"
            java.lang.String r3 = r23.getRequestMethod()
            r0 = r20
            r0.mo131a(r2, r3)
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = "User-Agent"
            r0 = r23
            java.lang.String r3 = r0.getRequestProperty(r3)
            r0 = r20
            r0.mo131a(r2, r3)
            java.lang.String r2 = "Content-Type"
            java.lang.String r3 = "Content-Type"
            r0 = r23
            java.lang.String r3 = r0.getRequestProperty(r3)
            r0 = r20
            r0.mo131a(r2, r3)
            int r2 = r22.mo16952a()
            r0 = r23
            r0.setConnectTimeout(r2)
            int r2 = r22.mo16952a()
            r0 = r23
            r0.setReadTimeout(r2)
            com.facebook.HttpMethod r2 = com.facebook.HttpMethod.POST
            r0 = r18
            if (r0 != r2) goto L_0x0098
            r19 = 1
        L_0x008f:
            if (r19 != 0) goto L_0x009b
            r20.mo130a()
        L_0x0094:
            return
        L_0x0095:
            com.facebook.HttpMethod r18 = com.facebook.HttpMethod.POST
            goto L_0x0021
        L_0x0098:
            r19 = 0
            goto L_0x008f
        L_0x009b:
            r2 = 1
            r0 = r23
            r0.setDoOutput(r2)
            r8 = 0
            java.io.BufferedOutputStream r9 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x00f1 }
            java.io.OutputStream r2 = r23.getOutputStream()     // Catch:{ all -> 0x00f1 }
            r9.<init>(r2)     // Catch:{ all -> 0x00f1 }
            if (r7 == 0) goto L_0x00b3
            java.util.zip.GZIPOutputStream r8 = new java.util.zip.GZIPOutputStream     // Catch:{ all -> 0x00f8 }
            r8.<init>(r9)     // Catch:{ all -> 0x00f8 }
            r9 = r8
        L_0x00b3:
            boolean r2 = m10498e(r22)     // Catch:{ all -> 0x00f8 }
            if (r2 == 0) goto L_0x00fb
            r6 = 0
            zp r6 = new zp     // Catch:{ all -> 0x00f8 }
            android.os.Handler r2 = r22.mo16963c()     // Catch:{ all -> 0x00f8 }
            r6.<init>(r2)     // Catch:{ all -> 0x00f8 }
            r3 = 0
            r2 = r22
            m10483a(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
            int r21 = r6.mo16988a()     // Catch:{ all -> 0x00f8 }
            java.util.Map r11 = r6.mo16991b()     // Catch:{ all -> 0x00f8 }
            zq r8 = new zq     // Catch:{ all -> 0x00f8 }
            r0 = r21
            long r12 = (long) r0     // Catch:{ all -> 0x00f8 }
            r10 = r22
            r8.<init>(r9, r10, r11, r12)     // Catch:{ all -> 0x00f8 }
        L_0x00db:
            r12 = r22
            r13 = r20
            r14 = r4
            r15 = r5
            r16 = r8
            r17 = r7
            m10483a(r12, r13, r14, r15, r16, r17)     // Catch:{ all -> 0x00f1 }
            if (r8 == 0) goto L_0x00ed
            r8.close()
        L_0x00ed:
            r20.mo130a()
            goto L_0x0094
        L_0x00f1:
            r2 = move-exception
        L_0x00f2:
            if (r8 == 0) goto L_0x00f7
            r8.close()
        L_0x00f7:
            throw r2
        L_0x00f8:
            r2 = move-exception
            r8 = r9
            goto L_0x00f2
        L_0x00fb:
            r8 = r9
            goto L_0x00db
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.GraphRequest.m10484a(zk, java.net.HttpURLConnection):void");
    }

    /* renamed from: a */
    static void m10485a(final GraphRequestBatch zkVar, List<GraphResponse> list) {
        int size = zkVar.size();
        final ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size; i++) {
            GraphRequest a = zkVar.get(i);
            if (a.f8042l != null) {
                arrayList.add(new Pair(a.f8042l, list.get(i)));
            }
        }
        if (arrayList.size() > 0) {
            C20003 r5 = new Runnable() {
                public void run() {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        Pair pair = (Pair) it.next();
                        ((C2004b) pair.first).onCompleted((GraphResponse) pair.second);
                    }
                    for (C3120a a : zkVar.mo16966e()) {
                        a.mo286a(zkVar);
                    }
                }
            };
            Handler c = zkVar.mo16963c();
            if (c == null) {
                r5.run();
            } else {
                c.post(r5);
            }
        }
    }

    /* renamed from: b */
    private String m10486b(String str) {
        Builder buildUpon = Uri.parse(str).buildUpon();
        for (String str2 : this.f8041k.keySet()) {
            Object obj = this.f8041k.get(str2);
            if (obj == null) {
                obj = "";
            }
            if (m10497e(obj)) {
                buildUpon.appendQueryParameter(str2, m10499f(obj).toString());
            } else if (this.f8035e == HttpMethod.GET) {
                throw new IllegalArgumentException(String.format(Locale.US, "Unsupported parameter type for GET request: %s", new Object[]{obj.getClass().getSimpleName()}));
            }
        }
        return buildUpon.toString();
    }

    /* renamed from: b */
    public static List<GraphResponse> m10487b(GraphRequestBatch zkVar) {
        List<GraphResponse> a;
        abh.m414c(zkVar, "requests");
        boolean z = false;
        try {
            z = m10471a(zkVar);
            a = m10472a(z, zkVar);
        } catch (Exception e) {
            a = GraphResponse.m17342a(zkVar.mo16965d(), (HttpURLConnection) null, new FacebookException((Throwable) e));
            m10485a(zkVar, a);
        } finally {
            abg.m368a((URLConnection) z);
        }
        return a;
    }

    /* renamed from: b */
    public static GraphRequestAsyncTask m10488b(Collection<GraphRequest> collection) {
        return m10493c(new GraphRequestBatch(collection));
    }

    /* renamed from: b */
    public static GraphRequestAsyncTask m10489b(GraphRequest... graphRequestArr) {
        abh.m406a((Object) graphRequestArr, "requests");
        return m10488b((Collection<GraphRequest>) Arrays.asList(graphRequestArr));
    }

    /* renamed from: b */
    static final boolean m10490b(GraphRequest graphRequest) {
        boolean z = false;
        String d = graphRequest.mo11556d();
        if (abg.m374a(d)) {
            return true;
        }
        if (d.startsWith("v")) {
            d = d.substring(1);
        }
        String[] split = d.split("\\.");
        if ((split.length >= 2 && Integer.parseInt(split[0]) > 2) || (Integer.parseInt(split[0]) >= 2 && Integer.parseInt(split[1]) >= 4)) {
            z = true;
        }
        return z;
    }

    /* renamed from: c */
    public static GraphRequestAsyncTask m10493c(GraphRequestBatch zkVar) {
        abh.m414c(zkVar, "requests");
        GraphRequestAsyncTask zjVar = new GraphRequestAsyncTask(zkVar);
        zjVar.executeOnExecutor(FacebookSdk.m17301d(), new Void[0]);
        return zjVar;
    }

    /* renamed from: c */
    private static boolean m10494c(String str) {
        Matcher matcher = f8032c.matcher(str);
        if (matcher.matches()) {
            str = matcher.group(1);
        }
        return str.startsWith("me/") || str.startsWith("/me/");
    }

    /* renamed from: d */
    static final void m10495d(GraphRequestBatch zkVar) {
        Iterator it = zkVar.iterator();
        while (it.hasNext()) {
            GraphRequest graphRequest = (GraphRequest) it.next();
            if (HttpMethod.GET.equals(graphRequest.mo11555c()) && m10490b(graphRequest)) {
                Bundle e = graphRequest.mo11557e();
                if (!e.containsKey("fields") || abg.m374a(e.getString("fields"))) {
                    aaz.m197a(LoggingBehavior.DEVELOPER_ERRORS, 5, "Request", "starting with Graph API v2.4, GET requests for /%s should contain an explicit \"fields\" parameter.", graphRequest.mo11554b());
                }
            }
        }
    }

    /* renamed from: d */
    private static boolean m10496d(Object obj) {
        return (obj instanceof Bitmap) || (obj instanceof byte[]) || (obj instanceof Uri) || (obj instanceof ParcelFileDescriptor) || (obj instanceof ParcelableResourceWithMimeType);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public static boolean m10497e(Object obj) {
        return (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Number) || (obj instanceof Date);
    }

    /* renamed from: e */
    private static boolean m10498e(GraphRequestBatch zkVar) {
        for (C3120a aVar : zkVar.mo16966e()) {
            if (aVar instanceof C3121b) {
                return true;
            }
        }
        Iterator it = zkVar.iterator();
        while (it.hasNext()) {
            if (((GraphRequest) it.next()).mo11559g() instanceof C2007e) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public static String m10499f(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if ((obj instanceof Boolean) || (obj instanceof Number)) {
            return obj.toString();
        }
        if (obj instanceof Date) {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format(obj);
        }
        throw new IllegalArgumentException("Unsupported parameter type.");
    }

    /* renamed from: f */
    private static boolean m10500f(GraphRequestBatch zkVar) {
        Iterator it = zkVar.iterator();
        while (it.hasNext()) {
            GraphRequest graphRequest = (GraphRequest) it.next();
            Iterator it2 = graphRequest.f8041k.keySet().iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (m10496d(graphRequest.f8041k.get((String) it2.next()))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /* renamed from: g */
    private static String m10501g(GraphRequestBatch zkVar) {
        if (!abg.m374a(zkVar.mo16967f())) {
            return zkVar.mo16967f();
        }
        Iterator it = zkVar.iterator();
        while (it.hasNext()) {
            AccessToken accessToken = ((GraphRequest) it.next()).f8034d;
            if (accessToken != null) {
                String h = accessToken.mo11474h();
                if (h != null) {
                    return h;
                }
            }
        }
        return !abg.m374a(f8031b) ? f8031b : FacebookSdk.m17307j();
    }

    /* renamed from: m */
    private void m10502m() {
        if (this.f8034d != null) {
            if (!this.f8041k.containsKey("access_token")) {
                String b = this.f8034d.mo11466b();
                aaz.m200a(b);
                this.f8041k.putString("access_token", b);
            }
        } else if (!this.f8046p && !this.f8041k.containsKey("access_token")) {
            String j = FacebookSdk.m17307j();
            String l = FacebookSdk.m17309l();
            if (abg.m374a(j) || abg.m374a(l)) {
                Log.d(f8030a, "Warning: Request without access token missing application ID or client token.");
            } else {
                this.f8041k.putString("access_token", j + "|" + l);
            }
        }
        this.f8041k.putString("sdk", "android");
        this.f8041k.putString("format", "json");
        if (FacebookSdk.m17294a(LoggingBehavior.GRAPH_API_DEBUG_INFO)) {
            this.f8041k.putString("debug", "info");
        } else if (FacebookSdk.m17294a(LoggingBehavior.GRAPH_API_DEBUG_WARNING)) {
            this.f8041k.putString("debug", "warning");
        }
    }

    /* renamed from: n */
    private String m10503n() {
        if (f8032c.matcher(this.f8036f).matches()) {
            return this.f8036f;
        }
        return String.format("%s/%s", new Object[]{this.f8045o, this.f8036f});
    }

    /* renamed from: o */
    private static String m10504o() {
        return String.format("multipart/form-data; boundary=%s", new Object[]{"3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f"});
    }

    /* renamed from: p */
    private static String m10505p() {
        if (f8033q == null) {
            f8033q = String.format("%s.%s", new Object[]{"FBAndroidSDK", "4.18.0"});
            String a = aax.m190a();
            if (!abg.m374a(a)) {
                f8033q = String.format(Locale.ROOT, "%s/%s", new Object[]{f8033q, a});
            }
        }
        return f8033q;
    }

    /* renamed from: a */
    public final JSONObject mo11546a() {
        return this.f8037g;
    }

    /* renamed from: a */
    public final void mo11547a(Bundle bundle) {
        this.f8041k = bundle;
    }

    /* renamed from: a */
    public final void mo11548a(final C2004b bVar) {
        if (FacebookSdk.m17294a(LoggingBehavior.GRAPH_API_DEBUG_INFO) || FacebookSdk.m17294a(LoggingBehavior.GRAPH_API_DEBUG_WARNING)) {
            this.f8042l = new C2004b() {
                public void onCompleted(GraphResponse zlVar) {
                    JSONObject b = zlVar.mo16978b();
                    JSONObject jSONObject = b != null ? b.optJSONObject("__debug__") : null;
                    JSONArray jSONArray = jSONObject != null ? jSONObject.optJSONArray("messages") : null;
                    if (jSONArray != null) {
                        for (int i = 0; i < jSONArray.length(); i++) {
                            JSONObject optJSONObject = jSONArray.optJSONObject(i);
                            String str = optJSONObject != null ? optJSONObject.optString("message") : null;
                            String str2 = optJSONObject != null ? optJSONObject.optString("type") : null;
                            String str3 = optJSONObject != null ? optJSONObject.optString("link") : null;
                            if (!(str == null || str2 == null)) {
                                LoggingBehavior loggingBehavior = LoggingBehavior.GRAPH_API_DEBUG_INFO;
                                if (str2.equals("warning")) {
                                    loggingBehavior = LoggingBehavior.GRAPH_API_DEBUG_WARNING;
                                }
                                if (!abg.m374a(str3)) {
                                    str = str + " Link: " + str3;
                                }
                                aaz.m198a(loggingBehavior, GraphRequest.f8030a, str);
                            }
                        }
                    }
                    if (bVar != null) {
                        bVar.onCompleted(zlVar);
                    }
                }
            };
        } else {
            this.f8042l = bVar;
        }
    }

    /* renamed from: a */
    public final void mo11549a(HttpMethod httpMethod) {
        if (this.f8043m == null || httpMethod == HttpMethod.GET) {
            if (httpMethod == null) {
                httpMethod = HttpMethod.GET;
            }
            this.f8035e = httpMethod;
            return;
        }
        throw new FacebookException("Can't change HTTP method on request with overridden URL.");
    }

    /* renamed from: a */
    public final void mo11550a(Object obj) {
        this.f8044n = obj;
    }

    /* renamed from: a */
    public final void mo11551a(String str) {
        this.f8045o = str;
    }

    /* renamed from: a */
    public final void mo11552a(JSONObject jSONObject) {
        this.f8037g = jSONObject;
    }

    /* renamed from: a */
    public final void mo11553a(boolean z) {
        this.f8046p = z;
    }

    /* renamed from: b */
    public final String mo11554b() {
        return this.f8036f;
    }

    /* renamed from: c */
    public final HttpMethod mo11555c() {
        return this.f8035e;
    }

    /* renamed from: d */
    public final String mo11556d() {
        return this.f8045o;
    }

    /* renamed from: e */
    public final Bundle mo11557e() {
        return this.f8041k;
    }

    /* renamed from: f */
    public final AccessToken mo11558f() {
        return this.f8034d;
    }

    /* renamed from: g */
    public final C2004b mo11559g() {
        return this.f8042l;
    }

    /* renamed from: h */
    public final Object mo11560h() {
        return this.f8044n;
    }

    /* renamed from: i */
    public final GraphResponse mo11561i() {
        return m10475a(this);
    }

    /* renamed from: j */
    public final GraphRequestAsyncTask mo11562j() {
        return m10489b(this);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: k */
    public final String mo11563k() {
        if (this.f8043m != null) {
            throw new FacebookException("Can't override URL for a batch request");
        }
        String format = String.format("%s/%s", new Object[]{abe.m330b(), m10503n()});
        m10502m();
        Uri parse = Uri.parse(m10486b(format));
        return String.format("%s?%s", new Object[]{parse.getPath(), parse.getQuery()});
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: l */
    public final String mo11564l() {
        if (this.f8043m != null) {
            return this.f8043m.toString();
        }
        String format = String.format("%s/%s", new Object[]{(mo11555c() != HttpMethod.POST || this.f8036f == null || !this.f8036f.endsWith("/videos")) ? abe.m330b() : abe.m331c(), m10503n()});
        m10502m();
        return m10486b(format);
    }

    public String toString() {
        return "{Request: " + " accessToken: " + (this.f8034d == null ? "null" : this.f8034d) + ", graphPath: " + this.f8036f + ", graphObject: " + this.f8037g + ", httpMethod: " + this.f8035e + ", parameters: " + this.f8041k + "}";
    }
}
