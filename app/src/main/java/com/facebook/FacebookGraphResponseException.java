package com.facebook;

public class FacebookGraphResponseException extends FacebookException {

    /* renamed from: a */
    private final GraphResponse f8007a;

    public FacebookGraphResponseException(GraphResponse zlVar, String str) {
        super(str);
        this.f8007a = zlVar;
    }

    public final String toString() {
        FacebookRequestError facebookRequestError = this.f8007a != null ? this.f8007a.mo16977a() : null;
        StringBuilder append = new StringBuilder().append("{FacebookGraphResponseException: ");
        String message = getMessage();
        if (message != null) {
            append.append(message);
            append.append(" ");
        }
        if (facebookRequestError != null) {
            append.append("httpResponseCode: ").append(facebookRequestError.mo11530a()).append(", facebookErrorCode: ").append(facebookRequestError.mo11531b()).append(", facebookErrorType: ").append(facebookRequestError.mo11533d()).append(", message: ").append(facebookRequestError.mo11535e()).append("}");
        }
        return append.toString();
    }
}
