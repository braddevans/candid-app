package butterknife;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.util.Property;
import android.view.View;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class ButterKnife {
    static final Map<Class<?>, ViewBinder<Object>> BINDERS = new LinkedHashMap();
    static final ViewBinder<Object> NOP_VIEW_BINDER = new ViewBinder<Object>() {
        public Unbinder bind(Finder finder, Object obj, Object obj2) {
            return Unbinder.EMPTY;
        }
    };
    private static final String TAG = "ButterKnife";
    private static boolean debug = false;

    public interface Action<T extends View> {
        void apply(T t, int i);
    }

    public interface Setter<T extends View, V> {
        void set(T t, V v, int i);
    }

    private ButterKnife() {
        throw new AssertionError("No instances.");
    }

    @TargetApi(14)
    public static <T extends View, V> void apply(T t, Property<? super T, V> property, V v) {
        property.set(t, v);
    }

    public static <T extends View> void apply(T t, Action<? super T> action) {
        action.apply(t, 0);
    }

    public static <T extends View, V> void apply(T t, Setter<? super T, V> setter, V v) {
        setter.set(t, v, 0);
    }

    @SafeVarargs
    public static <T extends View> void apply(T t, Action<? super T>... actionArr) {
        for (Action<? super T> apply : actionArr) {
            apply.apply(t, 0);
        }
    }

    @TargetApi(14)
    public static <T extends View, V> void apply(List<T> list, Property<? super T, V> property, V v) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            property.set(list.get(i), v);
        }
    }

    public static <T extends View> void apply(List<T> list, Action<? super T> action) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            action.apply((View) list.get(i), i);
        }
    }

    public static <T extends View, V> void apply(List<T> list, Setter<? super T, V> setter, V v) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            setter.set((View) list.get(i), v, i);
        }
    }

    @SafeVarargs
    public static <T extends View> void apply(List<T> list, Action<? super T>... actionArr) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            for (Action<? super T> apply : actionArr) {
                apply.apply((View) list.get(i), i);
            }
        }
    }

    @TargetApi(14)
    public static <T extends View, V> void apply(T[] tArr, Property<? super T, V> property, V v) {
        for (T t : tArr) {
            property.set(t, v);
        }
    }

    public static <T extends View> void apply(T[] tArr, Action<? super T> action) {
        int length = tArr.length;
        for (int i = 0; i < length; i++) {
            action.apply(tArr[i], i);
        }
    }

    public static <T extends View, V> void apply(T[] tArr, Setter<? super T, V> setter, V v) {
        int length = tArr.length;
        for (int i = 0; i < length; i++) {
            setter.set(tArr[i], v, i);
        }
    }

    @SafeVarargs
    public static <T extends View> void apply(T[] tArr, Action<? super T>... actionArr) {
        int length = tArr.length;
        for (int i = 0; i < length; i++) {
            for (Action<? super T> apply : actionArr) {
                apply.apply(tArr[i], i);
            }
        }
    }

    public static Unbinder bind(Activity activity) {
        return bind(activity, activity, Finder.ACTIVITY);
    }

    public static Unbinder bind(Dialog dialog) {
        return bind(dialog, dialog, Finder.DIALOG);
    }

    public static Unbinder bind(View view) {
        return bind(view, view, Finder.VIEW);
    }

    public static Unbinder bind(Object obj, Activity activity) {
        return bind(obj, activity, Finder.ACTIVITY);
    }

    public static Unbinder bind(Object obj, Dialog dialog) {
        return bind(obj, dialog, Finder.DIALOG);
    }

    public static Unbinder bind(Object obj, View view) {
        return bind(obj, view, Finder.VIEW);
    }

    static Unbinder bind(Object obj, Object obj2, Finder finder) {
        Class cls = obj.getClass();
        try {
            if (debug) {
                Log.d(TAG, "Looking up view binder for " + cls.getName());
            }
            return findViewBinderForClass(cls).bind(finder, obj, obj2);
        } catch (Exception e) {
            throw new RuntimeException("Unable to bind views for " + cls.getName(), e);
        }
    }

    public static <T extends View> T findById(Activity activity, int i) {
        return activity.findViewById(i);
    }

    public static <T extends View> T findById(Dialog dialog, int i) {
        return dialog.findViewById(i);
    }

    public static <T extends View> T findById(View view, int i) {
        return view.findViewById(i);
    }

    private static ViewBinder<Object> findViewBinderForClass(Class<?> cls) throws IllegalAccessException, InstantiationException {
        ViewBinder findViewBinderForClass;
        ViewBinder viewBinder = (ViewBinder) BINDERS.get(cls);
        if (viewBinder != null) {
            if (debug) {
                Log.d(TAG, "HIT: Cached in view binder map.");
            }
            return viewBinder;
        }
        String name = cls.getName();
        if (name.startsWith("android.") || name.startsWith("java.")) {
            if (debug) {
                Log.d(TAG, "MISS: Reached framework class. Abandoning search.");
            }
            return NOP_VIEW_BINDER;
        }
        try {
            findViewBinderForClass = (ViewBinder) Class.forName(name + "$$ViewBinder").newInstance();
            if (debug) {
                Log.d(TAG, "HIT: Loaded view binder class.");
            }
        } catch (ClassNotFoundException e) {
            if (debug) {
                Log.d(TAG, "Not found. Trying superclass " + cls.getSuperclass().getName());
            }
            findViewBinderForClass = findViewBinderForClass(cls.getSuperclass());
        }
        BINDERS.put(cls, findViewBinderForClass);
        return findViewBinderForClass;
    }

    public static void setDebug(boolean z) {
        debug = z;
    }
}
