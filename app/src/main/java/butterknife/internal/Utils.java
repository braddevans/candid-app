package butterknife.internal;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.TypedValue;
import java.lang.reflect.Array;
import java.util.List;

public final class Utils {
    private static final boolean HAS_SUPPORT_V4 = hasSupportV4();

    static class SupportV4 {
        private static final TypedValue OUT_VALUE = new TypedValue();

        SupportV4() {
        }

        static Drawable getTintedDrawable(Resources resources, Theme theme, int i, int i2) {
            if (!theme.resolveAttribute(i2, OUT_VALUE, true)) {
                throw new NotFoundException("Required tint color attribute with name " + resources.getResourceEntryName(i2) + " and attribute ID " + i2 + " was not found.");
            }
            Drawable f = DrawableCompat.m8896f(Utils.getDrawable(resources, theme, i).mutate());
            DrawableCompat.m8884a(f, Utils.getColor(resources, theme, OUT_VALUE.resourceId));
            return f;
        }
    }

    private Utils() {
        throw new AssertionError("No instances.");
    }

    @SafeVarargs
    public static <T> T[] arrayOf(T... tArr) {
        return filterNull(tArr);
    }

    private static <T> T[] filterNull(T[] tArr) {
        int i;
        int length = tArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            T t = tArr[i2];
            if (t != null) {
                i = i3 + 1;
                tArr[i3] = t;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        if (i3 == length) {
            return tArr;
        }
        Object[] objArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), i3);
        System.arraycopy(tArr, 0, objArr, 0, i3);
        return objArr;
    }

    public static int getColor(Resources resources, Theme theme, int i) {
        return VERSION.SDK_INT < 23 ? resources.getColor(i) : resources.getColor(i, theme);
    }

    public static ColorStateList getColorStateList(Resources resources, Theme theme, int i) {
        return VERSION.SDK_INT < 23 ? resources.getColorStateList(i) : resources.getColorStateList(i, theme);
    }

    public static Drawable getDrawable(Resources resources, Theme theme, int i) {
        return VERSION.SDK_INT < 21 ? resources.getDrawable(i) : resources.getDrawable(i, theme);
    }

    public static Drawable getTintedDrawable(Resources resources, Theme theme, int i, int i2) {
        if (HAS_SUPPORT_V4) {
            return SupportV4.getTintedDrawable(resources, theme, i, i2);
        }
        throw new RuntimeException("Android support-v4 library is required for @BindDrawable with tint.");
    }

    private static boolean hasSupportV4() {
        try {
            Class.forName("bx");
            return true;
        } catch (ClassNotFoundException | VerifyError e) {
            return false;
        }
    }

    @SafeVarargs
    public static <T> List<T> listOf(T... tArr) {
        return new ImmutableList(filterNull(tArr));
    }
}
