package android.support.p003v7.graphics;

import android.support.p003v7.graphics.Palette.Swatch;
import java.util.List;

/* renamed from: android.support.v7.graphics.DefaultGenerator */
class DefaultGenerator extends Generator {
    private static final float MAX_DARK_LUMA = 0.45f;
    private static final float MAX_MUTED_SATURATION = 0.4f;
    private static final float MAX_NORMAL_LUMA = 0.7f;
    private static final float MIN_LIGHT_LUMA = 0.55f;
    private static final float MIN_NORMAL_LUMA = 0.3f;
    private static final float MIN_VIBRANT_SATURATION = 0.35f;
    private static final float TARGET_DARK_LUMA = 0.26f;
    private static final float TARGET_LIGHT_LUMA = 0.74f;
    private static final float TARGET_MUTED_SATURATION = 0.3f;
    private static final float TARGET_NORMAL_LUMA = 0.5f;
    private static final float TARGET_VIBRANT_SATURATION = 1.0f;
    private static final float WEIGHT_LUMA = 6.0f;
    private static final float WEIGHT_POPULATION = 1.0f;
    private static final float WEIGHT_SATURATION = 3.0f;
    private Swatch mDarkMutedSwatch;
    private Swatch mDarkVibrantSwatch;
    private int mHighestPopulation;
    private Swatch mLightMutedSwatch;
    private Swatch mLightVibrantSwatch;
    private Swatch mMutedSwatch;
    private List<Swatch> mSwatches;
    private Swatch mVibrantSwatch;

    DefaultGenerator() {
    }

    private static float[] copyHslValues(Swatch swatch) {
        float[] fArr = new float[3];
        System.arraycopy(swatch.getHsl(), 0, fArr, 0, 3);
        return fArr;
    }

    private static float createComparisonValue(float f, float f2, float f3, float f4, float f5, float f6, int i, int i2, float f7) {
        return weightedMean(invertDiff(f, f2), f3, invertDiff(f4, f5), f6, ((float) i) / ((float) i2), f7);
    }

    private static float createComparisonValue(float f, float f2, float f3, float f4, int i, int i2) {
        return createComparisonValue(f, f2, WEIGHT_SATURATION, f3, f4, WEIGHT_LUMA, i, i2, 1.0f);
    }

    private Swatch findColorVariation(float f, float f2, float f3, float f4, float f5, float f6) {
        Swatch swatch = null;
        float f7 = 0.0f;
        for (Swatch swatch2 : this.mSwatches) {
            float f8 = swatch2.getHsl()[1];
            float f9 = swatch2.getHsl()[2];
            if (f8 >= f5 && f8 <= f6 && f9 >= f2 && f9 <= f3 && !isAlreadySelected(swatch2)) {
                float createComparisonValue = createComparisonValue(f8, f4, f9, f, swatch2.getPopulation(), this.mHighestPopulation);
                if (swatch == null || createComparisonValue > f7) {
                    swatch = swatch2;
                    f7 = createComparisonValue;
                }
            }
        }
        return swatch;
    }

    private int findMaxPopulation() {
        int i = 0;
        for (Swatch population : this.mSwatches) {
            i = Math.max(i, population.getPopulation());
        }
        return i;
    }

    private void generateEmptySwatches() {
        if (this.mVibrantSwatch == null && this.mDarkVibrantSwatch != null) {
            float[] copyHslValues = copyHslValues(this.mDarkVibrantSwatch);
            copyHslValues[2] = 0.5f;
            this.mVibrantSwatch = new Swatch(ColorUtils.m8873a(copyHslValues), 0);
        }
        if (this.mDarkVibrantSwatch == null && this.mVibrantSwatch != null) {
            float[] copyHslValues2 = copyHslValues(this.mVibrantSwatch);
            copyHslValues2[2] = 0.26f;
            this.mDarkVibrantSwatch = new Swatch(ColorUtils.m8873a(copyHslValues2), 0);
        }
    }

    private void generateVariationColors() {
        this.mVibrantSwatch = findColorVariation(TARGET_NORMAL_LUMA, 0.3f, MAX_NORMAL_LUMA, 1.0f, MIN_VIBRANT_SATURATION, 1.0f);
        this.mLightVibrantSwatch = findColorVariation(TARGET_LIGHT_LUMA, MIN_LIGHT_LUMA, 1.0f, 1.0f, MIN_VIBRANT_SATURATION, 1.0f);
        this.mDarkVibrantSwatch = findColorVariation(TARGET_DARK_LUMA, 0.0f, MAX_DARK_LUMA, 1.0f, MIN_VIBRANT_SATURATION, 1.0f);
        this.mMutedSwatch = findColorVariation(TARGET_NORMAL_LUMA, 0.3f, MAX_NORMAL_LUMA, 0.3f, 0.0f, MAX_MUTED_SATURATION);
        this.mLightMutedSwatch = findColorVariation(TARGET_LIGHT_LUMA, MIN_LIGHT_LUMA, 1.0f, 0.3f, 0.0f, MAX_MUTED_SATURATION);
        this.mDarkMutedSwatch = findColorVariation(TARGET_DARK_LUMA, 0.0f, MAX_DARK_LUMA, 0.3f, 0.0f, MAX_MUTED_SATURATION);
    }

    private static float invertDiff(float f, float f2) {
        return 1.0f - Math.abs(f - f2);
    }

    private boolean isAlreadySelected(Swatch swatch) {
        return this.mVibrantSwatch == swatch || this.mDarkVibrantSwatch == swatch || this.mLightVibrantSwatch == swatch || this.mMutedSwatch == swatch || this.mDarkMutedSwatch == swatch || this.mLightMutedSwatch == swatch;
    }

    private static float weightedMean(float... fArr) {
        float f = 0.0f;
        float f2 = 0.0f;
        for (int i = 0; i < fArr.length; i += 2) {
            float f3 = fArr[i];
            float f4 = fArr[i + 1];
            f += f3 * f4;
            f2 += f4;
        }
        return f / f2;
    }

    public void generate(List<Swatch> list) {
        this.mSwatches = list;
        this.mHighestPopulation = findMaxPopulation();
        generateVariationColors();
        generateEmptySwatches();
    }

    public Swatch getDarkMutedSwatch() {
        return this.mDarkMutedSwatch;
    }

    public Swatch getDarkVibrantSwatch() {
        return this.mDarkVibrantSwatch;
    }

    public Swatch getLightMutedSwatch() {
        return this.mLightMutedSwatch;
    }

    public Swatch getLightVibrantSwatch() {
        return this.mLightVibrantSwatch;
    }

    public Swatch getMutedSwatch() {
        return this.mMutedSwatch;
    }

    public Swatch getVibrantSwatch() {
        return this.mVibrantSwatch;
    }
}
