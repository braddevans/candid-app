package android.support.p003v7.graphics;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.util.TimingLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: android.support.v7.graphics.Palette */
public final class Palette {
    private static final int DEFAULT_CALCULATE_NUMBER_COLORS = 16;
    /* access modifiers changed from: private */
    public static final Filter DEFAULT_FILTER = new Filter() {
        private static final float BLACK_MAX_LIGHTNESS = 0.05f;
        private static final float WHITE_MIN_LIGHTNESS = 0.95f;

        private boolean isBlack(float[] fArr) {
            return fArr[2] <= BLACK_MAX_LIGHTNESS;
        }

        private boolean isNearRedILine(float[] fArr) {
            return fArr[0] >= 10.0f && fArr[0] <= 37.0f && fArr[1] <= 0.82f;
        }

        private boolean isWhite(float[] fArr) {
            return fArr[2] >= WHITE_MIN_LIGHTNESS;
        }

        public boolean isAllowed(int i, float[] fArr) {
            return !isWhite(fArr) && !isBlack(fArr) && !isNearRedILine(fArr);
        }
    };
    private static final int DEFAULT_RESIZE_BITMAP_MAX_DIMENSION = 192;
    private static final String LOG_TAG = "Palette";
    private static final boolean LOG_TIMINGS = false;
    private static final float MIN_CONTRAST_BODY_TEXT = 4.5f;
    private static final float MIN_CONTRAST_TITLE_TEXT = 3.0f;
    private final Generator mGenerator;
    private final List<Swatch> mSwatches;

    /* renamed from: android.support.v7.graphics.Palette$Builder */
    public static final class Builder {
        private final Bitmap mBitmap;
        private final List<Filter> mFilters = new ArrayList();
        private Generator mGenerator;
        private int mMaxColors = 16;
        private Rect mRegion;
        private int mResizeMaxDimension = Palette.DEFAULT_RESIZE_BITMAP_MAX_DIMENSION;
        private final List<Swatch> mSwatches;

        public Builder(Bitmap bitmap) {
            if (bitmap == null || bitmap.isRecycled()) {
                throw new IllegalArgumentException("Bitmap is not valid");
            }
            this.mFilters.add(Palette.DEFAULT_FILTER);
            this.mBitmap = bitmap;
            this.mSwatches = null;
        }

        public Builder(List<Swatch> list) {
            if (list == null || list.isEmpty()) {
                throw new IllegalArgumentException("List of Swatches is not valid");
            }
            this.mFilters.add(Palette.DEFAULT_FILTER);
            this.mSwatches = list;
            this.mBitmap = null;
        }

        private int[] getPixelsFromBitmap(Bitmap bitmap) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int[] iArr = new int[(width * height)];
            if (this.mRegion == null) {
                bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
                return iArr;
            }
            int width2 = this.mRegion.width();
            int height2 = this.mRegion.height();
            bitmap.getPixels(iArr, 0, width, this.mRegion.left, this.mRegion.top, width2, height2);
            int[] iArr2 = new int[(width2 * height2)];
            for (int i = 0; i < height2; i++) {
                System.arraycopy(iArr, ((this.mRegion.top + i) * width) + this.mRegion.left, iArr2, i * width2, width2);
            }
            return iArr2;
        }

        public Builder addFilter(Filter filter) {
            if (filter != null) {
                this.mFilters.add(filter);
            }
            return this;
        }

        public Builder clearFilters() {
            this.mFilters.clear();
            return this;
        }

        public Builder clearRegion() {
            this.mRegion = null;
            return this;
        }

        public AsyncTask<Bitmap, Void, Palette> generate(final PaletteAsyncListener paletteAsyncListener) {
            if (paletteAsyncListener == null) {
                throw new IllegalArgumentException("listener can not be null");
            }
            return AsyncTaskCompat.m12480a(new AsyncTask<Bitmap, Void, Palette>() {
                /* access modifiers changed from: protected */
                public Palette doInBackground(Bitmap... bitmapArr) {
                    try {
                        return Builder.this.generate();
                    } catch (Exception e) {
                        Log.e(Palette.LOG_TAG, "Exception thrown during async generate", e);
                        return null;
                    }
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Palette palette) {
                    paletteAsyncListener.onGenerated(palette);
                }
            }, this.mBitmap);
        }

        public Palette generate() {
            List<Swatch> list;
            TimingLogger timingLogger = null;
            if (this.mBitmap == null) {
                list = this.mSwatches;
            } else if (this.mResizeMaxDimension <= 0) {
                throw new IllegalArgumentException("Minimum dimension size for resizing should should be >= 1");
            } else {
                Bitmap access$100 = Palette.scaleBitmapDown(this.mBitmap, this.mResizeMaxDimension);
                if (timingLogger != null) {
                    timingLogger.addSplit("Processed Bitmap");
                }
                Rect rect = this.mRegion;
                if (!(access$100 == this.mBitmap || rect == null)) {
                    float width = ((float) access$100.getWidth()) / ((float) this.mBitmap.getWidth());
                    rect.left = (int) Math.floor((double) (((float) rect.left) * width));
                    rect.top = (int) Math.floor((double) (((float) rect.top) * width));
                    rect.right = (int) Math.ceil((double) (((float) rect.right) * width));
                    rect.bottom = (int) Math.ceil((double) (((float) rect.bottom) * width));
                }
                ColorCutQuantizer colorCutQuantizer = new ColorCutQuantizer(getPixelsFromBitmap(access$100), this.mMaxColors, this.mFilters.isEmpty() ? null : (Filter[]) this.mFilters.toArray(new Filter[this.mFilters.size()]));
                if (access$100 != this.mBitmap) {
                    access$100.recycle();
                }
                list = colorCutQuantizer.getQuantizedColors();
                if (timingLogger != null) {
                    timingLogger.addSplit("Color quantization completed");
                }
            }
            if (this.mGenerator == null) {
                this.mGenerator = new DefaultGenerator();
            }
            this.mGenerator.generate(list);
            if (timingLogger != null) {
                timingLogger.addSplit("Generator.generate() completed");
            }
            Palette palette = new Palette(list, this.mGenerator);
            if (timingLogger != null) {
                timingLogger.addSplit("Created Palette");
                timingLogger.dumpToLog();
            }
            return palette;
        }

        /* access modifiers changed from: 0000 */
        public Builder generator(Generator generator) {
            this.mGenerator = generator;
            return this;
        }

        public Builder maximumColorCount(int i) {
            this.mMaxColors = i;
            return this;
        }

        public Builder resizeBitmapSize(int i) {
            this.mResizeMaxDimension = i;
            return this;
        }

        public Builder setRegion(int i, int i2, int i3, int i4) {
            if (this.mBitmap != null) {
                if (this.mRegion == null) {
                    this.mRegion = new Rect();
                }
                this.mRegion.set(0, 0, this.mBitmap.getWidth(), this.mBitmap.getHeight());
                if (!this.mRegion.intersect(i, i2, i3, i4)) {
                    throw new IllegalArgumentException("The given region must intersect with the Bitmap's dimensions.");
                }
            }
            return this;
        }
    }

    /* renamed from: android.support.v7.graphics.Palette$Filter */
    public interface Filter {
        boolean isAllowed(int i, float[] fArr);
    }

    /* renamed from: android.support.v7.graphics.Palette$Generator */
    static abstract class Generator {
        Generator() {
        }

        public abstract void generate(List<Swatch> list);

        public Swatch getDarkMutedSwatch() {
            return null;
        }

        public Swatch getDarkVibrantSwatch() {
            return null;
        }

        public Swatch getLightMutedSwatch() {
            return null;
        }

        public Swatch getLightVibrantSwatch() {
            return null;
        }

        public Swatch getMutedSwatch() {
            return null;
        }

        public Swatch getVibrantSwatch() {
            return null;
        }
    }

    /* renamed from: android.support.v7.graphics.Palette$PaletteAsyncListener */
    public interface PaletteAsyncListener {
        void onGenerated(Palette palette);
    }

    /* renamed from: android.support.v7.graphics.Palette$Swatch */
    public static final class Swatch {
        private final int mBlue;
        private int mBodyTextColor;
        private boolean mGeneratedTextColors;
        private final int mGreen;
        private float[] mHsl;
        private final int mPopulation;
        private final int mRed;
        private final int mRgb;
        private int mTitleTextColor;

        public Swatch(int i, int i2) {
            this.mRed = Color.red(i);
            this.mGreen = Color.green(i);
            this.mBlue = Color.blue(i);
            this.mRgb = i;
            this.mPopulation = i2;
        }

        Swatch(int i, int i2, int i3, int i4) {
            this.mRed = i;
            this.mGreen = i2;
            this.mBlue = i3;
            this.mRgb = Color.rgb(i, i2, i3);
            this.mPopulation = i4;
        }

        private void ensureTextColorsGenerated() {
            if (!this.mGeneratedTextColors) {
                int a = ColorUtils.m8870a(-1, this.mRgb, (float) Palette.MIN_CONTRAST_BODY_TEXT);
                int a2 = ColorUtils.m8870a(-1, this.mRgb, (float) Palette.MIN_CONTRAST_TITLE_TEXT);
                if (a == -1 || a2 == -1) {
                    int a3 = ColorUtils.m8870a(-16777216, this.mRgb, (float) Palette.MIN_CONTRAST_BODY_TEXT);
                    int a4 = ColorUtils.m8870a(-16777216, this.mRgb, (float) Palette.MIN_CONTRAST_TITLE_TEXT);
                    if (a3 == -1 || a3 == -1) {
                        this.mBodyTextColor = a != -1 ? ColorUtils.m8880c(-1, a) : ColorUtils.m8880c(-16777216, a3);
                        this.mTitleTextColor = a2 != -1 ? ColorUtils.m8880c(-1, a2) : ColorUtils.m8880c(-16777216, a4);
                        this.mGeneratedTextColors = true;
                        return;
                    }
                    this.mBodyTextColor = ColorUtils.m8880c(-16777216, a3);
                    this.mTitleTextColor = ColorUtils.m8880c(-16777216, a4);
                    this.mGeneratedTextColors = true;
                    return;
                }
                this.mBodyTextColor = ColorUtils.m8880c(-1, a);
                this.mTitleTextColor = ColorUtils.m8880c(-1, a2);
                this.mGeneratedTextColors = true;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Swatch swatch = (Swatch) obj;
            return this.mPopulation == swatch.mPopulation && this.mRgb == swatch.mRgb;
        }

        public int getBodyTextColor() {
            ensureTextColorsGenerated();
            return this.mBodyTextColor;
        }

        public float[] getHsl() {
            if (this.mHsl == null) {
                this.mHsl = new float[3];
                ColorUtils.m8875a(this.mRed, this.mGreen, this.mBlue, this.mHsl);
            }
            return this.mHsl;
        }

        public int getPopulation() {
            return this.mPopulation;
        }

        public int getRgb() {
            return this.mRgb;
        }

        public int getTitleTextColor() {
            ensureTextColorsGenerated();
            return this.mTitleTextColor;
        }

        public int hashCode() {
            return (this.mRgb * 31) + this.mPopulation;
        }

        public String toString() {
            return new StringBuilder(getClass().getSimpleName()).append(" [RGB: #").append(Integer.toHexString(getRgb())).append(']').append(" [HSL: ").append(Arrays.toString(getHsl())).append(']').append(" [Population: ").append(this.mPopulation).append(']').append(" [Title Text: #").append(Integer.toHexString(getTitleTextColor())).append(']').append(" [Body Text: #").append(Integer.toHexString(getBodyTextColor())).append(']').toString();
        }
    }

    private Palette(List<Swatch> list, Generator generator) {
        this.mSwatches = list;
        this.mGenerator = generator;
    }

    public static Builder from(Bitmap bitmap) {
        return new Builder(bitmap);
    }

    public static Palette from(List<Swatch> list) {
        return new Builder(list).generate();
    }

    @Deprecated
    public static Palette generate(Bitmap bitmap) {
        return from(bitmap).generate();
    }

    @Deprecated
    public static Palette generate(Bitmap bitmap, int i) {
        return from(bitmap).maximumColorCount(i).generate();
    }

    @Deprecated
    public static AsyncTask<Bitmap, Void, Palette> generateAsync(Bitmap bitmap, int i, PaletteAsyncListener paletteAsyncListener) {
        return from(bitmap).maximumColorCount(i).generate(paletteAsyncListener);
    }

    @Deprecated
    public static AsyncTask<Bitmap, Void, Palette> generateAsync(Bitmap bitmap, PaletteAsyncListener paletteAsyncListener) {
        return from(bitmap).generate(paletteAsyncListener);
    }

    /* access modifiers changed from: private */
    public static Bitmap scaleBitmapDown(Bitmap bitmap, int i) {
        int max = Math.max(bitmap.getWidth(), bitmap.getHeight());
        if (max <= i) {
            return bitmap;
        }
        double d = ((double) i) / ((double) max);
        return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * d), (int) Math.ceil(((double) bitmap.getHeight()) * d), false);
    }

    public int getDarkMutedColor(int i) {
        Swatch darkMutedSwatch = getDarkMutedSwatch();
        return darkMutedSwatch != null ? darkMutedSwatch.getRgb() : i;
    }

    public Swatch getDarkMutedSwatch() {
        return this.mGenerator.getDarkMutedSwatch();
    }

    public int getDarkVibrantColor(int i) {
        Swatch darkVibrantSwatch = getDarkVibrantSwatch();
        return darkVibrantSwatch != null ? darkVibrantSwatch.getRgb() : i;
    }

    public Swatch getDarkVibrantSwatch() {
        return this.mGenerator.getDarkVibrantSwatch();
    }

    public int getLightMutedColor(int i) {
        Swatch lightMutedSwatch = getLightMutedSwatch();
        return lightMutedSwatch != null ? lightMutedSwatch.getRgb() : i;
    }

    public Swatch getLightMutedSwatch() {
        return this.mGenerator.getLightMutedSwatch();
    }

    public int getLightVibrantColor(int i) {
        Swatch lightVibrantSwatch = getLightVibrantSwatch();
        return lightVibrantSwatch != null ? lightVibrantSwatch.getRgb() : i;
    }

    public Swatch getLightVibrantSwatch() {
        return this.mGenerator.getLightVibrantSwatch();
    }

    public int getMutedColor(int i) {
        Swatch mutedSwatch = getMutedSwatch();
        return mutedSwatch != null ? mutedSwatch.getRgb() : i;
    }

    public Swatch getMutedSwatch() {
        return this.mGenerator.getMutedSwatch();
    }

    public List<Swatch> getSwatches() {
        return Collections.unmodifiableList(this.mSwatches);
    }

    public int getVibrantColor(int i) {
        Swatch vibrantSwatch = getVibrantSwatch();
        return vibrantSwatch != null ? vibrantSwatch.getRgb() : i;
    }

    public Swatch getVibrantSwatch() {
        return this.mGenerator.getVibrantSwatch();
    }
}
