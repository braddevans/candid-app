package android.support.p003v7.graphics;

import android.graphics.Color;
import android.support.p003v7.graphics.Palette.Filter;
import android.support.p003v7.graphics.Palette.Swatch;
import android.util.TimingLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/* renamed from: android.support.v7.graphics.ColorCutQuantizer */
final class ColorCutQuantizer {
    private static final int COMPONENT_BLUE = -1;
    private static final int COMPONENT_GREEN = -2;
    private static final int COMPONENT_RED = -3;
    private static final String LOG_TAG = "ColorCutQuantizer";
    private static final boolean LOG_TIMINGS = false;
    private static final int QUANTIZE_WORD_MASK = 31;
    private static final int QUANTIZE_WORD_WIDTH = 5;
    private static final Comparator<Vbox> VBOX_COMPARATOR_VOLUME = new Comparator<Vbox>() {
        public int compare(Vbox vbox, Vbox vbox2) {
            return vbox2.getVolume() - vbox.getVolume();
        }
    };
    final int[] mColors;
    final Filter[] mFilters;
    final int[] mHistogram;
    final List<Swatch> mQuantizedColors;
    private final float[] mTempHsl = new float[3];
    final TimingLogger mTimingLogger = null;

    /* renamed from: android.support.v7.graphics.ColorCutQuantizer$Vbox */
    class Vbox {
        private int mLowerIndex;
        private int mMaxBlue;
        private int mMaxGreen;
        private int mMaxRed;
        private int mMinBlue;
        private int mMinGreen;
        private int mMinRed;
        private int mPopulation;
        private int mUpperIndex;

        Vbox(int i, int i2) {
            this.mLowerIndex = i;
            this.mUpperIndex = i2;
            fitBox();
        }

        /* access modifiers changed from: 0000 */
        public final boolean canSplit() {
            return getColorCount() > 1;
        }

        /* access modifiers changed from: 0000 */
        public final int findSplitPoint() {
            int longestColorDimension = getLongestColorDimension();
            int[] iArr = ColorCutQuantizer.this.mColors;
            int[] iArr2 = ColorCutQuantizer.this.mHistogram;
            ColorCutQuantizer.modifySignificantOctet(iArr, longestColorDimension, this.mLowerIndex, this.mUpperIndex);
            Arrays.sort(iArr, this.mLowerIndex, this.mUpperIndex + 1);
            ColorCutQuantizer.modifySignificantOctet(iArr, longestColorDimension, this.mLowerIndex, this.mUpperIndex);
            int i = this.mPopulation / 2;
            int i2 = 0;
            for (int i3 = this.mLowerIndex; i3 <= this.mUpperIndex; i3++) {
                i2 += iArr2[iArr[i3]];
                if (i2 >= i) {
                    return i3;
                }
            }
            return this.mLowerIndex;
        }

        /* access modifiers changed from: 0000 */
        public final void fitBox() {
            int[] iArr = ColorCutQuantizer.this.mColors;
            int[] iArr2 = ColorCutQuantizer.this.mHistogram;
            int i = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            int i2 = Integer.MAX_VALUE;
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MIN_VALUE;
            int i5 = Integer.MIN_VALUE;
            int i6 = Integer.MIN_VALUE;
            int i7 = 0;
            for (int i8 = this.mLowerIndex; i8 <= this.mUpperIndex; i8++) {
                int i9 = iArr[i8];
                i7 += iArr2[i9];
                int access$000 = ColorCutQuantizer.quantizedRed(i9);
                int access$100 = ColorCutQuantizer.quantizedGreen(i9);
                int access$200 = ColorCutQuantizer.quantizedBlue(i9);
                if (access$000 > i6) {
                    i6 = access$000;
                }
                if (access$000 < i3) {
                    i3 = access$000;
                }
                if (access$100 > i5) {
                    i5 = access$100;
                }
                if (access$100 < i2) {
                    i2 = access$100;
                }
                if (access$200 > i4) {
                    i4 = access$200;
                }
                if (access$200 < i) {
                    i = access$200;
                }
            }
            this.mMinRed = i3;
            this.mMaxRed = i6;
            this.mMinGreen = i2;
            this.mMaxGreen = i5;
            this.mMinBlue = i;
            this.mMaxBlue = i4;
            this.mPopulation = i7;
        }

        /* access modifiers changed from: 0000 */
        public final Swatch getAverageColor() {
            int[] iArr = ColorCutQuantizer.this.mColors;
            int[] iArr2 = ColorCutQuantizer.this.mHistogram;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            for (int i5 = this.mLowerIndex; i5 <= this.mUpperIndex; i5++) {
                int i6 = iArr[i5];
                int i7 = iArr2[i6];
                i4 += i7;
                i += ColorCutQuantizer.quantizedRed(i6) * i7;
                i2 += ColorCutQuantizer.quantizedGreen(i6) * i7;
                i3 += ColorCutQuantizer.quantizedBlue(i6) * i7;
            }
            return new Swatch(ColorCutQuantizer.approximateToRgb888(Math.round(((float) i) / ((float) i4)), Math.round(((float) i2) / ((float) i4)), Math.round(((float) i3) / ((float) i4))), i4);
        }

        /* access modifiers changed from: 0000 */
        public final int getColorCount() {
            return (this.mUpperIndex + 1) - this.mLowerIndex;
        }

        /* access modifiers changed from: 0000 */
        public final int getLongestColorDimension() {
            int i = this.mMaxRed - this.mMinRed;
            int i2 = this.mMaxGreen - this.mMinGreen;
            int i3 = this.mMaxBlue - this.mMinBlue;
            return (i < i2 || i < i3) ? (i2 < i || i2 < i3) ? -1 : -2 : ColorCutQuantizer.COMPONENT_RED;
        }

        /* access modifiers changed from: 0000 */
        public final int getVolume() {
            return ((this.mMaxRed - this.mMinRed) + 1) * ((this.mMaxGreen - this.mMinGreen) + 1) * ((this.mMaxBlue - this.mMinBlue) + 1);
        }

        /* access modifiers changed from: 0000 */
        public final Vbox splitBox() {
            if (!canSplit()) {
                throw new IllegalStateException("Can not split a box with only 1 color");
            }
            int findSplitPoint = findSplitPoint();
            Vbox vbox = new Vbox(findSplitPoint + 1, this.mUpperIndex);
            this.mUpperIndex = findSplitPoint;
            fitBox();
            return vbox;
        }
    }

    ColorCutQuantizer(int[] iArr, int i, Filter[] filterArr) {
        int[] iArr2;
        this.mFilters = filterArr;
        int[] iArr3 = new int[32768];
        this.mHistogram = iArr3;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            int quantizeFromRgb888 = quantizeFromRgb888(iArr[i2]);
            iArr[i2] = quantizeFromRgb888;
            iArr3[quantizeFromRgb888] = iArr3[quantizeFromRgb888] + 1;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < iArr3.length; i4++) {
            if (iArr3[i4] > 0 && shouldIgnoreColor(i4)) {
                iArr3[i4] = 0;
            }
            if (iArr3[i4] > 0) {
                i3++;
            }
        }
        int[] iArr4 = new int[i3];
        this.mColors = iArr4;
        int i5 = 0;
        for (int i6 = 0; i6 < iArr3.length; i6++) {
            if (iArr3[i6] > 0) {
                int i7 = i5 + 1;
                iArr4[i5] = i6;
                i5 = i7;
            }
        }
        if (i3 <= i) {
            this.mQuantizedColors = new ArrayList();
            for (int i8 : iArr4) {
                this.mQuantizedColors.add(new Swatch(approximateToRgb888(i8), iArr3[i8]));
            }
            return;
        }
        this.mQuantizedColors = quantizePixels(i);
    }

    private static int approximateToRgb888(int i) {
        return approximateToRgb888(quantizedRed(i), quantizedGreen(i), quantizedBlue(i));
    }

    /* access modifiers changed from: private */
    public static int approximateToRgb888(int i, int i2, int i3) {
        return Color.rgb(modifyWordWidth(i, 5, 8), modifyWordWidth(i2, 5, 8), modifyWordWidth(i3, 5, 8));
    }

    private List<Swatch> generateAverageColors(Collection<Vbox> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (Vbox averageColor : collection) {
            Swatch averageColor2 = averageColor.getAverageColor();
            if (!shouldIgnoreColor(averageColor2)) {
                arrayList.add(averageColor2);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public static void modifySignificantOctet(int[] iArr, int i, int i2, int i3) {
        switch (i) {
            case -2:
                for (int i4 = i2; i4 <= i3; i4++) {
                    int i5 = iArr[i4];
                    iArr[i4] = (quantizedGreen(i5) << 10) | (quantizedRed(i5) << 5) | quantizedBlue(i5);
                }
                return;
            case -1:
                for (int i6 = i2; i6 <= i3; i6++) {
                    int i7 = iArr[i6];
                    iArr[i6] = (quantizedBlue(i7) << 10) | (quantizedGreen(i7) << 5) | quantizedRed(i7);
                }
                return;
            default:
                return;
        }
    }

    private static int modifyWordWidth(int i, int i2, int i3) {
        return ((1 << i3) - 1) & (i3 > i2 ? i << (i3 - i2) : i >> (i2 - i3));
    }

    private static int quantizeFromRgb888(int i) {
        int modifyWordWidth = modifyWordWidth(Color.red(i), 8, 5);
        int modifyWordWidth2 = modifyWordWidth(Color.green(i), 8, 5);
        return (modifyWordWidth << 10) | (modifyWordWidth2 << 5) | modifyWordWidth(Color.blue(i), 8, 5);
    }

    private List<Swatch> quantizePixels(int i) {
        PriorityQueue priorityQueue = new PriorityQueue(i, VBOX_COMPARATOR_VOLUME);
        priorityQueue.offer(new Vbox(0, this.mColors.length - 1));
        splitBoxes(priorityQueue, i);
        return generateAverageColors(priorityQueue);
    }

    /* access modifiers changed from: private */
    public static int quantizedBlue(int i) {
        return i & 31;
    }

    /* access modifiers changed from: private */
    public static int quantizedGreen(int i) {
        return (i >> 5) & 31;
    }

    /* access modifiers changed from: private */
    public static int quantizedRed(int i) {
        return (i >> 10) & 31;
    }

    private boolean shouldIgnoreColor(int i) {
        int approximateToRgb888 = approximateToRgb888(i);
        ColorUtils.m8877a(approximateToRgb888, this.mTempHsl);
        return shouldIgnoreColor(approximateToRgb888, this.mTempHsl);
    }

    private boolean shouldIgnoreColor(int i, float[] fArr) {
        if (this.mFilters != null && this.mFilters.length > 0) {
            for (Filter isAllowed : this.mFilters) {
                if (!isAllowed.isAllowed(i, fArr)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean shouldIgnoreColor(Swatch swatch) {
        return shouldIgnoreColor(swatch.getRgb(), swatch.getHsl());
    }

    private void splitBoxes(PriorityQueue<Vbox> priorityQueue, int i) {
        while (priorityQueue.size() < i) {
            Vbox vbox = (Vbox) priorityQueue.poll();
            if (vbox != null && vbox.canSplit()) {
                priorityQueue.offer(vbox.splitBox());
                priorityQueue.offer(vbox);
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public List<Swatch> getQuantizedColors() {
        return this.mQuantizedColors;
    }
}
