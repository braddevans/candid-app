package android.support.p003v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.p003v7.appcompat.C0553R;
import android.support.p003v7.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.util.Xml;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: android.support.v7.widget.AppCompatDrawableManager */
public final class AppCompatDrawableManager {
    private static final int[] COLORFILTER_COLOR_BACKGROUND_MULTIPLY = {C0553R.C0554drawable.abc_popup_background_mtrl_mult, C0553R.C0554drawable.abc_cab_background_internal_bg, C0553R.C0554drawable.abc_menu_hardkey_panel_mtrl_mult};
    private static final int[] COLORFILTER_COLOR_CONTROL_ACTIVATED = {C0553R.C0554drawable.abc_textfield_activated_mtrl_alpha, C0553R.C0554drawable.abc_textfield_search_activated_mtrl_alpha, C0553R.C0554drawable.abc_cab_background_top_mtrl_alpha, C0553R.C0554drawable.abc_text_cursor_material, C0553R.C0554drawable.abc_text_select_handle_left_mtrl_dark, C0553R.C0554drawable.abc_text_select_handle_middle_mtrl_dark, C0553R.C0554drawable.abc_text_select_handle_right_mtrl_dark, C0553R.C0554drawable.abc_text_select_handle_left_mtrl_light, C0553R.C0554drawable.abc_text_select_handle_middle_mtrl_light, C0553R.C0554drawable.abc_text_select_handle_right_mtrl_light};
    private static final int[] COLORFILTER_TINT_COLOR_CONTROL_NORMAL = {C0553R.C0554drawable.abc_textfield_search_default_mtrl_alpha, C0553R.C0554drawable.abc_textfield_default_mtrl_alpha, C0553R.C0554drawable.abc_ab_share_pack_mtrl_alpha};
    private static final ColorFilterLruCache COLOR_FILTER_CACHE = new ColorFilterLruCache(6);
    private static final boolean DEBUG = false;
    private static final Mode DEFAULT_MODE = Mode.SRC_IN;
    private static AppCompatDrawableManager INSTANCE = null;
    private static final String PLATFORM_VD_CLAZZ = "android.graphics.drawable.VectorDrawable";
    private static final String SKIP_DRAWABLE_TAG = "appcompat_skip_skip";
    private static final String TAG = "AppCompatDrawableManager";
    private static final int[] TINT_CHECKABLE_BUTTON_LIST = {C0553R.C0554drawable.abc_btn_check_material, C0553R.C0554drawable.abc_btn_radio_material};
    private static final int[] TINT_COLOR_CONTROL_NORMAL = {C0553R.C0554drawable.abc_ic_commit_search_api_mtrl_alpha, C0553R.C0554drawable.abc_seekbar_tick_mark_material, C0553R.C0554drawable.abc_ic_menu_share_mtrl_alpha, C0553R.C0554drawable.abc_ic_menu_copy_mtrl_am_alpha, C0553R.C0554drawable.abc_ic_menu_cut_mtrl_alpha, C0553R.C0554drawable.abc_ic_menu_selectall_mtrl_alpha, C0553R.C0554drawable.abc_ic_menu_paste_mtrl_am_alpha};
    private static final int[] TINT_COLOR_CONTROL_STATE_LIST = {C0553R.C0554drawable.abc_tab_indicator_material, C0553R.C0554drawable.abc_textfield_search_material};
    private ArrayMap<String, InflateDelegate> mDelegates;
    private final Object mDrawableCacheLock = new Object();
    private final WeakHashMap<Context, LongSparseArray<WeakReference<ConstantState>>> mDrawableCaches = new WeakHashMap<>(0);
    private boolean mHasCheckedVectorDrawableSetup;
    private SparseArray<String> mKnownDrawableIdTags;
    private WeakHashMap<Context, SparseArray<ColorStateList>> mTintLists;
    private TypedValue mTypedValue;

    /* renamed from: android.support.v7.widget.AppCompatDrawableManager$AvdcInflateDelegate */
    static class AvdcInflateDelegate implements InflateDelegate {
        AvdcInflateDelegate() {
        }

        public Drawable createFromXmlInner(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            try {
                return AnimatedVectorDrawableCompat.m2427a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    /* renamed from: android.support.v7.widget.AppCompatDrawableManager$ColorFilterLruCache */
    static class ColorFilterLruCache extends LruCache<Integer, PorterDuffColorFilter> {
        public ColorFilterLruCache(int i) {
            super(i);
        }

        private static int generateCacheKey(int i, Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        /* access modifiers changed from: 0000 */
        public PorterDuffColorFilter get(int i, Mode mode) {
            return (PorterDuffColorFilter) get(Integer.valueOf(generateCacheKey(i, mode)));
        }

        /* access modifiers changed from: 0000 */
        public PorterDuffColorFilter put(int i, Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) put(Integer.valueOf(generateCacheKey(i, mode)), porterDuffColorFilter);
        }
    }

    /* renamed from: android.support.v7.widget.AppCompatDrawableManager$InflateDelegate */
    interface InflateDelegate {
        Drawable createFromXmlInner(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme);
    }

    /* renamed from: android.support.v7.widget.AppCompatDrawableManager$VdcInflateDelegate */
    static class VdcInflateDelegate implements InflateDelegate {
        VdcInflateDelegate() {
        }

        public Drawable createFromXmlInner(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Theme theme) {
            try {
                return VectorDrawableCompat.m4139a(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    private void addDelegate(String str, InflateDelegate inflateDelegate) {
        if (this.mDelegates == null) {
            this.mDelegates = new ArrayMap<>();
        }
        this.mDelegates.put(str, inflateDelegate);
    }

    private boolean addDrawableToCache(Context context, long j, Drawable drawable) {
        ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        synchronized (this.mDrawableCacheLock) {
            LongSparseArray doVar = (LongSparseArray) this.mDrawableCaches.get(context);
            if (doVar == null) {
                doVar = new LongSparseArray();
                this.mDrawableCaches.put(context, doVar);
            }
            doVar.mo13207b(j, new WeakReference(constantState));
        }
        return true;
    }

    private void addTintListToCache(Context context, int i, ColorStateList colorStateList) {
        if (this.mTintLists == null) {
            this.mTintLists = new WeakHashMap<>();
        }
        SparseArray sparseArray = (SparseArray) this.mTintLists.get(context);
        if (sparseArray == null) {
            sparseArray = new SparseArray();
            this.mTintLists.put(context, sparseArray);
        }
        sparseArray.append(i, colorStateList);
    }

    private static boolean arrayContains(int[] iArr, int i) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    private void checkVectorDrawableSetup(Context context) {
        if (!this.mHasCheckedVectorDrawableSetup) {
            this.mHasCheckedVectorDrawableSetup = true;
            Drawable drawable = getDrawable(context, C0553R.C0554drawable.abc_vector_test);
            if (drawable == null || !isVectorDrawable(drawable)) {
                this.mHasCheckedVectorDrawableSetup = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    private ColorStateList createBorderlessButtonColorStateList(Context context, ColorStateList colorStateList) {
        return createButtonColorStateList(context, 0, null);
    }

    private ColorStateList createButtonColorStateList(Context context, int i, ColorStateList colorStateList) {
        int[][] iArr = new int[4][];
        int[] iArr2 = new int[4];
        int themeAttrColor = ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorControlHighlight);
        int disabledThemeAttrColor = ThemeUtils.getDisabledThemeAttrColor(context, C0553R.attr.colorButtonNormal);
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        if (colorStateList != null) {
            disabledThemeAttrColor = colorStateList.getColorForState(iArr[0], 0);
        }
        iArr2[0] = disabledThemeAttrColor;
        int i2 = 0 + 1;
        iArr[i2] = ThemeUtils.PRESSED_STATE_SET;
        iArr2[i2] = ColorUtils.m8869a(themeAttrColor, colorStateList == null ? i : colorStateList.getColorForState(iArr[i2], 0));
        int i3 = i2 + 1;
        iArr[i3] = ThemeUtils.FOCUSED_STATE_SET;
        iArr2[i3] = ColorUtils.m8869a(themeAttrColor, colorStateList == null ? i : colorStateList.getColorForState(iArr[i3], 0));
        int i4 = i3 + 1;
        iArr[i4] = ThemeUtils.EMPTY_STATE_SET;
        if (colorStateList != null) {
            i = colorStateList.getColorForState(iArr[i4], 0);
        }
        iArr2[i4] = i;
        int i5 = i4 + 1;
        return new ColorStateList(iArr, iArr2);
    }

    private static long createCacheKey(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    private ColorStateList createColoredButtonColorStateList(Context context, ColorStateList colorStateList) {
        return createButtonColorStateList(context, ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorAccent), colorStateList);
    }

    private ColorStateList createDefaultButtonColorStateList(Context context, ColorStateList colorStateList) {
        return createButtonColorStateList(context, ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorButtonNormal), colorStateList);
    }

    private Drawable createDrawableIfNeeded(Context context, int i) {
        if (this.mTypedValue == null) {
            this.mTypedValue = new TypedValue();
        }
        TypedValue typedValue = this.mTypedValue;
        context.getResources().getValue(i, typedValue, true);
        long createCacheKey = createCacheKey(typedValue);
        Drawable cachedDrawable = getCachedDrawable(context, createCacheKey);
        if (cachedDrawable != null) {
            return cachedDrawable;
        }
        if (i == C0553R.C0554drawable.abc_cab_background_top_material) {
            cachedDrawable = new LayerDrawable(new Drawable[]{getDrawable(context, C0553R.C0554drawable.abc_cab_background_internal_bg), getDrawable(context, C0553R.C0554drawable.abc_cab_background_top_mtrl_alpha)});
        }
        if (cachedDrawable != null) {
            cachedDrawable.setChangingConfigurations(typedValue.changingConfigurations);
            addDrawableToCache(context, createCacheKey, cachedDrawable);
        }
        return cachedDrawable;
    }

    private static PorterDuffColorFilter createTintFilter(ColorStateList colorStateList, Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return getPorterDuffColorFilter(colorStateList.getColorForState(iArr, 0), mode);
    }

    public static AppCompatDrawableManager get() {
        if (INSTANCE == null) {
            INSTANCE = new AppCompatDrawableManager();
            installDefaultInflateDelegates(INSTANCE);
        }
        return INSTANCE;
    }

    private Drawable getCachedDrawable(Context context, long j) {
        Drawable drawable = null;
        synchronized (this.mDrawableCacheLock) {
            LongSparseArray doVar = (LongSparseArray) this.mDrawableCaches.get(context);
            if (doVar != null) {
                WeakReference weakReference = (WeakReference) doVar.mo13201a(j);
                if (weakReference != null) {
                    ConstantState constantState = (ConstantState) weakReference.get();
                    if (constantState != null) {
                        drawable = constantState.newDrawable(context.getResources());
                    } else {
                        doVar.mo13206b(j);
                    }
                }
            }
        }
        return drawable;
    }

    public static PorterDuffColorFilter getPorterDuffColorFilter(int i, Mode mode) {
        PorterDuffColorFilter porterDuffColorFilter = COLOR_FILTER_CACHE.get(i, mode);
        if (porterDuffColorFilter != null) {
            return porterDuffColorFilter;
        }
        PorterDuffColorFilter porterDuffColorFilter2 = new PorterDuffColorFilter(i, mode);
        COLOR_FILTER_CACHE.put(i, mode, porterDuffColorFilter2);
        return porterDuffColorFilter2;
    }

    private ColorStateList getTintListFromCache(Context context, int i) {
        if (this.mTintLists == null) {
            return null;
        }
        SparseArray sparseArray = (SparseArray) this.mTintLists.get(context);
        if (sparseArray != null) {
            return (ColorStateList) sparseArray.get(i);
        }
        return null;
    }

    static Mode getTintMode(int i) {
        if (i == C0553R.C0554drawable.abc_switch_thumb_material) {
            return Mode.MULTIPLY;
        }
        return null;
    }

    private static void installDefaultInflateDelegates(AppCompatDrawableManager appCompatDrawableManager) {
        int i = VERSION.SDK_INT;
        if (i < 24) {
            appCompatDrawableManager.addDelegate("vector", new VdcInflateDelegate());
            if (i >= 11) {
                appCompatDrawableManager.addDelegate("animated-vector", new AvdcInflateDelegate());
            }
        }
    }

    private static boolean isVectorDrawable(Drawable drawable) {
        return (drawable instanceof VectorDrawableCompat) || PLATFORM_VD_CLAZZ.equals(drawable.getClass().getName());
    }

    private Drawable loadDrawableFromDelegates(Context context, int i) {
        int next;
        if (this.mDelegates == null || this.mDelegates.isEmpty()) {
            return null;
        }
        if (this.mKnownDrawableIdTags != null) {
            String str = (String) this.mKnownDrawableIdTags.get(i);
            if (SKIP_DRAWABLE_TAG.equals(str) || (str != null && this.mDelegates.get(str) == null)) {
                return null;
            }
        } else {
            this.mKnownDrawableIdTags = new SparseArray<>();
        }
        if (this.mTypedValue == null) {
            this.mTypedValue = new TypedValue();
        }
        TypedValue typedValue = this.mTypedValue;
        Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        long createCacheKey = createCacheKey(typedValue);
        Drawable cachedDrawable = getCachedDrawable(context, createCacheKey);
        if (cachedDrawable != null) {
            return cachedDrawable;
        }
        if (typedValue.string != null && typedValue.string.toString().endsWith(".xml")) {
            try {
                XmlResourceParser xml = resources.getXml(i);
                AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
                do {
                    next = xml.next();
                    if (next == 2) {
                        break;
                    }
                } while (next != 1);
                if (next != 2) {
                    throw new XmlPullParserException("No start tag found");
                }
                String name = xml.getName();
                this.mKnownDrawableIdTags.append(i, name);
                InflateDelegate inflateDelegate = (InflateDelegate) this.mDelegates.get(name);
                if (inflateDelegate != null) {
                    cachedDrawable = inflateDelegate.createFromXmlInner(context, xml, asAttributeSet, context.getTheme());
                }
                if (cachedDrawable != null) {
                    cachedDrawable.setChangingConfigurations(typedValue.changingConfigurations);
                    if (addDrawableToCache(context, createCacheKey, cachedDrawable)) {
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception while inflating drawable", e);
            }
        }
        if (cachedDrawable != null) {
            return cachedDrawable;
        }
        this.mKnownDrawableIdTags.append(i, SKIP_DRAWABLE_TAG);
        return cachedDrawable;
    }

    private void removeDelegate(String str, InflateDelegate inflateDelegate) {
        if (this.mDelegates != null && this.mDelegates.get(str) == inflateDelegate) {
            this.mDelegates.remove(str);
        }
    }

    private static void setPorterDuffColorFilter(Drawable drawable, int i, Mode mode) {
        if (DrawableUtils.canSafelyMutateDrawable(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = DEFAULT_MODE;
        }
        drawable.setColorFilter(getPorterDuffColorFilter(i, mode));
    }

    private Drawable tintDrawable(Context context, int i, boolean z, Drawable drawable) {
        ColorStateList tintList = getTintList(context, i);
        if (tintList != null) {
            if (DrawableUtils.canSafelyMutateDrawable(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable f = DrawableCompat.m8896f(drawable);
            DrawableCompat.m8886a(f, tintList);
            Mode tintMode = getTintMode(i);
            if (tintMode == null) {
                return f;
            }
            DrawableCompat.m8889a(f, tintMode);
            return f;
        } else if (i == C0553R.C0554drawable.abc_seekbar_track_material) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            setPorterDuffColorFilter(layerDrawable.findDrawableByLayerId(16908288), ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorControlNormal), DEFAULT_MODE);
            setPorterDuffColorFilter(layerDrawable.findDrawableByLayerId(16908303), ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorControlNormal), DEFAULT_MODE);
            setPorterDuffColorFilter(layerDrawable.findDrawableByLayerId(16908301), ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorControlActivated), DEFAULT_MODE);
            return drawable;
        } else if (i == C0553R.C0554drawable.abc_ratingbar_material || i == C0553R.C0554drawable.abc_ratingbar_indicator_material || i == C0553R.C0554drawable.abc_ratingbar_small_material) {
            LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
            setPorterDuffColorFilter(layerDrawable2.findDrawableByLayerId(16908288), ThemeUtils.getDisabledThemeAttrColor(context, C0553R.attr.colorControlNormal), DEFAULT_MODE);
            setPorterDuffColorFilter(layerDrawable2.findDrawableByLayerId(16908303), ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorControlActivated), DEFAULT_MODE);
            setPorterDuffColorFilter(layerDrawable2.findDrawableByLayerId(16908301), ThemeUtils.getThemeAttrColor(context, C0553R.attr.colorControlActivated), DEFAULT_MODE);
            return drawable;
        } else if (tintDrawableUsingColorFilter(context, i, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    static void tintDrawable(Drawable drawable, TintInfo tintInfo, int[] iArr) {
        if (!DrawableUtils.canSafelyMutateDrawable(drawable) || drawable.mutate() == drawable) {
            if (tintInfo.mHasTintList || tintInfo.mHasTintMode) {
                drawable.setColorFilter(createTintFilter(tintInfo.mHasTintList ? tintInfo.mTintList : null, tintInfo.mHasTintMode ? tintInfo.mTintMode : DEFAULT_MODE, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d(TAG, "Mutated drawable is not the same instance as the input.");
    }

    static boolean tintDrawableUsingColorFilter(Context context, int i, Drawable drawable) {
        Mode mode = DEFAULT_MODE;
        boolean z = false;
        int i2 = 0;
        int i3 = -1;
        if (arrayContains(COLORFILTER_TINT_COLOR_CONTROL_NORMAL, i)) {
            i2 = C0553R.attr.colorControlNormal;
            z = true;
        } else if (arrayContains(COLORFILTER_COLOR_CONTROL_ACTIVATED, i)) {
            i2 = C0553R.attr.colorControlActivated;
            z = true;
        } else if (arrayContains(COLORFILTER_COLOR_BACKGROUND_MULTIPLY, i)) {
            i2 = 16842801;
            z = true;
            mode = Mode.MULTIPLY;
        } else if (i == C0553R.C0554drawable.abc_list_divider_mtrl_alpha) {
            i2 = 16842800;
            z = true;
            i3 = Math.round(40.8f);
        } else if (i == C0553R.C0554drawable.abc_dialog_material_background) {
            i2 = 16842801;
            z = true;
        }
        if (!z) {
            return false;
        }
        if (DrawableUtils.canSafelyMutateDrawable(drawable)) {
            drawable = drawable.mutate();
        }
        drawable.setColorFilter(getPorterDuffColorFilter(ThemeUtils.getThemeAttrColor(context, i2), mode));
        if (i3 != -1) {
            drawable.setAlpha(i3);
        }
        return true;
    }

    public Drawable getDrawable(Context context, int i) {
        return getDrawable(context, i, false);
    }

    /* access modifiers changed from: 0000 */
    public Drawable getDrawable(Context context, int i, boolean z) {
        checkVectorDrawableSetup(context);
        Drawable loadDrawableFromDelegates = loadDrawableFromDelegates(context, i);
        if (loadDrawableFromDelegates == null) {
            loadDrawableFromDelegates = createDrawableIfNeeded(context, i);
        }
        if (loadDrawableFromDelegates == null) {
            loadDrawableFromDelegates = ContextCompat.getDrawable(context, i);
        }
        if (loadDrawableFromDelegates != null) {
            loadDrawableFromDelegates = tintDrawable(context, i, z, loadDrawableFromDelegates);
        }
        if (loadDrawableFromDelegates != null) {
            DrawableUtils.fixDrawable(loadDrawableFromDelegates);
        }
        return loadDrawableFromDelegates;
    }

    /* access modifiers changed from: 0000 */
    public ColorStateList getTintList(Context context, int i) {
        return getTintList(context, i, null);
    }

    /* access modifiers changed from: 0000 */
    public ColorStateList getTintList(Context context, int i, ColorStateList colorStateList) {
        boolean z = colorStateList == null;
        ColorStateList colorStateList2 = z ? getTintListFromCache(context, i) : null;
        if (colorStateList2 == null) {
            if (i == C0553R.C0554drawable.abc_edit_text_material) {
                colorStateList2 = AppCompatResources.getColorStateList(context, C0553R.color.abc_tint_edittext);
            } else if (i == C0553R.C0554drawable.abc_switch_track_mtrl_alpha) {
                colorStateList2 = AppCompatResources.getColorStateList(context, C0553R.color.abc_tint_switch_track);
            } else if (i == C0553R.C0554drawable.abc_switch_thumb_material) {
                colorStateList2 = AppCompatResources.getColorStateList(context, C0553R.color.abc_tint_switch_thumb);
            } else if (i == C0553R.C0554drawable.abc_btn_default_mtrl_shape) {
                colorStateList2 = createDefaultButtonColorStateList(context, colorStateList);
            } else if (i == C0553R.C0554drawable.abc_btn_borderless_material) {
                colorStateList2 = createBorderlessButtonColorStateList(context, colorStateList);
            } else if (i == C0553R.C0554drawable.abc_btn_colored_material) {
                colorStateList2 = createColoredButtonColorStateList(context, colorStateList);
            } else if (i == C0553R.C0554drawable.abc_spinner_mtrl_am_alpha || i == C0553R.C0554drawable.abc_spinner_textfield_background_material) {
                colorStateList2 = AppCompatResources.getColorStateList(context, C0553R.color.abc_tint_spinner);
            } else if (arrayContains(TINT_COLOR_CONTROL_NORMAL, i)) {
                colorStateList2 = ThemeUtils.getThemeAttrColorStateList(context, C0553R.attr.colorControlNormal);
            } else if (arrayContains(TINT_COLOR_CONTROL_STATE_LIST, i)) {
                colorStateList2 = AppCompatResources.getColorStateList(context, C0553R.color.abc_tint_default);
            } else if (arrayContains(TINT_CHECKABLE_BUTTON_LIST, i)) {
                colorStateList2 = AppCompatResources.getColorStateList(context, C0553R.color.abc_tint_btn_checkable);
            } else if (i == C0553R.C0554drawable.abc_seekbar_thumb_material) {
                colorStateList2 = AppCompatResources.getColorStateList(context, C0553R.color.abc_tint_seek_thumb);
            }
            if (z && colorStateList2 != null) {
                addTintListToCache(context, i, colorStateList2);
            }
        }
        return colorStateList2;
    }

    public void onConfigurationChanged(Context context) {
        synchronized (this.mDrawableCacheLock) {
            LongSparseArray doVar = (LongSparseArray) this.mDrawableCaches.get(context);
            if (doVar != null) {
                doVar.mo13209c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public Drawable onDrawableLoadedFromResources(Context context, VectorEnabledTintResources vectorEnabledTintResources, int i) {
        Drawable loadDrawableFromDelegates = loadDrawableFromDelegates(context, i);
        if (loadDrawableFromDelegates == null) {
            loadDrawableFromDelegates = vectorEnabledTintResources.superGetDrawable(i);
        }
        if (loadDrawableFromDelegates != null) {
            return tintDrawable(context, i, false, loadDrawableFromDelegates);
        }
        return null;
    }
}
