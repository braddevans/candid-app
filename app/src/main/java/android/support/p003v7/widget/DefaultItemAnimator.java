package android.support.p003v7.widget;

import android.support.p003v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: android.support.v7.widget.DefaultItemAnimator */
public class DefaultItemAnimator extends SimpleItemAnimator {
    private static final boolean DEBUG = false;
    /* access modifiers changed from: private */
    public ArrayList<ViewHolder> mAddAnimations = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<ArrayList<ViewHolder>> mAdditionsList = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<ViewHolder> mChangeAnimations = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<ArrayList<ChangeInfo>> mChangesList = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<ViewHolder> mMoveAnimations = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<ArrayList<MoveInfo>> mMovesList = new ArrayList<>();
    private ArrayList<ViewHolder> mPendingAdditions = new ArrayList<>();
    private ArrayList<ChangeInfo> mPendingChanges = new ArrayList<>();
    private ArrayList<MoveInfo> mPendingMoves = new ArrayList<>();
    private ArrayList<ViewHolder> mPendingRemovals = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<ViewHolder> mRemoveAnimations = new ArrayList<>();

    /* renamed from: android.support.v7.widget.DefaultItemAnimator$ChangeInfo */
    static class ChangeInfo {
        public int fromX;
        public int fromY;
        public ViewHolder newHolder;
        public ViewHolder oldHolder;
        public int toX;
        public int toY;

        private ChangeInfo(ViewHolder viewHolder, ViewHolder viewHolder2) {
            this.oldHolder = viewHolder;
            this.newHolder = viewHolder2;
        }

        private ChangeInfo(ViewHolder viewHolder, ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
            this(viewHolder, viewHolder2);
            this.fromX = i;
            this.fromY = i2;
            this.toX = i3;
            this.toY = i4;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.oldHolder + ", newHolder=" + this.newHolder + ", fromX=" + this.fromX + ", fromY=" + this.fromY + ", toX=" + this.toX + ", toY=" + this.toY + '}';
        }
    }

    /* renamed from: android.support.v7.widget.DefaultItemAnimator$MoveInfo */
    static class MoveInfo {
        public int fromX;
        public int fromY;
        public ViewHolder holder;
        public int toX;
        public int toY;

        private MoveInfo(ViewHolder viewHolder, int i, int i2, int i3, int i4) {
            this.holder = viewHolder;
            this.fromX = i;
            this.fromY = i2;
            this.toX = i3;
            this.toY = i4;
        }
    }

    /* renamed from: android.support.v7.widget.DefaultItemAnimator$VpaListenerAdapter */
    static class VpaListenerAdapter implements ViewPropertyAnimatorListener {
        private VpaListenerAdapter() {
        }

        public void onAnimationCancel(View view) {
        }

        public void onAnimationEnd(View view) {
        }

        public void onAnimationStart(View view) {
        }
    }

    /* access modifiers changed from: private */
    public void animateAddImpl(final ViewHolder viewHolder) {
        final ViewPropertyAnimatorCompat s = ViewCompat.m12934s(viewHolder.itemView);
        this.mAddAnimations.add(viewHolder);
        s.mo13535a(1.0f).mo13536a(getAddDuration()).mo13538a((ViewPropertyAnimatorListener) new VpaListenerAdapter() {
            public void onAnimationCancel(View view) {
                ViewCompat.m12909c(view, 1.0f);
            }

            public void onAnimationEnd(View view) {
                s.mo13538a((ViewPropertyAnimatorListener) null);
                DefaultItemAnimator.this.dispatchAddFinished(viewHolder);
                DefaultItemAnimator.this.mAddAnimations.remove(viewHolder);
                DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }

            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.dispatchAddStarting(viewHolder);
            }
        }).mo13544c();
    }

    /* access modifiers changed from: private */
    public void animateChangeImpl(final ChangeInfo changeInfo) {
        ViewHolder viewHolder = changeInfo.oldHolder;
        View view = viewHolder == null ? null : viewHolder.itemView;
        ViewHolder viewHolder2 = changeInfo.newHolder;
        final View view2 = viewHolder2 != null ? viewHolder2.itemView : null;
        if (view != null) {
            final ViewPropertyAnimatorCompat a = ViewCompat.m12934s(view).mo13536a(getChangeDuration());
            this.mChangeAnimations.add(changeInfo.oldHolder);
            a.mo13540b((float) (changeInfo.toX - changeInfo.fromX));
            a.mo13543c((float) (changeInfo.toY - changeInfo.fromY));
            a.mo13535a(0.0f).mo13538a((ViewPropertyAnimatorListener) new VpaListenerAdapter() {
                public void onAnimationEnd(View view) {
                    a.mo13538a((ViewPropertyAnimatorListener) null);
                    ViewCompat.m12909c(view, 1.0f);
                    ViewCompat.m12885a(view, 0.0f);
                    ViewCompat.m12904b(view, 0.0f);
                    DefaultItemAnimator.this.dispatchChangeFinished(changeInfo.oldHolder, true);
                    DefaultItemAnimator.this.mChangeAnimations.remove(changeInfo.oldHolder);
                    DefaultItemAnimator.this.dispatchFinishedWhenDone();
                }

                public void onAnimationStart(View view) {
                    DefaultItemAnimator.this.dispatchChangeStarting(changeInfo.oldHolder, true);
                }
            }).mo13544c();
        }
        if (view2 != null) {
            final ViewPropertyAnimatorCompat s = ViewCompat.m12934s(view2);
            this.mChangeAnimations.add(changeInfo.newHolder);
            s.mo13540b(0.0f).mo13543c(0.0f).mo13536a(getChangeDuration()).mo13535a(1.0f).mo13538a((ViewPropertyAnimatorListener) new VpaListenerAdapter() {
                public void onAnimationEnd(View view) {
                    s.mo13538a((ViewPropertyAnimatorListener) null);
                    ViewCompat.m12909c(view2, 1.0f);
                    ViewCompat.m12885a(view2, 0.0f);
                    ViewCompat.m12904b(view2, 0.0f);
                    DefaultItemAnimator.this.dispatchChangeFinished(changeInfo.newHolder, false);
                    DefaultItemAnimator.this.mChangeAnimations.remove(changeInfo.newHolder);
                    DefaultItemAnimator.this.dispatchFinishedWhenDone();
                }

                public void onAnimationStart(View view) {
                    DefaultItemAnimator.this.dispatchChangeStarting(changeInfo.newHolder, false);
                }
            }).mo13544c();
        }
    }

    /* access modifiers changed from: private */
    public void animateMoveImpl(ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        View view = viewHolder.itemView;
        final int i5 = i3 - i;
        final int i6 = i4 - i2;
        if (i5 != 0) {
            ViewCompat.m12934s(view).mo13540b(0.0f);
        }
        if (i6 != 0) {
            ViewCompat.m12934s(view).mo13543c(0.0f);
        }
        final ViewPropertyAnimatorCompat s = ViewCompat.m12934s(view);
        this.mMoveAnimations.add(viewHolder);
        final ViewHolder viewHolder2 = viewHolder;
        s.mo13536a(getMoveDuration()).mo13538a((ViewPropertyAnimatorListener) new VpaListenerAdapter() {
            public void onAnimationCancel(View view) {
                if (i5 != 0) {
                    ViewCompat.m12885a(view, 0.0f);
                }
                if (i6 != 0) {
                    ViewCompat.m12904b(view, 0.0f);
                }
            }

            public void onAnimationEnd(View view) {
                s.mo13538a((ViewPropertyAnimatorListener) null);
                DefaultItemAnimator.this.dispatchMoveFinished(viewHolder2);
                DefaultItemAnimator.this.mMoveAnimations.remove(viewHolder2);
                DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }

            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.dispatchMoveStarting(viewHolder2);
            }
        }).mo13544c();
    }

    private void animateRemoveImpl(final ViewHolder viewHolder) {
        final ViewPropertyAnimatorCompat s = ViewCompat.m12934s(viewHolder.itemView);
        this.mRemoveAnimations.add(viewHolder);
        s.mo13536a(getRemoveDuration()).mo13535a(0.0f).mo13538a((ViewPropertyAnimatorListener) new VpaListenerAdapter() {
            public void onAnimationEnd(View view) {
                s.mo13538a((ViewPropertyAnimatorListener) null);
                ViewCompat.m12909c(view, 1.0f);
                DefaultItemAnimator.this.dispatchRemoveFinished(viewHolder);
                DefaultItemAnimator.this.mRemoveAnimations.remove(viewHolder);
                DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }

            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.dispatchRemoveStarting(viewHolder);
            }
        }).mo13544c();
    }

    /* access modifiers changed from: private */
    public void dispatchFinishedWhenDone() {
        if (!isRunning()) {
            dispatchAnimationsFinished();
        }
    }

    private void endChangeAnimation(List<ChangeInfo> list, ViewHolder viewHolder) {
        for (int size = list.size() - 1; size >= 0; size--) {
            ChangeInfo changeInfo = (ChangeInfo) list.get(size);
            if (endChangeAnimationIfNecessary(changeInfo, viewHolder) && changeInfo.oldHolder == null && changeInfo.newHolder == null) {
                list.remove(changeInfo);
            }
        }
    }

    private void endChangeAnimationIfNecessary(ChangeInfo changeInfo) {
        if (changeInfo.oldHolder != null) {
            endChangeAnimationIfNecessary(changeInfo, changeInfo.oldHolder);
        }
        if (changeInfo.newHolder != null) {
            endChangeAnimationIfNecessary(changeInfo, changeInfo.newHolder);
        }
    }

    private boolean endChangeAnimationIfNecessary(ChangeInfo changeInfo, ViewHolder viewHolder) {
        boolean z = false;
        if (changeInfo.newHolder == viewHolder) {
            changeInfo.newHolder = null;
        } else if (changeInfo.oldHolder != viewHolder) {
            return false;
        } else {
            changeInfo.oldHolder = null;
            z = true;
        }
        ViewCompat.m12909c(viewHolder.itemView, 1.0f);
        ViewCompat.m12885a(viewHolder.itemView, 0.0f);
        ViewCompat.m12904b(viewHolder.itemView, 0.0f);
        dispatchChangeFinished(viewHolder, z);
        return true;
    }

    private void resetAnimation(ViewHolder viewHolder) {
        AnimatorCompatHelper.m5437a(viewHolder.itemView);
        endAnimation(viewHolder);
    }

    public boolean animateAdd(ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        ViewCompat.m12909c(viewHolder.itemView, 0.0f);
        this.mPendingAdditions.add(viewHolder);
        return true;
    }

    public boolean animateChange(ViewHolder viewHolder, ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
        if (viewHolder == viewHolder2) {
            return animateMove(viewHolder, i, i2, i3, i4);
        }
        float o = ViewCompat.m12930o(viewHolder.itemView);
        float p = ViewCompat.m12931p(viewHolder.itemView);
        float f = ViewCompat.m12919f(viewHolder.itemView);
        resetAnimation(viewHolder);
        int i5 = (int) (((float) (i3 - i)) - o);
        int i6 = (int) (((float) (i4 - i2)) - p);
        ViewCompat.m12885a(viewHolder.itemView, o);
        ViewCompat.m12904b(viewHolder.itemView, p);
        ViewCompat.m12909c(viewHolder.itemView, f);
        if (viewHolder2 != null) {
            resetAnimation(viewHolder2);
            ViewCompat.m12885a(viewHolder2.itemView, (float) (-i5));
            ViewCompat.m12904b(viewHolder2.itemView, (float) (-i6));
            ViewCompat.m12909c(viewHolder2.itemView, 0.0f);
        }
        this.mPendingChanges.add(new ChangeInfo(viewHolder, viewHolder2, i, i2, i3, i4));
        return true;
    }

    public boolean animateMove(ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        View view = viewHolder.itemView;
        int o = (int) (((float) i) + ViewCompat.m12930o(viewHolder.itemView));
        int p = (int) (((float) i2) + ViewCompat.m12931p(viewHolder.itemView));
        resetAnimation(viewHolder);
        int i5 = i3 - o;
        int i6 = i4 - p;
        if (i5 == 0 && i6 == 0) {
            dispatchMoveFinished(viewHolder);
            return false;
        }
        if (i5 != 0) {
            ViewCompat.m12885a(view, (float) (-i5));
        }
        if (i6 != 0) {
            ViewCompat.m12904b(view, (float) (-i6));
        }
        this.mPendingMoves.add(new MoveInfo(viewHolder, o, p, i3, i4));
        return true;
    }

    public boolean animateRemove(ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        this.mPendingRemovals.add(viewHolder);
        return true;
    }

    public boolean canReuseUpdatedViewHolder(ViewHolder viewHolder, List<Object> list) {
        return !list.isEmpty() || super.canReuseUpdatedViewHolder(viewHolder, list);
    }

    /* access modifiers changed from: 0000 */
    public void cancelAll(List<ViewHolder> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            ViewCompat.m12934s(((ViewHolder) list.get(size)).itemView).mo13542b();
        }
    }

    public void endAnimation(ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        ViewCompat.m12934s(view).mo13542b();
        for (int size = this.mPendingMoves.size() - 1; size >= 0; size--) {
            if (((MoveInfo) this.mPendingMoves.get(size)).holder == viewHolder) {
                ViewCompat.m12904b(view, 0.0f);
                ViewCompat.m12885a(view, 0.0f);
                dispatchMoveFinished(viewHolder);
                this.mPendingMoves.remove(size);
            }
        }
        endChangeAnimation(this.mPendingChanges, viewHolder);
        if (this.mPendingRemovals.remove(viewHolder)) {
            ViewCompat.m12909c(view, 1.0f);
            dispatchRemoveFinished(viewHolder);
        }
        if (this.mPendingAdditions.remove(viewHolder)) {
            ViewCompat.m12909c(view, 1.0f);
            dispatchAddFinished(viewHolder);
        }
        for (int size2 = this.mChangesList.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = (ArrayList) this.mChangesList.get(size2);
            endChangeAnimation(arrayList, viewHolder);
            if (arrayList.isEmpty()) {
                this.mChangesList.remove(size2);
            }
        }
        for (int size3 = this.mMovesList.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = (ArrayList) this.mMovesList.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((MoveInfo) arrayList2.get(size4)).holder == viewHolder) {
                    ViewCompat.m12904b(view, 0.0f);
                    ViewCompat.m12885a(view, 0.0f);
                    dispatchMoveFinished(viewHolder);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.mMovesList.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.mAdditionsList.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = (ArrayList) this.mAdditionsList.get(size5);
            if (arrayList3.remove(viewHolder)) {
                ViewCompat.m12909c(view, 1.0f);
                dispatchAddFinished(viewHolder);
                if (arrayList3.isEmpty()) {
                    this.mAdditionsList.remove(size5);
                }
            }
        }
        if (this.mRemoveAnimations.remove(viewHolder)) {
        }
        if (this.mAddAnimations.remove(viewHolder)) {
        }
        if (this.mChangeAnimations.remove(viewHolder)) {
        }
        if (this.mMoveAnimations.remove(viewHolder)) {
        }
        dispatchFinishedWhenDone();
    }

    public void endAnimations() {
        for (int size = this.mPendingMoves.size() - 1; size >= 0; size--) {
            MoveInfo moveInfo = (MoveInfo) this.mPendingMoves.get(size);
            View view = moveInfo.holder.itemView;
            ViewCompat.m12904b(view, 0.0f);
            ViewCompat.m12885a(view, 0.0f);
            dispatchMoveFinished(moveInfo.holder);
            this.mPendingMoves.remove(size);
        }
        for (int size2 = this.mPendingRemovals.size() - 1; size2 >= 0; size2--) {
            dispatchRemoveFinished((ViewHolder) this.mPendingRemovals.get(size2));
            this.mPendingRemovals.remove(size2);
        }
        for (int size3 = this.mPendingAdditions.size() - 1; size3 >= 0; size3--) {
            ViewHolder viewHolder = (ViewHolder) this.mPendingAdditions.get(size3);
            ViewCompat.m12909c(viewHolder.itemView, 1.0f);
            dispatchAddFinished(viewHolder);
            this.mPendingAdditions.remove(size3);
        }
        for (int size4 = this.mPendingChanges.size() - 1; size4 >= 0; size4--) {
            endChangeAnimationIfNecessary((ChangeInfo) this.mPendingChanges.get(size4));
        }
        this.mPendingChanges.clear();
        if (isRunning()) {
            for (int size5 = this.mMovesList.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = (ArrayList) this.mMovesList.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    MoveInfo moveInfo2 = (MoveInfo) arrayList.get(size6);
                    View view2 = moveInfo2.holder.itemView;
                    ViewCompat.m12904b(view2, 0.0f);
                    ViewCompat.m12885a(view2, 0.0f);
                    dispatchMoveFinished(moveInfo2.holder);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.mMovesList.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.mAdditionsList.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = (ArrayList) this.mAdditionsList.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    ViewHolder viewHolder2 = (ViewHolder) arrayList2.get(size8);
                    ViewCompat.m12909c(viewHolder2.itemView, 1.0f);
                    dispatchAddFinished(viewHolder2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.mAdditionsList.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.mChangesList.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = (ArrayList) this.mChangesList.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    endChangeAnimationIfNecessary((ChangeInfo) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.mChangesList.remove(arrayList3);
                    }
                }
            }
            cancelAll(this.mRemoveAnimations);
            cancelAll(this.mMoveAnimations);
            cancelAll(this.mAddAnimations);
            cancelAll(this.mChangeAnimations);
            dispatchAnimationsFinished();
        }
    }

    public boolean isRunning() {
        return !this.mPendingAdditions.isEmpty() || !this.mPendingChanges.isEmpty() || !this.mPendingMoves.isEmpty() || !this.mPendingRemovals.isEmpty() || !this.mMoveAnimations.isEmpty() || !this.mRemoveAnimations.isEmpty() || !this.mAddAnimations.isEmpty() || !this.mChangeAnimations.isEmpty() || !this.mMovesList.isEmpty() || !this.mAdditionsList.isEmpty() || !this.mChangesList.isEmpty();
    }

    public void runPendingAnimations() {
        boolean z = !this.mPendingRemovals.isEmpty();
        boolean z2 = !this.mPendingMoves.isEmpty();
        boolean z3 = !this.mPendingChanges.isEmpty();
        boolean z4 = !this.mPendingAdditions.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator it = this.mPendingRemovals.iterator();
            while (it.hasNext()) {
                animateRemoveImpl((ViewHolder) it.next());
            }
            this.mPendingRemovals.clear();
            if (z2) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.mPendingMoves);
                this.mMovesList.add(arrayList);
                this.mPendingMoves.clear();
                final ArrayList arrayList2 = arrayList;
                C05951 r0 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList2.iterator();
                        while (it.hasNext()) {
                            MoveInfo moveInfo = (MoveInfo) it.next();
                            DefaultItemAnimator.this.animateMoveImpl(moveInfo.holder, moveInfo.fromX, moveInfo.fromY, moveInfo.toX, moveInfo.toY);
                        }
                        arrayList2.clear();
                        DefaultItemAnimator.this.mMovesList.remove(arrayList2);
                    }
                };
                if (z) {
                    ViewCompat.m12898a(((MoveInfo) arrayList.get(0)).holder.itemView, (Runnable) r0, getRemoveDuration());
                } else {
                    r0.run();
                }
            }
            if (z3) {
                final ArrayList arrayList3 = new ArrayList();
                arrayList3.addAll(this.mPendingChanges);
                this.mChangesList.add(arrayList3);
                this.mPendingChanges.clear();
                C05962 r7 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList3.iterator();
                        while (it.hasNext()) {
                            DefaultItemAnimator.this.animateChangeImpl((ChangeInfo) it.next());
                        }
                        arrayList3.clear();
                        DefaultItemAnimator.this.mChangesList.remove(arrayList3);
                    }
                };
                if (z) {
                    ViewCompat.m12898a(((ChangeInfo) arrayList3.get(0)).oldHolder.itemView, (Runnable) r7, getRemoveDuration());
                } else {
                    r7.run();
                }
            }
            if (z4) {
                final ArrayList arrayList4 = new ArrayList();
                arrayList4.addAll(this.mPendingAdditions);
                this.mAdditionsList.add(arrayList4);
                this.mPendingAdditions.clear();
                C05973 r4 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList4.iterator();
                        while (it.hasNext()) {
                            DefaultItemAnimator.this.animateAddImpl((ViewHolder) it.next());
                        }
                        arrayList4.clear();
                        DefaultItemAnimator.this.mAdditionsList.remove(arrayList4);
                    }
                };
                if (z || z2 || z3) {
                    ViewCompat.m12898a(((ViewHolder) arrayList4.get(0)).itemView, (Runnable) r4, (z ? getRemoveDuration() : 0) + Math.max(z2 ? getMoveDuration() : 0, z3 ? getChangeDuration() : 0));
                } else {
                    r4.run();
                }
            }
        }
    }
}
