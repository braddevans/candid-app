package android.support.p003v7.view.menu;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

/* renamed from: android.support.v7.view.menu.MenuWrapperFactory */
public final class MenuWrapperFactory {
    private MenuWrapperFactory() {
    }

    public static Menu wrapSupportMenu(Context context, SupportMenu ckVar) {
        if (VERSION.SDK_INT >= 14) {
            return new MenuWrapperICS(context, ckVar);
        }
        throw new UnsupportedOperationException();
    }

    public static MenuItem wrapSupportMenuItem(Context context, SupportMenuItem clVar) {
        if (VERSION.SDK_INT >= 16) {
            return new MenuItemWrapperJB(context, clVar);
        }
        if (VERSION.SDK_INT >= 14) {
            return new MenuItemWrapperICS(context, clVar);
        }
        throw new UnsupportedOperationException();
    }

    public static SubMenu wrapSupportSubMenu(Context context, SupportSubMenu cmVar) {
        if (VERSION.SDK_INT >= 14) {
            return new SubMenuWrapperICS(context, cmVar);
        }
        throw new UnsupportedOperationException();
    }
}
