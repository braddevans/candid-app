package android.support.p003v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.support.p003v7.app.AlertController.AlertParams;
import android.support.p003v7.appcompat.C0553R;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

/* renamed from: android.support.v7.app.AlertDialog */
public class AlertDialog extends AppCompatDialog implements DialogInterface {
    static final int LAYOUT_HINT_NONE = 0;
    static final int LAYOUT_HINT_SIDE = 1;
    final AlertController mAlert;

    /* renamed from: android.support.v7.app.AlertDialog$Builder */
    public static class Builder {

        /* renamed from: P */
        private final AlertParams f3595P;
        private final int mTheme;

        public Builder(Context context) {
            this(context, AlertDialog.resolveDialogTheme(context, 0));
        }

        public Builder(Context context, int i) {
            this.f3595P = new AlertParams(new ContextThemeWrapper(context, AlertDialog.resolveDialogTheme(context, i)));
            this.mTheme = i;
        }

        public AlertDialog create() {
            AlertDialog alertDialog = new AlertDialog(this.f3595P.mContext, this.mTheme);
            this.f3595P.apply(alertDialog.mAlert);
            alertDialog.setCancelable(this.f3595P.mCancelable);
            if (this.f3595P.mCancelable) {
                alertDialog.setCanceledOnTouchOutside(true);
            }
            alertDialog.setOnCancelListener(this.f3595P.mOnCancelListener);
            alertDialog.setOnDismissListener(this.f3595P.mOnDismissListener);
            if (this.f3595P.mOnKeyListener != null) {
                alertDialog.setOnKeyListener(this.f3595P.mOnKeyListener);
            }
            return alertDialog;
        }

        public Context getContext() {
            return this.f3595P.mContext;
        }

        public Builder setAdapter(ListAdapter listAdapter, OnClickListener onClickListener) {
            this.f3595P.mAdapter = listAdapter;
            this.f3595P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setCancelable(boolean z) {
            this.f3595P.mCancelable = z;
            return this;
        }

        public Builder setCursor(Cursor cursor, OnClickListener onClickListener, String str) {
            this.f3595P.mCursor = cursor;
            this.f3595P.mLabelColumn = str;
            this.f3595P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setCustomTitle(View view) {
            this.f3595P.mCustomTitleView = view;
            return this;
        }

        public Builder setIcon(int i) {
            this.f3595P.mIconId = i;
            return this;
        }

        public Builder setIcon(Drawable drawable) {
            this.f3595P.mIcon = drawable;
            return this;
        }

        public Builder setIconAttribute(int i) {
            TypedValue typedValue = new TypedValue();
            this.f3595P.mContext.getTheme().resolveAttribute(i, typedValue, true);
            this.f3595P.mIconId = typedValue.resourceId;
            return this;
        }

        @Deprecated
        public Builder setInverseBackgroundForced(boolean z) {
            this.f3595P.mForceInverseBackground = z;
            return this;
        }

        public Builder setItems(int i, OnClickListener onClickListener) {
            this.f3595P.mItems = this.f3595P.mContext.getResources().getTextArray(i);
            this.f3595P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setItems(CharSequence[] charSequenceArr, OnClickListener onClickListener) {
            this.f3595P.mItems = charSequenceArr;
            this.f3595P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setMessage(int i) {
            this.f3595P.mMessage = this.f3595P.mContext.getText(i);
            return this;
        }

        public Builder setMessage(CharSequence charSequence) {
            this.f3595P.mMessage = charSequence;
            return this;
        }

        public Builder setMultiChoiceItems(int i, boolean[] zArr, OnMultiChoiceClickListener onMultiChoiceClickListener) {
            this.f3595P.mItems = this.f3595P.mContext.getResources().getTextArray(i);
            this.f3595P.mOnCheckboxClickListener = onMultiChoiceClickListener;
            this.f3595P.mCheckedItems = zArr;
            this.f3595P.mIsMultiChoice = true;
            return this;
        }

        public Builder setMultiChoiceItems(Cursor cursor, String str, String str2, OnMultiChoiceClickListener onMultiChoiceClickListener) {
            this.f3595P.mCursor = cursor;
            this.f3595P.mOnCheckboxClickListener = onMultiChoiceClickListener;
            this.f3595P.mIsCheckedColumn = str;
            this.f3595P.mLabelColumn = str2;
            this.f3595P.mIsMultiChoice = true;
            return this;
        }

        public Builder setMultiChoiceItems(CharSequence[] charSequenceArr, boolean[] zArr, OnMultiChoiceClickListener onMultiChoiceClickListener) {
            this.f3595P.mItems = charSequenceArr;
            this.f3595P.mOnCheckboxClickListener = onMultiChoiceClickListener;
            this.f3595P.mCheckedItems = zArr;
            this.f3595P.mIsMultiChoice = true;
            return this;
        }

        public Builder setNegativeButton(int i, OnClickListener onClickListener) {
            this.f3595P.mNegativeButtonText = this.f3595P.mContext.getText(i);
            this.f3595P.mNegativeButtonListener = onClickListener;
            return this;
        }

        public Builder setNegativeButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.f3595P.mNegativeButtonText = charSequence;
            this.f3595P.mNegativeButtonListener = onClickListener;
            return this;
        }

        public Builder setNeutralButton(int i, OnClickListener onClickListener) {
            this.f3595P.mNeutralButtonText = this.f3595P.mContext.getText(i);
            this.f3595P.mNeutralButtonListener = onClickListener;
            return this;
        }

        public Builder setNeutralButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.f3595P.mNeutralButtonText = charSequence;
            this.f3595P.mNeutralButtonListener = onClickListener;
            return this;
        }

        public Builder setOnCancelListener(OnCancelListener onCancelListener) {
            this.f3595P.mOnCancelListener = onCancelListener;
            return this;
        }

        public Builder setOnDismissListener(OnDismissListener onDismissListener) {
            this.f3595P.mOnDismissListener = onDismissListener;
            return this;
        }

        public Builder setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
            this.f3595P.mOnItemSelectedListener = onItemSelectedListener;
            return this;
        }

        public Builder setOnKeyListener(OnKeyListener onKeyListener) {
            this.f3595P.mOnKeyListener = onKeyListener;
            return this;
        }

        public Builder setPositiveButton(int i, OnClickListener onClickListener) {
            this.f3595P.mPositiveButtonText = this.f3595P.mContext.getText(i);
            this.f3595P.mPositiveButtonListener = onClickListener;
            return this;
        }

        public Builder setPositiveButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.f3595P.mPositiveButtonText = charSequence;
            this.f3595P.mPositiveButtonListener = onClickListener;
            return this;
        }

        public Builder setRecycleOnMeasureEnabled(boolean z) {
            this.f3595P.mRecycleOnMeasure = z;
            return this;
        }

        public Builder setSingleChoiceItems(int i, int i2, OnClickListener onClickListener) {
            this.f3595P.mItems = this.f3595P.mContext.getResources().getTextArray(i);
            this.f3595P.mOnClickListener = onClickListener;
            this.f3595P.mCheckedItem = i2;
            this.f3595P.mIsSingleChoice = true;
            return this;
        }

        public Builder setSingleChoiceItems(Cursor cursor, int i, String str, OnClickListener onClickListener) {
            this.f3595P.mCursor = cursor;
            this.f3595P.mOnClickListener = onClickListener;
            this.f3595P.mCheckedItem = i;
            this.f3595P.mLabelColumn = str;
            this.f3595P.mIsSingleChoice = true;
            return this;
        }

        public Builder setSingleChoiceItems(ListAdapter listAdapter, int i, OnClickListener onClickListener) {
            this.f3595P.mAdapter = listAdapter;
            this.f3595P.mOnClickListener = onClickListener;
            this.f3595P.mCheckedItem = i;
            this.f3595P.mIsSingleChoice = true;
            return this;
        }

        public Builder setSingleChoiceItems(CharSequence[] charSequenceArr, int i, OnClickListener onClickListener) {
            this.f3595P.mItems = charSequenceArr;
            this.f3595P.mOnClickListener = onClickListener;
            this.f3595P.mCheckedItem = i;
            this.f3595P.mIsSingleChoice = true;
            return this;
        }

        public Builder setTitle(int i) {
            this.f3595P.mTitle = this.f3595P.mContext.getText(i);
            return this;
        }

        public Builder setTitle(CharSequence charSequence) {
            this.f3595P.mTitle = charSequence;
            return this;
        }

        public Builder setView(int i) {
            this.f3595P.mView = null;
            this.f3595P.mViewLayoutResId = i;
            this.f3595P.mViewSpacingSpecified = false;
            return this;
        }

        public Builder setView(View view) {
            this.f3595P.mView = view;
            this.f3595P.mViewLayoutResId = 0;
            this.f3595P.mViewSpacingSpecified = false;
            return this;
        }

        @Deprecated
        public Builder setView(View view, int i, int i2, int i3, int i4) {
            this.f3595P.mView = view;
            this.f3595P.mViewLayoutResId = 0;
            this.f3595P.mViewSpacingSpecified = true;
            this.f3595P.mViewSpacingLeft = i;
            this.f3595P.mViewSpacingTop = i2;
            this.f3595P.mViewSpacingRight = i3;
            this.f3595P.mViewSpacingBottom = i4;
            return this;
        }

        public AlertDialog show() {
            AlertDialog create = create();
            create.show();
            return create;
        }
    }

    protected AlertDialog(Context context) {
        this(context, 0);
    }

    protected AlertDialog(Context context, int i) {
        super(context, resolveDialogTheme(context, i));
        this.mAlert = new AlertController(getContext(), this, getWindow());
    }

    protected AlertDialog(Context context, boolean z, OnCancelListener onCancelListener) {
        this(context, 0);
        setCancelable(z);
        setOnCancelListener(onCancelListener);
    }

    static int resolveDialogTheme(Context context, int i) {
        if (i >= 16777216) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(C0553R.attr.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public Button getButton(int i) {
        return this.mAlert.getButton(i);
    }

    public ListView getListView() {
        return this.mAlert.getListView();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mAlert.installContent();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.mAlert.onKeyDown(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.mAlert.onKeyUp(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void setButton(int i, CharSequence charSequence, OnClickListener onClickListener) {
        this.mAlert.setButton(i, charSequence, onClickListener, null);
    }

    public void setButton(int i, CharSequence charSequence, Message message) {
        this.mAlert.setButton(i, charSequence, null, message);
    }

    /* access modifiers changed from: 0000 */
    public void setButtonPanelLayoutHint(int i) {
        this.mAlert.setButtonPanelLayoutHint(i);
    }

    public void setCustomTitle(View view) {
        this.mAlert.setCustomTitle(view);
    }

    public void setIcon(int i) {
        this.mAlert.setIcon(i);
    }

    public void setIcon(Drawable drawable) {
        this.mAlert.setIcon(drawable);
    }

    public void setIconAttribute(int i) {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(i, typedValue, true);
        this.mAlert.setIcon(typedValue.resourceId);
    }

    public void setMessage(CharSequence charSequence) {
        this.mAlert.setMessage(charSequence);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.mAlert.setTitle(charSequence);
    }

    public void setView(View view) {
        this.mAlert.setView(view);
    }

    public void setView(View view, int i, int i2, int i3, int i4) {
        this.mAlert.setView(view, i, i2, i3, i4);
    }
}
