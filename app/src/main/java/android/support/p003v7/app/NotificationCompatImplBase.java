package android.support.p003v7.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.support.p001v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.p001v4.app.NotificationCompat.Action;
import android.support.p001v4.app.NotificationCompatBase;
import android.support.p003v7.appcompat.C0553R;
import android.widget.RemoteViews;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v7.app.NotificationCompatImplBase */
class NotificationCompatImplBase {
    private static final int MAX_ACTION_BUTTONS = 3;
    static final int MAX_MEDIA_BUTTONS = 5;
    static final int MAX_MEDIA_BUTTONS_IN_COMPACT = 3;

    NotificationCompatImplBase() {
    }

    public static RemoteViews applyStandardTemplate(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, int i2, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i3, int i4, int i5, boolean z2) {
        Resources resources = context.getResources();
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), i5);
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = i3 < -1;
        boolean z6 = VERSION.SDK_INT >= 16;
        boolean z7 = VERSION.SDK_INT >= 21;
        if (z6 && !z7) {
            if (z5) {
                remoteViews.setInt(C0553R.C0555id.notification_background, "setBackgroundResource", C0553R.C0554drawable.notification_bg_low);
                remoteViews.setInt(C0553R.C0555id.icon, "setBackgroundResource", C0553R.C0554drawable.notification_template_icon_low_bg);
            } else {
                remoteViews.setInt(C0553R.C0555id.notification_background, "setBackgroundResource", C0553R.C0554drawable.notification_bg);
                remoteViews.setInt(C0553R.C0555id.icon, "setBackgroundResource", C0553R.C0554drawable.notification_template_icon_bg);
            }
        }
        if (bitmap != null) {
            if (z6) {
                remoteViews.setViewVisibility(C0553R.C0555id.icon, 0);
                remoteViews.setImageViewBitmap(C0553R.C0555id.icon, bitmap);
            } else {
                remoteViews.setViewVisibility(C0553R.C0555id.icon, 8);
            }
            if (i2 != 0) {
                int dimensionPixelSize = resources.getDimensionPixelSize(C0553R.dimen.notification_right_icon_size);
                int dimensionPixelSize2 = dimensionPixelSize - (resources.getDimensionPixelSize(C0553R.dimen.notification_small_icon_background_padding) * 2);
                if (z7) {
                    remoteViews.setImageViewBitmap(C0553R.C0555id.right_icon, createIconWithBackground(context, i2, dimensionPixelSize, dimensionPixelSize2, i4));
                } else {
                    remoteViews.setImageViewBitmap(C0553R.C0555id.right_icon, createColoredBitmap(context, i2, -1));
                }
                remoteViews.setViewVisibility(C0553R.C0555id.right_icon, 0);
            }
        } else if (i2 != 0) {
            remoteViews.setViewVisibility(C0553R.C0555id.icon, 0);
            if (z7) {
                remoteViews.setImageViewBitmap(C0553R.C0555id.icon, createIconWithBackground(context, i2, resources.getDimensionPixelSize(C0553R.dimen.notification_large_icon_width) - resources.getDimensionPixelSize(C0553R.dimen.notification_big_circle_margin), resources.getDimensionPixelSize(C0553R.dimen.notification_small_icon_size_as_large), i4));
            } else {
                remoteViews.setImageViewBitmap(C0553R.C0555id.icon, createColoredBitmap(context, i2, -1));
            }
        }
        if (charSequence != null) {
            remoteViews.setTextViewText(C0553R.C0555id.title, charSequence);
        }
        if (charSequence2 != null) {
            remoteViews.setTextViewText(C0553R.C0555id.text, charSequence2);
            z3 = true;
        }
        boolean z8 = !z7 && bitmap != null;
        if (charSequence3 != null) {
            remoteViews.setTextViewText(C0553R.C0555id.info, charSequence3);
            remoteViews.setViewVisibility(C0553R.C0555id.info, 0);
            z3 = true;
            z8 = true;
        } else if (i > 0) {
            if (i > resources.getInteger(C0553R.integer.status_bar_notification_info_maxnum)) {
                remoteViews.setTextViewText(C0553R.C0555id.info, resources.getString(C0553R.string.status_bar_notification_info_overflow));
            } else {
                remoteViews.setTextViewText(C0553R.C0555id.info, NumberFormat.getIntegerInstance().format((long) i));
            }
            remoteViews.setViewVisibility(C0553R.C0555id.info, 0);
            z3 = true;
            z8 = true;
        } else {
            remoteViews.setViewVisibility(C0553R.C0555id.info, 8);
        }
        if (charSequence4 != null && z6) {
            remoteViews.setTextViewText(C0553R.C0555id.text, charSequence4);
            if (charSequence2 != null) {
                remoteViews.setTextViewText(C0553R.C0555id.text2, charSequence2);
                remoteViews.setViewVisibility(C0553R.C0555id.text2, 0);
                z4 = true;
            } else {
                remoteViews.setViewVisibility(C0553R.C0555id.text2, 8);
            }
        }
        if (z4 && z6) {
            if (z2) {
                remoteViews.setTextViewTextSize(C0553R.C0555id.text, 0, (float) resources.getDimensionPixelSize(C0553R.dimen.notification_subtext_size));
            }
            remoteViews.setViewPadding(C0553R.C0555id.line1, 0, 0, 0, 0);
        }
        if (j != 0) {
            if (!z || !z6) {
                remoteViews.setViewVisibility(C0553R.C0555id.time, 0);
                remoteViews.setLong(C0553R.C0555id.time, "setTime", j);
            } else {
                remoteViews.setViewVisibility(C0553R.C0555id.chronometer, 0);
                remoteViews.setLong(C0553R.C0555id.chronometer, "setBase", (SystemClock.elapsedRealtime() - System.currentTimeMillis()) + j);
                remoteViews.setBoolean(C0553R.C0555id.chronometer, "setStarted", true);
            }
            z8 = true;
        }
        remoteViews.setViewVisibility(C0553R.C0555id.right_side, z8 ? 0 : 8);
        remoteViews.setViewVisibility(C0553R.C0555id.line3, z3 ? 0 : 8);
        return remoteViews;
    }

    public static RemoteViews applyStandardTemplateWithActions(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, int i2, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i3, int i4, int i5, boolean z2, ArrayList<Action> arrayList) {
        RemoteViews applyStandardTemplate = applyStandardTemplate(context, charSequence, charSequence2, charSequence3, i, i2, bitmap, charSequence4, z, j, i3, i4, i5, z2);
        applyStandardTemplate.removeAllViews(C0553R.C0555id.actions);
        boolean z3 = false;
        if (arrayList != null) {
            int size = arrayList.size();
            if (size > 0) {
                z3 = true;
                if (size > 3) {
                    size = 3;
                }
                for (int i6 = 0; i6 < size; i6++) {
                    applyStandardTemplate.addView(C0553R.C0555id.actions, generateActionButton(context, (Action) arrayList.get(i6)));
                }
            }
        }
        int i7 = z3 ? 0 : 8;
        applyStandardTemplate.setViewVisibility(C0553R.C0555id.actions, i7);
        applyStandardTemplate.setViewVisibility(C0553R.C0555id.action_divider, i7);
        return applyStandardTemplate;
    }

    public static void buildIntoRemoteViews(Context context, RemoteViews remoteViews, RemoteViews remoteViews2) {
        hideNormalContent(remoteViews);
        remoteViews.removeAllViews(C0553R.C0555id.notification_main_column);
        remoteViews.addView(C0553R.C0555id.notification_main_column, remoteViews2.clone());
        remoteViews.setViewVisibility(C0553R.C0555id.notification_main_column, 0);
        if (VERSION.SDK_INT >= 21) {
            remoteViews.setViewPadding(C0553R.C0555id.notification_main_column_container, 0, calculateTopPadding(context), 0, 0);
        }
    }

    public static int calculateTopPadding(Context context) {
        float constrain = (constrain(context.getResources().getConfiguration().fontScale, 1.0f, 1.3f) - 1.0f) / 0.29999995f;
        return Math.round(((1.0f - constrain) * ((float) context.getResources().getDimensionPixelSize(C0553R.dimen.notification_top_pad))) + (((float) context.getResources().getDimensionPixelSize(C0553R.dimen.notification_top_pad_large_text)) * constrain));
    }

    public static float constrain(float f, float f2, float f3) {
        return f < f2 ? f2 : f > f3 ? f3 : f;
    }

    private static Bitmap createColoredBitmap(Context context, int i, int i2) {
        return createColoredBitmap(context, i, i2, 0);
    }

    private static Bitmap createColoredBitmap(Context context, int i, int i2, int i3) {
        Drawable drawable = context.getResources().getDrawable(i);
        int i4 = i3 == 0 ? drawable.getIntrinsicWidth() : i3;
        int i5 = i3 == 0 ? drawable.getIntrinsicHeight() : i3;
        Bitmap createBitmap = Bitmap.createBitmap(i4, i5, Config.ARGB_8888);
        drawable.setBounds(0, 0, i4, i5);
        if (i2 != 0) {
            drawable.mutate().setColorFilter(new PorterDuffColorFilter(i2, Mode.SRC_IN));
        }
        drawable.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    public static Bitmap createIconWithBackground(Context context, int i, int i2, int i3, int i4) {
        int i5 = C0553R.C0554drawable.notification_icon_background;
        if (i4 == 0) {
            i4 = 0;
        }
        Bitmap createColoredBitmap = createColoredBitmap(context, i5, i4, i2);
        Canvas canvas = new Canvas(createColoredBitmap);
        Drawable mutate = context.getResources().getDrawable(i).mutate();
        mutate.setFilterBitmap(true);
        int i6 = (i2 - i3) / 2;
        mutate.setBounds(i6, i6, i3 + i6, i3 + i6);
        mutate.setColorFilter(new PorterDuffColorFilter(-1, Mode.SRC_ATOP));
        mutate.draw(canvas);
        return createColoredBitmap;
    }

    private static RemoteViews generateActionButton(Context context, Action action) {
        boolean z = action.actionIntent == null;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), z ? getActionTombstoneLayoutResource() : getActionLayoutResource());
        remoteViews.setImageViewBitmap(C0553R.C0555id.action_image, createColoredBitmap(context, action.getIcon(), context.getResources().getColor(C0553R.color.notification_action_color_filter)));
        remoteViews.setTextViewText(C0553R.C0555id.action_text, action.title);
        if (!z) {
            remoteViews.setOnClickPendingIntent(C0553R.C0555id.action_container, action.actionIntent);
        }
        remoteViews.setContentDescription(C0553R.C0555id.action_container, action.title);
        return remoteViews;
    }

    private static <T extends NotificationCompatBase.Action> RemoteViews generateContentViewMedia(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, List<T> list, int[] iArr, boolean z2, PendingIntent pendingIntent, boolean z3) {
        RemoteViews applyStandardTemplate = applyStandardTemplate(context, charSequence, charSequence2, charSequence3, i, 0, bitmap, charSequence4, z, j, i2, 0, z3 ? C0553R.layout.notification_template_media_custom : C0553R.layout.notification_template_media, true);
        int size = list.size();
        int min = iArr == null ? 0 : Math.min(iArr.length, 3);
        applyStandardTemplate.removeAllViews(C0553R.C0555id.media_actions);
        if (min > 0) {
            for (int i3 = 0; i3 < min; i3++) {
                if (i3 >= size) {
                    throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", new Object[]{Integer.valueOf(i3), Integer.valueOf(size - 1)}));
                }
                applyStandardTemplate.addView(C0553R.C0555id.media_actions, generateMediaActionButton(context, (NotificationCompatBase.Action) list.get(iArr[i3])));
            }
        }
        if (z2) {
            applyStandardTemplate.setViewVisibility(C0553R.C0555id.end_padder, 8);
            applyStandardTemplate.setViewVisibility(C0553R.C0555id.cancel_action, 0);
            applyStandardTemplate.setOnClickPendingIntent(C0553R.C0555id.cancel_action, pendingIntent);
            applyStandardTemplate.setInt(C0553R.C0555id.cancel_action, "setAlpha", context.getResources().getInteger(C0553R.integer.cancel_button_image_alpha));
        } else {
            applyStandardTemplate.setViewVisibility(C0553R.C0555id.end_padder, 0);
            applyStandardTemplate.setViewVisibility(C0553R.C0555id.cancel_action, 8);
        }
        return applyStandardTemplate;
    }

    private static RemoteViews generateMediaActionButton(Context context, NotificationCompatBase.Action action) {
        boolean z = action.getActionIntent() == null;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0553R.layout.notification_media_action);
        remoteViews.setImageViewResource(C0553R.C0555id.action0, action.getIcon());
        if (!z) {
            remoteViews.setOnClickPendingIntent(C0553R.C0555id.action0, action.getActionIntent());
        }
        if (VERSION.SDK_INT >= 15) {
            remoteViews.setContentDescription(C0553R.C0555id.action0, action.getTitle());
        }
        return remoteViews;
    }

    public static <T extends NotificationCompatBase.Action> RemoteViews generateMediaBigView(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, int i3, List<T> list, boolean z2, PendingIntent pendingIntent, boolean z3) {
        int min = Math.min(list.size(), 5);
        RemoteViews applyStandardTemplate = applyStandardTemplate(context, charSequence, charSequence2, charSequence3, i, 0, bitmap, charSequence4, z, j, i2, i3, getBigMediaLayoutResource(z3, min), false);
        applyStandardTemplate.removeAllViews(C0553R.C0555id.media_actions);
        if (min > 0) {
            for (int i4 = 0; i4 < min; i4++) {
                RemoteViews generateMediaActionButton = generateMediaActionButton(context, (NotificationCompatBase.Action) list.get(i4));
                applyStandardTemplate.addView(C0553R.C0555id.media_actions, generateMediaActionButton);
            }
        }
        if (z2) {
            applyStandardTemplate.setViewVisibility(C0553R.C0555id.cancel_action, 0);
            applyStandardTemplate.setInt(C0553R.C0555id.cancel_action, "setAlpha", context.getResources().getInteger(C0553R.integer.cancel_button_image_alpha));
            applyStandardTemplate.setOnClickPendingIntent(C0553R.C0555id.cancel_action, pendingIntent);
        } else {
            applyStandardTemplate.setViewVisibility(C0553R.C0555id.cancel_action, 8);
        }
        return applyStandardTemplate;
    }

    private static int getActionLayoutResource() {
        return C0553R.layout.notification_action;
    }

    private static int getActionTombstoneLayoutResource() {
        return C0553R.layout.notification_action_tombstone;
    }

    private static int getBigMediaLayoutResource(boolean z, int i) {
        return i <= 3 ? z ? C0553R.layout.notification_template_big_media_narrow_custom : C0553R.layout.notification_template_big_media_narrow : z ? C0553R.layout.notification_template_big_media_custom : C0553R.layout.notification_template_big_media;
    }

    private static void hideNormalContent(RemoteViews remoteViews) {
        remoteViews.setViewVisibility(C0553R.C0555id.title, 8);
        remoteViews.setViewVisibility(C0553R.C0555id.text2, 8);
        remoteViews.setViewVisibility(C0553R.C0555id.text, 8);
    }

    public static <T extends NotificationCompatBase.Action> RemoteViews overrideContentViewMedia(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, List<T> list, int[] iArr, boolean z2, PendingIntent pendingIntent, boolean z3) {
        RemoteViews generateContentViewMedia = generateContentViewMedia(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, i2, list, iArr, z2, pendingIntent, z3);
        notificationBuilderWithBuilderAccessor.getBuilder().setContent(generateContentViewMedia);
        if (z2) {
            notificationBuilderWithBuilderAccessor.getBuilder().setOngoing(true);
        }
        return generateContentViewMedia;
    }

    public static <T extends NotificationCompatBase.Action> void overrideMediaBigContentView(Notification notification, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, int i3, List<T> list, boolean z2, PendingIntent pendingIntent, boolean z3) {
        notification.bigContentView = generateMediaBigView(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, i2, i3, list, z2, pendingIntent, z3);
        if (z2) {
            notification.flags |= 2;
        }
    }
}
