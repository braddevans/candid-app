package android.support.design.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class ScrimInsetsFrameLayout extends FrameLayout {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public Drawable f2335a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Rect f2336b;

    /* renamed from: c */
    private Rect f2337c;

    public ScrimInsetsFrameLayout(Context context) {
        this(context, null);
    }

    public ScrimInsetsFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ScrimInsetsFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2337c = new Rect();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.ScrimInsetsFrameLayout, i, C2393h.Widget_Design_ScrimInsetsFrameLayout);
        this.f2335a = obtainStyledAttributes.getDrawable(C2394i.ScrimInsetsFrameLayout_insetForeground);
        obtainStyledAttributes.recycle();
        setWillNotDraw(true);
        ViewCompat.m12895a((View) this, (OnApplyWindowInsetsListener) new OnApplyWindowInsetsListener() {
            public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat fzVar) {
                if (ScrimInsetsFrameLayout.this.f2336b == null) {
                    ScrimInsetsFrameLayout.this.f2336b = new Rect();
                }
                ScrimInsetsFrameLayout.this.f2336b.set(fzVar.mo13565a(), fzVar.mo13567b(), fzVar.mo13568c(), fzVar.mo13569d());
                ScrimInsetsFrameLayout.this.mo1428a(ScrimInsetsFrameLayout.this.f2336b);
                ScrimInsetsFrameLayout.this.setWillNotDraw(ScrimInsetsFrameLayout.this.f2336b.isEmpty() || ScrimInsetsFrameLayout.this.f2335a == null);
                ViewCompat.m12913d(ScrimInsetsFrameLayout.this);
                return fzVar.mo13572f();
            }
        });
    }

    /* renamed from: a */
    public void mo1428a(Rect rect) {
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        int width = getWidth();
        int height = getHeight();
        if (this.f2336b != null && this.f2335a != null) {
            int save = canvas.save();
            canvas.translate((float) getScrollX(), (float) getScrollY());
            this.f2337c.set(0, 0, width, this.f2336b.top);
            this.f2335a.setBounds(this.f2337c);
            this.f2335a.draw(canvas);
            this.f2337c.set(0, height - this.f2336b.bottom, width, height);
            this.f2335a.setBounds(this.f2337c);
            this.f2335a.draw(canvas);
            this.f2337c.set(0, this.f2336b.top, this.f2336b.left, height - this.f2336b.bottom);
            this.f2335a.setBounds(this.f2337c);
            this.f2335a.draw(canvas);
            this.f2337c.set(width - this.f2336b.right, this.f2336b.top, width, height - this.f2336b.bottom);
            this.f2335a.setBounds(this.f2337c);
            this.f2335a.draw(canvas);
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f2335a != null) {
            this.f2335a.setCallback(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f2335a != null) {
            this.f2335a.setCallback(null);
        }
    }
}
