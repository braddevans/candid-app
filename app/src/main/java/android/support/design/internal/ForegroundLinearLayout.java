package android.support.design.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.p003v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.Gravity;

public class ForegroundLinearLayout extends LinearLayoutCompat {

    /* renamed from: a */
    protected boolean f2323a;

    /* renamed from: b */
    boolean f2324b;

    /* renamed from: c */
    private Drawable f2325c;

    /* renamed from: d */
    private final Rect f2326d;

    /* renamed from: e */
    private final Rect f2327e;

    /* renamed from: f */
    private int f2328f;

    public ForegroundLinearLayout(Context context) {
        this(context, null);
    }

    public ForegroundLinearLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ForegroundLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2326d = new Rect();
        this.f2327e = new Rect();
        this.f2328f = 119;
        this.f2323a = true;
        this.f2324b = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.ForegroundLinearLayout, i, 0);
        this.f2328f = obtainStyledAttributes.getInt(C2394i.ForegroundLinearLayout_android_foregroundGravity, this.f2328f);
        Drawable drawable = obtainStyledAttributes.getDrawable(C2394i.ForegroundLinearLayout_android_foreground);
        if (drawable != null) {
            setForeground(drawable);
        }
        this.f2323a = obtainStyledAttributes.getBoolean(C2394i.ForegroundLinearLayout_foregroundInsidePadding, true);
        obtainStyledAttributes.recycle();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f2325c != null) {
            Drawable drawable = this.f2325c;
            if (this.f2324b) {
                this.f2324b = false;
                Rect rect = this.f2326d;
                Rect rect2 = this.f2327e;
                int right = getRight() - getLeft();
                int bottom = getBottom() - getTop();
                if (this.f2323a) {
                    rect.set(0, 0, right, bottom);
                } else {
                    rect.set(getPaddingLeft(), getPaddingTop(), right - getPaddingRight(), bottom - getPaddingBottom());
                }
                Gravity.apply(this.f2328f, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), rect, rect2);
                drawable.setBounds(rect2);
            }
            drawable.draw(canvas);
        }
    }

    public void drawableHotspotChanged(float f, float f2) {
        super.drawableHotspotChanged(f, f2);
        if (this.f2325c != null) {
            this.f2325c.setHotspot(f, f2);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f2325c != null && this.f2325c.isStateful()) {
            this.f2325c.setState(getDrawableState());
        }
    }

    public Drawable getForeground() {
        return this.f2325c;
    }

    public int getForegroundGravity() {
        return this.f2328f;
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        if (this.f2325c != null) {
            this.f2325c.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f2324b |= z;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f2324b = true;
    }

    public void setForeground(Drawable drawable) {
        if (this.f2325c != drawable) {
            if (this.f2325c != null) {
                this.f2325c.setCallback(null);
                unscheduleDrawable(this.f2325c);
            }
            this.f2325c = drawable;
            if (drawable != null) {
                setWillNotDraw(false);
                drawable.setCallback(this);
                if (drawable.isStateful()) {
                    drawable.setState(getDrawableState());
                }
                if (this.f2328f == 119) {
                    drawable.getPadding(new Rect());
                }
            } else {
                setWillNotDraw(true);
            }
            requestLayout();
            invalidate();
        }
    }

    public void setForegroundGravity(int i) {
        if (this.f2328f != i) {
            if ((8388615 & i) == 0) {
                i |= 8388611;
            }
            if ((i & 112) == 0) {
                i |= 48;
            }
            this.f2328f = i;
            if (this.f2328f == 119 && this.f2325c != null) {
                this.f2325c.getPadding(new Rect());
            }
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f2325c;
    }
}
