package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.StateListDrawable;
import android.support.p001v4.widget.TextViewCompat;
import android.support.p003v7.view.menu.MenuItemImpl;
import android.support.p003v7.view.menu.MenuView.ItemView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;

public class NavigationMenuItemView extends ForegroundLinearLayout implements ItemView {

    /* renamed from: c */
    private static final int[] f2329c = {16842912};

    /* renamed from: d */
    private final int f2330d;

    /* renamed from: e */
    private final CheckedTextView f2331e;

    /* renamed from: f */
    private FrameLayout f2332f;

    /* renamed from: g */
    private MenuItemImpl f2333g;

    /* renamed from: h */
    private ColorStateList f2334h;

    public NavigationMenuItemView(Context context) {
        this(context, null);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setOrientation(0);
        LayoutInflater.from(context).inflate(C2391f.design_navigation_menu_item, this, true);
        this.f2330d = context.getResources().getDimensionPixelSize(C2389d.design_navigation_icon_size);
        this.f2331e = (CheckedTextView) findViewById(C2390e.design_menu_item_text);
        this.f2331e.setDuplicateParentStateEnabled(true);
    }

    /* renamed from: b */
    private StateListDrawable m3066b() {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(C2387b.colorControlHighlight, typedValue, true)) {
            return null;
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(f2329c, new ColorDrawable(typedValue.data));
        stateListDrawable.addState(EMPTY_STATE_SET, new ColorDrawable(0));
        return stateListDrawable;
    }

    private void setActionView(View view) {
        if (this.f2332f == null) {
            this.f2332f = (FrameLayout) ((ViewStub) findViewById(C2390e.design_menu_item_action_area_stub)).inflate();
        }
        this.f2332f.removeAllViews();
        if (view != null) {
            this.f2332f.addView(view);
        }
    }

    /* renamed from: a */
    public void mo1406a() {
        if (this.f2332f != null) {
            this.f2332f.removeAllViews();
        }
        this.f2331e.setCompoundDrawables(null, null, null, null);
    }

    public MenuItemImpl getItemData() {
        return this.f2333g;
    }

    public void initialize(MenuItemImpl menuItemImpl, int i) {
        this.f2333g = menuItemImpl;
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        if (getBackground() == null) {
            setBackgroundDrawable(m3066b());
        }
        setCheckable(menuItemImpl.isCheckable());
        setChecked(menuItemImpl.isChecked());
        setEnabled(menuItemImpl.isEnabled());
        setTitle(menuItemImpl.getTitle());
        setIcon(menuItemImpl.getIcon());
        setActionView(menuItemImpl.getActionView());
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (this.f2333g != null && this.f2333g.isCheckable() && this.f2333g.isChecked()) {
            mergeDrawableStates(onCreateDrawableState, f2329c);
        }
        return onCreateDrawableState;
    }

    public boolean prefersCondensedTitle() {
        return false;
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
    }

    public void setChecked(boolean z) {
        refreshDrawableState();
        this.f2331e.setChecked(z);
    }

    public void setIcon(Drawable drawable) {
        if (drawable != null) {
            ConstantState constantState = drawable.getConstantState();
            if (constantState != null) {
                drawable = constantState.newDrawable();
            }
            drawable = DrawableCompat.m8896f(drawable).mutate();
            drawable.setBounds(0, 0, this.f2330d, this.f2330d);
            DrawableCompat.m8886a(drawable, this.f2334h);
        }
        TextViewCompat.setCompoundDrawablesRelative(this.f2331e, drawable, null, null, null);
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.f2334h = colorStateList;
        if (this.f2333g != null) {
            setIcon(this.f2333g.getIcon());
        }
    }

    public void setShortcut(boolean z, char c) {
    }

    public void setTextAppearance(Context context, int i) {
        this.f2331e.setTextAppearance(context, i);
    }

    public void setTextColor(ColorStateList colorStateList) {
        this.f2331e.setTextColor(colorStateList);
    }

    public void setTitle(CharSequence charSequence) {
        this.f2331e.setText(charSequence);
    }

    public boolean showsIcon() {
        return true;
    }
}
