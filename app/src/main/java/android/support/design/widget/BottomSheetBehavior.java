package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.support.p001v4.widget.ViewDragHelper;
import android.support.p001v4.widget.ViewDragHelper.Callback;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

public class BottomSheetBehavior<V extends View> extends Behavior<V> {

    /* renamed from: a */
    private float f2365a;

    /* renamed from: b */
    private int f2366b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f2367c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public int f2368d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f2369e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public int f2370f = 4;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public ViewDragHelper f2371g;

    /* renamed from: h */
    private boolean f2372h;

    /* renamed from: i */
    private int f2373i;

    /* renamed from: j */
    private boolean f2374j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public int f2375k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public WeakReference<V> f2376l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public WeakReference<View> f2377m;

    /* renamed from: n */
    private C0372a f2378n;

    /* renamed from: o */
    private VelocityTracker f2379o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public int f2380p;

    /* renamed from: q */
    private int f2381q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public boolean f2382r;

    /* renamed from: s */
    private final Callback f2383s = new Callback() {
        public int clampViewPositionHorizontal(View view, int i, int i2) {
            return view.getLeft();
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return MathUtils.m16569a(i, BottomSheetBehavior.this.f2367c, BottomSheetBehavior.this.f2369e ? BottomSheetBehavior.this.f2375k : BottomSheetBehavior.this.f2368d);
        }

        public int getViewVerticalDragRange(View view) {
            return BottomSheetBehavior.this.f2369e ? BottomSheetBehavior.this.f2375k - BottomSheetBehavior.this.f2367c : BottomSheetBehavior.this.f2368d - BottomSheetBehavior.this.f2367c;
        }

        public void onViewDragStateChanged(int i) {
            if (i == 1) {
                BottomSheetBehavior.this.m3157b(1);
            }
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            BottomSheetBehavior.this.m3161c(i2);
        }

        public void onViewReleased(View view, float f, float f2) {
            int i;
            int i2;
            if (f2 < 0.0f) {
                i = BottomSheetBehavior.this.f2367c;
                i2 = 3;
            } else if (BottomSheetBehavior.this.f2369e && BottomSheetBehavior.this.m3155a(view, f2)) {
                i = BottomSheetBehavior.this.f2375k;
                i2 = 5;
            } else if (f2 == 0.0f) {
                int top = view.getTop();
                if (Math.abs(top - BottomSheetBehavior.this.f2367c) < Math.abs(top - BottomSheetBehavior.this.f2368d)) {
                    i = BottomSheetBehavior.this.f2367c;
                    i2 = 3;
                } else {
                    i = BottomSheetBehavior.this.f2368d;
                    i2 = 4;
                }
            } else {
                i = BottomSheetBehavior.this.f2368d;
                i2 = 4;
            }
            if (BottomSheetBehavior.this.f2371g.settleCapturedViewAt(view.getLeft(), i)) {
                BottomSheetBehavior.this.m3157b(2);
                ViewCompat.m12897a(view, (Runnable) new C0373b(view, i2));
                return;
            }
            BottomSheetBehavior.this.m3157b(i2);
        }

        public boolean tryCaptureView(View view, int i) {
            boolean z = true;
            if (BottomSheetBehavior.this.f2370f == 1 || BottomSheetBehavior.this.f2382r) {
                return false;
            }
            if (BottomSheetBehavior.this.f2370f == 3 && BottomSheetBehavior.this.f2380p == i) {
                View view2 = (View) BottomSheetBehavior.this.f2377m.get();
                if (view2 != null && ViewCompat.m12908b(view2, -1)) {
                    return false;
                }
            }
            if (BottomSheetBehavior.this.f2376l == null || BottomSheetBehavior.this.f2376l.get() != view) {
                z = false;
            }
            return z;
        }
    };

    public static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a */
        final int f2385a;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f2385a = parcel.readInt();
        }

        public SavedState(Parcelable parcelable, int i) {
            super(parcelable);
            this.f2385a = i;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f2385a);
        }
    }

    /* renamed from: android.support.design.widget.BottomSheetBehavior$a */
    public static abstract class C0372a {
        /* renamed from: a */
        public abstract void mo1511a(View view, float f);

        /* renamed from: a */
        public abstract void mo1512a(View view, int i);
    }

    /* renamed from: android.support.design.widget.BottomSheetBehavior$b */
    class C0373b implements Runnable {

        /* renamed from: b */
        private final View f2387b;

        /* renamed from: c */
        private final int f2388c;

        C0373b(View view, int i) {
            this.f2387b = view;
            this.f2388c = i;
        }

        public void run() {
            if (BottomSheetBehavior.this.f2371g == null || !BottomSheetBehavior.this.f2371g.continueSettling(true)) {
                BottomSheetBehavior.this.m3157b(this.f2388c);
            } else {
                ViewCompat.m12897a(this.f2387b, (Runnable) this);
            }
        }
    }

    public BottomSheetBehavior() {
    }

    public BottomSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.BottomSheetBehavior_Params);
        mo1494a(obtainStyledAttributes.getDimensionPixelSize(C2394i.BottomSheetBehavior_Params_behavior_peekHeight, 0));
        mo1495a(obtainStyledAttributes.getBoolean(C2394i.BottomSheetBehavior_Params_behavior_hideable, false));
        obtainStyledAttributes.recycle();
        this.f2365a = (float) ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
    }

    /* renamed from: a */
    private View m3151a(View view) {
        if (view instanceof NestedScrollingChild) {
            return view;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View a = m3151a(viewGroup.getChildAt(i));
                if (a != null) {
                    return a;
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    private void m3152a() {
        this.f2380p = -1;
        if (this.f2379o != null) {
            this.f2379o.recycle();
            this.f2379o = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m3155a(View view, float f) {
        return view.getTop() >= this.f2368d && Math.abs((((float) view.getTop()) + (0.1f * f)) - ((float) this.f2368d)) / ((float) this.f2366b) > 0.5f;
    }

    /* renamed from: b */
    private float m3156b() {
        this.f2379o.computeCurrentVelocity(1000, this.f2365a);
        return VelocityTrackerCompat.m12858b(this.f2379o, this.f2380p);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m3157b(int i) {
        if (this.f2370f != i) {
            this.f2370f = i;
            View view = (View) this.f2376l.get();
            if (view != null && this.f2378n != null) {
                this.f2378n.mo1512a(view, i);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m3161c(int i) {
        View view = (View) this.f2376l.get();
        if (view != null && this.f2378n != null) {
            if (i > this.f2368d) {
                this.f2378n.mo1511a(view, ((float) (this.f2368d - i)) / ((float) this.f2366b));
            } else {
                this.f2378n.mo1511a(view, ((float) (this.f2368d - i)) / ((float) (this.f2368d - this.f2367c)));
            }
        }
    }

    /* renamed from: a */
    public final void mo1494a(int i) {
        this.f2366b = Math.max(0, i);
        this.f2368d = this.f2375k - i;
    }

    /* renamed from: a */
    public void mo1461a(CoordinatorLayout coordinatorLayout, V v, Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.mo1461a(coordinatorLayout, v, savedState.getSuperState());
        if (savedState.f2385a == 1 || savedState.f2385a == 2) {
            this.f2370f = 4;
        } else {
            this.f2370f = savedState.f2385a;
        }
    }

    /* renamed from: a */
    public void mo1462a(CoordinatorLayout coordinatorLayout, V v, View view) {
        int i;
        int i2;
        if (v.getTop() == this.f2367c) {
            m3157b(3);
        } else if (view == this.f2377m.get() && this.f2374j) {
            if (this.f2373i > 0) {
                i = this.f2367c;
                i2 = 3;
            } else if (this.f2369e && m3155a((View) v, m3156b())) {
                i = this.f2375k;
                i2 = 5;
            } else if (this.f2373i == 0) {
                int top = v.getTop();
                if (Math.abs(top - this.f2367c) < Math.abs(top - this.f2368d)) {
                    i = this.f2367c;
                    i2 = 3;
                } else {
                    i = this.f2368d;
                    i2 = 4;
                }
            } else {
                i = this.f2368d;
                i2 = 4;
            }
            if (this.f2371g.smoothSlideViewTo(v, v.getLeft(), i)) {
                m3157b(2);
                ViewCompat.m12897a((View) v, (Runnable) new C0373b(v, i2));
            } else {
                m3157b(i2);
            }
            this.f2374j = false;
        }
    }

    /* renamed from: a */
    public void mo1464a(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr) {
        if (view == ((View) this.f2377m.get())) {
            int top = v.getTop();
            int i3 = top - i2;
            if (i2 > 0) {
                if (i3 < this.f2367c) {
                    iArr[1] = top - this.f2367c;
                    ViewCompat.m12918e((View) v, -iArr[1]);
                    m3157b(3);
                } else {
                    iArr[1] = i2;
                    ViewCompat.m12918e((View) v, -i2);
                    m3157b(1);
                }
            } else if (i2 < 0 && !ViewCompat.m12908b(view, -1)) {
                if (i3 <= this.f2368d || this.f2369e) {
                    iArr[1] = i2;
                    ViewCompat.m12918e((View) v, -i2);
                    m3157b(1);
                } else {
                    iArr[1] = top - this.f2368d;
                    ViewCompat.m12918e((View) v, -iArr[1]);
                    m3157b(4);
                }
            }
            m3161c(v.getTop());
            this.f2373i = i2;
            this.f2374j = true;
        }
    }

    /* renamed from: a */
    public void mo1495a(boolean z) {
        this.f2369e = z;
    }

    /* renamed from: a */
    public boolean mo1471a(CoordinatorLayout coordinatorLayout, V v, int i) {
        if (!(this.f2370f == 1 || this.f2370f == 2)) {
            if (ViewCompat.m12939x(coordinatorLayout) && !ViewCompat.m12939x(v)) {
                ViewCompat.m12899a((View) v, true);
            }
            coordinatorLayout.mo1570a((View) v, i);
        }
        this.f2375k = coordinatorLayout.getHeight();
        this.f2367c = Math.max(0, this.f2375k - v.getHeight());
        this.f2368d = Math.max(this.f2375k - this.f2366b, this.f2367c);
        if (this.f2370f == 3) {
            ViewCompat.m12918e((View) v, this.f2367c);
        } else if (this.f2369e && this.f2370f == 5) {
            ViewCompat.m12918e((View) v, this.f2375k);
        } else if (this.f2370f == 4) {
            ViewCompat.m12918e((View) v, this.f2368d);
        }
        if (this.f2371g == null) {
            this.f2371g = ViewDragHelper.create(coordinatorLayout, this.f2383s);
        }
        this.f2376l = new WeakReference<>(v);
        this.f2377m = new WeakReference<>(m3151a((View) v));
        return true;
    }

    /* renamed from: a */
    public boolean mo1496a(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        boolean z = true;
        if (!v.isShown()) {
            return false;
        }
        int a = MotionEventCompat.m12830a(motionEvent);
        if (a == 0) {
            m3152a();
        }
        if (this.f2379o == null) {
            this.f2379o = VelocityTracker.obtain();
        }
        this.f2379o.addMovement(motionEvent);
        switch (a) {
            case 0:
                int x = (int) motionEvent.getX();
                this.f2381q = (int) motionEvent.getY();
                View view = (View) this.f2377m.get();
                if (view != null && coordinatorLayout.mo1576a(view, x, this.f2381q)) {
                    this.f2380p = motionEvent.getPointerId(motionEvent.getActionIndex());
                    this.f2382r = true;
                }
                this.f2372h = this.f2380p == -1 && !coordinatorLayout.mo1576a((View) v, x, this.f2381q);
                break;
            case 1:
            case 3:
                this.f2382r = false;
                this.f2380p = -1;
                if (this.f2372h) {
                    this.f2372h = false;
                    return false;
                }
                break;
        }
        if (!this.f2372h && this.f2371g.shouldInterceptTouchEvent(motionEvent)) {
            return true;
        }
        View view2 = (View) this.f2377m.get();
        if (a != 2 || view2 == null || this.f2372h || this.f2370f == 1 || coordinatorLayout.mo1576a(view2, (int) motionEvent.getX(), (int) motionEvent.getY()) || Math.abs(((float) this.f2381q) - motionEvent.getY()) <= ((float) this.f2371g.getTouchSlop())) {
            z = false;
        }
        return z;
    }

    /* renamed from: a */
    public boolean mo1497a(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2) {
        return view == this.f2377m.get() && (this.f2370f != 3 || super.mo1497a(coordinatorLayout, v, view, f, f2));
    }

    /* renamed from: a */
    public boolean mo1474a(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
        this.f2373i = 0;
        this.f2374j = false;
        return (i & 2) != 0;
    }

    /* renamed from: b */
    public Parcelable mo1479b(CoordinatorLayout coordinatorLayout, V v) {
        return new SavedState(super.mo1479b(coordinatorLayout, v), this.f2370f);
    }

    /* renamed from: b */
    public boolean mo1498b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (!v.isShown()) {
            return false;
        }
        int a = MotionEventCompat.m12830a(motionEvent);
        if (this.f2370f == 1 && a == 0) {
            return true;
        }
        this.f2371g.processTouchEvent(motionEvent);
        if (a == 0) {
            m3152a();
        }
        if (this.f2379o == null) {
            this.f2379o = VelocityTracker.obtain();
        }
        this.f2379o.addMovement(motionEvent);
        if (a == 2 && !this.f2372h && Math.abs(((float) this.f2381q) - motionEvent.getY()) > ((float) this.f2371g.getTouchSlop())) {
            this.f2371g.captureChildView(v, motionEvent.getPointerId(motionEvent.getActionIndex()));
        }
        return !this.f2372h;
    }
}
