package android.support.design.widget;

import android.content.Context;
import android.support.p001v4.widget.ScrollerCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

abstract class HeaderBehavior<V extends View> extends ViewOffsetBehavior<V> {

    /* renamed from: a */
    private Runnable f2479a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public ScrollerCompat f2480b;

    /* renamed from: c */
    private boolean f2481c;

    /* renamed from: d */
    private int f2482d = -1;

    /* renamed from: e */
    private int f2483e;

    /* renamed from: f */
    private int f2484f = -1;

    /* renamed from: g */
    private VelocityTracker f2485g;

    /* renamed from: android.support.design.widget.HeaderBehavior$a */
    class C0389a implements Runnable {

        /* renamed from: b */
        private final CoordinatorLayout f2487b;

        /* renamed from: c */
        private final V f2488c;

        C0389a(CoordinatorLayout coordinatorLayout, V v) {
            this.f2487b = coordinatorLayout;
            this.f2488c = v;
        }

        public void run() {
            if (this.f2488c != null && HeaderBehavior.this.f2480b != null) {
                if (HeaderBehavior.this.f2480b.computeScrollOffset()) {
                    HeaderBehavior.this.mo1689a_(this.f2487b, this.f2488c, HeaderBehavior.this.f2480b.getCurrY());
                    ViewCompat.m12897a((View) this.f2488c, (Runnable) this);
                    return;
                }
                HeaderBehavior.this.mo1460a(this.f2487b, this.f2488c);
            }
        }
    }

    public HeaderBehavior() {
    }

    public HeaderBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* renamed from: c */
    private void m3321c() {
        if (this.f2485g == null) {
            this.f2485g = VelocityTracker.obtain();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public int mo1451a() {
        return mo1475b();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public int mo1453a(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        int b = mo1475b();
        if (i2 == 0 || b < i2 || b > i3) {
            return 0;
        }
        int a = MathUtils.m16569a(i, i2, i3);
        if (b == a) {
            return 0;
        }
        mo1465a(a);
        return b - a;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public int mo1454a(V v) {
        return v.getHeight();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1460a(CoordinatorLayout coordinatorLayout, V v) {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public final boolean mo1688a(CoordinatorLayout coordinatorLayout, V v, int i, int i2, float f) {
        if (this.f2479a != null) {
            v.removeCallbacks(this.f2479a);
            this.f2479a = null;
        }
        if (this.f2480b == null) {
            this.f2480b = ScrollerCompat.create(v.getContext());
        }
        this.f2480b.fling(0, mo1475b(), 0, Math.round(f), 0, 0, i, i2);
        if (this.f2480b.computeScrollOffset()) {
            this.f2479a = new C0389a(coordinatorLayout, v);
            ViewCompat.m12897a((View) v, this.f2479a);
            return true;
        }
        mo1460a(coordinatorLayout, v);
        return false;
    }

    /* renamed from: a */
    public boolean mo1496a(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (this.f2484f < 0) {
            this.f2484f = ViewConfiguration.get(coordinatorLayout.getContext()).getScaledTouchSlop();
        }
        if (motionEvent.getAction() == 2 && this.f2481c) {
            return true;
        }
        switch (MotionEventCompat.m12830a(motionEvent)) {
            case 0:
                this.f2481c = false;
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();
                if (mo1481c(v) && coordinatorLayout.mo1576a((View) v, x, y)) {
                    this.f2483e = y;
                    this.f2482d = MotionEventCompat.m12833b(motionEvent, 0);
                    m3321c();
                    break;
                }
            case 1:
            case 3:
                this.f2481c = false;
                this.f2482d = -1;
                if (this.f2485g != null) {
                    this.f2485g.recycle();
                    this.f2485g = null;
                    break;
                }
                break;
            case 2:
                int i = this.f2482d;
                if (i != -1) {
                    int a = MotionEventCompat.m12831a(motionEvent, i);
                    if (a != -1) {
                        int d = (int) MotionEventCompat.m12836d(motionEvent, a);
                        if (Math.abs(d - this.f2483e) > this.f2484f) {
                            this.f2481c = true;
                            this.f2483e = d;
                            break;
                        }
                    }
                }
                break;
        }
        if (this.f2485g != null) {
            this.f2485g.addMovement(motionEvent);
        }
        return this.f2481c;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a_ */
    public int mo1689a_(CoordinatorLayout coordinatorLayout, V v, int i) {
        return mo1453a(coordinatorLayout, v, i, Integer.MIN_VALUE, (int) ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public final int mo1690b(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        return mo1453a(coordinatorLayout, v, mo1451a() - i, i2, i3);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public int mo1477b(V v) {
        return -v.getHeight();
    }

    /* renamed from: b */
    public boolean mo1498b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (this.f2484f < 0) {
            this.f2484f = ViewConfiguration.get(coordinatorLayout.getContext()).getScaledTouchSlop();
        }
        switch (MotionEventCompat.m12830a(motionEvent)) {
            case 0:
                int y = (int) motionEvent.getY();
                if (coordinatorLayout.mo1576a((View) v, (int) motionEvent.getX(), y) && mo1481c(v)) {
                    this.f2483e = y;
                    this.f2482d = MotionEventCompat.m12833b(motionEvent, 0);
                    m3321c();
                    break;
                } else {
                    return false;
                }
            case 1:
                if (this.f2485g != null) {
                    this.f2485g.addMovement(motionEvent);
                    this.f2485g.computeCurrentVelocity(1000);
                    mo1688a(coordinatorLayout, v, -mo1454a(v), 0, VelocityTrackerCompat.m12858b(this.f2485g, this.f2482d));
                    break;
                }
                break;
            case 2:
                int a = MotionEventCompat.m12831a(motionEvent, this.f2482d);
                if (a != -1) {
                    int d = (int) MotionEventCompat.m12836d(motionEvent, a);
                    int i = this.f2483e - d;
                    if (!this.f2481c && Math.abs(i) > this.f2484f) {
                        this.f2481c = true;
                        i = i > 0 ? i - this.f2484f : i + this.f2484f;
                    }
                    if (this.f2481c) {
                        this.f2483e = d;
                        mo1690b(coordinatorLayout, v, i, mo1477b(v), 0);
                        break;
                    }
                } else {
                    return false;
                }
                break;
            case 3:
                break;
        }
        this.f2481c = false;
        this.f2482d = -1;
        if (this.f2485g != null) {
            this.f2485g.recycle();
            this.f2485g = null;
        }
        if (this.f2485g != null) {
            this.f2485g.addMovement(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public boolean mo1481c(V v) {
        return false;
    }
}
