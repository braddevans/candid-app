package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.design.internal.ScrimInsetsFrameLayout;
import android.support.p003v7.view.SupportMenuInflater;
import android.support.p003v7.view.menu.MenuBuilder;
import android.support.p003v7.view.menu.MenuBuilder.Callback;
import android.support.p003v7.view.menu.MenuItemImpl;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;

public class NavigationView extends ScrimInsetsFrameLayout {

    /* renamed from: a */
    private static final int[] f2493a = {16842912};

    /* renamed from: b */
    private static final int[] f2494b = {-16842910};

    /* renamed from: c */
    private final NavigationMenu f2495c;

    /* renamed from: d */
    private final NavigationMenuPresenter f2496d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C0392a f2497e;

    /* renamed from: f */
    private int f2498f;

    /* renamed from: g */
    private MenuInflater f2499g;

    public static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = ParcelableCompat.m12486a(new ParcelableCompatCreatorCallbacks<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a */
        public Bundle f2501a;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            this.f2501a = parcel.readBundle(classLoader);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.f2501a);
        }
    }

    /* renamed from: android.support.design.widget.NavigationView$a */
    public interface C0392a {
        /* renamed from: a */
        boolean mo1718a(MenuItem menuItem);
    }

    public NavigationView(Context context) {
        this(context, null);
    }

    public NavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2496d = new NavigationMenuPresenter();
        ThemeUtils.m0a(context);
        this.f2495c = new NavigationMenu(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.NavigationView, i, C2393h.Widget_Design_NavigationView);
        setBackgroundDrawable(obtainStyledAttributes.getDrawable(C2394i.NavigationView_android_background));
        if (obtainStyledAttributes.hasValue(C2394i.NavigationView_elevation)) {
            ViewCompat.m12920f((View) this, (float) obtainStyledAttributes.getDimensionPixelSize(C2394i.NavigationView_elevation, 0));
        }
        ViewCompat.m12899a((View) this, obtainStyledAttributes.getBoolean(C2394i.NavigationView_android_fitsSystemWindows, false));
        this.f2498f = obtainStyledAttributes.getDimensionPixelSize(C2394i.NavigationView_android_maxWidth, 0);
        ColorStateList c = obtainStyledAttributes.hasValue(C2394i.NavigationView_itemIconTint) ? obtainStyledAttributes.getColorStateList(C2394i.NavigationView_itemIconTint) : m3343c(16842808);
        boolean z = false;
        int i2 = 0;
        if (obtainStyledAttributes.hasValue(C2394i.NavigationView_itemTextAppearance)) {
            i2 = obtainStyledAttributes.getResourceId(C2394i.NavigationView_itemTextAppearance, 0);
            z = true;
        }
        ColorStateList colorStateList = null;
        if (obtainStyledAttributes.hasValue(C2394i.NavigationView_itemTextColor)) {
            colorStateList = obtainStyledAttributes.getColorStateList(C2394i.NavigationView_itemTextColor);
        }
        if (!z && colorStateList == null) {
            colorStateList = m3343c(16842806);
        }
        Drawable drawable = obtainStyledAttributes.getDrawable(C2394i.NavigationView_itemBackground);
        this.f2495c.setCallback(new Callback() {
            public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
                return NavigationView.this.f2497e != null && NavigationView.this.f2497e.mo1718a(menuItem);
            }

            public void onMenuModeChange(MenuBuilder menuBuilder) {
            }
        });
        this.f2496d.mo13916a(1);
        this.f2496d.initForMenu(context, this.f2495c);
        this.f2496d.mo13917a(c);
        if (z) {
            this.f2496d.mo13926c(i2);
        }
        this.f2496d.mo13924b(colorStateList);
        this.f2496d.mo13918a(drawable);
        this.f2495c.addMenuPresenter(this.f2496d);
        addView((View) this.f2496d.getMenuView(this));
        if (obtainStyledAttributes.hasValue(C2394i.NavigationView_menu)) {
            mo1696a(obtainStyledAttributes.getResourceId(C2394i.NavigationView_menu, 0));
        }
        if (obtainStyledAttributes.hasValue(C2394i.NavigationView_headerLayout)) {
            mo1697b(obtainStyledAttributes.getResourceId(C2394i.NavigationView_headerLayout, 0));
        }
        obtainStyledAttributes.recycle();
    }

    /* renamed from: c */
    private ColorStateList m3343c(int i) {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(i, typedValue, true)) {
            return null;
        }
        ColorStateList colorStateList = getResources().getColorStateList(typedValue.resourceId);
        if (!getContext().getTheme().resolveAttribute(C2387b.colorPrimary, typedValue, true)) {
            return null;
        }
        int i2 = typedValue.data;
        int defaultColor = colorStateList.getDefaultColor();
        return new ColorStateList(new int[][]{f2494b, f2493a, EMPTY_STATE_SET}, new int[]{colorStateList.getColorForState(f2494b, defaultColor), i2, defaultColor});
    }

    private MenuInflater getMenuInflater() {
        if (this.f2499g == null) {
            this.f2499g = new SupportMenuInflater(getContext());
        }
        return this.f2499g;
    }

    /* renamed from: a */
    public void mo1696a(int i) {
        this.f2496d.mo13921a(true);
        getMenuInflater().inflate(i, this.f2495c);
        this.f2496d.mo13921a(false);
        this.f2496d.updateMenuView(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1428a(Rect rect) {
        this.f2496d.mo13928d(rect.top);
    }

    /* renamed from: b */
    public View mo1697b(int i) {
        return this.f2496d.mo13923b(i);
    }

    public int getHeaderCount() {
        return this.f2496d.mo13915a();
    }

    public Drawable getItemBackground() {
        return this.f2496d.mo13927d();
    }

    public ColorStateList getItemIconTintList() {
        return this.f2496d.mo13922b();
    }

    public ColorStateList getItemTextColor() {
        return this.f2496d.mo13925c();
    }

    public Menu getMenu() {
        return this.f2495c;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        switch (MeasureSpec.getMode(i)) {
            case Integer.MIN_VALUE:
                i = MeasureSpec.makeMeasureSpec(Math.min(MeasureSpec.getSize(i), this.f2498f), 1073741824);
                break;
            case 0:
                i = MeasureSpec.makeMeasureSpec(this.f2498f, 1073741824);
                break;
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.f2495c.restorePresenterStates(savedState.f2501a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2501a = new Bundle();
        this.f2495c.savePresenterStates(savedState.f2501a);
        return savedState;
    }

    public void setCheckedItem(int i) {
        MenuItem findItem = this.f2495c.findItem(i);
        if (findItem != null) {
            this.f2496d.mo13919a((MenuItemImpl) findItem);
        }
    }

    public void setItemBackground(Drawable drawable) {
        this.f2496d.mo13918a(drawable);
    }

    public void setItemBackgroundResource(int i) {
        setItemBackground(ContextCompat.getDrawable(getContext(), i));
    }

    public void setItemIconTintList(ColorStateList colorStateList) {
        this.f2496d.mo13917a(colorStateList);
    }

    public void setItemTextAppearance(int i) {
        this.f2496d.mo13926c(i);
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.f2496d.mo13924b(colorStateList);
    }

    public void setNavigationItemSelectedListener(C0392a aVar) {
        this.f2497e = aVar;
    }
}
