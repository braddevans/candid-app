package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.design.widget.CoordinatorLayout.C0380b;
import android.support.design.widget.CoordinatorLayout.C0382d;
import android.support.design.widget.Snackbar.SnackbarLayout;
import android.support.p003v7.widget.AppCompatDrawableManager;
import android.support.p003v7.widget.AppCompatImageHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import java.util.List;

@C0380b(mo1632a = Behavior.class)
public class FloatingActionButton extends VisibilityAwareImageButton {

    /* renamed from: a */
    private ColorStateList f2460a;

    /* renamed from: b */
    private Mode f2461b;

    /* renamed from: c */
    private int f2462c;

    /* renamed from: d */
    private int f2463d;

    /* renamed from: e */
    private int f2464e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public int f2465f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f2466g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final Rect f2467h;

    /* renamed from: i */
    private AppCompatImageHelper f2468i;

    /* renamed from: j */
    private FloatingActionButtonImpl f2469j;

    public static class Behavior extends android.support.design.widget.CoordinatorLayout.Behavior<FloatingActionButton> {

        /* renamed from: a */
        private static final boolean f2472a = (VERSION.SDK_INT >= 11);

        /* renamed from: b */
        private ValueAnimatorCompat f2473b;

        /* renamed from: c */
        private float f2474c;

        /* renamed from: d */
        private Rect f2475d;

        /* renamed from: a */
        private float m3301a(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton) {
            float f = 0.0f;
            List d = coordinatorLayout.mo1587d((View) floatingActionButton);
            int size = d.size();
            for (int i = 0; i < size; i++) {
                View view = (View) d.get(i);
                if ((view instanceof SnackbarLayout) && coordinatorLayout.mo1577a((View) floatingActionButton, view)) {
                    f = Math.min(f, ViewCompat.m12931p(view) - ((float) view.getHeight()));
                }
            }
            return f;
        }

        /* renamed from: a */
        private boolean m3302a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, FloatingActionButton floatingActionButton) {
            if (((C0382d) floatingActionButton.getLayoutParams()).mo1635a() != appBarLayout.getId() || floatingActionButton.getUserSetVisibility() != 0) {
                return false;
            }
            if (this.f2475d == null) {
                this.f2475d = new Rect();
            }
            Rect rect = this.f2475d;
            ViewGroupUtils.m1250b(coordinatorLayout, appBarLayout, rect);
            if (rect.bottom <= appBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
                floatingActionButton.m3296b(null, false);
            } else {
                floatingActionButton.m3292a((C0387a) null, false);
            }
            return true;
        }

        /* renamed from: b */
        private void m3303b(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton) {
            Rect a = floatingActionButton.f2467h;
            if (a != null && a.centerX() > 0 && a.centerY() > 0) {
                C0382d dVar = (C0382d) floatingActionButton.getLayoutParams();
                int i = 0;
                int i2 = 0;
                if (floatingActionButton.getRight() >= coordinatorLayout.getWidth() - dVar.rightMargin) {
                    i2 = a.right;
                } else if (floatingActionButton.getLeft() <= dVar.leftMargin) {
                    i2 = -a.left;
                }
                if (floatingActionButton.getBottom() >= coordinatorLayout.getBottom() - dVar.bottomMargin) {
                    i = a.bottom;
                } else if (floatingActionButton.getTop() <= dVar.topMargin) {
                    i = -a.top;
                }
                floatingActionButton.offsetTopAndBottom(i);
                floatingActionButton.offsetLeftAndRight(i2);
            }
        }

        /* renamed from: d */
        private void m3304d(CoordinatorLayout coordinatorLayout, final FloatingActionButton floatingActionButton, View view) {
            float a = m3301a(coordinatorLayout, floatingActionButton);
            if (this.f2474c != a) {
                float p = ViewCompat.m12931p(floatingActionButton);
                if (this.f2473b != null && this.f2473b.mo142b()) {
                    this.f2473b.mo145e();
                }
                if (!floatingActionButton.isShown() || Math.abs(p - a) <= ((float) floatingActionButton.getHeight()) * 0.667f) {
                    ViewCompat.m12904b((View) floatingActionButton, a);
                } else {
                    if (this.f2473b == null) {
                        this.f2473b = ViewUtils.m1925a();
                        this.f2473b.mo141a(AnimationUtils.f10964b);
                        this.f2473b.mo140a((C0046c) new C0046c() {
                            /* renamed from: a */
                            public void mo155a(ValueAnimatorCompat abVar) {
                                ViewCompat.m12904b((View) floatingActionButton, abVar.mo144d());
                            }
                        });
                    }
                    this.f2473b.mo136a(p, a);
                    this.f2473b.mo135a();
                }
                this.f2474c = a;
            }
        }

        /* renamed from: a */
        public boolean mo1471a(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, int i) {
            List d = coordinatorLayout.mo1587d((View) floatingActionButton);
            int size = d.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = (View) d.get(i2);
                if ((view instanceof AppBarLayout) && m3302a(coordinatorLayout, (AppBarLayout) view, floatingActionButton)) {
                    break;
                }
            }
            coordinatorLayout.mo1570a((View) floatingActionButton, i);
            m3303b(coordinatorLayout, floatingActionButton);
            return true;
        }

        /* renamed from: a */
        public boolean mo1491b(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, View view) {
            return f2472a && (view instanceof SnackbarLayout);
        }

        /* renamed from: b */
        public boolean mo1492c(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, View view) {
            if (view instanceof SnackbarLayout) {
                m3304d(coordinatorLayout, floatingActionButton, view);
            } else if (view instanceof AppBarLayout) {
                m3302a(coordinatorLayout, (AppBarLayout) view, floatingActionButton);
            }
            return false;
        }

        /* renamed from: c */
        public void mo1627d(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, View view) {
            if (view instanceof SnackbarLayout) {
                m3304d(coordinatorLayout, floatingActionButton, view);
            }
        }
    }

    /* renamed from: android.support.design.widget.FloatingActionButton$a */
    public static abstract class C0387a {
        /* renamed from: a */
        public void mo1682a(FloatingActionButton floatingActionButton) {
        }

        /* renamed from: b */
        public void mo1683b(FloatingActionButton floatingActionButton) {
        }
    }

    /* renamed from: android.support.design.widget.FloatingActionButton$b */
    class C0388b implements ShadowViewDelegate {
        private C0388b() {
        }

        /* renamed from: a */
        public float mo1684a() {
            return ((float) FloatingActionButton.this.getSizeDimension()) / 2.0f;
        }

        /* renamed from: a */
        public void mo1685a(int i, int i2, int i3, int i4) {
            FloatingActionButton.this.f2467h.set(i, i2, i3, i4);
            FloatingActionButton.this.setPadding(FloatingActionButton.this.f2465f + i, FloatingActionButton.this.f2465f + i2, FloatingActionButton.this.f2465f + i3, FloatingActionButton.this.f2465f + i4);
        }

        /* renamed from: a */
        public void mo1686a(Drawable drawable) {
            FloatingActionButton.super.setBackgroundDrawable(drawable);
        }

        /* renamed from: b */
        public boolean mo1687b() {
            return FloatingActionButton.this.f2466g;
        }
    }

    public FloatingActionButton(Context context) {
        this(context, null);
    }

    public FloatingActionButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FloatingActionButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2467h = new Rect();
        ThemeUtils.m0a(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.FloatingActionButton, i, C2393h.Widget_Design_FloatingActionButton);
        this.f2460a = obtainStyledAttributes.getColorStateList(C2394i.FloatingActionButton_backgroundTint);
        this.f2461b = m3288a(obtainStyledAttributes.getInt(C2394i.FloatingActionButton_backgroundTintMode, -1), (Mode) null);
        this.f2463d = obtainStyledAttributes.getColor(C2394i.FloatingActionButton_rippleColor, 0);
        this.f2464e = obtainStyledAttributes.getInt(C2394i.FloatingActionButton_fabSize, 0);
        this.f2462c = obtainStyledAttributes.getDimensionPixelSize(C2394i.FloatingActionButton_borderWidth, 0);
        float dimension = obtainStyledAttributes.getDimension(C2394i.FloatingActionButton_elevation, 0.0f);
        float dimension2 = obtainStyledAttributes.getDimension(C2394i.FloatingActionButton_pressedTranslationZ, 0.0f);
        this.f2466g = obtainStyledAttributes.getBoolean(C2394i.FloatingActionButton_useCompatPadding, false);
        obtainStyledAttributes.recycle();
        this.f2468i = new AppCompatImageHelper(this, AppCompatDrawableManager.get());
        this.f2468i.loadFromAttributes(attributeSet, i);
        this.f2465f = (getSizeDimension() - ((int) getResources().getDimension(C2389d.design_fab_image_size))) / 2;
        getImpl().mo15971a(this.f2460a, this.f2461b, this.f2463d, this.f2462c);
        getImpl().mo16354c(dimension);
        getImpl().mo16355d(dimension2);
        getImpl().mo16357g();
    }

    /* renamed from: a */
    private static int m3287a(int i, int i2) {
        int i3 = i;
        int mode = MeasureSpec.getMode(i2);
        int size = MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                return Math.min(i, size);
            case 0:
                return i;
            case 1073741824:
                return size;
            default:
                return i3;
        }
    }

    /* renamed from: a */
    static Mode m3288a(int i, Mode mode) {
        switch (i) {
            case 3:
                return Mode.SRC_OVER;
            case 5:
                return Mode.SRC_IN;
            case 9:
                return Mode.SRC_ATOP;
            case 14:
                return Mode.MULTIPLY;
            case 15:
                return Mode.SCREEN;
            default:
                return mode;
        }
    }

    /* renamed from: a */
    private C2997a m3290a(final C0387a aVar) {
        if (aVar == null) {
            return null;
        }
        return new C2997a() {
            /* renamed from: a */
            public void mo1676a() {
                aVar.mo1682a(FloatingActionButton.this);
            }

            /* renamed from: b */
            public void mo1677b() {
                aVar.mo1683b(FloatingActionButton.this);
            }
        };
    }

    /* renamed from: a */
    private FloatingActionButtonImpl m3291a() {
        int i = VERSION.SDK_INT;
        return i >= 21 ? new FloatingActionButtonLollipop(this, new C0388b()) : i >= 14 ? new FloatingActionButtonIcs(this, new C0388b()) : new FloatingActionButtonEclairMr1(this, new C0388b());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3292a(C0387a aVar, boolean z) {
        getImpl().mo15978b(m3290a(aVar), z);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m3296b(C0387a aVar, boolean z) {
        getImpl().mo15974a(m3290a(aVar), z);
    }

    private FloatingActionButtonImpl getImpl() {
        if (this.f2469j == null) {
            this.f2469j = m3291a();
        }
        return this.f2469j;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        getImpl().mo15975a(getDrawableState());
    }

    public ColorStateList getBackgroundTintList() {
        return this.f2460a;
    }

    public Mode getBackgroundTintMode() {
        return this.f2461b;
    }

    public float getCompatElevation() {
        return getImpl().mo15967a();
    }

    public Drawable getContentBackground() {
        return getImpl().mo16356f();
    }

    /* access modifiers changed from: 0000 */
    public final int getSizeDimension() {
        switch (this.f2464e) {
            case 1:
                return getResources().getDimensionPixelSize(C2389d.design_fab_size_mini);
            default:
                return getResources().getDimensionPixelSize(C2389d.design_fab_size_normal);
        }
    }

    public boolean getUseCompatPadding() {
        return this.f2466g;
    }

    @TargetApi(11)
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        getImpl().mo15976b();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getImpl().mo16358h();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getImpl().mo16359i();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int sizeDimension = getSizeDimension();
        int min = Math.min(m3287a(sizeDimension, i), m3287a(sizeDimension, i2));
        setMeasuredDimension(this.f2467h.left + min + this.f2467h.right, this.f2467h.top + min + this.f2467h.bottom);
    }

    public void setBackgroundColor(int i) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundResource(int i) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundTintList(ColorStateList colorStateList) {
        if (this.f2460a != colorStateList) {
            this.f2460a = colorStateList;
            getImpl().mo15970a(colorStateList);
        }
    }

    public void setBackgroundTintMode(Mode mode) {
        if (this.f2461b != mode) {
            this.f2461b = mode;
            getImpl().mo15972a(mode);
        }
    }

    public void setCompatElevation(float f) {
        getImpl().mo16354c(f);
    }

    public void setImageResource(int i) {
        this.f2468i.setImageResource(i);
    }

    public void setRippleColor(int i) {
        if (this.f2463d != i) {
            this.f2463d = i;
            getImpl().mo15969a(i);
        }
    }

    public void setUseCompatPadding(boolean z) {
        if (this.f2466g != z) {
            this.f2466g = z;
            getImpl().mo15979c();
        }
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i) {
        super.setVisibility(i);
    }
}
