package android.support.design.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

public class VisibilityAwareImageButton extends ImageButton {

    /* renamed from: a */
    private int f2644a;

    public VisibilityAwareImageButton(Context context) {
        this(context, null);
    }

    public VisibilityAwareImageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VisibilityAwareImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2644a = getVisibility();
    }

    /* renamed from: a */
    public final void mo1861a(int i, boolean z) {
        super.setVisibility(i);
        if (z) {
            this.f2644a = i;
        }
    }

    /* access modifiers changed from: 0000 */
    public final int getUserSetVisibility() {
        return this.f2644a;
    }

    public void setVisibility(int i) {
        mo1861a(i, true);
    }
}
