package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.view.ViewParent;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoordinatorLayout extends ViewGroup implements NestedScrollingParent {

    /* renamed from: a */
    static final String f2415a;

    /* renamed from: b */
    static final Class<?>[] f2416b = {Context.class, AttributeSet.class};

    /* renamed from: c */
    static final ThreadLocal<Map<String, Constructor<Behavior>>> f2417c = new ThreadLocal<>();

    /* renamed from: e */
    static final Comparator<View> f2418e;

    /* renamed from: f */
    static final CoordinatorLayoutInsetsHelper f2419f;

    /* renamed from: A */
    private final NestedScrollingParentHelper f2420A;

    /* renamed from: d */
    final Comparator<View> f2421d;

    /* renamed from: g */
    private final List<View> f2422g;

    /* renamed from: h */
    private final List<View> f2423h;

    /* renamed from: i */
    private final List<View> f2424i;

    /* renamed from: j */
    private final Rect f2425j;

    /* renamed from: k */
    private final Rect f2426k;

    /* renamed from: l */
    private final Rect f2427l;

    /* renamed from: m */
    private final int[] f2428m;

    /* renamed from: n */
    private Paint f2429n;

    /* renamed from: o */
    private boolean f2430o;

    /* renamed from: p */
    private boolean f2431p;

    /* renamed from: q */
    private int[] f2432q;

    /* renamed from: r */
    private View f2433r;

    /* renamed from: s */
    private View f2434s;

    /* renamed from: t */
    private View f2435t;

    /* renamed from: u */
    private C0383e f2436u;

    /* renamed from: v */
    private boolean f2437v;

    /* renamed from: w */
    private WindowInsetsCompat f2438w;

    /* renamed from: x */
    private boolean f2439x;

    /* renamed from: y */
    private Drawable f2440y;
    /* access modifiers changed from: private */

    /* renamed from: z */
    public OnHierarchyChangeListener f2441z;

    public static abstract class Behavior<V extends View> {
        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
        }

        /* renamed from: a */
        public WindowInsetsCompat mo1623a(CoordinatorLayout coordinatorLayout, V v, WindowInsetsCompat fzVar) {
            return fzVar;
        }

        /* renamed from: a */
        public void mo1461a(CoordinatorLayout coordinatorLayout, V v, Parcelable parcelable) {
        }

        /* renamed from: a */
        public void mo1462a(CoordinatorLayout coordinatorLayout, V v, View view) {
        }

        /* renamed from: a */
        public void mo1463a(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int i3, int i4) {
        }

        /* renamed from: a */
        public void mo1464a(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr) {
        }

        /* renamed from: a */
        public boolean mo1471a(CoordinatorLayout coordinatorLayout, V v, int i) {
            return false;
        }

        /* renamed from: a */
        public boolean mo1472a(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3, int i4) {
            return false;
        }

        /* renamed from: a */
        public boolean mo1496a(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
            return false;
        }

        /* renamed from: a */
        public boolean mo1497a(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2) {
            return false;
        }

        /* renamed from: a */
        public boolean mo1473a(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2, boolean z) {
            return false;
        }

        /* renamed from: a */
        public boolean mo1474a(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
            return false;
        }

        /* renamed from: b */
        public Parcelable mo1479b(CoordinatorLayout coordinatorLayout, V v) {
            return BaseSavedState.EMPTY_STATE;
        }

        /* renamed from: b */
        public void mo1624b(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
        }

        /* renamed from: b */
        public boolean mo1498b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
            return false;
        }

        /* renamed from: b */
        public boolean mo1491b(CoordinatorLayout coordinatorLayout, V v, View view) {
            return false;
        }

        /* renamed from: c */
        public int mo1625c(CoordinatorLayout coordinatorLayout, V v) {
            return -16777216;
        }

        /* renamed from: c */
        public boolean mo1492c(CoordinatorLayout coordinatorLayout, V v, View view) {
            return false;
        }

        /* renamed from: d */
        public float mo1626d(CoordinatorLayout coordinatorLayout, V v) {
            return 0.0f;
        }

        /* renamed from: d */
        public void mo1627d(CoordinatorLayout coordinatorLayout, V v, View view) {
        }

        /* renamed from: e */
        public boolean mo1628e(CoordinatorLayout coordinatorLayout, V v) {
            return mo1626d(coordinatorLayout, v) > 0.0f;
        }
    }

    public static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = ParcelableCompat.m12486a(new ParcelableCompatCreatorCallbacks<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a */
        SparseArray<Parcelable> f2443a;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            int readInt = parcel.readInt();
            int[] iArr = new int[readInt];
            parcel.readIntArray(iArr);
            Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
            this.f2443a = new SparseArray<>(readInt);
            for (int i = 0; i < readInt; i++) {
                this.f2443a.append(iArr[i], readParcelableArray[i]);
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            int i2 = this.f2443a != null ? this.f2443a.size() : 0;
            parcel.writeInt(i2);
            int[] iArr = new int[i2];
            Parcelable[] parcelableArr = new Parcelable[i2];
            for (int i3 = 0; i3 < i2; i3++) {
                iArr[i3] = this.f2443a.keyAt(i3);
                parcelableArr[i3] = (Parcelable) this.f2443a.valueAt(i3);
            }
            parcel.writeIntArray(iArr);
            parcel.writeParcelableArray(parcelableArr, i);
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$a */
    class C0379a implements OnApplyWindowInsetsListener {
        private C0379a() {
        }

        public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat fzVar) {
            return CoordinatorLayout.this.m3208a(fzVar);
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    /* renamed from: android.support.design.widget.CoordinatorLayout$b */
    public @interface C0380b {
        /* renamed from: a */
        Class<? extends Behavior> mo1632a();
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$c */
    class C0381c implements OnHierarchyChangeListener {
        private C0381c() {
        }

        public void onChildViewAdded(View view, View view2) {
            if (CoordinatorLayout.this.f2441z != null) {
                CoordinatorLayout.this.f2441z.onChildViewAdded(view, view2);
            }
        }

        public void onChildViewRemoved(View view, View view2) {
            CoordinatorLayout.this.mo1579b(view2);
            if (CoordinatorLayout.this.f2441z != null) {
                CoordinatorLayout.this.f2441z.onChildViewRemoved(view, view2);
            }
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$d */
    public static class C0382d extends MarginLayoutParams {

        /* renamed from: a */
        Behavior f2446a;

        /* renamed from: b */
        boolean f2447b = false;

        /* renamed from: c */
        public int f2448c = 0;

        /* renamed from: d */
        public int f2449d = 0;

        /* renamed from: e */
        public int f2450e = -1;

        /* renamed from: f */
        int f2451f = -1;

        /* renamed from: g */
        View f2452g;

        /* renamed from: h */
        View f2453h;

        /* renamed from: i */
        final Rect f2454i = new Rect();

        /* renamed from: j */
        Object f2455j;

        /* renamed from: k */
        private boolean f2456k;

        /* renamed from: l */
        private boolean f2457l;

        /* renamed from: m */
        private boolean f2458m;

        public C0382d(int i, int i2) {
            super(i, i2);
        }

        C0382d(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.CoordinatorLayout_LayoutParams);
            this.f2448c = obtainStyledAttributes.getInteger(C2394i.CoordinatorLayout_LayoutParams_android_layout_gravity, 0);
            this.f2451f = obtainStyledAttributes.getResourceId(C2394i.CoordinatorLayout_LayoutParams_layout_anchor, -1);
            this.f2449d = obtainStyledAttributes.getInteger(C2394i.CoordinatorLayout_LayoutParams_layout_anchorGravity, 0);
            this.f2450e = obtainStyledAttributes.getInteger(C2394i.CoordinatorLayout_LayoutParams_layout_keyline, -1);
            this.f2447b = obtainStyledAttributes.hasValue(C2394i.CoordinatorLayout_LayoutParams_layout_behavior);
            if (this.f2447b) {
                this.f2446a = CoordinatorLayout.m3205a(context, attributeSet, obtainStyledAttributes.getString(C2394i.CoordinatorLayout_LayoutParams_layout_behavior));
            }
            obtainStyledAttributes.recycle();
        }

        public C0382d(C0382d dVar) {
            super(dVar);
        }

        public C0382d(LayoutParams layoutParams) {
            super(layoutParams);
        }

        public C0382d(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        /* renamed from: a */
        private void m3267a(View view, CoordinatorLayout coordinatorLayout) {
            this.f2452g = coordinatorLayout.findViewById(this.f2451f);
            if (this.f2452g != null) {
                if (this.f2452g != coordinatorLayout) {
                    View view2 = this.f2452g;
                    ViewParent parent = this.f2452g.getParent();
                    while (parent != coordinatorLayout && parent != null) {
                        if (parent != view) {
                            if (parent instanceof View) {
                                view2 = (View) parent;
                            }
                            parent = parent.getParent();
                        } else if (coordinatorLayout.isInEditMode()) {
                            this.f2453h = null;
                            this.f2452g = null;
                            return;
                        } else {
                            throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                        }
                    }
                    this.f2453h = view2;
                } else if (coordinatorLayout.isInEditMode()) {
                    this.f2453h = null;
                    this.f2452g = null;
                } else {
                    throw new IllegalStateException("View can not be anchored to the the parent CoordinatorLayout");
                }
            } else if (coordinatorLayout.isInEditMode()) {
                this.f2453h = null;
                this.f2452g = null;
            } else {
                throw new IllegalStateException("Could not find CoordinatorLayout descendant view with id " + coordinatorLayout.getResources().getResourceName(this.f2451f) + " to anchor view " + view);
            }
        }

        /* renamed from: b */
        private boolean m3268b(View view, CoordinatorLayout coordinatorLayout) {
            if (this.f2452g.getId() != this.f2451f) {
                return false;
            }
            View view2 = this.f2452g;
            for (ViewParent parent = this.f2452g.getParent(); parent != coordinatorLayout; parent = parent.getParent()) {
                if (parent == null || parent == view) {
                    this.f2453h = null;
                    this.f2452g = null;
                    return false;
                }
                if (parent instanceof View) {
                    view2 = (View) parent;
                }
            }
            this.f2453h = view2;
            return true;
        }

        /* renamed from: a */
        public int mo1635a() {
            return this.f2451f;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo1636a(Rect rect) {
            this.f2454i.set(rect);
        }

        /* renamed from: a */
        public void mo1637a(Behavior behavior) {
            if (this.f2446a != behavior) {
                this.f2446a = behavior;
                this.f2455j = null;
                this.f2447b = true;
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo1638a(boolean z) {
            this.f2457l = z;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo1639a(CoordinatorLayout coordinatorLayout, View view) {
            if (this.f2456k) {
                return true;
            }
            boolean e = (this.f2446a != null ? this.f2446a.mo1628e(coordinatorLayout, view) : false) | this.f2456k;
            this.f2456k = e;
            return e;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo1640a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 == this.f2453h || (this.f2446a != null && this.f2446a.mo1491b(coordinatorLayout, view, view2));
        }

        /* renamed from: b */
        public Behavior mo1641b() {
            return this.f2446a;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public View mo1642b(CoordinatorLayout coordinatorLayout, View view) {
            if (this.f2451f == -1) {
                this.f2453h = null;
                this.f2452g = null;
                return null;
            }
            if (this.f2452g == null || !m3268b(view, coordinatorLayout)) {
                m3267a(view, coordinatorLayout);
            }
            return this.f2452g;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void mo1643b(boolean z) {
            this.f2458m = z;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public Rect mo1644c() {
            return this.f2454i;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public boolean mo1645d() {
            return this.f2452g == null && this.f2451f != -1;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: e */
        public boolean mo1646e() {
            if (this.f2446a == null) {
                this.f2456k = false;
            }
            return this.f2456k;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: f */
        public void mo1647f() {
            this.f2456k = false;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: g */
        public void mo1648g() {
            this.f2457l = false;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: h */
        public boolean mo1649h() {
            return this.f2457l;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: i */
        public boolean mo1650i() {
            return this.f2458m;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: j */
        public void mo1651j() {
            this.f2458m = false;
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$e */
    class C0383e implements OnPreDrawListener {
        C0383e() {
        }

        public boolean onPreDraw() {
            CoordinatorLayout.this.mo1575a(false);
            return true;
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$f */
    static class C0384f implements Comparator<View> {
        C0384f() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            float G = ViewCompat.m12878G(view);
            float G2 = ViewCompat.m12878G(view2);
            if (G > G2) {
                return -1;
            }
            return G < G2 ? 1 : 0;
        }
    }

    static {
        Package packageR = CoordinatorLayout.class.getPackage();
        f2415a = packageR != null ? packageR.getName() : null;
        if (VERSION.SDK_INT >= 21) {
            f2418e = new C0384f();
            f2419f = new CoordinatorLayoutInsetsHelperLollipop();
        } else {
            f2418e = null;
            f2419f = null;
        }
    }

    public CoordinatorLayout(Context context) {
        this(context, null);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2421d = new Comparator<View>() {
            /* renamed from: a */
            public int compare(View view, View view2) {
                if (view == view2) {
                    return 0;
                }
                if (((C0382d) view.getLayoutParams()).mo1640a(CoordinatorLayout.this, view, view2)) {
                    return 1;
                }
                return ((C0382d) view2.getLayoutParams()).mo1640a(CoordinatorLayout.this, view2, view) ? -1 : 0;
            }
        };
        this.f2422g = new ArrayList();
        this.f2423h = new ArrayList();
        this.f2424i = new ArrayList();
        this.f2425j = new Rect();
        this.f2426k = new Rect();
        this.f2427l = new Rect();
        this.f2428m = new int[2];
        this.f2420A = new NestedScrollingParentHelper(this);
        ThemeUtils.m0a(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.CoordinatorLayout, i, C2393h.Widget_Design_CoordinatorLayout);
        int resourceId = obtainStyledAttributes.getResourceId(C2394i.CoordinatorLayout_keylines, 0);
        if (resourceId != 0) {
            Resources resources = context.getResources();
            this.f2432q = resources.getIntArray(resourceId);
            float f = resources.getDisplayMetrics().density;
            int length = this.f2432q.length;
            for (int i2 = 0; i2 < length; i2++) {
                int[] iArr = this.f2432q;
                iArr[i2] = (int) (((float) iArr[i2]) * f);
            }
        }
        this.f2440y = obtainStyledAttributes.getDrawable(C2394i.CoordinatorLayout_statusBarBackground);
        obtainStyledAttributes.recycle();
        if (f2419f != null) {
            f2419f.mo14906a(this, new C0379a());
        }
        super.setOnHierarchyChangeListener(new C0381c());
    }

    /* renamed from: a */
    private int m3204a(int i) {
        if (this.f2432q == null) {
            Log.e("CoordinatorLayout", "No keylines defined for " + this + " - attempted index lookup " + i);
            return 0;
        } else if (i >= 0 && i < this.f2432q.length) {
            return this.f2432q[i];
        } else {
            Log.e("CoordinatorLayout", "Keyline index " + i + " out of range for " + this);
            return 0;
        }
    }

    /* renamed from: a */
    static Behavior m3205a(Context context, AttributeSet attributeSet, String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        r4 = str.startsWith(".") ? context.getPackageName() + str : str.indexOf(46) >= 0 ? str : !TextUtils.isEmpty(f2415a) ? f2415a + '.' + str : str;
        try {
            Map map = (Map) f2417c.get();
            if (map == null) {
                map = new HashMap();
                f2417c.set(map);
            }
            Constructor constructor = (Constructor) map.get(r4);
            if (constructor == null) {
                constructor = Class.forName(r4, true, context.getClassLoader()).getConstructor(f2416b);
                constructor.setAccessible(true);
                map.put(r4, constructor);
            }
            return (Behavior) constructor.newInstance(new Object[]{context, attributeSet});
        } catch (Exception e) {
            throw new RuntimeException("Could not inflate Behavior subclass " + r4, e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public WindowInsetsCompat m3208a(WindowInsetsCompat fzVar) {
        boolean z = true;
        if (this.f2438w == fzVar) {
            return fzVar;
        }
        this.f2438w = fzVar;
        this.f2439x = fzVar != null && fzVar.mo13567b() > 0;
        if (this.f2439x || getBackground() != null) {
            z = false;
        }
        setWillNotDraw(z);
        WindowInsetsCompat b = m3214b(fzVar);
        requestLayout();
        return b;
    }

    /* renamed from: a */
    private void m3209a(View view, View view2, int i) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        Rect rect = this.f2425j;
        Rect rect2 = this.f2426k;
        mo1573a(view2, rect);
        mo1572a(view, i, rect, rect2);
        view.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
    }

    /* renamed from: a */
    private void m3210a(List<View> list) {
        list.clear();
        boolean isChildrenDrawingOrderEnabled = isChildrenDrawingOrderEnabled();
        int childCount = getChildCount();
        int i = childCount - 1;
        while (i >= 0) {
            list.add(getChildAt(isChildrenDrawingOrderEnabled ? getChildDrawingOrder(childCount, i) : i));
            i--;
        }
        if (f2418e != null) {
            Collections.sort(list, f2418e);
        }
    }

    /* renamed from: a */
    private static void m3211a(List<View> list, Comparator<View> comparator) {
        if (list != null && list.size() >= 2) {
            View[] viewArr = new View[list.size()];
            list.toArray(viewArr);
            for (int i = 0; i < r1; i++) {
                int i2 = i;
                for (int i3 = i + 1; i3 < r1; i3++) {
                    if (comparator.compare(viewArr[i3], viewArr[i2]) < 0) {
                        i2 = i3;
                    }
                }
                if (i != i2) {
                    View view = viewArr[i2];
                    viewArr[i2] = viewArr[i];
                    viewArr[i] = view;
                }
            }
            list.clear();
            for (View add : viewArr) {
                list.add(add);
            }
        }
    }

    /* renamed from: a */
    private boolean m3212a(MotionEvent motionEvent, int i) {
        boolean z = false;
        boolean z2 = false;
        MotionEvent motionEvent2 = null;
        int a = MotionEventCompat.m12830a(motionEvent);
        List<View> list = this.f2423h;
        m3210a(list);
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) list.get(i2);
            C0382d dVar = (C0382d) view.getLayoutParams();
            Behavior b = dVar.mo1641b();
            if ((z || z2) && a != 0) {
                if (b != null) {
                    if (motionEvent2 == null) {
                        long uptimeMillis = SystemClock.uptimeMillis();
                        motionEvent2 = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    }
                    switch (i) {
                        case 0:
                            b.mo1496a(this, view, motionEvent2);
                            break;
                        case 1:
                            b.mo1498b(this, view, motionEvent2);
                            break;
                    }
                }
            } else {
                if (!z && b != null) {
                    switch (i) {
                        case 0:
                            z = b.mo1496a(this, view, motionEvent);
                            break;
                        case 1:
                            z = b.mo1498b(this, view, motionEvent);
                            break;
                    }
                    if (z) {
                        this.f2433r = view;
                    }
                }
                boolean e = dVar.mo1646e();
                boolean a2 = dVar.mo1639a(this, view);
                z2 = a2 && !e;
                if (a2 && !z2) {
                    list.clear();
                    return z;
                }
            }
        }
        list.clear();
        return z;
    }

    /* renamed from: b */
    private static int m3213b(int i) {
        if (i == 0) {
            return 8388659;
        }
        return i;
    }

    /* renamed from: b */
    private WindowInsetsCompat m3214b(WindowInsetsCompat fzVar) {
        if (fzVar.mo13570e()) {
            return fzVar;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (ViewCompat.m12939x(childAt)) {
                Behavior b = ((C0382d) childAt.getLayoutParams()).mo1641b();
                if (b != null) {
                    fzVar = b.mo1623a(this, childAt, fzVar);
                    if (fzVar.mo13570e()) {
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        return fzVar;
    }

    /* renamed from: b */
    private void m3215b(View view, int i, int i2) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        int a = GravityCompat.m12720a(m3216c(dVar.f2448c), i2);
        int i3 = a & 7;
        int i4 = a & 112;
        int width = getWidth();
        int height = getHeight();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        if (i2 == 1) {
            i = width - i;
        }
        int a2 = m3204a(i) - measuredWidth;
        int i5 = 0;
        switch (i3) {
            case 1:
                a2 += measuredWidth / 2;
                break;
            case 5:
                a2 += measuredWidth;
                break;
        }
        switch (i4) {
            case 16:
                i5 = 0 + (measuredHeight / 2);
                break;
            case 80:
                i5 = 0 + measuredHeight;
                break;
        }
        int max = Math.max(getPaddingLeft() + dVar.leftMargin, Math.min(a2, ((width - getPaddingRight()) - measuredWidth) - dVar.rightMargin));
        int max2 = Math.max(getPaddingTop() + dVar.topMargin, Math.min(i5, ((height - getPaddingBottom()) - measuredHeight) - dVar.bottomMargin));
        view.layout(max, max2, max + measuredWidth, max2 + measuredHeight);
    }

    /* renamed from: c */
    private static int m3216c(int i) {
        if (i == 0) {
            return 8388661;
        }
        return i;
    }

    /* renamed from: c */
    private void m3217c(View view, int i) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        Rect rect = this.f2425j;
        rect.set(getPaddingLeft() + dVar.leftMargin, getPaddingTop() + dVar.topMargin, (getWidth() - getPaddingRight()) - dVar.rightMargin, (getHeight() - getPaddingBottom()) - dVar.bottomMargin);
        if (this.f2438w != null && ViewCompat.m12939x(this) && !ViewCompat.m12939x(view)) {
            rect.left += this.f2438w.mo13565a();
            rect.top += this.f2438w.mo13567b();
            rect.right -= this.f2438w.mo13568c();
            rect.bottom -= this.f2438w.mo13569d();
        }
        Rect rect2 = this.f2426k;
        GravityCompat.m12721a(m3213b(dVar.f2448c), view.getMeasuredWidth(), view.getMeasuredHeight(), rect, rect2, i);
        view.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
    }

    /* renamed from: d */
    private static int m3218d(int i) {
        if (i == 0) {
            return 17;
        }
        return i;
    }

    /* renamed from: e */
    private void m3219e() {
        if (this.f2433r != null) {
            Behavior b = ((C0382d) this.f2433r.getLayoutParams()).mo1641b();
            if (b != null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                b.mo1498b(this, this.f2433r, obtain);
                obtain.recycle();
            }
            this.f2433r = null;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ((C0382d) getChildAt(i).getLayoutParams()).mo1647f();
        }
        this.f2430o = false;
    }

    /* renamed from: f */
    private void m3220f() {
        this.f2422g.clear();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            mo1567a(childAt).mo1642b(this, childAt);
            this.f2422g.add(childAt);
        }
        m3211a(this.f2422g, this.f2421d);
    }

    /* renamed from: a */
    public C0382d generateLayoutParams(AttributeSet attributeSet) {
        return new C0382d(getContext(), attributeSet);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C0382d mo1567a(View view) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        if (!dVar.f2447b) {
            C0380b bVar = null;
            for (Class cls = view.getClass(); cls != null; cls = cls.getSuperclass()) {
                bVar = (C0380b) cls.getAnnotation(C0380b.class);
                if (bVar != null) {
                    break;
                }
            }
            if (bVar != null) {
                try {
                    dVar.mo1637a((Behavior) bVar.mo1632a().newInstance());
                } catch (Exception e) {
                    Log.e("CoordinatorLayout", "Default behavior class " + bVar.mo1632a().getName() + " could not be instantiated. Did you forget a default constructor?", e);
                }
            }
            dVar.f2447b = true;
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C0382d generateLayoutParams(LayoutParams layoutParams) {
        return layoutParams instanceof C0382d ? new C0382d((C0382d) layoutParams) : layoutParams instanceof MarginLayoutParams ? new C0382d((MarginLayoutParams) layoutParams) : new C0382d(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1569a() {
        boolean z = false;
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            } else if (mo1590e(getChildAt(i))) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z == this.f2437v) {
            return;
        }
        if (z) {
            mo1578b();
        } else {
            mo1582c();
        }
    }

    /* renamed from: a */
    public void mo1570a(View view, int i) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        if (dVar.mo1645d()) {
            throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
        } else if (dVar.f2452g != null) {
            m3209a(view, dVar.f2452g, i);
        } else if (dVar.f2450e >= 0) {
            m3215b(view, dVar.f2450e, i);
        } else {
            m3217c(view, i);
        }
    }

    /* renamed from: a */
    public void mo1571a(View view, int i, int i2, int i3, int i4) {
        measureChildWithMargins(view, i, i2, i3, i4);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1572a(View view, int i, Rect rect, Rect rect2) {
        int width;
        int height;
        C0382d dVar = (C0382d) view.getLayoutParams();
        int a = GravityCompat.m12720a(m3218d(dVar.f2448c), i);
        int a2 = GravityCompat.m12720a(m3213b(dVar.f2449d), i);
        int i2 = a & 7;
        int i3 = a & 112;
        int i4 = a2 & 7;
        int i5 = a2 & 112;
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        switch (i4) {
            case 1:
                width = rect.left + (rect.width() / 2);
                break;
            case 5:
                width = rect.right;
                break;
            default:
                width = rect.left;
                break;
        }
        switch (i5) {
            case 16:
                height = rect.top + (rect.height() / 2);
                break;
            case 80:
                height = rect.bottom;
                break;
            default:
                height = rect.top;
                break;
        }
        switch (i2) {
            case 1:
                width -= measuredWidth / 2;
                break;
            case 5:
                break;
            default:
                width -= measuredWidth;
                break;
        }
        switch (i3) {
            case 16:
                height -= measuredHeight / 2;
                break;
            case 80:
                break;
            default:
                height -= measuredHeight;
                break;
        }
        int width2 = getWidth();
        int height2 = getHeight();
        int max = Math.max(getPaddingLeft() + dVar.leftMargin, Math.min(width, ((width2 - getPaddingRight()) - measuredWidth) - dVar.rightMargin));
        int max2 = Math.max(getPaddingTop() + dVar.topMargin, Math.min(height, ((height2 - getPaddingBottom()) - measuredHeight) - dVar.bottomMargin));
        rect2.set(max, max2, max + measuredWidth, max2 + measuredHeight);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1573a(View view, Rect rect) {
        ViewGroupUtils.m1250b(this, view, rect);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1574a(View view, boolean z, Rect rect) {
        if (view.isLayoutRequested() || view.getVisibility() == 8) {
            rect.set(0, 0, 0, 0);
        } else if (z) {
            mo1573a(view, rect);
        } else {
            rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1575a(boolean z) {
        int h = ViewCompat.m12923h(this);
        int size = this.f2422g.size();
        for (int i = 0; i < size; i++) {
            View view = (View) this.f2422g.get(i);
            C0382d dVar = (C0382d) view.getLayoutParams();
            for (int i2 = 0; i2 < i; i2++) {
                if (dVar.f2453h == ((View) this.f2422g.get(i2))) {
                    mo1580b(view, h);
                }
            }
            Rect rect = this.f2425j;
            Rect rect2 = this.f2426k;
            mo1584c(view, rect);
            mo1574a(view, true, rect2);
            if (!rect.equals(rect2)) {
                mo1581b(view, rect2);
                for (int i3 = i + 1; i3 < size; i3++) {
                    View view2 = (View) this.f2422g.get(i3);
                    C0382d dVar2 = (C0382d) view2.getLayoutParams();
                    Behavior b = dVar2.mo1641b();
                    if (b != null && b.mo1491b(this, view2, view)) {
                        if (z || !dVar2.mo1650i()) {
                            boolean c = b.mo1492c(this, view2, view);
                            if (z) {
                                dVar2.mo1643b(c);
                            }
                        } else {
                            dVar2.mo1651j();
                        }
                    }
                }
            }
        }
    }

    /* renamed from: a */
    public boolean mo1576a(View view, int i, int i2) {
        Rect rect = this.f2425j;
        mo1573a(view, rect);
        return rect.contains(i, i2);
    }

    /* renamed from: a */
    public boolean mo1577a(View view, View view2) {
        if (view.getVisibility() != 0 || view2.getVisibility() != 0) {
            return false;
        }
        Rect rect = this.f2425j;
        mo1574a(view, view.getParent() != this, rect);
        Rect rect2 = this.f2426k;
        mo1574a(view2, view2.getParent() != this, rect2);
        return rect.left <= rect2.right && rect.top <= rect2.bottom && rect.right >= rect2.left && rect.bottom >= rect2.top;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo1578b() {
        if (this.f2431p) {
            if (this.f2436u == null) {
                this.f2436u = new C0383e();
            }
            getViewTreeObserver().addOnPreDrawListener(this.f2436u);
        }
        this.f2437v = true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo1579b(View view) {
        int size = this.f2422g.size();
        boolean z = false;
        for (int i = 0; i < size; i++) {
            View view2 = (View) this.f2422g.get(i);
            if (view2 == view) {
                z = true;
            } else if (z) {
                C0382d dVar = (C0382d) view2.getLayoutParams();
                Behavior b = dVar.mo1641b();
                if (b != null && dVar.mo1640a(this, view2, view)) {
                    b.mo1627d(this, view2, view);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo1580b(View view, int i) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        if (dVar.f2452g != null) {
            Rect rect = this.f2425j;
            Rect rect2 = this.f2426k;
            Rect rect3 = this.f2427l;
            mo1573a(dVar.f2452g, rect);
            mo1574a(view, false, rect2);
            mo1572a(view, i, rect, rect3);
            int i2 = rect3.left - rect2.left;
            int i3 = rect3.top - rect2.top;
            if (i2 != 0) {
                view.offsetLeftAndRight(i2);
            }
            if (i3 != 0) {
                view.offsetTopAndBottom(i3);
            }
            if (i2 != 0 || i3 != 0) {
                Behavior b = dVar.mo1641b();
                if (b != null) {
                    b.mo1492c(this, view, dVar.f2452g);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo1581b(View view, Rect rect) {
        ((C0382d) view.getLayoutParams()).mo1636a(rect);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo1582c() {
        if (this.f2431p && this.f2436u != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.f2436u);
        }
        this.f2437v = false;
    }

    /* renamed from: c */
    public void mo1583c(View view) {
        int size = this.f2422g.size();
        boolean z = false;
        for (int i = 0; i < size; i++) {
            View view2 = (View) this.f2422g.get(i);
            if (view2 == view) {
                z = true;
            } else if (z) {
                C0382d dVar = (C0382d) view2.getLayoutParams();
                Behavior b = dVar.mo1641b();
                if (b != null && dVar.mo1640a(this, view2, view)) {
                    b.mo1492c(this, view2, view);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo1584c(View view, Rect rect) {
        rect.set(((C0382d) view.getLayoutParams()).mo1644c());
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(LayoutParams layoutParams) {
        return (layoutParams instanceof C0382d) && super.checkLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C0382d generateDefaultLayoutParams() {
        return new C0382d(-2, -2);
    }

    /* renamed from: d */
    public List<View> mo1587d(View view) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        List<View> list = this.f2424i;
        list.clear();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt != view && dVar.mo1640a(this, view, childAt)) {
                list.add(childAt);
            }
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        if (dVar.f2446a != null && dVar.f2446a.mo1626d(this, view) > 0.0f) {
            if (this.f2429n == null) {
                this.f2429n = new Paint();
            }
            this.f2429n.setColor(dVar.f2446a.mo1625c(this, view));
            canvas.drawRect((float) getPaddingLeft(), (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()), this.f2429n);
        }
        return super.drawChild(canvas, view, j);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        boolean z = false;
        Drawable drawable = this.f2440y;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public boolean mo1590e(View view) {
        C0382d dVar = (C0382d) view.getLayoutParams();
        if (dVar.f2452g != null) {
            return true;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt != view && dVar.mo1640a(this, view, childAt)) {
                return true;
            }
        }
        return false;
    }

    public int getNestedScrollAxes() {
        return this.f2420A.mo13430a();
    }

    public Drawable getStatusBarBackground() {
        return this.f2440y;
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        m3219e();
        if (this.f2437v) {
            if (this.f2436u == null) {
                this.f2436u = new C0383e();
            }
            getViewTreeObserver().addOnPreDrawListener(this.f2436u);
        }
        if (this.f2438w == null && ViewCompat.m12939x(this)) {
            ViewCompat.m12938w(this);
        }
        this.f2431p = true;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m3219e();
        if (this.f2437v && this.f2436u != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.f2436u);
        }
        if (this.f2435t != null) {
            onStopNestedScroll(this.f2435t);
        }
        this.f2431p = false;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f2439x && this.f2440y != null) {
            int i = this.f2438w != null ? this.f2438w.mo13567b() : 0;
            if (i > 0) {
                this.f2440y.setBounds(0, 0, getWidth(), i);
                this.f2440y.draw(canvas);
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = null;
        int a = MotionEventCompat.m12830a(motionEvent);
        if (a == 0) {
            m3219e();
        }
        boolean a2 = m3212a(motionEvent, 0);
        if (motionEvent2 != null) {
            motionEvent2.recycle();
        }
        if (a == 1 || a == 3) {
            m3219e();
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int h = ViewCompat.m12923h(this);
        int size = this.f2422g.size();
        for (int i5 = 0; i5 < size; i5++) {
            View view = (View) this.f2422g.get(i5);
            Behavior b = ((C0382d) view.getLayoutParams()).mo1641b();
            if (b == null || !b.mo1471a(this, view, h)) {
                mo1570a(view, h);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        m3220f();
        mo1569a();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int h = ViewCompat.m12923h(this);
        boolean z = h == 1;
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        int i3 = paddingLeft + paddingRight;
        int i4 = paddingTop + paddingBottom;
        int suggestedMinimumWidth = getSuggestedMinimumWidth();
        int suggestedMinimumHeight = getSuggestedMinimumHeight();
        int i5 = 0;
        boolean z2 = this.f2438w != null && ViewCompat.m12939x(this);
        int size3 = this.f2422g.size();
        for (int i6 = 0; i6 < size3; i6++) {
            View view = (View) this.f2422g.get(i6);
            C0382d dVar = (C0382d) view.getLayoutParams();
            int i7 = 0;
            if (dVar.f2450e >= 0 && mode != 0) {
                int a = m3204a(dVar.f2450e);
                int a2 = GravityCompat.m12720a(m3216c(dVar.f2448c), h) & 7;
                if ((a2 == 3 && !z) || (a2 == 5 && z)) {
                    i7 = Math.max(0, (size - paddingRight) - a);
                } else if ((a2 == 5 && !z) || (a2 == 3 && z)) {
                    i7 = Math.max(0, a - paddingLeft);
                }
            }
            int i8 = i;
            int i9 = i2;
            if (z2 && !ViewCompat.m12939x(view)) {
                int b = this.f2438w.mo13567b() + this.f2438w.mo13569d();
                i8 = MeasureSpec.makeMeasureSpec(size - (this.f2438w.mo13565a() + this.f2438w.mo13568c()), mode);
                i9 = MeasureSpec.makeMeasureSpec(size2 - b, mode2);
            }
            Behavior b2 = dVar.mo1641b();
            if (b2 == null || !b2.mo1472a(this, view, i8, i7, i9, 0)) {
                mo1571a(view, i8, i7, i9, 0);
            }
            suggestedMinimumWidth = Math.max(suggestedMinimumWidth, view.getMeasuredWidth() + i3 + dVar.leftMargin + dVar.rightMargin);
            suggestedMinimumHeight = Math.max(suggestedMinimumHeight, view.getMeasuredHeight() + i4 + dVar.topMargin + dVar.bottomMargin);
            i5 = ViewCompat.m12881a(i5, ViewCompat.m12927l(view));
        }
        setMeasuredDimension(ViewCompat.m12882a(suggestedMinimumWidth, i, -16777216 & i5), ViewCompat.m12882a(suggestedMinimumHeight, i2, i5 << 16));
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        boolean z2 = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            C0382d dVar = (C0382d) childAt.getLayoutParams();
            if (dVar.mo1649h()) {
                Behavior b = dVar.mo1641b();
                if (b != null) {
                    z2 |= b.mo1473a(this, childAt, view, f, f2, z);
                }
            }
        }
        if (z2) {
            mo1575a(true);
        }
        return z2;
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        boolean z = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            C0382d dVar = (C0382d) childAt.getLayoutParams();
            if (dVar.mo1649h()) {
                Behavior b = dVar.mo1641b();
                if (b != null) {
                    z |= b.mo1497a(this, childAt, view, f, f2);
                }
            }
        }
        return z;
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        int i3 = 0;
        int i4 = 0;
        boolean z = false;
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            C0382d dVar = (C0382d) childAt.getLayoutParams();
            if (dVar.mo1649h()) {
                Behavior b = dVar.mo1641b();
                if (b != null) {
                    int[] iArr2 = this.f2428m;
                    this.f2428m[1] = 0;
                    iArr2[0] = 0;
                    b.mo1464a(this, childAt, view, i, i2, this.f2428m);
                    i3 = i > 0 ? Math.max(i3, this.f2428m[0]) : Math.min(i3, this.f2428m[0]);
                    i4 = i2 > 0 ? Math.max(i4, this.f2428m[1]) : Math.min(i4, this.f2428m[1]);
                    z = true;
                }
            }
        }
        iArr[0] = i3;
        iArr[1] = i4;
        if (z) {
            mo1575a(true);
        }
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        boolean z = false;
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            C0382d dVar = (C0382d) childAt.getLayoutParams();
            if (dVar.mo1649h()) {
                Behavior b = dVar.mo1641b();
                if (b != null) {
                    b.mo1463a(this, childAt, view, i, i2, i3, i4);
                    z = true;
                }
            }
        }
        if (z) {
            mo1575a(true);
        }
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.f2420A.mo13432a(view, view2, i);
        this.f2434s = view;
        this.f2435t = view2;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            C0382d dVar = (C0382d) childAt.getLayoutParams();
            if (dVar.mo1649h()) {
                Behavior b = dVar.mo1641b();
                if (b != null) {
                    b.mo1624b(this, childAt, view, view2, i);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        SparseArray<Parcelable> sparseArray = savedState.f2443a;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int id = childAt.getId();
            Behavior b = mo1567a(childAt).mo1641b();
            if (!(id == -1 || b == null)) {
                Parcelable parcelable2 = (Parcelable) sparseArray.get(id);
                if (parcelable2 != null) {
                    b.mo1461a(this, childAt, parcelable2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int id = childAt.getId();
            Behavior b = ((C0382d) childAt.getLayoutParams()).mo1641b();
            if (!(id == -1 || b == null)) {
                Parcelable b2 = b.mo1479b(this, childAt);
                if (b2 != null) {
                    sparseArray.append(id, b2);
                }
            }
        }
        savedState.f2443a = sparseArray;
        return savedState;
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        boolean z = false;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            C0382d dVar = (C0382d) childAt.getLayoutParams();
            Behavior b = dVar.mo1641b();
            if (b != null) {
                boolean a = b.mo1474a(this, childAt, view, view2, i);
                z |= a;
                dVar.mo1638a(a);
            } else {
                dVar.mo1638a(false);
            }
        }
        return z;
    }

    public void onStopNestedScroll(View view) {
        this.f2420A.mo13431a(view);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            C0382d dVar = (C0382d) childAt.getLayoutParams();
            if (dVar.mo1649h()) {
                Behavior b = dVar.mo1641b();
                if (b != null) {
                    b.mo1462a(this, childAt, view);
                }
                dVar.mo1648g();
                dVar.mo1651j();
            }
        }
        this.f2434s = null;
        this.f2435t = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        if (r11 != false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r15) {
        /*
            r14 = this;
            r12 = 0
            r11 = 0
            r10 = 0
            int r8 = p000.MotionEventCompat.m12830a(r15)
            android.view.View r2 = r14.f2433r
            if (r2 != 0) goto L_0x0012
            r2 = 1
            boolean r11 = r14.m3212a(r15, r2)
            if (r11 == 0) goto L_0x0026
        L_0x0012:
            android.view.View r2 = r14.f2433r
            android.view.ViewGroup$LayoutParams r13 = r2.getLayoutParams()
            android.support.design.widget.CoordinatorLayout$d r13 = (android.support.design.widget.CoordinatorLayout.C0382d) r13
            android.support.design.widget.CoordinatorLayout$Behavior r9 = r13.mo1641b()
            if (r9 == 0) goto L_0x0026
            android.view.View r2 = r14.f2433r
            boolean r12 = r9.mo1498b(r14, r2, r15)
        L_0x0026:
            android.view.View r2 = r14.f2433r
            if (r2 != 0) goto L_0x0042
            boolean r2 = super.onTouchEvent(r15)
            r12 = r12 | r2
        L_0x002f:
            if (r12 != 0) goto L_0x0033
            if (r8 != 0) goto L_0x0033
        L_0x0033:
            if (r10 == 0) goto L_0x0038
            r10.recycle()
        L_0x0038:
            r2 = 1
            if (r8 == r2) goto L_0x003e
            r2 = 3
            if (r8 != r2) goto L_0x0041
        L_0x003e:
            r14.m3219e()
        L_0x0041:
            return r12
        L_0x0042:
            if (r11 == 0) goto L_0x002f
            if (r10 != 0) goto L_0x0053
            long r0 = android.os.SystemClock.uptimeMillis()
            r4 = 3
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r0
            android.view.MotionEvent r10 = android.view.MotionEvent.obtain(r0, r2, r4, r5, r6, r7)
        L_0x0053:
            super.onTouchEvent(r10)
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z && !this.f2430o) {
            m3219e();
            this.f2430o = true;
        }
    }

    public void setOnHierarchyChangeListener(OnHierarchyChangeListener onHierarchyChangeListener) {
        this.f2441z = onHierarchyChangeListener;
    }

    public void setStatusBarBackground(Drawable drawable) {
        Drawable drawable2 = null;
        if (this.f2440y != drawable) {
            if (this.f2440y != null) {
                this.f2440y.setCallback(null);
            }
            if (drawable != null) {
                drawable2 = drawable.mutate();
            }
            this.f2440y = drawable2;
            if (this.f2440y != null) {
                if (this.f2440y.isStateful()) {
                    this.f2440y.setState(getDrawableState());
                }
                DrawableCompat.setLayoutDirection(this.f2440y, ViewCompat.m12923h(this));
                this.f2440y.setVisible(getVisibility() == 0, false);
                this.f2440y.setCallback(this);
            }
            ViewCompat.m12913d(this);
        }
    }

    public void setStatusBarBackgroundColor(int i) {
        setStatusBarBackground(new ColorDrawable(i));
    }

    public void setStatusBarBackgroundResource(int i) {
        setStatusBarBackground(i != 0 ? ContextCompat.getDrawable(getContext(), i) : null);
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean z = i == 0;
        if (this.f2440y != null && this.f2440y.isVisible() != z) {
            this.f2440y.setVisible(z, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f2440y;
    }
}
