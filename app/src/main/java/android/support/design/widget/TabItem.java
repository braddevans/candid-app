package android.support.design.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.p003v7.widget.TintTypedArray;
import android.util.AttributeSet;
import android.view.View;

public final class TabItem extends View {

    /* renamed from: a */
    final CharSequence f2544a;

    /* renamed from: b */
    final Drawable f2545b;

    /* renamed from: c */
    final int f2546c;

    public TabItem(Context context) {
        this(context, null);
    }

    public TabItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, attributeSet, C2394i.TabItem);
        this.f2544a = obtainStyledAttributes.getText(C2394i.TabItem_android_text);
        this.f2545b = obtainStyledAttributes.getDrawable(C2394i.TabItem_android_icon);
        this.f2546c = obtainStyledAttributes.getResourceId(C2394i.TabItem_android_layout, 0);
        obtainStyledAttributes.recycle();
    }
}
