package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.design.widget.CoordinatorLayout.C0380b;
import android.support.design.widget.CoordinatorLayout.C0382d;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@C0380b(mo1632a = Behavior.class)
public class AppBarLayout extends LinearLayout {

    /* renamed from: a */
    boolean f2339a;

    /* renamed from: b */
    private int f2340b;

    /* renamed from: c */
    private int f2341c;

    /* renamed from: d */
    private int f2342d;

    /* renamed from: e */
    private float f2343e;

    /* renamed from: f */
    private int f2344f;

    /* renamed from: g */
    private WindowInsetsCompat f2345g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final List<C0369a> f2346h;

    public static class Behavior extends HeaderBehavior<AppBarLayout> {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public int f2348a;

        /* renamed from: b */
        private boolean f2349b;

        /* renamed from: c */
        private boolean f2350c;

        /* renamed from: d */
        private ValueAnimatorCompat f2351d;

        /* renamed from: e */
        private int f2352e = -1;

        /* renamed from: f */
        private boolean f2353f;

        /* renamed from: g */
        private float f2354g;

        /* renamed from: h */
        private WeakReference<View> f2355h;

        /* renamed from: i */
        private C0368a f2356i;

        public static class SavedState extends BaseSavedState {
            public static final Creator<SavedState> CREATOR = ParcelableCompat.m12486a(new ParcelableCompatCreatorCallbacks<SavedState>() {
                /* renamed from: a */
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return new SavedState(parcel, classLoader);
                }

                /* renamed from: a */
                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            });

            /* renamed from: a */
            int f2360a;

            /* renamed from: b */
            float f2361b;

            /* renamed from: c */
            boolean f2362c;

            public SavedState(Parcel parcel, ClassLoader classLoader) {
                super(parcel);
                this.f2360a = parcel.readInt();
                this.f2361b = parcel.readFloat();
                this.f2362c = parcel.readByte() != 0;
            }

            public SavedState(Parcelable parcelable) {
                super(parcelable);
            }

            public void writeToParcel(Parcel parcel, int i) {
                super.writeToParcel(parcel, i);
                parcel.writeInt(this.f2360a);
                parcel.writeFloat(this.f2361b);
                parcel.writeByte((byte) (this.f2362c ? 1 : 0));
            }
        }

        /* renamed from: android.support.design.widget.AppBarLayout$Behavior$a */
        public static abstract class C0368a {
            /* renamed from: a */
            public abstract boolean mo1485a(AppBarLayout appBarLayout);
        }

        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        /* renamed from: a */
        private int m3095a(AppBarLayout appBarLayout, int i) {
            int childCount = appBarLayout.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = appBarLayout.getChildAt(i2);
                if (childAt.getTop() <= (-i) && childAt.getBottom() >= (-i)) {
                    return i2;
                }
            }
            return -1;
        }

        /* renamed from: a */
        private static boolean m3096a(int i, int i2) {
            return (i & i2) == i2;
        }

        /* renamed from: b */
        private int m3097b(AppBarLayout appBarLayout, int i) {
            int abs = Math.abs(i);
            int i2 = 0;
            int childCount = appBarLayout.getChildCount();
            while (i2 < childCount) {
                View childAt = appBarLayout.getChildAt(i2);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                Interpolator b = layoutParams.mo1487b();
                if (abs < childAt.getTop() || abs > childAt.getBottom()) {
                    i2++;
                } else if (b == null) {
                    return i;
                } else {
                    int i3 = 0;
                    int a = layoutParams.mo1486a();
                    if ((a & 1) != 0) {
                        i3 = 0 + childAt.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
                        if ((a & 2) != 0) {
                            i3 -= ViewCompat.m12933r(childAt);
                        }
                    }
                    if (ViewCompat.m12939x(childAt)) {
                        i3 -= appBarLayout.getTopInset();
                    }
                    if (i3 <= 0) {
                        return i;
                    }
                    return Integer.signum(i) * (childAt.getTop() + Math.round(((float) i3) * b.getInterpolation(((float) (abs - childAt.getTop())) / ((float) i3))));
                }
            }
            return i;
        }

        /* renamed from: b */
        private void m3098b(final CoordinatorLayout coordinatorLayout, final AppBarLayout appBarLayout, int i) {
            int a = mo1451a();
            if (a != i) {
                if (this.f2351d == null) {
                    this.f2351d = ViewUtils.m1925a();
                    this.f2351d.mo141a(AnimationUtils.f10967e);
                    this.f2351d.mo140a((C0046c) new C0046c() {
                        /* renamed from: a */
                        public void mo155a(ValueAnimatorCompat abVar) {
                            Behavior.this.mo1689a_(coordinatorLayout, appBarLayout, abVar.mo143c());
                        }
                    });
                } else {
                    this.f2351d.mo145e();
                }
                this.f2351d.mo137a(Math.round((1000.0f * (((float) Math.abs(a - i)) / coordinatorLayout.getResources().getDisplayMetrics().density)) / 300.0f));
                this.f2351d.mo138a(a, i);
                this.f2351d.mo135a();
            } else if (this.f2351d != null && this.f2351d.mo142b()) {
                this.f2351d.mo145e();
            }
        }

        /* renamed from: c */
        private void m3099c(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            int a = mo1451a();
            int a2 = m3095a(appBarLayout, a);
            if (a2 >= 0) {
                View childAt = appBarLayout.getChildAt(a2);
                int a3 = ((LayoutParams) childAt.getLayoutParams()).mo1486a();
                if ((a3 & 17) == 17) {
                    int i = -childAt.getTop();
                    int i2 = -childAt.getBottom();
                    if (a2 == appBarLayout.getChildCount() - 1) {
                        i2 += appBarLayout.getTopInset();
                    }
                    if (m3096a(a3, 2)) {
                        i2 += ViewCompat.m12933r(childAt);
                    } else if (m3096a(a3, 5)) {
                        int r = i2 + ViewCompat.m12933r(childAt);
                        if (a < r) {
                            i = r;
                        } else {
                            i2 = r;
                        }
                    }
                    m3098b(coordinatorLayout, appBarLayout, MathUtils.m16569a(a < (i2 + i) / 2 ? i2 : i, -appBarLayout.getTotalScrollRange(), 0));
                }
            }
        }

        /* renamed from: d */
        private void m3100d(AppBarLayout appBarLayout) {
            List i = appBarLayout.f2346h;
            int size = i.size();
            for (int i2 = 0; i2 < size; i2++) {
                C0369a aVar = (C0369a) i.get(i2);
                if (aVar != null) {
                    aVar.mo1493a(appBarLayout, mo1475b());
                }
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public int mo1451a() {
            return mo1475b() + this.f2348a;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public int mo1453a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3) {
            int a = mo1451a();
            int i4 = 0;
            if (i2 == 0 || a < i2 || a > i3) {
                this.f2348a = 0;
            } else {
                int a2 = MathUtils.m16569a(i, i2, i3);
                AppBarLayout appBarLayout2 = appBarLayout;
                if (a != a2) {
                    int i5 = appBarLayout2.m3080c() ? m3097b(appBarLayout2, a2) : a2;
                    boolean a3 = mo1465a(i5);
                    i4 = a - a2;
                    this.f2348a = a2 - i5;
                    if (!a3 && appBarLayout2.m3080c()) {
                        coordinatorLayout.mo1583c((View) appBarLayout2);
                    }
                    m3100d(appBarLayout2);
                }
            }
            return i4;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo1460a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            m3099c(coordinatorLayout, appBarLayout);
        }

        /* renamed from: a */
        public void mo1461a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, Parcelable parcelable) {
            if (parcelable instanceof SavedState) {
                SavedState savedState = (SavedState) parcelable;
                super.mo1461a(coordinatorLayout, appBarLayout, savedState.getSuperState());
                this.f2352e = savedState.f2360a;
                this.f2354g = savedState.f2361b;
                this.f2353f = savedState.f2362c;
                return;
            }
            super.mo1461a(coordinatorLayout, appBarLayout, parcelable);
            this.f2352e = -1;
        }

        /* renamed from: a */
        public void mo1462a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view) {
            if (!this.f2350c) {
                m3099c(coordinatorLayout, appBarLayout);
            }
            this.f2349b = false;
            this.f2350c = false;
            this.f2355h = new WeakReference<>(view);
        }

        /* renamed from: a */
        public void mo1463a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int i3, int i4) {
            if (i4 < 0) {
                mo1690b(coordinatorLayout, appBarLayout, i4, -appBarLayout.getDownNestedScrollRange(), 0);
                this.f2349b = true;
                return;
            }
            this.f2349b = false;
        }

        /* renamed from: a */
        public void mo1464a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int[] iArr) {
            int i3;
            int i4;
            if (i2 != 0 && !this.f2349b) {
                if (i2 < 0) {
                    i3 = -appBarLayout.getTotalScrollRange();
                    i4 = i3 + appBarLayout.getDownNestedPreScrollRange();
                } else {
                    i3 = -appBarLayout.getUpNestedPreScrollRange();
                    i4 = 0;
                }
                iArr[1] = mo1690b(coordinatorLayout, appBarLayout, i2, i3, i4);
            }
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ boolean mo1465a(int i) {
            return super.mo1465a(i);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo1481c(AppBarLayout appBarLayout) {
            if (this.f2356i != null) {
                return this.f2356i.mo1485a(appBarLayout);
            }
            if (this.f2355h == null) {
                return true;
            }
            View view = (View) this.f2355h.get();
            return view != null && view.isShown() && !ViewCompat.m12908b(view, -1);
        }

        /* renamed from: a */
        public boolean mo1471a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            boolean a = super.mo1471a(coordinatorLayout, appBarLayout, i);
            int f = appBarLayout.getPendingAction();
            if (f != 0) {
                boolean z = (f & 4) != 0;
                if ((f & 2) != 0) {
                    int i2 = -appBarLayout.getUpNestedPreScrollRange();
                    if (z) {
                        m3098b(coordinatorLayout, appBarLayout, i2);
                    } else {
                        mo1689a_(coordinatorLayout, appBarLayout, i2);
                    }
                } else if ((f & 1) != 0) {
                    if (z) {
                        m3098b(coordinatorLayout, appBarLayout, 0);
                    } else {
                        mo1689a_(coordinatorLayout, appBarLayout, 0);
                    }
                }
            } else if (this.f2352e >= 0) {
                View childAt = appBarLayout.getChildAt(this.f2352e);
                int i3 = -childAt.getBottom();
                mo1465a(this.f2353f ? i3 + ViewCompat.m12933r(childAt) : i3 + Math.round(((float) childAt.getHeight()) * this.f2354g));
            }
            appBarLayout.m3084e();
            this.f2352e = -1;
            mo1465a(MathUtils.m16569a(mo1475b(), -appBarLayout.getTotalScrollRange(), 0));
            m3100d(appBarLayout);
            return a;
        }

        /* renamed from: a */
        public boolean mo1472a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3, int i4) {
            if (((C0382d) appBarLayout.getLayoutParams()).height != -2) {
                return super.mo1472a(coordinatorLayout, appBarLayout, i, i2, i3, i4);
            }
            coordinatorLayout.mo1571a(appBarLayout, i, i2, MeasureSpec.makeMeasureSpec(0, 0), i4);
            return true;
        }

        /* renamed from: a */
        public boolean mo1473a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, float f, float f2, boolean z) {
            boolean z2 = false;
            if (!z) {
                z2 = mo1688a(coordinatorLayout, appBarLayout, -appBarLayout.getTotalScrollRange(), 0, -f2);
            } else if (f2 < 0.0f) {
                int b = (-appBarLayout.getTotalScrollRange()) + appBarLayout.getDownNestedPreScrollRange();
                if (mo1451a() < b) {
                    m3098b(coordinatorLayout, appBarLayout, b);
                    z2 = true;
                }
            } else {
                int i = -appBarLayout.getUpNestedPreScrollRange();
                if (mo1451a() > i) {
                    m3098b(coordinatorLayout, appBarLayout, i);
                    z2 = true;
                }
            }
            this.f2350c = z2;
            return z2;
        }

        /* renamed from: a */
        public boolean mo1474a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, View view2, int i) {
            boolean z = (i & 2) != 0 && appBarLayout.m3082d() && coordinatorLayout.getHeight() - view.getHeight() <= appBarLayout.getHeight();
            if (z && this.f2351d != null) {
                this.f2351d.mo145e();
            }
            this.f2355h = null;
            return z;
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ int mo1475b() {
            return super.mo1475b();
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public int mo1477b(AppBarLayout appBarLayout) {
            return -appBarLayout.getDownNestedScrollRange();
        }

        /* renamed from: b */
        public Parcelable mo1479b(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            Parcelable b = super.mo1479b(coordinatorLayout, appBarLayout);
            int b2 = mo1475b();
            int i = 0;
            int childCount = appBarLayout.getChildCount();
            while (i < childCount) {
                View childAt = appBarLayout.getChildAt(i);
                int bottom = childAt.getBottom() + b2;
                if (childAt.getTop() + b2 > 0 || bottom < 0) {
                    i++;
                } else {
                    SavedState savedState = new SavedState(b);
                    savedState.f2360a = i;
                    savedState.f2362c = bottom == ViewCompat.m12933r(childAt);
                    savedState.f2361b = ((float) bottom) / ((float) childAt.getHeight());
                    return savedState;
                }
            }
            return b;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public int mo1454a(AppBarLayout appBarLayout) {
            return appBarLayout.getTotalScrollRange();
        }
    }

    public static class LayoutParams extends android.widget.LinearLayout.LayoutParams {

        /* renamed from: a */
        int f2363a = 1;

        /* renamed from: b */
        Interpolator f2364b;

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.AppBarLayout_LayoutParams);
            this.f2363a = obtainStyledAttributes.getInt(C2394i.AppBarLayout_LayoutParams_layout_scrollFlags, 0);
            if (obtainStyledAttributes.hasValue(C2394i.AppBarLayout_LayoutParams_layout_scrollInterpolator)) {
                this.f2364b = AnimationUtils.loadInterpolator(context, obtainStyledAttributes.getResourceId(C2394i.AppBarLayout_LayoutParams_layout_scrollInterpolator, 0));
            }
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(android.widget.LinearLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* renamed from: a */
        public int mo1486a() {
            return this.f2363a;
        }

        /* renamed from: b */
        public Interpolator mo1487b() {
            return this.f2364b;
        }
    }

    public static class ScrollingViewBehavior extends HeaderScrollingViewBehavior {
        public ScrollingViewBehavior() {
        }

        public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.ScrollingViewBehavior_Params);
            mo1693b(obtainStyledAttributes.getDimensionPixelSize(C2394i.ScrollingViewBehavior_Params_behavior_overlapTop, 0));
            obtainStyledAttributes.recycle();
        }

        /* renamed from: a */
        private static int m3138a(AppBarLayout appBarLayout) {
            android.support.design.widget.CoordinatorLayout.Behavior b = ((C0382d) appBarLayout.getLayoutParams()).mo1641b();
            if (b instanceof Behavior) {
                return ((Behavior) b).mo1451a();
            }
            return 0;
        }

        /* renamed from: e */
        private void m3139e(CoordinatorLayout coordinatorLayout, View view, View view2) {
            android.support.design.widget.CoordinatorLayout.Behavior b = ((C0382d) view2.getLayoutParams()).mo1641b();
            if (b instanceof Behavior) {
                Behavior behavior = (Behavior) b;
                int a = behavior.mo1451a();
                ViewCompat.m12918e(view, (((view2.getBottom() - view.getTop()) + behavior.f2348a) + mo1692a()) - mo1695c(view2));
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public float mo1488a(View view) {
            if (!(view instanceof AppBarLayout)) {
                return 0.0f;
            }
            AppBarLayout appBarLayout = (AppBarLayout) view;
            int totalScrollRange = appBarLayout.getTotalScrollRange();
            int b = appBarLayout.getDownNestedPreScrollRange();
            int a = m3138a(appBarLayout);
            if (b != 0 && totalScrollRange + a <= b) {
                return 0.0f;
            }
            int i = totalScrollRange - b;
            if (i != 0) {
                return 1.0f + (((float) a) / ((float) i));
            }
            return 0.0f;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public View mo1489a(List<View> list) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                View view = (View) list.get(i);
                if (view instanceof AppBarLayout) {
                    return view;
                }
            }
            return null;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ boolean mo1465a(int i) {
            return super.mo1465a(i);
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ boolean mo1471a(CoordinatorLayout coordinatorLayout, View view, int i) {
            return super.mo1471a(coordinatorLayout, view, i);
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ boolean mo1472a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
            return super.mo1472a(coordinatorLayout, view, i, i2, i3, i4);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ int mo1475b() {
            return super.mo1475b();
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public int mo1490b(View view) {
            return view instanceof AppBarLayout ? ((AppBarLayout) view).getTotalScrollRange() : super.mo1490b(view);
        }

        /* renamed from: b */
        public boolean mo1491b(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 instanceof AppBarLayout;
        }

        /* renamed from: c */
        public boolean mo1492c(CoordinatorLayout coordinatorLayout, View view, View view2) {
            m3139e(coordinatorLayout, view, view2);
            return false;
        }
    }

    /* renamed from: android.support.design.widget.AppBarLayout$a */
    public interface C0369a {
        /* renamed from: a */
        void mo1493a(AppBarLayout appBarLayout, int i);
    }

    public AppBarLayout(Context context) {
        this(context, null);
    }

    public AppBarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2340b = -1;
        this.f2341c = -1;
        this.f2342d = -1;
        this.f2344f = 0;
        setOrientation(1);
        ThemeUtils.m0a(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.AppBarLayout, 0, C2393h.Widget_Design_AppBarLayout);
        this.f2343e = (float) obtainStyledAttributes.getDimensionPixelSize(C2394i.AppBarLayout_elevation, 0);
        setBackgroundDrawable(obtainStyledAttributes.getDrawable(C2394i.AppBarLayout_android_background));
        if (obtainStyledAttributes.hasValue(C2394i.AppBarLayout_expanded)) {
            setExpanded(obtainStyledAttributes.getBoolean(C2394i.AppBarLayout_expanded, false));
        }
        obtainStyledAttributes.recycle();
        ViewUtils.m1926a(this);
        this.f2346h = new ArrayList();
        ViewCompat.m12920f((View) this, this.f2343e);
        ViewCompat.m12895a((View) this, (OnApplyWindowInsetsListener) new OnApplyWindowInsetsListener() {
            public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat fzVar) {
                return AppBarLayout.this.m3075a(fzVar);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public WindowInsetsCompat m3075a(WindowInsetsCompat fzVar) {
        WindowInsetsCompat fzVar2 = null;
        if (ViewCompat.m12939x(this)) {
            fzVar2 = fzVar;
        }
        if (fzVar2 != this.f2345g) {
            this.f2345g = fzVar2;
            m3078b();
        }
        return fzVar;
    }

    /* renamed from: b */
    private void m3078b() {
        this.f2340b = -1;
        this.f2341c = -1;
        this.f2342d = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public boolean m3080c() {
        return this.f2339a;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public boolean m3082d() {
        return getTotalScrollRange() != 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m3084e() {
        this.f2344f = 0;
    }

    /* access modifiers changed from: private */
    public int getDownNestedPreScrollRange() {
        if (this.f2341c != -1) {
            return this.f2341c;
        }
        int i = 0;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i2 = layoutParams.f2363a;
            if ((i2 & 5) == 5) {
                int i3 = i + layoutParams.topMargin + layoutParams.bottomMargin;
                i = (i2 & 8) != 0 ? i3 + ViewCompat.m12933r(childAt) : (i2 & 2) != 0 ? i3 + (measuredHeight - ViewCompat.m12933r(childAt)) : i3 + measuredHeight;
            } else if (i > 0) {
                break;
            }
        }
        int max = Math.max(0, i - getTopInset());
        this.f2341c = max;
        return max;
    }

    /* access modifiers changed from: private */
    public int getDownNestedScrollRange() {
        if (this.f2342d != -1) {
            return this.f2342d;
        }
        int i = 0;
        int i2 = 0;
        int childCount = getChildCount();
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
            int i3 = layoutParams.f2363a;
            if ((i3 & 1) == 0) {
                break;
            }
            i += measuredHeight;
            if ((i3 & 2) != 0) {
                i -= ViewCompat.m12933r(childAt) + getTopInset();
                break;
            }
            i2++;
        }
        int max = Math.max(0, i);
        this.f2342d = max;
        return max;
    }

    /* access modifiers changed from: private */
    public int getPendingAction() {
        return this.f2344f;
    }

    /* access modifiers changed from: private */
    public int getTopInset() {
        if (this.f2345g != null) {
            return this.f2345g.mo13567b();
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public int getUpNestedPreScrollRange() {
        return getTotalScrollRange();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -2);
    }

    /* renamed from: a */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof android.widget.LinearLayout.LayoutParams ? new LayoutParams((android.widget.LinearLayout.LayoutParams) layoutParams) : layoutParams instanceof MarginLayoutParams ? new LayoutParams((MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* renamed from: a */
    public void mo1436a(C0369a aVar) {
        if (aVar != null && !this.f2346h.contains(aVar)) {
            this.f2346h.add(aVar);
        }
    }

    /* renamed from: b */
    public void mo1437b(C0369a aVar) {
        if (aVar != null) {
            this.f2346h.remove(aVar);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: 0000 */
    public final int getMinimumHeightForVisibleOverlappingContent() {
        int topInset = getTopInset();
        int r = ViewCompat.m12933r(this);
        if (r != 0) {
            return (r * 2) + topInset;
        }
        int childCount = getChildCount();
        if (childCount >= 1) {
            return (ViewCompat.m12933r(getChildAt(childCount - 1)) * 2) + topInset;
        }
        return 0;
    }

    public float getTargetElevation() {
        return this.f2343e;
    }

    public final int getTotalScrollRange() {
        if (this.f2340b != -1) {
            return this.f2340b;
        }
        int i = 0;
        int i2 = 0;
        int childCount = getChildCount();
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i3 = layoutParams.f2363a;
            if ((i3 & 1) == 0) {
                break;
            }
            i += layoutParams.topMargin + measuredHeight + layoutParams.bottomMargin;
            if ((i3 & 2) != 0) {
                i -= ViewCompat.m12933r(childAt);
                break;
            }
            i2++;
        }
        int max = Math.max(0, i - getTopInset());
        this.f2340b = max;
        return max;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        m3078b();
        this.f2339a = false;
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            if (((LayoutParams) getChildAt(i5).getLayoutParams()).mo1487b() != null) {
                this.f2339a = true;
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        m3078b();
    }

    public void setExpanded(boolean z) {
        setExpanded(z, ViewCompat.m12877F(this));
    }

    public void setExpanded(boolean z, boolean z2) {
        this.f2344f = (z2 ? 4 : 0) | (z ? 1 : 2);
        requestLayout();
    }

    public void setOrientation(int i) {
        if (i != 1) {
            throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
        }
        super.setOrientation(i);
    }

    public void setTargetElevation(float f) {
        this.f2343e = f;
    }
}
