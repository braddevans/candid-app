package android.support.design.widget;

import android.support.design.widget.CoordinatorLayout.Behavior;
import android.support.p001v4.widget.ViewDragHelper;
import android.support.p001v4.widget.ViewDragHelper.Callback;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class SwipeDismissBehavior<V extends View> extends Behavior<V> {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public ViewDragHelper f2528a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C0408a f2529b;

    /* renamed from: c */
    private boolean f2530c;

    /* renamed from: d */
    private float f2531d = 0.0f;

    /* renamed from: e */
    private boolean f2532e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public int f2533f = 2;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public float f2534g = 0.5f;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public float f2535h = 0.0f;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public float f2536i = 0.5f;

    /* renamed from: j */
    private final Callback f2537j = new Callback() {

        /* renamed from: b */
        private int f2539b;

        /* renamed from: c */
        private int f2540c = -1;

        /* renamed from: a */
        private boolean m3410a(View view, float f) {
            if (f != 0.0f) {
                boolean z = ViewCompat.m12923h(view) == 1;
                if (SwipeDismissBehavior.this.f2533f == 2) {
                    return true;
                }
                if (SwipeDismissBehavior.this.f2533f == 0) {
                    return z ? f < 0.0f : f > 0.0f;
                }
                if (SwipeDismissBehavior.this.f2533f == 1) {
                    return z ? f > 0.0f : f < 0.0f;
                }
                return false;
            }
            return Math.abs(view.getLeft() - this.f2539b) >= Math.round(((float) view.getWidth()) * SwipeDismissBehavior.this.f2534g);
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            int width;
            int width2;
            boolean z = ViewCompat.m12923h(view) == 1;
            if (SwipeDismissBehavior.this.f2533f == 0) {
                if (z) {
                    width = this.f2539b - view.getWidth();
                    width2 = this.f2539b;
                } else {
                    width = this.f2539b;
                    width2 = this.f2539b + view.getWidth();
                }
            } else if (SwipeDismissBehavior.this.f2533f != 1) {
                width = this.f2539b - view.getWidth();
                width2 = this.f2539b + view.getWidth();
            } else if (z) {
                width = this.f2539b;
                width2 = this.f2539b + view.getWidth();
            } else {
                width = this.f2539b - view.getWidth();
                width2 = this.f2539b;
            }
            return SwipeDismissBehavior.m3396b(width, i, width2);
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return view.getTop();
        }

        public int getViewHorizontalDragRange(View view) {
            return view.getWidth();
        }

        public void onViewCaptured(View view, int i) {
            this.f2540c = i;
            this.f2539b = view.getLeft();
            ViewParent parent = view.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }

        public void onViewDragStateChanged(int i) {
            if (SwipeDismissBehavior.this.f2529b != null) {
                SwipeDismissBehavior.this.f2529b.mo1731a(i);
            }
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            float width = ((float) this.f2539b) + (((float) view.getWidth()) * SwipeDismissBehavior.this.f2535h);
            float width2 = ((float) this.f2539b) + (((float) view.getWidth()) * SwipeDismissBehavior.this.f2536i);
            if (((float) i) <= width) {
                ViewCompat.m12909c(view, 1.0f);
            } else if (((float) i) >= width2) {
                ViewCompat.m12909c(view, 0.0f);
            } else {
                ViewCompat.m12909c(view, SwipeDismissBehavior.m3398c(0.0f, 1.0f - SwipeDismissBehavior.m3391a(width, width2, (float) i), 1.0f));
            }
        }

        public void onViewReleased(View view, float f, float f2) {
            int i;
            this.f2540c = -1;
            int width = view.getWidth();
            boolean z = false;
            if (m3410a(view, f)) {
                i = view.getLeft() < this.f2539b ? this.f2539b - width : this.f2539b + width;
                z = true;
            } else {
                i = this.f2539b;
            }
            if (SwipeDismissBehavior.this.f2528a.settleCapturedViewAt(i, view.getTop())) {
                ViewCompat.m12897a(view, (Runnable) new C0409b(view, z));
            } else if (z && SwipeDismissBehavior.this.f2529b != null) {
                SwipeDismissBehavior.this.f2529b.mo1732a(view);
            }
        }

        public boolean tryCaptureView(View view, int i) {
            return this.f2540c == -1 && SwipeDismissBehavior.this.mo1754a(view);
        }
    };

    /* renamed from: android.support.design.widget.SwipeDismissBehavior$a */
    public interface C0408a {
        /* renamed from: a */
        void mo1731a(int i);

        /* renamed from: a */
        void mo1732a(View view);
    }

    /* renamed from: android.support.design.widget.SwipeDismissBehavior$b */
    class C0409b implements Runnable {

        /* renamed from: b */
        private final View f2542b;

        /* renamed from: c */
        private final boolean f2543c;

        C0409b(View view, boolean z) {
            this.f2542b = view;
            this.f2543c = z;
        }

        public void run() {
            if (SwipeDismissBehavior.this.f2528a != null && SwipeDismissBehavior.this.f2528a.continueSettling(true)) {
                ViewCompat.m12897a(this.f2542b, (Runnable) this);
            } else if (this.f2543c && SwipeDismissBehavior.this.f2529b != null) {
                SwipeDismissBehavior.this.f2529b.mo1732a(this.f2542b);
            }
        }
    }

    /* renamed from: a */
    static float m3391a(float f, float f2, float f3) {
        return (f3 - f) / (f2 - f);
    }

    /* renamed from: a */
    private void m3394a(ViewGroup viewGroup) {
        if (this.f2528a == null) {
            this.f2528a = this.f2532e ? ViewDragHelper.create(viewGroup, this.f2531d, this.f2537j) : ViewDragHelper.create(viewGroup, this.f2537j);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static int m3396b(int i, int i2, int i3) {
        return Math.min(Math.max(i, i2), i3);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static float m3398c(float f, float f2, float f3) {
        return Math.min(Math.max(f, f2), f3);
    }

    /* renamed from: a */
    public void mo1757a(float f) {
        this.f2535h = m3398c(0.0f, f, 1.0f);
    }

    /* renamed from: a */
    public void mo1758a(int i) {
        this.f2533f = i;
    }

    /* renamed from: a */
    public void mo1759a(C0408a aVar) {
        this.f2529b = aVar;
    }

    /* renamed from: a */
    public boolean mo1496a(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        switch (MotionEventCompat.m12830a(motionEvent)) {
            case 1:
            case 3:
                if (this.f2530c) {
                    this.f2530c = false;
                    return false;
                }
                break;
            default:
                this.f2530c = !coordinatorLayout.mo1576a((View) v, (int) motionEvent.getX(), (int) motionEvent.getY());
                break;
        }
        if (this.f2530c) {
            return false;
        }
        m3394a((ViewGroup) coordinatorLayout);
        return this.f2528a.shouldInterceptTouchEvent(motionEvent);
    }

    /* renamed from: a */
    public boolean mo1754a(View view) {
        return true;
    }

    /* renamed from: b */
    public void mo1760b(float f) {
        this.f2536i = m3398c(0.0f, f, 1.0f);
    }

    /* renamed from: b */
    public boolean mo1498b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (this.f2528a == null) {
            return false;
        }
        this.f2528a.processTouchEvent(motionEvent);
        return true;
    }
}
