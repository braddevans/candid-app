package android.support.design.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout.C0382d;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import java.util.List;

abstract class HeaderScrollingViewBehavior extends ViewOffsetBehavior<View> {

    /* renamed from: a */
    private final Rect f2489a = new Rect();

    /* renamed from: b */
    private final Rect f2490b = new Rect();

    /* renamed from: c */
    private int f2491c = 0;

    /* renamed from: d */
    private int f2492d;

    public HeaderScrollingViewBehavior() {
    }

    public HeaderScrollingViewBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* renamed from: c */
    private static int m3333c(int i) {
        if (i == 0) {
            return 8388659;
        }
        return i;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public float mo1488a(View view) {
        return 1.0f;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public final int mo1692a() {
        return this.f2491c;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public abstract View mo1489a(List<View> list);

    /* renamed from: a */
    public boolean mo1472a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
        int i5 = view.getLayoutParams().height;
        if (i5 == -1 || i5 == -2) {
            View a = mo1489a(coordinatorLayout.mo1587d(view));
            if (a != null) {
                if (ViewCompat.m12939x(a) && !ViewCompat.m12939x(view)) {
                    ViewCompat.m12899a(view, true);
                    if (ViewCompat.m12939x(view)) {
                        view.requestLayout();
                        return true;
                    }
                }
                if (ViewCompat.m12877F(a)) {
                    int size = MeasureSpec.getSize(i3);
                    if (size == 0) {
                        size = coordinatorLayout.getHeight();
                    }
                    coordinatorLayout.mo1571a(view, i, i2, MeasureSpec.makeMeasureSpec((size - a.getMeasuredHeight()) + mo1490b(a), i5 == -1 ? 1073741824 : Integer.MIN_VALUE), i4);
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public int mo1490b(View view) {
        return view.getMeasuredHeight();
    }

    /* renamed from: b */
    public final void mo1693b(int i) {
        this.f2492d = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1694b(CoordinatorLayout coordinatorLayout, View view, int i) {
        View a = mo1489a(coordinatorLayout.mo1587d(view));
        if (a != null) {
            C0382d dVar = (C0382d) view.getLayoutParams();
            Rect rect = this.f2489a;
            rect.set(coordinatorLayout.getPaddingLeft() + dVar.leftMargin, a.getBottom() + dVar.topMargin, (coordinatorLayout.getWidth() - coordinatorLayout.getPaddingRight()) - dVar.rightMargin, ((coordinatorLayout.getHeight() + a.getBottom()) - coordinatorLayout.getPaddingBottom()) - dVar.bottomMargin);
            Rect rect2 = this.f2490b;
            GravityCompat.m12721a(m3333c(dVar.f2448c), view.getMeasuredWidth(), view.getMeasuredHeight(), rect, rect2, i);
            int c = mo1695c(a);
            view.layout(rect2.left, rect2.top - c, rect2.right, rect2.bottom - c);
            this.f2491c = rect2.top - a.getBottom();
            return;
        }
        super.mo1694b(coordinatorLayout, view, i);
        this.f2491c = 0;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public final int mo1695c(View view) {
        if (this.f2492d == 0) {
            return 0;
        }
        return MathUtils.m16569a(Math.round(mo1488a(view) * ((float) this.f2492d)), 0, this.f2492d);
    }
}
