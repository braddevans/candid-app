package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.AppBarLayout.C0369a;
import android.support.p003v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.widget.FrameLayout;

public class CollapsingToolbarLayout extends FrameLayout {

    /* renamed from: a */
    private boolean f2389a;

    /* renamed from: b */
    private int f2390b;

    /* renamed from: c */
    private Toolbar f2391c;

    /* renamed from: d */
    private View f2392d;

    /* renamed from: e */
    private View f2393e;

    /* renamed from: f */
    private int f2394f;

    /* renamed from: g */
    private int f2395g;

    /* renamed from: h */
    private int f2396h;

    /* renamed from: i */
    private int f2397i;

    /* renamed from: j */
    private final Rect f2398j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public final CollapsingTextHelper f2399k;

    /* renamed from: l */
    private boolean f2400l;

    /* renamed from: m */
    private boolean f2401m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public Drawable f2402n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public Drawable f2403o;

    /* renamed from: p */
    private int f2404p;

    /* renamed from: q */
    private boolean f2405q;

    /* renamed from: r */
    private ValueAnimatorCompat f2406r;

    /* renamed from: s */
    private C0369a f2407s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public int f2408t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public WindowInsetsCompat f2409u;

    public static class LayoutParams extends android.widget.FrameLayout.LayoutParams {

        /* renamed from: a */
        int f2412a = 0;

        /* renamed from: b */
        float f2413b = 0.5f;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.CollapsingAppBarLayout_LayoutParams);
            this.f2412a = obtainStyledAttributes.getInt(C2394i.CollapsingAppBarLayout_LayoutParams_layout_collapseMode, 0);
            mo1565a(obtainStyledAttributes.getFloat(C2394i.f10029xad49a364, 0.5f));
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(android.widget.FrameLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* renamed from: a */
        public void mo1565a(float f) {
            this.f2413b = f;
        }
    }

    /* renamed from: android.support.design.widget.CollapsingToolbarLayout$a */
    class C0376a implements C0369a {
        private C0376a() {
        }

        /* renamed from: a */
        public void mo1493a(AppBarLayout appBarLayout, int i) {
            boolean z = false;
            CollapsingToolbarLayout.this.f2408t = i;
            int i2 = CollapsingToolbarLayout.this.f2409u != null ? CollapsingToolbarLayout.this.f2409u.mo13567b() : 0;
            int totalScrollRange = appBarLayout.getTotalScrollRange();
            int childCount = CollapsingToolbarLayout.this.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = CollapsingToolbarLayout.this.getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                ViewOffsetHelper a = CollapsingToolbarLayout.m3197d(childAt);
                switch (layoutParams.f2412a) {
                    case 1:
                        if ((CollapsingToolbarLayout.this.getHeight() - i2) + i < childAt.getHeight()) {
                            break;
                        } else {
                            a.mo753a(-i);
                            break;
                        }
                    case 2:
                        a.mo753a(Math.round(((float) (-i)) * layoutParams.f2413b));
                        break;
                }
            }
            if (!(CollapsingToolbarLayout.this.f2402n == null && CollapsingToolbarLayout.this.f2403o == null)) {
                CollapsingToolbarLayout collapsingToolbarLayout = CollapsingToolbarLayout.this;
                if (CollapsingToolbarLayout.this.getHeight() + i < CollapsingToolbarLayout.this.getScrimTriggerOffset() + i2) {
                    z = true;
                }
                collapsingToolbarLayout.setScrimsShown(z);
            }
            if (CollapsingToolbarLayout.this.f2403o != null && i2 > 0) {
                ViewCompat.m12913d(CollapsingToolbarLayout.this);
            }
            CollapsingToolbarLayout.this.f2399k.mo14837b(((float) Math.abs(i)) / ((float) ((CollapsingToolbarLayout.this.getHeight() - ViewCompat.m12933r(CollapsingToolbarLayout.this)) - i2)));
            if (Math.abs(i) == totalScrollRange) {
                ViewCompat.m12920f((View) appBarLayout, appBarLayout.getTargetElevation());
            } else {
                ViewCompat.m12920f((View) appBarLayout, 0.0f);
            }
        }
    }

    public CollapsingToolbarLayout(Context context) {
        this(context, null);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2389a = true;
        this.f2398j = new Rect();
        ThemeUtils.m0a(context);
        this.f2399k = new CollapsingTextHelper(this);
        this.f2399k.mo14834a(AnimationUtils.f10967e);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.CollapsingToolbarLayout, i, C2393h.Widget_Design_CollapsingToolbar);
        this.f2399k.mo14843c(obtainStyledAttributes.getInt(C2394i.CollapsingToolbarLayout_expandedTitleGravity, 8388691));
        this.f2399k.mo14846d(obtainStyledAttributes.getInt(C2394i.CollapsingToolbarLayout_collapsedTitleGravity, 8388627));
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(C2394i.CollapsingToolbarLayout_expandedTitleMargin, 0);
        this.f2397i = dimensionPixelSize;
        this.f2396h = dimensionPixelSize;
        this.f2395g = dimensionPixelSize;
        this.f2394f = dimensionPixelSize;
        if (obtainStyledAttributes.hasValue(C2394i.CollapsingToolbarLayout_expandedTitleMarginStart)) {
            this.f2394f = obtainStyledAttributes.getDimensionPixelSize(C2394i.CollapsingToolbarLayout_expandedTitleMarginStart, 0);
        }
        if (obtainStyledAttributes.hasValue(C2394i.CollapsingToolbarLayout_expandedTitleMarginEnd)) {
            this.f2396h = obtainStyledAttributes.getDimensionPixelSize(C2394i.CollapsingToolbarLayout_expandedTitleMarginEnd, 0);
        }
        if (obtainStyledAttributes.hasValue(C2394i.CollapsingToolbarLayout_expandedTitleMarginTop)) {
            this.f2395g = obtainStyledAttributes.getDimensionPixelSize(C2394i.CollapsingToolbarLayout_expandedTitleMarginTop, 0);
        }
        if (obtainStyledAttributes.hasValue(C2394i.CollapsingToolbarLayout_expandedTitleMarginBottom)) {
            this.f2397i = obtainStyledAttributes.getDimensionPixelSize(C2394i.CollapsingToolbarLayout_expandedTitleMarginBottom, 0);
        }
        this.f2400l = obtainStyledAttributes.getBoolean(C2394i.CollapsingToolbarLayout_titleEnabled, true);
        setTitle(obtainStyledAttributes.getText(C2394i.CollapsingToolbarLayout_title));
        this.f2399k.mo14850f(C2393h.TextAppearance_Design_CollapsingToolbar_Expanded);
        this.f2399k.mo14848e(C2393h.TextAppearance_AppCompat_Widget_ActionBar_Title);
        if (obtainStyledAttributes.hasValue(C2394i.CollapsingToolbarLayout_expandedTitleTextAppearance)) {
            this.f2399k.mo14850f(obtainStyledAttributes.getResourceId(C2394i.CollapsingToolbarLayout_expandedTitleTextAppearance, 0));
        }
        if (obtainStyledAttributes.hasValue(C2394i.CollapsingToolbarLayout_collapsedTitleTextAppearance)) {
            this.f2399k.mo14848e(obtainStyledAttributes.getResourceId(C2394i.CollapsingToolbarLayout_collapsedTitleTextAppearance, 0));
        }
        setContentScrim(obtainStyledAttributes.getDrawable(C2394i.CollapsingToolbarLayout_contentScrim));
        setStatusBarScrim(obtainStyledAttributes.getDrawable(C2394i.CollapsingToolbarLayout_statusBarScrim));
        this.f2390b = obtainStyledAttributes.getResourceId(C2394i.CollapsingToolbarLayout_toolbarId, -1);
        obtainStyledAttributes.recycle();
        setWillNotDraw(false);
        ViewCompat.m12895a((View) this, (OnApplyWindowInsetsListener) new OnApplyWindowInsetsListener() {
            public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat fzVar) {
                return CollapsingToolbarLayout.this.m3187a(fzVar);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public WindowInsetsCompat m3187a(WindowInsetsCompat fzVar) {
        if (this.f2409u != fzVar) {
            this.f2409u = fzVar;
            requestLayout();
        }
        return fzVar.mo13572f();
    }

    /* renamed from: a */
    private void m3188a(int i) {
        m3193b();
        if (this.f2406r == null) {
            this.f2406r = ViewUtils.m1925a();
            this.f2406r.mo137a(600);
            this.f2406r.mo141a(i > this.f2404p ? AnimationUtils.f10965c : AnimationUtils.f10966d);
            this.f2406r.mo140a((C0046c) new C0046c() {
                /* renamed from: a */
                public void mo155a(ValueAnimatorCompat abVar) {
                    CollapsingToolbarLayout.this.setScrimAlpha(abVar.mo143c());
                }
            });
        } else if (this.f2406r.mo142b()) {
            this.f2406r.mo145e();
        }
        this.f2406r.mo138a(this.f2404p, i);
        this.f2406r.mo135a();
    }

    /* renamed from: b */
    private View m3192b(View view) {
        View view2 = view;
        ViewParent parent = view.getParent();
        while (parent != this && parent != null) {
            if (parent instanceof View) {
                view2 = (View) parent;
            }
            parent = parent.getParent();
        }
        return view2;
    }

    /* renamed from: b */
    private void m3193b() {
        if (this.f2389a) {
            this.f2391c = null;
            this.f2392d = null;
            if (this.f2390b != -1) {
                this.f2391c = (Toolbar) findViewById(this.f2390b);
                if (this.f2391c != null) {
                    this.f2392d = m3192b((View) this.f2391c);
                }
            }
            if (this.f2391c == null) {
                Toolbar toolbar = null;
                int i = 0;
                int childCount = getChildCount();
                while (true) {
                    if (i >= childCount) {
                        break;
                    }
                    View childAt = getChildAt(i);
                    if (childAt instanceof Toolbar) {
                        toolbar = (Toolbar) childAt;
                        break;
                    }
                    i++;
                }
                this.f2391c = toolbar;
            }
            m3196c();
            this.f2389a = false;
        }
    }

    /* renamed from: c */
    private static int m3194c(View view) {
        android.view.ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof MarginLayoutParams)) {
            return view.getHeight();
        }
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) layoutParams;
        return view.getHeight() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    /* renamed from: c */
    private void m3196c() {
        if (!this.f2400l && this.f2393e != null) {
            ViewParent parent = this.f2393e.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.f2393e);
            }
        }
        if (this.f2400l && this.f2391c != null) {
            if (this.f2393e == null) {
                this.f2393e = new View(getContext());
            }
            if (this.f2393e.getParent() == null) {
                this.f2391c.addView(this.f2393e, -1, -1);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static ViewOffsetHelper m3197d(View view) {
        ViewOffsetHelper agVar = (ViewOffsetHelper) view.getTag(C2390e.view_offset_helper);
        if (agVar != null) {
            return agVar;
        }
        ViewOffsetHelper agVar2 = new ViewOffsetHelper(view);
        view.setTag(C2390e.view_offset_helper, agVar2);
        return agVar2;
    }

    /* access modifiers changed from: private */
    public void setScrimAlpha(int i) {
        if (i != this.f2404p) {
            if (!(this.f2402n == null || this.f2391c == null)) {
                ViewCompat.m12913d(this.f2391c);
            }
            this.f2404p = i;
            ViewCompat.m12913d(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(super.generateDefaultLayoutParams());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public android.widget.FrameLayout.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        m3193b();
        if (this.f2391c == null && this.f2402n != null && this.f2404p > 0) {
            this.f2402n.mutate().setAlpha(this.f2404p);
            this.f2402n.draw(canvas);
        }
        if (this.f2400l && this.f2401m) {
            this.f2399k.mo14832a(canvas);
        }
        if (this.f2403o != null && this.f2404p > 0) {
            int i = this.f2409u != null ? this.f2409u.mo13567b() : 0;
            if (i > 0) {
                this.f2403o.setBounds(0, -this.f2408t, getWidth(), i - this.f2408t);
                this.f2403o.mutate().setAlpha(this.f2404p);
                this.f2403o.draw(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        m3193b();
        if (view == this.f2391c && this.f2402n != null && this.f2404p > 0) {
            this.f2402n.mutate().setAlpha(this.f2404p);
            this.f2402n.draw(canvas);
        }
        return super.drawChild(canvas, view, j);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        boolean z = false;
        Drawable drawable = this.f2403o;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        Drawable drawable2 = this.f2402n;
        if (drawable2 != null && drawable2.isStateful()) {
            z |= drawable2.setState(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    public android.widget.FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public int getCollapsedTitleGravity() {
        return this.f2399k.mo14842c();
    }

    public Typeface getCollapsedTitleTypeface() {
        return this.f2399k.mo14845d();
    }

    public Drawable getContentScrim() {
        return this.f2402n;
    }

    public int getExpandedTitleGravity() {
        return this.f2399k.mo14836b();
    }

    public int getExpandedTitleMarginBottom() {
        return this.f2397i;
    }

    public int getExpandedTitleMarginEnd() {
        return this.f2396h;
    }

    public int getExpandedTitleMarginStart() {
        return this.f2394f;
    }

    public int getExpandedTitleMarginTop() {
        return this.f2395g;
    }

    public Typeface getExpandedTitleTypeface() {
        return this.f2399k.mo14847e();
    }

    /* access modifiers changed from: 0000 */
    public final int getScrimTriggerOffset() {
        return ViewCompat.m12933r(this) * 2;
    }

    public Drawable getStatusBarScrim() {
        return this.f2403o;
    }

    public CharSequence getTitle() {
        if (this.f2400l) {
            return this.f2399k.mo14853i();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            if (this.f2407s == null) {
                this.f2407s = new C0376a();
            }
            ((AppBarLayout) parent).mo1436a(this.f2407s);
        }
        ViewCompat.m12938w(this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ViewParent parent = getParent();
        if (this.f2407s != null && (parent instanceof AppBarLayout)) {
            ((AppBarLayout) parent).mo1437b(this.f2407s);
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.f2400l && this.f2393e != null) {
            this.f2401m = ViewCompat.m12879H(this.f2393e) && this.f2393e.getVisibility() == 0;
            if (this.f2401m) {
                int i5 = 0;
                if (!(this.f2392d == null || this.f2392d == this)) {
                    i5 = ((LayoutParams) this.f2392d.getLayoutParams()).bottomMargin;
                }
                ViewGroupUtils.m1250b(this, this.f2393e, this.f2398j);
                this.f2399k.mo14839b(this.f2398j.left, (i4 - this.f2398j.height()) - i5, this.f2398j.right, i4 - i5);
                boolean z2 = ViewCompat.m12923h(this) == 1;
                this.f2399k.mo14831a(z2 ? this.f2396h : this.f2394f, this.f2395g + this.f2398j.bottom, (i3 - i) - (z2 ? this.f2394f : this.f2396h), (i4 - i2) - this.f2397i);
                this.f2399k.mo14852h();
            }
        }
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (this.f2409u != null && !ViewCompat.m12939x(childAt)) {
                int b = this.f2409u.mo13567b();
                if (childAt.getTop() < b) {
                    ViewCompat.m12918e(childAt, b);
                }
            }
            m3197d(childAt).mo752a();
        }
        if (this.f2391c != null) {
            if (this.f2400l && TextUtils.isEmpty(this.f2399k.mo14853i())) {
                this.f2399k.mo14835a(this.f2391c.getTitle());
            }
            if (this.f2392d == null || this.f2392d == this) {
                setMinimumHeight(m3194c((View) this.f2391c));
            } else {
                setMinimumHeight(m3194c(this.f2392d));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        m3193b();
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.f2402n != null) {
            this.f2402n.setBounds(0, 0, i, i2);
        }
    }

    public void setCollapsedTitleGravity(int i) {
        this.f2399k.mo14846d(i);
    }

    public void setCollapsedTitleTextAppearance(int i) {
        this.f2399k.mo14848e(i);
    }

    public void setCollapsedTitleTextColor(int i) {
        this.f2399k.mo14830a(i);
    }

    public void setCollapsedTitleTypeface(Typeface typeface) {
        this.f2399k.mo14833a(typeface);
    }

    public void setContentScrim(Drawable drawable) {
        Drawable drawable2 = null;
        if (this.f2402n != drawable) {
            if (this.f2402n != null) {
                this.f2402n.setCallback(null);
            }
            if (drawable != null) {
                drawable2 = drawable.mutate();
            }
            this.f2402n = drawable2;
            if (this.f2402n != null) {
                this.f2402n.setBounds(0, 0, getWidth(), getHeight());
                this.f2402n.setCallback(this);
                this.f2402n.setAlpha(this.f2404p);
            }
            ViewCompat.m12913d(this);
        }
    }

    public void setContentScrimColor(int i) {
        setContentScrim(new ColorDrawable(i));
    }

    public void setContentScrimResource(int i) {
        setContentScrim(ContextCompat.getDrawable(getContext(), i));
    }

    public void setExpandedTitleColor(int i) {
        this.f2399k.mo14838b(i);
    }

    public void setExpandedTitleGravity(int i) {
        this.f2399k.mo14843c(i);
    }

    public void setExpandedTitleMargin(int i, int i2, int i3, int i4) {
        this.f2394f = i;
        this.f2395g = i2;
        this.f2396h = i3;
        this.f2397i = i4;
        requestLayout();
    }

    public void setExpandedTitleMarginBottom(int i) {
        this.f2397i = i;
        requestLayout();
    }

    public void setExpandedTitleMarginEnd(int i) {
        this.f2396h = i;
        requestLayout();
    }

    public void setExpandedTitleMarginStart(int i) {
        this.f2394f = i;
        requestLayout();
    }

    public void setExpandedTitleMarginTop(int i) {
        this.f2395g = i;
        requestLayout();
    }

    public void setExpandedTitleTextAppearance(int i) {
        this.f2399k.mo14850f(i);
    }

    public void setExpandedTitleTypeface(Typeface typeface) {
        this.f2399k.mo14840b(typeface);
    }

    public void setScrimsShown(boolean z) {
        setScrimsShown(z, ViewCompat.m12877F(this) && !isInEditMode());
    }

    public void setScrimsShown(boolean z, boolean z2) {
        int i = 255;
        if (this.f2405q != z) {
            if (z2) {
                if (!z) {
                    i = 0;
                }
                m3188a(i);
            } else {
                if (!z) {
                    i = 0;
                }
                setScrimAlpha(i);
            }
            this.f2405q = z;
        }
    }

    public void setStatusBarScrim(Drawable drawable) {
        Drawable drawable2 = null;
        if (this.f2403o != drawable) {
            if (this.f2403o != null) {
                this.f2403o.setCallback(null);
            }
            if (drawable != null) {
                drawable2 = drawable.mutate();
            }
            this.f2403o = drawable2;
            if (this.f2403o != null) {
                if (this.f2403o.isStateful()) {
                    this.f2403o.setState(getDrawableState());
                }
                DrawableCompat.setLayoutDirection(this.f2403o, ViewCompat.m12923h(this));
                this.f2403o.setVisible(getVisibility() == 0, false);
                this.f2403o.setCallback(this);
                this.f2403o.setAlpha(this.f2404p);
            }
            ViewCompat.m12913d(this);
        }
    }

    public void setStatusBarScrimColor(int i) {
        setStatusBarScrim(new ColorDrawable(i));
    }

    public void setStatusBarScrimResource(int i) {
        setStatusBarScrim(ContextCompat.getDrawable(getContext(), i));
    }

    public void setTitle(CharSequence charSequence) {
        this.f2399k.mo14835a(charSequence);
    }

    public void setTitleEnabled(boolean z) {
        if (z != this.f2400l) {
            this.f2400l = z;
            m3196c();
            requestLayout();
        }
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean z = i == 0;
        if (!(this.f2403o == null || this.f2403o.isVisible() == z)) {
            this.f2403o.setVisible(z, false);
        }
        if (this.f2402n != null && this.f2402n.isVisible() != z) {
            this.f2402n.setVisible(z, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f2402n || drawable == this.f2403o;
    }
}
