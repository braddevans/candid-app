package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.p001v4.view.ViewPager;
import android.support.p001v4.view.ViewPager.C0497e;
import android.support.p001v4.widget.TextViewCompat;
import android.support.p003v7.app.ActionBar.Tab;
import android.support.p003v7.widget.AppCompatDrawableManager;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class TabLayout extends HorizontalScrollView {

    /* renamed from: a */
    private static final C2286a<C0416d> f2547a = new C2288c(16);

    /* renamed from: A */
    private final C2286a<C0418f> f2548A;

    /* renamed from: b */
    private final ArrayList<C0416d> f2549b;

    /* renamed from: c */
    private C0416d f2550c;

    /* renamed from: d */
    private final C0413c f2551d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public int f2552e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public int f2553f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public int f2554g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public int f2555h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public int f2556i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public ColorStateList f2557j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public float f2558k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public float f2559l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public final int f2560m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public int f2561n;

    /* renamed from: o */
    private final int f2562o;

    /* renamed from: p */
    private final int f2563p;

    /* renamed from: q */
    private final int f2564q;

    /* renamed from: r */
    private int f2565r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public int f2566s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public int f2567t;

    /* renamed from: u */
    private C0411a f2568u;

    /* renamed from: v */
    private ValueAnimatorCompat f2569v;

    /* renamed from: w */
    private ViewPager f2570w;

    /* renamed from: x */
    private PagerAdapter f2571x;

    /* renamed from: y */
    private DataSetObserver f2572y;

    /* renamed from: z */
    private C0417e f2573z;

    /* renamed from: android.support.design.widget.TabLayout$a */
    public interface C0411a {
        void onTabReselected(C0416d dVar);

        void onTabSelected(C0416d dVar);

        void onTabUnselected(C0416d dVar);
    }

    /* renamed from: android.support.design.widget.TabLayout$b */
    class C0412b extends DataSetObserver {
        private C0412b() {
        }

        public void onChanged() {
            TabLayout.this.m3431c();
        }

        public void onInvalidated() {
            TabLayout.this.m3431c();
        }
    }

    /* renamed from: android.support.design.widget.TabLayout$c */
    class C0413c extends LinearLayout {

        /* renamed from: b */
        private int f2577b;

        /* renamed from: c */
        private final Paint f2578c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public int f2579d = -1;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public float f2580e;

        /* renamed from: f */
        private int f2581f = -1;

        /* renamed from: g */
        private int f2582g = -1;

        /* renamed from: h */
        private ValueAnimatorCompat f2583h;

        C0413c(Context context) {
            super(context);
            setWillNotDraw(false);
            this.f2578c = new Paint();
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m3460b(int i, int i2) {
            if (i != this.f2581f || i2 != this.f2582g) {
                this.f2581f = i;
                this.f2582g = i2;
                ViewCompat.m12913d(this);
            }
        }

        /* renamed from: c */
        private void m3461c() {
            int i;
            int i2;
            View childAt = getChildAt(this.f2579d);
            if (childAt == null || childAt.getWidth() <= 0) {
                i = -1;
                i2 = -1;
            } else {
                i2 = childAt.getLeft();
                i = childAt.getRight();
                if (this.f2580e > 0.0f && this.f2579d < getChildCount() - 1) {
                    View childAt2 = getChildAt(this.f2579d + 1);
                    i2 = (int) ((this.f2580e * ((float) childAt2.getLeft())) + ((1.0f - this.f2580e) * ((float) i2)));
                    i = (int) ((this.f2580e * ((float) childAt2.getRight())) + ((1.0f - this.f2580e) * ((float) i)));
                }
            }
            m3460b(i2, i);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo1798a(int i) {
            if (this.f2578c.getColor() != i) {
                this.f2578c.setColor(i);
                ViewCompat.m12913d(this);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo1799a(int i, float f) {
            if (this.f2583h != null && this.f2583h.mo142b()) {
                this.f2583h.mo145e();
            }
            this.f2579d = i;
            this.f2580e = f;
            m3461c();
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo1800a(final int i, int i2) {
            final int i3;
            final int i4;
            if (this.f2583h != null && this.f2583h.mo142b()) {
                this.f2583h.mo145e();
            }
            boolean z = ViewCompat.m12923h(this) == 1;
            View childAt = getChildAt(i);
            if (childAt == null) {
                m3461c();
                return;
            }
            final int left = childAt.getLeft();
            final int right = childAt.getRight();
            if (Math.abs(i - this.f2579d) <= 1) {
                i4 = this.f2581f;
                i3 = this.f2582g;
            } else {
                int a = TabLayout.this.m3426b(24);
                if (i < this.f2579d) {
                    if (z) {
                        i3 = left - a;
                        i4 = i3;
                    } else {
                        i3 = right + a;
                        i4 = i3;
                    }
                } else if (z) {
                    i3 = right + a;
                    i4 = i3;
                } else {
                    i3 = left - a;
                    i4 = i3;
                }
            }
            if (i4 != left || i3 != right) {
                ValueAnimatorCompat a2 = ViewUtils.m1925a();
                this.f2583h = a2;
                a2.mo141a(AnimationUtils.f10964b);
                a2.mo137a(i2);
                a2.mo136a(0.0f, 1.0f);
                a2.mo140a((C0046c) new C0046c() {
                    /* renamed from: a */
                    public void mo155a(ValueAnimatorCompat abVar) {
                        float f = abVar.mo146f();
                        C0413c.this.m3460b(AnimationUtils.m14713a(i4, left, f), AnimationUtils.m14713a(i3, right, f));
                    }
                });
                a2.mo139a((C0044a) new C0045b() {
                    /* renamed from: a */
                    public void mo152a(ValueAnimatorCompat abVar) {
                        C0413c.this.f2579d = i;
                        C0413c.this.f2580e = 0.0f;
                    }
                });
                a2.mo135a();
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo1801a() {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                if (getChildAt(i).getWidth() <= 0) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public float mo1802b() {
            return ((float) this.f2579d) + this.f2580e;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void mo1803b(int i) {
            if (this.f2577b != i) {
                this.f2577b = i;
                ViewCompat.m12913d(this);
            }
        }

        public void draw(Canvas canvas) {
            super.draw(canvas);
            if (this.f2581f >= 0 && this.f2582g > this.f2581f) {
                canvas.drawRect((float) this.f2581f, (float) (getHeight() - this.f2577b), (float) this.f2582g, (float) getHeight(), this.f2578c);
            }
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            if (this.f2583h == null || !this.f2583h.mo142b()) {
                m3461c();
                return;
            }
            this.f2583h.mo145e();
            mo1800a(this.f2579d, Math.round((1.0f - this.f2583h.mo146f()) * ((float) this.f2583h.mo147g())));
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (MeasureSpec.getMode(i) == 1073741824 && TabLayout.this.f2567t == 1 && TabLayout.this.f2566s == 1) {
                int childCount = getChildCount();
                int i3 = 0;
                int i4 = childCount;
                for (int i5 = 0; i5 < i4; i5++) {
                    View childAt = getChildAt(i5);
                    if (childAt.getVisibility() == 0) {
                        i3 = Math.max(i3, childAt.getMeasuredWidth());
                    }
                }
                if (i3 > 0) {
                    boolean z = false;
                    if (i3 * childCount <= getMeasuredWidth() - (TabLayout.this.m3426b(16) * 2)) {
                        for (int i6 = 0; i6 < childCount; i6++) {
                            LayoutParams layoutParams = (LayoutParams) getChildAt(i6).getLayoutParams();
                            if (layoutParams.width != i3 || layoutParams.weight != 0.0f) {
                                layoutParams.width = i3;
                                layoutParams.weight = 0.0f;
                                z = true;
                            }
                        }
                    } else {
                        TabLayout.this.f2566s = 0;
                        TabLayout.this.m3425a(false);
                        z = true;
                    }
                    if (z) {
                        super.onMeasure(i, i2);
                    }
                }
            }
        }
    }

    /* renamed from: android.support.design.widget.TabLayout$d */
    public static final class C0416d {

        /* renamed from: a */
        private Object f2591a;

        /* renamed from: b */
        private Drawable f2592b;

        /* renamed from: c */
        private CharSequence f2593c;

        /* renamed from: d */
        private CharSequence f2594d;

        /* renamed from: e */
        private int f2595e;

        /* renamed from: f */
        private View f2596f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public TabLayout f2597g;
        /* access modifiers changed from: private */

        /* renamed from: h */
        public C0418f f2598h;

        private C0416d() {
            this.f2595e = -1;
        }

        /* access modifiers changed from: private */
        /* renamed from: g */
        public void m3476g() {
            if (this.f2598h != null) {
                this.f2598h.mo1821a();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: h */
        public void m3477h() {
            this.f2597g = null;
            this.f2598h = null;
            this.f2591a = null;
            this.f2592b = null;
            this.f2593c = null;
            this.f2594d = null;
            this.f2595e = -1;
            this.f2596f = null;
        }

        /* renamed from: a */
        public C0416d mo1807a(int i) {
            return mo1809a(LayoutInflater.from(this.f2598h.getContext()).inflate(i, this.f2598h, false));
        }

        /* renamed from: a */
        public C0416d mo1808a(Drawable drawable) {
            this.f2592b = drawable;
            m3476g();
            return this;
        }

        /* renamed from: a */
        public C0416d mo1809a(View view) {
            this.f2596f = view;
            m3476g();
            return this;
        }

        /* renamed from: a */
        public C0416d mo1810a(CharSequence charSequence) {
            this.f2593c = charSequence;
            m3476g();
            return this;
        }

        /* renamed from: a */
        public View mo1811a() {
            return this.f2596f;
        }

        /* renamed from: b */
        public Drawable mo1812b() {
            return this.f2592b;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void mo1813b(int i) {
            this.f2595e = i;
        }

        /* renamed from: c */
        public int mo1814c() {
            return this.f2595e;
        }

        /* renamed from: d */
        public CharSequence mo1815d() {
            return this.f2593c;
        }

        /* renamed from: e */
        public void mo1816e() {
            if (this.f2597g == null) {
                throw new IllegalArgumentException("Tab not attached to a TabLayout");
            }
            this.f2597g.mo1773b(this);
        }

        /* renamed from: f */
        public CharSequence mo1817f() {
            return this.f2594d;
        }
    }

    /* renamed from: android.support.design.widget.TabLayout$e */
    public static class C0417e implements C0497e {

        /* renamed from: a */
        private final WeakReference<TabLayout> f2599a;

        /* renamed from: b */
        private int f2600b;

        /* renamed from: c */
        private int f2601c;

        public C0417e(TabLayout tabLayout) {
            this.f2599a = new WeakReference<>(tabLayout);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m3489a() {
            this.f2601c = 0;
            this.f2600b = 0;
        }

        public void onPageScrollStateChanged(int i) {
            this.f2600b = this.f2601c;
            this.f2601c = i;
        }

        public void onPageScrolled(int i, float f, int i2) {
            TabLayout tabLayout = (TabLayout) this.f2599a.get();
            if (tabLayout != null) {
                tabLayout.m3417a(i, f, this.f2601c != 2 || this.f2600b == 1, (this.f2601c == 2 && this.f2600b == 0) ? false : true);
            }
        }

        public void onPageSelected(int i) {
            TabLayout tabLayout = (TabLayout) this.f2599a.get();
            if (tabLayout != null && tabLayout.getSelectedTabPosition() != i) {
                tabLayout.mo1774b(tabLayout.mo1765a(i), this.f2601c == 0 || (this.f2601c == 2 && this.f2600b == 0));
            }
        }
    }

    /* renamed from: android.support.design.widget.TabLayout$f */
    class C0418f extends LinearLayout implements OnLongClickListener {

        /* renamed from: b */
        private C0416d f2603b;

        /* renamed from: c */
        private TextView f2604c;

        /* renamed from: d */
        private ImageView f2605d;

        /* renamed from: e */
        private View f2606e;

        /* renamed from: f */
        private TextView f2607f;

        /* renamed from: g */
        private ImageView f2608g;

        /* renamed from: h */
        private int f2609h = 2;

        public C0418f(Context context) {
            super(context);
            if (TabLayout.this.f2560m != 0) {
                setBackgroundDrawable(AppCompatDrawableManager.get().getDrawable(context, TabLayout.this.f2560m));
            }
            ViewCompat.m12905b(this, TabLayout.this.f2552e, TabLayout.this.f2553f, TabLayout.this.f2554g, TabLayout.this.f2555h);
            setGravity(17);
            setOrientation(1);
            setClickable(true);
        }

        /* renamed from: a */
        private float m3491a(Layout layout, int i, float f) {
            return layout.getLineWidth(i) * (f / layout.getPaint().getTextSize());
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m3492a(C0416d dVar) {
            if (dVar != this.f2603b) {
                this.f2603b = dVar;
                mo1821a();
            }
        }

        /* renamed from: a */
        private void m3495a(TextView textView, ImageView imageView) {
            Drawable drawable = this.f2603b != null ? this.f2603b.mo1812b() : null;
            CharSequence charSequence = this.f2603b != null ? this.f2603b.mo1815d() : null;
            CharSequence charSequence2 = this.f2603b != null ? this.f2603b.mo1817f() : null;
            if (imageView != null) {
                if (drawable != null) {
                    imageView.setImageDrawable(drawable);
                    imageView.setVisibility(0);
                    setVisibility(0);
                } else {
                    imageView.setVisibility(8);
                    imageView.setImageDrawable(null);
                }
                imageView.setContentDescription(charSequence2);
            }
            boolean z = !TextUtils.isEmpty(charSequence);
            if (textView != null) {
                if (z) {
                    textView.setText(charSequence);
                    textView.setVisibility(0);
                    setVisibility(0);
                } else {
                    textView.setVisibility(8);
                    textView.setText(null);
                }
                textView.setContentDescription(charSequence2);
            }
            if (imageView != null) {
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) imageView.getLayoutParams();
                int i = 0;
                if (z && imageView.getVisibility() == 0) {
                    i = TabLayout.this.m3426b(8);
                }
                if (i != marginLayoutParams.bottomMargin) {
                    marginLayoutParams.bottomMargin = i;
                    imageView.requestLayout();
                }
            }
            if (z || TextUtils.isEmpty(charSequence2)) {
                setOnLongClickListener(null);
                setLongClickable(false);
                return;
            }
            setOnLongClickListener(this);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m3496b() {
            m3492a((C0416d) null);
            setSelected(false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public final void mo1821a() {
            C0416d dVar = this.f2603b;
            View view = dVar != null ? dVar.mo1811a() : null;
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(view);
                    }
                    addView(view);
                }
                this.f2606e = view;
                if (this.f2604c != null) {
                    this.f2604c.setVisibility(8);
                }
                if (this.f2605d != null) {
                    this.f2605d.setVisibility(8);
                    this.f2605d.setImageDrawable(null);
                }
                this.f2607f = (TextView) view.findViewById(16908308);
                if (this.f2607f != null) {
                    this.f2609h = TextViewCompat.getMaxLines(this.f2607f);
                }
                this.f2608g = (ImageView) view.findViewById(16908294);
            } else {
                if (this.f2606e != null) {
                    removeView(this.f2606e);
                    this.f2606e = null;
                }
                this.f2607f = null;
                this.f2608g = null;
            }
            if (this.f2606e == null) {
                if (this.f2605d == null) {
                    ImageView imageView = (ImageView) LayoutInflater.from(getContext()).inflate(C2391f.design_layout_tab_icon, this, false);
                    addView(imageView, 0);
                    this.f2605d = imageView;
                }
                if (this.f2604c == null) {
                    TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(C2391f.design_layout_tab_text, this, false);
                    addView(textView);
                    this.f2604c = textView;
                    this.f2609h = TextViewCompat.getMaxLines(this.f2604c);
                }
                this.f2604c.setTextAppearance(getContext(), TabLayout.this.f2556i);
                if (TabLayout.this.f2557j != null) {
                    this.f2604c.setTextColor(TabLayout.this.f2557j);
                }
                m3495a(this.f2604c, this.f2605d);
            } else if (this.f2607f != null || this.f2608g != null) {
                m3495a(this.f2607f, this.f2608g);
            }
        }

        @TargetApi(14)
        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(Tab.class.getName());
        }

        @TargetApi(14)
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(Tab.class.getName());
        }

        public boolean onLongClick(View view) {
            int[] iArr = new int[2];
            getLocationOnScreen(iArr);
            Context context = getContext();
            int width = getWidth();
            int height = getHeight();
            int i = context.getResources().getDisplayMetrics().widthPixels;
            Toast makeText = Toast.makeText(context, this.f2603b.mo1817f(), 0);
            makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
            makeText.show();
            return true;
        }

        public void onMeasure(int i, int i2) {
            int size = MeasureSpec.getSize(i);
            int mode = MeasureSpec.getMode(i);
            int f = TabLayout.this.getTabMaxWidth();
            int i3 = i2;
            int i4 = (f <= 0 || (mode != 0 && size <= f)) ? i : MeasureSpec.makeMeasureSpec(TabLayout.this.f2561n, Integer.MIN_VALUE);
            super.onMeasure(i4, i3);
            if (this.f2604c != null) {
                Resources resources = getResources();
                float h = TabLayout.this.f2558k;
                int i5 = this.f2609h;
                if (this.f2605d != null && this.f2605d.getVisibility() == 0) {
                    i5 = 1;
                } else if (this.f2604c != null && this.f2604c.getLineCount() > 1) {
                    h = TabLayout.this.f2559l;
                }
                float textSize = this.f2604c.getTextSize();
                int lineCount = this.f2604c.getLineCount();
                int maxLines = TextViewCompat.getMaxLines(this.f2604c);
                if (h != textSize || (maxLines >= 0 && i5 != maxLines)) {
                    boolean z = true;
                    if (TabLayout.this.f2567t == 1 && h > textSize && lineCount == 1) {
                        Layout layout = this.f2604c.getLayout();
                        if (layout == null || m3491a(layout, 0, h) > ((float) layout.getWidth())) {
                            z = false;
                        }
                    }
                    if (z) {
                        this.f2604c.setTextSize(0, h);
                        this.f2604c.setMaxLines(i5);
                        super.onMeasure(i4, i3);
                    }
                }
            }
        }

        public boolean performClick() {
            boolean performClick = super.performClick();
            if (this.f2603b == null) {
                return performClick;
            }
            this.f2603b.mo1816e();
            return true;
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
                if (this.f2604c != null) {
                    this.f2604c.setSelected(z);
                }
                if (this.f2605d != null) {
                    this.f2605d.setSelected(z);
                }
            }
        }
    }

    /* renamed from: android.support.design.widget.TabLayout$g */
    public static class C0419g implements C0411a {

        /* renamed from: a */
        private final ViewPager f2610a;

        public C0419g(ViewPager viewPager) {
            this.f2610a = viewPager;
        }

        public void onTabReselected(C0416d dVar) {
        }

        public void onTabSelected(C0416d dVar) {
            this.f2610a.setCurrentItem(dVar.mo1814c());
        }

        public void onTabUnselected(C0416d dVar) {
        }
    }

    public TabLayout(Context context) {
        this(context, null);
    }

    public TabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public TabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2549b = new ArrayList<>();
        this.f2561n = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        this.f2548A = new C2287b(12);
        ThemeUtils.m0a(context);
        setHorizontalScrollBarEnabled(false);
        this.f2551d = new C0413c(context);
        super.addView(this.f2551d, 0, new FrameLayout.LayoutParams(-2, -1));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.TabLayout, i, C2393h.Widget_Design_TabLayout);
        this.f2551d.mo1803b(obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabIndicatorHeight, 0));
        this.f2551d.mo1798a(obtainStyledAttributes.getColor(C2394i.TabLayout_tabIndicatorColor, 0));
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabPadding, 0);
        this.f2555h = dimensionPixelSize;
        this.f2554g = dimensionPixelSize;
        this.f2553f = dimensionPixelSize;
        this.f2552e = dimensionPixelSize;
        this.f2552e = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabPaddingStart, this.f2552e);
        this.f2553f = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabPaddingTop, this.f2553f);
        this.f2554g = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabPaddingEnd, this.f2554g);
        this.f2555h = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabPaddingBottom, this.f2555h);
        this.f2556i = obtainStyledAttributes.getResourceId(C2394i.TabLayout_tabTextAppearance, C2393h.TextAppearance_Design_Tab);
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(this.f2556i, C2394i.TextAppearance);
        try {
            this.f2558k = (float) obtainStyledAttributes2.getDimensionPixelSize(C2394i.TextAppearance_android_textSize, 0);
            this.f2557j = obtainStyledAttributes2.getColorStateList(C2394i.TextAppearance_android_textColor);
            obtainStyledAttributes2.recycle();
            if (obtainStyledAttributes.hasValue(C2394i.TabLayout_tabTextColor)) {
                this.f2557j = obtainStyledAttributes.getColorStateList(C2394i.TabLayout_tabTextColor);
            }
            if (obtainStyledAttributes.hasValue(C2394i.TabLayout_tabSelectedTextColor)) {
                this.f2557j = m3416a(this.f2557j.getDefaultColor(), obtainStyledAttributes.getColor(C2394i.TabLayout_tabSelectedTextColor, 0));
            }
            this.f2562o = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabMinWidth, -1);
            this.f2563p = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabMaxWidth, -1);
            this.f2560m = obtainStyledAttributes.getResourceId(C2394i.TabLayout_tabBackground, 0);
            this.f2565r = obtainStyledAttributes.getDimensionPixelSize(C2394i.TabLayout_tabContentStart, 0);
            this.f2567t = obtainStyledAttributes.getInt(C2394i.TabLayout_tabMode, 1);
            this.f2566s = obtainStyledAttributes.getInt(C2394i.TabLayout_tabGravity, 0);
            obtainStyledAttributes.recycle();
            Resources resources = getResources();
            this.f2559l = (float) resources.getDimensionPixelSize(C2389d.design_tab_text_size_2line);
            this.f2564q = resources.getDimensionPixelSize(C2389d.design_tab_scrollable_min_width);
            m3440f();
        } catch (Throwable th) {
            obtainStyledAttributes2.recycle();
            throw th;
        }
    }

    /* renamed from: a */
    private int m3413a(int i, float f) {
        int i2 = 0;
        if (this.f2567t != 0) {
            return 0;
        }
        View childAt = this.f2551d.getChildAt(i);
        View view = i + 1 < this.f2551d.getChildCount() ? this.f2551d.getChildAt(i + 1) : null;
        int i3 = childAt != null ? childAt.getWidth() : 0;
        if (view != null) {
            i2 = view.getWidth();
        }
        return ((childAt.getLeft() + ((int) ((((float) (i3 + i2)) * f) * 0.5f))) + (childAt.getWidth() / 2)) - (getWidth() / 2);
    }

    /* renamed from: a */
    private static ColorStateList m3416a(int i, int i2) {
        int[][] iArr = new int[2][];
        int[] iArr2 = new int[2];
        iArr[0] = SELECTED_STATE_SET;
        iArr2[0] = i2;
        int i3 = 0 + 1;
        iArr[i3] = EMPTY_STATE_SET;
        iArr2[i3] = i;
        int i4 = i3 + 1;
        return new ColorStateList(iArr, iArr2);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3417a(int i, float f, boolean z, boolean z2) {
        int round = Math.round(((float) i) + f);
        if (round >= 0 && round < this.f2551d.getChildCount()) {
            if (z2) {
                this.f2551d.mo1799a(i, f);
            }
            if (this.f2569v != null && this.f2569v.mo142b()) {
                this.f2569v.mo145e();
            }
            scrollTo(m3413a(i, f), 0);
            if (z) {
                setSelectedTabView(round);
            }
        }
    }

    /* renamed from: a */
    private void m3418a(TabItem tabItem) {
        C0416d a = mo1764a();
        if (tabItem.f2544a != null) {
            a.mo1810a(tabItem.f2544a);
        }
        if (tabItem.f2545b != null) {
            a.mo1808a(tabItem.f2545b);
        }
        if (tabItem.f2546c != 0) {
            a.mo1807a(tabItem.f2546c);
        }
        mo1766a(a);
    }

    /* renamed from: a */
    private void m3419a(C0416d dVar, int i) {
        dVar.mo1813b(i);
        this.f2549b.add(i, dVar);
        int size = this.f2549b.size();
        for (int i2 = i + 1; i2 < size; i2++) {
            ((C0416d) this.f2549b.get(i2)).mo1813b(i2);
        }
    }

    /* renamed from: a */
    private void m3422a(View view) {
        if (view instanceof TabItem) {
            m3418a((TabItem) view);
            return;
        }
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    /* renamed from: a */
    private void m3423a(LayoutParams layoutParams) {
        if (this.f2567t == 1 && this.f2566s == 0) {
            layoutParams.width = 0;
            layoutParams.weight = 1.0f;
            return;
        }
        layoutParams.width = -2;
        layoutParams.weight = 0.0f;
    }

    /* renamed from: a */
    private void m3424a(PagerAdapter evVar, boolean z) {
        if (!(this.f2571x == null || this.f2572y == null)) {
            this.f2571x.unregisterDataSetObserver(this.f2572y);
        }
        this.f2571x = evVar;
        if (z && evVar != null) {
            if (this.f2572y == null) {
                this.f2572y = new C0412b();
            }
            evVar.registerDataSetObserver(this.f2572y);
        }
        m3431c();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3425a(boolean z) {
        for (int i = 0; i < this.f2551d.getChildCount(); i++) {
            View childAt = this.f2551d.getChildAt(i);
            childAt.setMinimumWidth(getTabMinWidth());
            m3423a((LayoutParams) childAt.getLayoutParams());
            if (z) {
                childAt.requestLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public int m3426b(int i) {
        return Math.round(getResources().getDisplayMetrics().density * ((float) i));
    }

    /* renamed from: c */
    private C0418f m3430c(C0416d dVar) {
        C0418f fVar = this.f2548A != null ? (C0418f) this.f2548A.mo13295a() : null;
        if (fVar == null) {
            fVar = new C0418f(getContext());
        }
        fVar.m3492a(dVar);
        fVar.setFocusable(true);
        fVar.setMinimumWidth(getTabMinWidth());
        return fVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m3431c() {
        mo1772b();
        if (this.f2571x != null) {
            int count = this.f2571x.getCount();
            for (int i = 0; i < count; i++) {
                mo1767a(mo1764a().mo1810a(this.f2571x.getPageTitle(i)), false);
            }
            if (this.f2570w != null && count > 0) {
                int currentItem = this.f2570w.getCurrentItem();
                if (currentItem != getSelectedTabPosition() && currentItem < getTabCount()) {
                    mo1773b(mo1765a(currentItem));
                    return;
                }
                return;
            }
            return;
        }
        mo1772b();
    }

    /* renamed from: c */
    private void m3432c(int i) {
        C0418f fVar = (C0418f) this.f2551d.getChildAt(i);
        this.f2551d.removeViewAt(i);
        if (fVar != null) {
            fVar.m3496b();
            this.f2548A.mo13296a(fVar);
        }
        requestLayout();
    }

    /* renamed from: c */
    private void m3433c(C0416d dVar, boolean z) {
        C0418f d = dVar.f2598h;
        this.f2551d.addView(d, m3438e());
        if (z) {
            d.setSelected(true);
        }
    }

    /* renamed from: d */
    private void m3435d() {
        int size = this.f2549b.size();
        for (int i = 0; i < size; i++) {
            ((C0416d) this.f2549b.get(i)).m3476g();
        }
    }

    /* renamed from: d */
    private void m3436d(int i) {
        if (i != -1) {
            if (getWindowToken() == null || !ViewCompat.m12877F(this) || this.f2551d.mo1801a()) {
                setScrollPosition(i, 0.0f, true);
                return;
            }
            int scrollX = getScrollX();
            int a = m3413a(i, 0.0f);
            if (scrollX != a) {
                if (this.f2569v == null) {
                    this.f2569v = ViewUtils.m1925a();
                    this.f2569v.mo141a(AnimationUtils.f10964b);
                    this.f2569v.mo137a(300);
                    this.f2569v.mo140a((C0046c) new C0046c() {
                        /* renamed from: a */
                        public void mo155a(ValueAnimatorCompat abVar) {
                            TabLayout.this.scrollTo(abVar.mo143c(), 0);
                        }
                    });
                }
                this.f2569v.mo138a(scrollX, a);
                this.f2569v.mo135a();
            }
            this.f2551d.mo1800a(i, 300);
        }
    }

    /* renamed from: e */
    private LayoutParams m3438e() {
        LayoutParams layoutParams = new LayoutParams(-2, -1);
        m3423a(layoutParams);
        return layoutParams;
    }

    /* renamed from: f */
    private void m3440f() {
        int i = 0;
        if (this.f2567t == 0) {
            i = Math.max(0, this.f2565r - this.f2552e);
        }
        ViewCompat.m12905b(this.f2551d, i, 0, 0, 0);
        switch (this.f2567t) {
            case 0:
                this.f2551d.setGravity(8388611);
                break;
            case 1:
                this.f2551d.setGravity(1);
                break;
        }
        m3425a(true);
    }

    private int getDefaultHeight() {
        boolean z = false;
        int i = 0;
        int size = this.f2549b.size();
        while (true) {
            if (i >= size) {
                break;
            }
            C0416d dVar = (C0416d) this.f2549b.get(i);
            if (dVar != null && dVar.mo1812b() != null && !TextUtils.isEmpty(dVar.mo1815d())) {
                z = true;
                break;
            }
            i++;
        }
        return z ? 72 : 48;
    }

    private float getScrollPosition() {
        return this.f2551d.mo1802b();
    }

    /* access modifiers changed from: private */
    public int getTabMaxWidth() {
        return this.f2561n;
    }

    private int getTabMinWidth() {
        if (this.f2562o != -1) {
            return this.f2562o;
        }
        if (this.f2567t == 0) {
            return this.f2564q;
        }
        return 0;
    }

    private int getTabScrollRange() {
        return Math.max(0, ((this.f2551d.getWidth() - getWidth()) - getPaddingLeft()) - getPaddingRight());
    }

    private void setSelectedTabView(int i) {
        int childCount = this.f2551d.getChildCount();
        if (i < childCount && !this.f2551d.getChildAt(i).isSelected()) {
            int i2 = 0;
            while (i2 < childCount) {
                this.f2551d.getChildAt(i2).setSelected(i2 == i);
                i2++;
            }
        }
    }

    /* renamed from: a */
    public C0416d mo1764a() {
        C0416d dVar = (C0416d) f2547a.mo13295a();
        if (dVar == null) {
            dVar = new C0416d();
        }
        dVar.f2597g = this;
        dVar.f2598h = m3430c(dVar);
        return dVar;
    }

    /* renamed from: a */
    public C0416d mo1765a(int i) {
        return (C0416d) this.f2549b.get(i);
    }

    /* renamed from: a */
    public void mo1766a(C0416d dVar) {
        mo1767a(dVar, this.f2549b.isEmpty());
    }

    /* renamed from: a */
    public void mo1767a(C0416d dVar, boolean z) {
        if (dVar.f2597g != this) {
            throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
        }
        m3433c(dVar, z);
        m3419a(dVar, this.f2549b.size());
        if (z) {
            dVar.mo1816e();
        }
    }

    public void addView(View view) {
        m3422a(view);
    }

    public void addView(View view, int i) {
        m3422a(view);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        m3422a(view);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        m3422a(view);
    }

    /* renamed from: b */
    public void mo1772b() {
        for (int childCount = this.f2551d.getChildCount() - 1; childCount >= 0; childCount--) {
            m3432c(childCount);
        }
        Iterator it = this.f2549b.iterator();
        while (it.hasNext()) {
            C0416d dVar = (C0416d) it.next();
            it.remove();
            dVar.m3477h();
            f2547a.mo13296a(dVar);
        }
        this.f2550c = null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo1773b(C0416d dVar) {
        mo1774b(dVar, true);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo1774b(C0416d dVar, boolean z) {
        if (this.f2550c != dVar) {
            if (z) {
                int i = dVar != null ? dVar.mo1814c() : -1;
                if (i != -1) {
                    setSelectedTabView(i);
                }
                if ((this.f2550c == null || this.f2550c.mo1814c() == -1) && i != -1) {
                    setScrollPosition(i, 0.0f, true);
                } else {
                    m3436d(i);
                }
            }
            if (!(this.f2550c == null || this.f2568u == null)) {
                this.f2568u.onTabUnselected(this.f2550c);
            }
            this.f2550c = dVar;
            if (this.f2550c != null && this.f2568u != null) {
                this.f2568u.onTabSelected(this.f2550c);
            }
        } else if (this.f2550c != null) {
            if (this.f2568u != null) {
                this.f2568u.onTabReselected(this.f2550c);
            }
            m3436d(dVar.mo1814c());
        }
    }

    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return generateDefaultLayoutParams();
    }

    public int getSelectedTabPosition() {
        if (this.f2550c != null) {
            return this.f2550c.mo1814c();
        }
        return -1;
    }

    public int getTabCount() {
        return this.f2549b.size();
    }

    public int getTabGravity() {
        return this.f2566s;
    }

    public int getTabMode() {
        return this.f2567t;
    }

    public ColorStateList getTabTextColors() {
        return this.f2557j;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int b = m3426b(getDefaultHeight()) + getPaddingTop() + getPaddingBottom();
        switch (MeasureSpec.getMode(i2)) {
            case Integer.MIN_VALUE:
                i2 = MeasureSpec.makeMeasureSpec(Math.min(b, MeasureSpec.getSize(i2)), 1073741824);
                break;
            case 0:
                i2 = MeasureSpec.makeMeasureSpec(b, 1073741824);
                break;
        }
        int size = MeasureSpec.getSize(i);
        if (MeasureSpec.getMode(i) != 0) {
            this.f2561n = this.f2563p > 0 ? this.f2563p : size - m3426b(56);
        }
        super.onMeasure(i, i2);
        if (getChildCount() == 1) {
            View childAt = getChildAt(0);
            boolean z = false;
            switch (this.f2567t) {
                case 0:
                    if (childAt.getMeasuredWidth() >= getMeasuredWidth()) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case 1:
                    if (childAt.getMeasuredWidth() == getMeasuredWidth()) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
            }
            if (z) {
                childAt.measure(MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), getChildMeasureSpec(i2, getPaddingTop() + getPaddingBottom(), childAt.getLayoutParams().height));
            }
        }
    }

    public void setOnTabSelectedListener(C0411a aVar) {
        this.f2568u = aVar;
    }

    public void setScrollPosition(int i, float f, boolean z) {
        m3417a(i, f, z, true);
    }

    public void setSelectedTabIndicatorColor(int i) {
        this.f2551d.mo1798a(i);
    }

    public void setSelectedTabIndicatorHeight(int i) {
        this.f2551d.mo1803b(i);
    }

    public void setTabGravity(int i) {
        if (this.f2566s != i) {
            this.f2566s = i;
            m3440f();
        }
    }

    public void setTabMode(int i) {
        if (i != this.f2567t) {
            this.f2567t = i;
            m3440f();
        }
    }

    public void setTabTextColors(int i, int i2) {
        setTabTextColors(m3416a(i, i2));
    }

    public void setTabTextColors(ColorStateList colorStateList) {
        if (this.f2557j != colorStateList) {
            this.f2557j = colorStateList;
            m3435d();
        }
    }

    @Deprecated
    public void setTabsFromPagerAdapter(PagerAdapter evVar) {
        m3424a(evVar, false);
    }

    public void setupWithViewPager(ViewPager viewPager) {
        if (!(this.f2570w == null || this.f2573z == null)) {
            this.f2570w.mo2894b((C0497e) this.f2573z);
        }
        if (viewPager != null) {
            PagerAdapter adapter = viewPager.getAdapter();
            if (adapter == null) {
                throw new IllegalArgumentException("ViewPager does not have a PagerAdapter set");
            }
            this.f2570w = viewPager;
            if (this.f2573z == null) {
                this.f2573z = new C0417e(this);
            }
            this.f2573z.m3489a();
            viewPager.mo2884a((C0497e) this.f2573z);
            setOnTabSelectedListener(new C0419g(viewPager));
            m3424a(adapter, true);
            return;
        }
        this.f2570w = null;
        setOnTabSelectedListener(null);
        m3424a((PagerAdapter) null, true);
    }

    public boolean shouldDelayChildPressedState() {
        return getTabScrollRange() > 0;
    }
}
