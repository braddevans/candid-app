package android.support.design.widget;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.util.AttributeSet;
import android.view.View;

class ViewOffsetBehavior<V extends View> extends Behavior<V> {

    /* renamed from: a */
    private ViewOffsetHelper f2641a;

    /* renamed from: b */
    private int f2642b = 0;

    /* renamed from: c */
    private int f2643c = 0;

    public ViewOffsetBehavior() {
    }

    public ViewOffsetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* renamed from: a */
    public boolean mo1465a(int i) {
        if (this.f2641a != null) {
            return this.f2641a.mo753a(i);
        }
        this.f2642b = i;
        return false;
    }

    /* renamed from: a */
    public boolean mo1471a(CoordinatorLayout coordinatorLayout, V v, int i) {
        mo1694b(coordinatorLayout, v, i);
        if (this.f2641a == null) {
            this.f2641a = new ViewOffsetHelper(v);
        }
        this.f2641a.mo752a();
        if (this.f2642b != 0) {
            this.f2641a.mo753a(this.f2642b);
            this.f2642b = 0;
        }
        if (this.f2643c != 0) {
            this.f2641a.mo755b(this.f2643c);
            this.f2643c = 0;
        }
        return true;
    }

    /* renamed from: b */
    public int mo1475b() {
        if (this.f2641a != null) {
            return this.f2641a.mo754b();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1694b(CoordinatorLayout coordinatorLayout, V v, int i) {
        coordinatorLayout.mo1570a((View) v, i);
    }
}
