package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.support.design.widget.CoordinatorLayout.C0382d;
import android.support.design.widget.SwipeDismissBehavior.C0408a;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public final class Snackbar {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final Handler f2502a = new Handler(Looper.getMainLooper(), new Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ((Snackbar) message.obj).mo1724c();
                    return true;
                case 1:
                    ((Snackbar) message.obj).mo1722b(message.arg1);
                    return true;
                default:
                    return false;
            }
        }
    });

    /* renamed from: b */
    private final ViewGroup f2503b;

    /* renamed from: c */
    private final Context f2504c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public final SnackbarLayout f2505d;

    /* renamed from: e */
    private int f2506e;

    /* renamed from: f */
    private C0406b f2507f;

    /* renamed from: g */
    private final AccessibilityManager f2508g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final C3087a f2509h = new C3087a() {
        /* renamed from: a */
        public void mo1729a() {
            Snackbar.f2502a.sendMessage(Snackbar.f2502a.obtainMessage(0, Snackbar.this));
        }

        /* renamed from: a */
        public void mo1730a(int i) {
            Snackbar.f2502a.sendMessage(Snackbar.f2502a.obtainMessage(1, i, 0, Snackbar.this));
        }
    };

    public static class SnackbarLayout extends LinearLayout {

        /* renamed from: a */
        private TextView f2521a;

        /* renamed from: b */
        private Button f2522b;

        /* renamed from: c */
        private int f2523c;

        /* renamed from: d */
        private int f2524d;

        /* renamed from: e */
        private C0404b f2525e;

        /* renamed from: f */
        private C0403a f2526f;

        /* renamed from: android.support.design.widget.Snackbar$SnackbarLayout$a */
        interface C0403a {
            /* renamed from: a */
            void mo1733a(View view);

            /* renamed from: b */
            void mo1734b(View view);
        }

        /* renamed from: android.support.design.widget.Snackbar$SnackbarLayout$b */
        interface C0404b {
            /* renamed from: a */
            void mo1736a(View view, int i, int i2, int i3, int i4);
        }

        public SnackbarLayout(Context context) {
            this(context, null);
        }

        public SnackbarLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.SnackbarLayout);
            this.f2523c = obtainStyledAttributes.getDimensionPixelSize(C2394i.SnackbarLayout_android_maxWidth, -1);
            this.f2524d = obtainStyledAttributes.getDimensionPixelSize(C2394i.SnackbarLayout_maxActionInlineWidth, -1);
            if (obtainStyledAttributes.hasValue(C2394i.SnackbarLayout_elevation)) {
                ViewCompat.m12920f((View) this, (float) obtainStyledAttributes.getDimensionPixelSize(C2394i.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            setClickable(true);
            LayoutInflater.from(context).inflate(C2391f.design_layout_snackbar_include, this);
            ViewCompat.m12915d((View) this, 1);
            ViewCompat.m12910c((View) this, 1);
        }

        /* renamed from: a */
        private static void m3379a(View view, int i, int i2) {
            if (ViewCompat.m12872A(view)) {
                ViewCompat.m12905b(view, ViewCompat.m12928m(view), i, ViewCompat.m12929n(view), i2);
            } else {
                view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), i2);
            }
        }

        /* renamed from: a */
        private boolean m3380a(int i, int i2, int i3) {
            boolean z = false;
            if (i != getOrientation()) {
                setOrientation(i);
                z = true;
            }
            if (this.f2521a.getPaddingTop() == i2 && this.f2521a.getPaddingBottom() == i3) {
                return z;
            }
            m3379a((View) this.f2521a, i2, i3);
            return true;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo1742a(int i, int i2) {
            ViewCompat.m12909c((View) this.f2521a, 0.0f);
            ViewCompat.m12934s(this.f2521a).mo13535a(1.0f).mo13536a((long) i2).mo13541b((long) i).mo13544c();
            if (this.f2522b.getVisibility() == 0) {
                ViewCompat.m12909c((View) this.f2522b, 0.0f);
                ViewCompat.m12934s(this.f2522b).mo13535a(1.0f).mo13536a((long) i2).mo13541b((long) i).mo13544c();
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void mo1743b(int i, int i2) {
            ViewCompat.m12909c((View) this.f2521a, 1.0f);
            ViewCompat.m12934s(this.f2521a).mo13535a(0.0f).mo13536a((long) i2).mo13541b((long) i).mo13544c();
            if (this.f2522b.getVisibility() == 0) {
                ViewCompat.m12909c((View) this.f2522b, 1.0f);
                ViewCompat.m12934s(this.f2522b).mo13535a(0.0f).mo13536a((long) i2).mo13541b((long) i).mo13544c();
            }
        }

        /* access modifiers changed from: 0000 */
        public Button getActionView() {
            return this.f2522b;
        }

        /* access modifiers changed from: 0000 */
        public TextView getMessageView() {
            return this.f2521a;
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            if (this.f2526f != null) {
                this.f2526f.mo1733a(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            if (this.f2526f != null) {
                this.f2526f.mo1734b(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            this.f2521a = (TextView) findViewById(C2390e.snackbar_text);
            this.f2522b = (Button) findViewById(C2390e.snackbar_action);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            if (this.f2525e != null) {
                this.f2525e.mo1736a(this, i, i2, i3, i4);
            }
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (this.f2523c > 0 && getMeasuredWidth() > this.f2523c) {
                i = MeasureSpec.makeMeasureSpec(this.f2523c, 1073741824);
                super.onMeasure(i, i2);
            }
            int dimensionPixelSize = getResources().getDimensionPixelSize(C2389d.design_snackbar_padding_vertical_2lines);
            int dimensionPixelSize2 = getResources().getDimensionPixelSize(C2389d.design_snackbar_padding_vertical);
            boolean z = this.f2521a.getLayout().getLineCount() > 1;
            boolean z2 = false;
            if (!z || this.f2524d <= 0 || this.f2522b.getMeasuredWidth() <= this.f2524d) {
                int i3 = z ? dimensionPixelSize : dimensionPixelSize2;
                if (m3380a(0, i3, i3)) {
                    z2 = true;
                }
            } else if (m3380a(1, dimensionPixelSize, dimensionPixelSize - dimensionPixelSize2)) {
                z2 = true;
            }
            if (z2) {
                super.onMeasure(i, i2);
            }
        }

        /* access modifiers changed from: 0000 */
        public void setOnAttachStateChangeListener(C0403a aVar) {
            this.f2526f = aVar;
        }

        /* access modifiers changed from: 0000 */
        public void setOnLayoutChangeListener(C0404b bVar) {
            this.f2525e = bVar;
        }
    }

    /* renamed from: android.support.design.widget.Snackbar$a */
    final class C0405a extends SwipeDismissBehavior<SnackbarLayout> {
        C0405a() {
        }

        /* renamed from: a */
        public boolean mo1496a(CoordinatorLayout coordinatorLayout, SnackbarLayout snackbarLayout, MotionEvent motionEvent) {
            if (coordinatorLayout.mo1576a((View) snackbarLayout, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                switch (motionEvent.getActionMasked()) {
                    case 0:
                        SnackbarManager.m17094a().mo16823c(Snackbar.this.f2509h);
                        break;
                    case 1:
                    case 3:
                        SnackbarManager.m17094a().mo16824d(Snackbar.this.f2509h);
                        break;
                }
            }
            return super.mo1496a(coordinatorLayout, snackbarLayout, motionEvent);
        }

        /* renamed from: a */
        public boolean mo1754a(View view) {
            return view instanceof SnackbarLayout;
        }
    }

    /* renamed from: android.support.design.widget.Snackbar$b */
    public static abstract class C0406b {
        /* renamed from: a */
        public void mo1755a(Snackbar snackbar) {
        }

        /* renamed from: a */
        public void mo1756a(Snackbar snackbar, int i) {
        }
    }

    private Snackbar(ViewGroup viewGroup) {
        this.f2503b = viewGroup;
        this.f2504c = viewGroup.getContext();
        ThemeUtils.m0a(this.f2504c);
        this.f2505d = (SnackbarLayout) LayoutInflater.from(this.f2504c).inflate(C2391f.design_layout_snackbar, this.f2503b, false);
        this.f2508g = (AccessibilityManager) this.f2504c.getSystemService("accessibility");
    }

    /* renamed from: a */
    public static Snackbar m3350a(View view, CharSequence charSequence, int i) {
        Snackbar snackbar = new Snackbar(m3351a(view));
        snackbar.mo1720a(charSequence);
        snackbar.mo1719a(i);
        return snackbar;
    }

    /* renamed from: a */
    private static ViewGroup m3351a(View view) {
        ViewGroup viewGroup = null;
        while (!(view instanceof CoordinatorLayout)) {
            if (view instanceof FrameLayout) {
                if (view.getId() == 16908290) {
                    return (ViewGroup) view;
                }
                viewGroup = (ViewGroup) view;
            }
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent instanceof View) {
                    view = (View) parent;
                    continue;
                } else {
                    view = null;
                    continue;
                }
            }
            if (view == null) {
                return viewGroup;
            }
        }
        return (ViewGroup) view;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m3356c(int i) {
        SnackbarManager.m17094a().mo16821a(this.f2509h, i);
    }

    /* renamed from: d */
    private void m3359d(final int i) {
        if (VERSION.SDK_INT >= 14) {
            ViewCompat.m12934s(this.f2505d).mo13543c((float) this.f2505d.getHeight()).mo13537a(AnimationUtils.f10964b).mo13536a(250).mo13538a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationEnd(View view) {
                    Snackbar.this.m3362e(i);
                }

                public void onAnimationStart(View view) {
                    Snackbar.this.f2505d.mo1743b(0, 180);
                }
            }).mo13544c();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f2505d.getContext(), C2386a.design_snackbar_out);
        loadAnimation.setInterpolator(AnimationUtils.f10964b);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                Snackbar.this.m3362e(i);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.f2505d.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m3361e() {
        if (VERSION.SDK_INT >= 14) {
            ViewCompat.m12904b((View) this.f2505d, (float) this.f2505d.getHeight());
            ViewCompat.m12934s(this.f2505d).mo13543c(0.0f).mo13537a(AnimationUtils.f10964b).mo13536a(250).mo13538a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationEnd(View view) {
                    Snackbar.this.m3364f();
                }

                public void onAnimationStart(View view) {
                    Snackbar.this.f2505d.mo1742a(70, 180);
                }
            }).mo13544c();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f2505d.getContext(), C2386a.design_snackbar_in);
        loadAnimation.setInterpolator(AnimationUtils.f10964b);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                Snackbar.this.m3364f();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.f2505d.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m3362e(int i) {
        SnackbarManager.m17094a().mo16820a(this.f2509h);
        if (this.f2507f != null) {
            this.f2507f.mo1756a(this, i);
        }
        ViewParent parent = this.f2505d.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.f2505d);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m3364f() {
        SnackbarManager.m17094a().mo16822b(this.f2509h);
        if (this.f2507f != null) {
            this.f2507f.mo1755a(this);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public boolean m3365g() {
        return !this.f2508g.isEnabled();
    }

    /* renamed from: a */
    public Snackbar mo1719a(int i) {
        this.f2506e = i;
        return this;
    }

    /* renamed from: a */
    public Snackbar mo1720a(CharSequence charSequence) {
        this.f2505d.getMessageView().setText(charSequence);
        return this;
    }

    /* renamed from: a */
    public void mo1721a() {
        SnackbarManager.m17094a().mo16819a(this.f2506e, this.f2509h);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public final void mo1722b(int i) {
        if (!m3365g() || this.f2505d.getVisibility() != 0) {
            m3362e(i);
        } else {
            m3359d(i);
        }
    }

    /* renamed from: b */
    public boolean mo1723b() {
        return SnackbarManager.m17094a().mo16825e(this.f2509h);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public final void mo1724c() {
        if (this.f2505d.getParent() == null) {
            LayoutParams layoutParams = this.f2505d.getLayoutParams();
            if (layoutParams instanceof C0382d) {
                C0405a aVar = new C0405a();
                aVar.mo1757a(0.1f);
                aVar.mo1760b(0.6f);
                aVar.mo1758a(0);
                aVar.mo1759a((C0408a) new C0408a() {
                    /* renamed from: a */
                    public void mo1731a(int i) {
                        switch (i) {
                            case 0:
                                SnackbarManager.m17094a().mo16824d(Snackbar.this.f2509h);
                                return;
                            case 1:
                            case 2:
                                SnackbarManager.m17094a().mo16823c(Snackbar.this.f2509h);
                                return;
                            default:
                                return;
                        }
                    }

                    /* renamed from: a */
                    public void mo1732a(View view) {
                        view.setVisibility(8);
                        Snackbar.this.m3356c(0);
                    }
                });
                ((C0382d) layoutParams).mo1637a((Behavior) aVar);
            }
            this.f2503b.addView(this.f2505d);
        }
        this.f2505d.setOnAttachStateChangeListener(new C0403a() {
            /* renamed from: a */
            public void mo1733a(View view) {
            }

            /* renamed from: b */
            public void mo1734b(View view) {
                if (Snackbar.this.mo1723b()) {
                    Snackbar.f2502a.post(new Runnable() {
                        public void run() {
                            Snackbar.this.m3362e(3);
                        }
                    });
                }
            }
        });
        if (!ViewCompat.m12877F(this.f2505d)) {
            this.f2505d.setOnLayoutChangeListener(new C0404b() {
                /* renamed from: a */
                public void mo1736a(View view, int i, int i2, int i3, int i4) {
                    Snackbar.this.f2505d.setOnLayoutChangeListener(null);
                    if (Snackbar.this.m3365g()) {
                        Snackbar.this.m3361e();
                    } else {
                        Snackbar.this.m3364f();
                    }
                }
            });
        } else if (m3365g()) {
            m3361e();
        } else {
            m3364f();
        }
    }
}
