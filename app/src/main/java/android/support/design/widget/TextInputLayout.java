package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.DrawableContainer.DrawableContainerState;
import android.graphics.drawable.InsetDrawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p001v4.widget.Space;
import android.support.p003v7.widget.AppCompatDrawableManager;
import android.support.p003v7.widget.DrawableUtils;
import android.support.p003v7.widget.RecyclerView.ItemAnimator;
import android.support.p003v7.widget.helper.ItemTouchHelper.Callback;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class TextInputLayout extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public EditText f2611a;

    /* renamed from: b */
    private boolean f2612b;

    /* renamed from: c */
    private CharSequence f2613c;

    /* renamed from: d */
    private Paint f2614d;

    /* renamed from: e */
    private LinearLayout f2615e;

    /* renamed from: f */
    private int f2616f;

    /* renamed from: g */
    private boolean f2617g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public TextView f2618h;

    /* renamed from: i */
    private int f2619i;

    /* renamed from: j */
    private boolean f2620j;

    /* renamed from: k */
    private CharSequence f2621k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public boolean f2622l;

    /* renamed from: m */
    private TextView f2623m;

    /* renamed from: n */
    private int f2624n;

    /* renamed from: o */
    private int f2625o;

    /* renamed from: p */
    private int f2626p;

    /* renamed from: q */
    private boolean f2627q;

    /* renamed from: r */
    private ColorStateList f2628r;

    /* renamed from: s */
    private ColorStateList f2629s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public final CollapsingTextHelper f2630t;

    /* renamed from: u */
    private boolean f2631u;

    /* renamed from: v */
    private ValueAnimatorCompat f2632v;

    /* renamed from: w */
    private boolean f2633w;

    static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a */
        CharSequence f2639a;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f2639a = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "TextInputLayout.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " error=" + this.f2639a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            TextUtils.writeToParcel(this.f2639a, parcel, i);
        }
    }

    /* renamed from: android.support.design.widget.TextInputLayout$a */
    class C0425a extends AccessibilityDelegateCompat {
        private C0425a() {
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(TextInputLayout.class.getSimpleName());
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat ggVar) {
            super.onInitializeAccessibilityNodeInfo(view, ggVar);
            ggVar.mo13595b((CharSequence) TextInputLayout.class.getSimpleName());
            CharSequence i = TextInputLayout.this.f2630t.mo14853i();
            if (!TextUtils.isEmpty(i)) {
                ggVar.mo13601c(i);
            }
            if (TextInputLayout.this.f2611a != null) {
                ggVar.mo13610e((View) TextInputLayout.this.f2611a);
            }
            CharSequence charSequence = TextInputLayout.this.f2618h != null ? TextInputLayout.this.f2618h.getText() : null;
            if (!TextUtils.isEmpty(charSequence)) {
                ggVar.mo13623j(true);
                ggVar.mo13611e(charSequence);
            }
        }

        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onPopulateAccessibilityEvent(view, accessibilityEvent);
            CharSequence i = TextInputLayout.this.f2630t.mo14853i();
            if (!TextUtils.isEmpty(i)) {
                accessibilityEvent.getText().add(i);
            }
        }
    }

    public TextInputLayout(Context context) {
        this(context, null);
    }

    public TextInputLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.f2630t = new CollapsingTextHelper(this);
        ThemeUtils.m0a(context);
        setOrientation(1);
        setWillNotDraw(false);
        setAddStatesFromChildren(true);
        this.f2630t.mo14834a(AnimationUtils.f10964b);
        this.f2630t.mo14841b((Interpolator) new AccelerateInterpolator());
        this.f2630t.mo14846d(8388659);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C2394i.TextInputLayout, i, C2393h.Widget_Design_TextInputLayout);
        this.f2612b = obtainStyledAttributes.getBoolean(C2394i.TextInputLayout_hintEnabled, true);
        setHint(obtainStyledAttributes.getText(C2394i.TextInputLayout_android_hint));
        this.f2631u = obtainStyledAttributes.getBoolean(C2394i.TextInputLayout_hintAnimationEnabled, true);
        if (obtainStyledAttributes.hasValue(C2394i.TextInputLayout_android_textColorHint)) {
            ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(C2394i.TextInputLayout_android_textColorHint);
            this.f2629s = colorStateList;
            this.f2628r = colorStateList;
        }
        if (obtainStyledAttributes.getResourceId(C2394i.TextInputLayout_hintTextAppearance, -1) != -1) {
            setHintTextAppearance(obtainStyledAttributes.getResourceId(C2394i.TextInputLayout_hintTextAppearance, 0));
        }
        this.f2619i = obtainStyledAttributes.getResourceId(C2394i.TextInputLayout_errorTextAppearance, 0);
        boolean z = obtainStyledAttributes.getBoolean(C2394i.TextInputLayout_errorEnabled, false);
        boolean z2 = obtainStyledAttributes.getBoolean(C2394i.TextInputLayout_counterEnabled, false);
        setCounterMaxLength(obtainStyledAttributes.getInt(C2394i.TextInputLayout_counterMaxLength, -1));
        this.f2625o = obtainStyledAttributes.getResourceId(C2394i.TextInputLayout_counterTextAppearance, 0);
        this.f2626p = obtainStyledAttributes.getResourceId(C2394i.TextInputLayout_counterOverflowTextAppearance, 0);
        obtainStyledAttributes.recycle();
        setErrorEnabled(z);
        setCounterEnabled(z2);
        if (ViewCompat.m12916e(this) == 0) {
            ViewCompat.m12910c((View) this, 1);
        }
        ViewCompat.m12894a((View) this, (AccessibilityDelegateCompat) new C0425a());
    }

    /* renamed from: a */
    private LayoutParams m3498a(ViewGroup.LayoutParams layoutParams) {
        LayoutParams layoutParams2 = layoutParams instanceof LayoutParams ? (LayoutParams) layoutParams : new LayoutParams(layoutParams);
        if (this.f2612b) {
            if (this.f2614d == null) {
                this.f2614d = new Paint();
            }
            this.f2614d.setTypeface(this.f2630t.mo14845d());
            this.f2614d.setTextSize(this.f2630t.mo14851g());
            layoutParams2.topMargin = (int) (-this.f2614d.ascent());
        } else {
            layoutParams2.topMargin = 0;
        }
        return layoutParams2;
    }

    /* renamed from: a */
    private void m3499a() {
        ViewCompat.m12905b(this.f2615e, ViewCompat.m12928m(this.f2611a), 0, ViewCompat.m12929n(this.f2611a), this.f2611a.getPaddingBottom());
    }

    /* renamed from: a */
    private void m3500a(float f) {
        if (this.f2630t.mo14849f() != f) {
            if (this.f2632v == null) {
                this.f2632v = ViewUtils.m1925a();
                this.f2632v.mo141a(AnimationUtils.f10963a);
                this.f2632v.mo137a((int) Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                this.f2632v.mo140a((C0046c) new C0046c() {
                    /* renamed from: a */
                    public void mo155a(ValueAnimatorCompat abVar) {
                        TextInputLayout.this.f2630t.mo14837b(abVar.mo144d());
                    }
                });
            }
            this.f2632v.mo136a(this.f2630t.mo14849f(), f);
            this.f2632v.mo135a();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3501a(int i) {
        boolean z = this.f2627q;
        if (this.f2624n == -1) {
            this.f2623m.setText(String.valueOf(i));
            this.f2627q = false;
        } else {
            this.f2627q = i > this.f2624n;
            if (z != this.f2627q) {
                this.f2623m.setTextAppearance(getContext(), this.f2627q ? this.f2626p : this.f2625o);
            }
            this.f2623m.setText(getContext().getString(C2392g.character_counter_pattern, new Object[]{Integer.valueOf(i), Integer.valueOf(this.f2624n)}));
        }
        if (this.f2611a != null && z != this.f2627q) {
            m3507a(false);
            m3511b();
        }
    }

    /* renamed from: a */
    private static void m3502a(Drawable drawable) {
        drawable.clearColorFilter();
        if (VERSION.SDK_INT != 21 && VERSION.SDK_INT != 22) {
            return;
        }
        if (drawable instanceof InsetDrawable) {
            m3502a(((InsetDrawable) drawable).getDrawable());
        } else if (drawable instanceof DrawableWrapper) {
            m3502a(((DrawableWrapper) drawable).mo9656a());
        } else if (drawable instanceof DrawableContainer) {
            DrawableContainerState drawableContainerState = (DrawableContainerState) ((DrawableContainer) drawable).getConstantState();
            if (drawableContainerState != null) {
                int childCount = drawableContainerState.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    m3502a(drawableContainerState.getChild(i));
                }
            }
        }
    }

    /* renamed from: a */
    private void m3505a(TextView textView) {
        if (this.f2615e != null) {
            this.f2615e.removeView(textView);
            int i = this.f2616f - 1;
            this.f2616f = i;
            if (i == 0) {
                this.f2615e.setVisibility(8);
            }
        }
    }

    /* renamed from: a */
    private void m3506a(TextView textView, int i) {
        if (this.f2615e == null) {
            this.f2615e = new LinearLayout(getContext());
            this.f2615e.setOrientation(0);
            addView(this.f2615e, -1, -2);
            this.f2615e.addView(new Space(getContext()), new LayoutParams(0, 0, 1.0f));
            if (this.f2611a != null) {
                m3499a();
            }
        }
        this.f2615e.setVisibility(0);
        this.f2615e.addView(textView, i);
        this.f2616f++;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3507a(boolean z) {
        boolean z2 = this.f2611a != null && !TextUtils.isEmpty(this.f2611a.getText());
        boolean a = m3509a(getDrawableState(), 16842908);
        boolean z3 = !TextUtils.isEmpty(getError());
        if (this.f2628r != null) {
            this.f2630t.mo14838b(this.f2628r.getDefaultColor());
        }
        if (this.f2627q && this.f2623m != null) {
            this.f2630t.mo14830a(this.f2623m.getCurrentTextColor());
        } else if (a && this.f2629s != null) {
            this.f2630t.mo14830a(this.f2629s.getDefaultColor());
        } else if (this.f2628r != null) {
            this.f2630t.mo14830a(this.f2628r.getDefaultColor());
        }
        if (z2 || a || z3) {
            m3512b(z);
        } else {
            m3515c(z);
        }
    }

    /* renamed from: a */
    private static boolean m3509a(int[] iArr, int i) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: b */
    private void m3511b() {
        m3514c();
        Drawable background = this.f2611a.getBackground();
        if (background != null) {
            if (DrawableUtils.canSafelyMutateDrawable(background)) {
                background = background.mutate();
            }
            if (this.f2620j && this.f2618h != null) {
                background.setColorFilter(AppCompatDrawableManager.getPorterDuffColorFilter(this.f2618h.getCurrentTextColor(), Mode.SRC_IN));
            } else if (!this.f2627q || this.f2623m == null) {
                m3502a(background);
                this.f2611a.refreshDrawableState();
            } else {
                background.setColorFilter(AppCompatDrawableManager.getPorterDuffColorFilter(this.f2623m.getCurrentTextColor(), Mode.SRC_IN));
            }
        }
    }

    /* renamed from: b */
    private void m3512b(boolean z) {
        if (this.f2632v != null && this.f2632v.mo142b()) {
            this.f2632v.mo145e();
        }
        if (!z || !this.f2631u) {
            this.f2630t.mo14837b(1.0f);
        } else {
            m3500a(1.0f);
        }
    }

    /* renamed from: c */
    private void m3514c() {
        int i = VERSION.SDK_INT;
        if (i == 21 || i == 22) {
            Drawable background = this.f2611a.getBackground();
            if (background != null && !this.f2633w) {
                Drawable newDrawable = background.getConstantState().newDrawable();
                if (background instanceof DrawableContainer) {
                    this.f2633w = DrawableUtils.m15855a((DrawableContainer) background, newDrawable.getConstantState());
                }
                if (!this.f2633w) {
                    this.f2611a.setBackgroundDrawable(newDrawable);
                    this.f2633w = true;
                }
            }
        }
    }

    /* renamed from: c */
    private void m3515c(boolean z) {
        if (this.f2632v != null && this.f2632v.mo142b()) {
            this.f2632v.mo145e();
        }
        if (!z || !this.f2631u) {
            this.f2630t.mo14837b(0.0f);
        } else {
            m3500a(0.0f);
        }
    }

    private void setEditText(EditText editText) {
        if (this.f2611a != null) {
            throw new IllegalArgumentException("We already have an EditText, can only have one");
        }
        if (!(editText instanceof TextInputEditText)) {
            Log.i("TextInputLayout", "EditText added is not a TextInputEditText. Please switch to using that class instead.");
        }
        this.f2611a = editText;
        this.f2630t.mo14844c(this.f2611a.getTypeface());
        this.f2630t.mo14829a(this.f2611a.getTextSize());
        int gravity = this.f2611a.getGravity();
        this.f2630t.mo14846d((8388615 & gravity) | 48);
        this.f2630t.mo14843c(gravity);
        this.f2611a.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                TextInputLayout.this.m3507a(true);
                if (TextInputLayout.this.f2622l) {
                    TextInputLayout.this.m3501a(editable.length());
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        if (this.f2628r == null) {
            this.f2628r = this.f2611a.getHintTextColors();
        }
        if (this.f2612b && TextUtils.isEmpty(this.f2613c)) {
            setHint(this.f2611a.getHint());
            this.f2611a.setHint(null);
        }
        if (this.f2623m != null) {
            m3501a(this.f2611a.getText().length());
        }
        if (this.f2615e != null) {
            m3499a();
        }
        m3507a(false);
    }

    private void setHintInternal(CharSequence charSequence) {
        this.f2613c = charSequence;
        this.f2630t.mo14835a(charSequence);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof EditText) {
            setEditText((EditText) view);
            super.addView(view, 0, m3498a(layoutParams));
            return;
        }
        super.addView(view, i, layoutParams);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f2612b) {
            this.f2630t.mo14832a(canvas);
        }
    }

    public int getCounterMaxLength() {
        return this.f2624n;
    }

    public EditText getEditText() {
        return this.f2611a;
    }

    public CharSequence getError() {
        if (this.f2617g) {
            return this.f2621k;
        }
        return null;
    }

    public CharSequence getHint() {
        if (this.f2612b) {
            return this.f2613c;
        }
        return null;
    }

    public Typeface getTypeface() {
        return this.f2630t.mo14845d();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.f2612b && this.f2611a != null) {
            int left = this.f2611a.getLeft() + this.f2611a.getCompoundPaddingLeft();
            int right = this.f2611a.getRight() - this.f2611a.getCompoundPaddingRight();
            this.f2630t.mo14831a(left, this.f2611a.getTop() + this.f2611a.getCompoundPaddingTop(), right, this.f2611a.getBottom() - this.f2611a.getCompoundPaddingBottom());
            this.f2630t.mo14839b(left, getPaddingTop(), right, (i4 - i2) - getPaddingBottom());
            this.f2630t.mo14852h();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setError(savedState.f2639a);
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (this.f2620j) {
            savedState.f2639a = getError();
        }
        return savedState;
    }

    public void refreshDrawableState() {
        super.refreshDrawableState();
        m3507a(ViewCompat.m12877F(this));
    }

    public void setCounterEnabled(boolean z) {
        if (this.f2622l != z) {
            if (z) {
                this.f2623m = new TextView(getContext());
                this.f2623m.setMaxLines(1);
                try {
                    this.f2623m.setTextAppearance(getContext(), this.f2625o);
                } catch (Exception e) {
                    this.f2623m.setTextAppearance(getContext(), C2393h.TextAppearance_AppCompat_Caption);
                    this.f2623m.setTextColor(ContextCompat.getColor(getContext(), C2388c.design_textinput_error_color_light));
                }
                m3506a(this.f2623m, -1);
                if (this.f2611a == null) {
                    m3501a(0);
                } else {
                    m3501a(this.f2611a.getText().length());
                }
            } else {
                m3505a(this.f2623m);
                this.f2623m = null;
            }
            this.f2622l = z;
        }
    }

    public void setCounterMaxLength(int i) {
        if (this.f2624n != i) {
            if (i > 0) {
                this.f2624n = i;
            } else {
                this.f2624n = -1;
            }
            if (this.f2622l) {
                m3501a(this.f2611a == null ? 0 : this.f2611a.getText().length());
            }
        }
    }

    public void setError(final CharSequence charSequence) {
        if (!TextUtils.equals(this.f2621k, charSequence)) {
            this.f2621k = charSequence;
            if (!this.f2617g) {
                if (!TextUtils.isEmpty(charSequence)) {
                    setErrorEnabled(true);
                } else {
                    return;
                }
            }
            boolean F = ViewCompat.m12877F(this);
            this.f2620j = !TextUtils.isEmpty(charSequence);
            ViewCompat.m12934s(this.f2618h).mo13542b();
            if (this.f2620j) {
                this.f2618h.setText(charSequence);
                this.f2618h.setVisibility(0);
                if (F) {
                    if (ViewCompat.m12919f(this.f2618h) == 1.0f) {
                        ViewCompat.m12909c((View) this.f2618h, 0.0f);
                    }
                    ViewCompat.m12934s(this.f2618h).mo13535a(1.0f).mo13536a(200).mo13537a(AnimationUtils.f10966d).mo13538a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
                        public void onAnimationStart(View view) {
                            view.setVisibility(0);
                        }
                    }).mo13544c();
                }
            } else if (this.f2618h.getVisibility() == 0) {
                if (F) {
                    ViewCompat.m12934s(this.f2618h).mo13535a(0.0f).mo13536a(200).mo13537a(AnimationUtils.f10965c).mo13538a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
                        public void onAnimationEnd(View view) {
                            TextInputLayout.this.f2618h.setText(charSequence);
                            view.setVisibility(4);
                        }
                    }).mo13544c();
                } else {
                    this.f2618h.setVisibility(4);
                }
            }
            m3511b();
            m3507a(true);
        }
    }

    public void setErrorEnabled(boolean z) {
        if (this.f2617g != z) {
            if (this.f2618h != null) {
                ViewCompat.m12934s(this.f2618h).mo13542b();
            }
            if (z) {
                this.f2618h = new TextView(getContext());
                try {
                    this.f2618h.setTextAppearance(getContext(), this.f2619i);
                } catch (Exception e) {
                    this.f2618h.setTextAppearance(getContext(), C2393h.TextAppearance_AppCompat_Caption);
                    this.f2618h.setTextColor(ContextCompat.getColor(getContext(), C2388c.design_textinput_error_color_light));
                }
                this.f2618h.setVisibility(4);
                ViewCompat.m12915d((View) this.f2618h, 1);
                m3506a(this.f2618h, 0);
            } else {
                this.f2620j = false;
                m3511b();
                m3505a(this.f2618h);
                this.f2618h = null;
            }
            this.f2617g = z;
        }
    }

    public void setHint(CharSequence charSequence) {
        if (this.f2612b) {
            setHintInternal(charSequence);
            sendAccessibilityEvent(ItemAnimator.FLAG_MOVED);
        }
    }

    public void setHintAnimationEnabled(boolean z) {
        this.f2631u = z;
    }

    public void setHintEnabled(boolean z) {
        if (z != this.f2612b) {
            this.f2612b = z;
            CharSequence hint = this.f2611a.getHint();
            if (!this.f2612b) {
                if (!TextUtils.isEmpty(this.f2613c) && TextUtils.isEmpty(hint)) {
                    this.f2611a.setHint(this.f2613c);
                }
                setHintInternal(null);
            } else if (!TextUtils.isEmpty(hint)) {
                if (TextUtils.isEmpty(this.f2613c)) {
                    setHint(hint);
                }
                this.f2611a.setHint(null);
            }
            if (this.f2611a != null) {
                this.f2611a.setLayoutParams(m3498a(this.f2611a.getLayoutParams()));
            }
        }
    }

    public void setHintTextAppearance(int i) {
        this.f2630t.mo14848e(i);
        this.f2629s = ColorStateList.valueOf(this.f2630t.mo14854j());
        if (this.f2611a != null) {
            m3507a(false);
            this.f2611a.setLayoutParams(m3498a(this.f2611a.getLayoutParams()));
            this.f2611a.requestLayout();
        }
    }

    public void setTypeface(Typeface typeface) {
        this.f2630t.mo14844c(typeface);
    }
}
