package android.support.percent;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class PercentRelativeLayout extends RelativeLayout {

    /* renamed from: a */
    private final PercentLayoutHelper f2647a = new PercentLayoutHelper(this);

    public static class LayoutParams extends android.widget.RelativeLayout.LayoutParams implements C0779b {

        /* renamed from: a */
        private C0778a f2648a;

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f2648a = PercentLayoutHelper.m4861a(context, attributeSet);
        }

        /* renamed from: a */
        public C0778a mo1870a() {
            if (this.f2648a == null) {
                this.f2648a = new C0778a();
            }
            return this.f2648a;
        }

        /* access modifiers changed from: protected */
        public void setBaseAttributes(TypedArray typedArray, int i, int i2) {
            PercentLayoutHelper.m4862a(this, typedArray, i, i2);
        }
    }

    public PercentRelativeLayout(Context context) {
        super(context);
    }

    public PercentRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PercentRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    /* renamed from: a */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f2647a.mo7025a();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.f2647a.mo7026a(i, i2);
        super.onMeasure(i, i2);
        if (this.f2647a.mo7027b()) {
            super.onMeasure(i, i2);
        }
    }
}
