package android.support.customtabs;

import android.app.Service;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public abstract class CustomTabsService extends Service {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Map<IBinder, DeathRecipient> f2318a = new ArrayMap();

    /* renamed from: b */
    private C2338a f2319b = new C2338a() {
        /* renamed from: a */
        public Bundle mo1389a(String str, Bundle bundle) {
            return CustomTabsService.this.mo1383a(str, bundle);
        }

        /* renamed from: a */
        public boolean mo1390a(long j) {
            return CustomTabsService.this.mo1384a(j);
        }

        /* renamed from: a */
        public boolean mo1391a(ICustomTabsCallback eVar) {
            boolean z = false;
            final CustomTabsSessionToken dVar = new CustomTabsSessionToken(eVar);
            try {
                C03621 r0 = new DeathRecipient() {
                    public void binderDied() {
                        CustomTabsService.this.mo1385a(dVar);
                    }
                };
                synchronized (CustomTabsService.this.f2318a) {
                    eVar.asBinder().linkToDeath(r0, 0);
                    CustomTabsService.this.f2318a.put(eVar.asBinder(), r0);
                }
                return CustomTabsService.this.mo1388b(dVar);
            } catch (RemoteException e) {
                return z;
            }
        }

        /* renamed from: a */
        public boolean mo1392a(ICustomTabsCallback eVar, Uri uri, Bundle bundle, List<Bundle> list) {
            return CustomTabsService.this.mo1386a(new CustomTabsSessionToken(eVar), uri, bundle, list);
        }

        /* renamed from: a */
        public boolean mo1393a(ICustomTabsCallback eVar, Bundle bundle) {
            return CustomTabsService.this.mo1387a(new CustomTabsSessionToken(eVar), bundle);
        }
    };

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract Bundle mo1383a(String str, Bundle bundle);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract boolean mo1384a(long j);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo1385a(CustomTabsSessionToken dVar) {
        try {
            synchronized (this.f2318a) {
                IBinder a = dVar.mo13167a();
                a.unlinkToDeath((DeathRecipient) this.f2318a.get(a), 0);
                this.f2318a.remove(a);
            }
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract boolean mo1386a(CustomTabsSessionToken dVar, Uri uri, Bundle bundle, List<Bundle> list);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract boolean mo1387a(CustomTabsSessionToken dVar, Bundle bundle);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract boolean mo1388b(CustomTabsSessionToken dVar);
}
