package android.support.p001v4.app;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.p001v4.app.Fragment.SavedState;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* renamed from: android.support.v4.app.FragmentStatePagerAdapter */
public abstract class FragmentStatePagerAdapter extends PagerAdapter {

    /* renamed from: a */
    private final FragmentManager f2835a;

    /* renamed from: b */
    private FragmentTransaction f2836b = null;

    /* renamed from: c */
    private ArrayList<SavedState> f2837c = new ArrayList<>();

    /* renamed from: d */
    private ArrayList<Fragment> f2838d = new ArrayList<>();

    /* renamed from: e */
    private Fragment f2839e = null;

    public FragmentStatePagerAdapter(FragmentManager fragmentManager) {
        this.f2835a = fragmentManager;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (this.f2836b == null) {
            this.f2836b = this.f2835a.beginTransaction();
        }
        while (this.f2837c.size() <= i) {
            this.f2837c.add(null);
        }
        this.f2837c.set(i, fragment.isAdded() ? this.f2835a.saveFragmentInstanceState(fragment) : null);
        this.f2838d.set(i, null);
        this.f2836b.remove(fragment);
    }

    public void finishUpdate(ViewGroup viewGroup) {
        if (this.f2836b != null) {
            this.f2836b.commitNowAllowingStateLoss();
            this.f2836b = null;
        }
    }

    public abstract Fragment getItem(int i);

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (this.f2838d.size() > i) {
            Fragment fragment = (Fragment) this.f2838d.get(i);
            if (fragment != null) {
                return fragment;
            }
        }
        if (this.f2836b == null) {
            this.f2836b = this.f2835a.beginTransaction();
        }
        Fragment item = getItem(i);
        if (this.f2837c.size() > i) {
            SavedState savedState = (SavedState) this.f2837c.get(i);
            if (savedState != null) {
                item.setInitialSavedState(savedState);
            }
        }
        while (this.f2838d.size() <= i) {
            this.f2838d.add(null);
        }
        item.setMenuVisibility(false);
        item.setUserVisibleHint(false);
        this.f2838d.set(i, item);
        this.f2836b.add(viewGroup.getId(), item);
        return item;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        if (parcelable != null) {
            Bundle bundle = (Bundle) parcelable;
            bundle.setClassLoader(classLoader);
            Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            this.f2837c.clear();
            this.f2838d.clear();
            if (parcelableArray != null) {
                for (Parcelable parcelable2 : parcelableArray) {
                    this.f2837c.add((SavedState) parcelable2);
                }
            }
            for (String str : bundle.keySet()) {
                if (str.startsWith("f")) {
                    int parseInt = Integer.parseInt(str.substring(1));
                    Fragment fragment = this.f2835a.getFragment(bundle, str);
                    if (fragment != null) {
                        while (this.f2838d.size() <= parseInt) {
                            this.f2838d.add(null);
                        }
                        fragment.setMenuVisibility(false);
                        this.f2838d.set(parseInt, fragment);
                    } else {
                        Log.w("FragmentStatePagerAdapter", "Bad fragment at key " + str);
                    }
                }
            }
        }
    }

    public Parcelable saveState() {
        Bundle bundle = null;
        if (this.f2837c.size() > 0) {
            bundle = new Bundle();
            SavedState[] savedStateArr = new SavedState[this.f2837c.size()];
            this.f2837c.toArray(savedStateArr);
            bundle.putParcelableArray("states", savedStateArr);
        }
        for (int i = 0; i < this.f2838d.size(); i++) {
            Fragment fragment = (Fragment) this.f2838d.get(i);
            if (fragment != null && fragment.isAdded()) {
                if (bundle == null) {
                    bundle = new Bundle();
                }
                this.f2835a.putFragment(bundle, "f" + i, fragment);
            }
        }
        return bundle;
    }

    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.f2839e) {
            if (this.f2839e != null) {
                this.f2839e.setMenuVisibility(false);
                this.f2839e.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            this.f2839e = fragment;
        }
    }

    public void startUpdate(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }
}
