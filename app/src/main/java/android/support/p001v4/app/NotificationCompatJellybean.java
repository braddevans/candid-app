package android.support.p001v4.app;

import android.app.Notification;
import android.app.Notification.BigPictureStyle;
import android.app.Notification.BigTextStyle;
import android.app.Notification.InboxStyle;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.p001v4.app.NotificationCompatBase.Action;
import android.support.p001v4.app.NotificationCompatBase.Action.Factory;
import android.support.p001v4.app.RemoteInputCompatBase.RemoteInput;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: android.support.v4.app.NotificationCompatJellybean */
class NotificationCompatJellybean {
    public static final String TAG = "NotificationCompat";

    /* renamed from: a */
    private static final Object f2981a = new Object();

    /* renamed from: b */
    private static Field f2982b;

    /* renamed from: c */
    private static boolean f2983c;

    /* renamed from: d */
    private static final Object f2984d = new Object();

    /* renamed from: e */
    private static Class<?> f2985e;

    /* renamed from: f */
    private static Field f2986f;

    /* renamed from: g */
    private static Field f2987g;

    /* renamed from: h */
    private static Field f2988h;

    /* renamed from: i */
    private static Field f2989i;

    /* renamed from: j */
    private static boolean f2990j;

    /* renamed from: android.support.v4.app.NotificationCompatJellybean$Builder */
    public static class Builder implements NotificationBuilderWithActions, NotificationBuilderWithBuilderAccessor {

        /* renamed from: a */
        private android.app.Notification.Builder f2991a;

        /* renamed from: b */
        private final Bundle f2992b;

        /* renamed from: c */
        private List<Bundle> f2993c = new ArrayList();

        /* renamed from: d */
        private RemoteViews f2994d;

        /* renamed from: e */
        private RemoteViews f2995e;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, int i4, CharSequence charSequence4, boolean z3, Bundle bundle, String str, boolean z4, String str2, RemoteViews remoteViews2, RemoteViews remoteViews3) {
            this.f2991a = new android.app.Notification.Builder(context).setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(pendingIntent2, (notification.flags & NotificationCompat.FLAG_HIGH_PRIORITY) != 0).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z2).setPriority(i4).setProgress(i2, i3, z);
            this.f2992b = new Bundle();
            if (bundle != null) {
                this.f2992b.putAll(bundle);
            }
            if (z3) {
                this.f2992b.putBoolean(NotificationCompatExtras.EXTRA_LOCAL_ONLY, true);
            }
            if (str != null) {
                this.f2992b.putString(NotificationCompatExtras.EXTRA_GROUP_KEY, str);
                if (z4) {
                    this.f2992b.putBoolean(NotificationCompatExtras.EXTRA_GROUP_SUMMARY, true);
                } else {
                    this.f2992b.putBoolean(NotificationManagerCompat.EXTRA_USE_SIDE_CHANNEL, true);
                }
            }
            if (str2 != null) {
                this.f2992b.putString(NotificationCompatExtras.EXTRA_SORT_KEY, str2);
            }
            this.f2994d = remoteViews2;
            this.f2995e = remoteViews3;
        }

        public void addAction(Action action) {
            this.f2993c.add(NotificationCompatJellybean.writeActionAndGetExtras(this.f2991a, action));
        }

        public Notification build() {
            Notification build = this.f2991a.build();
            Bundle extras = NotificationCompatJellybean.getExtras(build);
            Bundle bundle = new Bundle(this.f2992b);
            for (String str : this.f2992b.keySet()) {
                if (extras.containsKey(str)) {
                    bundle.remove(str);
                }
            }
            extras.putAll(bundle);
            SparseArray buildActionExtrasMap = NotificationCompatJellybean.buildActionExtrasMap(this.f2993c);
            if (buildActionExtrasMap != null) {
                NotificationCompatJellybean.getExtras(build).putSparseParcelableArray(NotificationCompatExtras.EXTRA_ACTION_EXTRAS, buildActionExtrasMap);
            }
            if (this.f2994d != null) {
                build.contentView = this.f2994d;
            }
            if (this.f2995e != null) {
                build.bigContentView = this.f2995e;
            }
            return build;
        }

        public android.app.Notification.Builder getBuilder() {
            return this.f2991a;
        }
    }

    NotificationCompatJellybean() {
    }

    /* renamed from: a */
    private static Bundle m3663a(Action action) {
        Bundle bundle = new Bundle();
        bundle.putInt("icon", action.getIcon());
        bundle.putCharSequence("title", action.getTitle());
        bundle.putParcelable("actionIntent", action.getActionIntent());
        bundle.putBundle("extras", action.getExtras());
        bundle.putParcelableArray("remoteInputs", RemoteInputCompatJellybean.m3687a(action.getRemoteInputs()));
        return bundle;
    }

    /* renamed from: a */
    private static Action m3664a(Bundle bundle, Factory factory, RemoteInput.Factory factory2) {
        return factory.build(bundle.getInt("icon"), bundle.getCharSequence("title"), (PendingIntent) bundle.getParcelable("actionIntent"), bundle.getBundle("extras"), RemoteInputCompatJellybean.m3688a(BundleUtil.getBundleArrayFromBundle(bundle, "remoteInputs"), factory2), bundle.getBoolean("allowGeneratedReplies"));
    }

    /* renamed from: a */
    private static boolean m3665a() {
        boolean z = true;
        if (f2990j) {
            return false;
        }
        try {
            if (f2986f == null) {
                f2985e = Class.forName("android.app.Notification$Action");
                f2987g = f2985e.getDeclaredField("icon");
                f2988h = f2985e.getDeclaredField("title");
                f2989i = f2985e.getDeclaredField("actionIntent");
                f2986f = Notification.class.getDeclaredField("actions");
                f2986f.setAccessible(true);
            }
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "Unable to access notification actions", e);
            f2990j = true;
        } catch (NoSuchFieldException e2) {
            Log.e(TAG, "Unable to access notification actions", e2);
            f2990j = true;
        }
        if (f2990j) {
            z = false;
        }
        return z;
    }

    /* renamed from: a */
    private static Object[] m3666a(Notification notification) {
        synchronized (f2984d) {
            if (!m3665a()) {
                return null;
            }
            try {
                Object[] objArr = (Object[]) f2986f.get(notification);
                return objArr;
            } catch (IllegalAccessException e) {
                Log.e(TAG, "Unable to access notification actions", e);
                f2990j = true;
                return null;
            }
        }
    }

    public static void addBigPictureStyle(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, CharSequence charSequence, boolean z, CharSequence charSequence2, Bitmap bitmap, Bitmap bitmap2, boolean z2) {
        BigPictureStyle bigPicture = new BigPictureStyle(notificationBuilderWithBuilderAccessor.getBuilder()).setBigContentTitle(charSequence).bigPicture(bitmap);
        if (z2) {
            bigPicture.bigLargeIcon(bitmap2);
        }
        if (z) {
            bigPicture.setSummaryText(charSequence2);
        }
    }

    public static void addBigTextStyle(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, CharSequence charSequence, boolean z, CharSequence charSequence2, CharSequence charSequence3) {
        BigTextStyle bigText = new BigTextStyle(notificationBuilderWithBuilderAccessor.getBuilder()).setBigContentTitle(charSequence).bigText(charSequence3);
        if (z) {
            bigText.setSummaryText(charSequence2);
        }
    }

    public static void addInboxStyle(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, CharSequence charSequence, boolean z, CharSequence charSequence2, ArrayList<CharSequence> arrayList) {
        InboxStyle bigContentTitle = new InboxStyle(notificationBuilderWithBuilderAccessor.getBuilder()).setBigContentTitle(charSequence);
        if (z) {
            bigContentTitle.setSummaryText(charSequence2);
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            bigContentTitle.addLine((CharSequence) it.next());
        }
    }

    public static SparseArray<Bundle> buildActionExtrasMap(List<Bundle> list) {
        SparseArray<Bundle> sparseArray = null;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Bundle bundle = (Bundle) list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.support.p001v4.app.NotificationCompatBase.Action getAction(android.app.Notification r11, int r12, android.support.p001v4.app.NotificationCompatBase.Action.Factory r13, android.support.p001v4.app.RemoteInputCompatBase.RemoteInput.Factory r14) {
        /*
            java.lang.Object r10 = f2984d
            monitor-enter(r10)
            java.lang.Object[] r0 = m3666a(r11)     // Catch:{ IllegalAccessException -> 0x003c }
            r7 = r0[r12]     // Catch:{ IllegalAccessException -> 0x003c }
            r5 = 0
            android.os.Bundle r9 = getExtras(r11)     // Catch:{ IllegalAccessException -> 0x003c }
            if (r9 == 0) goto L_0x001e
            java.lang.String r0 = "android.support.actionExtras"
            android.util.SparseArray r6 = r9.getSparseParcelableArray(r0)     // Catch:{ IllegalAccessException -> 0x003c }
            if (r6 == 0) goto L_0x001e
            java.lang.Object r5 = r6.get(r12)     // Catch:{ IllegalAccessException -> 0x003c }
            android.os.Bundle r5 = (android.os.Bundle) r5     // Catch:{ IllegalAccessException -> 0x003c }
        L_0x001e:
            java.lang.reflect.Field r0 = f2987g     // Catch:{ IllegalAccessException -> 0x003c }
            int r2 = r0.getInt(r7)     // Catch:{ IllegalAccessException -> 0x003c }
            java.lang.reflect.Field r0 = f2988h     // Catch:{ IllegalAccessException -> 0x003c }
            java.lang.Object r3 = r0.get(r7)     // Catch:{ IllegalAccessException -> 0x003c }
            java.lang.CharSequence r3 = (java.lang.CharSequence) r3     // Catch:{ IllegalAccessException -> 0x003c }
            java.lang.reflect.Field r0 = f2989i     // Catch:{ IllegalAccessException -> 0x003c }
            java.lang.Object r4 = r0.get(r7)     // Catch:{ IllegalAccessException -> 0x003c }
            android.app.PendingIntent r4 = (android.app.PendingIntent) r4     // Catch:{ IllegalAccessException -> 0x003c }
            r0 = r13
            r1 = r14
            android.support.v4.app.NotificationCompatBase$Action r0 = readAction(r0, r1, r2, r3, r4, r5)     // Catch:{ IllegalAccessException -> 0x003c }
            monitor-exit(r10)     // Catch:{ all -> 0x004a }
        L_0x003b:
            return r0
        L_0x003c:
            r8 = move-exception
            java.lang.String r0 = "NotificationCompat"
            java.lang.String r1 = "Unable to access notification actions"
            android.util.Log.e(r0, r1, r8)     // Catch:{ all -> 0x004a }
            r0 = 1
            f2990j = r0     // Catch:{ all -> 0x004a }
            monitor-exit(r10)     // Catch:{ all -> 0x004a }
            r0 = 0
            goto L_0x003b
        L_0x004a:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x004a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.p001v4.app.NotificationCompatJellybean.getAction(android.app.Notification, int, android.support.v4.app.NotificationCompatBase$Action$Factory, android.support.v4.app.RemoteInputCompatBase$RemoteInput$Factory):android.support.v4.app.NotificationCompatBase$Action");
    }

    public static int getActionCount(Notification notification) {
        int i;
        synchronized (f2984d) {
            Object[] a = m3666a(notification);
            i = a != null ? a.length : 0;
        }
        return i;
    }

    public static Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList, Factory factory, RemoteInput.Factory factory2) {
        if (arrayList == null) {
            return null;
        }
        Action[] newArray = factory.newArray(arrayList.size());
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = m3664a((Bundle) arrayList.get(i), factory, factory2);
        }
        return newArray;
    }

    public static Bundle getExtras(Notification notification) {
        synchronized (f2981a) {
            if (f2983c) {
                return null;
            }
            try {
                if (f2982b == null) {
                    Field declaredField = Notification.class.getDeclaredField("extras");
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        Log.e(TAG, "Notification.extras field is not of type Bundle");
                        f2983c = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    f2982b = declaredField;
                }
                Bundle bundle = (Bundle) f2982b.get(notification);
                if (bundle == null) {
                    bundle = new Bundle();
                    f2982b.set(notification, bundle);
                }
                return bundle;
            } catch (IllegalAccessException e) {
                Log.e(TAG, "Unable to access notification extras", e);
                f2983c = true;
                return null;
            } catch (NoSuchFieldException e2) {
                Log.e(TAG, "Unable to access notification extras", e2);
                f2983c = true;
                return null;
            }
        }
    }

    public static String getGroup(Notification notification) {
        return getExtras(notification).getString(NotificationCompatExtras.EXTRA_GROUP_KEY);
    }

    public static boolean getLocalOnly(Notification notification) {
        return getExtras(notification).getBoolean(NotificationCompatExtras.EXTRA_LOCAL_ONLY);
    }

    public static ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr) {
        if (actionArr == null) {
            return null;
        }
        ArrayList<Parcelable> arrayList = new ArrayList<>(actionArr.length);
        for (Action a : actionArr) {
            arrayList.add(m3663a(a));
        }
        return arrayList;
    }

    public static String getSortKey(Notification notification) {
        return getExtras(notification).getString(NotificationCompatExtras.EXTRA_SORT_KEY);
    }

    public static boolean isGroupSummary(Notification notification) {
        return getExtras(notification).getBoolean(NotificationCompatExtras.EXTRA_GROUP_SUMMARY);
    }

    public static Action readAction(Factory factory, RemoteInput.Factory factory2, int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle) {
        RemoteInput[] remoteInputArr = null;
        boolean z = false;
        if (bundle != null) {
            remoteInputArr = RemoteInputCompatJellybean.m3688a(BundleUtil.getBundleArrayFromBundle(bundle, NotificationCompatExtras.EXTRA_REMOTE_INPUTS), factory2);
            z = bundle.getBoolean("android.support.allowGeneratedReplies");
        }
        return factory.build(i, charSequence, pendingIntent, bundle, remoteInputArr, z);
    }

    public static Bundle writeActionAndGetExtras(android.app.Notification.Builder builder, Action action) {
        builder.addAction(action.getIcon(), action.getTitle(), action.getActionIntent());
        Bundle bundle = new Bundle(action.getExtras());
        if (action.getRemoteInputs() != null) {
            bundle.putParcelableArray(NotificationCompatExtras.EXTRA_REMOTE_INPUTS, RemoteInputCompatJellybean.m3687a(action.getRemoteInputs()));
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", action.getAllowGeneratedReplies());
        return bundle;
    }
}
