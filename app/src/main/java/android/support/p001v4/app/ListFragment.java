package android.support.p001v4.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/* renamed from: android.support.v4.app.ListFragment */
public class ListFragment extends Fragment {

    /* renamed from: a */
    ListAdapter f2877a;

    /* renamed from: b */
    ListView f2878b;

    /* renamed from: c */
    View f2879c;

    /* renamed from: d */
    TextView f2880d;

    /* renamed from: e */
    View f2881e;

    /* renamed from: f */
    View f2882f;

    /* renamed from: g */
    CharSequence f2883g;

    /* renamed from: h */
    boolean f2884h;

    /* renamed from: i */
    private final Handler f2885i = new Handler();

    /* renamed from: j */
    private final Runnable f2886j = new Runnable() {
        public void run() {
            ListFragment.this.f2878b.focusableViewAvailable(ListFragment.this.f2878b);
        }
    };

    /* renamed from: k */
    private final OnItemClickListener f2887k = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            ListFragment.this.onListItemClick((ListView) adapterView, view, i, j);
        }
    };

    /* renamed from: a */
    private void m3627a() {
        if (this.f2878b == null) {
            View view = getView();
            if (view == null) {
                throw new IllegalStateException("Content view not yet created");
            }
            if (view instanceof ListView) {
                this.f2878b = (ListView) view;
            } else {
                this.f2880d = (TextView) view.findViewById(16711681);
                if (this.f2880d == null) {
                    this.f2879c = view.findViewById(16908292);
                } else {
                    this.f2880d.setVisibility(8);
                }
                this.f2881e = view.findViewById(16711682);
                this.f2882f = view.findViewById(16711683);
                View findViewById = view.findViewById(16908298);
                if (findViewById instanceof ListView) {
                    this.f2878b = (ListView) findViewById;
                    if (this.f2879c != null) {
                        this.f2878b.setEmptyView(this.f2879c);
                    } else if (this.f2883g != null) {
                        this.f2880d.setText(this.f2883g);
                        this.f2878b.setEmptyView(this.f2880d);
                    }
                } else if (findViewById == null) {
                    throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
                } else {
                    throw new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
                }
            }
            this.f2884h = true;
            this.f2878b.setOnItemClickListener(this.f2887k);
            if (this.f2877a != null) {
                ListAdapter listAdapter = this.f2877a;
                this.f2877a = null;
                setListAdapter(listAdapter);
            } else if (this.f2881e != null) {
                m3628a(false, false);
            }
            this.f2885i.post(this.f2886j);
        }
    }

    /* renamed from: a */
    private void m3628a(boolean z, boolean z2) {
        m3627a();
        if (this.f2881e == null) {
            throw new IllegalStateException("Can't be used with a custom content view");
        } else if (this.f2884h != z) {
            this.f2884h = z;
            if (z) {
                if (z2) {
                    this.f2881e.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
                    this.f2882f.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432576));
                } else {
                    this.f2881e.clearAnimation();
                    this.f2882f.clearAnimation();
                }
                this.f2881e.setVisibility(8);
                this.f2882f.setVisibility(0);
                return;
            }
            if (z2) {
                this.f2881e.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432576));
                this.f2882f.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
            } else {
                this.f2881e.clearAnimation();
                this.f2882f.clearAnimation();
            }
            this.f2881e.setVisibility(0);
            this.f2882f.setVisibility(8);
        }
    }

    public ListAdapter getListAdapter() {
        return this.f2877a;
    }

    public ListView getListView() {
        m3627a();
        return this.f2878b;
    }

    public long getSelectedItemId() {
        m3627a();
        return this.f2878b.getSelectedItemId();
    }

    public int getSelectedItemPosition() {
        m3627a();
        return this.f2878b.getSelectedItemPosition();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Context context = getContext();
        FrameLayout frameLayout = new FrameLayout(context);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setId(16711682);
        linearLayout.setOrientation(1);
        linearLayout.setVisibility(8);
        linearLayout.setGravity(17);
        linearLayout.addView(new ProgressBar(context, null, 16842874), new LayoutParams(-2, -2));
        frameLayout.addView(linearLayout, new LayoutParams(-1, -1));
        FrameLayout frameLayout2 = new FrameLayout(context);
        frameLayout2.setId(16711683);
        TextView textView = new TextView(context);
        textView.setId(16711681);
        textView.setGravity(17);
        frameLayout2.addView(textView, new LayoutParams(-1, -1));
        ListView listView = new ListView(context);
        listView.setId(16908298);
        listView.setDrawSelectorOnTop(false);
        frameLayout2.addView(listView, new LayoutParams(-1, -1));
        frameLayout.addView(frameLayout2, new LayoutParams(-1, -1));
        frameLayout.setLayoutParams(new LayoutParams(-1, -1));
        return frameLayout;
    }

    public void onDestroyView() {
        this.f2885i.removeCallbacks(this.f2886j);
        this.f2878b = null;
        this.f2884h = false;
        this.f2882f = null;
        this.f2881e = null;
        this.f2879c = null;
        this.f2880d = null;
        super.onDestroyView();
    }

    public void onListItemClick(ListView listView, View view, int i, long j) {
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        m3627a();
    }

    public void setEmptyText(CharSequence charSequence) {
        m3627a();
        if (this.f2880d == null) {
            throw new IllegalStateException("Can't be used with a custom content view");
        }
        this.f2880d.setText(charSequence);
        if (this.f2883g == null) {
            this.f2878b.setEmptyView(this.f2880d);
        }
        this.f2883g = charSequence;
    }

    public void setListAdapter(ListAdapter listAdapter) {
        boolean z = false;
        boolean z2 = this.f2877a != null;
        this.f2877a = listAdapter;
        if (this.f2878b != null) {
            this.f2878b.setAdapter(listAdapter);
            if (!this.f2884h && !z2) {
                if (getView().getWindowToken() != null) {
                    z = true;
                }
                m3628a(true, z);
            }
        }
    }

    public void setListShown(boolean z) {
        m3628a(z, true);
    }

    public void setListShownNoAnimation(boolean z) {
        m3628a(z, false);
    }

    public void setSelection(int i) {
        m3627a();
        this.f2878b.setSelection(i);
    }
}
