package android.support.p001v4.app;

import android.os.Bundle;
import android.support.p001v4.app.LoaderManager.LoaderCallbacks;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

/* renamed from: android.support.v4.app.LoaderManagerImpl */
/* compiled from: LoaderManager */
class LoaderManagerImpl extends LoaderManager {

    /* renamed from: a */
    static boolean f2890a = false;

    /* renamed from: b */
    final SparseArrayCompat<LoaderInfo> f2891b = new SparseArrayCompat<>();

    /* renamed from: c */
    final SparseArrayCompat<LoaderInfo> f2892c = new SparseArrayCompat<>();

    /* renamed from: d */
    final String f2893d;

    /* renamed from: e */
    boolean f2894e;

    /* renamed from: f */
    boolean f2895f;

    /* renamed from: g */
    boolean f2896g;

    /* renamed from: h */
    FragmentHostCallback f2897h;

    /* renamed from: android.support.v4.app.LoaderManagerImpl$LoaderInfo */
    /* compiled from: LoaderManager */
    final class LoaderInfo implements C1188a<Object>, C1189b<Object> {

        /* renamed from: a */
        final int f2898a;

        /* renamed from: b */
        final Bundle f2899b;

        /* renamed from: c */
        LoaderCallbacks<Object> f2900c;

        /* renamed from: d */
        Loader<Object> f2901d;

        /* renamed from: e */
        boolean f2902e;

        /* renamed from: f */
        boolean f2903f;

        /* renamed from: g */
        Object f2904g;

        /* renamed from: h */
        boolean f2905h;

        /* renamed from: i */
        boolean f2906i;

        /* renamed from: j */
        boolean f2907j;

        /* renamed from: k */
        boolean f2908k;

        /* renamed from: l */
        boolean f2909l;

        /* renamed from: m */
        boolean f2910m;

        /* renamed from: n */
        LoaderInfo f2911n;

        public LoaderInfo(int i, Bundle bundle, LoaderCallbacks<Object> loaderCallbacks) {
            this.f2898a = i;
            this.f2899b = bundle;
            this.f2900c = loaderCallbacks;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo2427a() {
            if (this.f2906i && this.f2907j) {
                this.f2905h = true;
            } else if (!this.f2905h) {
                this.f2905h = true;
                if (LoaderManagerImpl.f2890a) {
                    Log.v("LoaderManager", "  Starting: " + this);
                }
                if (this.f2901d == null && this.f2900c != null) {
                    this.f2901d = this.f2900c.onCreateLoader(this.f2898a, this.f2899b);
                }
                if (this.f2901d == null) {
                    return;
                }
                if (!this.f2901d.getClass().isMemberClass() || Modifier.isStatic(this.f2901d.getClass().getModifiers())) {
                    if (!this.f2910m) {
                        this.f2901d.mo9318a(this.f2898a, this);
                        this.f2901d.mo9319a((C1188a<D>) this);
                        this.f2910m = true;
                    }
                    this.f2901d.mo9317a();
                    return;
                }
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.f2901d);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo2428a(Loader<Object> blVar, Object obj) {
            if (this.f2900c != null) {
                String str = null;
                if (LoaderManagerImpl.this.f2897h != null) {
                    str = LoaderManagerImpl.this.f2897h.f2764d.f2796v;
                    LoaderManagerImpl.this.f2897h.f2764d.f2796v = "onLoadFinished";
                }
                try {
                    if (LoaderManagerImpl.f2890a) {
                        Log.v("LoaderManager", "  onLoadFinished in " + blVar + ": " + blVar.mo9316a(obj));
                    }
                    this.f2900c.onLoadFinished(blVar, obj);
                    this.f2903f = true;
                } finally {
                    if (LoaderManagerImpl.this.f2897h != null) {
                        LoaderManagerImpl.this.f2897h.f2764d.f2796v = str;
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void mo2429b() {
            if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "  Retaining: " + this);
            }
            this.f2906i = true;
            this.f2907j = this.f2905h;
            this.f2905h = false;
            this.f2900c = null;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public void mo2430c() {
            if (this.f2906i) {
                if (LoaderManagerImpl.f2890a) {
                    Log.v("LoaderManager", "  Finished Retaining: " + this);
                }
                this.f2906i = false;
                if (this.f2905h != this.f2907j && !this.f2905h) {
                    mo2433e();
                }
            }
            if (this.f2905h && this.f2902e && !this.f2908k) {
                mo2428a(this.f2901d, this.f2904g);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public void mo2431d() {
            if (this.f2905h && this.f2908k) {
                this.f2908k = false;
                if (this.f2902e && !this.f2906i) {
                    mo2428a(this.f2901d, this.f2904g);
                }
            }
        }

        public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.f2898a);
            printWriter.print(" mArgs=");
            printWriter.println(this.f2899b);
            printWriter.print(str);
            printWriter.print("mCallbacks=");
            printWriter.println(this.f2900c);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.f2901d);
            if (this.f2901d != null) {
                this.f2901d.mo9321a(str + "  ", fileDescriptor, printWriter, strArr);
            }
            if (this.f2902e || this.f2903f) {
                printWriter.print(str);
                printWriter.print("mHaveData=");
                printWriter.print(this.f2902e);
                printWriter.print("  mDeliveredData=");
                printWriter.println(this.f2903f);
                printWriter.print(str);
                printWriter.print("mData=");
                printWriter.println(this.f2904g);
            }
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.f2905h);
            printWriter.print(" mReportNextStart=");
            printWriter.print(this.f2908k);
            printWriter.print(" mDestroyed=");
            printWriter.println(this.f2909l);
            printWriter.print(str);
            printWriter.print("mRetaining=");
            printWriter.print(this.f2906i);
            printWriter.print(" mRetainingStarted=");
            printWriter.print(this.f2907j);
            printWriter.print(" mListenerRegistered=");
            printWriter.println(this.f2910m);
            if (this.f2911n != null) {
                printWriter.print(str);
                printWriter.println("Pending Loader ");
                printWriter.print(this.f2911n);
                printWriter.println(":");
                this.f2911n.dump(str + "  ", fileDescriptor, printWriter, strArr);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: e */
        public void mo2433e() {
            if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.f2905h = false;
            if (!this.f2906i && this.f2901d != null && this.f2910m) {
                this.f2910m = false;
                this.f2901d.mo9320a((C1189b<D>) this);
                this.f2901d.mo9323b(this);
                this.f2901d.mo9326e();
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: f */
        public boolean mo2434f() {
            if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "  Canceling: " + this);
            }
            if (!this.f2905h || this.f2901d == null || !this.f2910m) {
                return false;
            }
            boolean c = this.f2901d.mo9324c();
            if (c) {
                return c;
            }
            onLoadCanceled(this.f2901d);
            return c;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: g */
        public void mo2435g() {
            if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.f2909l = true;
            boolean z = this.f2903f;
            this.f2903f = false;
            if (this.f2900c != null && this.f2901d != null && this.f2902e && z) {
                if (LoaderManagerImpl.f2890a) {
                    Log.v("LoaderManager", "  Resetting: " + this);
                }
                String str = null;
                if (LoaderManagerImpl.this.f2897h != null) {
                    str = LoaderManagerImpl.this.f2897h.f2764d.f2796v;
                    LoaderManagerImpl.this.f2897h.f2764d.f2796v = "onLoaderReset";
                }
                try {
                    this.f2900c.onLoaderReset(this.f2901d);
                } finally {
                    if (LoaderManagerImpl.this.f2897h != null) {
                        LoaderManagerImpl.this.f2897h.f2764d.f2796v = str;
                    }
                }
            }
            this.f2900c = null;
            this.f2904g = null;
            this.f2902e = false;
            if (this.f2901d != null) {
                if (this.f2910m) {
                    this.f2910m = false;
                    this.f2901d.mo9320a((C1189b<D>) this);
                    this.f2901d.mo9323b(this);
                }
                this.f2901d.mo9330i();
            }
            if (this.f2911n != null) {
                this.f2911n.mo2435g();
            }
        }

        public void onLoadCanceled(Loader<Object> blVar) {
            if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "onLoadCanceled: " + this);
            }
            if (this.f2909l) {
                if (LoaderManagerImpl.f2890a) {
                    Log.v("LoaderManager", "  Ignoring load canceled -- destroyed");
                }
            } else if (LoaderManagerImpl.this.f2891b.mo13320a(this.f2898a) == this) {
                LoaderInfo loaderInfo = this.f2911n;
                if (loaderInfo != null) {
                    if (LoaderManagerImpl.f2890a) {
                        Log.v("LoaderManager", "  Switching to pending loader: " + loaderInfo);
                    }
                    this.f2911n = null;
                    LoaderManagerImpl.this.f2891b.mo13324b(this.f2898a, null);
                    mo2435g();
                    LoaderManagerImpl.this.mo2419a(loaderInfo);
                }
            } else if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "  Ignoring load canceled -- not active");
            }
        }

        public void onLoadComplete(Loader<Object> blVar, Object obj) {
            if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (this.f2909l) {
                if (LoaderManagerImpl.f2890a) {
                    Log.v("LoaderManager", "  Ignoring load complete -- destroyed");
                }
            } else if (LoaderManagerImpl.this.f2891b.mo13320a(this.f2898a) == this) {
                LoaderInfo loaderInfo = this.f2911n;
                if (loaderInfo != null) {
                    if (LoaderManagerImpl.f2890a) {
                        Log.v("LoaderManager", "  Switching to pending loader: " + loaderInfo);
                    }
                    this.f2911n = null;
                    LoaderManagerImpl.this.f2891b.mo13324b(this.f2898a, null);
                    mo2435g();
                    LoaderManagerImpl.this.mo2419a(loaderInfo);
                    return;
                }
                if (this.f2904g != obj || !this.f2902e) {
                    this.f2904g = obj;
                    this.f2902e = true;
                    if (this.f2905h) {
                        mo2428a(blVar, obj);
                    }
                }
                LoaderInfo loaderInfo2 = (LoaderInfo) LoaderManagerImpl.this.f2892c.mo13320a(this.f2898a);
                if (!(loaderInfo2 == null || loaderInfo2 == this)) {
                    loaderInfo2.f2903f = false;
                    loaderInfo2.mo2435g();
                    LoaderManagerImpl.this.f2892c.mo13326c(this.f2898a);
                }
                if (LoaderManagerImpl.this.f2897h != null && !LoaderManagerImpl.this.hasRunningLoaders()) {
                    LoaderManagerImpl.this.f2897h.f2764d.mo2286a();
                }
            } else if (LoaderManagerImpl.f2890a) {
                Log.v("LoaderManager", "  Ignoring load complete -- not active");
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.f2898a);
            sb.append(" : ");
            DebugUtils.m12560a(this.f2901d, sb);
            sb.append("}}");
            return sb.toString();
        }
    }

    LoaderManagerImpl(String str, FragmentHostCallback fragmentHostCallback, boolean z) {
        this.f2893d = str;
        this.f2897h = fragmentHostCallback;
        this.f2894e = z;
    }

    /* renamed from: a */
    private LoaderInfo m3629a(int i, Bundle bundle, LoaderCallbacks<Object> loaderCallbacks) {
        LoaderInfo loaderInfo = new LoaderInfo(i, bundle, loaderCallbacks);
        loaderInfo.f2901d = loaderCallbacks.onCreateLoader(i, bundle);
        return loaderInfo;
    }

    /* renamed from: b */
    private LoaderInfo m3630b(int i, Bundle bundle, LoaderCallbacks<Object> loaderCallbacks) {
        try {
            this.f2896g = true;
            LoaderInfo a = m3629a(i, bundle, loaderCallbacks);
            mo2419a(a);
            return a;
        } finally {
            this.f2896g = false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2417a() {
        if (f2890a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.f2894e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.f2894e = true;
        for (int b = this.f2891b.mo13322b() - 1; b >= 0; b--) {
            ((LoaderInfo) this.f2891b.mo13330f(b)).mo2427a();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2418a(FragmentHostCallback fragmentHostCallback) {
        this.f2897h = fragmentHostCallback;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2419a(LoaderInfo loaderInfo) {
        this.f2891b.mo13324b(loaderInfo.f2898a, loaderInfo);
        if (this.f2894e) {
            loaderInfo.mo2427a();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo2420b() {
        if (f2890a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.f2894e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int b = this.f2891b.mo13322b() - 1; b >= 0; b--) {
            ((LoaderInfo) this.f2891b.mo13330f(b)).mo2433e();
        }
        this.f2894e = false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo2421c() {
        if (f2890a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.f2894e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.f2895f = true;
        this.f2894e = false;
        for (int b = this.f2891b.mo13322b() - 1; b >= 0; b--) {
            ((LoaderInfo) this.f2891b.mo13330f(b)).mo2429b();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public void mo2422d() {
        if (this.f2895f) {
            if (f2890a) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.f2895f = false;
            for (int b = this.f2891b.mo13322b() - 1; b >= 0; b--) {
                ((LoaderInfo) this.f2891b.mo13330f(b)).mo2430c();
            }
        }
    }

    public void destroyLoader(int i) {
        if (this.f2896g) {
            throw new IllegalStateException("Called while creating a loader");
        }
        if (f2890a) {
            Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
        }
        int g = this.f2891b.mo13331g(i);
        if (g >= 0) {
            LoaderInfo loaderInfo = (LoaderInfo) this.f2891b.mo13330f(g);
            this.f2891b.mo13328d(g);
            loaderInfo.mo2435g();
        }
        int g2 = this.f2892c.mo13331g(i);
        if (g2 >= 0) {
            LoaderInfo loaderInfo2 = (LoaderInfo) this.f2892c.mo13330f(g2);
            this.f2892c.mo13328d(g2);
            loaderInfo2.mo2435g();
        }
        if (this.f2897h != null && !hasRunningLoaders()) {
            this.f2897h.f2764d.mo2286a();
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.f2891b.mo13322b() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.f2891b.mo13322b(); i++) {
                LoaderInfo loaderInfo = (LoaderInfo) this.f2891b.mo13330f(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f2891b.mo13329e(i));
                printWriter.print(": ");
                printWriter.println(loaderInfo.toString());
                loaderInfo.dump(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.f2892c.mo13322b() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.f2892c.mo13322b(); i2++) {
                LoaderInfo loaderInfo2 = (LoaderInfo) this.f2892c.mo13330f(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f2892c.mo13329e(i2));
                printWriter.print(": ");
                printWriter.println(loaderInfo2.toString());
                loaderInfo2.dump(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public void mo2423e() {
        for (int b = this.f2891b.mo13322b() - 1; b >= 0; b--) {
            ((LoaderInfo) this.f2891b.mo13330f(b)).f2908k = true;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public void mo2424f() {
        for (int b = this.f2891b.mo13322b() - 1; b >= 0; b--) {
            ((LoaderInfo) this.f2891b.mo13330f(b)).mo2431d();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public void mo2425g() {
        if (!this.f2895f) {
            if (f2890a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int b = this.f2891b.mo13322b() - 1; b >= 0; b--) {
                ((LoaderInfo) this.f2891b.mo13330f(b)).mo2435g();
            }
            this.f2891b.mo13325c();
        }
        if (f2890a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int b2 = this.f2892c.mo13322b() - 1; b2 >= 0; b2--) {
            ((LoaderInfo) this.f2892c.mo13330f(b2)).mo2435g();
        }
        this.f2892c.mo13325c();
    }

    public <D> Loader<D> getLoader(int i) {
        if (this.f2896g) {
            throw new IllegalStateException("Called while creating a loader");
        }
        LoaderInfo loaderInfo = (LoaderInfo) this.f2891b.mo13320a(i);
        if (loaderInfo != null) {
            return loaderInfo.f2911n != null ? loaderInfo.f2911n.f2901d : loaderInfo.f2901d;
        }
        return null;
    }

    public boolean hasRunningLoaders() {
        boolean z = false;
        for (int i = 0; i < this.f2891b.mo13322b(); i++) {
            LoaderInfo loaderInfo = (LoaderInfo) this.f2891b.mo13330f(i);
            z |= loaderInfo.f2905h && !loaderInfo.f2903f;
        }
        return z;
    }

    public <D> Loader<D> initLoader(int i, Bundle bundle, LoaderCallbacks<D> loaderCallbacks) {
        if (this.f2896g) {
            throw new IllegalStateException("Called while creating a loader");
        }
        LoaderInfo loaderInfo = (LoaderInfo) this.f2891b.mo13320a(i);
        if (f2890a) {
            Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
        }
        if (loaderInfo == null) {
            loaderInfo = m3630b(i, bundle, loaderCallbacks);
            if (f2890a) {
                Log.v("LoaderManager", "  Created new loader " + loaderInfo);
            }
        } else {
            if (f2890a) {
                Log.v("LoaderManager", "  Re-using existing loader " + loaderInfo);
            }
            loaderInfo.f2900c = loaderCallbacks;
        }
        if (loaderInfo.f2902e && this.f2894e) {
            loaderInfo.mo2428a(loaderInfo.f2901d, loaderInfo.f2904g);
        }
        return loaderInfo.f2901d;
    }

    public <D> Loader<D> restartLoader(int i, Bundle bundle, LoaderCallbacks<D> loaderCallbacks) {
        if (this.f2896g) {
            throw new IllegalStateException("Called while creating a loader");
        }
        LoaderInfo loaderInfo = (LoaderInfo) this.f2891b.mo13320a(i);
        if (f2890a) {
            Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
        }
        if (loaderInfo != null) {
            LoaderInfo loaderInfo2 = (LoaderInfo) this.f2892c.mo13320a(i);
            if (loaderInfo2 == null) {
                if (f2890a) {
                    Log.v("LoaderManager", "  Making last loader inactive: " + loaderInfo);
                }
                loaderInfo.f2901d.mo9328g();
                this.f2892c.mo13324b(i, loaderInfo);
            } else if (loaderInfo.f2902e) {
                if (f2890a) {
                    Log.v("LoaderManager", "  Removing last inactive loader: " + loaderInfo);
                }
                loaderInfo2.f2903f = false;
                loaderInfo2.mo2435g();
                loaderInfo.f2901d.mo9328g();
                this.f2892c.mo13324b(i, loaderInfo);
            } else if (!loaderInfo.mo2434f()) {
                if (f2890a) {
                    Log.v("LoaderManager", "  Current loader is stopped; replacing");
                }
                this.f2891b.mo13324b(i, null);
                loaderInfo.mo2435g();
            } else {
                if (f2890a) {
                    Log.v("LoaderManager", "  Current loader is running; configuring pending loader");
                }
                if (loaderInfo.f2911n != null) {
                    if (f2890a) {
                        Log.v("LoaderManager", "  Removing pending loader: " + loaderInfo.f2911n);
                    }
                    loaderInfo.f2911n.mo2435g();
                    loaderInfo.f2911n = null;
                }
                if (f2890a) {
                    Log.v("LoaderManager", "  Enqueuing as new pending loader");
                }
                loaderInfo.f2911n = m3629a(i, bundle, loaderCallbacks);
                return loaderInfo.f2911n.f2901d;
            }
        }
        return m3630b(i, bundle, loaderCallbacks).f2901d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(NotificationCompat.FLAG_HIGH_PRIORITY);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        DebugUtils.m12560a(this.f2897h, sb);
        sb.append("}}");
        return sb.toString();
    }
}
