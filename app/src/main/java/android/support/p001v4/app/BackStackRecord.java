package android.support.p001v4.app;

import android.os.Build.VERSION;
import android.support.p001v4.app.FragmentManager.BackStackEntry;
import android.support.p001v4.app.FragmentTransitionCompat21.EpicenterView;
import android.support.p001v4.app.FragmentTransitionCompat21.ViewRetriever;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/* renamed from: android.support.v4.app.BackStackRecord */
final class BackStackRecord extends FragmentTransaction implements BackStackEntry, Runnable {

    /* renamed from: a */
    static final boolean f2689a = (VERSION.SDK_INT >= 21);

    /* renamed from: b */
    final FragmentManagerImpl f2690b;

    /* renamed from: c */
    C0432Op f2691c;

    /* renamed from: d */
    C0432Op f2692d;

    /* renamed from: e */
    int f2693e;

    /* renamed from: f */
    int f2694f;

    /* renamed from: g */
    int f2695g;

    /* renamed from: h */
    int f2696h;

    /* renamed from: i */
    int f2697i;

    /* renamed from: j */
    int f2698j;

    /* renamed from: k */
    int f2699k;

    /* renamed from: l */
    boolean f2700l;

    /* renamed from: m */
    boolean f2701m = true;

    /* renamed from: n */
    String f2702n;

    /* renamed from: o */
    boolean f2703o;

    /* renamed from: p */
    int f2704p = -1;

    /* renamed from: q */
    int f2705q;

    /* renamed from: r */
    CharSequence f2706r;

    /* renamed from: s */
    int f2707s;

    /* renamed from: t */
    CharSequence f2708t;

    /* renamed from: u */
    ArrayList<String> f2709u;

    /* renamed from: v */
    ArrayList<String> f2710v;

    /* renamed from: android.support.v4.app.BackStackRecord$Op */
    static final class C0432Op {

        /* renamed from: a */
        C0432Op f2728a;

        /* renamed from: b */
        C0432Op f2729b;

        /* renamed from: c */
        int f2730c;

        /* renamed from: d */
        Fragment f2731d;

        /* renamed from: e */
        int f2732e;

        /* renamed from: f */
        int f2733f;

        /* renamed from: g */
        int f2734g;

        /* renamed from: h */
        int f2735h;

        /* renamed from: i */
        ArrayList<Fragment> f2736i;

        C0432Op() {
        }
    }

    /* renamed from: android.support.v4.app.BackStackRecord$TransitionState */
    public class TransitionState {
        public EpicenterView enteringEpicenterView = new EpicenterView();
        public ArrayList<View> hiddenFragmentViews = new ArrayList<>();
        public ArrayMap<String, String> nameOverrides = new ArrayMap<>();
        public View nonExistentView;

        public TransitionState() {
        }
    }

    public BackStackRecord(FragmentManagerImpl fragmentManagerImpl) {
        this.f2690b = fragmentManagerImpl;
    }

    /* renamed from: a */
    private TransitionState m3540a(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2, boolean z) {
        TransitionState transitionState = new TransitionState();
        transitionState.nonExistentView = new View(this.f2690b.f2790o.mo2253b());
        boolean z2 = false;
        for (int i = 0; i < sparseArray.size(); i++) {
            if (m3554a(sparseArray.keyAt(i), transitionState, z, sparseArray, sparseArray2)) {
                z2 = true;
            }
        }
        for (int i2 = 0; i2 < sparseArray2.size(); i2++) {
            int keyAt = sparseArray2.keyAt(i2);
            if (sparseArray.get(keyAt) == null && m3554a(keyAt, transitionState, z, sparseArray, sparseArray2)) {
                z2 = true;
            }
        }
        if (!z2) {
            return null;
        }
        return transitionState;
    }

    /* renamed from: a */
    private ArrayMap<String, View> m3541a(TransitionState transitionState, Fragment fragment, boolean z) {
        ArrayMap<String, View> dkVar = new ArrayMap<>();
        if (this.f2709u != null) {
            FragmentTransitionCompat21.findNamedViews(dkVar, fragment.getView());
            if (z) {
                dkVar.mo13183a(this.f2710v);
            } else {
                dkVar = m3542a(this.f2709u, this.f2710v, dkVar);
            }
        }
        if (z) {
            if (fragment.mEnterTransitionCallback != null) {
                fragment.mEnterTransitionCallback.onMapSharedElements(this.f2710v, dkVar);
            }
            m3548a(transitionState, dkVar, false);
        } else {
            if (fragment.mExitTransitionCallback != null) {
                fragment.mExitTransitionCallback.onMapSharedElements(this.f2710v, dkVar);
            }
            m3557b(transitionState, dkVar, false);
        }
        return dkVar;
    }

    /* renamed from: a */
    private static ArrayMap<String, View> m3542a(ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayMap<String, View> dkVar) {
        if (dkVar.isEmpty()) {
            return dkVar;
        }
        ArrayMap dkVar2 = new ArrayMap();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            View view = (View) dkVar.get(arrayList.get(i));
            if (view != null) {
                dkVar2.put(arrayList2.get(i), view);
            }
        }
        return dkVar2;
    }

    /* renamed from: a */
    private static Object m3543a(Fragment fragment, Fragment fragment2, boolean z) {
        if (fragment == null || fragment2 == null) {
            return null;
        }
        return FragmentTransitionCompat21.wrapSharedElementTransition(z ? fragment2.getSharedElementReturnTransition() : fragment.getSharedElementEnterTransition());
    }

    /* renamed from: a */
    private static Object m3544a(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return FragmentTransitionCompat21.cloneTransition(z ? fragment.getReenterTransition() : fragment.getEnterTransition());
    }

    /* renamed from: a */
    private static Object m3545a(Object obj, Fragment fragment, ArrayList<View> arrayList, ArrayMap<String, View> dkVar, View view) {
        return obj != null ? FragmentTransitionCompat21.captureExitingViews(obj, fragment.getView(), arrayList, dkVar, view) : obj;
    }

    /* renamed from: a */
    private void m3546a(int i, Fragment fragment, String str, int i2) {
        Class cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from" + " instance state.");
        }
        fragment.mFragmentManager = this.f2690b;
        if (str != null) {
            if (fragment.mTag == null || str.equals(fragment.mTag)) {
                fragment.mTag = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i != 0) {
            if (i == -1) {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            } else if (fragment.mFragmentId == 0 || fragment.mFragmentId == i) {
                fragment.mFragmentId = i;
                fragment.mContainerId = i;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i);
            }
        }
        C0432Op op = new C0432Op();
        op.f2730c = i2;
        op.f2731d = fragment;
        mo1952a(op);
    }

    /* renamed from: a */
    private void m3547a(TransitionState transitionState, View view, Object obj, Fragment fragment, Fragment fragment2, boolean z, ArrayList<View> arrayList, Object obj2, Object obj3) {
        if (obj != null) {
            final View view2 = view;
            final Object obj4 = obj;
            final ArrayList<View> arrayList2 = arrayList;
            final TransitionState transitionState2 = transitionState;
            final Object obj5 = obj2;
            final Object obj6 = obj3;
            final boolean z2 = z;
            final Fragment fragment3 = fragment;
            final Fragment fragment4 = fragment2;
            view.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
                public boolean onPreDraw() {
                    view2.getViewTreeObserver().removeOnPreDrawListener(this);
                    FragmentTransitionCompat21.removeTargets(obj4, arrayList2);
                    arrayList2.remove(transitionState2.nonExistentView);
                    FragmentTransitionCompat21.excludeSharedElementViews(obj5, obj6, obj4, arrayList2, false);
                    arrayList2.clear();
                    ArrayMap a = BackStackRecord.this.mo1950a(transitionState2, z2, fragment3);
                    FragmentTransitionCompat21.setSharedElementTargets(obj4, transitionState2.nonExistentView, a, arrayList2);
                    BackStackRecord.this.mo1955a(a, transitionState2);
                    BackStackRecord.this.mo1954a(transitionState2, fragment3, fragment4, z2, a);
                    FragmentTransitionCompat21.excludeSharedElementViews(obj5, obj6, obj4, arrayList2, true);
                    return true;
                }
            });
        }
    }

    /* renamed from: a */
    private void m3548a(TransitionState transitionState, ArrayMap<String, View> dkVar, boolean z) {
        int size = this.f2710v == null ? 0 : this.f2710v.size();
        for (int i = 0; i < size; i++) {
            String str = (String) this.f2709u.get(i);
            View view = (View) dkVar.get((String) this.f2710v.get(i));
            if (view != null) {
                String transitionName = FragmentTransitionCompat21.getTransitionName(view);
                if (z) {
                    m3553a(transitionState.nameOverrides, str, transitionName);
                } else {
                    m3553a(transitionState.nameOverrides, transitionName, str);
                }
            }
        }
    }

    /* renamed from: a */
    private static void m3549a(TransitionState transitionState, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        if (arrayList != null) {
            for (int i = 0; i < arrayList.size(); i++) {
                m3553a(transitionState.nameOverrides, (String) arrayList.get(i), (String) arrayList2.get(i));
            }
        }
    }

    /* renamed from: a */
    private void m3550a(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        if (this.f2690b.f2791p.onHasView()) {
            for (C0432Op op = this.f2691c; op != null; op = op.f2728a) {
                switch (op.f2730c) {
                    case 1:
                        m3558b(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 2:
                        Fragment fragment = op.f2731d;
                        if (this.f2690b.f2782g != null) {
                            for (int i = 0; i < this.f2690b.f2782g.size(); i++) {
                                Fragment fragment2 = (Fragment) this.f2690b.f2782g.get(i);
                                if (fragment == null || fragment2.mContainerId == fragment.mContainerId) {
                                    if (fragment2 == fragment) {
                                        fragment = null;
                                        sparseArray2.remove(fragment2.mContainerId);
                                    } else {
                                        m3551a(sparseArray, sparseArray2, fragment2);
                                    }
                                }
                            }
                        }
                        m3558b(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 3:
                        m3551a(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 4:
                        m3551a(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 5:
                        m3558b(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 6:
                        m3551a(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 7:
                        m3558b(sparseArray, sparseArray2, op.f2731d);
                        break;
                }
            }
        }
    }

    /* renamed from: a */
    private static void m3551a(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2, Fragment fragment) {
        if (fragment != null) {
            int i = fragment.mContainerId;
            if (i != 0 && !fragment.isHidden()) {
                if (fragment.isAdded() && fragment.getView() != null && sparseArray.get(i) == null) {
                    sparseArray.put(i, fragment);
                }
                if (sparseArray2.get(i) == fragment) {
                    sparseArray2.remove(i);
                }
            }
        }
    }

    /* renamed from: a */
    private void m3552a(View view, TransitionState transitionState, int i, Object obj) {
        final View view2 = view;
        final TransitionState transitionState2 = transitionState;
        final int i2 = i;
        final Object obj2 = obj;
        view.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public boolean onPreDraw() {
                view2.getViewTreeObserver().removeOnPreDrawListener(this);
                BackStackRecord.this.mo1953a(transitionState2, i2, obj2);
                return true;
            }
        });
    }

    /* renamed from: a */
    private static void m3553a(ArrayMap<String, String> dkVar, String str, String str2) {
        if (str != null && str2 != null) {
            for (int i = 0; i < dkVar.size(); i++) {
                if (str.equals(dkVar.mo13305c(i))) {
                    dkVar.mo13300a(i, str2);
                    return;
                }
            }
            dkVar.put(str, str2);
        }
    }

    /* renamed from: a */
    private boolean m3554a(int i, TransitionState transitionState, boolean z, SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        ViewGroup viewGroup = (ViewGroup) this.f2690b.f2791p.onFindViewById(i);
        if (viewGroup == null) {
            return false;
        }
        final Fragment fragment = (Fragment) sparseArray2.get(i);
        Fragment fragment2 = (Fragment) sparseArray.get(i);
        Object a = m3544a(fragment, z);
        Object a2 = m3543a(fragment, fragment2, z);
        Object b = m3556b(fragment2, z);
        ArrayMap dkVar = null;
        ArrayList arrayList = new ArrayList();
        if (a2 != null) {
            dkVar = m3541a(transitionState, fragment2, z);
            if (dkVar.isEmpty()) {
                a2 = null;
                dkVar = null;
            } else {
                SharedElementCallback sharedElementCallback = z ? fragment2.mEnterTransitionCallback : fragment.mEnterTransitionCallback;
                if (sharedElementCallback != null) {
                    ArrayList arrayList2 = new ArrayList(dkVar.keySet());
                    ArrayList arrayList3 = new ArrayList(dkVar.values());
                    sharedElementCallback.onSharedElementStart(arrayList2, arrayList3, null);
                }
                m3547a(transitionState, viewGroup, a2, fragment, fragment2, z, arrayList, a, b);
            }
        }
        if (a == null && a2 == null && b == null) {
            return false;
        }
        ArrayList arrayList4 = new ArrayList();
        Object a3 = m3545a(b, fragment2, arrayList4, dkVar, transitionState.nonExistentView);
        if (!(this.f2710v == null || dkVar == null)) {
            View view = (View) dkVar.get(this.f2710v.get(0));
            if (view != null) {
                if (a3 != null) {
                    FragmentTransitionCompat21.setEpicenter(a3, view);
                }
                if (a2 != null) {
                    FragmentTransitionCompat21.setEpicenter(a2, view);
                }
            }
        }
        C04291 r0 = new ViewRetriever() {
            public View getView() {
                return fragment.getView();
            }
        };
        ArrayList arrayList5 = new ArrayList();
        ArrayMap dkVar2 = new ArrayMap();
        boolean z2 = true;
        if (fragment != null) {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        Object mergeTransitions = FragmentTransitionCompat21.mergeTransitions(a, a3, a2, z2);
        if (mergeTransitions != null) {
            FragmentTransitionCompat21.addTransitionTargets(a, a2, a3, viewGroup, r0, transitionState.nonExistentView, transitionState.enteringEpicenterView, transitionState.nameOverrides, arrayList5, arrayList4, dkVar, dkVar2, arrayList);
            m3552a((View) viewGroup, transitionState, i, mergeTransitions);
            FragmentTransitionCompat21.excludeTarget(mergeTransitions, transitionState.nonExistentView, true);
            mo1953a(transitionState, i, mergeTransitions);
            FragmentTransitionCompat21.beginDelayedTransition(viewGroup, mergeTransitions);
            FragmentTransitionCompat21.cleanupTransitions(viewGroup, transitionState.nonExistentView, a, arrayList5, a3, arrayList4, a2, arrayList, mergeTransitions, transitionState.hiddenFragmentViews, dkVar2);
        }
        return mergeTransitions != null;
    }

    /* renamed from: b */
    private ArrayMap<String, View> m3555b(TransitionState transitionState, Fragment fragment, boolean z) {
        ArrayMap<String, View> dkVar = new ArrayMap<>();
        View view = fragment.getView();
        if (view == null || this.f2709u == null) {
            return dkVar;
        }
        FragmentTransitionCompat21.findNamedViews(dkVar, view);
        if (z) {
            return m3542a(this.f2709u, this.f2710v, dkVar);
        }
        dkVar.mo13183a(this.f2710v);
        return dkVar;
    }

    /* renamed from: b */
    private static Object m3556b(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return FragmentTransitionCompat21.cloneTransition(z ? fragment.getReturnTransition() : fragment.getExitTransition());
    }

    /* renamed from: b */
    private void m3557b(TransitionState transitionState, ArrayMap<String, View> dkVar, boolean z) {
        int size = dkVar.size();
        for (int i = 0; i < size; i++) {
            String str = (String) dkVar.mo13304b(i);
            String transitionName = FragmentTransitionCompat21.getTransitionName((View) dkVar.mo13305c(i));
            if (z) {
                m3553a(transitionState.nameOverrides, str, transitionName);
            } else {
                m3553a(transitionState.nameOverrides, transitionName, str);
            }
        }
    }

    /* renamed from: b */
    private void m3558b(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2, Fragment fragment) {
        if (fragment != null) {
            int i = fragment.mContainerId;
            if (i != 0) {
                if (!fragment.isAdded()) {
                    sparseArray2.put(i, fragment);
                }
                if (sparseArray.get(i) == fragment) {
                    sparseArray.remove(i);
                }
            }
            if (fragment.mState < 1 && this.f2690b.f2789n >= 1) {
                this.f2690b.mo2300b(fragment);
                this.f2690b.mo2292a(fragment, 1, 0, 0, false);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public int mo1949a(boolean z) {
        if (this.f2703o) {
            throw new IllegalStateException("commit already called");
        }
        if (FragmentManagerImpl.f2775a) {
            Log.v("FragmentManager", "Commit: " + this);
            dump("  ", null, new PrintWriter(new LogWriter("FragmentManager")), null);
        }
        this.f2703o = true;
        if (this.f2700l) {
            this.f2704p = this.f2690b.allocBackStackIndex(this);
        } else {
            this.f2704p = -1;
        }
        this.f2690b.enqueueAction(this, z);
        return this.f2704p;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public ArrayMap<String, View> mo1950a(TransitionState transitionState, boolean z, Fragment fragment) {
        ArrayMap<String, View> b = m3555b(transitionState, fragment, z);
        if (z) {
            if (fragment.mExitTransitionCallback != null) {
                fragment.mExitTransitionCallback.onMapSharedElements(this.f2710v, b);
            }
            m3548a(transitionState, b, true);
        } else {
            if (fragment.mEnterTransitionCallback != null) {
                fragment.mEnterTransitionCallback.onMapSharedElements(this.f2710v, b);
            }
            m3557b(transitionState, b, true);
        }
        return b;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1951a(int i) {
        if (this.f2700l) {
            if (FragmentManagerImpl.f2775a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            C0432Op op = this.f2691c;
            while (op != null) {
                if (op.f2731d != null) {
                    op.f2731d.mBackStackNesting += i;
                    if (FragmentManagerImpl.f2775a) {
                        Log.v("FragmentManager", "Bump nesting of " + op.f2731d + " to " + op.f2731d.mBackStackNesting);
                    }
                }
                if (op.f2736i != null) {
                    for (int size = op.f2736i.size() - 1; size >= 0; size--) {
                        Fragment fragment = (Fragment) op.f2736i.get(size);
                        fragment.mBackStackNesting += i;
                        if (FragmentManagerImpl.f2775a) {
                            Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.mBackStackNesting);
                        }
                    }
                }
                op = op.f2728a;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1952a(C0432Op op) {
        if (this.f2691c == null) {
            this.f2692d = op;
            this.f2691c = op;
        } else {
            op.f2729b = this.f2692d;
            this.f2692d.f2728a = op;
            this.f2692d = op;
        }
        op.f2732e = this.f2694f;
        op.f2733f = this.f2695g;
        op.f2734g = this.f2696h;
        op.f2735h = this.f2697i;
        this.f2693e++;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1953a(TransitionState transitionState, int i, Object obj) {
        if (this.f2690b.f2782g != null) {
            for (int i2 = 0; i2 < this.f2690b.f2782g.size(); i2++) {
                Fragment fragment = (Fragment) this.f2690b.f2782g.get(i2);
                if (!(fragment.mView == null || fragment.mContainer == null || fragment.mContainerId != i)) {
                    if (!fragment.mHidden) {
                        FragmentTransitionCompat21.excludeTarget(obj, fragment.mView, false);
                        transitionState.hiddenFragmentViews.remove(fragment.mView);
                    } else if (!transitionState.hiddenFragmentViews.contains(fragment.mView)) {
                        FragmentTransitionCompat21.excludeTarget(obj, fragment.mView, true);
                        transitionState.hiddenFragmentViews.add(fragment.mView);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1954a(TransitionState transitionState, Fragment fragment, Fragment fragment2, boolean z, ArrayMap<String, View> dkVar) {
        SharedElementCallback sharedElementCallback = z ? fragment2.mEnterTransitionCallback : fragment.mEnterTransitionCallback;
        if (sharedElementCallback != null) {
            sharedElementCallback.onSharedElementEnd(new ArrayList(dkVar.keySet()), new ArrayList(dkVar.values()), null);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1955a(ArrayMap<String, View> dkVar, TransitionState transitionState) {
        if (this.f2710v != null && !dkVar.isEmpty()) {
            View view = (View) dkVar.get(this.f2710v.get(0));
            if (view != null) {
                transitionState.enteringEpicenterView.epicenter = view;
            }
        }
    }

    public FragmentTransaction add(int i, Fragment fragment) {
        m3546a(i, fragment, (String) null, 1);
        return this;
    }

    public FragmentTransaction add(int i, Fragment fragment, String str) {
        m3546a(i, fragment, str, 1);
        return this;
    }

    public FragmentTransaction add(Fragment fragment, String str) {
        m3546a(0, fragment, str, 1);
        return this;
    }

    public FragmentTransaction addSharedElement(View view, String str) {
        if (f2689a) {
            String transitionName = FragmentTransitionCompat21.getTransitionName(view);
            if (transitionName == null) {
                throw new IllegalArgumentException("Unique transitionNames are required for all sharedElements");
            }
            if (this.f2709u == null) {
                this.f2709u = new ArrayList<>();
                this.f2710v = new ArrayList<>();
            }
            this.f2709u.add(transitionName);
            this.f2710v.add(str);
        }
        return this;
    }

    public FragmentTransaction addToBackStack(String str) {
        if (!this.f2701m) {
            throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
        }
        this.f2700l = true;
        this.f2702n = str;
        return this;
    }

    public FragmentTransaction attach(Fragment fragment) {
        C0432Op op = new C0432Op();
        op.f2730c = 7;
        op.f2731d = fragment;
        mo1952a(op);
        return this;
    }

    public void calculateBackFragments(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        if (this.f2690b.f2791p.onHasView()) {
            for (C0432Op op = this.f2692d; op != null; op = op.f2729b) {
                switch (op.f2730c) {
                    case 1:
                        m3551a(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 2:
                        if (op.f2736i != null) {
                            for (int size = op.f2736i.size() - 1; size >= 0; size--) {
                                m3558b(sparseArray, sparseArray2, (Fragment) op.f2736i.get(size));
                            }
                        }
                        m3551a(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 3:
                        m3558b(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 4:
                        m3558b(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 5:
                        m3551a(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 6:
                        m3558b(sparseArray, sparseArray2, op.f2731d);
                        break;
                    case 7:
                        m3551a(sparseArray, sparseArray2, op.f2731d);
                        break;
                }
            }
        }
    }

    public int commit() {
        return mo1949a(false);
    }

    public int commitAllowingStateLoss() {
        return mo1949a(true);
    }

    public void commitNow() {
        disallowAddToBackStack();
        this.f2690b.execSingleAction(this, false);
    }

    public void commitNowAllowingStateLoss() {
        disallowAddToBackStack();
        this.f2690b.execSingleAction(this, true);
    }

    public FragmentTransaction detach(Fragment fragment) {
        C0432Op op = new C0432Op();
        op.f2730c = 6;
        op.f2731d = fragment;
        mo1952a(op);
        return this;
    }

    public FragmentTransaction disallowAddToBackStack() {
        if (this.f2700l) {
            throw new IllegalStateException("This transaction is already being added to the back stack");
        }
        this.f2701m = false;
        return this;
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        dump(str, printWriter, true);
    }

    public void dump(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.f2702n);
            printWriter.print(" mIndex=");
            printWriter.print(this.f2704p);
            printWriter.print(" mCommitted=");
            printWriter.println(this.f2703o);
            if (this.f2698j != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f2698j));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.f2699k));
            }
            if (!(this.f2694f == 0 && this.f2695g == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f2694f));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f2695g));
            }
            if (!(this.f2696h == 0 && this.f2697i == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f2696h));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.f2697i));
            }
            if (!(this.f2705q == 0 && this.f2706r == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.f2705q));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.f2706r);
            }
            if (!(this.f2707s == 0 && this.f2708t == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.f2707s));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.f2708t);
            }
        }
        if (this.f2691c != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            C0432Op op = this.f2691c;
            int i = 0;
            while (op != null) {
                switch (op.f2730c) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + op.f2730c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(op.f2731d);
                if (z) {
                    if (!(op.f2732e == 0 && op.f2733f == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(op.f2732e));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(op.f2733f));
                    }
                    if (!(op.f2734g == 0 && op.f2735h == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(op.f2734g));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(op.f2735h));
                    }
                }
                if (op.f2736i != null && op.f2736i.size() > 0) {
                    for (int i2 = 0; i2 < op.f2736i.size(); i2++) {
                        printWriter.print(str3);
                        if (op.f2736i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i2 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i2);
                            printWriter.print(": ");
                        }
                        printWriter.println(op.f2736i.get(i2));
                    }
                }
                op = op.f2728a;
                i++;
            }
        }
    }

    public CharSequence getBreadCrumbShortTitle() {
        return this.f2707s != 0 ? this.f2690b.f2790o.mo2253b().getText(this.f2707s) : this.f2708t;
    }

    public int getBreadCrumbShortTitleRes() {
        return this.f2707s;
    }

    public CharSequence getBreadCrumbTitle() {
        return this.f2705q != 0 ? this.f2690b.f2790o.mo2253b().getText(this.f2705q) : this.f2706r;
    }

    public int getBreadCrumbTitleRes() {
        return this.f2705q;
    }

    public int getId() {
        return this.f2704p;
    }

    public String getName() {
        return this.f2702n;
    }

    public int getTransition() {
        return this.f2698j;
    }

    public int getTransitionStyle() {
        return this.f2699k;
    }

    public FragmentTransaction hide(Fragment fragment) {
        C0432Op op = new C0432Op();
        op.f2730c = 4;
        op.f2731d = fragment;
        mo1952a(op);
        return this;
    }

    public boolean isAddToBackStackAllowed() {
        return this.f2701m;
    }

    public boolean isEmpty() {
        return this.f2693e == 0;
    }

    public TransitionState popFromBackStack(boolean z, TransitionState transitionState, SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        if (FragmentManagerImpl.f2775a) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            dump("  ", null, new PrintWriter(new LogWriter("FragmentManager")), null);
        }
        if (f2689a && this.f2690b.f2789n >= 1) {
            if (transitionState == null) {
                if (!(sparseArray.size() == 0 && sparseArray2.size() == 0)) {
                    transitionState = m3540a(sparseArray, sparseArray2, true);
                }
            } else if (!z) {
                m3549a(transitionState, this.f2710v, this.f2709u);
            }
        }
        mo1951a(-1);
        int i = transitionState != null ? 0 : this.f2699k;
        int i2 = transitionState != null ? 0 : this.f2698j;
        C0432Op op = this.f2692d;
        while (op != null) {
            int i3 = transitionState != null ? 0 : op.f2734g;
            int i4 = transitionState != null ? 0 : op.f2735h;
            switch (op.f2730c) {
                case 1:
                    Fragment fragment = op.f2731d;
                    fragment.mNextAnim = i4;
                    this.f2690b.removeFragment(fragment, FragmentManagerImpl.reverseTransit(i2), i);
                    break;
                case 2:
                    Fragment fragment2 = op.f2731d;
                    if (fragment2 != null) {
                        fragment2.mNextAnim = i4;
                        this.f2690b.removeFragment(fragment2, FragmentManagerImpl.reverseTransit(i2), i);
                    }
                    if (op.f2736i == null) {
                        break;
                    } else {
                        for (int i5 = 0; i5 < op.f2736i.size(); i5++) {
                            Fragment fragment3 = (Fragment) op.f2736i.get(i5);
                            fragment3.mNextAnim = i3;
                            this.f2690b.addFragment(fragment3, false);
                        }
                        break;
                    }
                case 3:
                    Fragment fragment4 = op.f2731d;
                    fragment4.mNextAnim = i3;
                    this.f2690b.addFragment(fragment4, false);
                    break;
                case 4:
                    Fragment fragment5 = op.f2731d;
                    fragment5.mNextAnim = i3;
                    this.f2690b.showFragment(fragment5, FragmentManagerImpl.reverseTransit(i2), i);
                    break;
                case 5:
                    Fragment fragment6 = op.f2731d;
                    fragment6.mNextAnim = i4;
                    this.f2690b.hideFragment(fragment6, FragmentManagerImpl.reverseTransit(i2), i);
                    break;
                case 6:
                    Fragment fragment7 = op.f2731d;
                    fragment7.mNextAnim = i3;
                    this.f2690b.attachFragment(fragment7, FragmentManagerImpl.reverseTransit(i2), i);
                    break;
                case 7:
                    Fragment fragment8 = op.f2731d;
                    fragment8.mNextAnim = i3;
                    this.f2690b.detachFragment(fragment8, FragmentManagerImpl.reverseTransit(i2), i);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + op.f2730c);
            }
            op = op.f2729b;
        }
        if (z) {
            this.f2690b.mo2287a(this.f2690b.f2789n, FragmentManagerImpl.reverseTransit(i2), i, true);
            transitionState = null;
        }
        if (this.f2704p >= 0) {
            this.f2690b.freeBackStackIndex(this.f2704p);
            this.f2704p = -1;
        }
        return transitionState;
    }

    public FragmentTransaction remove(Fragment fragment) {
        C0432Op op = new C0432Op();
        op.f2730c = 3;
        op.f2731d = fragment;
        mo1952a(op);
        return this;
    }

    public FragmentTransaction replace(int i, Fragment fragment) {
        return replace(i, fragment, null);
    }

    public FragmentTransaction replace(int i, Fragment fragment, String str) {
        if (i == 0) {
            throw new IllegalArgumentException("Must use non-zero containerViewId");
        }
        m3546a(i, fragment, str, 2);
        return this;
    }

    public void run() {
        if (FragmentManagerImpl.f2775a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.f2700l || this.f2704p >= 0) {
            mo1951a(1);
            TransitionState transitionState = null;
            if (f2689a && this.f2690b.f2789n >= 1) {
                SparseArray sparseArray = new SparseArray();
                SparseArray sparseArray2 = new SparseArray();
                m3550a(sparseArray, sparseArray2);
                transitionState = m3540a(sparseArray, sparseArray2, false);
            }
            int i = transitionState != null ? 0 : this.f2699k;
            int i2 = transitionState != null ? 0 : this.f2698j;
            C0432Op op = this.f2691c;
            while (op != null) {
                int i3 = transitionState != null ? 0 : op.f2732e;
                int i4 = transitionState != null ? 0 : op.f2733f;
                switch (op.f2730c) {
                    case 1:
                        Fragment fragment = op.f2731d;
                        fragment.mNextAnim = i3;
                        this.f2690b.addFragment(fragment, false);
                        break;
                    case 2:
                        Fragment fragment2 = op.f2731d;
                        int i5 = fragment2.mContainerId;
                        if (this.f2690b.f2782g != null) {
                            for (int size = this.f2690b.f2782g.size() - 1; size >= 0; size--) {
                                Fragment fragment3 = (Fragment) this.f2690b.f2782g.get(size);
                                if (FragmentManagerImpl.f2775a) {
                                    Log.v("FragmentManager", "OP_REPLACE: adding=" + fragment2 + " old=" + fragment3);
                                }
                                if (fragment3.mContainerId == i5) {
                                    if (fragment3 == fragment2) {
                                        fragment2 = null;
                                        op.f2731d = null;
                                    } else {
                                        if (op.f2736i == null) {
                                            op.f2736i = new ArrayList<>();
                                        }
                                        op.f2736i.add(fragment3);
                                        fragment3.mNextAnim = i4;
                                        if (this.f2700l) {
                                            fragment3.mBackStackNesting++;
                                            if (FragmentManagerImpl.f2775a) {
                                                Log.v("FragmentManager", "Bump nesting of " + fragment3 + " to " + fragment3.mBackStackNesting);
                                            }
                                        }
                                        this.f2690b.removeFragment(fragment3, i2, i);
                                    }
                                }
                            }
                        }
                        if (fragment2 == null) {
                            break;
                        } else {
                            fragment2.mNextAnim = i3;
                            this.f2690b.addFragment(fragment2, false);
                            break;
                        }
                    case 3:
                        Fragment fragment4 = op.f2731d;
                        fragment4.mNextAnim = i4;
                        this.f2690b.removeFragment(fragment4, i2, i);
                        break;
                    case 4:
                        Fragment fragment5 = op.f2731d;
                        fragment5.mNextAnim = i4;
                        this.f2690b.hideFragment(fragment5, i2, i);
                        break;
                    case 5:
                        Fragment fragment6 = op.f2731d;
                        fragment6.mNextAnim = i3;
                        this.f2690b.showFragment(fragment6, i2, i);
                        break;
                    case 6:
                        Fragment fragment7 = op.f2731d;
                        fragment7.mNextAnim = i4;
                        this.f2690b.detachFragment(fragment7, i2, i);
                        break;
                    case 7:
                        Fragment fragment8 = op.f2731d;
                        fragment8.mNextAnim = i3;
                        this.f2690b.attachFragment(fragment8, i2, i);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + op.f2730c);
                }
                op = op.f2728a;
            }
            this.f2690b.mo2287a(this.f2690b.f2789n, i2, i, true);
            if (this.f2700l) {
                this.f2690b.mo2290a(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    public FragmentTransaction setBreadCrumbShortTitle(int i) {
        this.f2707s = i;
        this.f2708t = null;
        return this;
    }

    public FragmentTransaction setBreadCrumbShortTitle(CharSequence charSequence) {
        this.f2707s = 0;
        this.f2708t = charSequence;
        return this;
    }

    public FragmentTransaction setBreadCrumbTitle(int i) {
        this.f2705q = i;
        this.f2706r = null;
        return this;
    }

    public FragmentTransaction setBreadCrumbTitle(CharSequence charSequence) {
        this.f2705q = 0;
        this.f2706r = charSequence;
        return this;
    }

    public FragmentTransaction setCustomAnimations(int i, int i2) {
        return setCustomAnimations(i, i2, 0, 0);
    }

    public FragmentTransaction setCustomAnimations(int i, int i2, int i3, int i4) {
        this.f2694f = i;
        this.f2695g = i2;
        this.f2696h = i3;
        this.f2697i = i4;
        return this;
    }

    public FragmentTransaction setTransition(int i) {
        this.f2698j = i;
        return this;
    }

    public FragmentTransaction setTransitionStyle(int i) {
        this.f2699k = i;
        return this;
    }

    public FragmentTransaction show(Fragment fragment) {
        C0432Op op = new C0432Op();
        op.f2730c = 5;
        op.f2731d = fragment;
        mo1952a(op);
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(NotificationCompat.FLAG_HIGH_PRIORITY);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.f2704p >= 0) {
            sb.append(" #");
            sb.append(this.f2704p);
        }
        if (this.f2702n != null) {
            sb.append(" ");
            sb.append(this.f2702n);
        }
        sb.append("}");
        return sb.toString();
    }
}
