package android.support.p001v4.app;

import java.util.List;

/* renamed from: android.support.v4.app.FragmentManagerNonConfig */
public class FragmentManagerNonConfig {

    /* renamed from: a */
    private final List<Fragment> f2815a;

    /* renamed from: b */
    private final List<FragmentManagerNonConfig> f2816b;

    FragmentManagerNonConfig(List<Fragment> list, List<FragmentManagerNonConfig> list2) {
        this.f2815a = list;
        this.f2816b = list2;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public List<Fragment> mo2348a() {
        return this.f2815a;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public List<FragmentManagerNonConfig> mo2349b() {
        return this.f2816b;
    }
}
