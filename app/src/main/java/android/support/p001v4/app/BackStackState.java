package android.support.p001v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

/* renamed from: android.support.v4.app.BackStackState */
/* compiled from: BackStackRecord */
final class BackStackState implements Parcelable {
    public static final Creator<BackStackState> CREATOR = new Creator<BackStackState>() {
        public BackStackState createFromParcel(Parcel parcel) {
            return new BackStackState(parcel);
        }

        public BackStackState[] newArray(int i) {
            return new BackStackState[i];
        }
    };

    /* renamed from: a */
    final int[] f2738a;

    /* renamed from: b */
    final int f2739b;

    /* renamed from: c */
    final int f2740c;

    /* renamed from: d */
    final String f2741d;

    /* renamed from: e */
    final int f2742e;

    /* renamed from: f */
    final int f2743f;

    /* renamed from: g */
    final CharSequence f2744g;

    /* renamed from: h */
    final int f2745h;

    /* renamed from: i */
    final CharSequence f2746i;

    /* renamed from: j */
    final ArrayList<String> f2747j;

    /* renamed from: k */
    final ArrayList<String> f2748k;

    public BackStackState(Parcel parcel) {
        this.f2738a = parcel.createIntArray();
        this.f2739b = parcel.readInt();
        this.f2740c = parcel.readInt();
        this.f2741d = parcel.readString();
        this.f2742e = parcel.readInt();
        this.f2743f = parcel.readInt();
        this.f2744g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f2745h = parcel.readInt();
        this.f2746i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f2747j = parcel.createStringArrayList();
        this.f2748k = parcel.createStringArrayList();
    }

    public BackStackState(BackStackRecord backStackRecord) {
        int i;
        int i2 = 0;
        for (C0432Op op = backStackRecord.f2691c; op != null; op = op.f2728a) {
            if (op.f2736i != null) {
                i2 += op.f2736i.size();
            }
        }
        this.f2738a = new int[((backStackRecord.f2693e * 7) + i2)];
        if (!backStackRecord.f2700l) {
            throw new IllegalStateException("Not on back stack");
        }
        C0432Op op2 = backStackRecord.f2691c;
        int i3 = 0;
        while (op2 != null) {
            int i4 = i3 + 1;
            this.f2738a[i3] = op2.f2730c;
            int i5 = i4 + 1;
            this.f2738a[i4] = op2.f2731d != null ? op2.f2731d.mIndex : -1;
            int i6 = i5 + 1;
            this.f2738a[i5] = op2.f2732e;
            int i7 = i6 + 1;
            this.f2738a[i6] = op2.f2733f;
            int i8 = i7 + 1;
            this.f2738a[i7] = op2.f2734g;
            int i9 = i8 + 1;
            this.f2738a[i8] = op2.f2735h;
            if (op2.f2736i != null) {
                int size = op2.f2736i.size();
                int i10 = i9 + 1;
                this.f2738a[i9] = size;
                int i11 = 0;
                int i12 = i10;
                while (i11 < size) {
                    int i13 = i12 + 1;
                    this.f2738a[i12] = ((Fragment) op2.f2736i.get(i11)).mIndex;
                    i11++;
                    i12 = i13;
                }
                i = i12;
            } else {
                i = i9 + 1;
                this.f2738a[i9] = 0;
            }
            op2 = op2.f2728a;
            i3 = i;
        }
        this.f2739b = backStackRecord.f2698j;
        this.f2740c = backStackRecord.f2699k;
        this.f2741d = backStackRecord.f2702n;
        this.f2742e = backStackRecord.f2704p;
        this.f2743f = backStackRecord.f2705q;
        this.f2744g = backStackRecord.f2706r;
        this.f2745h = backStackRecord.f2707s;
        this.f2746i = backStackRecord.f2708t;
        this.f2747j = backStackRecord.f2709u;
        this.f2748k = backStackRecord.f2710v;
    }

    public int describeContents() {
        return 0;
    }

    public BackStackRecord instantiate(FragmentManagerImpl fragmentManagerImpl) {
        BackStackRecord backStackRecord = new BackStackRecord(fragmentManagerImpl);
        int i = 0;
        int i2 = 0;
        while (i < this.f2738a.length) {
            C0432Op op = new C0432Op();
            int i3 = i + 1;
            op.f2730c = this.f2738a[i];
            if (FragmentManagerImpl.f2775a) {
                Log.v("FragmentManager", "Instantiate " + backStackRecord + " op #" + i2 + " base fragment #" + this.f2738a[i3]);
            }
            int i4 = i3 + 1;
            int i5 = this.f2738a[i3];
            if (i5 >= 0) {
                op.f2731d = (Fragment) fragmentManagerImpl.f2781f.get(i5);
            } else {
                op.f2731d = null;
            }
            int i6 = i4 + 1;
            op.f2732e = this.f2738a[i4];
            int i7 = i6 + 1;
            op.f2733f = this.f2738a[i6];
            int i8 = i7 + 1;
            op.f2734g = this.f2738a[i7];
            int i9 = i8 + 1;
            op.f2735h = this.f2738a[i8];
            int i10 = i9 + 1;
            int i11 = this.f2738a[i9];
            if (i11 > 0) {
                op.f2736i = new ArrayList<>(i11);
                int i12 = 0;
                while (i12 < i11) {
                    if (FragmentManagerImpl.f2775a) {
                        Log.v("FragmentManager", "Instantiate " + backStackRecord + " set remove fragment #" + this.f2738a[i10]);
                    }
                    int i13 = i10 + 1;
                    op.f2736i.add((Fragment) fragmentManagerImpl.f2781f.get(this.f2738a[i10]));
                    i12++;
                    i10 = i13;
                }
            }
            i = i10;
            backStackRecord.f2694f = op.f2732e;
            backStackRecord.f2695g = op.f2733f;
            backStackRecord.f2696h = op.f2734g;
            backStackRecord.f2697i = op.f2735h;
            backStackRecord.mo1952a(op);
            i2++;
        }
        backStackRecord.f2698j = this.f2739b;
        backStackRecord.f2699k = this.f2740c;
        backStackRecord.f2702n = this.f2741d;
        backStackRecord.f2704p = this.f2742e;
        backStackRecord.f2700l = true;
        backStackRecord.f2705q = this.f2743f;
        backStackRecord.f2706r = this.f2744g;
        backStackRecord.f2707s = this.f2745h;
        backStackRecord.f2708t = this.f2746i;
        backStackRecord.f2709u = this.f2747j;
        backStackRecord.f2710v = this.f2748k;
        backStackRecord.mo1951a(1);
        return backStackRecord;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(this.f2738a);
        parcel.writeInt(this.f2739b);
        parcel.writeInt(this.f2740c);
        parcel.writeString(this.f2741d);
        parcel.writeInt(this.f2742e);
        parcel.writeInt(this.f2743f);
        TextUtils.writeToParcel(this.f2744g, parcel, 0);
        parcel.writeInt(this.f2745h);
        TextUtils.writeToParcel(this.f2746i, parcel, 0);
        parcel.writeStringList(this.f2747j);
        parcel.writeStringList(this.f2748k);
    }
}
