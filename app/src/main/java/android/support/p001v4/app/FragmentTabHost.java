package android.support.p001v4.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.BaseSavedState;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import java.util.ArrayList;

/* renamed from: android.support.v4.app.FragmentTabHost */
public class FragmentTabHost extends TabHost implements OnTabChangeListener {

    /* renamed from: a */
    private final ArrayList<TabInfo> f2840a = new ArrayList<>();

    /* renamed from: b */
    private FrameLayout f2841b;

    /* renamed from: c */
    private Context f2842c;

    /* renamed from: d */
    private FragmentManager f2843d;

    /* renamed from: e */
    private int f2844e;

    /* renamed from: f */
    private OnTabChangeListener f2845f;

    /* renamed from: g */
    private TabInfo f2846g;

    /* renamed from: h */
    private boolean f2847h;

    /* renamed from: android.support.v4.app.FragmentTabHost$DummyTabFactory */
    static class DummyTabFactory implements TabContentFactory {

        /* renamed from: a */
        private final Context f2848a;

        public DummyTabFactory(Context context) {
            this.f2848a = context;
        }

        public View createTabContent(String str) {
            View view = new View(this.f2848a);
            view.setMinimumWidth(0);
            view.setMinimumHeight(0);
            return view;
        }
    }

    /* renamed from: android.support.v4.app.FragmentTabHost$SavedState */
    static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a */
        String f2849a;

        SavedState(Parcel parcel) {
            super(parcel);
            this.f2849a = parcel.readString();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.f2849a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f2849a);
        }
    }

    /* renamed from: android.support.v4.app.FragmentTabHost$TabInfo */
    static final class TabInfo {

        /* renamed from: a */
        final String f2850a;

        /* renamed from: b */
        final Class<?> f2851b;

        /* renamed from: c */
        final Bundle f2852c;

        /* renamed from: d */
        Fragment f2853d;

        TabInfo(String str, Class<?> cls, Bundle bundle) {
            this.f2850a = str;
            this.f2851b = cls;
            this.f2852c = bundle;
        }
    }

    public FragmentTabHost(Context context) {
        super(context, null);
        m3615a(context, (AttributeSet) null);
    }

    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m3615a(context, attributeSet);
    }

    /* renamed from: a */
    private TabInfo m3611a(String str) {
        int size = this.f2840a.size();
        for (int i = 0; i < size; i++) {
            TabInfo tabInfo = (TabInfo) this.f2840a.get(i);
            if (tabInfo.f2850a.equals(str)) {
                return tabInfo;
            }
        }
        return null;
    }

    /* renamed from: a */
    private FragmentTransaction m3612a(String str, FragmentTransaction fragmentTransaction) {
        TabInfo a = m3611a(str);
        if (this.f2846g != a) {
            if (fragmentTransaction == null) {
                fragmentTransaction = this.f2843d.beginTransaction();
            }
            if (!(this.f2846g == null || this.f2846g.f2853d == null)) {
                fragmentTransaction.detach(this.f2846g.f2853d);
            }
            if (a != null) {
                if (a.f2853d == null) {
                    a.f2853d = Fragment.instantiate(this.f2842c, a.f2851b.getName(), a.f2852c);
                    fragmentTransaction.add(this.f2844e, a.f2853d, a.f2850a);
                } else {
                    fragmentTransaction.attach(a.f2853d);
                }
            }
            this.f2846g = a;
        }
        return fragmentTransaction;
    }

    /* renamed from: a */
    private void m3613a() {
        if (this.f2841b == null) {
            this.f2841b = (FrameLayout) findViewById(this.f2844e);
            if (this.f2841b == null) {
                throw new IllegalStateException("No tab content FrameLayout found for id " + this.f2844e);
            }
        }
    }

    /* renamed from: a */
    private void m3614a(Context context) {
        if (findViewById(16908307) == null) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            addView(linearLayout, new LayoutParams(-1, -1));
            TabWidget tabWidget = new TabWidget(context);
            tabWidget.setId(16908307);
            tabWidget.setOrientation(0);
            linearLayout.addView(tabWidget, new LinearLayout.LayoutParams(-1, -2, 0.0f));
            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setId(16908305);
            linearLayout.addView(frameLayout, new LinearLayout.LayoutParams(0, 0, 0.0f));
            FrameLayout frameLayout2 = new FrameLayout(context);
            this.f2841b = frameLayout2;
            this.f2841b.setId(this.f2844e);
            linearLayout.addView(frameLayout2, new LinearLayout.LayoutParams(-1, 0, 1.0f));
        }
    }

    /* renamed from: a */
    private void m3615a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.f2844e = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
    }

    public void addTab(TabSpec tabSpec, Class<?> cls, Bundle bundle) {
        tabSpec.setContent(new DummyTabFactory(this.f2842c));
        String tag = tabSpec.getTag();
        TabInfo tabInfo = new TabInfo(tag, cls, bundle);
        if (this.f2847h) {
            tabInfo.f2853d = this.f2843d.findFragmentByTag(tag);
            if (tabInfo.f2853d != null && !tabInfo.f2853d.isDetached()) {
                FragmentTransaction beginTransaction = this.f2843d.beginTransaction();
                beginTransaction.detach(tabInfo.f2853d);
                beginTransaction.commit();
            }
        }
        this.f2840a.add(tabInfo);
        addTab(tabSpec);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        FragmentTransaction fragmentTransaction = null;
        int size = this.f2840a.size();
        for (int i = 0; i < size; i++) {
            TabInfo tabInfo = (TabInfo) this.f2840a.get(i);
            tabInfo.f2853d = this.f2843d.findFragmentByTag(tabInfo.f2850a);
            if (tabInfo.f2853d != null && !tabInfo.f2853d.isDetached()) {
                if (tabInfo.f2850a.equals(currentTabTag)) {
                    this.f2846g = tabInfo;
                } else {
                    if (fragmentTransaction == null) {
                        fragmentTransaction = this.f2843d.beginTransaction();
                    }
                    fragmentTransaction.detach(tabInfo.f2853d);
                }
            }
        }
        this.f2847h = true;
        FragmentTransaction a = m3612a(currentTabTag, fragmentTransaction);
        if (a != null) {
            a.commit();
            this.f2843d.executePendingTransactions();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f2847h = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.f2849a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2849a = getCurrentTabTag();
        return savedState;
    }

    public void onTabChanged(String str) {
        if (this.f2847h) {
            FragmentTransaction a = m3612a(str, (FragmentTransaction) null);
            if (a != null) {
                a.commit();
            }
        }
        if (this.f2845f != null) {
            this.f2845f.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(OnTabChangeListener onTabChangeListener) {
        this.f2845f = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    public void setup(Context context, FragmentManager fragmentManager) {
        m3614a(context);
        super.setup();
        this.f2842c = context;
        this.f2843d = fragmentManager;
        m3613a();
    }

    public void setup(Context context, FragmentManager fragmentManager, int i) {
        m3614a(context);
        super.setup();
        this.f2842c = context;
        this.f2843d = fragmentManager;
        this.f2844e = i;
        m3613a();
        this.f2841b.setId(i);
        if (getId() == -1) {
            setId(16908306);
        }
    }
}
