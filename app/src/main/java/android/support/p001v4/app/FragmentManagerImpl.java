package android.support.p001v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.p001v4.app.BackStackRecord.TransitionState;
import android.support.p001v4.app.Fragment.SavedState;
import android.support.p001v4.app.FragmentManager.BackStackEntry;
import android.support.p001v4.app.FragmentManager.OnBackStackChangedListener;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: android.support.v4.app.FragmentManagerImpl */
/* compiled from: FragmentManager */
final class FragmentManagerImpl extends FragmentManager implements LayoutInflaterFactory {

    /* renamed from: A */
    static final Interpolator f2771A = new DecelerateInterpolator(2.5f);
    public static final int ANIM_STYLE_CLOSE_ENTER = 3;
    public static final int ANIM_STYLE_CLOSE_EXIT = 4;
    public static final int ANIM_STYLE_FADE_ENTER = 5;
    public static final int ANIM_STYLE_FADE_EXIT = 6;
    public static final int ANIM_STYLE_OPEN_ENTER = 1;
    public static final int ANIM_STYLE_OPEN_EXIT = 2;

    /* renamed from: B */
    static final Interpolator f2772B = new DecelerateInterpolator(1.5f);

    /* renamed from: C */
    static final Interpolator f2773C = new AccelerateInterpolator(2.5f);

    /* renamed from: D */
    static final Interpolator f2774D = new AccelerateInterpolator(1.5f);

    /* renamed from: a */
    static boolean f2775a = false;

    /* renamed from: b */
    static final boolean f2776b;

    /* renamed from: r */
    static Field f2777r = null;

    /* renamed from: c */
    ArrayList<Runnable> f2778c;

    /* renamed from: d */
    Runnable[] f2779d;

    /* renamed from: e */
    boolean f2780e;

    /* renamed from: f */
    ArrayList<Fragment> f2781f;

    /* renamed from: g */
    ArrayList<Fragment> f2782g;

    /* renamed from: h */
    ArrayList<Integer> f2783h;

    /* renamed from: i */
    ArrayList<BackStackRecord> f2784i;

    /* renamed from: j */
    ArrayList<Fragment> f2785j;

    /* renamed from: k */
    ArrayList<BackStackRecord> f2786k;

    /* renamed from: l */
    ArrayList<Integer> f2787l;

    /* renamed from: m */
    ArrayList<OnBackStackChangedListener> f2788m;

    /* renamed from: n */
    int f2789n = 0;

    /* renamed from: o */
    FragmentHostCallback f2790o;

    /* renamed from: p */
    FragmentContainer f2791p;

    /* renamed from: q */
    Fragment f2792q;

    /* renamed from: s */
    boolean f2793s;

    /* renamed from: t */
    boolean f2794t;

    /* renamed from: u */
    boolean f2795u;

    /* renamed from: v */
    String f2796v;

    /* renamed from: w */
    boolean f2797w;

    /* renamed from: x */
    Bundle f2798x = null;

    /* renamed from: y */
    SparseArray<Parcelable> f2799y = null;

    /* renamed from: z */
    Runnable f2800z = new Runnable() {
        public void run() {
            FragmentManagerImpl.this.execPendingActions();
        }
    };

    /* renamed from: android.support.v4.app.FragmentManagerImpl$AnimateOnHWLayerIfNeededListener */
    /* compiled from: FragmentManager */
    static class AnimateOnHWLayerIfNeededListener implements AnimationListener {

        /* renamed from: a */
        private AnimationListener f2811a;

        /* renamed from: b */
        private boolean f2812b;

        /* renamed from: c */
        View f2813c;

        public AnimateOnHWLayerIfNeededListener(View view, Animation animation) {
            if (view != null && animation != null) {
                this.f2813c = view;
            }
        }

        public AnimateOnHWLayerIfNeededListener(View view, Animation animation, AnimationListener animationListener) {
            if (view != null && animation != null) {
                this.f2811a = animationListener;
                this.f2813c = view;
                this.f2812b = true;
            }
        }

        public void onAnimationEnd(Animation animation) {
            if (this.f2813c != null && this.f2812b) {
                if (ViewCompat.m12879H(this.f2813c) || BuildCompat.m12482a()) {
                    this.f2813c.post(new Runnable() {
                        public void run() {
                            ViewCompat.m12888a(AnimateOnHWLayerIfNeededListener.this.f2813c, 0, (Paint) null);
                        }
                    });
                } else {
                    ViewCompat.m12888a(this.f2813c, 0, (Paint) null);
                }
            }
            if (this.f2811a != null) {
                this.f2811a.onAnimationEnd(animation);
            }
        }

        public void onAnimationRepeat(Animation animation) {
            if (this.f2811a != null) {
                this.f2811a.onAnimationRepeat(animation);
            }
        }

        public void onAnimationStart(Animation animation) {
            if (this.f2811a != null) {
                this.f2811a.onAnimationStart(animation);
            }
        }
    }

    /* renamed from: android.support.v4.app.FragmentManagerImpl$FragmentTag */
    /* compiled from: FragmentManager */
    static class FragmentTag {
        public static final int[] Fragment = {16842755, 16842960, 16842961};
        public static final int Fragment_id = 1;
        public static final int Fragment_name = 0;
        public static final int Fragment_tag = 2;

        FragmentTag() {
        }
    }

    static {
        boolean z = false;
        if (VERSION.SDK_INT >= 11) {
            z = true;
        }
        f2776b = z;
    }

    FragmentManagerImpl() {
    }

    /* renamed from: a */
    static Animation m3582a(Context context, float f, float f2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2);
        alphaAnimation.setInterpolator(f2772B);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    /* renamed from: a */
    static Animation m3583a(Context context, float f, float f2, float f3, float f4) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f, f2, f, f2, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(f2771A);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f4);
        alphaAnimation.setInterpolator(f2772B);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    /* renamed from: a */
    private void m3584a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
        if (this.f2790o != null) {
            try {
                this.f2790o.onDump("  ", null, printWriter, new String[0]);
            } catch (Exception e) {
                Log.e("FragmentManager", "Failed dumping state", e);
            }
        } else {
            try {
                dump("  ", null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        }
        throw runtimeException;
    }

    /* renamed from: a */
    static boolean m3585a(View view, Animation animation) {
        return VERSION.SDK_INT >= 19 && ViewCompat.m12922g(view) == 0 && ViewCompat.m12941z(view) && m3586a(animation);
    }

    /* renamed from: a */
    static boolean m3586a(Animation animation) {
        if (animation instanceof AlphaAnimation) {
            return true;
        }
        if (animation instanceof AnimationSet) {
            List animations = ((AnimationSet) animation).getAnimations();
            for (int i = 0; i < animations.size(); i++) {
                if (animations.get(i) instanceof AlphaAnimation) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: b */
    private void m3587b(View view, Animation animation) {
        if (view != null && animation != null && m3585a(view, animation)) {
            AnimationListener animationListener = null;
            try {
                if (f2777r == null) {
                    f2777r = Animation.class.getDeclaredField("mListener");
                    f2777r.setAccessible(true);
                }
                animationListener = (AnimationListener) f2777r.get(animation);
            } catch (NoSuchFieldException e) {
                Log.e("FragmentManager", "No field with the name mListener is found in Animation class", e);
            } catch (IllegalAccessException e2) {
                Log.e("FragmentManager", "Cannot access Animation's mListener field", e2);
            }
            ViewCompat.m12888a(view, 2, (Paint) null);
            animation.setAnimationListener(new AnimateOnHWLayerIfNeededListener(view, animation, animationListener));
        }
    }

    /* renamed from: g */
    private void m3588g() {
        if (this.f2794t) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.f2796v != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.f2796v);
        }
    }

    public static int reverseTransit(int i) {
        switch (i) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*4097*/:
                return FragmentTransaction.TRANSIT_FRAGMENT_CLOSE;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*4099*/:
                return FragmentTransaction.TRANSIT_FRAGMENT_FADE;
            case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE /*8194*/:
                return FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
            default:
                return 0;
        }
    }

    public static int transitToStyleIndex(int i, boolean z) {
        switch (i) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*4097*/:
                return z ? 1 : 2;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*4099*/:
                return z ? 5 : 6;
            case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE /*8194*/:
                return z ? 3 : 4;
            default:
                return -1;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public Animation mo2285a(Fragment fragment, int i, boolean z, int i2) {
        Animation onCreateAnimation = fragment.onCreateAnimation(i, z, fragment.mNextAnim);
        if (onCreateAnimation != null) {
            return onCreateAnimation;
        }
        if (fragment.mNextAnim != 0) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.f2790o.mo2253b(), fragment.mNextAnim);
            if (loadAnimation != null) {
                return loadAnimation;
            }
        }
        if (i == 0) {
            return null;
        }
        int transitToStyleIndex = transitToStyleIndex(i, z);
        if (transitToStyleIndex < 0) {
            return null;
        }
        switch (transitToStyleIndex) {
            case 1:
                return m3583a(this.f2790o.mo2253b(), 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return m3583a(this.f2790o.mo2253b(), 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return m3583a(this.f2790o.mo2253b(), 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return m3583a(this.f2790o.mo2253b(), 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return m3582a(this.f2790o.mo2253b(), 0.0f, 1.0f);
            case 6:
                return m3582a(this.f2790o.mo2253b(), 1.0f, 0.0f);
            default:
                if (i2 == 0 && this.f2790o.onHasWindowAnimations()) {
                    i2 = this.f2790o.onGetWindowAnimations();
                }
                return i2 == 0 ? null : null;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2286a() {
        if (this.f2781f != null) {
            for (int i = 0; i < this.f2781f.size(); i++) {
                Fragment fragment = (Fragment) this.f2781f.get(i);
                if (fragment != null) {
                    performPendingDeferredStart(fragment);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2287a(int i, int i2, int i3, boolean z) {
        if (this.f2790o == null && i != 0) {
            throw new IllegalStateException("No host");
        } else if (z || this.f2789n != i) {
            this.f2789n = i;
            if (this.f2781f != null) {
                boolean z2 = false;
                for (int i4 = 0; i4 < this.f2781f.size(); i4++) {
                    Fragment fragment = (Fragment) this.f2781f.get(i4);
                    if (fragment != null) {
                        mo2292a(fragment, i, i2, i3, false);
                        if (fragment.mLoaderManager != null) {
                            z2 |= fragment.mLoaderManager.hasRunningLoaders();
                        }
                    }
                }
                if (!z2) {
                    mo2286a();
                }
                if (this.f2793s && this.f2790o != null && this.f2789n == 5) {
                    this.f2790o.onSupportInvalidateOptionsMenu();
                    this.f2793s = false;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2288a(int i, boolean z) {
        mo2287a(i, 0, 0, z);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2289a(Parcelable parcelable, FragmentManagerNonConfig fragmentManagerNonConfig) {
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.f2817a != null) {
                List list = null;
                if (fragmentManagerNonConfig != null) {
                    List a = fragmentManagerNonConfig.mo2348a();
                    list = fragmentManagerNonConfig.mo2349b();
                    int i = a != null ? a.size() : 0;
                    for (int i2 = 0; i2 < i; i2++) {
                        Fragment fragment = (Fragment) a.get(i2);
                        if (f2775a) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + fragment);
                        }
                        FragmentState fragmentState = fragmentManagerState.f2817a[fragment.mIndex];
                        fragmentState.f2834l = fragment;
                        fragment.mSavedViewState = null;
                        fragment.mBackStackNesting = 0;
                        fragment.mInLayout = false;
                        fragment.mAdded = false;
                        fragment.mTarget = null;
                        if (fragmentState.f2833k != null) {
                            fragmentState.f2833k.setClassLoader(this.f2790o.mo2253b().getClassLoader());
                            fragment.mSavedViewState = fragmentState.f2833k.getSparseParcelableArray("android:view_state");
                            fragment.mSavedFragmentState = fragmentState.f2833k;
                        }
                    }
                }
                this.f2781f = new ArrayList<>(fragmentManagerState.f2817a.length);
                if (this.f2783h != null) {
                    this.f2783h.clear();
                }
                for (int i3 = 0; i3 < fragmentManagerState.f2817a.length; i3++) {
                    FragmentState fragmentState2 = fragmentManagerState.f2817a[i3];
                    if (fragmentState2 != null) {
                        FragmentManagerNonConfig fragmentManagerNonConfig2 = null;
                        if (list != null && i3 < list.size()) {
                            fragmentManagerNonConfig2 = (FragmentManagerNonConfig) list.get(i3);
                        }
                        Fragment instantiate = fragmentState2.instantiate(this.f2790o, this.f2792q, fragmentManagerNonConfig2);
                        if (f2775a) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i3 + ": " + instantiate);
                        }
                        this.f2781f.add(instantiate);
                        fragmentState2.f2834l = null;
                    } else {
                        this.f2781f.add(null);
                        if (this.f2783h == null) {
                            this.f2783h = new ArrayList<>();
                        }
                        if (f2775a) {
                            Log.v("FragmentManager", "restoreAllState: avail #" + i3);
                        }
                        this.f2783h.add(Integer.valueOf(i3));
                    }
                }
                if (fragmentManagerNonConfig != null) {
                    List a2 = fragmentManagerNonConfig.mo2348a();
                    int i4 = a2 != null ? a2.size() : 0;
                    for (int i5 = 0; i5 < i4; i5++) {
                        Fragment fragment2 = (Fragment) a2.get(i5);
                        if (fragment2.mTargetIndex >= 0) {
                            if (fragment2.mTargetIndex < this.f2781f.size()) {
                                fragment2.mTarget = (Fragment) this.f2781f.get(fragment2.mTargetIndex);
                            } else {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + fragment2 + " target no longer exists: " + fragment2.mTargetIndex);
                                fragment2.mTarget = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.f2818b != null) {
                    this.f2782g = new ArrayList<>(fragmentManagerState.f2818b.length);
                    for (int i6 = 0; i6 < fragmentManagerState.f2818b.length; i6++) {
                        Fragment fragment3 = (Fragment) this.f2781f.get(fragmentManagerState.f2818b[i6]);
                        if (fragment3 == null) {
                            m3584a((RuntimeException) new IllegalStateException("No instantiated fragment for index #" + fragmentManagerState.f2818b[i6]));
                        }
                        fragment3.mAdded = true;
                        if (f2775a) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i6 + ": " + fragment3);
                        }
                        if (this.f2782g.contains(fragment3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.f2782g.add(fragment3);
                    }
                } else {
                    this.f2782g = null;
                }
                if (fragmentManagerState.f2819c != null) {
                    this.f2784i = new ArrayList<>(fragmentManagerState.f2819c.length);
                    for (int i7 = 0; i7 < fragmentManagerState.f2819c.length; i7++) {
                        BackStackRecord instantiate2 = fragmentManagerState.f2819c[i7].instantiate(this);
                        if (f2775a) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i7 + " (index " + instantiate2.f2704p + "): " + instantiate2);
                            instantiate2.dump("  ", new PrintWriter(new LogWriter("FragmentManager")), false);
                        }
                        this.f2784i.add(instantiate2);
                        if (instantiate2.f2704p >= 0) {
                            setBackStackIndex(instantiate2.f2704p, instantiate2);
                        }
                    }
                    return;
                }
                this.f2784i = null;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2290a(BackStackRecord backStackRecord) {
        if (this.f2784i == null) {
            this.f2784i = new ArrayList<>();
        }
        this.f2784i.add(backStackRecord);
        mo2301c();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2291a(Fragment fragment) {
        mo2292a(fragment, this.f2789n, 0, 0, false);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0405, code lost:
        r14 = "unknown";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0432, code lost:
        if (r19 >= 1) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0438, code lost:
        if (r17.f2795u == false) goto L_0x044c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x043e, code lost:
        if (r18.mAnimatingAway == null) goto L_0x044c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0440, code lost:
        r15 = r18.mAnimatingAway;
        r18.mAnimatingAway = null;
        r15.clearAnimation();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0450, code lost:
        if (r18.mAnimatingAway == null) goto L_0x057c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0452, code lost:
        r18.mStateAfterAnimating = r19;
        r19 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0485, code lost:
        if (r19 >= 4) goto L_0x04a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0489, code lost:
        if (f2775a == false) goto L_0x04a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x048b, code lost:
        android.util.Log.v("FragmentManager", "movefrom STARTED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x04a5, code lost:
        r18.performStop();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x04ab, code lost:
        if (r19 >= 3) goto L_0x04ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x04af, code lost:
        if (f2775a == false) goto L_0x04cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x04b1, code lost:
        android.util.Log.v("FragmentManager", "movefrom STOPPED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x04cb, code lost:
        r18.performReallyStop();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x04d1, code lost:
        if (r19 >= 2) goto L_0x042f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x04d5, code lost:
        if (f2775a == false) goto L_0x04f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x04d7, code lost:
        android.util.Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x04f5, code lost:
        if (r18.mView == null) goto L_0x050c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0501, code lost:
        if (r17.f2790o.onShouldSaveFragmentState(r18) == false) goto L_0x050c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0507, code lost:
        if (r18.mSavedViewState != null) goto L_0x050c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0509, code lost:
        mo2304d(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x050c, code lost:
        r18.performDestroyView();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0513, code lost:
        if (r18.mView == null) goto L_0x056b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x0519, code lost:
        if (r18.mContainer == null) goto L_0x056b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x051b, code lost:
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x0520, code lost:
        if (r17.f2789n <= 0) goto L_0x0535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0526, code lost:
        if (r17.f2795u != false) goto L_0x0535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0528, code lost:
        r10 = mo2285a(r18, r20, false, r21);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x0535, code lost:
        if (r10 == null) goto L_0x0560;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x0537, code lost:
        r13 = r18;
        r18.mAnimatingAway = r18.mView;
        r18.mStateAfterAnimating = r19;
        r10.setAnimationListener(new android.support.p001v4.app.FragmentManagerImpl.C04415(r17, r18.mView, r10));
        r18.mView.startAnimation(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x0560, code lost:
        r18.mContainer.removeView(r18.mView);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x056b, code lost:
        r18.mContainer = null;
        r18.mView = null;
        r18.mInnerView = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x057e, code lost:
        if (f2775a == false) goto L_0x059a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0580, code lost:
        android.util.Log.v("FragmentManager", "movefrom CREATED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x059e, code lost:
        if (r18.mRetaining != false) goto L_0x05b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x05a0, code lost:
        r18.performDestroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x05a3, code lost:
        r18.performDetach();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x05a6, code lost:
        if (r22 != false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x05ac, code lost:
        if (r18.mRetaining != false) goto L_0x05b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x05ae, code lost:
        mo2302c(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x05b3, code lost:
        r18.mState = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x05b9, code lost:
        r18.mHost = null;
        r18.mParentFragment = null;
        r18.mFragmentManager = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0223, code lost:
        if (r19 <= 1) goto L_0x0372;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0227, code lost:
        if (f2775a == false) goto L_0x0243;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0229, code lost:
        android.util.Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0247, code lost:
        if (r18.mFromLayout != false) goto L_0x0355;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0249, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x024e, code lost:
        if (r18.mContainerId == 0) goto L_0x02d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0255, code lost:
        if (r18.mContainerId != -1) goto L_0x027c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0257, code lost:
        m3584a((java.lang.RuntimeException) new java.lang.IllegalArgumentException("Cannot create fragment " + r18 + " for a container view with no id"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x027c, code lost:
        r11 = (android.view.ViewGroup) r17.f2791p.onFindViewById(r18.mContainerId);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x028a, code lost:
        if (r11 != null) goto L_0x02d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0290, code lost:
        if (r18.mRestored != false) goto L_0x02d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        r14 = r18.getResources().getResourceName(r18.mContainerId);
     */
    /* JADX WARNING: Removed duplicated region for block: B:202:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0078  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo2292a(android.support.p001v4.app.Fragment r18, int r19, int r20, int r21, boolean r22) {
        /*
            r17 = this;
            r0 = r18
            boolean r4 = r0.mAdded
            if (r4 == 0) goto L_0x000c
            r0 = r18
            boolean r4 = r0.mDetached
            if (r4 == 0) goto L_0x0013
        L_0x000c:
            r4 = 1
            r0 = r19
            if (r0 <= r4) goto L_0x0013
            r19 = 1
        L_0x0013:
            r0 = r18
            boolean r4 = r0.mRemoving
            if (r4 == 0) goto L_0x0027
            r0 = r18
            int r4 = r0.mState
            r0 = r19
            if (r0 <= r4) goto L_0x0027
            r0 = r18
            int r0 = r0.mState
            r19 = r0
        L_0x0027:
            r0 = r18
            boolean r4 = r0.mDeferStart
            if (r4 == 0) goto L_0x003b
            r0 = r18
            int r4 = r0.mState
            r5 = 4
            if (r4 >= r5) goto L_0x003b
            r4 = 3
            r0 = r19
            if (r0 <= r4) goto L_0x003b
            r19 = 3
        L_0x003b:
            r0 = r18
            int r4 = r0.mState
            r0 = r19
            if (r4 >= r0) goto L_0x041e
            r0 = r18
            boolean r4 = r0.mFromLayout
            if (r4 == 0) goto L_0x0050
            r0 = r18
            boolean r4 = r0.mInLayout
            if (r4 != 0) goto L_0x0050
        L_0x004f:
            return
        L_0x0050:
            r0 = r18
            android.view.View r4 = r0.mAnimatingAway
            if (r4 == 0) goto L_0x0069
            r4 = 0
            r0 = r18
            r0.mAnimatingAway = r4
            r0 = r18
            int r6 = r0.mStateAfterAnimating
            r7 = 0
            r8 = 0
            r9 = 1
            r4 = r17
            r5 = r18
            r4.mo2292a(r5, r6, r7, r8, r9)
        L_0x0069:
            r0 = r18
            int r4 = r0.mState
            switch(r4) {
                case 0: goto L_0x00b9;
                case 1: goto L_0x0220;
                case 2: goto L_0x0372;
                case 3: goto L_0x037c;
                case 4: goto L_0x03a2;
                default: goto L_0x0070;
            }
        L_0x0070:
            r0 = r18
            int r4 = r0.mState
            r0 = r19
            if (r4 == r0) goto L_0x004f
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "moveToState: Fragment state for "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = " not updated inline; "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "expected state "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r19
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = " found "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            int r6 = r0.mState
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            android.util.Log.w(r4, r5)
            r0 = r19
            r1 = r18
            r1.mState = r0
            goto L_0x004f
        L_0x00b9:
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x00d7
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "moveto CREATED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x00d7:
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            if (r4 == 0) goto L_0x0144
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            r0 = r17
            android.support.v4.app.FragmentHostCallback r5 = r0.f2790o
            android.content.Context r5 = r5.mo2253b()
            java.lang.ClassLoader r5 = r5.getClassLoader()
            r4.setClassLoader(r5)
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            java.lang.String r5 = "android:view_state"
            android.util.SparseArray r4 = r4.getSparseParcelableArray(r5)
            r0 = r18
            r0.mSavedViewState = r4
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            java.lang.String r5 = "android:target_state"
            r0 = r17
            android.support.v4.app.Fragment r4 = r0.getFragment(r4, r5)
            r0 = r18
            r0.mTarget = r4
            r0 = r18
            android.support.v4.app.Fragment r4 = r0.mTarget
            if (r4 == 0) goto L_0x0123
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            java.lang.String r5 = "android:target_req_state"
            r6 = 0
            int r4 = r4.getInt(r5, r6)
            r0 = r18
            r0.mTargetRequestCode = r4
        L_0x0123:
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            java.lang.String r5 = "android:user_visible_hint"
            r6 = 1
            boolean r4 = r4.getBoolean(r5, r6)
            r0 = r18
            r0.mUserVisibleHint = r4
            r0 = r18
            boolean r4 = r0.mUserVisibleHint
            if (r4 != 0) goto L_0x0144
            r4 = 1
            r0 = r18
            r0.mDeferStart = r4
            r4 = 3
            r0 = r19
            if (r0 <= r4) goto L_0x0144
            r19 = 3
        L_0x0144:
            r0 = r17
            android.support.v4.app.FragmentHostCallback r4 = r0.f2790o
            r0 = r18
            r0.mHost = r4
            r0 = r17
            android.support.v4.app.Fragment r4 = r0.f2792q
            r0 = r18
            r0.mParentFragment = r4
            r0 = r17
            android.support.v4.app.Fragment r4 = r0.f2792q
            if (r4 == 0) goto L_0x019d
            r0 = r17
            android.support.v4.app.Fragment r4 = r0.f2792q
            android.support.v4.app.FragmentManagerImpl r4 = r4.mChildFragmentManager
        L_0x0160:
            r0 = r18
            r0.mFragmentManager = r4
            r4 = 0
            r0 = r18
            r0.mCalled = r4
            r0 = r17
            android.support.v4.app.FragmentHostCallback r4 = r0.f2790o
            android.content.Context r4 = r4.mo2253b()
            r0 = r18
            r0.onAttach(r4)
            r0 = r18
            boolean r4 = r0.mCalled
            if (r4 != 0) goto L_0x01a6
            android.support.v4.app.SuperNotCalledException r4 = new android.support.v4.app.SuperNotCalledException
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Fragment "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = " did not call through to super.onAttach()"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.<init>(r5)
            throw r4
        L_0x019d:
            r0 = r17
            android.support.v4.app.FragmentHostCallback r4 = r0.f2790o
            android.support.v4.app.FragmentManagerImpl r4 = r4.mo2255d()
            goto L_0x0160
        L_0x01a6:
            r0 = r18
            android.support.v4.app.Fragment r4 = r0.mParentFragment
            if (r4 != 0) goto L_0x03d4
            r0 = r17
            android.support.v4.app.FragmentHostCallback r4 = r0.f2790o
            r0 = r18
            r4.onAttachFragment(r0)
        L_0x01b5:
            r0 = r18
            boolean r4 = r0.mRetaining
            if (r4 != 0) goto L_0x03df
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            r0 = r18
            r0.performCreate(r4)
        L_0x01c4:
            r4 = 0
            r0 = r18
            r0.mRetaining = r4
            r0 = r18
            boolean r4 = r0.mFromLayout
            if (r4 == 0) goto L_0x0220
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            r0 = r18
            android.view.LayoutInflater r4 = r0.getLayoutInflater(r4)
            r5 = 0
            r0 = r18
            android.os.Bundle r6 = r0.mSavedFragmentState
            r0 = r18
            android.view.View r4 = r0.performCreateView(r4, r5, r6)
            r0 = r18
            r0.mView = r4
            r0 = r18
            android.view.View r4 = r0.mView
            if (r4 == 0) goto L_0x03fd
            r0 = r18
            android.view.View r4 = r0.mView
            r0 = r18
            r0.mInnerView = r4
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 11
            if (r4 < r5) goto L_0x03ef
            r0 = r18
            android.view.View r4 = r0.mView
            r5 = 0
            p000.ViewCompat.m12906b(r4, r5)
        L_0x0204:
            r0 = r18
            boolean r4 = r0.mHidden
            if (r4 == 0) goto L_0x0213
            r0 = r18
            android.view.View r4 = r0.mView
            r5 = 8
            r4.setVisibility(r5)
        L_0x0213:
            r0 = r18
            android.view.View r4 = r0.mView
            r0 = r18
            android.os.Bundle r5 = r0.mSavedFragmentState
            r0 = r18
            r0.onViewCreated(r4, r5)
        L_0x0220:
            r4 = 1
            r0 = r19
            if (r0 <= r4) goto L_0x0372
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x0243
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "moveto ACTIVITY_CREATED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x0243:
            r0 = r18
            boolean r4 = r0.mFromLayout
            if (r4 != 0) goto L_0x0355
            r11 = 0
            r0 = r18
            int r4 = r0.mContainerId
            if (r4 == 0) goto L_0x02d9
            r0 = r18
            int r4 = r0.mContainerId
            r5 = -1
            if (r4 != r5) goto L_0x027c
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Cannot create fragment "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = " for a container view with no id"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.<init>(r5)
            r0 = r17
            r0.m3584a(r4)
        L_0x027c:
            r0 = r17
            android.support.v4.app.FragmentContainer r4 = r0.f2791p
            r0 = r18
            int r5 = r0.mContainerId
            android.view.View r11 = r4.onFindViewById(r5)
            android.view.ViewGroup r11 = (android.view.ViewGroup) r11
            if (r11 != 0) goto L_0x02d9
            r0 = r18
            boolean r4 = r0.mRestored
            if (r4 != 0) goto L_0x02d9
            android.content.res.Resources r4 = r18.getResources()     // Catch:{ NotFoundException -> 0x0404 }
            r0 = r18
            int r5 = r0.mContainerId     // Catch:{ NotFoundException -> 0x0404 }
            java.lang.String r14 = r4.getResourceName(r5)     // Catch:{ NotFoundException -> 0x0404 }
        L_0x029e:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "No view found for id 0x"
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            int r6 = r0.mContainerId
            java.lang.String r6 = java.lang.Integer.toHexString(r6)
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = " ("
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r14)
            java.lang.String r6 = ") for fragment "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            r4.<init>(r5)
            r0 = r17
            r0.m3584a(r4)
        L_0x02d9:
            r0 = r18
            r0.mContainer = r11
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            r0 = r18
            android.view.LayoutInflater r4 = r0.getLayoutInflater(r4)
            r0 = r18
            android.os.Bundle r5 = r0.mSavedFragmentState
            r0 = r18
            android.view.View r4 = r0.performCreateView(r4, r11, r5)
            r0 = r18
            r0.mView = r4
            r0 = r18
            android.view.View r4 = r0.mView
            if (r4 == 0) goto L_0x0417
            r0 = r18
            android.view.View r4 = r0.mView
            r0 = r18
            r0.mInnerView = r4
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 11
            if (r4 < r5) goto L_0x0409
            r0 = r18
            android.view.View r4 = r0.mView
            r5 = 0
            p000.ViewCompat.m12906b(r4, r5)
        L_0x0311:
            if (r11 == 0) goto L_0x0339
            r4 = 1
            r0 = r17
            r1 = r18
            r2 = r20
            r3 = r21
            android.view.animation.Animation r10 = r0.mo2285a(r1, r2, r4, r3)
            if (r10 == 0) goto L_0x0332
            r0 = r18
            android.view.View r4 = r0.mView
            r0 = r17
            r0.m3587b(r4, r10)
            r0 = r18
            android.view.View r4 = r0.mView
            r4.startAnimation(r10)
        L_0x0332:
            r0 = r18
            android.view.View r4 = r0.mView
            r11.addView(r4)
        L_0x0339:
            r0 = r18
            boolean r4 = r0.mHidden
            if (r4 == 0) goto L_0x0348
            r0 = r18
            android.view.View r4 = r0.mView
            r5 = 8
            r4.setVisibility(r5)
        L_0x0348:
            r0 = r18
            android.view.View r4 = r0.mView
            r0 = r18
            android.os.Bundle r5 = r0.mSavedFragmentState
            r0 = r18
            r0.onViewCreated(r4, r5)
        L_0x0355:
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            r0 = r18
            r0.performActivityCreated(r4)
            r0 = r18
            android.view.View r4 = r0.mView
            if (r4 == 0) goto L_0x036d
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            r0 = r18
            r0.restoreViewState(r4)
        L_0x036d:
            r4 = 0
            r0 = r18
            r0.mSavedFragmentState = r4
        L_0x0372:
            r4 = 2
            r0 = r19
            if (r0 <= r4) goto L_0x037c
            r4 = 3
            r0 = r18
            r0.mState = r4
        L_0x037c:
            r4 = 3
            r0 = r19
            if (r0 <= r4) goto L_0x03a2
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x039f
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "moveto STARTED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x039f:
            r18.performStart()
        L_0x03a2:
            r4 = 4
            r0 = r19
            if (r0 <= r4) goto L_0x0070
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x03c5
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "moveto RESUMED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x03c5:
            r18.performResume()
            r4 = 0
            r0 = r18
            r0.mSavedFragmentState = r4
            r4 = 0
            r0 = r18
            r0.mSavedViewState = r4
            goto L_0x0070
        L_0x03d4:
            r0 = r18
            android.support.v4.app.Fragment r4 = r0.mParentFragment
            r0 = r18
            r4.onAttachFragment(r0)
            goto L_0x01b5
        L_0x03df:
            r0 = r18
            android.os.Bundle r4 = r0.mSavedFragmentState
            r0 = r18
            r0.restoreChildFragmentState(r4)
            r4 = 1
            r0 = r18
            r0.mState = r4
            goto L_0x01c4
        L_0x03ef:
            r0 = r18
            android.view.View r4 = r0.mView
            android.view.ViewGroup r4 = android.support.p001v4.app.NoSaveStateFrameLayout.m3649a(r4)
            r0 = r18
            r0.mView = r4
            goto L_0x0204
        L_0x03fd:
            r4 = 0
            r0 = r18
            r0.mInnerView = r4
            goto L_0x0220
        L_0x0404:
            r12 = move-exception
            java.lang.String r14 = "unknown"
            goto L_0x029e
        L_0x0409:
            r0 = r18
            android.view.View r4 = r0.mView
            android.view.ViewGroup r4 = android.support.p001v4.app.NoSaveStateFrameLayout.m3649a(r4)
            r0 = r18
            r0.mView = r4
            goto L_0x0311
        L_0x0417:
            r4 = 0
            r0 = r18
            r0.mInnerView = r4
            goto L_0x0355
        L_0x041e:
            r0 = r18
            int r4 = r0.mState
            r0 = r19
            if (r4 <= r0) goto L_0x0070
            r0 = r18
            int r4 = r0.mState
            switch(r4) {
                case 1: goto L_0x042f;
                case 2: goto L_0x04ce;
                case 3: goto L_0x04a8;
                case 4: goto L_0x0482;
                case 5: goto L_0x045c;
                default: goto L_0x042d;
            }
        L_0x042d:
            goto L_0x0070
        L_0x042f:
            r4 = 1
            r0 = r19
            if (r0 >= r4) goto L_0x0070
            r0 = r17
            boolean r4 = r0.f2795u
            if (r4 == 0) goto L_0x044c
            r0 = r18
            android.view.View r4 = r0.mAnimatingAway
            if (r4 == 0) goto L_0x044c
            r0 = r18
            android.view.View r15 = r0.mAnimatingAway
            r4 = 0
            r0 = r18
            r0.mAnimatingAway = r4
            r15.clearAnimation()
        L_0x044c:
            r0 = r18
            android.view.View r4 = r0.mAnimatingAway
            if (r4 == 0) goto L_0x057c
            r0 = r19
            r1 = r18
            r1.mStateAfterAnimating = r0
            r19 = 1
            goto L_0x0070
        L_0x045c:
            r4 = 5
            r0 = r19
            if (r0 >= r4) goto L_0x0482
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x047f
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "movefrom RESUMED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x047f:
            r18.performPause()
        L_0x0482:
            r4 = 4
            r0 = r19
            if (r0 >= r4) goto L_0x04a8
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x04a5
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "movefrom STARTED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x04a5:
            r18.performStop()
        L_0x04a8:
            r4 = 3
            r0 = r19
            if (r0 >= r4) goto L_0x04ce
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x04cb
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "movefrom STOPPED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x04cb:
            r18.performReallyStop()
        L_0x04ce:
            r4 = 2
            r0 = r19
            if (r0 >= r4) goto L_0x042f
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x04f1
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "movefrom ACTIVITY_CREATED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x04f1:
            r0 = r18
            android.view.View r4 = r0.mView
            if (r4 == 0) goto L_0x050c
            r0 = r17
            android.support.v4.app.FragmentHostCallback r4 = r0.f2790o
            r0 = r18
            boolean r4 = r4.onShouldSaveFragmentState(r0)
            if (r4 == 0) goto L_0x050c
            r0 = r18
            android.util.SparseArray<android.os.Parcelable> r4 = r0.mSavedViewState
            if (r4 != 0) goto L_0x050c
            r17.mo2304d(r18)
        L_0x050c:
            r18.performDestroyView()
            r0 = r18
            android.view.View r4 = r0.mView
            if (r4 == 0) goto L_0x056b
            r0 = r18
            android.view.ViewGroup r4 = r0.mContainer
            if (r4 == 0) goto L_0x056b
            r10 = 0
            r0 = r17
            int r4 = r0.f2789n
            if (r4 <= 0) goto L_0x0535
            r0 = r17
            boolean r4 = r0.f2795u
            if (r4 != 0) goto L_0x0535
            r4 = 0
            r0 = r17
            r1 = r18
            r2 = r20
            r3 = r21
            android.view.animation.Animation r10 = r0.mo2285a(r1, r2, r4, r3)
        L_0x0535:
            if (r10 == 0) goto L_0x0560
            r13 = r18
            r0 = r18
            android.view.View r4 = r0.mView
            r0 = r18
            r0.mAnimatingAway = r4
            r0 = r19
            r1 = r18
            r1.mStateAfterAnimating = r0
            r0 = r18
            android.view.View r0 = r0.mView
            r16 = r0
            android.support.v4.app.FragmentManagerImpl$5 r4 = new android.support.v4.app.FragmentManagerImpl$5
            r0 = r17
            r1 = r16
            r4.<init>(r1, r10, r13)
            r10.setAnimationListener(r4)
            r0 = r18
            android.view.View r4 = r0.mView
            r4.startAnimation(r10)
        L_0x0560:
            r0 = r18
            android.view.ViewGroup r4 = r0.mContainer
            r0 = r18
            android.view.View r5 = r0.mView
            r4.removeView(r5)
        L_0x056b:
            r4 = 0
            r0 = r18
            r0.mContainer = r4
            r4 = 0
            r0 = r18
            r0.mView = r4
            r4 = 0
            r0 = r18
            r0.mInnerView = r4
            goto L_0x042f
        L_0x057c:
            boolean r4 = f2775a
            if (r4 == 0) goto L_0x059a
            java.lang.String r4 = "FragmentManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "movefrom CREATED: "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r18
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
        L_0x059a:
            r0 = r18
            boolean r4 = r0.mRetaining
            if (r4 != 0) goto L_0x05b3
            r18.performDestroy()
        L_0x05a3:
            r18.performDetach()
            if (r22 != 0) goto L_0x0070
            r0 = r18
            boolean r4 = r0.mRetaining
            if (r4 != 0) goto L_0x05b9
            r17.mo2302c(r18)
            goto L_0x0070
        L_0x05b3:
            r4 = 0
            r0 = r18
            r0.mState = r4
            goto L_0x05a3
        L_0x05b9:
            r4 = 0
            r0 = r18
            r0.mHost = r4
            r4 = 0
            r0 = r18
            r0.mParentFragment = r4
            r4 = 0
            r0 = r18
            r0.mFragmentManager = r4
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.p001v4.app.FragmentManagerImpl.mo2292a(android.support.v4.app.Fragment, int, int, int, boolean):void");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo2293a(int i) {
        return this.f2789n >= i;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo2294a(Handler handler, String str, int i, int i2) {
        if (this.f2784i == null) {
            return false;
        }
        if (str == null && i < 0 && (i2 & 1) == 0) {
            int size = this.f2784i.size() - 1;
            if (size < 0) {
                return false;
            }
            BackStackRecord backStackRecord = (BackStackRecord) this.f2784i.remove(size);
            SparseArray sparseArray = new SparseArray();
            SparseArray sparseArray2 = new SparseArray();
            if (this.f2789n >= 1) {
                backStackRecord.calculateBackFragments(sparseArray, sparseArray2);
            }
            backStackRecord.popFromBackStack(true, null, sparseArray, sparseArray2);
            mo2301c();
        } else {
            int i3 = -1;
            if (str != null || i >= 0) {
                int size2 = this.f2784i.size() - 1;
                while (i3 >= 0) {
                    BackStackRecord backStackRecord2 = (BackStackRecord) this.f2784i.get(i3);
                    if ((str != null && str.equals(backStackRecord2.getName())) || (i >= 0 && i == backStackRecord2.f2704p)) {
                        break;
                    }
                    size2 = i3 - 1;
                }
                if (i3 < 0) {
                    return false;
                }
                if ((i2 & 1) != 0) {
                    i3--;
                    while (i3 >= 0) {
                        BackStackRecord backStackRecord3 = (BackStackRecord) this.f2784i.get(i3);
                        if ((str == null || !str.equals(backStackRecord3.getName())) && (i < 0 || i != backStackRecord3.f2704p)) {
                            break;
                        }
                        i3--;
                    }
                }
            }
            if (i3 == this.f2784i.size() - 1) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int size3 = this.f2784i.size() - 1; size3 > i3; size3--) {
                arrayList.add(this.f2784i.remove(size3));
            }
            int size4 = arrayList.size() - 1;
            SparseArray sparseArray3 = new SparseArray();
            SparseArray sparseArray4 = new SparseArray();
            if (this.f2789n >= 1) {
                for (int i4 = 0; i4 <= size4; i4++) {
                    ((BackStackRecord) arrayList.get(i4)).calculateBackFragments(sparseArray3, sparseArray4);
                }
            }
            TransitionState transitionState = null;
            int i5 = 0;
            while (i5 <= size4) {
                if (f2775a) {
                    Log.v("FragmentManager", "Popping back stack state: " + arrayList.get(i5));
                }
                transitionState = ((BackStackRecord) arrayList.get(i5)).popFromBackStack(i5 == size4, transitionState, sparseArray3, sparseArray4);
                i5++;
            }
            mo2301c();
        }
        return true;
    }

    public void addFragment(Fragment fragment, boolean z) {
        if (this.f2782g == null) {
            this.f2782g = new ArrayList<>();
        }
        if (f2775a) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        mo2300b(fragment);
        if (fragment.mDetached) {
            return;
        }
        if (this.f2782g.contains(fragment)) {
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
        this.f2782g.add(fragment);
        fragment.mAdded = true;
        fragment.mRemoving = false;
        if (fragment.mHasMenu && fragment.mMenuVisible) {
            this.f2793s = true;
        }
        if (z) {
            mo2291a(fragment);
        }
    }

    public void addOnBackStackChangedListener(OnBackStackChangedListener onBackStackChangedListener) {
        if (this.f2788m == null) {
            this.f2788m = new ArrayList<>();
        }
        this.f2788m.add(onBackStackChangedListener);
    }

    public int allocBackStackIndex(BackStackRecord backStackRecord) {
        synchronized (this) {
            if (this.f2787l == null || this.f2787l.size() <= 0) {
                if (this.f2786k == null) {
                    this.f2786k = new ArrayList<>();
                }
                int size = this.f2786k.size();
                if (f2775a) {
                    Log.v("FragmentManager", "Setting back stack index " + size + " to " + backStackRecord);
                }
                this.f2786k.add(backStackRecord);
                return size;
            }
            int intValue = ((Integer) this.f2787l.remove(this.f2787l.size() - 1)).intValue();
            if (f2775a) {
                Log.v("FragmentManager", "Adding back stack index " + intValue + " with " + backStackRecord);
            }
            this.f2786k.set(intValue, backStackRecord);
            return intValue;
        }
    }

    public void attachController(FragmentHostCallback fragmentHostCallback, FragmentContainer fragmentContainer, Fragment fragment) {
        if (this.f2790o != null) {
            throw new IllegalStateException("Already attached");
        }
        this.f2790o = fragmentHostCallback;
        this.f2791p = fragmentContainer;
        this.f2792q = fragment;
    }

    public void attachFragment(Fragment fragment, int i, int i2) {
        if (f2775a) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (!fragment.mAdded) {
                if (this.f2782g == null) {
                    this.f2782g = new ArrayList<>();
                }
                if (this.f2782g.contains(fragment)) {
                    throw new IllegalStateException("Fragment already added: " + fragment);
                }
                if (f2775a) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                this.f2782g.add(fragment);
                fragment.mAdded = true;
                if (fragment.mHasMenu && fragment.mMenuVisible) {
                    this.f2793s = true;
                }
                mo2292a(fragment, this.f2789n, i, i2, false);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo2299b() {
        if (this.f2797w) {
            boolean z = false;
            for (int i = 0; i < this.f2781f.size(); i++) {
                Fragment fragment = (Fragment) this.f2781f.get(i);
                if (!(fragment == null || fragment.mLoaderManager == null)) {
                    z |= fragment.mLoaderManager.hasRunningLoaders();
                }
            }
            if (!z) {
                this.f2797w = false;
                mo2286a();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo2300b(Fragment fragment) {
        if (fragment.mIndex < 0) {
            if (this.f2783h == null || this.f2783h.size() <= 0) {
                if (this.f2781f == null) {
                    this.f2781f = new ArrayList<>();
                }
                fragment.setIndex(this.f2781f.size(), this.f2792q);
                this.f2781f.add(fragment);
            } else {
                fragment.setIndex(((Integer) this.f2783h.remove(this.f2783h.size() - 1)).intValue(), this.f2792q);
                this.f2781f.set(fragment.mIndex, fragment);
            }
            if (f2775a) {
                Log.v("FragmentManager", "Allocated fragment index " + fragment);
            }
        }
    }

    public FragmentTransaction beginTransaction() {
        return new BackStackRecord(this);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo2301c() {
        if (this.f2788m != null) {
            for (int i = 0; i < this.f2788m.size(); i++) {
                ((OnBackStackChangedListener) this.f2788m.get(i)).onBackStackChanged();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo2302c(Fragment fragment) {
        if (fragment.mIndex >= 0) {
            if (f2775a) {
                Log.v("FragmentManager", "Freeing fragment index " + fragment);
            }
            this.f2781f.set(fragment.mIndex, null);
            if (this.f2783h == null) {
                this.f2783h = new ArrayList<>();
            }
            this.f2783h.add(Integer.valueOf(fragment.mIndex));
            this.f2790o.mo2250a(fragment.mWho);
            fragment.initState();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public FragmentManagerNonConfig mo2303d() {
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        if (this.f2781f != null) {
            for (int i = 0; i < this.f2781f.size(); i++) {
                Fragment fragment = (Fragment) this.f2781f.get(i);
                if (fragment != null) {
                    if (fragment.mRetainInstance) {
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(fragment);
                        fragment.mRetaining = true;
                        fragment.mTargetIndex = fragment.mTarget != null ? fragment.mTarget.mIndex : -1;
                        if (f2775a) {
                            Log.v("FragmentManager", "retainNonConfig: keeping retained " + fragment);
                        }
                    }
                    boolean z = false;
                    if (fragment.mChildFragmentManager != null) {
                        FragmentManagerNonConfig d = fragment.mChildFragmentManager.mo2303d();
                        if (d != null) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                for (int i2 = 0; i2 < i; i2++) {
                                    arrayList2.add(null);
                                }
                            }
                            arrayList2.add(d);
                            z = true;
                        }
                    }
                    if (arrayList2 != null && !z) {
                        arrayList2.add(null);
                    }
                }
            }
        }
        if (arrayList == null && arrayList2 == null) {
            return null;
        }
        return new FragmentManagerNonConfig(arrayList, arrayList2);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public void mo2304d(Fragment fragment) {
        if (fragment.mInnerView != null) {
            if (this.f2799y == null) {
                this.f2799y = new SparseArray<>();
            } else {
                this.f2799y.clear();
            }
            fragment.mInnerView.saveHierarchyState(this.f2799y);
            if (this.f2799y.size() > 0) {
                fragment.mSavedViewState = this.f2799y;
                this.f2799y = null;
            }
        }
    }

    public void detachFragment(Fragment fragment, int i, int i2) {
        if (f2775a) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.mDetached) {
            fragment.mDetached = true;
            if (fragment.mAdded) {
                if (this.f2782g != null) {
                    if (f2775a) {
                        Log.v("FragmentManager", "remove from detach: " + fragment);
                    }
                    this.f2782g.remove(fragment);
                }
                if (fragment.mHasMenu && fragment.mMenuVisible) {
                    this.f2793s = true;
                }
                fragment.mAdded = false;
                mo2292a(fragment, 1, i, i2, false);
            }
        }
    }

    public void dispatchActivityCreated() {
        this.f2794t = false;
        mo2288a(2, false);
    }

    public void dispatchConfigurationChanged(Configuration configuration) {
        if (this.f2782g != null) {
            for (int i = 0; i < this.f2782g.size(); i++) {
                Fragment fragment = (Fragment) this.f2782g.get(i);
                if (fragment != null) {
                    fragment.performConfigurationChanged(configuration);
                }
            }
        }
    }

    public boolean dispatchContextItemSelected(MenuItem menuItem) {
        if (this.f2782g != null) {
            for (int i = 0; i < this.f2782g.size(); i++) {
                Fragment fragment = (Fragment) this.f2782g.get(i);
                if (fragment != null && fragment.performContextItemSelected(menuItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void dispatchCreate() {
        this.f2794t = false;
        mo2288a(1, false);
    }

    public boolean dispatchCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        boolean z = false;
        ArrayList<Fragment> arrayList = null;
        if (this.f2782g != null) {
            for (int i = 0; i < this.f2782g.size(); i++) {
                Fragment fragment = (Fragment) this.f2782g.get(i);
                if (fragment != null && fragment.performCreateOptionsMenu(menu, menuInflater)) {
                    z = true;
                    if (arrayList == null) {
                        arrayList = new ArrayList<>();
                    }
                    arrayList.add(fragment);
                }
            }
        }
        if (this.f2785j != null) {
            for (int i2 = 0; i2 < this.f2785j.size(); i2++) {
                Fragment fragment2 = (Fragment) this.f2785j.get(i2);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    fragment2.onDestroyOptionsMenu();
                }
            }
        }
        this.f2785j = arrayList;
        return z;
    }

    public void dispatchDestroy() {
        this.f2795u = true;
        execPendingActions();
        mo2288a(0, false);
        this.f2790o = null;
        this.f2791p = null;
        this.f2792q = null;
    }

    public void dispatchDestroyView() {
        mo2288a(1, false);
    }

    public void dispatchLowMemory() {
        if (this.f2782g != null) {
            for (int i = 0; i < this.f2782g.size(); i++) {
                Fragment fragment = (Fragment) this.f2782g.get(i);
                if (fragment != null) {
                    fragment.performLowMemory();
                }
            }
        }
    }

    public void dispatchMultiWindowModeChanged(boolean z) {
        if (this.f2782g != null) {
            for (int size = this.f2782g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.f2782g.get(size);
                if (fragment != null) {
                    fragment.performMultiWindowModeChanged(z);
                }
            }
        }
    }

    public boolean dispatchOptionsItemSelected(MenuItem menuItem) {
        if (this.f2782g != null) {
            for (int i = 0; i < this.f2782g.size(); i++) {
                Fragment fragment = (Fragment) this.f2782g.get(i);
                if (fragment != null && fragment.performOptionsItemSelected(menuItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void dispatchOptionsMenuClosed(Menu menu) {
        if (this.f2782g != null) {
            for (int i = 0; i < this.f2782g.size(); i++) {
                Fragment fragment = (Fragment) this.f2782g.get(i);
                if (fragment != null) {
                    fragment.performOptionsMenuClosed(menu);
                }
            }
        }
    }

    public void dispatchPause() {
        mo2288a(4, false);
    }

    public void dispatchPictureInPictureModeChanged(boolean z) {
        if (this.f2782g != null) {
            for (int size = this.f2782g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.f2782g.get(size);
                if (fragment != null) {
                    fragment.performPictureInPictureModeChanged(z);
                }
            }
        }
    }

    public boolean dispatchPrepareOptionsMenu(Menu menu) {
        boolean z = false;
        if (this.f2782g != null) {
            for (int i = 0; i < this.f2782g.size(); i++) {
                Fragment fragment = (Fragment) this.f2782g.get(i);
                if (fragment != null && fragment.performPrepareOptionsMenu(menu)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public void dispatchReallyStop() {
        mo2288a(2, false);
    }

    public void dispatchResume() {
        this.f2794t = false;
        mo2288a(5, false);
    }

    public void dispatchStart() {
        this.f2794t = false;
        mo2288a(4, false);
    }

    public void dispatchStop() {
        this.f2794t = true;
        mo2288a(3, false);
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str + "    ";
        if (this.f2781f != null) {
            int size = this.f2781f.size();
            if (size > 0) {
                printWriter.print(str);
                printWriter.print("Active Fragments in ");
                printWriter.print(Integer.toHexString(System.identityHashCode(this)));
                printWriter.println(":");
                for (int i = 0; i < size; i++) {
                    Fragment fragment = (Fragment) this.f2781f.get(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i);
                    printWriter.print(": ");
                    printWriter.println(fragment);
                    if (fragment != null) {
                        fragment.dump(str2, fileDescriptor, printWriter, strArr);
                    }
                }
            }
        }
        if (this.f2782g != null) {
            int size2 = this.f2782g.size();
            if (size2 > 0) {
                printWriter.print(str);
                printWriter.println("Added Fragments:");
                for (int i2 = 0; i2 < size2; i2++) {
                    Fragment fragment2 = (Fragment) this.f2782g.get(i2);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i2);
                    printWriter.print(": ");
                    printWriter.println(fragment2.toString());
                }
            }
        }
        if (this.f2785j != null) {
            int size3 = this.f2785j.size();
            if (size3 > 0) {
                printWriter.print(str);
                printWriter.println("Fragments Created Menus:");
                for (int i3 = 0; i3 < size3; i3++) {
                    Fragment fragment3 = (Fragment) this.f2785j.get(i3);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i3);
                    printWriter.print(": ");
                    printWriter.println(fragment3.toString());
                }
            }
        }
        if (this.f2784i != null) {
            int size4 = this.f2784i.size();
            if (size4 > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack:");
                for (int i4 = 0; i4 < size4; i4++) {
                    BackStackRecord backStackRecord = (BackStackRecord) this.f2784i.get(i4);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i4);
                    printWriter.print(": ");
                    printWriter.println(backStackRecord.toString());
                    backStackRecord.dump(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        synchronized (this) {
            if (this.f2786k != null) {
                int size5 = this.f2786k.size();
                if (size5 > 0) {
                    printWriter.print(str);
                    printWriter.println("Back Stack Indices:");
                    for (int i5 = 0; i5 < size5; i5++) {
                        BackStackRecord backStackRecord2 = (BackStackRecord) this.f2786k.get(i5);
                        printWriter.print(str);
                        printWriter.print("  #");
                        printWriter.print(i5);
                        printWriter.print(": ");
                        printWriter.println(backStackRecord2);
                    }
                }
            }
            if (this.f2787l != null && this.f2787l.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.f2787l.toArray()));
            }
        }
        if (this.f2778c != null) {
            int size6 = this.f2778c.size();
            if (size6 > 0) {
                printWriter.print(str);
                printWriter.println("Pending Actions:");
                for (int i6 = 0; i6 < size6; i6++) {
                    Runnable runnable = (Runnable) this.f2778c.get(i6);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i6);
                    printWriter.print(": ");
                    printWriter.println(runnable);
                }
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.f2790o);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.f2791p);
        if (this.f2792q != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.f2792q);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.f2789n);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.f2794t);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.f2795u);
        if (this.f2793s) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.f2793s);
        }
        if (this.f2796v != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.f2796v);
        }
        if (this.f2783h != null && this.f2783h.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.f2783h.toArray()));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public Bundle mo2324e(Fragment fragment) {
        Bundle bundle = null;
        if (this.f2798x == null) {
            this.f2798x = new Bundle();
        }
        fragment.performSaveInstanceState(this.f2798x);
        if (!this.f2798x.isEmpty()) {
            bundle = this.f2798x;
            this.f2798x = null;
        }
        if (fragment.mView != null) {
            mo2304d(fragment);
        }
        if (fragment.mSavedViewState != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", fragment.mSavedViewState);
        }
        if (!fragment.mUserVisibleHint) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", fragment.mUserVisibleHint);
        }
        return bundle;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public Parcelable mo2325e() {
        execPendingActions();
        if (f2776b) {
            this.f2794t = true;
        }
        if (this.f2781f == null || this.f2781f.size() <= 0) {
            return null;
        }
        int size = this.f2781f.size();
        FragmentState[] fragmentStateArr = new FragmentState[size];
        boolean z = false;
        for (int i = 0; i < size; i++) {
            Fragment fragment = (Fragment) this.f2781f.get(i);
            if (fragment != null) {
                if (fragment.mIndex < 0) {
                    m3584a((RuntimeException) new IllegalStateException("Failure saving state: active " + fragment + " has cleared index: " + fragment.mIndex));
                }
                z = true;
                FragmentState fragmentState = new FragmentState(fragment);
                fragmentStateArr[i] = fragmentState;
                if (fragment.mState <= 0 || fragmentState.f2833k != null) {
                    fragmentState.f2833k = fragment.mSavedFragmentState;
                } else {
                    fragmentState.f2833k = mo2324e(fragment);
                    if (fragment.mTarget != null) {
                        if (fragment.mTarget.mIndex < 0) {
                            m3584a((RuntimeException) new IllegalStateException("Failure saving state: " + fragment + " has target not in fragment manager: " + fragment.mTarget));
                        }
                        if (fragmentState.f2833k == null) {
                            fragmentState.f2833k = new Bundle();
                        }
                        putFragment(fragmentState.f2833k, "android:target_state", fragment.mTarget);
                        if (fragment.mTargetRequestCode != 0) {
                            fragmentState.f2833k.putInt("android:target_req_state", fragment.mTargetRequestCode);
                        }
                    }
                }
                if (f2775a) {
                    Log.v("FragmentManager", "Saved state of " + fragment + ": " + fragmentState.f2833k);
                }
            }
        }
        if (z) {
            int[] iArr = null;
            BackStackState[] backStackStateArr = null;
            if (this.f2782g != null) {
                int size2 = this.f2782g.size();
                if (size2 > 0) {
                    iArr = new int[size2];
                    for (int i2 = 0; i2 < size2; i2++) {
                        iArr[i2] = ((Fragment) this.f2782g.get(i2)).mIndex;
                        if (iArr[i2] < 0) {
                            m3584a((RuntimeException) new IllegalStateException("Failure saving state: active " + this.f2782g.get(i2) + " has cleared index: " + iArr[i2]));
                        }
                        if (f2775a) {
                            Log.v("FragmentManager", "saveAllState: adding fragment #" + i2 + ": " + this.f2782g.get(i2));
                        }
                    }
                }
            }
            if (this.f2784i != null) {
                int size3 = this.f2784i.size();
                if (size3 > 0) {
                    backStackStateArr = new BackStackState[size3];
                    for (int i3 = 0; i3 < size3; i3++) {
                        backStackStateArr[i3] = new BackStackState((BackStackRecord) this.f2784i.get(i3));
                        if (f2775a) {
                            Log.v("FragmentManager", "saveAllState: adding back stack #" + i3 + ": " + this.f2784i.get(i3));
                        }
                    }
                }
            }
            FragmentManagerState fragmentManagerState = new FragmentManagerState();
            fragmentManagerState.f2817a = fragmentStateArr;
            fragmentManagerState.f2818b = iArr;
            fragmentManagerState.f2819c = backStackStateArr;
            return fragmentManagerState;
        } else if (!f2775a) {
            return null;
        } else {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
    }

    public void enqueueAction(Runnable runnable, boolean z) {
        if (!z) {
            m3588g();
        }
        synchronized (this) {
            if (this.f2795u || this.f2790o == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.f2778c == null) {
                this.f2778c = new ArrayList<>();
            }
            this.f2778c.add(runnable);
            if (this.f2778c.size() == 1) {
                this.f2790o.mo2254c().removeCallbacks(this.f2800z);
                this.f2790o.mo2254c().post(this.f2800z);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0062, code lost:
        r5.f2780e = true;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0066, code lost:
        if (r1 >= r2) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0068, code lost:
        r5.f2779d[r1].run();
        r5.f2779d[r1] = null;
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean execPendingActions() {
        /*
            r5 = this;
            boolean r3 = r5.f2780e
            if (r3 == 0) goto L_0x000c
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r4 = "FragmentManager is already executing transactions"
            r3.<init>(r4)
            throw r3
        L_0x000c:
            android.os.Looper r3 = android.os.Looper.myLooper()
            android.support.v4.app.FragmentHostCallback r4 = r5.f2790o
            android.os.Handler r4 = r4.mo2254c()
            android.os.Looper r4 = r4.getLooper()
            if (r3 == r4) goto L_0x0024
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r4 = "Must be called from main thread of fragment host"
            r3.<init>(r4)
            throw r3
        L_0x0024:
            r0 = 0
        L_0x0025:
            monitor-enter(r5)
            java.util.ArrayList<java.lang.Runnable> r3 = r5.f2778c     // Catch:{ all -> 0x0077 }
            if (r3 == 0) goto L_0x0032
            java.util.ArrayList<java.lang.Runnable> r3 = r5.f2778c     // Catch:{ all -> 0x0077 }
            int r3 = r3.size()     // Catch:{ all -> 0x0077 }
            if (r3 != 0) goto L_0x0037
        L_0x0032:
            monitor-exit(r5)     // Catch:{ all -> 0x0077 }
            r5.mo2299b()
            return r0
        L_0x0037:
            java.util.ArrayList<java.lang.Runnable> r3 = r5.f2778c     // Catch:{ all -> 0x0077 }
            int r2 = r3.size()     // Catch:{ all -> 0x0077 }
            java.lang.Runnable[] r3 = r5.f2779d     // Catch:{ all -> 0x0077 }
            if (r3 == 0) goto L_0x0046
            java.lang.Runnable[] r3 = r5.f2779d     // Catch:{ all -> 0x0077 }
            int r3 = r3.length     // Catch:{ all -> 0x0077 }
            if (r3 >= r2) goto L_0x004a
        L_0x0046:
            java.lang.Runnable[] r3 = new java.lang.Runnable[r2]     // Catch:{ all -> 0x0077 }
            r5.f2779d = r3     // Catch:{ all -> 0x0077 }
        L_0x004a:
            java.util.ArrayList<java.lang.Runnable> r3 = r5.f2778c     // Catch:{ all -> 0x0077 }
            java.lang.Runnable[] r4 = r5.f2779d     // Catch:{ all -> 0x0077 }
            r3.toArray(r4)     // Catch:{ all -> 0x0077 }
            java.util.ArrayList<java.lang.Runnable> r3 = r5.f2778c     // Catch:{ all -> 0x0077 }
            r3.clear()     // Catch:{ all -> 0x0077 }
            android.support.v4.app.FragmentHostCallback r3 = r5.f2790o     // Catch:{ all -> 0x0077 }
            android.os.Handler r3 = r3.mo2254c()     // Catch:{ all -> 0x0077 }
            java.lang.Runnable r4 = r5.f2800z     // Catch:{ all -> 0x0077 }
            r3.removeCallbacks(r4)     // Catch:{ all -> 0x0077 }
            monitor-exit(r5)     // Catch:{ all -> 0x0077 }
            r3 = 1
            r5.f2780e = r3
            r1 = 0
        L_0x0066:
            if (r1 >= r2) goto L_0x007a
            java.lang.Runnable[] r3 = r5.f2779d
            r3 = r3[r1]
            r3.run()
            java.lang.Runnable[] r3 = r5.f2779d
            r4 = 0
            r3[r1] = r4
            int r1 = r1 + 1
            goto L_0x0066
        L_0x0077:
            r3 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0077 }
            throw r3
        L_0x007a:
            r3 = 0
            r5.f2780e = r3
            r0 = 1
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.p001v4.app.FragmentManagerImpl.execPendingActions():boolean");
    }

    public void execSingleAction(Runnable runnable, boolean z) {
        if (this.f2780e) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (Looper.myLooper() != this.f2790o.mo2254c().getLooper()) {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        } else {
            if (!z) {
                m3588g();
            }
            this.f2780e = true;
            runnable.run();
            this.f2780e = false;
            mo2299b();
        }
    }

    public boolean executePendingTransactions() {
        return execPendingActions();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public LayoutInflaterFactory mo2329f() {
        return this;
    }

    public Fragment findFragmentById(int i) {
        if (this.f2782g != null) {
            for (int size = this.f2782g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.f2782g.get(size);
                if (fragment != null && fragment.mFragmentId == i) {
                    return fragment;
                }
            }
        }
        if (this.f2781f != null) {
            for (int size2 = this.f2781f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f2781f.get(size2);
                if (fragment2 != null && fragment2.mFragmentId == i) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment findFragmentByTag(String str) {
        if (!(this.f2782g == null || str == null)) {
            for (int size = this.f2782g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.f2782g.get(size);
                if (fragment != null && str.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (!(this.f2781f == null || str == null)) {
            for (int size2 = this.f2781f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f2781f.get(size2);
                if (fragment2 != null && str.equals(fragment2.mTag)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment findFragmentByWho(String str) {
        if (!(this.f2781f == null || str == null)) {
            for (int size = this.f2781f.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.f2781f.get(size);
                if (fragment != null) {
                    Fragment findFragmentByWho = fragment.findFragmentByWho(str);
                    if (findFragmentByWho != null) {
                        return findFragmentByWho;
                    }
                }
            }
        }
        return null;
    }

    public void freeBackStackIndex(int i) {
        synchronized (this) {
            this.f2786k.set(i, null);
            if (this.f2787l == null) {
                this.f2787l = new ArrayList<>();
            }
            if (f2775a) {
                Log.v("FragmentManager", "Freeing back stack index " + i);
            }
            this.f2787l.add(Integer.valueOf(i));
        }
    }

    public BackStackEntry getBackStackEntryAt(int i) {
        return (BackStackEntry) this.f2784i.get(i);
    }

    public int getBackStackEntryCount() {
        if (this.f2784i != null) {
            return this.f2784i.size();
        }
        return 0;
    }

    public Fragment getFragment(Bundle bundle, String str) {
        int i = bundle.getInt(str, -1);
        if (i == -1) {
            return null;
        }
        if (i >= this.f2781f.size()) {
            m3584a((RuntimeException) new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i));
        }
        Fragment fragment = (Fragment) this.f2781f.get(i);
        if (fragment != null) {
            return fragment;
        }
        m3584a((RuntimeException) new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i));
        return fragment;
    }

    public List<Fragment> getFragments() {
        return this.f2781f;
    }

    public void hideFragment(Fragment fragment, int i, int i2) {
        if (f2775a) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.mHidden) {
            fragment.mHidden = true;
            if (fragment.mView != null) {
                Animation a = mo2285a(fragment, i, false, i2);
                if (a != null) {
                    m3587b(fragment.mView, a);
                    fragment.mView.startAnimation(a);
                }
                fragment.mView.setVisibility(8);
            }
            if (fragment.mAdded && fragment.mHasMenu && fragment.mMenuVisible) {
                this.f2793s = true;
            }
            fragment.onHiddenChanged(true);
        }
    }

    public boolean isDestroyed() {
        return this.f2795u;
    }

    public void noteStateNotSaved() {
        this.f2794t = false;
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, FragmentTag.Fragment);
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(0);
        }
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!Fragment.isSupportFragmentClass(this.f2790o.mo2253b(), attributeValue)) {
            return null;
        }
        int i = view != null ? view.getId() : 0;
        if (i == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + attributeValue);
        }
        Fragment fragment = resourceId != -1 ? findFragmentById(resourceId) : null;
        if (fragment == null && string != null) {
            fragment = findFragmentByTag(string);
        }
        if (fragment == null && i != -1) {
            fragment = findFragmentById(i);
        }
        if (f2775a) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + attributeValue + " existing=" + fragment);
        }
        if (fragment == null) {
            fragment = Fragment.instantiate(context, attributeValue);
            fragment.mFromLayout = true;
            fragment.mFragmentId = resourceId != 0 ? resourceId : i;
            fragment.mContainerId = i;
            fragment.mTag = string;
            fragment.mInLayout = true;
            fragment.mFragmentManager = this;
            fragment.mHost = this.f2790o;
            fragment.onInflate(this.f2790o.mo2253b(), attributeSet, fragment.mSavedFragmentState);
            addFragment(fragment, true);
        } else if (fragment.mInLayout) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(i) + " with another fragment for " + attributeValue);
        } else {
            fragment.mInLayout = true;
            fragment.mHost = this.f2790o;
            if (!fragment.mRetaining) {
                fragment.onInflate(this.f2790o.mo2253b(), attributeSet, fragment.mSavedFragmentState);
            }
        }
        if (this.f2789n >= 1 || !fragment.mFromLayout) {
            mo2291a(fragment);
        } else {
            mo2292a(fragment, 1, 0, 0, false);
        }
        if (fragment.mView == null) {
            throw new IllegalStateException("Fragment " + attributeValue + " did not create a view.");
        }
        if (resourceId != 0) {
            fragment.mView.setId(resourceId);
        }
        if (fragment.mView.getTag() == null) {
            fragment.mView.setTag(string);
        }
        return fragment.mView;
    }

    public void performPendingDeferredStart(Fragment fragment) {
        if (!fragment.mDeferStart) {
            return;
        }
        if (this.f2780e) {
            this.f2797w = true;
            return;
        }
        fragment.mDeferStart = false;
        mo2292a(fragment, this.f2789n, 0, 0, false);
    }

    public void popBackStack() {
        enqueueAction(new Runnable() {
            public void run() {
                FragmentManagerImpl.this.mo2294a(FragmentManagerImpl.this.f2790o.mo2254c(), (String) null, -1, 0);
            }
        }, false);
    }

    public void popBackStack(final int i, final int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("Bad id: " + i);
        }
        enqueueAction(new Runnable() {
            public void run() {
                FragmentManagerImpl.this.mo2294a(FragmentManagerImpl.this.f2790o.mo2254c(), (String) null, i, i2);
            }
        }, false);
    }

    public void popBackStack(final String str, final int i) {
        enqueueAction(new Runnable() {
            public void run() {
                FragmentManagerImpl.this.mo2294a(FragmentManagerImpl.this.f2790o.mo2254c(), str, -1, i);
            }
        }, false);
    }

    public boolean popBackStackImmediate() {
        m3588g();
        executePendingTransactions();
        return mo2294a(this.f2790o.mo2254c(), (String) null, -1, 0);
    }

    public boolean popBackStackImmediate(int i, int i2) {
        m3588g();
        executePendingTransactions();
        if (i >= 0) {
            return mo2294a(this.f2790o.mo2254c(), (String) null, i, i2);
        }
        throw new IllegalArgumentException("Bad id: " + i);
    }

    public boolean popBackStackImmediate(String str, int i) {
        m3588g();
        executePendingTransactions();
        return mo2294a(this.f2790o.mo2254c(), str, -1, i);
    }

    public void putFragment(Bundle bundle, String str, Fragment fragment) {
        if (fragment.mIndex < 0) {
            m3584a((RuntimeException) new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, fragment.mIndex);
    }

    public void removeFragment(Fragment fragment, int i, int i2) {
        if (f2775a) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.mBackStackNesting);
        }
        boolean z = !fragment.isInBackStack();
        if (!fragment.mDetached || z) {
            if (this.f2782g != null) {
                this.f2782g.remove(fragment);
            }
            if (fragment.mHasMenu && fragment.mMenuVisible) {
                this.f2793s = true;
            }
            fragment.mAdded = false;
            fragment.mRemoving = true;
            mo2292a(fragment, z ? 0 : 1, i, i2, false);
        }
    }

    public void removeOnBackStackChangedListener(OnBackStackChangedListener onBackStackChangedListener) {
        if (this.f2788m != null) {
            this.f2788m.remove(onBackStackChangedListener);
        }
    }

    public SavedState saveFragmentInstanceState(Fragment fragment) {
        if (fragment.mIndex < 0) {
            m3584a((RuntimeException) new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        if (fragment.mState <= 0) {
            return null;
        }
        Bundle e = mo2324e(fragment);
        if (e != null) {
            return new SavedState(e);
        }
        return null;
    }

    public void setBackStackIndex(int i, BackStackRecord backStackRecord) {
        synchronized (this) {
            if (this.f2786k == null) {
                this.f2786k = new ArrayList<>();
            }
            int size = this.f2786k.size();
            if (i < size) {
                if (f2775a) {
                    Log.v("FragmentManager", "Setting back stack index " + i + " to " + backStackRecord);
                }
                this.f2786k.set(i, backStackRecord);
            } else {
                while (size < i) {
                    this.f2786k.add(null);
                    if (this.f2787l == null) {
                        this.f2787l = new ArrayList<>();
                    }
                    if (f2775a) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.f2787l.add(Integer.valueOf(size));
                    size++;
                }
                if (f2775a) {
                    Log.v("FragmentManager", "Adding back stack index " + i + " with " + backStackRecord);
                }
                this.f2786k.add(backStackRecord);
            }
        }
    }

    public void showFragment(Fragment fragment, int i, int i2) {
        if (f2775a) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.mHidden) {
            fragment.mHidden = false;
            if (fragment.mView != null) {
                Animation a = mo2285a(fragment, i, true, i2);
                if (a != null) {
                    m3587b(fragment.mView, a);
                    fragment.mView.startAnimation(a);
                }
                fragment.mView.setVisibility(0);
            }
            if (fragment.mAdded && fragment.mHasMenu && fragment.mMenuVisible) {
                this.f2793s = true;
            }
            fragment.onHiddenChanged(false);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(NotificationCompat.FLAG_HIGH_PRIORITY);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.f2792q != null) {
            DebugUtils.m12560a(this.f2792q, sb);
        } else {
            DebugUtils.m12560a(this.f2790o, sb);
        }
        sb.append("}}");
        return sb.toString();
    }
}
