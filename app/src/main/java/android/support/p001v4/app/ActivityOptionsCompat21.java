package android.support.p001v4.app;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;

/* renamed from: android.support.v4.app.ActivityOptionsCompat21 */
class ActivityOptionsCompat21 {

    /* renamed from: a */
    private final ActivityOptions f2684a;

    private ActivityOptionsCompat21(ActivityOptions activityOptions) {
        this.f2684a = activityOptions;
    }

    public static ActivityOptionsCompat21 makeCustomAnimation(Context context, int i, int i2) {
        return new ActivityOptionsCompat21(ActivityOptions.makeCustomAnimation(context, i, i2));
    }

    public static ActivityOptionsCompat21 makeScaleUpAnimation(View view, int i, int i2, int i3, int i4) {
        return new ActivityOptionsCompat21(ActivityOptions.makeScaleUpAnimation(view, i, i2, i3, i4));
    }

    public static ActivityOptionsCompat21 makeSceneTransitionAnimation(Activity activity, View view, String str) {
        return new ActivityOptionsCompat21(ActivityOptions.makeSceneTransitionAnimation(activity, view, str));
    }

    public static ActivityOptionsCompat21 makeSceneTransitionAnimation(Activity activity, View[] viewArr, String[] strArr) {
        Pair[] pairArr = null;
        if (viewArr != null) {
            pairArr = new Pair[viewArr.length];
            for (int i = 0; i < pairArr.length; i++) {
                pairArr[i] = Pair.create(viewArr[i], strArr[i]);
            }
        }
        return new ActivityOptionsCompat21(ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    public static ActivityOptionsCompat21 makeTaskLaunchBehind() {
        return new ActivityOptionsCompat21(ActivityOptions.makeTaskLaunchBehind());
    }

    public static ActivityOptionsCompat21 makeThumbnailScaleUpAnimation(View view, Bitmap bitmap, int i, int i2) {
        return new ActivityOptionsCompat21(ActivityOptions.makeThumbnailScaleUpAnimation(view, bitmap, i, i2));
    }

    public Bundle toBundle() {
        return this.f2684a.toBundle();
    }

    public void update(ActivityOptionsCompat21 activityOptionsCompat21) {
        this.f2684a.update(activityOptionsCompat21.f2684a);
    }
}
