package android.support.p001v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.graphics.drawable.InsetDrawable;
import android.os.Build.VERSION;
import android.support.p001v4.widget.DrawerLayout;
import android.support.p001v4.widget.DrawerLayout.DrawerListener;
import android.view.MenuItem;
import android.view.View;

@Deprecated
/* renamed from: android.support.v4.app.ActionBarDrawerToggle */
public class ActionBarDrawerToggle implements DrawerListener {

    /* renamed from: b */
    private static final ActionBarDrawerToggleImpl f2649b;

    /* renamed from: a */
    final Activity f2650a;

    /* renamed from: c */
    private final Delegate f2651c;

    /* renamed from: d */
    private final DrawerLayout f2652d;

    /* renamed from: e */
    private boolean f2653e;

    /* renamed from: f */
    private boolean f2654f;

    /* renamed from: g */
    private Drawable f2655g;

    /* renamed from: h */
    private Drawable f2656h;

    /* renamed from: i */
    private SlideDrawable f2657i;

    /* renamed from: j */
    private final int f2658j;

    /* renamed from: k */
    private final int f2659k;

    /* renamed from: l */
    private final int f2660l;

    /* renamed from: m */
    private Object f2661m;

    /* renamed from: android.support.v4.app.ActionBarDrawerToggle$ActionBarDrawerToggleImpl */
    interface ActionBarDrawerToggleImpl {
        Drawable getThemeUpIndicator(Activity activity);

        Object setActionBarDescription(Object obj, Activity activity, int i);

        Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i);
    }

    /* renamed from: android.support.v4.app.ActionBarDrawerToggle$ActionBarDrawerToggleImplBase */
    static class ActionBarDrawerToggleImplBase implements ActionBarDrawerToggleImpl {
        ActionBarDrawerToggleImplBase() {
        }

        public Drawable getThemeUpIndicator(Activity activity) {
            return null;
        }

        public Object setActionBarDescription(Object obj, Activity activity, int i) {
            return obj;
        }

        public Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i) {
            return obj;
        }
    }

    /* renamed from: android.support.v4.app.ActionBarDrawerToggle$ActionBarDrawerToggleImplHC */
    static class ActionBarDrawerToggleImplHC implements ActionBarDrawerToggleImpl {
        ActionBarDrawerToggleImplHC() {
        }

        public Drawable getThemeUpIndicator(Activity activity) {
            return ActionBarDrawerToggleHoneycomb.getThemeUpIndicator(activity);
        }

        public Object setActionBarDescription(Object obj, Activity activity, int i) {
            return ActionBarDrawerToggleHoneycomb.setActionBarDescription(obj, activity, i);
        }

        public Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i) {
            return ActionBarDrawerToggleHoneycomb.setActionBarUpIndicator(obj, activity, drawable, i);
        }
    }

    /* renamed from: android.support.v4.app.ActionBarDrawerToggle$ActionBarDrawerToggleImplJellybeanMR2 */
    static class ActionBarDrawerToggleImplJellybeanMR2 implements ActionBarDrawerToggleImpl {
        ActionBarDrawerToggleImplJellybeanMR2() {
        }

        public Drawable getThemeUpIndicator(Activity activity) {
            return ActionBarDrawerToggleJellybeanMR2.getThemeUpIndicator(activity);
        }

        public Object setActionBarDescription(Object obj, Activity activity, int i) {
            return ActionBarDrawerToggleJellybeanMR2.setActionBarDescription(obj, activity, i);
        }

        public Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i) {
            return ActionBarDrawerToggleJellybeanMR2.setActionBarUpIndicator(obj, activity, drawable, i);
        }
    }

    /* renamed from: android.support.v4.app.ActionBarDrawerToggle$Delegate */
    public interface Delegate {
        Drawable getThemeUpIndicator();

        void setActionBarDescription(int i);

        void setActionBarUpIndicator(Drawable drawable, int i);
    }

    /* renamed from: android.support.v4.app.ActionBarDrawerToggle$DelegateProvider */
    public interface DelegateProvider {
        Delegate getDrawerToggleDelegate();
    }

    /* renamed from: android.support.v4.app.ActionBarDrawerToggle$SlideDrawable */
    class SlideDrawable extends InsetDrawable implements Callback {

        /* renamed from: a */
        final /* synthetic */ ActionBarDrawerToggle f2662a;

        /* renamed from: b */
        private final boolean f2663b;

        /* renamed from: c */
        private final Rect f2664c;

        /* renamed from: d */
        private float f2665d;

        /* renamed from: e */
        private float f2666e;

        SlideDrawable(ActionBarDrawerToggle actionBarDrawerToggle, Drawable drawable) {
            boolean z = false;
            this.f2662a = actionBarDrawerToggle;
            super(drawable, 0);
            if (VERSION.SDK_INT > 18) {
                z = true;
            }
            this.f2663b = z;
            this.f2664c = new Rect();
        }

        public void draw(Canvas canvas) {
            int i = 1;
            copyBounds(this.f2664c);
            canvas.save();
            boolean z = ViewCompat.m12923h(this.f2662a.f2650a.getWindow().getDecorView()) == 1;
            if (z) {
                i = -1;
            }
            int width = this.f2664c.width();
            canvas.translate((-this.f2666e) * ((float) width) * this.f2665d * ((float) i), 0.0f);
            if (z && !this.f2663b) {
                canvas.translate((float) width, 0.0f);
                canvas.scale(-1.0f, 1.0f);
            }
            super.draw(canvas);
            canvas.restore();
        }

        public float getPosition() {
            return this.f2665d;
        }

        public void setOffset(float f) {
            this.f2666e = f;
            invalidateSelf();
        }

        public void setPosition(float f) {
            this.f2665d = f;
            invalidateSelf();
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 18) {
            f2649b = new ActionBarDrawerToggleImplJellybeanMR2();
        } else if (i >= 11) {
            f2649b = new ActionBarDrawerToggleImplHC();
        } else {
            f2649b = new ActionBarDrawerToggleImplBase();
        }
    }

    public ActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, int i, int i2, int i3) {
        this(activity, drawerLayout, !m3531a((Context) activity), i, i2, i3);
    }

    public ActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, boolean z, int i, int i2, int i3) {
        this.f2653e = true;
        this.f2650a = activity;
        if (activity instanceof DelegateProvider) {
            this.f2651c = ((DelegateProvider) activity).getDrawerToggleDelegate();
        } else {
            this.f2651c = null;
        }
        this.f2652d = drawerLayout;
        this.f2658j = i;
        this.f2659k = i2;
        this.f2660l = i3;
        this.f2655g = mo1879a();
        this.f2656h = ContextCompat.getDrawable(activity, i);
        this.f2657i = new SlideDrawable(this, this.f2656h);
        this.f2657i.setOffset(z ? 0.33333334f : 0.0f);
    }

    /* renamed from: a */
    private static boolean m3531a(Context context) {
        return context.getApplicationInfo().targetSdkVersion >= 21 && VERSION.SDK_INT >= 21;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public Drawable mo1879a() {
        return this.f2651c != null ? this.f2651c.getThemeUpIndicator() : f2649b.getThemeUpIndicator(this.f2650a);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1880a(int i) {
        if (this.f2651c != null) {
            this.f2651c.setActionBarDescription(i);
        } else {
            this.f2661m = f2649b.setActionBarDescription(this.f2661m, this.f2650a, i);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1881a(Drawable drawable, int i) {
        if (this.f2651c != null) {
            this.f2651c.setActionBarUpIndicator(drawable, i);
        } else {
            this.f2661m = f2649b.setActionBarUpIndicator(this.f2661m, this.f2650a, drawable, i);
        }
    }

    public boolean isDrawerIndicatorEnabled() {
        return this.f2653e;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (!this.f2654f) {
            this.f2655g = mo1879a();
        }
        this.f2656h = ContextCompat.getDrawable(this.f2650a, this.f2658j);
        syncState();
    }

    public void onDrawerClosed(View view) {
        this.f2657i.setPosition(0.0f);
        if (this.f2653e) {
            mo1880a(this.f2659k);
        }
    }

    public void onDrawerOpened(View view) {
        this.f2657i.setPosition(1.0f);
        if (this.f2653e) {
            mo1880a(this.f2660l);
        }
    }

    public void onDrawerSlide(View view, float f) {
        float position = this.f2657i.getPosition();
        this.f2657i.setPosition(f > 0.5f ? Math.max(position, Math.max(0.0f, f - 0.5f) * 2.0f) : Math.min(position, f * 2.0f));
    }

    public void onDrawerStateChanged(int i) {
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem == null || menuItem.getItemId() != 16908332 || !this.f2653e) {
            return false;
        }
        if (this.f2652d.isDrawerVisible(8388611)) {
            this.f2652d.closeDrawer(8388611);
        } else {
            this.f2652d.openDrawer(8388611);
        }
        return true;
    }

    public void setDrawerIndicatorEnabled(boolean z) {
        if (z != this.f2653e) {
            if (z) {
                mo1881a(this.f2657i, this.f2652d.isDrawerOpen(8388611) ? this.f2660l : this.f2659k);
            } else {
                mo1881a(this.f2655g, 0);
            }
            this.f2653e = z;
        }
    }

    public void setHomeAsUpIndicator(int i) {
        Drawable drawable = null;
        if (i != 0) {
            drawable = ContextCompat.getDrawable(this.f2650a, i);
        }
        setHomeAsUpIndicator(drawable);
    }

    public void setHomeAsUpIndicator(Drawable drawable) {
        if (drawable == null) {
            this.f2655g = mo1879a();
            this.f2654f = false;
        } else {
            this.f2655g = drawable;
            this.f2654f = true;
        }
        if (!this.f2653e) {
            mo1881a(this.f2655g, 0);
        }
    }

    public void syncState() {
        if (this.f2652d.isDrawerOpen(8388611)) {
            this.f2657i.setPosition(1.0f);
        } else {
            this.f2657i.setPosition(0.0f);
        }
        if (this.f2653e) {
            mo1881a(this.f2657i, this.f2652d.isDrawerOpen(8388611) ? this.f2660l : this.f2659k);
        }
    }
}
