package android.support.p001v4.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings.Secure;
import android.support.p001v4.app.INotificationSideChannel.Stub;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/* renamed from: android.support.v4.app.NotificationManagerCompat */
public final class NotificationManagerCompat {
    public static final String ACTION_BIND_SIDE_CHANNEL = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL";
    public static final String EXTRA_USE_SIDE_CHANNEL = "android.support.useSideChannel";
    public static final int IMPORTANCE_DEFAULT = 3;
    public static final int IMPORTANCE_HIGH = 4;
    public static final int IMPORTANCE_LOW = 2;
    public static final int IMPORTANCE_MAX = 5;
    public static final int IMPORTANCE_MIN = 1;
    public static final int IMPORTANCE_NONE = 0;
    public static final int IMPORTANCE_UNSPECIFIED = -1000;

    /* renamed from: a */
    static final int f3002a = f3008i.getSideChannelBindFlags();

    /* renamed from: b */
    private static final Object f3003b = new Object();

    /* renamed from: c */
    private static String f3004c;

    /* renamed from: d */
    private static Set<String> f3005d = new HashSet();

    /* renamed from: g */
    private static final Object f3006g = new Object();

    /* renamed from: h */
    private static SideChannelManager f3007h;

    /* renamed from: i */
    private static final Impl f3008i;

    /* renamed from: e */
    private final Context f3009e;

    /* renamed from: f */
    private final NotificationManager f3010f = ((NotificationManager) this.f3009e.getSystemService("notification"));

    /* renamed from: android.support.v4.app.NotificationManagerCompat$CancelTask */
    static class CancelTask implements Task {

        /* renamed from: a */
        final String f3011a;

        /* renamed from: b */
        final int f3012b;

        /* renamed from: c */
        final String f3013c;

        /* renamed from: d */
        final boolean f3014d;

        public CancelTask(String str) {
            this.f3011a = str;
            this.f3012b = 0;
            this.f3013c = null;
            this.f3014d = true;
        }

        public CancelTask(String str, int i, String str2) {
            this.f3011a = str;
            this.f3012b = i;
            this.f3013c = str2;
            this.f3014d = false;
        }

        public void send(INotificationSideChannel iNotificationSideChannel) throws RemoteException {
            if (this.f3014d) {
                iNotificationSideChannel.cancelAll(this.f3011a);
            } else {
                iNotificationSideChannel.cancel(this.f3011a, this.f3012b, this.f3013c);
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("CancelTask[");
            sb.append("packageName:").append(this.f3011a);
            sb.append(", id:").append(this.f3012b);
            sb.append(", tag:").append(this.f3013c);
            sb.append(", all:").append(this.f3014d);
            sb.append("]");
            return sb.toString();
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$Impl */
    interface Impl {
        boolean areNotificationsEnabled(Context context, NotificationManager notificationManager);

        void cancelNotification(NotificationManager notificationManager, String str, int i);

        int getImportance(NotificationManager notificationManager);

        int getSideChannelBindFlags();

        void postNotification(NotificationManager notificationManager, String str, int i, Notification notification);
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$ImplApi24 */
    static class ImplApi24 extends ImplKitKat {
        ImplApi24() {
        }

        public boolean areNotificationsEnabled(Context context, NotificationManager notificationManager) {
            return NotificationManagerCompatApi24.areNotificationsEnabled(notificationManager);
        }

        public int getImportance(NotificationManager notificationManager) {
            return NotificationManagerCompatApi24.getImportance(notificationManager);
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$ImplBase */
    static class ImplBase implements Impl {
        ImplBase() {
        }

        public boolean areNotificationsEnabled(Context context, NotificationManager notificationManager) {
            return true;
        }

        public void cancelNotification(NotificationManager notificationManager, String str, int i) {
            notificationManager.cancel(str, i);
        }

        public int getImportance(NotificationManager notificationManager) {
            return NotificationManagerCompat.IMPORTANCE_UNSPECIFIED;
        }

        public int getSideChannelBindFlags() {
            return 1;
        }

        public void postNotification(NotificationManager notificationManager, String str, int i, Notification notification) {
            notificationManager.notify(str, i, notification);
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$ImplIceCreamSandwich */
    static class ImplIceCreamSandwich extends ImplBase {
        ImplIceCreamSandwich() {
        }

        public int getSideChannelBindFlags() {
            return 33;
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$ImplKitKat */
    static class ImplKitKat extends ImplIceCreamSandwich {
        ImplKitKat() {
        }

        public boolean areNotificationsEnabled(Context context, NotificationManager notificationManager) {
            return NotificationManagerCompatKitKat.areNotificationsEnabled(context);
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$NotifyTask */
    static class NotifyTask implements Task {

        /* renamed from: a */
        final String f3015a;

        /* renamed from: b */
        final int f3016b;

        /* renamed from: c */
        final String f3017c;

        /* renamed from: d */
        final Notification f3018d;

        public NotifyTask(String str, int i, String str2, Notification notification) {
            this.f3015a = str;
            this.f3016b = i;
            this.f3017c = str2;
            this.f3018d = notification;
        }

        public void send(INotificationSideChannel iNotificationSideChannel) throws RemoteException {
            iNotificationSideChannel.notify(this.f3015a, this.f3016b, this.f3017c, this.f3018d);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("NotifyTask[");
            sb.append("packageName:").append(this.f3015a);
            sb.append(", id:").append(this.f3016b);
            sb.append(", tag:").append(this.f3017c);
            sb.append("]");
            return sb.toString();
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$ServiceConnectedEvent */
    static class ServiceConnectedEvent {

        /* renamed from: a */
        final ComponentName f3019a;

        /* renamed from: b */
        final IBinder f3020b;

        public ServiceConnectedEvent(ComponentName componentName, IBinder iBinder) {
            this.f3019a = componentName;
            this.f3020b = iBinder;
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$SideChannelManager */
    static class SideChannelManager implements ServiceConnection, Callback {

        /* renamed from: a */
        private final Context f3021a;

        /* renamed from: b */
        private final HandlerThread f3022b;

        /* renamed from: c */
        private final Handler f3023c;

        /* renamed from: d */
        private final Map<ComponentName, ListenerRecord> f3024d = new HashMap();

        /* renamed from: e */
        private Set<String> f3025e = new HashSet();

        /* renamed from: android.support.v4.app.NotificationManagerCompat$SideChannelManager$ListenerRecord */
        static class ListenerRecord {
            public boolean bound = false;
            public final ComponentName componentName;
            public int retryCount = 0;
            public INotificationSideChannel service;
            public LinkedList<Task> taskQueue = new LinkedList<>();

            public ListenerRecord(ComponentName componentName2) {
                this.componentName = componentName2;
            }
        }

        public SideChannelManager(Context context) {
            this.f3021a = context;
            this.f3022b = new HandlerThread("NotificationManagerCompat");
            this.f3022b.start();
            this.f3023c = new Handler(this.f3022b.getLooper(), this);
        }

        /* renamed from: a */
        private void m3670a() {
            Set<String> enabledListenerPackages = NotificationManagerCompat.getEnabledListenerPackages(this.f3021a);
            if (!enabledListenerPackages.equals(this.f3025e)) {
                this.f3025e = enabledListenerPackages;
                List<ResolveInfo> queryIntentServices = this.f3021a.getPackageManager().queryIntentServices(new Intent().setAction(NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL), 4);
                HashSet<ComponentName> hashSet = new HashSet<>();
                for (ResolveInfo resolveInfo : queryIntentServices) {
                    if (enabledListenerPackages.contains(resolveInfo.serviceInfo.packageName)) {
                        ComponentName componentName = new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name);
                        if (resolveInfo.serviceInfo.permission != null) {
                            Log.w("NotifManCompat", "Permission present on component " + componentName + ", not adding listener record.");
                        } else {
                            hashSet.add(componentName);
                        }
                    }
                }
                for (ComponentName componentName2 : hashSet) {
                    if (!this.f3024d.containsKey(componentName2)) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Adding listener record for " + componentName2);
                        }
                        this.f3024d.put(componentName2, new ListenerRecord(componentName2));
                    }
                }
                Iterator it = this.f3024d.entrySet().iterator();
                while (it.hasNext()) {
                    Entry entry = (Entry) it.next();
                    if (!hashSet.contains(entry.getKey())) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Removing listener record for " + entry.getKey());
                        }
                        m3676b((ListenerRecord) entry.getValue());
                        it.remove();
                    }
                }
            }
        }

        /* renamed from: a */
        private void m3671a(ComponentName componentName) {
            ListenerRecord listenerRecord = (ListenerRecord) this.f3024d.get(componentName);
            if (listenerRecord != null) {
                m3676b(listenerRecord);
            }
        }

        /* renamed from: a */
        private void m3672a(ComponentName componentName, IBinder iBinder) {
            ListenerRecord listenerRecord = (ListenerRecord) this.f3024d.get(componentName);
            if (listenerRecord != null) {
                listenerRecord.service = Stub.asInterface(iBinder);
                listenerRecord.retryCount = 0;
                m3678d(listenerRecord);
            }
        }

        /* renamed from: a */
        private void m3673a(Task task) {
            m3670a();
            for (ListenerRecord listenerRecord : this.f3024d.values()) {
                listenerRecord.taskQueue.add(task);
                m3678d(listenerRecord);
            }
        }

        /* renamed from: a */
        private boolean m3674a(ListenerRecord listenerRecord) {
            if (listenerRecord.bound) {
                return true;
            }
            listenerRecord.bound = this.f3021a.bindService(new Intent(NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL).setComponent(listenerRecord.componentName), this, NotificationManagerCompat.f3002a);
            if (listenerRecord.bound) {
                listenerRecord.retryCount = 0;
            } else {
                Log.w("NotifManCompat", "Unable to bind to listener " + listenerRecord.componentName);
                this.f3021a.unbindService(this);
            }
            return listenerRecord.bound;
        }

        /* renamed from: b */
        private void m3675b(ComponentName componentName) {
            ListenerRecord listenerRecord = (ListenerRecord) this.f3024d.get(componentName);
            if (listenerRecord != null) {
                m3678d(listenerRecord);
            }
        }

        /* renamed from: b */
        private void m3676b(ListenerRecord listenerRecord) {
            if (listenerRecord.bound) {
                this.f3021a.unbindService(this);
                listenerRecord.bound = false;
            }
            listenerRecord.service = null;
        }

        /* renamed from: c */
        private void m3677c(ListenerRecord listenerRecord) {
            if (!this.f3023c.hasMessages(3, listenerRecord.componentName)) {
                listenerRecord.retryCount++;
                if (listenerRecord.retryCount > 6) {
                    Log.w("NotifManCompat", "Giving up on delivering " + listenerRecord.taskQueue.size() + " tasks to " + listenerRecord.componentName + " after " + listenerRecord.retryCount + " retries");
                    listenerRecord.taskQueue.clear();
                    return;
                }
                int i = (1 << (listenerRecord.retryCount - 1)) * 1000;
                if (Log.isLoggable("NotifManCompat", 3)) {
                    Log.d("NotifManCompat", "Scheduling retry for " + i + " ms");
                }
                this.f3023c.sendMessageDelayed(this.f3023c.obtainMessage(3, listenerRecord.componentName), (long) i);
            }
        }

        /* renamed from: d */
        private void m3678d(ListenerRecord listenerRecord) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Processing component " + listenerRecord.componentName + ", " + listenerRecord.taskQueue.size() + " queued tasks");
            }
            if (!listenerRecord.taskQueue.isEmpty()) {
                if (!m3674a(listenerRecord) || listenerRecord.service == null) {
                    m3677c(listenerRecord);
                    return;
                }
                while (true) {
                    Task task = (Task) listenerRecord.taskQueue.peek();
                    if (task == null) {
                        break;
                    }
                    try {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Sending task " + task);
                        }
                        task.send(listenerRecord.service);
                        listenerRecord.taskQueue.remove();
                    } catch (DeadObjectException e) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Remote service has died: " + listenerRecord.componentName);
                        }
                    } catch (RemoteException e2) {
                        Log.w("NotifManCompat", "RemoteException communicating with " + listenerRecord.componentName, e2);
                    }
                }
                if (!listenerRecord.taskQueue.isEmpty()) {
                    m3677c(listenerRecord);
                }
            }
        }

        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    m3673a((Task) message.obj);
                    return true;
                case 1:
                    ServiceConnectedEvent serviceConnectedEvent = (ServiceConnectedEvent) message.obj;
                    m3672a(serviceConnectedEvent.f3019a, serviceConnectedEvent.f3020b);
                    return true;
                case 2:
                    m3671a((ComponentName) message.obj);
                    return true;
                case 3:
                    m3675b((ComponentName) message.obj);
                    return true;
                default:
                    return false;
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Connected to service " + componentName);
            }
            this.f3023c.obtainMessage(1, new ServiceConnectedEvent(componentName, iBinder)).sendToTarget();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Disconnected from service " + componentName);
            }
            this.f3023c.obtainMessage(2, componentName).sendToTarget();
        }

        public void queueTask(Task task) {
            this.f3023c.obtainMessage(0, task).sendToTarget();
        }
    }

    /* renamed from: android.support.v4.app.NotificationManagerCompat$Task */
    interface Task {
        void send(INotificationSideChannel iNotificationSideChannel) throws RemoteException;
    }

    static {
        if (BuildCompat.m12482a()) {
            f3008i = new ImplApi24();
        } else if (VERSION.SDK_INT >= 19) {
            f3008i = new ImplKitKat();
        } else if (VERSION.SDK_INT >= 14) {
            f3008i = new ImplIceCreamSandwich();
        } else {
            f3008i = new ImplBase();
        }
    }

    private NotificationManagerCompat(Context context) {
        this.f3009e = context;
    }

    /* renamed from: a */
    private void m3668a(Task task) {
        synchronized (f3006g) {
            if (f3007h == null) {
                f3007h = new SideChannelManager(this.f3009e.getApplicationContext());
            }
            f3007h.queueTask(task);
        }
    }

    /* renamed from: a */
    private static boolean m3669a(Notification notification) {
        Bundle extras = NotificationCompat.getExtras(notification);
        return extras != null && extras.getBoolean(EXTRA_USE_SIDE_CHANNEL);
    }

    public static NotificationManagerCompat from(Context context) {
        return new NotificationManagerCompat(context);
    }

    public static Set<String> getEnabledListenerPackages(Context context) {
        Set<String> set;
        String string = Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        synchronized (f3003b) {
            if (string != null) {
                if (!string.equals(f3004c)) {
                    String[] split = string.split(":");
                    HashSet hashSet = new HashSet(split.length);
                    for (String unflattenFromString : split) {
                        ComponentName unflattenFromString2 = ComponentName.unflattenFromString(unflattenFromString);
                        if (unflattenFromString2 != null) {
                            hashSet.add(unflattenFromString2.getPackageName());
                        }
                    }
                    f3005d = hashSet;
                    f3004c = string;
                }
            }
            set = f3005d;
        }
        return set;
    }

    public boolean areNotificationsEnabled() {
        return f3008i.areNotificationsEnabled(this.f3009e, this.f3010f);
    }

    public void cancel(int i) {
        cancel(null, i);
    }

    public void cancel(String str, int i) {
        f3008i.cancelNotification(this.f3010f, str, i);
        if (VERSION.SDK_INT <= 19) {
            m3668a((Task) new CancelTask(this.f3009e.getPackageName(), i, str));
        }
    }

    public void cancelAll() {
        this.f3010f.cancelAll();
        if (VERSION.SDK_INT <= 19) {
            m3668a((Task) new CancelTask(this.f3009e.getPackageName()));
        }
    }

    public int getImportance() {
        return f3008i.getImportance(this.f3010f);
    }

    public void notify(int i, Notification notification) {
        notify(null, i, notification);
    }

    public void notify(String str, int i, Notification notification) {
        if (m3669a(notification)) {
            m3668a((Task) new NotifyTask(this.f3009e.getPackageName(), i, str, notification));
            f3008i.cancelNotification(this.f3010f, str, i);
            return;
        }
        f3008i.postNotification(this.f3010f, str, i, notification);
    }
}
