package android.support.p001v4.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

/* renamed from: android.support.v4.app.FragmentState */
/* compiled from: Fragment */
final class FragmentState implements Parcelable {
    public static final Creator<FragmentState> CREATOR = new Creator<FragmentState>() {
        public FragmentState createFromParcel(Parcel parcel) {
            return new FragmentState(parcel);
        }

        public FragmentState[] newArray(int i) {
            return new FragmentState[i];
        }
    };

    /* renamed from: a */
    final String f2823a;

    /* renamed from: b */
    final int f2824b;

    /* renamed from: c */
    final boolean f2825c;

    /* renamed from: d */
    final int f2826d;

    /* renamed from: e */
    final int f2827e;

    /* renamed from: f */
    final String f2828f;

    /* renamed from: g */
    final boolean f2829g;

    /* renamed from: h */
    final boolean f2830h;

    /* renamed from: i */
    final Bundle f2831i;

    /* renamed from: j */
    final boolean f2832j;

    /* renamed from: k */
    Bundle f2833k;

    /* renamed from: l */
    Fragment f2834l;

    public FragmentState(Parcel parcel) {
        boolean z = true;
        this.f2823a = parcel.readString();
        this.f2824b = parcel.readInt();
        this.f2825c = parcel.readInt() != 0;
        this.f2826d = parcel.readInt();
        this.f2827e = parcel.readInt();
        this.f2828f = parcel.readString();
        this.f2829g = parcel.readInt() != 0;
        this.f2830h = parcel.readInt() != 0;
        this.f2831i = parcel.readBundle();
        if (parcel.readInt() == 0) {
            z = false;
        }
        this.f2832j = z;
        this.f2833k = parcel.readBundle();
    }

    public FragmentState(Fragment fragment) {
        this.f2823a = fragment.getClass().getName();
        this.f2824b = fragment.mIndex;
        this.f2825c = fragment.mFromLayout;
        this.f2826d = fragment.mFragmentId;
        this.f2827e = fragment.mContainerId;
        this.f2828f = fragment.mTag;
        this.f2829g = fragment.mRetainInstance;
        this.f2830h = fragment.mDetached;
        this.f2831i = fragment.mArguments;
        this.f2832j = fragment.mHidden;
    }

    public int describeContents() {
        return 0;
    }

    public Fragment instantiate(FragmentHostCallback fragmentHostCallback, Fragment fragment, FragmentManagerNonConfig fragmentManagerNonConfig) {
        if (this.f2834l == null) {
            Context b = fragmentHostCallback.mo2253b();
            if (this.f2831i != null) {
                this.f2831i.setClassLoader(b.getClassLoader());
            }
            this.f2834l = Fragment.instantiate(b, this.f2823a, this.f2831i);
            if (this.f2833k != null) {
                this.f2833k.setClassLoader(b.getClassLoader());
                this.f2834l.mSavedFragmentState = this.f2833k;
            }
            this.f2834l.setIndex(this.f2824b, fragment);
            this.f2834l.mFromLayout = this.f2825c;
            this.f2834l.mRestored = true;
            this.f2834l.mFragmentId = this.f2826d;
            this.f2834l.mContainerId = this.f2827e;
            this.f2834l.mTag = this.f2828f;
            this.f2834l.mRetainInstance = this.f2829g;
            this.f2834l.mDetached = this.f2830h;
            this.f2834l.mHidden = this.f2832j;
            this.f2834l.mFragmentManager = fragmentHostCallback.f2764d;
            if (FragmentManagerImpl.f2775a) {
                Log.v("FragmentManager", "Instantiated fragment " + this.f2834l);
            }
        }
        this.f2834l.mChildNonConfig = fragmentManagerNonConfig;
        return this.f2834l;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.f2823a);
        parcel.writeInt(this.f2824b);
        parcel.writeInt(this.f2825c ? 1 : 0);
        parcel.writeInt(this.f2826d);
        parcel.writeInt(this.f2827e);
        parcel.writeString(this.f2828f);
        parcel.writeInt(this.f2829g ? 1 : 0);
        parcel.writeInt(this.f2830h ? 1 : 0);
        parcel.writeBundle(this.f2831i);
        if (!this.f2832j) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        parcel.writeBundle(this.f2833k);
    }
}
