package android.support.p001v4.app;

import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.p001v4.app.RemoteInputCompatBase.RemoteInput.Factory;
import android.util.Log;

/* renamed from: android.support.v4.app.RemoteInput */
public final class RemoteInput extends android.support.p001v4.app.RemoteInputCompatBase.RemoteInput {
    public static final String EXTRA_RESULTS_DATA = "android.remoteinput.resultsData";
    public static final Factory FACTORY = new Factory() {
        public RemoteInput build(String str, CharSequence charSequence, CharSequence[] charSequenceArr, boolean z, Bundle bundle) {
            return new RemoteInput(str, charSequence, charSequenceArr, z, bundle);
        }

        public RemoteInput[] newArray(int i) {
            return new RemoteInput[i];
        }
    };
    public static final String RESULTS_CLIP_LABEL = "android.remoteinput.results";

    /* renamed from: f */
    private static final Impl f3026f;

    /* renamed from: a */
    private final String f3027a;

    /* renamed from: b */
    private final CharSequence f3028b;

    /* renamed from: c */
    private final CharSequence[] f3029c;

    /* renamed from: d */
    private final boolean f3030d;

    /* renamed from: e */
    private final Bundle f3031e;

    /* renamed from: android.support.v4.app.RemoteInput$Builder */
    public static final class Builder {

        /* renamed from: a */
        private final String f3032a;

        /* renamed from: b */
        private CharSequence f3033b;

        /* renamed from: c */
        private CharSequence[] f3034c;

        /* renamed from: d */
        private boolean f3035d = true;

        /* renamed from: e */
        private Bundle f3036e = new Bundle();

        public Builder(String str) {
            if (str == null) {
                throw new IllegalArgumentException("Result key can't be null");
            }
            this.f3032a = str;
        }

        public Builder addExtras(Bundle bundle) {
            if (bundle != null) {
                this.f3036e.putAll(bundle);
            }
            return this;
        }

        public RemoteInput build() {
            return new RemoteInput(this.f3032a, this.f3033b, this.f3034c, this.f3035d, this.f3036e);
        }

        public Bundle getExtras() {
            return this.f3036e;
        }

        public Builder setAllowFreeFormInput(boolean z) {
            this.f3035d = z;
            return this;
        }

        public Builder setChoices(CharSequence[] charSequenceArr) {
            this.f3034c = charSequenceArr;
            return this;
        }

        public Builder setLabel(CharSequence charSequence) {
            this.f3033b = charSequence;
            return this;
        }
    }

    /* renamed from: android.support.v4.app.RemoteInput$Impl */
    interface Impl {
        void addResultsToIntent(RemoteInput[] remoteInputArr, Intent intent, Bundle bundle);

        Bundle getResultsFromIntent(Intent intent);
    }

    /* renamed from: android.support.v4.app.RemoteInput$ImplApi20 */
    static class ImplApi20 implements Impl {
        ImplApi20() {
        }

        public void addResultsToIntent(RemoteInput[] remoteInputArr, Intent intent, Bundle bundle) {
            RemoteInputCompatApi20.m3680a(remoteInputArr, intent, bundle);
        }

        public Bundle getResultsFromIntent(Intent intent) {
            return RemoteInputCompatApi20.m3679a(intent);
        }
    }

    /* renamed from: android.support.v4.app.RemoteInput$ImplBase */
    static class ImplBase implements Impl {
        ImplBase() {
        }

        public void addResultsToIntent(RemoteInput[] remoteInputArr, Intent intent, Bundle bundle) {
            Log.w("RemoteInput", "RemoteInput is only supported from API Level 16");
        }

        public Bundle getResultsFromIntent(Intent intent) {
            Log.w("RemoteInput", "RemoteInput is only supported from API Level 16");
            return null;
        }
    }

    /* renamed from: android.support.v4.app.RemoteInput$ImplJellybean */
    static class ImplJellybean implements Impl {
        ImplJellybean() {
        }

        public void addResultsToIntent(RemoteInput[] remoteInputArr, Intent intent, Bundle bundle) {
            RemoteInputCompatJellybean.m3686a(remoteInputArr, intent, bundle);
        }

        public Bundle getResultsFromIntent(Intent intent) {
            return RemoteInputCompatJellybean.m3683a(intent);
        }
    }

    static {
        if (VERSION.SDK_INT >= 20) {
            f3026f = new ImplApi20();
        } else if (VERSION.SDK_INT >= 16) {
            f3026f = new ImplJellybean();
        } else {
            f3026f = new ImplBase();
        }
    }

    RemoteInput(String str, CharSequence charSequence, CharSequence[] charSequenceArr, boolean z, Bundle bundle) {
        this.f3027a = str;
        this.f3028b = charSequence;
        this.f3029c = charSequenceArr;
        this.f3030d = z;
        this.f3031e = bundle;
    }

    public static void addResultsToIntent(RemoteInput[] remoteInputArr, Intent intent, Bundle bundle) {
        f3026f.addResultsToIntent(remoteInputArr, intent, bundle);
    }

    public static Bundle getResultsFromIntent(Intent intent) {
        return f3026f.getResultsFromIntent(intent);
    }

    public boolean getAllowFreeFormInput() {
        return this.f3030d;
    }

    public CharSequence[] getChoices() {
        return this.f3029c;
    }

    public Bundle getExtras() {
        return this.f3031e;
    }

    public CharSequence getLabel() {
        return this.f3028b;
    }

    public String getResultKey() {
        return this.f3027a;
    }
}
