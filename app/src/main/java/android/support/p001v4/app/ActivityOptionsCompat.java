package android.support.p001v4.app;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;

/* renamed from: android.support.v4.app.ActivityOptionsCompat */
public class ActivityOptionsCompat {
    public static final String EXTRA_USAGE_TIME_REPORT = "android.activity.usage_time";
    public static final String EXTRA_USAGE_TIME_REPORT_PACKAGES = "android.usage_time_packages";

    /* renamed from: android.support.v4.app.ActivityOptionsCompat$ActivityOptionsImpl21 */
    static class ActivityOptionsImpl21 extends ActivityOptionsCompat {

        /* renamed from: a */
        private final ActivityOptionsCompat21 f2680a;

        ActivityOptionsImpl21(ActivityOptionsCompat21 activityOptionsCompat21) {
            this.f2680a = activityOptionsCompat21;
        }

        public Bundle toBundle() {
            return this.f2680a.toBundle();
        }

        public void update(ActivityOptionsCompat activityOptionsCompat) {
            if (activityOptionsCompat instanceof ActivityOptionsImpl21) {
                this.f2680a.update(((ActivityOptionsImpl21) activityOptionsCompat).f2680a);
            }
        }
    }

    /* renamed from: android.support.v4.app.ActivityOptionsCompat$ActivityOptionsImpl23 */
    static class ActivityOptionsImpl23 extends ActivityOptionsCompat {

        /* renamed from: a */
        private final ActivityOptionsCompat23 f2681a;

        ActivityOptionsImpl23(ActivityOptionsCompat23 activityOptionsCompat23) {
            this.f2681a = activityOptionsCompat23;
        }

        public void requestUsageTimeReport(PendingIntent pendingIntent) {
            this.f2681a.requestUsageTimeReport(pendingIntent);
        }

        public Bundle toBundle() {
            return this.f2681a.toBundle();
        }

        public void update(ActivityOptionsCompat activityOptionsCompat) {
            if (activityOptionsCompat instanceof ActivityOptionsImpl23) {
                this.f2681a.update(((ActivityOptionsImpl23) activityOptionsCompat).f2681a);
            }
        }
    }

    /* renamed from: android.support.v4.app.ActivityOptionsCompat$ActivityOptionsImpl24 */
    static class ActivityOptionsImpl24 extends ActivityOptionsCompat {

        /* renamed from: a */
        private final ActivityOptionsCompat24 f2682a;

        ActivityOptionsImpl24(ActivityOptionsCompat24 activityOptionsCompat24) {
            this.f2682a = activityOptionsCompat24;
        }

        public Rect getLaunchBounds() {
            return this.f2682a.getLaunchBounds();
        }

        public void requestUsageTimeReport(PendingIntent pendingIntent) {
            this.f2682a.requestUsageTimeReport(pendingIntent);
        }

        public ActivityOptionsCompat setLaunchBounds(Rect rect) {
            return new ActivityOptionsImpl24(this.f2682a.setLaunchBounds(rect));
        }

        public Bundle toBundle() {
            return this.f2682a.toBundle();
        }

        public void update(ActivityOptionsCompat activityOptionsCompat) {
            if (activityOptionsCompat instanceof ActivityOptionsImpl24) {
                this.f2682a.update(((ActivityOptionsImpl24) activityOptionsCompat).f2682a);
            }
        }
    }

    /* renamed from: android.support.v4.app.ActivityOptionsCompat$ActivityOptionsImplJB */
    static class ActivityOptionsImplJB extends ActivityOptionsCompat {

        /* renamed from: a */
        private final ActivityOptionsCompatJB f2683a;

        ActivityOptionsImplJB(ActivityOptionsCompatJB activityOptionsCompatJB) {
            this.f2683a = activityOptionsCompatJB;
        }

        public Bundle toBundle() {
            return this.f2683a.toBundle();
        }

        public void update(ActivityOptionsCompat activityOptionsCompat) {
            if (activityOptionsCompat instanceof ActivityOptionsImplJB) {
                this.f2683a.update(((ActivityOptionsImplJB) activityOptionsCompat).f2683a);
            }
        }
    }

    protected ActivityOptionsCompat() {
    }

    public static ActivityOptionsCompat makeBasic() {
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeBasic()) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeBasic()) : new ActivityOptionsCompat();
    }

    public static ActivityOptionsCompat makeClipRevealAnimation(View view, int i, int i2, int i3, int i4) {
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeClipRevealAnimation(view, i, i2, i3, i4)) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeClipRevealAnimation(view, i, i2, i3, i4)) : new ActivityOptionsCompat();
    }

    public static ActivityOptionsCompat makeCustomAnimation(Context context, int i, int i2) {
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeCustomAnimation(context, i, i2)) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeCustomAnimation(context, i, i2)) : VERSION.SDK_INT >= 21 ? new ActivityOptionsImpl21(ActivityOptionsCompat21.makeCustomAnimation(context, i, i2)) : VERSION.SDK_INT >= 16 ? new ActivityOptionsImplJB(ActivityOptionsCompatJB.makeCustomAnimation(context, i, i2)) : new ActivityOptionsCompat();
    }

    public static ActivityOptionsCompat makeScaleUpAnimation(View view, int i, int i2, int i3, int i4) {
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeScaleUpAnimation(view, i, i2, i3, i4)) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeScaleUpAnimation(view, i, i2, i3, i4)) : VERSION.SDK_INT >= 21 ? new ActivityOptionsImpl21(ActivityOptionsCompat21.makeScaleUpAnimation(view, i, i2, i3, i4)) : VERSION.SDK_INT >= 16 ? new ActivityOptionsImplJB(ActivityOptionsCompatJB.makeScaleUpAnimation(view, i, i2, i3, i4)) : new ActivityOptionsCompat();
    }

    public static ActivityOptionsCompat makeSceneTransitionAnimation(Activity activity, View view, String str) {
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeSceneTransitionAnimation(activity, view, str)) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeSceneTransitionAnimation(activity, view, str)) : VERSION.SDK_INT >= 21 ? new ActivityOptionsImpl21(ActivityOptionsCompat21.makeSceneTransitionAnimation(activity, view, str)) : new ActivityOptionsCompat();
    }

    public static ActivityOptionsCompat makeSceneTransitionAnimation(Activity activity, Pair<View, String>... drVarArr) {
        if (VERSION.SDK_INT < 21) {
            return new ActivityOptionsCompat();
        }
        View[] viewArr = null;
        String[] strArr = null;
        if (drVarArr != null) {
            viewArr = new View[drVarArr.length];
            strArr = new String[drVarArr.length];
            for (int i = 0; i < drVarArr.length; i++) {
                viewArr[i] = (View) drVarArr[i].f9919a;
                strArr[i] = (String) drVarArr[i].f9920b;
            }
        }
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeSceneTransitionAnimation(activity, viewArr, strArr)) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeSceneTransitionAnimation(activity, viewArr, strArr)) : new ActivityOptionsImpl21(ActivityOptionsCompat21.makeSceneTransitionAnimation(activity, viewArr, strArr));
    }

    public static ActivityOptionsCompat makeTaskLaunchBehind() {
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeTaskLaunchBehind()) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeTaskLaunchBehind()) : VERSION.SDK_INT >= 21 ? new ActivityOptionsImpl21(ActivityOptionsCompat21.makeTaskLaunchBehind()) : new ActivityOptionsCompat();
    }

    public static ActivityOptionsCompat makeThumbnailScaleUpAnimation(View view, Bitmap bitmap, int i, int i2) {
        return VERSION.SDK_INT >= 24 ? new ActivityOptionsImpl24(ActivityOptionsCompat24.makeThumbnailScaleUpAnimation(view, bitmap, i, i2)) : VERSION.SDK_INT >= 23 ? new ActivityOptionsImpl23(ActivityOptionsCompat23.makeThumbnailScaleUpAnimation(view, bitmap, i, i2)) : VERSION.SDK_INT >= 21 ? new ActivityOptionsImpl21(ActivityOptionsCompat21.makeThumbnailScaleUpAnimation(view, bitmap, i, i2)) : VERSION.SDK_INT >= 16 ? new ActivityOptionsImplJB(ActivityOptionsCompatJB.makeThumbnailScaleUpAnimation(view, bitmap, i, i2)) : new ActivityOptionsCompat();
    }

    public Rect getLaunchBounds() {
        return null;
    }

    public void requestUsageTimeReport(PendingIntent pendingIntent) {
    }

    public ActivityOptionsCompat setLaunchBounds(Rect rect) {
        return null;
    }

    public Bundle toBundle() {
        return null;
    }

    public void update(ActivityOptionsCompat activityOptionsCompat) {
    }
}
