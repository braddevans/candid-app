package android.support.p001v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: android.support.v4.app.FragmentHostCallback */
public abstract class FragmentHostCallback<E> extends FragmentContainer {

    /* renamed from: a */
    private final Activity f2761a;

    /* renamed from: b */
    final Context f2762b;

    /* renamed from: c */
    final int f2763c;

    /* renamed from: d */
    final FragmentManagerImpl f2764d;

    /* renamed from: e */
    private final Handler f2765e;

    /* renamed from: f */
    private SimpleArrayMap<String, LoaderManager> f2766f;

    /* renamed from: g */
    private boolean f2767g;

    /* renamed from: h */
    private LoaderManagerImpl f2768h;

    /* renamed from: i */
    private boolean f2769i;

    /* renamed from: j */
    private boolean f2770j;

    FragmentHostCallback(Activity activity, Context context, Handler handler, int i) {
        this.f2764d = new FragmentManagerImpl();
        this.f2761a = activity;
        this.f2762b = context;
        this.f2765e = handler;
        this.f2763c = i;
    }

    public FragmentHostCallback(Context context, Handler handler, int i) {
        this(null, context, handler, i);
    }

    FragmentHostCallback(FragmentActivity fragmentActivity) {
        this(fragmentActivity, fragmentActivity, fragmentActivity.mHandler, 0);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public Activity mo2247a() {
        return this.f2761a;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public LoaderManagerImpl mo2248a(String str, boolean z, boolean z2) {
        if (this.f2766f == null) {
            this.f2766f = new SimpleArrayMap<>();
        }
        LoaderManagerImpl loaderManagerImpl = (LoaderManagerImpl) this.f2766f.get(str);
        if (loaderManagerImpl != null) {
            loaderManagerImpl.mo2418a(this);
            return loaderManagerImpl;
        } else if (!z2) {
            return loaderManagerImpl;
        } else {
            LoaderManagerImpl loaderManagerImpl2 = new LoaderManagerImpl(str, this, z);
            this.f2766f.put(str, loaderManagerImpl2);
            return loaderManagerImpl2;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2249a(SimpleArrayMap<String, LoaderManager> dtVar) {
        this.f2766f = dtVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2250a(String str) {
        if (this.f2766f != null) {
            LoaderManagerImpl loaderManagerImpl = (LoaderManagerImpl) this.f2766f.get(str);
            if (loaderManagerImpl != null && !loaderManagerImpl.f2895f) {
                loaderManagerImpl.mo2425g();
                this.f2766f.remove(str);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2251a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.f2770j);
        if (this.f2768h != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.f2768h)));
            printWriter.println(":");
            this.f2768h.dump(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2252a(boolean z) {
        this.f2767g = z;
        if (this.f2768h != null && this.f2770j) {
            this.f2770j = false;
            if (z) {
                this.f2768h.mo2421c();
            } else {
                this.f2768h.mo2420b();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public Context mo2253b() {
        return this.f2762b;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public Handler mo2254c() {
        return this.f2765e;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public FragmentManagerImpl mo2255d() {
        return this.f2764d;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public LoaderManagerImpl mo2256e() {
        if (this.f2768h != null) {
            return this.f2768h;
        }
        this.f2769i = true;
        this.f2768h = mo2248a("(root)", this.f2770j, true);
        return this.f2768h;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public boolean mo2257f() {
        return this.f2767g;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public void mo2258g() {
        if (!this.f2770j) {
            this.f2770j = true;
            if (this.f2768h != null) {
                this.f2768h.mo2417a();
            } else if (!this.f2769i) {
                this.f2768h = mo2248a("(root)", this.f2770j, false);
                if (this.f2768h != null && !this.f2768h.f2894e) {
                    this.f2768h.mo2417a();
                }
            }
            this.f2769i = true;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: h */
    public void mo2259h() {
        if (this.f2768h != null) {
            this.f2768h.mo2421c();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: i */
    public void mo2260i() {
        if (this.f2768h != null) {
            this.f2768h.mo2425g();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: j */
    public void mo2261j() {
        if (this.f2766f != null) {
            int size = this.f2766f.size();
            LoaderManagerImpl[] loaderManagerImplArr = new LoaderManagerImpl[size];
            for (int i = size - 1; i >= 0; i--) {
                loaderManagerImplArr[i] = (LoaderManagerImpl) this.f2766f.mo13305c(i);
            }
            for (int i2 = 0; i2 < size; i2++) {
                LoaderManagerImpl loaderManagerImpl = loaderManagerImplArr[i2];
                loaderManagerImpl.mo2422d();
                loaderManagerImpl.mo2424f();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: k */
    public SimpleArrayMap<String, LoaderManager> mo2262k() {
        boolean z = false;
        if (this.f2766f != null) {
            int size = this.f2766f.size();
            LoaderManagerImpl[] loaderManagerImplArr = new LoaderManagerImpl[size];
            for (int i = size - 1; i >= 0; i--) {
                loaderManagerImplArr[i] = (LoaderManagerImpl) this.f2766f.mo13305c(i);
            }
            boolean f = mo2257f();
            for (int i2 = 0; i2 < size; i2++) {
                LoaderManagerImpl loaderManagerImpl = loaderManagerImplArr[i2];
                if (!loaderManagerImpl.f2895f && f) {
                    if (!loaderManagerImpl.f2894e) {
                        loaderManagerImpl.mo2417a();
                    }
                    loaderManagerImpl.mo2421c();
                }
                if (loaderManagerImpl.f2895f) {
                    z = true;
                } else {
                    loaderManagerImpl.mo2425g();
                    this.f2766f.remove(loaderManagerImpl.f2893d);
                }
            }
        }
        if (z) {
            return this.f2766f;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void onAttachFragment(Fragment fragment) {
    }

    public void onDump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public View onFindViewById(int i) {
        return null;
    }

    public abstract E onGetHost();

    public LayoutInflater onGetLayoutInflater() {
        return (LayoutInflater) this.f2762b.getSystemService("layout_inflater");
    }

    public int onGetWindowAnimations() {
        return this.f2763c;
    }

    public boolean onHasView() {
        return true;
    }

    public boolean onHasWindowAnimations() {
        return true;
    }

    public void onRequestPermissionsFromFragment(Fragment fragment, String[] strArr, int i) {
    }

    public boolean onShouldSaveFragmentState(Fragment fragment) {
        return true;
    }

    public boolean onShouldShowRequestPermissionRationale(String str) {
        return false;
    }

    public void onStartActivityFromFragment(Fragment fragment, Intent intent, int i) {
        onStartActivityFromFragment(fragment, intent, i, null);
    }

    public void onStartActivityFromFragment(Fragment fragment, Intent intent, int i, Bundle bundle) {
        if (i != -1) {
            throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
        this.f2762b.startActivity(intent);
    }

    public void onStartIntentSenderFromFragment(Fragment fragment, IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) throws SendIntentException {
        if (i != -1) {
            throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
        }
        ActivityCompat.startIntentSenderForResult(this.f2761a, intentSender, i, intent, i2, i3, i4, bundle);
    }

    public void onSupportInvalidateOptionsMenu() {
    }
}
