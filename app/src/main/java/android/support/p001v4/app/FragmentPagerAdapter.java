package android.support.p001v4.app;

import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: android.support.v4.app.FragmentPagerAdapter */
public abstract class FragmentPagerAdapter extends PagerAdapter {

    /* renamed from: a */
    private final FragmentManager f2820a;

    /* renamed from: b */
    private FragmentTransaction f2821b = null;

    /* renamed from: c */
    private Fragment f2822c = null;

    public FragmentPagerAdapter(FragmentManager fragmentManager) {
        this.f2820a = fragmentManager;
    }

    /* renamed from: a */
    private static String m3610a(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        if (this.f2821b == null) {
            this.f2821b = this.f2820a.beginTransaction();
        }
        this.f2821b.detach((Fragment) obj);
    }

    public void finishUpdate(ViewGroup viewGroup) {
        if (this.f2821b != null) {
            this.f2821b.commitNowAllowingStateLoss();
            this.f2821b = null;
        }
    }

    public abstract Fragment getItem(int i);

    public long getItemId(int i) {
        return (long) i;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (this.f2821b == null) {
            this.f2821b = this.f2820a.beginTransaction();
        }
        long itemId = getItemId(i);
        Fragment findFragmentByTag = this.f2820a.findFragmentByTag(m3610a(viewGroup.getId(), itemId));
        if (findFragmentByTag != null) {
            this.f2821b.attach(findFragmentByTag);
        } else {
            findFragmentByTag = getItem(i);
            this.f2821b.add(viewGroup.getId(), findFragmentByTag, m3610a(viewGroup.getId(), itemId));
        }
        if (findFragmentByTag != this.f2822c) {
            findFragmentByTag.setMenuVisibility(false);
            findFragmentByTag.setUserVisibleHint(false);
        }
        return findFragmentByTag;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
    }

    public Parcelable saveState() {
        return null;
    }

    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.f2822c) {
            if (this.f2822c != null) {
                this.f2822c.setMenuVisibility(false);
                this.f2822c.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            this.f2822c = fragment;
        }
    }

    public void startUpdate(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }
}
