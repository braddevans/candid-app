package android.support.p001v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* renamed from: android.support.v4.app.FragmentManagerState */
/* compiled from: FragmentManager */
final class FragmentManagerState implements Parcelable {
    public static final Creator<FragmentManagerState> CREATOR = new Creator<FragmentManagerState>() {
        public FragmentManagerState createFromParcel(Parcel parcel) {
            return new FragmentManagerState(parcel);
        }

        public FragmentManagerState[] newArray(int i) {
            return new FragmentManagerState[i];
        }
    };

    /* renamed from: a */
    FragmentState[] f2817a;

    /* renamed from: b */
    int[] f2818b;

    /* renamed from: c */
    BackStackState[] f2819c;

    public FragmentManagerState() {
    }

    public FragmentManagerState(Parcel parcel) {
        this.f2817a = (FragmentState[]) parcel.createTypedArray(FragmentState.CREATOR);
        this.f2818b = parcel.createIntArray();
        this.f2819c = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.f2817a, i);
        parcel.writeIntArray(this.f2818b);
        parcel.writeTypedArray(this.f2819c, i);
    }
}
