package android.support.p001v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.app.FragmentController */
public class FragmentController {

    /* renamed from: a */
    private final FragmentHostCallback<?> f2760a;

    private FragmentController(FragmentHostCallback<?> fragmentHostCallback) {
        this.f2760a = fragmentHostCallback;
    }

    public static final FragmentController createController(FragmentHostCallback<?> fragmentHostCallback) {
        return new FragmentController(fragmentHostCallback);
    }

    public void attachHost(Fragment fragment) {
        this.f2760a.f2764d.attachController(this.f2760a, this.f2760a, fragment);
    }

    public void dispatchActivityCreated() {
        this.f2760a.f2764d.dispatchActivityCreated();
    }

    public void dispatchConfigurationChanged(Configuration configuration) {
        this.f2760a.f2764d.dispatchConfigurationChanged(configuration);
    }

    public boolean dispatchContextItemSelected(MenuItem menuItem) {
        return this.f2760a.f2764d.dispatchContextItemSelected(menuItem);
    }

    public void dispatchCreate() {
        this.f2760a.f2764d.dispatchCreate();
    }

    public boolean dispatchCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        return this.f2760a.f2764d.dispatchCreateOptionsMenu(menu, menuInflater);
    }

    public void dispatchDestroy() {
        this.f2760a.f2764d.dispatchDestroy();
    }

    public void dispatchDestroyView() {
        this.f2760a.f2764d.dispatchDestroyView();
    }

    public void dispatchLowMemory() {
        this.f2760a.f2764d.dispatchLowMemory();
    }

    public void dispatchMultiWindowModeChanged(boolean z) {
        this.f2760a.f2764d.dispatchMultiWindowModeChanged(z);
    }

    public boolean dispatchOptionsItemSelected(MenuItem menuItem) {
        return this.f2760a.f2764d.dispatchOptionsItemSelected(menuItem);
    }

    public void dispatchOptionsMenuClosed(Menu menu) {
        this.f2760a.f2764d.dispatchOptionsMenuClosed(menu);
    }

    public void dispatchPause() {
        this.f2760a.f2764d.dispatchPause();
    }

    public void dispatchPictureInPictureModeChanged(boolean z) {
        this.f2760a.f2764d.dispatchPictureInPictureModeChanged(z);
    }

    public boolean dispatchPrepareOptionsMenu(Menu menu) {
        return this.f2760a.f2764d.dispatchPrepareOptionsMenu(menu);
    }

    public void dispatchReallyStop() {
        this.f2760a.f2764d.dispatchReallyStop();
    }

    public void dispatchResume() {
        this.f2760a.f2764d.dispatchResume();
    }

    public void dispatchStart() {
        this.f2760a.f2764d.dispatchStart();
    }

    public void dispatchStop() {
        this.f2760a.f2764d.dispatchStop();
    }

    public void doLoaderDestroy() {
        this.f2760a.mo2260i();
    }

    public void doLoaderRetain() {
        this.f2760a.mo2259h();
    }

    public void doLoaderStart() {
        this.f2760a.mo2258g();
    }

    public void doLoaderStop(boolean z) {
        this.f2760a.mo2252a(z);
    }

    public void dumpLoaders(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.f2760a.mo2251a(str, fileDescriptor, printWriter, strArr);
    }

    public boolean execPendingActions() {
        return this.f2760a.f2764d.execPendingActions();
    }

    public Fragment findFragmentByWho(String str) {
        return this.f2760a.f2764d.findFragmentByWho(str);
    }

    public List<Fragment> getActiveFragments(List<Fragment> list) {
        if (this.f2760a.f2764d.f2781f == null) {
            return null;
        }
        if (list == null) {
            list = new ArrayList<>(getActiveFragmentsCount());
        }
        list.addAll(this.f2760a.f2764d.f2781f);
        return list;
    }

    public int getActiveFragmentsCount() {
        ArrayList<Fragment> arrayList = this.f2760a.f2764d.f2781f;
        if (arrayList == null) {
            return 0;
        }
        return arrayList.size();
    }

    public FragmentManager getSupportFragmentManager() {
        return this.f2760a.mo2255d();
    }

    public LoaderManager getSupportLoaderManager() {
        return this.f2760a.mo2256e();
    }

    public void noteStateNotSaved() {
        this.f2760a.f2764d.noteStateNotSaved();
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return this.f2760a.f2764d.onCreateView(view, str, context, attributeSet);
    }

    public void reportLoaderStart() {
        this.f2760a.mo2261j();
    }

    public void restoreAllState(Parcelable parcelable, FragmentManagerNonConfig fragmentManagerNonConfig) {
        this.f2760a.f2764d.mo2289a(parcelable, fragmentManagerNonConfig);
    }

    @Deprecated
    public void restoreAllState(Parcelable parcelable, List<Fragment> list) {
        this.f2760a.f2764d.mo2289a(parcelable, new FragmentManagerNonConfig(list, null));
    }

    public void restoreLoaderNonConfig(SimpleArrayMap<String, LoaderManager> dtVar) {
        this.f2760a.mo2249a(dtVar);
    }

    public SimpleArrayMap<String, LoaderManager> retainLoaderNonConfig() {
        return this.f2760a.mo2262k();
    }

    public FragmentManagerNonConfig retainNestedNonConfig() {
        return this.f2760a.f2764d.mo2303d();
    }

    @Deprecated
    public List<Fragment> retainNonConfig() {
        FragmentManagerNonConfig d = this.f2760a.f2764d.mo2303d();
        if (d != null) {
            return d.mo2348a();
        }
        return null;
    }

    public Parcelable saveAllState() {
        return this.f2760a.f2764d.mo2325e();
    }
}
