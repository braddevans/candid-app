package android.support.p001v4.app;

import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: android.support.v4.app.BundleCompatGingerbread */
class BundleCompatGingerbread {

    /* renamed from: a */
    private static Method f2749a;

    /* renamed from: b */
    private static boolean f2750b;

    /* renamed from: c */
    private static Method f2751c;

    /* renamed from: d */
    private static boolean f2752d;

    BundleCompatGingerbread() {
    }

    public static IBinder getBinder(Bundle bundle, String str) {
        if (!f2750b) {
            try {
                f2749a = Bundle.class.getMethod("getIBinder", new Class[]{String.class});
                f2749a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("BundleCompatGingerbread", "Failed to retrieve getIBinder method", e);
            }
            f2750b = true;
        }
        if (f2749a != null) {
            try {
                return (IBinder) f2749a.invoke(bundle, new Object[]{str});
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                Log.i("BundleCompatGingerbread", "Failed to invoke getIBinder via reflection", e2);
                f2749a = null;
            }
        }
        return null;
    }

    public static void putBinder(Bundle bundle, String str, IBinder iBinder) {
        if (!f2752d) {
            try {
                f2751c = Bundle.class.getMethod("putIBinder", new Class[]{String.class, IBinder.class});
                f2751c.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("BundleCompatGingerbread", "Failed to retrieve putIBinder method", e);
            }
            f2752d = true;
        }
        if (f2751c != null) {
            try {
                f2751c.invoke(bundle, new Object[]{str, iBinder});
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                Log.i("BundleCompatGingerbread", "Failed to invoke putIBinder via reflection", e2);
                f2751c = null;
            }
        }
    }
}
