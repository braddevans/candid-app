package android.support.p001v4.app;

import android.content.Context;
import android.os.Build.VERSION;

/* renamed from: android.support.v4.app.AppOpsManagerCompat */
public final class AppOpsManagerCompat {
    public static final int MODE_ALLOWED = 0;
    public static final int MODE_DEFAULT = 3;
    public static final int MODE_IGNORED = 1;

    /* renamed from: a */
    private static final AppOpsManagerImpl f2688a;

    /* renamed from: android.support.v4.app.AppOpsManagerCompat$AppOpsManager23 */
    static class AppOpsManager23 extends AppOpsManagerImpl {
        AppOpsManager23() {
        }

        public int noteOp(Context context, String str, int i, String str2) {
            return AppOpsManagerCompat23.noteOp(context, str, i, str2);
        }

        public int noteProxyOp(Context context, String str, String str2) {
            return AppOpsManagerCompat23.noteProxyOp(context, str, str2);
        }

        public String permissionToOp(String str) {
            return AppOpsManagerCompat23.permissionToOp(str);
        }
    }

    /* renamed from: android.support.v4.app.AppOpsManagerCompat$AppOpsManagerImpl */
    static class AppOpsManagerImpl {
        AppOpsManagerImpl() {
        }

        public int noteOp(Context context, String str, int i, String str2) {
            return 1;
        }

        public int noteProxyOp(Context context, String str, String str2) {
            return 1;
        }

        public String permissionToOp(String str) {
            return null;
        }
    }

    static {
        if (VERSION.SDK_INT >= 23) {
            f2688a = new AppOpsManager23();
        } else {
            f2688a = new AppOpsManagerImpl();
        }
    }

    private AppOpsManagerCompat() {
    }

    public static int noteOp(Context context, String str, int i, String str2) {
        return f2688a.noteOp(context, str, i, str2);
    }

    public static int noteProxyOp(Context context, String str, String str2) {
        return f2688a.noteProxyOp(context, str, str2);
    }

    public static String permissionToOp(String str) {
        return f2688a.permissionToOp(str);
    }
}
