package android.support.p001v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.p001v4.app.NotificationCompatBase.Action.Factory;
import android.support.p001v4.app.NotificationCompatBase.UnreadConversation;
import android.support.p001v4.app.RemoteInputCompatBase.RemoteInput;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: android.support.v4.app.NotificationCompat */
public class NotificationCompat {
    public static final String CATEGORY_ALARM = "alarm";
    public static final String CATEGORY_CALL = "call";
    public static final String CATEGORY_EMAIL = "email";
    public static final String CATEGORY_ERROR = "err";
    public static final String CATEGORY_EVENT = "event";
    public static final String CATEGORY_MESSAGE = "msg";
    public static final String CATEGORY_PROGRESS = "progress";
    public static final String CATEGORY_PROMO = "promo";
    public static final String CATEGORY_RECOMMENDATION = "recommendation";
    public static final String CATEGORY_REMINDER = "reminder";
    public static final String CATEGORY_SERVICE = "service";
    public static final String CATEGORY_SOCIAL = "social";
    public static final String CATEGORY_STATUS = "status";
    public static final String CATEGORY_SYSTEM = "sys";
    public static final String CATEGORY_TRANSPORT = "transport";
    public static final int COLOR_DEFAULT = 0;
    public static final int DEFAULT_ALL = -1;
    public static final int DEFAULT_LIGHTS = 4;
    public static final int DEFAULT_SOUND = 1;
    public static final int DEFAULT_VIBRATE = 2;
    public static final String EXTRA_BACKGROUND_IMAGE_URI = "android.backgroundImageUri";
    public static final String EXTRA_BIG_TEXT = "android.bigText";
    public static final String EXTRA_COMPACT_ACTIONS = "android.compactActions";
    public static final String EXTRA_CONVERSATION_TITLE = "android.conversationTitle";
    public static final String EXTRA_INFO_TEXT = "android.infoText";
    public static final String EXTRA_LARGE_ICON = "android.largeIcon";
    public static final String EXTRA_LARGE_ICON_BIG = "android.largeIcon.big";
    public static final String EXTRA_MEDIA_SESSION = "android.mediaSession";
    public static final String EXTRA_MESSAGES = "android.messages";
    public static final String EXTRA_PEOPLE = "android.people";
    public static final String EXTRA_PICTURE = "android.picture";
    public static final String EXTRA_PROGRESS = "android.progress";
    public static final String EXTRA_PROGRESS_INDETERMINATE = "android.progressIndeterminate";
    public static final String EXTRA_PROGRESS_MAX = "android.progressMax";
    public static final String EXTRA_REMOTE_INPUT_HISTORY = "android.remoteInputHistory";
    public static final String EXTRA_SELF_DISPLAY_NAME = "android.selfDisplayName";
    public static final String EXTRA_SHOW_CHRONOMETER = "android.showChronometer";
    public static final String EXTRA_SHOW_WHEN = "android.showWhen";
    public static final String EXTRA_SMALL_ICON = "android.icon";
    public static final String EXTRA_SUB_TEXT = "android.subText";
    public static final String EXTRA_SUMMARY_TEXT = "android.summaryText";
    public static final String EXTRA_TEMPLATE = "android.template";
    public static final String EXTRA_TEXT = "android.text";
    public static final String EXTRA_TEXT_LINES = "android.textLines";
    public static final String EXTRA_TITLE = "android.title";
    public static final String EXTRA_TITLE_BIG = "android.title.big";
    public static final int FLAG_AUTO_CANCEL = 16;
    public static final int FLAG_FOREGROUND_SERVICE = 64;
    public static final int FLAG_GROUP_SUMMARY = 512;
    @Deprecated
    public static final int FLAG_HIGH_PRIORITY = 128;
    public static final int FLAG_INSISTENT = 4;
    public static final int FLAG_LOCAL_ONLY = 256;
    public static final int FLAG_NO_CLEAR = 32;
    public static final int FLAG_ONGOING_EVENT = 2;
    public static final int FLAG_ONLY_ALERT_ONCE = 8;
    public static final int FLAG_SHOW_LIGHTS = 1;
    static final NotificationCompatImpl IMPL;
    public static final int PRIORITY_DEFAULT = 0;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_LOW = -1;
    public static final int PRIORITY_MAX = 2;
    public static final int PRIORITY_MIN = -2;
    public static final int STREAM_DEFAULT = -1;
    public static final int VISIBILITY_PRIVATE = 0;
    public static final int VISIBILITY_PUBLIC = 1;
    public static final int VISIBILITY_SECRET = -1;

    /* renamed from: android.support.v4.app.NotificationCompat$Action */
    public static class Action extends android.support.p001v4.app.NotificationCompatBase.Action {
        public static final Factory FACTORY = new Factory() {
            public android.support.p001v4.app.NotificationCompatBase.Action build(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, RemoteInput[] remoteInputArr, boolean z) {
                return new Action(i, charSequence, pendingIntent, bundle, (RemoteInput[]) remoteInputArr, z);
            }

            public Action[] newArray(int i) {
                return new Action[i];
            }
        };

        /* renamed from: a */
        final Bundle f2914a;
        public PendingIntent actionIntent;

        /* renamed from: b */
        private final RemoteInput[] f2915b;

        /* renamed from: c */
        private boolean f2916c;
        public int icon;
        public CharSequence title;

        /* renamed from: android.support.v4.app.NotificationCompat$Action$Builder */
        public static final class Builder {

            /* renamed from: a */
            private final int f2917a;

            /* renamed from: b */
            private final CharSequence f2918b;

            /* renamed from: c */
            private final PendingIntent f2919c;

            /* renamed from: d */
            private boolean f2920d;

            /* renamed from: e */
            private final Bundle f2921e;

            /* renamed from: f */
            private ArrayList<RemoteInput> f2922f;

            public Builder(int i, CharSequence charSequence, PendingIntent pendingIntent) {
                this(i, charSequence, pendingIntent, new Bundle());
            }

            private Builder(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle) {
                this.f2917a = i;
                this.f2918b = Builder.limitCharSequenceLength(charSequence);
                this.f2919c = pendingIntent;
                this.f2921e = bundle;
            }

            public Builder(Action action) {
                this(action.icon, action.title, action.actionIntent, new Bundle(action.f2914a));
            }

            public Builder addExtras(Bundle bundle) {
                if (bundle != null) {
                    this.f2921e.putAll(bundle);
                }
                return this;
            }

            public Builder addRemoteInput(RemoteInput remoteInput) {
                if (this.f2922f == null) {
                    this.f2922f = new ArrayList<>();
                }
                this.f2922f.add(remoteInput);
                return this;
            }

            public Action build() {
                return new Action(this.f2917a, this.f2918b, this.f2919c, this.f2921e, this.f2922f != null ? (RemoteInput[]) this.f2922f.toArray(new RemoteInput[this.f2922f.size()]) : null, this.f2920d);
            }

            public Builder extend(Extender extender) {
                extender.extend(this);
                return this;
            }

            public Bundle getExtras() {
                return this.f2921e;
            }

            public Builder setAllowGeneratedReplies(boolean z) {
                this.f2920d = z;
                return this;
            }
        }

        /* renamed from: android.support.v4.app.NotificationCompat$Action$Extender */
        public interface Extender {
            Builder extend(Builder builder);
        }

        /* renamed from: android.support.v4.app.NotificationCompat$Action$WearableExtender */
        public static final class WearableExtender implements Extender {

            /* renamed from: a */
            private int f2923a = 1;

            /* renamed from: b */
            private CharSequence f2924b;

            /* renamed from: c */
            private CharSequence f2925c;

            /* renamed from: d */
            private CharSequence f2926d;

            public WearableExtender() {
            }

            public WearableExtender(Action action) {
                Bundle bundle = action.getExtras().getBundle("android.wearable.EXTENSIONS");
                if (bundle != null) {
                    this.f2923a = bundle.getInt("flags", 1);
                    this.f2924b = bundle.getCharSequence("inProgressLabel");
                    this.f2925c = bundle.getCharSequence("confirmLabel");
                    this.f2926d = bundle.getCharSequence("cancelLabel");
                }
            }

            /* renamed from: a */
            private void m3650a(int i, boolean z) {
                if (z) {
                    this.f2923a |= i;
                } else {
                    this.f2923a &= i ^ -1;
                }
            }

            public WearableExtender clone() {
                WearableExtender wearableExtender = new WearableExtender();
                wearableExtender.f2923a = this.f2923a;
                wearableExtender.f2924b = this.f2924b;
                wearableExtender.f2925c = this.f2925c;
                wearableExtender.f2926d = this.f2926d;
                return wearableExtender;
            }

            public Builder extend(Builder builder) {
                Bundle bundle = new Bundle();
                if (this.f2923a != 1) {
                    bundle.putInt("flags", this.f2923a);
                }
                if (this.f2924b != null) {
                    bundle.putCharSequence("inProgressLabel", this.f2924b);
                }
                if (this.f2925c != null) {
                    bundle.putCharSequence("confirmLabel", this.f2925c);
                }
                if (this.f2926d != null) {
                    bundle.putCharSequence("cancelLabel", this.f2926d);
                }
                builder.getExtras().putBundle("android.wearable.EXTENSIONS", bundle);
                return builder;
            }

            public CharSequence getCancelLabel() {
                return this.f2926d;
            }

            public CharSequence getConfirmLabel() {
                return this.f2925c;
            }

            public boolean getHintDisplayActionInline() {
                return (this.f2923a & 4) != 0;
            }

            public boolean getHintLaunchesActivity() {
                return (this.f2923a & 2) != 0;
            }

            public CharSequence getInProgressLabel() {
                return this.f2924b;
            }

            public boolean isAvailableOffline() {
                return (this.f2923a & 1) != 0;
            }

            public WearableExtender setAvailableOffline(boolean z) {
                m3650a(1, z);
                return this;
            }

            public WearableExtender setCancelLabel(CharSequence charSequence) {
                this.f2926d = charSequence;
                return this;
            }

            public WearableExtender setConfirmLabel(CharSequence charSequence) {
                this.f2925c = charSequence;
                return this;
            }

            public WearableExtender setHintDisplayActionInline(boolean z) {
                m3650a(4, z);
                return this;
            }

            public WearableExtender setHintLaunchesActivity(boolean z) {
                m3650a(2, z);
                return this;
            }

            public WearableExtender setInProgressLabel(CharSequence charSequence) {
                this.f2924b = charSequence;
                return this;
            }
        }

        public Action(int i, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i, charSequence, pendingIntent, new Bundle(), null, false);
        }

        Action(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, RemoteInput[] remoteInputArr, boolean z) {
            this.f2916c = false;
            this.icon = i;
            this.title = Builder.limitCharSequenceLength(charSequence);
            this.actionIntent = pendingIntent;
            if (bundle == null) {
                bundle = new Bundle();
            }
            this.f2914a = bundle;
            this.f2915b = remoteInputArr;
            this.f2916c = z;
        }

        public PendingIntent getActionIntent() {
            return this.actionIntent;
        }

        public boolean getAllowGeneratedReplies() {
            return this.f2916c;
        }

        public Bundle getExtras() {
            return this.f2914a;
        }

        public int getIcon() {
            return this.icon;
        }

        public RemoteInput[] getRemoteInputs() {
            return this.f2915b;
        }

        public CharSequence getTitle() {
            return this.title;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$BigPictureStyle */
    public static class BigPictureStyle extends Style {

        /* renamed from: a */
        Bitmap f2927a;

        /* renamed from: b */
        Bitmap f2928b;

        /* renamed from: c */
        boolean f2929c;

        public BigPictureStyle() {
        }

        public BigPictureStyle(Builder builder) {
            setBuilder(builder);
        }

        public BigPictureStyle bigLargeIcon(Bitmap bitmap) {
            this.f2928b = bitmap;
            this.f2929c = true;
            return this;
        }

        public BigPictureStyle bigPicture(Bitmap bitmap) {
            this.f2927a = bitmap;
            return this;
        }

        public BigPictureStyle setBigContentTitle(CharSequence charSequence) {
            this.mBigContentTitle = Builder.limitCharSequenceLength(charSequence);
            return this;
        }

        public BigPictureStyle setSummaryText(CharSequence charSequence) {
            this.mSummaryText = Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$BigTextStyle */
    public static class BigTextStyle extends Style {

        /* renamed from: a */
        CharSequence f2930a;

        public BigTextStyle() {
        }

        public BigTextStyle(Builder builder) {
            setBuilder(builder);
        }

        public BigTextStyle bigText(CharSequence charSequence) {
            this.f2930a = Builder.limitCharSequenceLength(charSequence);
            return this;
        }

        public BigTextStyle setBigContentTitle(CharSequence charSequence) {
            this.mBigContentTitle = Builder.limitCharSequenceLength(charSequence);
            return this;
        }

        public BigTextStyle setSummaryText(CharSequence charSequence) {
            this.mSummaryText = Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$Builder */
    public static class Builder {
        private static final int MAX_CHARSEQUENCE_LENGTH = 5120;
        public ArrayList<Action> mActions = new ArrayList<>();
        RemoteViews mBigContentView;
        String mCategory;
        int mColor = 0;
        public CharSequence mContentInfo;
        PendingIntent mContentIntent;
        public CharSequence mContentText;
        public CharSequence mContentTitle;
        RemoteViews mContentView;
        public Context mContext;
        Bundle mExtras;
        PendingIntent mFullScreenIntent;
        String mGroupKey;
        boolean mGroupSummary;
        RemoteViews mHeadsUpContentView;
        public Bitmap mLargeIcon;
        boolean mLocalOnly = false;
        public Notification mNotification = new Notification();
        public int mNumber;
        public ArrayList<String> mPeople;
        int mPriority;
        int mProgress;
        boolean mProgressIndeterminate;
        int mProgressMax;
        Notification mPublicVersion;
        public CharSequence[] mRemoteInputHistory;
        boolean mShowWhen = true;
        String mSortKey;
        public Style mStyle;
        public CharSequence mSubText;
        RemoteViews mTickerView;
        public boolean mUseChronometer;
        int mVisibility = 0;

        public Builder(Context context) {
            this.mContext = context;
            this.mNotification.when = System.currentTimeMillis();
            this.mNotification.audioStreamType = -1;
            this.mPriority = 0;
            this.mPeople = new ArrayList<>();
        }

        protected static CharSequence limitCharSequenceLength(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > MAX_CHARSEQUENCE_LENGTH) ? charSequence.subSequence(0, MAX_CHARSEQUENCE_LENGTH) : charSequence;
        }

        private void setFlag(int i, boolean z) {
            if (z) {
                this.mNotification.flags |= i;
                return;
            }
            this.mNotification.flags &= i ^ -1;
        }

        public Builder addAction(int i, CharSequence charSequence, PendingIntent pendingIntent) {
            this.mActions.add(new Action(i, charSequence, pendingIntent));
            return this;
        }

        public Builder addAction(Action action) {
            this.mActions.add(action);
            return this;
        }

        public Builder addExtras(Bundle bundle) {
            if (bundle != null) {
                if (this.mExtras == null) {
                    this.mExtras = new Bundle(bundle);
                } else {
                    this.mExtras.putAll(bundle);
                }
            }
            return this;
        }

        public Builder addPerson(String str) {
            this.mPeople.add(str);
            return this;
        }

        public Notification build() {
            return NotificationCompat.IMPL.build(this, getExtender());
        }

        public Builder extend(Extender extender) {
            extender.extend(this);
            return this;
        }

        public RemoteViews getBigContentView() {
            return this.mBigContentView;
        }

        public int getColor() {
            return this.mColor;
        }

        public RemoteViews getContentView() {
            return this.mContentView;
        }

        /* access modifiers changed from: protected */
        public BuilderExtender getExtender() {
            return new BuilderExtender();
        }

        public Bundle getExtras() {
            if (this.mExtras == null) {
                this.mExtras = new Bundle();
            }
            return this.mExtras;
        }

        public RemoteViews getHeadsUpContentView() {
            return this.mHeadsUpContentView;
        }

        @Deprecated
        public Notification getNotification() {
            return build();
        }

        public int getPriority() {
            return this.mPriority;
        }

        public long getWhenIfShowing() {
            if (this.mShowWhen) {
                return this.mNotification.when;
            }
            return 0;
        }

        /* access modifiers changed from: protected */
        public CharSequence resolveText() {
            return this.mContentText;
        }

        /* access modifiers changed from: protected */
        public CharSequence resolveTitle() {
            return this.mContentTitle;
        }

        public Builder setAutoCancel(boolean z) {
            setFlag(16, z);
            return this;
        }

        public Builder setCategory(String str) {
            this.mCategory = str;
            return this;
        }

        public Builder setColor(int i) {
            this.mColor = i;
            return this;
        }

        public Builder setContent(RemoteViews remoteViews) {
            this.mNotification.contentView = remoteViews;
            return this;
        }

        public Builder setContentInfo(CharSequence charSequence) {
            this.mContentInfo = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setContentIntent(PendingIntent pendingIntent) {
            this.mContentIntent = pendingIntent;
            return this;
        }

        public Builder setContentText(CharSequence charSequence) {
            this.mContentText = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setContentTitle(CharSequence charSequence) {
            this.mContentTitle = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setCustomBigContentView(RemoteViews remoteViews) {
            this.mBigContentView = remoteViews;
            return this;
        }

        public Builder setCustomContentView(RemoteViews remoteViews) {
            this.mContentView = remoteViews;
            return this;
        }

        public Builder setCustomHeadsUpContentView(RemoteViews remoteViews) {
            this.mHeadsUpContentView = remoteViews;
            return this;
        }

        public Builder setDefaults(int i) {
            this.mNotification.defaults = i;
            if ((i & 4) != 0) {
                this.mNotification.flags |= 1;
            }
            return this;
        }

        public Builder setDeleteIntent(PendingIntent pendingIntent) {
            this.mNotification.deleteIntent = pendingIntent;
            return this;
        }

        public Builder setExtras(Bundle bundle) {
            this.mExtras = bundle;
            return this;
        }

        public Builder setFullScreenIntent(PendingIntent pendingIntent, boolean z) {
            this.mFullScreenIntent = pendingIntent;
            setFlag(NotificationCompat.FLAG_HIGH_PRIORITY, z);
            return this;
        }

        public Builder setGroup(String str) {
            this.mGroupKey = str;
            return this;
        }

        public Builder setGroupSummary(boolean z) {
            this.mGroupSummary = z;
            return this;
        }

        public Builder setLargeIcon(Bitmap bitmap) {
            this.mLargeIcon = bitmap;
            return this;
        }

        public Builder setLights(int i, int i2, int i3) {
            int i4 = 1;
            this.mNotification.ledARGB = i;
            this.mNotification.ledOnMS = i2;
            this.mNotification.ledOffMS = i3;
            boolean z = (this.mNotification.ledOnMS == 0 || this.mNotification.ledOffMS == 0) ? false : true;
            Notification notification = this.mNotification;
            int i5 = this.mNotification.flags & -2;
            if (!z) {
                i4 = 0;
            }
            notification.flags = i4 | i5;
            return this;
        }

        public Builder setLocalOnly(boolean z) {
            this.mLocalOnly = z;
            return this;
        }

        public Builder setNumber(int i) {
            this.mNumber = i;
            return this;
        }

        public Builder setOngoing(boolean z) {
            setFlag(2, z);
            return this;
        }

        public Builder setOnlyAlertOnce(boolean z) {
            setFlag(8, z);
            return this;
        }

        public Builder setPriority(int i) {
            this.mPriority = i;
            return this;
        }

        public Builder setProgress(int i, int i2, boolean z) {
            this.mProgressMax = i;
            this.mProgress = i2;
            this.mProgressIndeterminate = z;
            return this;
        }

        public Builder setPublicVersion(Notification notification) {
            this.mPublicVersion = notification;
            return this;
        }

        public Builder setRemoteInputHistory(CharSequence[] charSequenceArr) {
            this.mRemoteInputHistory = charSequenceArr;
            return this;
        }

        public Builder setShowWhen(boolean z) {
            this.mShowWhen = z;
            return this;
        }

        public Builder setSmallIcon(int i) {
            this.mNotification.icon = i;
            return this;
        }

        public Builder setSmallIcon(int i, int i2) {
            this.mNotification.icon = i;
            this.mNotification.iconLevel = i2;
            return this;
        }

        public Builder setSortKey(String str) {
            this.mSortKey = str;
            return this;
        }

        public Builder setSound(Uri uri) {
            this.mNotification.sound = uri;
            this.mNotification.audioStreamType = -1;
            return this;
        }

        public Builder setSound(Uri uri, int i) {
            this.mNotification.sound = uri;
            this.mNotification.audioStreamType = i;
            return this;
        }

        public Builder setStyle(Style style) {
            if (this.mStyle != style) {
                this.mStyle = style;
                if (this.mStyle != null) {
                    this.mStyle.setBuilder(this);
                }
            }
            return this;
        }

        public Builder setSubText(CharSequence charSequence) {
            this.mSubText = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setTicker(CharSequence charSequence) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setTicker(CharSequence charSequence, RemoteViews remoteViews) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            this.mTickerView = remoteViews;
            return this;
        }

        public Builder setUsesChronometer(boolean z) {
            this.mUseChronometer = z;
            return this;
        }

        public Builder setVibrate(long[] jArr) {
            this.mNotification.vibrate = jArr;
            return this;
        }

        public Builder setVisibility(int i) {
            this.mVisibility = i;
            return this;
        }

        public Builder setWhen(long j) {
            this.mNotification.when = j;
            return this;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$BuilderExtender */
    public static class BuilderExtender {
        protected BuilderExtender() {
        }

        public Notification build(Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            Notification build = notificationBuilderWithBuilderAccessor.build();
            if (builder.mContentView != null) {
                build.contentView = builder.mContentView;
            }
            return build;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$CarExtender */
    public static final class CarExtender implements Extender {

        /* renamed from: a */
        private Bitmap f2931a;

        /* renamed from: b */
        private UnreadConversation f2932b;

        /* renamed from: c */
        private int f2933c = 0;

        /* renamed from: android.support.v4.app.NotificationCompat$CarExtender$UnreadConversation */
        public static class UnreadConversation extends android.support.p001v4.app.NotificationCompatBase.UnreadConversation {

            /* renamed from: a */
            static final android.support.p001v4.app.NotificationCompatBase.UnreadConversation.Factory f2934a = new android.support.p001v4.app.NotificationCompatBase.UnreadConversation.Factory() {
                public UnreadConversation build(String[] strArr, RemoteInput remoteInput, PendingIntent pendingIntent, PendingIntent pendingIntent2, String[] strArr2, long j) {
                    return new UnreadConversation(strArr, (RemoteInput) remoteInput, pendingIntent, pendingIntent2, strArr2, j);
                }
            };

            /* renamed from: b */
            private final String[] f2935b;

            /* renamed from: c */
            private final RemoteInput f2936c;

            /* renamed from: d */
            private final PendingIntent f2937d;

            /* renamed from: e */
            private final PendingIntent f2938e;

            /* renamed from: f */
            private final String[] f2939f;

            /* renamed from: g */
            private final long f2940g;

            /* renamed from: android.support.v4.app.NotificationCompat$CarExtender$UnreadConversation$Builder */
            public static class Builder {

                /* renamed from: a */
                private final List<String> f2941a = new ArrayList();

                /* renamed from: b */
                private final String f2942b;

                /* renamed from: c */
                private RemoteInput f2943c;

                /* renamed from: d */
                private PendingIntent f2944d;

                /* renamed from: e */
                private PendingIntent f2945e;

                /* renamed from: f */
                private long f2946f;

                public Builder(String str) {
                    this.f2942b = str;
                }

                public Builder addMessage(String str) {
                    this.f2941a.add(str);
                    return this;
                }

                public UnreadConversation build() {
                    return new UnreadConversation((String[]) this.f2941a.toArray(new String[this.f2941a.size()]), this.f2943c, this.f2945e, this.f2944d, new String[]{this.f2942b}, this.f2946f);
                }

                public Builder setLatestTimestamp(long j) {
                    this.f2946f = j;
                    return this;
                }

                public Builder setReadPendingIntent(PendingIntent pendingIntent) {
                    this.f2944d = pendingIntent;
                    return this;
                }

                public Builder setReplyAction(PendingIntent pendingIntent, RemoteInput remoteInput) {
                    this.f2943c = remoteInput;
                    this.f2945e = pendingIntent;
                    return this;
                }
            }

            UnreadConversation(String[] strArr, RemoteInput remoteInput, PendingIntent pendingIntent, PendingIntent pendingIntent2, String[] strArr2, long j) {
                this.f2935b = strArr;
                this.f2936c = remoteInput;
                this.f2938e = pendingIntent2;
                this.f2937d = pendingIntent;
                this.f2939f = strArr2;
                this.f2940g = j;
            }

            public long getLatestTimestamp() {
                return this.f2940g;
            }

            public String[] getMessages() {
                return this.f2935b;
            }

            public String getParticipant() {
                if (this.f2939f.length > 0) {
                    return this.f2939f[0];
                }
                return null;
            }

            public String[] getParticipants() {
                return this.f2939f;
            }

            public PendingIntent getReadPendingIntent() {
                return this.f2938e;
            }

            public RemoteInput getRemoteInput() {
                return this.f2936c;
            }

            public PendingIntent getReplyPendingIntent() {
                return this.f2937d;
            }
        }

        public CarExtender() {
        }

        public CarExtender(Notification notification) {
            if (VERSION.SDK_INT >= 21) {
                Bundle bundle = NotificationCompat.getExtras(notification) == null ? null : NotificationCompat.getExtras(notification).getBundle("android.car.EXTENSIONS");
                if (bundle != null) {
                    this.f2931a = (Bitmap) bundle.getParcelable("large_icon");
                    this.f2933c = bundle.getInt("app_color", 0);
                    this.f2932b = (UnreadConversation) NotificationCompat.IMPL.getUnreadConversationFromBundle(bundle.getBundle("car_conversation"), UnreadConversation.f2934a, RemoteInput.FACTORY);
                }
            }
        }

        public Builder extend(Builder builder) {
            if (VERSION.SDK_INT >= 21) {
                Bundle bundle = new Bundle();
                if (this.f2931a != null) {
                    bundle.putParcelable("large_icon", this.f2931a);
                }
                if (this.f2933c != 0) {
                    bundle.putInt("app_color", this.f2933c);
                }
                if (this.f2932b != null) {
                    bundle.putBundle("car_conversation", NotificationCompat.IMPL.getBundleForUnreadConversation(this.f2932b));
                }
                builder.getExtras().putBundle("android.car.EXTENSIONS", bundle);
            }
            return builder;
        }

        public int getColor() {
            return this.f2933c;
        }

        public Bitmap getLargeIcon() {
            return this.f2931a;
        }

        public UnreadConversation getUnreadConversation() {
            return this.f2932b;
        }

        public CarExtender setColor(int i) {
            this.f2933c = i;
            return this;
        }

        public CarExtender setLargeIcon(Bitmap bitmap) {
            this.f2931a = bitmap;
            return this;
        }

        public CarExtender setUnreadConversation(UnreadConversation unreadConversation) {
            this.f2932b = unreadConversation;
            return this;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$Extender */
    public interface Extender {
        Builder extend(Builder builder);
    }

    /* renamed from: android.support.v4.app.NotificationCompat$InboxStyle */
    public static class InboxStyle extends Style {

        /* renamed from: a */
        ArrayList<CharSequence> f2947a = new ArrayList<>();

        public InboxStyle() {
        }

        public InboxStyle(Builder builder) {
            setBuilder(builder);
        }

        public InboxStyle addLine(CharSequence charSequence) {
            this.f2947a.add(Builder.limitCharSequenceLength(charSequence));
            return this;
        }

        public InboxStyle setBigContentTitle(CharSequence charSequence) {
            this.mBigContentTitle = Builder.limitCharSequenceLength(charSequence);
            return this;
        }

        public InboxStyle setSummaryText(CharSequence charSequence) {
            this.mSummaryText = Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$MessagingStyle */
    public static class MessagingStyle extends Style {
        public static final int MAXIMUM_RETAINED_MESSAGES = 25;

        /* renamed from: a */
        CharSequence f2948a;

        /* renamed from: b */
        CharSequence f2949b;

        /* renamed from: c */
        List<Message> f2950c = new ArrayList();

        /* renamed from: android.support.v4.app.NotificationCompat$MessagingStyle$Message */
        public static final class Message {

            /* renamed from: a */
            private final CharSequence f2951a;

            /* renamed from: b */
            private final long f2952b;

            /* renamed from: c */
            private final CharSequence f2953c;

            /* renamed from: d */
            private String f2954d;

            /* renamed from: e */
            private Uri f2955e;

            public Message(CharSequence charSequence, long j, CharSequence charSequence2) {
                this.f2951a = charSequence;
                this.f2952b = j;
                this.f2953c = charSequence2;
            }

            /* renamed from: a */
            private Bundle m3651a() {
                Bundle bundle = new Bundle();
                if (this.f2951a != null) {
                    bundle.putCharSequence("text", this.f2951a);
                }
                bundle.putLong("time", this.f2952b);
                if (this.f2953c != null) {
                    bundle.putCharSequence("sender", this.f2953c);
                }
                if (this.f2954d != null) {
                    bundle.putString("type", this.f2954d);
                }
                if (this.f2955e != null) {
                    bundle.putParcelable("uri", this.f2955e);
                }
                return bundle;
            }

            /* renamed from: a */
            static Message m3652a(Bundle bundle) {
                try {
                    if (!bundle.containsKey("text") || !bundle.containsKey("time")) {
                        return null;
                    }
                    Message message = new Message(bundle.getCharSequence("text"), bundle.getLong("time"), bundle.getCharSequence("sender"));
                    if (!bundle.containsKey("type") || !bundle.containsKey("uri")) {
                        return message;
                    }
                    message.setData(bundle.getString("type"), (Uri) bundle.getParcelable("uri"));
                    return message;
                } catch (ClassCastException e) {
                    return null;
                }
            }

            /* renamed from: a */
            static List<Message> m3653a(Parcelable[] parcelableArr) {
                ArrayList arrayList = new ArrayList(parcelableArr.length);
                for (int i = 0; i < parcelableArr.length; i++) {
                    if (parcelableArr[i] instanceof Bundle) {
                        Message a = m3652a(parcelableArr[i]);
                        if (a != null) {
                            arrayList.add(a);
                        }
                    }
                }
                return arrayList;
            }

            /* renamed from: a */
            static Bundle[] m3654a(List<Message> list) {
                Bundle[] bundleArr = new Bundle[list.size()];
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    bundleArr[i] = ((Message) list.get(i)).m3651a();
                }
                return bundleArr;
            }

            public String getDataMimeType() {
                return this.f2954d;
            }

            public Uri getDataUri() {
                return this.f2955e;
            }

            public CharSequence getSender() {
                return this.f2953c;
            }

            public CharSequence getText() {
                return this.f2951a;
            }

            public long getTimestamp() {
                return this.f2952b;
            }

            public Message setData(String str, Uri uri) {
                this.f2954d = str;
                this.f2955e = uri;
                return this;
            }
        }

        MessagingStyle() {
        }

        public MessagingStyle(CharSequence charSequence) {
            this.f2948a = charSequence;
        }

        public static MessagingStyle extractMessagingStyleFromNotification(Notification notification) {
            Bundle extras = NotificationCompat.IMPL.getExtras(notification);
            if (!extras.containsKey(NotificationCompat.EXTRA_SELF_DISPLAY_NAME)) {
                return null;
            }
            try {
                MessagingStyle messagingStyle = new MessagingStyle();
                messagingStyle.restoreFromCompatExtras(extras);
                return messagingStyle;
            } catch (ClassCastException e) {
                return null;
            }
        }

        public void addCompatExtras(Bundle bundle) {
            super.addCompatExtras(bundle);
            if (this.f2948a != null) {
                bundle.putCharSequence(NotificationCompat.EXTRA_SELF_DISPLAY_NAME, this.f2948a);
            }
            if (this.f2949b != null) {
                bundle.putCharSequence(NotificationCompat.EXTRA_CONVERSATION_TITLE, this.f2949b);
            }
            if (!this.f2950c.isEmpty()) {
                bundle.putParcelableArray(NotificationCompat.EXTRA_MESSAGES, Message.m3654a(this.f2950c));
            }
        }

        public MessagingStyle addMessage(Message message) {
            this.f2950c.add(message);
            if (this.f2950c.size() > 25) {
                this.f2950c.remove(0);
            }
            return this;
        }

        public MessagingStyle addMessage(CharSequence charSequence, long j, CharSequence charSequence2) {
            this.f2950c.add(new Message(charSequence, j, charSequence2));
            if (this.f2950c.size() > 25) {
                this.f2950c.remove(0);
            }
            return this;
        }

        public CharSequence getConversationTitle() {
            return this.f2949b;
        }

        public List<Message> getMessages() {
            return this.f2950c;
        }

        public CharSequence getUserDisplayName() {
            return this.f2948a;
        }

        /* access modifiers changed from: protected */
        public void restoreFromCompatExtras(Bundle bundle) {
            this.f2950c.clear();
            this.f2948a = bundle.getString(NotificationCompat.EXTRA_SELF_DISPLAY_NAME);
            this.f2949b = bundle.getString(NotificationCompat.EXTRA_CONVERSATION_TITLE);
            Parcelable[] parcelableArray = bundle.getParcelableArray(NotificationCompat.EXTRA_MESSAGES);
            if (parcelableArray != null) {
                this.f2950c = Message.m3653a(parcelableArray);
            }
        }

        public MessagingStyle setConversationTitle(CharSequence charSequence) {
            this.f2949b = charSequence;
            return this;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImpl */
    interface NotificationCompatImpl {
        Notification build(Builder builder, BuilderExtender builderExtender);

        Action getAction(Notification notification, int i);

        int getActionCount(Notification notification);

        Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList);

        Bundle getBundleForUnreadConversation(UnreadConversation unreadConversation);

        String getCategory(Notification notification);

        Bundle getExtras(Notification notification);

        String getGroup(Notification notification);

        boolean getLocalOnly(Notification notification);

        ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr);

        String getSortKey(Notification notification);

        UnreadConversation getUnreadConversationFromBundle(Bundle bundle, UnreadConversation.Factory factory, RemoteInput.Factory factory2);

        boolean isGroupSummary(Notification notification);
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplApi20 */
    static class NotificationCompatImplApi20 extends NotificationCompatImplKitKat {
        NotificationCompatImplApi20() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            android.support.p001v4.app.NotificationCompatApi20.Builder builder2 = new android.support.p001v4.app.NotificationCompatApi20.Builder(builder.mContext, builder.mNotification, builder.resolveTitle(), builder.resolveText(), builder.mContentInfo, builder.mTickerView, builder.mNumber, builder.mContentIntent, builder.mFullScreenIntent, builder.mLargeIcon, builder.mProgressMax, builder.mProgress, builder.mProgressIndeterminate, builder.mShowWhen, builder.mUseChronometer, builder.mPriority, builder.mSubText, builder.mLocalOnly, builder.mPeople, builder.mExtras, builder.mGroupKey, builder.mGroupSummary, builder.mSortKey, builder.mContentView, builder.mBigContentView);
            NotificationCompat.addActionsToBuilder(builder2, builder.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder2, builder.mStyle);
            Notification build = builderExtender.build(builder, builder2);
            if (builder.mStyle != null) {
                builder.mStyle.addCompatExtras(getExtras(build));
            }
            return build;
        }

        public Action getAction(Notification notification, int i) {
            return (Action) NotificationCompatApi20.getAction(notification, i, Action.FACTORY, RemoteInput.FACTORY);
        }

        public Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList) {
            return (Action[]) NotificationCompatApi20.getActionsFromParcelableArrayList(arrayList, Action.FACTORY, RemoteInput.FACTORY);
        }

        public String getGroup(Notification notification) {
            return NotificationCompatApi20.getGroup(notification);
        }

        public boolean getLocalOnly(Notification notification) {
            return NotificationCompatApi20.getLocalOnly(notification);
        }

        public ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr) {
            return NotificationCompatApi20.getParcelableArrayListForActions(actionArr);
        }

        public String getSortKey(Notification notification) {
            return NotificationCompatApi20.getSortKey(notification);
        }

        public boolean isGroupSummary(Notification notification) {
            return NotificationCompatApi20.isGroupSummary(notification);
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplApi21 */
    static class NotificationCompatImplApi21 extends NotificationCompatImplApi20 {
        NotificationCompatImplApi21() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            android.support.p001v4.app.NotificationCompatApi21.Builder builder2 = new android.support.p001v4.app.NotificationCompatApi21.Builder(builder.mContext, builder.mNotification, builder.resolveTitle(), builder.resolveText(), builder.mContentInfo, builder.mTickerView, builder.mNumber, builder.mContentIntent, builder.mFullScreenIntent, builder.mLargeIcon, builder.mProgressMax, builder.mProgress, builder.mProgressIndeterminate, builder.mShowWhen, builder.mUseChronometer, builder.mPriority, builder.mSubText, builder.mLocalOnly, builder.mCategory, builder.mPeople, builder.mExtras, builder.mColor, builder.mVisibility, builder.mPublicVersion, builder.mGroupKey, builder.mGroupSummary, builder.mSortKey, builder.mContentView, builder.mBigContentView, builder.mHeadsUpContentView);
            NotificationCompat.addActionsToBuilder(builder2, builder.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder2, builder.mStyle);
            Notification build = builderExtender.build(builder, builder2);
            if (builder.mStyle != null) {
                builder.mStyle.addCompatExtras(getExtras(build));
            }
            return build;
        }

        public Bundle getBundleForUnreadConversation(UnreadConversation unreadConversation) {
            return NotificationCompatApi21.m3659a(unreadConversation);
        }

        public String getCategory(Notification notification) {
            return NotificationCompatApi21.getCategory(notification);
        }

        public UnreadConversation getUnreadConversationFromBundle(Bundle bundle, UnreadConversation.Factory factory, RemoteInput.Factory factory2) {
            return NotificationCompatApi21.m3660a(bundle, factory, factory2);
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplApi24 */
    static class NotificationCompatImplApi24 extends NotificationCompatImplApi21 {
        NotificationCompatImplApi24() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            android.support.p001v4.app.NotificationCompatApi24.Builder builder2 = new android.support.p001v4.app.NotificationCompatApi24.Builder(builder.mContext, builder.mNotification, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mTickerView, builder.mNumber, builder.mContentIntent, builder.mFullScreenIntent, builder.mLargeIcon, builder.mProgressMax, builder.mProgress, builder.mProgressIndeterminate, builder.mShowWhen, builder.mUseChronometer, builder.mPriority, builder.mSubText, builder.mLocalOnly, builder.mCategory, builder.mPeople, builder.mExtras, builder.mColor, builder.mVisibility, builder.mPublicVersion, builder.mGroupKey, builder.mGroupSummary, builder.mSortKey, builder.mRemoteInputHistory, builder.mContentView, builder.mBigContentView, builder.mHeadsUpContentView);
            NotificationCompat.addActionsToBuilder(builder2, builder.mActions);
            NotificationCompat.addStyleToBuilderApi24(builder2, builder.mStyle);
            Notification build = builderExtender.build(builder, builder2);
            if (builder.mStyle != null) {
                builder.mStyle.addCompatExtras(getExtras(build));
            }
            return build;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplBase */
    static class NotificationCompatImplBase implements NotificationCompatImpl {
        NotificationCompatImplBase() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            Notification add = NotificationCompatBase.add(builder.mNotification, builder.mContext, builder.resolveTitle(), builder.resolveText(), builder.mContentIntent, builder.mFullScreenIntent);
            if (builder.mPriority > 0) {
                add.flags |= NotificationCompat.FLAG_HIGH_PRIORITY;
            }
            if (builder.mContentView != null) {
                add.contentView = builder.mContentView;
            }
            return add;
        }

        public Action getAction(Notification notification, int i) {
            return null;
        }

        public int getActionCount(Notification notification) {
            return 0;
        }

        public Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList) {
            return null;
        }

        public Bundle getBundleForUnreadConversation(UnreadConversation unreadConversation) {
            return null;
        }

        public String getCategory(Notification notification) {
            return null;
        }

        public Bundle getExtras(Notification notification) {
            return null;
        }

        public String getGroup(Notification notification) {
            return null;
        }

        public boolean getLocalOnly(Notification notification) {
            return false;
        }

        public ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr) {
            return null;
        }

        public String getSortKey(Notification notification) {
            return null;
        }

        public UnreadConversation getUnreadConversationFromBundle(Bundle bundle, UnreadConversation.Factory factory, RemoteInput.Factory factory2) {
            return null;
        }

        public boolean isGroupSummary(Notification notification) {
            return false;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplHoneycomb */
    static class NotificationCompatImplHoneycomb extends NotificationCompatImplBase {
        NotificationCompatImplHoneycomb() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            Notification a = NotificationCompatHoneycomb.m3662a(builder.mContext, builder.mNotification, builder.resolveTitle(), builder.resolveText(), builder.mContentInfo, builder.mTickerView, builder.mNumber, builder.mContentIntent, builder.mFullScreenIntent, builder.mLargeIcon);
            if (builder.mContentView != null) {
                a.contentView = builder.mContentView;
            }
            return a;
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplIceCreamSandwich */
    static class NotificationCompatImplIceCreamSandwich extends NotificationCompatImplBase {
        NotificationCompatImplIceCreamSandwich() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            return builderExtender.build(builder, new android.support.p001v4.app.NotificationCompatIceCreamSandwich.Builder(builder.mContext, builder.mNotification, builder.resolveTitle(), builder.resolveText(), builder.mContentInfo, builder.mTickerView, builder.mNumber, builder.mContentIntent, builder.mFullScreenIntent, builder.mLargeIcon, builder.mProgressMax, builder.mProgress, builder.mProgressIndeterminate));
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplJellybean */
    static class NotificationCompatImplJellybean extends NotificationCompatImplBase {
        NotificationCompatImplJellybean() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            android.support.p001v4.app.NotificationCompatJellybean.Builder builder2 = new android.support.p001v4.app.NotificationCompatJellybean.Builder(builder.mContext, builder.mNotification, builder.resolveTitle(), builder.resolveText(), builder.mContentInfo, builder.mTickerView, builder.mNumber, builder.mContentIntent, builder.mFullScreenIntent, builder.mLargeIcon, builder.mProgressMax, builder.mProgress, builder.mProgressIndeterminate, builder.mUseChronometer, builder.mPriority, builder.mSubText, builder.mLocalOnly, builder.mExtras, builder.mGroupKey, builder.mGroupSummary, builder.mSortKey, builder.mContentView, builder.mBigContentView);
            NotificationCompat.addActionsToBuilder(builder2, builder.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder2, builder.mStyle);
            Notification build = builderExtender.build(builder, builder2);
            if (builder.mStyle != null) {
                Bundle extras = getExtras(build);
                if (extras != null) {
                    builder.mStyle.addCompatExtras(extras);
                }
            }
            return build;
        }

        public Action getAction(Notification notification, int i) {
            return (Action) NotificationCompatJellybean.getAction(notification, i, Action.FACTORY, RemoteInput.FACTORY);
        }

        public int getActionCount(Notification notification) {
            return NotificationCompatJellybean.getActionCount(notification);
        }

        public Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList) {
            return (Action[]) NotificationCompatJellybean.getActionsFromParcelableArrayList(arrayList, Action.FACTORY, RemoteInput.FACTORY);
        }

        public Bundle getExtras(Notification notification) {
            return NotificationCompatJellybean.getExtras(notification);
        }

        public String getGroup(Notification notification) {
            return NotificationCompatJellybean.getGroup(notification);
        }

        public boolean getLocalOnly(Notification notification) {
            return NotificationCompatJellybean.getLocalOnly(notification);
        }

        public ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr) {
            return NotificationCompatJellybean.getParcelableArrayListForActions(actionArr);
        }

        public String getSortKey(Notification notification) {
            return NotificationCompatJellybean.getSortKey(notification);
        }

        public boolean isGroupSummary(Notification notification) {
            return NotificationCompatJellybean.isGroupSummary(notification);
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$NotificationCompatImplKitKat */
    static class NotificationCompatImplKitKat extends NotificationCompatImplJellybean {
        NotificationCompatImplKitKat() {
        }

        public Notification build(Builder builder, BuilderExtender builderExtender) {
            android.support.p001v4.app.NotificationCompatKitKat.Builder builder2 = new android.support.p001v4.app.NotificationCompatKitKat.Builder(builder.mContext, builder.mNotification, builder.resolveTitle(), builder.resolveText(), builder.mContentInfo, builder.mTickerView, builder.mNumber, builder.mContentIntent, builder.mFullScreenIntent, builder.mLargeIcon, builder.mProgressMax, builder.mProgress, builder.mProgressIndeterminate, builder.mShowWhen, builder.mUseChronometer, builder.mPriority, builder.mSubText, builder.mLocalOnly, builder.mPeople, builder.mExtras, builder.mGroupKey, builder.mGroupSummary, builder.mSortKey, builder.mContentView, builder.mBigContentView);
            NotificationCompat.addActionsToBuilder(builder2, builder.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder2, builder.mStyle);
            return builderExtender.build(builder, builder2);
        }

        public Action getAction(Notification notification, int i) {
            return (Action) NotificationCompatKitKat.getAction(notification, i, Action.FACTORY, RemoteInput.FACTORY);
        }

        public int getActionCount(Notification notification) {
            return NotificationCompatKitKat.getActionCount(notification);
        }

        public Bundle getExtras(Notification notification) {
            return NotificationCompatKitKat.getExtras(notification);
        }

        public String getGroup(Notification notification) {
            return NotificationCompatKitKat.getGroup(notification);
        }

        public boolean getLocalOnly(Notification notification) {
            return NotificationCompatKitKat.getLocalOnly(notification);
        }

        public String getSortKey(Notification notification) {
            return NotificationCompatKitKat.getSortKey(notification);
        }

        public boolean isGroupSummary(Notification notification) {
            return NotificationCompatKitKat.isGroupSummary(notification);
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$Style */
    public static abstract class Style {
        CharSequence mBigContentTitle;
        Builder mBuilder;
        CharSequence mSummaryText;
        boolean mSummaryTextSet = false;

        public void addCompatExtras(Bundle bundle) {
        }

        public Notification build() {
            if (this.mBuilder != null) {
                return this.mBuilder.build();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void restoreFromCompatExtras(Bundle bundle) {
        }

        public void setBuilder(Builder builder) {
            if (this.mBuilder != builder) {
                this.mBuilder = builder;
                if (this.mBuilder != null) {
                    this.mBuilder.setStyle(this);
                }
            }
        }
    }

    /* renamed from: android.support.v4.app.NotificationCompat$WearableExtender */
    public static final class WearableExtender implements Extender {
        public static final int SCREEN_TIMEOUT_LONG = -1;
        public static final int SCREEN_TIMEOUT_SHORT = 0;
        public static final int SIZE_DEFAULT = 0;
        public static final int SIZE_FULL_SCREEN = 5;
        public static final int SIZE_LARGE = 4;
        public static final int SIZE_MEDIUM = 3;
        public static final int SIZE_SMALL = 2;
        public static final int SIZE_XSMALL = 1;
        public static final int UNSET_ACTION_INDEX = -1;

        /* renamed from: a */
        private ArrayList<Action> f2956a = new ArrayList<>();

        /* renamed from: b */
        private int f2957b = 1;

        /* renamed from: c */
        private PendingIntent f2958c;

        /* renamed from: d */
        private ArrayList<Notification> f2959d = new ArrayList<>();

        /* renamed from: e */
        private Bitmap f2960e;

        /* renamed from: f */
        private int f2961f;

        /* renamed from: g */
        private int f2962g = 8388613;

        /* renamed from: h */
        private int f2963h = -1;

        /* renamed from: i */
        private int f2964i = 0;

        /* renamed from: j */
        private int f2965j;

        /* renamed from: k */
        private int f2966k = 80;

        /* renamed from: l */
        private int f2967l;

        /* renamed from: m */
        private String f2968m;

        /* renamed from: n */
        private String f2969n;

        public WearableExtender() {
        }

        public WearableExtender(Notification notification) {
            Bundle extras = NotificationCompat.getExtras(notification);
            Bundle bundle = extras != null ? extras.getBundle("android.wearable.EXTENSIONS") : null;
            if (bundle != null) {
                Action[] actionsFromParcelableArrayList = NotificationCompat.IMPL.getActionsFromParcelableArrayList(bundle.getParcelableArrayList("actions"));
                if (actionsFromParcelableArrayList != null) {
                    Collections.addAll(this.f2956a, actionsFromParcelableArrayList);
                }
                this.f2957b = bundle.getInt("flags", 1);
                this.f2958c = (PendingIntent) bundle.getParcelable("displayIntent");
                Notification[] notificationArrayFromBundle = NotificationCompat.getNotificationArrayFromBundle(bundle, "pages");
                if (notificationArrayFromBundle != null) {
                    Collections.addAll(this.f2959d, notificationArrayFromBundle);
                }
                this.f2960e = (Bitmap) bundle.getParcelable("background");
                this.f2961f = bundle.getInt("contentIcon");
                this.f2962g = bundle.getInt("contentIconGravity", 8388613);
                this.f2963h = bundle.getInt("contentActionIndex", -1);
                this.f2964i = bundle.getInt("customSizePreset", 0);
                this.f2965j = bundle.getInt("customContentHeight");
                this.f2966k = bundle.getInt("gravity", 80);
                this.f2967l = bundle.getInt("hintScreenTimeout");
                this.f2968m = bundle.getString("dismissalId");
                this.f2969n = bundle.getString("bridgeTag");
            }
        }

        /* renamed from: a */
        private void m3655a(int i, boolean z) {
            if (z) {
                this.f2957b |= i;
            } else {
                this.f2957b &= i ^ -1;
            }
        }

        public WearableExtender addAction(Action action) {
            this.f2956a.add(action);
            return this;
        }

        public WearableExtender addActions(List<Action> list) {
            this.f2956a.addAll(list);
            return this;
        }

        public WearableExtender addPage(Notification notification) {
            this.f2959d.add(notification);
            return this;
        }

        public WearableExtender addPages(List<Notification> list) {
            this.f2959d.addAll(list);
            return this;
        }

        public WearableExtender clearActions() {
            this.f2956a.clear();
            return this;
        }

        public WearableExtender clearPages() {
            this.f2959d.clear();
            return this;
        }

        public WearableExtender clone() {
            WearableExtender wearableExtender = new WearableExtender();
            wearableExtender.f2956a = new ArrayList<>(this.f2956a);
            wearableExtender.f2957b = this.f2957b;
            wearableExtender.f2958c = this.f2958c;
            wearableExtender.f2959d = new ArrayList<>(this.f2959d);
            wearableExtender.f2960e = this.f2960e;
            wearableExtender.f2961f = this.f2961f;
            wearableExtender.f2962g = this.f2962g;
            wearableExtender.f2963h = this.f2963h;
            wearableExtender.f2964i = this.f2964i;
            wearableExtender.f2965j = this.f2965j;
            wearableExtender.f2966k = this.f2966k;
            wearableExtender.f2967l = this.f2967l;
            wearableExtender.f2968m = this.f2968m;
            wearableExtender.f2969n = this.f2969n;
            return wearableExtender;
        }

        public Builder extend(Builder builder) {
            Bundle bundle = new Bundle();
            if (!this.f2956a.isEmpty()) {
                bundle.putParcelableArrayList("actions", NotificationCompat.IMPL.getParcelableArrayListForActions((Action[]) this.f2956a.toArray(new Action[this.f2956a.size()])));
            }
            if (this.f2957b != 1) {
                bundle.putInt("flags", this.f2957b);
            }
            if (this.f2958c != null) {
                bundle.putParcelable("displayIntent", this.f2958c);
            }
            if (!this.f2959d.isEmpty()) {
                bundle.putParcelableArray("pages", (Parcelable[]) this.f2959d.toArray(new Notification[this.f2959d.size()]));
            }
            if (this.f2960e != null) {
                bundle.putParcelable("background", this.f2960e);
            }
            if (this.f2961f != 0) {
                bundle.putInt("contentIcon", this.f2961f);
            }
            if (this.f2962g != 8388613) {
                bundle.putInt("contentIconGravity", this.f2962g);
            }
            if (this.f2963h != -1) {
                bundle.putInt("contentActionIndex", this.f2963h);
            }
            if (this.f2964i != 0) {
                bundle.putInt("customSizePreset", this.f2964i);
            }
            if (this.f2965j != 0) {
                bundle.putInt("customContentHeight", this.f2965j);
            }
            if (this.f2966k != 80) {
                bundle.putInt("gravity", this.f2966k);
            }
            if (this.f2967l != 0) {
                bundle.putInt("hintScreenTimeout", this.f2967l);
            }
            if (this.f2968m != null) {
                bundle.putString("dismissalId", this.f2968m);
            }
            if (this.f2969n != null) {
                bundle.putString("bridgeTag", this.f2969n);
            }
            builder.getExtras().putBundle("android.wearable.EXTENSIONS", bundle);
            return builder;
        }

        public List<Action> getActions() {
            return this.f2956a;
        }

        public Bitmap getBackground() {
            return this.f2960e;
        }

        public String getBridgeTag() {
            return this.f2969n;
        }

        public int getContentAction() {
            return this.f2963h;
        }

        public int getContentIcon() {
            return this.f2961f;
        }

        public int getContentIconGravity() {
            return this.f2962g;
        }

        public boolean getContentIntentAvailableOffline() {
            return (this.f2957b & 1) != 0;
        }

        public int getCustomContentHeight() {
            return this.f2965j;
        }

        public int getCustomSizePreset() {
            return this.f2964i;
        }

        public String getDismissalId() {
            return this.f2968m;
        }

        public PendingIntent getDisplayIntent() {
            return this.f2958c;
        }

        public int getGravity() {
            return this.f2966k;
        }

        public boolean getHintAmbientBigPicture() {
            return (this.f2957b & 32) != 0;
        }

        public boolean getHintAvoidBackgroundClipping() {
            return (this.f2957b & 16) != 0;
        }

        public boolean getHintContentIntentLaunchesActivity() {
            return (this.f2957b & 64) != 0;
        }

        public boolean getHintHideIcon() {
            return (this.f2957b & 2) != 0;
        }

        public int getHintScreenTimeout() {
            return this.f2967l;
        }

        public boolean getHintShowBackgroundOnly() {
            return (this.f2957b & 4) != 0;
        }

        public List<Notification> getPages() {
            return this.f2959d;
        }

        public boolean getStartScrollBottom() {
            return (this.f2957b & 8) != 0;
        }

        public WearableExtender setBackground(Bitmap bitmap) {
            this.f2960e = bitmap;
            return this;
        }

        public WearableExtender setBridgeTag(String str) {
            this.f2969n = str;
            return this;
        }

        public WearableExtender setContentAction(int i) {
            this.f2963h = i;
            return this;
        }

        public WearableExtender setContentIcon(int i) {
            this.f2961f = i;
            return this;
        }

        public WearableExtender setContentIconGravity(int i) {
            this.f2962g = i;
            return this;
        }

        public WearableExtender setContentIntentAvailableOffline(boolean z) {
            m3655a(1, z);
            return this;
        }

        public WearableExtender setCustomContentHeight(int i) {
            this.f2965j = i;
            return this;
        }

        public WearableExtender setCustomSizePreset(int i) {
            this.f2964i = i;
            return this;
        }

        public WearableExtender setDismissalId(String str) {
            this.f2968m = str;
            return this;
        }

        public WearableExtender setDisplayIntent(PendingIntent pendingIntent) {
            this.f2958c = pendingIntent;
            return this;
        }

        public WearableExtender setGravity(int i) {
            this.f2966k = i;
            return this;
        }

        public WearableExtender setHintAmbientBigPicture(boolean z) {
            m3655a(32, z);
            return this;
        }

        public WearableExtender setHintAvoidBackgroundClipping(boolean z) {
            m3655a(16, z);
            return this;
        }

        public WearableExtender setHintContentIntentLaunchesActivity(boolean z) {
            m3655a(64, z);
            return this;
        }

        public WearableExtender setHintHideIcon(boolean z) {
            m3655a(2, z);
            return this;
        }

        public WearableExtender setHintScreenTimeout(int i) {
            this.f2967l = i;
            return this;
        }

        public WearableExtender setHintShowBackgroundOnly(boolean z) {
            m3655a(4, z);
            return this;
        }

        public WearableExtender setStartScrollBottom(boolean z) {
            m3655a(8, z);
            return this;
        }
    }

    static {
        if (BuildCompat.m12482a()) {
            IMPL = new NotificationCompatImplApi24();
        } else if (VERSION.SDK_INT >= 21) {
            IMPL = new NotificationCompatImplApi21();
        } else if (VERSION.SDK_INT >= 20) {
            IMPL = new NotificationCompatImplApi20();
        } else if (VERSION.SDK_INT >= 19) {
            IMPL = new NotificationCompatImplKitKat();
        } else if (VERSION.SDK_INT >= 16) {
            IMPL = new NotificationCompatImplJellybean();
        } else if (VERSION.SDK_INT >= 14) {
            IMPL = new NotificationCompatImplIceCreamSandwich();
        } else if (VERSION.SDK_INT >= 11) {
            IMPL = new NotificationCompatImplHoneycomb();
        } else {
            IMPL = new NotificationCompatImplBase();
        }
    }

    static void addActionsToBuilder(NotificationBuilderWithActions notificationBuilderWithActions, ArrayList<Action> arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            notificationBuilderWithActions.addAction((Action) it.next());
        }
    }

    static void addStyleToBuilderApi24(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, Style style) {
        if (style == null) {
            return;
        }
        if (style instanceof MessagingStyle) {
            MessagingStyle messagingStyle = (MessagingStyle) style;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ArrayList arrayList5 = new ArrayList();
            for (Message message : messagingStyle.f2950c) {
                arrayList.add(message.getText());
                arrayList2.add(Long.valueOf(message.getTimestamp()));
                arrayList3.add(message.getSender());
                arrayList4.add(message.getDataMimeType());
                arrayList5.add(message.getDataUri());
            }
            NotificationCompatApi24.addMessagingStyle(notificationBuilderWithBuilderAccessor, messagingStyle.f2948a, messagingStyle.f2949b, arrayList, arrayList2, arrayList3, arrayList4, arrayList5);
            return;
        }
        addStyleToBuilderJellybean(notificationBuilderWithBuilderAccessor, style);
    }

    static void addStyleToBuilderJellybean(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, Style style) {
        if (style == null) {
            return;
        }
        if (style instanceof BigTextStyle) {
            BigTextStyle bigTextStyle = (BigTextStyle) style;
            NotificationCompatJellybean.addBigTextStyle(notificationBuilderWithBuilderAccessor, bigTextStyle.mBigContentTitle, bigTextStyle.mSummaryTextSet, bigTextStyle.mSummaryText, bigTextStyle.f2930a);
        } else if (style instanceof InboxStyle) {
            InboxStyle inboxStyle = (InboxStyle) style;
            NotificationCompatJellybean.addInboxStyle(notificationBuilderWithBuilderAccessor, inboxStyle.mBigContentTitle, inboxStyle.mSummaryTextSet, inboxStyle.mSummaryText, inboxStyle.f2947a);
        } else if (style instanceof BigPictureStyle) {
            BigPictureStyle bigPictureStyle = (BigPictureStyle) style;
            NotificationCompatJellybean.addBigPictureStyle(notificationBuilderWithBuilderAccessor, bigPictureStyle.mBigContentTitle, bigPictureStyle.mSummaryTextSet, bigPictureStyle.mSummaryText, bigPictureStyle.f2927a, bigPictureStyle.f2928b, bigPictureStyle.f2929c);
        }
    }

    public static Action getAction(Notification notification, int i) {
        return IMPL.getAction(notification, i);
    }

    public static int getActionCount(Notification notification) {
        return IMPL.getActionCount(notification);
    }

    public static String getCategory(Notification notification) {
        return IMPL.getCategory(notification);
    }

    public static Bundle getExtras(Notification notification) {
        return IMPL.getExtras(notification);
    }

    public static String getGroup(Notification notification) {
        return IMPL.getGroup(notification);
    }

    public static boolean getLocalOnly(Notification notification) {
        return IMPL.getLocalOnly(notification);
    }

    static Notification[] getNotificationArrayFromBundle(Bundle bundle, String str) {
        Parcelable[] parcelableArray = bundle.getParcelableArray(str);
        if ((parcelableArray instanceof Notification[]) || parcelableArray == null) {
            return (Notification[]) parcelableArray;
        }
        Notification[] notificationArr = new Notification[parcelableArray.length];
        for (int i = 0; i < parcelableArray.length; i++) {
            notificationArr[i] = (Notification) parcelableArray[i];
        }
        bundle.putParcelableArray(str, notificationArr);
        return notificationArr;
    }

    public static String getSortKey(Notification notification) {
        return IMPL.getSortKey(notification);
    }

    public static boolean isGroupSummary(Notification notification) {
        return IMPL.isGroupSummary(notification);
    }
}
