package android.support.p001v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;

/* renamed from: android.support.v4.app.ShareCompat */
public final class ShareCompat {
    public static final String EXTRA_CALLING_ACTIVITY = "android.support.v4.app.EXTRA_CALLING_ACTIVITY";
    public static final String EXTRA_CALLING_PACKAGE = "android.support.v4.app.EXTRA_CALLING_PACKAGE";

    /* renamed from: a */
    static ShareCompatImpl f3038a;

    /* renamed from: android.support.v4.app.ShareCompat$IntentBuilder */
    public static class IntentBuilder {

        /* renamed from: a */
        private Activity f3039a;

        /* renamed from: b */
        private Intent f3040b = new Intent().setAction("android.intent.action.SEND");

        /* renamed from: c */
        private CharSequence f3041c;

        /* renamed from: d */
        private ArrayList<String> f3042d;

        /* renamed from: e */
        private ArrayList<String> f3043e;

        /* renamed from: f */
        private ArrayList<String> f3044f;

        /* renamed from: g */
        private ArrayList<Uri> f3045g;

        private IntentBuilder(Activity activity) {
            this.f3039a = activity;
            this.f3040b.putExtra(ShareCompat.EXTRA_CALLING_PACKAGE, activity.getPackageName());
            this.f3040b.putExtra(ShareCompat.EXTRA_CALLING_ACTIVITY, activity.getComponentName());
            this.f3040b.addFlags(524288);
        }

        /* renamed from: a */
        private void m3689a(String str, ArrayList<String> arrayList) {
            String[] stringArrayExtra = this.f3040b.getStringArrayExtra(str);
            int i = stringArrayExtra != null ? stringArrayExtra.length : 0;
            String[] strArr = new String[(arrayList.size() + i)];
            arrayList.toArray(strArr);
            if (stringArrayExtra != null) {
                System.arraycopy(stringArrayExtra, 0, strArr, arrayList.size(), i);
            }
            this.f3040b.putExtra(str, strArr);
        }

        /* renamed from: a */
        private void m3690a(String str, String[] strArr) {
            Intent intent = getIntent();
            String[] stringArrayExtra = intent.getStringArrayExtra(str);
            int i = stringArrayExtra != null ? stringArrayExtra.length : 0;
            String[] strArr2 = new String[(strArr.length + i)];
            if (stringArrayExtra != null) {
                System.arraycopy(stringArrayExtra, 0, strArr2, 0, i);
            }
            System.arraycopy(strArr, 0, strArr2, i, strArr.length);
            intent.putExtra(str, strArr2);
        }

        public static IntentBuilder from(Activity activity) {
            return new IntentBuilder(activity);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public Activity mo2680a() {
            return this.f3039a;
        }

        public IntentBuilder addEmailBcc(String str) {
            if (this.f3044f == null) {
                this.f3044f = new ArrayList<>();
            }
            this.f3044f.add(str);
            return this;
        }

        public IntentBuilder addEmailBcc(String[] strArr) {
            m3690a("android.intent.extra.BCC", strArr);
            return this;
        }

        public IntentBuilder addEmailCc(String str) {
            if (this.f3043e == null) {
                this.f3043e = new ArrayList<>();
            }
            this.f3043e.add(str);
            return this;
        }

        public IntentBuilder addEmailCc(String[] strArr) {
            m3690a("android.intent.extra.CC", strArr);
            return this;
        }

        public IntentBuilder addEmailTo(String str) {
            if (this.f3042d == null) {
                this.f3042d = new ArrayList<>();
            }
            this.f3042d.add(str);
            return this;
        }

        public IntentBuilder addEmailTo(String[] strArr) {
            m3690a("android.intent.extra.EMAIL", strArr);
            return this;
        }

        public IntentBuilder addStream(Uri uri) {
            Uri uri2 = (Uri) this.f3040b.getParcelableExtra("android.intent.extra.STREAM");
            if (this.f3045g == null && uri2 == null) {
                return setStream(uri);
            }
            if (this.f3045g == null) {
                this.f3045g = new ArrayList<>();
            }
            if (uri2 != null) {
                this.f3040b.removeExtra("android.intent.extra.STREAM");
                this.f3045g.add(uri2);
            }
            this.f3045g.add(uri);
            return this;
        }

        public Intent createChooserIntent() {
            return Intent.createChooser(getIntent(), this.f3041c);
        }

        public Intent getIntent() {
            boolean z = true;
            if (this.f3042d != null) {
                m3689a("android.intent.extra.EMAIL", this.f3042d);
                this.f3042d = null;
            }
            if (this.f3043e != null) {
                m3689a("android.intent.extra.CC", this.f3043e);
                this.f3043e = null;
            }
            if (this.f3044f != null) {
                m3689a("android.intent.extra.BCC", this.f3044f);
                this.f3044f = null;
            }
            if (this.f3045g == null || this.f3045g.size() <= 1) {
                z = false;
            }
            boolean equals = this.f3040b.getAction().equals("android.intent.action.SEND_MULTIPLE");
            if (!z && equals) {
                this.f3040b.setAction("android.intent.action.SEND");
                if (this.f3045g == null || this.f3045g.isEmpty()) {
                    this.f3040b.removeExtra("android.intent.extra.STREAM");
                } else {
                    this.f3040b.putExtra("android.intent.extra.STREAM", (Parcelable) this.f3045g.get(0));
                }
                this.f3045g = null;
            }
            if (z && !equals) {
                this.f3040b.setAction("android.intent.action.SEND_MULTIPLE");
                if (this.f3045g == null || this.f3045g.isEmpty()) {
                    this.f3040b.removeExtra("android.intent.extra.STREAM");
                } else {
                    this.f3040b.putParcelableArrayListExtra("android.intent.extra.STREAM", this.f3045g);
                }
            }
            return this.f3040b;
        }

        public IntentBuilder setChooserTitle(int i) {
            return setChooserTitle(this.f3039a.getText(i));
        }

        public IntentBuilder setChooserTitle(CharSequence charSequence) {
            this.f3041c = charSequence;
            return this;
        }

        public IntentBuilder setEmailBcc(String[] strArr) {
            this.f3040b.putExtra("android.intent.extra.BCC", strArr);
            return this;
        }

        public IntentBuilder setEmailCc(String[] strArr) {
            this.f3040b.putExtra("android.intent.extra.CC", strArr);
            return this;
        }

        public IntentBuilder setEmailTo(String[] strArr) {
            if (this.f3042d != null) {
                this.f3042d = null;
            }
            this.f3040b.putExtra("android.intent.extra.EMAIL", strArr);
            return this;
        }

        public IntentBuilder setHtmlText(String str) {
            this.f3040b.putExtra("android.intent.extra.HTML_TEXT", str);
            if (!this.f3040b.hasExtra("android.intent.extra.TEXT")) {
                setText(Html.fromHtml(str));
            }
            return this;
        }

        public IntentBuilder setStream(Uri uri) {
            if (!this.f3040b.getAction().equals("android.intent.action.SEND")) {
                this.f3040b.setAction("android.intent.action.SEND");
            }
            this.f3045g = null;
            this.f3040b.putExtra("android.intent.extra.STREAM", uri);
            return this;
        }

        public IntentBuilder setSubject(String str) {
            this.f3040b.putExtra("android.intent.extra.SUBJECT", str);
            return this;
        }

        public IntentBuilder setText(CharSequence charSequence) {
            this.f3040b.putExtra("android.intent.extra.TEXT", charSequence);
            return this;
        }

        public IntentBuilder setType(String str) {
            this.f3040b.setType(str);
            return this;
        }

        public void startChooser() {
            this.f3039a.startActivity(createChooserIntent());
        }
    }

    /* renamed from: android.support.v4.app.ShareCompat$IntentReader */
    public static class IntentReader {

        /* renamed from: a */
        private Activity f3046a;

        /* renamed from: b */
        private Intent f3047b;

        /* renamed from: c */
        private String f3048c;

        /* renamed from: d */
        private ComponentName f3049d;

        /* renamed from: e */
        private ArrayList<Uri> f3050e;

        private IntentReader(Activity activity) {
            this.f3046a = activity;
            this.f3047b = activity.getIntent();
            this.f3048c = ShareCompat.getCallingPackage(activity);
            this.f3049d = ShareCompat.getCallingActivity(activity);
        }

        public static IntentReader from(Activity activity) {
            return new IntentReader(activity);
        }

        public ComponentName getCallingActivity() {
            return this.f3049d;
        }

        public Drawable getCallingActivityIcon() {
            Drawable drawable = null;
            if (this.f3049d == null) {
                return drawable;
            }
            try {
                return this.f3046a.getPackageManager().getActivityIcon(this.f3049d);
            } catch (NameNotFoundException e) {
                Log.e("IntentReader", "Could not retrieve icon for calling activity", e);
                return drawable;
            }
        }

        public Drawable getCallingApplicationIcon() {
            Drawable drawable = null;
            if (this.f3048c == null) {
                return drawable;
            }
            try {
                return this.f3046a.getPackageManager().getApplicationIcon(this.f3048c);
            } catch (NameNotFoundException e) {
                Log.e("IntentReader", "Could not retrieve icon for calling application", e);
                return drawable;
            }
        }

        public CharSequence getCallingApplicationLabel() {
            CharSequence charSequence = null;
            if (this.f3048c == null) {
                return charSequence;
            }
            PackageManager packageManager = this.f3046a.getPackageManager();
            try {
                return packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.f3048c, 0));
            } catch (NameNotFoundException e) {
                Log.e("IntentReader", "Could not retrieve label for calling application", e);
                return charSequence;
            }
        }

        public String getCallingPackage() {
            return this.f3048c;
        }

        public String[] getEmailBcc() {
            return this.f3047b.getStringArrayExtra("android.intent.extra.BCC");
        }

        public String[] getEmailCc() {
            return this.f3047b.getStringArrayExtra("android.intent.extra.CC");
        }

        public String[] getEmailTo() {
            return this.f3047b.getStringArrayExtra("android.intent.extra.EMAIL");
        }

        public String getHtmlText() {
            String stringExtra = this.f3047b.getStringExtra("android.intent.extra.HTML_TEXT");
            if (stringExtra != null) {
                return stringExtra;
            }
            CharSequence text = getText();
            return text instanceof Spanned ? Html.toHtml((Spanned) text) : text != null ? ShareCompat.f3038a.escapeHtml(text) : stringExtra;
        }

        public Uri getStream() {
            return (Uri) this.f3047b.getParcelableExtra("android.intent.extra.STREAM");
        }

        public Uri getStream(int i) {
            if (this.f3050e == null && isMultipleShare()) {
                this.f3050e = this.f3047b.getParcelableArrayListExtra("android.intent.extra.STREAM");
            }
            if (this.f3050e != null) {
                return (Uri) this.f3050e.get(i);
            }
            if (i == 0) {
                return (Uri) this.f3047b.getParcelableExtra("android.intent.extra.STREAM");
            }
            throw new IndexOutOfBoundsException("Stream items available: " + getStreamCount() + " index requested: " + i);
        }

        public int getStreamCount() {
            if (this.f3050e == null && isMultipleShare()) {
                this.f3050e = this.f3047b.getParcelableArrayListExtra("android.intent.extra.STREAM");
            }
            return this.f3050e != null ? this.f3050e.size() : this.f3047b.hasExtra("android.intent.extra.STREAM") ? 1 : 0;
        }

        public String getSubject() {
            return this.f3047b.getStringExtra("android.intent.extra.SUBJECT");
        }

        public CharSequence getText() {
            return this.f3047b.getCharSequenceExtra("android.intent.extra.TEXT");
        }

        public String getType() {
            return this.f3047b.getType();
        }

        public boolean isMultipleShare() {
            return "android.intent.action.SEND_MULTIPLE".equals(this.f3047b.getAction());
        }

        public boolean isShareIntent() {
            String action = this.f3047b.getAction();
            return "android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action);
        }

        public boolean isSingleShare() {
            return "android.intent.action.SEND".equals(this.f3047b.getAction());
        }
    }

    /* renamed from: android.support.v4.app.ShareCompat$ShareCompatImpl */
    interface ShareCompatImpl {
        void configureMenuItem(MenuItem menuItem, IntentBuilder intentBuilder);

        String escapeHtml(CharSequence charSequence);
    }

    /* renamed from: android.support.v4.app.ShareCompat$ShareCompatImplBase */
    static class ShareCompatImplBase implements ShareCompatImpl {
        ShareCompatImplBase() {
        }

        /* renamed from: a */
        private static void m3692a(StringBuilder sb, CharSequence charSequence, int i, int i2) {
            int i3 = i;
            while (i3 < i2) {
                char charAt = charSequence.charAt(i3);
                if (charAt == '<') {
                    sb.append("&lt;");
                } else if (charAt == '>') {
                    sb.append("&gt;");
                } else if (charAt == '&') {
                    sb.append("&amp;");
                } else if (charAt > '~' || charAt < ' ') {
                    sb.append("&#" + charAt + ";");
                } else if (charAt == ' ') {
                    while (i3 + 1 < i2 && charSequence.charAt(i3 + 1) == ' ') {
                        sb.append("&nbsp;");
                        i3++;
                    }
                    sb.append(' ');
                } else {
                    sb.append(charAt);
                }
                i3++;
            }
        }

        public void configureMenuItem(MenuItem menuItem, IntentBuilder intentBuilder) {
            menuItem.setIntent(intentBuilder.createChooserIntent());
        }

        public String escapeHtml(CharSequence charSequence) {
            StringBuilder sb = new StringBuilder();
            m3692a(sb, charSequence, 0, charSequence.length());
            return sb.toString();
        }
    }

    /* renamed from: android.support.v4.app.ShareCompat$ShareCompatImplICS */
    static class ShareCompatImplICS extends ShareCompatImplBase {
        ShareCompatImplICS() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo2721a(MenuItem menuItem) {
            return !menuItem.hasSubMenu();
        }

        public void configureMenuItem(MenuItem menuItem, IntentBuilder intentBuilder) {
            ShareCompatICS.configureMenuItem(menuItem, intentBuilder.mo2680a(), intentBuilder.getIntent());
            if (mo2721a(menuItem)) {
                menuItem.setIntent(intentBuilder.createChooserIntent());
            }
        }
    }

    /* renamed from: android.support.v4.app.ShareCompat$ShareCompatImplJB */
    static class ShareCompatImplJB extends ShareCompatImplICS {
        ShareCompatImplJB() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo2721a(MenuItem menuItem) {
            return false;
        }

        public String escapeHtml(CharSequence charSequence) {
            return ShareCompatJB.escapeHtml(charSequence);
        }
    }

    static {
        if (VERSION.SDK_INT >= 16) {
            f3038a = new ShareCompatImplJB();
        } else if (VERSION.SDK_INT >= 14) {
            f3038a = new ShareCompatImplICS();
        } else {
            f3038a = new ShareCompatImplBase();
        }
    }

    private ShareCompat() {
    }

    public static void configureMenuItem(Menu menu, int i, IntentBuilder intentBuilder) {
        MenuItem findItem = menu.findItem(i);
        if (findItem == null) {
            throw new IllegalArgumentException("Could not find menu item with id " + i + " in the supplied menu");
        }
        configureMenuItem(findItem, intentBuilder);
    }

    public static void configureMenuItem(MenuItem menuItem, IntentBuilder intentBuilder) {
        f3038a.configureMenuItem(menuItem, intentBuilder);
    }

    public static ComponentName getCallingActivity(Activity activity) {
        ComponentName callingActivity = activity.getCallingActivity();
        return callingActivity == null ? (ComponentName) activity.getIntent().getParcelableExtra(EXTRA_CALLING_ACTIVITY) : callingActivity;
    }

    public static String getCallingPackage(Activity activity) {
        String callingPackage = activity.getCallingPackage();
        return callingPackage == null ? activity.getIntent().getStringExtra(EXTRA_CALLING_PACKAGE) : callingPackage;
    }
}
