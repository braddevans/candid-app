package android.support.p001v4.widget;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/* renamed from: android.support.v4.widget.SimpleCursorAdapter */
public class SimpleCursorAdapter extends ResourceCursorAdapter {

    /* renamed from: a */
    String[] f3475a;

    /* renamed from: b */
    private int f3476b = -1;

    /* renamed from: c */
    private CursorToStringConverter f3477c;

    /* renamed from: d */
    private ViewBinder f3478d;
    protected int[] mFrom;
    protected int[] mTo;

    /* renamed from: android.support.v4.widget.SimpleCursorAdapter$CursorToStringConverter */
    public interface CursorToStringConverter {
        CharSequence convertToString(Cursor cursor);
    }

    /* renamed from: android.support.v4.widget.SimpleCursorAdapter$ViewBinder */
    public interface ViewBinder {
        boolean setViewValue(View view, Cursor cursor, int i);
    }

    @Deprecated
    public SimpleCursorAdapter(Context context, int i, Cursor cursor, String[] strArr, int[] iArr) {
        super(context, i, cursor);
        this.mTo = iArr;
        this.f3475a = strArr;
        m3955a(cursor, strArr);
    }

    public SimpleCursorAdapter(Context context, int i, Cursor cursor, String[] strArr, int[] iArr, int i2) {
        super(context, i, cursor, i2);
        this.mTo = iArr;
        this.f3475a = strArr;
        m3955a(cursor, strArr);
    }

    /* renamed from: a */
    private void m3955a(Cursor cursor, String[] strArr) {
        if (cursor != null) {
            int length = strArr.length;
            if (this.mFrom == null || this.mFrom.length != length) {
                this.mFrom = new int[length];
            }
            for (int i = 0; i < length; i++) {
                this.mFrom[i] = cursor.getColumnIndexOrThrow(strArr[i]);
            }
            return;
        }
        this.mFrom = null;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ViewBinder viewBinder = this.f3478d;
        int length = this.mTo.length;
        int[] iArr = this.mFrom;
        int[] iArr2 = this.mTo;
        for (int i = 0; i < length; i++) {
            View findViewById = view.findViewById(iArr2[i]);
            if (findViewById != null) {
                boolean z = false;
                if (viewBinder != null) {
                    z = viewBinder.setViewValue(findViewById, cursor, iArr[i]);
                }
                if (!z) {
                    String string = cursor.getString(iArr[i]);
                    if (string == null) {
                        string = "";
                    }
                    if (findViewById instanceof TextView) {
                        setViewText((TextView) findViewById, string);
                    } else if (findViewById instanceof ImageView) {
                        setViewImage((ImageView) findViewById, string);
                    } else {
                        throw new IllegalStateException(findViewById.getClass().getName() + " is not a " + " view that can be bounds by this SimpleCursorAdapter");
                    }
                } else {
                    continue;
                }
            }
        }
    }

    public void changeCursorAndColumns(Cursor cursor, String[] strArr, int[] iArr) {
        this.f3475a = strArr;
        this.mTo = iArr;
        m3955a(cursor, this.f3475a);
        super.changeCursor(cursor);
    }

    public CharSequence convertToString(Cursor cursor) {
        return this.f3477c != null ? this.f3477c.convertToString(cursor) : this.f3476b > -1 ? cursor.getString(this.f3476b) : super.convertToString(cursor);
    }

    public CursorToStringConverter getCursorToStringConverter() {
        return this.f3477c;
    }

    public int getStringConversionColumn() {
        return this.f3476b;
    }

    public ViewBinder getViewBinder() {
        return this.f3478d;
    }

    public void setCursorToStringConverter(CursorToStringConverter cursorToStringConverter) {
        this.f3477c = cursorToStringConverter;
    }

    public void setStringConversionColumn(int i) {
        this.f3476b = i;
    }

    public void setViewBinder(ViewBinder viewBinder) {
        this.f3478d = viewBinder;
    }

    public void setViewImage(ImageView imageView, String str) {
        try {
            imageView.setImageResource(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            imageView.setImageURI(Uri.parse(str));
        }
    }

    public void setViewText(TextView textView, String str) {
        textView.setText(str);
    }

    public Cursor swapCursor(Cursor cursor) {
        m3955a(cursor, this.f3475a);
        return super.swapCursor(cursor);
    }
}
