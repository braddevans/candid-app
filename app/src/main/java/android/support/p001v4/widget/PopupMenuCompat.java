package android.support.p001v4.widget;

import android.os.Build.VERSION;
import android.view.View.OnTouchListener;

/* renamed from: android.support.v4.widget.PopupMenuCompat */
public final class PopupMenuCompat {

    /* renamed from: a */
    static final PopupMenuImpl f3459a;

    /* renamed from: android.support.v4.widget.PopupMenuCompat$BasePopupMenuImpl */
    static class BasePopupMenuImpl implements PopupMenuImpl {
        BasePopupMenuImpl() {
        }

        public OnTouchListener getDragToOpenListener(Object obj) {
            return null;
        }
    }

    /* renamed from: android.support.v4.widget.PopupMenuCompat$KitKatPopupMenuImpl */
    static class KitKatPopupMenuImpl extends BasePopupMenuImpl {
        KitKatPopupMenuImpl() {
        }

        public OnTouchListener getDragToOpenListener(Object obj) {
            return PopupMenuCompatKitKat.getDragToOpenListener(obj);
        }
    }

    /* renamed from: android.support.v4.widget.PopupMenuCompat$PopupMenuImpl */
    interface PopupMenuImpl {
        OnTouchListener getDragToOpenListener(Object obj);
    }

    static {
        if (VERSION.SDK_INT >= 19) {
            f3459a = new KitKatPopupMenuImpl();
        } else {
            f3459a = new BasePopupMenuImpl();
        }
    }

    private PopupMenuCompat() {
    }

    public static OnTouchListener getDragToOpenListener(Object obj) {
        return f3459a.getDragToOpenListener(obj);
    }
}
