package android.support.p001v4.widget;

import android.widget.ListView;

/* renamed from: android.support.v4.widget.ListViewAutoScrollHelper */
public class ListViewAutoScrollHelper extends AutoScrollHelper {

    /* renamed from: f */
    private final ListView f3388f;

    public ListViewAutoScrollHelper(ListView listView) {
        super(listView);
        this.f3388f = listView;
    }

    public boolean canTargetScrollHorizontally(int i) {
        return false;
    }

    public boolean canTargetScrollVertically(int i) {
        ListView listView = this.f3388f;
        int count = listView.getCount();
        if (count == 0) {
            return false;
        }
        int childCount = listView.getChildCount();
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int i2 = firstVisiblePosition + childCount;
        if (i > 0) {
            if (i2 >= count && listView.getChildAt(childCount - 1).getBottom() <= listView.getHeight()) {
                return false;
            }
        } else if (i >= 0) {
            return false;
        } else {
            if (firstVisiblePosition <= 0 && listView.getChildAt(0).getTop() >= 0) {
                return false;
            }
        }
        return true;
    }

    public void scrollTargetBy(int i, int i2) {
        ListViewCompat.scrollListBy(this.f3388f, i2);
    }
}
