package android.support.p001v4.widget;

import android.widget.TextView;

/* renamed from: android.support.v4.widget.TextViewCompatJb */
class TextViewCompatJb {
    TextViewCompatJb() {
    }

    /* renamed from: a */
    static int m3999a(TextView textView) {
        return textView.getMaxLines();
    }

    /* renamed from: b */
    static int m4000b(TextView textView) {
        return textView.getMinLines();
    }
}
