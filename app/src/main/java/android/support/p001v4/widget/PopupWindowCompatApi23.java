package android.support.p001v4.widget;

import android.widget.PopupWindow;

/* renamed from: android.support.v4.widget.PopupWindowCompatApi23 */
class PopupWindowCompatApi23 {
    PopupWindowCompatApi23() {
    }

    /* renamed from: a */
    static void m3951a(PopupWindow popupWindow, int i) {
        popupWindow.setWindowLayoutType(i);
    }

    /* renamed from: a */
    static void m3952a(PopupWindow popupWindow, boolean z) {
        popupWindow.setOverlapAnchor(z);
    }

    /* renamed from: a */
    static boolean m3953a(PopupWindow popupWindow) {
        return popupWindow.getOverlapAnchor();
    }

    /* renamed from: b */
    static int m3954b(PopupWindow popupWindow) {
        return popupWindow.getWindowLayoutType();
    }
}
