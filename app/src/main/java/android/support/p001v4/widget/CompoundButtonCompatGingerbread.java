package android.support.p001v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.CompoundButton;
import java.lang.reflect.Field;

/* renamed from: android.support.v4.widget.CompoundButtonCompatGingerbread */
class CompoundButtonCompatGingerbread {

    /* renamed from: a */
    private static Field f3298a;

    /* renamed from: b */
    private static boolean f3299b;

    CompoundButtonCompatGingerbread() {
    }

    /* renamed from: a */
    static ColorStateList m3843a(CompoundButton compoundButton) {
        if (compoundButton instanceof TintableCompoundButton) {
            return ((TintableCompoundButton) compoundButton).getSupportButtonTintList();
        }
        return null;
    }

    /* renamed from: a */
    static void m3844a(CompoundButton compoundButton, ColorStateList colorStateList) {
        if (compoundButton instanceof TintableCompoundButton) {
            ((TintableCompoundButton) compoundButton).setSupportButtonTintList(colorStateList);
        }
    }

    /* renamed from: a */
    static void m3845a(CompoundButton compoundButton, Mode mode) {
        if (compoundButton instanceof TintableCompoundButton) {
            ((TintableCompoundButton) compoundButton).setSupportButtonTintMode(mode);
        }
    }

    /* renamed from: b */
    static Mode m3846b(CompoundButton compoundButton) {
        if (compoundButton instanceof TintableCompoundButton) {
            return ((TintableCompoundButton) compoundButton).getSupportButtonTintMode();
        }
        return null;
    }

    /* renamed from: c */
    static Drawable m3847c(CompoundButton compoundButton) {
        if (!f3299b) {
            try {
                f3298a = CompoundButton.class.getDeclaredField("mButtonDrawable");
                f3298a.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.i("CompoundButtonCompatGingerbread", "Failed to retrieve mButtonDrawable field", e);
            }
            f3299b = true;
        }
        if (f3298a != null) {
            try {
                return (Drawable) f3298a.get(compoundButton);
            } catch (IllegalAccessException e2) {
                Log.i("CompoundButtonCompatGingerbread", "Failed to get button drawable via reflection", e2);
                f3298a = null;
            }
        }
        return null;
    }
}
