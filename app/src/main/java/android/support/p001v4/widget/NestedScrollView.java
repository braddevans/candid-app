package android.support.p001v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p001v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import java.util.ArrayList;

/* renamed from: android.support.v4.widget.NestedScrollView */
public class NestedScrollView extends FrameLayout implements NestedScrollingChild, NestedScrollingParent, ScrollingView {

    /* renamed from: v */
    private static final AccessibilityDelegate f3432v = new AccessibilityDelegate();

    /* renamed from: w */
    private static final int[] f3433w = {16843130};

    /* renamed from: A */
    private OnScrollChangeListener f3434A;

    /* renamed from: a */
    private long f3435a;

    /* renamed from: b */
    private final Rect f3436b;

    /* renamed from: c */
    private ScrollerCompat f3437c;

    /* renamed from: d */
    private EdgeEffectCompat f3438d;

    /* renamed from: e */
    private EdgeEffectCompat f3439e;

    /* renamed from: f */
    private int f3440f;

    /* renamed from: g */
    private boolean f3441g;

    /* renamed from: h */
    private boolean f3442h;

    /* renamed from: i */
    private View f3443i;

    /* renamed from: j */
    private boolean f3444j;

    /* renamed from: k */
    private VelocityTracker f3445k;

    /* renamed from: l */
    private boolean f3446l;

    /* renamed from: m */
    private boolean f3447m;

    /* renamed from: n */
    private int f3448n;

    /* renamed from: o */
    private int f3449o;

    /* renamed from: p */
    private int f3450p;

    /* renamed from: q */
    private int f3451q;

    /* renamed from: r */
    private final int[] f3452r;

    /* renamed from: s */
    private final int[] f3453s;

    /* renamed from: t */
    private int f3454t;

    /* renamed from: u */
    private SavedState f3455u;

    /* renamed from: x */
    private final NestedScrollingParentHelper f3456x;

    /* renamed from: y */
    private final NestedScrollingChildHelper f3457y;

    /* renamed from: z */
    private float f3458z;

    /* renamed from: android.support.v4.widget.NestedScrollView$AccessibilityDelegate */
    static class AccessibilityDelegate extends AccessibilityDelegateCompat {
        AccessibilityDelegate() {
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            accessibilityEvent.setClassName(ScrollView.class.getName());
            AccessibilityRecordCompat a = AccessibilityEventCompat.m13397a(accessibilityEvent);
            a.mo13732d(nestedScrollView.getScrollRange() > 0);
            a.mo13731d(nestedScrollView.getScrollX());
            a.mo13733e(nestedScrollView.getScrollY());
            a.mo13735f(nestedScrollView.getScrollX());
            a.mo13736g(nestedScrollView.getScrollRange());
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat ggVar) {
            super.onInitializeAccessibilityNodeInfo(view, ggVar);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            ggVar.mo13595b((CharSequence) ScrollView.class.getName());
            if (nestedScrollView.isEnabled()) {
                int scrollRange = nestedScrollView.getScrollRange();
                if (scrollRange > 0) {
                    ggVar.mo13621i(true);
                    if (nestedScrollView.getScrollY() > 0) {
                        ggVar.mo13585a((int) FragmentTransaction.TRANSIT_EXIT_MASK);
                    }
                    if (nestedScrollView.getScrollY() < scrollRange) {
                        ggVar.mo13585a(4096);
                    }
                }
            }
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            if (super.performAccessibilityAction(view, i, bundle)) {
                return true;
            }
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            if (!nestedScrollView.isEnabled()) {
                return false;
            }
            switch (i) {
                case 4096:
                    int min = Math.min(nestedScrollView.getScrollY() + ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), nestedScrollView.getScrollRange());
                    if (min == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.smoothScrollTo(0, min);
                    return true;
                case FragmentTransaction.TRANSIT_EXIT_MASK /*8192*/:
                    int max = Math.max(nestedScrollView.getScrollY() - ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), 0);
                    if (max == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.smoothScrollTo(0, max);
                    return true;
                default:
                    return false;
            }
        }
    }

    /* renamed from: android.support.v4.widget.NestedScrollView$OnScrollChangeListener */
    public interface OnScrollChangeListener {
        void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4);
    }

    /* renamed from: android.support.v4.widget.NestedScrollView$SavedState */
    static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        public int scrollPosition;

        SavedState(Parcel parcel) {
            super(parcel);
            this.scrollPosition = parcel.readInt();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "HorizontalScrollView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " scrollPosition=" + this.scrollPosition + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.scrollPosition);
        }
    }

    public NestedScrollView(Context context) {
        this(context, null);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3436b = new Rect();
        this.f3441g = true;
        this.f3442h = false;
        this.f3443i = null;
        this.f3444j = false;
        this.f3447m = true;
        this.f3451q = -1;
        this.f3452r = new int[2];
        this.f3453s = new int[2];
        m3930a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f3433w, i, 0);
        setFillViewport(obtainStyledAttributes.getBoolean(0, false));
        obtainStyledAttributes.recycle();
        this.f3456x = new NestedScrollingParentHelper(this);
        this.f3457y = new NestedScrollingChildHelper(this);
        setNestedScrollingEnabled(true);
        ViewCompat.m12894a((View) this, (AccessibilityDelegateCompat) f3432v);
    }

    /* renamed from: a */
    private View m3929a(boolean z, int i, int i2) {
        ArrayList focusables = getFocusables(2);
        View view = null;
        boolean z2 = false;
        int size = focusables.size();
        for (int i3 = 0; i3 < size; i3++) {
            View view2 = (View) focusables.get(i3);
            int top = view2.getTop();
            int bottom = view2.getBottom();
            if (i < bottom && top < i2) {
                boolean z3 = i < top && bottom < i2;
                if (view == null) {
                    view = view2;
                    z2 = z3;
                } else {
                    boolean z4 = (z && top < view.getTop()) || (!z && bottom > view.getBottom());
                    if (z2) {
                        if (z3 && z4) {
                            view = view2;
                        }
                    } else if (z3) {
                        view = view2;
                        z2 = true;
                    } else if (z4) {
                        view = view2;
                    }
                }
            }
        }
        return view;
    }

    /* renamed from: a */
    private void m3930a() {
        this.f3437c = ScrollerCompat.create(getContext(), null);
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.f3448n = viewConfiguration.getScaledTouchSlop();
        this.f3449o = viewConfiguration.getScaledMinimumFlingVelocity();
        this.f3450p = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    /* renamed from: a */
    private void m3931a(int i) {
        if (i == 0) {
            return;
        }
        if (this.f3447m) {
            smoothScrollBy(0, i);
        } else {
            scrollBy(0, i);
        }
    }

    /* renamed from: a */
    private void m3932a(MotionEvent motionEvent) {
        int action = (motionEvent.getAction() & 65280) >> 8;
        if (motionEvent.getPointerId(action) == this.f3451q) {
            int i = action == 0 ? 1 : 0;
            this.f3440f = (int) motionEvent.getY(i);
            this.f3451q = motionEvent.getPointerId(i);
            if (this.f3445k != null) {
                this.f3445k.clear();
            }
        }
    }

    /* renamed from: a */
    private boolean m3933a(int i, int i2) {
        if (getChildCount() <= 0) {
            return false;
        }
        int scrollY = getScrollY();
        View childAt = getChildAt(0);
        return i2 >= childAt.getTop() - scrollY && i2 < childAt.getBottom() - scrollY && i >= childAt.getLeft() && i < childAt.getRight();
    }

    /* renamed from: a */
    private boolean m3934a(int i, int i2, int i3) {
        boolean z = true;
        int height = getHeight();
        int scrollY = getScrollY();
        int i4 = scrollY + height;
        boolean z2 = i == 33;
        View a = m3929a(z2, i2, i3);
        if (a == null) {
            a = this;
        }
        if (i2 < scrollY || i3 > i4) {
            m3931a(z2 ? i2 - scrollY : i3 - i4);
        } else {
            z = false;
        }
        if (a != findFocus()) {
            a.requestFocus(i);
        }
        return z;
    }

    /* renamed from: a */
    private boolean m3935a(Rect rect, boolean z) {
        int computeScrollDeltaToGetChildRectOnScreen = computeScrollDeltaToGetChildRectOnScreen(rect);
        boolean z2 = computeScrollDeltaToGetChildRectOnScreen != 0;
        if (z2) {
            if (z) {
                scrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
            } else {
                smoothScrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
            }
        }
        return z2;
    }

    /* renamed from: a */
    private boolean m3936a(View view) {
        return !m3937a(view, 0, getHeight());
    }

    /* renamed from: a */
    private boolean m3937a(View view, int i, int i2) {
        view.getDrawingRect(this.f3436b);
        offsetDescendantRectToMyCoords(view, this.f3436b);
        return this.f3436b.bottom + i >= getScrollY() && this.f3436b.top - i <= getScrollY() + i2;
    }

    /* renamed from: a */
    private static boolean m3938a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        ViewParent parent = view.getParent();
        return (parent instanceof ViewGroup) && m3938a((View) parent, view2);
    }

    /* renamed from: b */
    private static int m3939b(int i, int i2, int i3) {
        if (i2 >= i3 || i < 0) {
            return 0;
        }
        return i2 + i > i3 ? i3 - i2 : i;
    }

    /* renamed from: b */
    private void m3940b(int i) {
        int scrollY = getScrollY();
        boolean z = (scrollY > 0 || i > 0) && (scrollY < getScrollRange() || i < 0);
        if (!dispatchNestedPreFling(0.0f, (float) i)) {
            dispatchNestedFling(0.0f, (float) i, z);
            if (z) {
                fling(i);
            }
        }
    }

    /* renamed from: b */
    private void m3941b(View view) {
        view.getDrawingRect(this.f3436b);
        offsetDescendantRectToMyCoords(view, this.f3436b);
        int computeScrollDeltaToGetChildRectOnScreen = computeScrollDeltaToGetChildRectOnScreen(this.f3436b);
        if (computeScrollDeltaToGetChildRectOnScreen != 0) {
            scrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
        }
    }

    /* renamed from: b */
    private boolean m3942b() {
        View childAt = getChildAt(0);
        if (childAt == null) {
            return false;
        }
        return getHeight() < (getPaddingTop() + childAt.getHeight()) + getPaddingBottom();
    }

    /* renamed from: c */
    private void m3943c() {
        if (this.f3445k == null) {
            this.f3445k = VelocityTracker.obtain();
        } else {
            this.f3445k.clear();
        }
    }

    /* renamed from: d */
    private void m3944d() {
        if (this.f3445k == null) {
            this.f3445k = VelocityTracker.obtain();
        }
    }

    /* renamed from: e */
    private void m3945e() {
        if (this.f3445k != null) {
            this.f3445k.recycle();
            this.f3445k = null;
        }
    }

    /* renamed from: f */
    private void m3946f() {
        this.f3444j = false;
        m3945e();
        stopNestedScroll();
        if (this.f3438d != null) {
            this.f3438d.onRelease();
            this.f3439e.onRelease();
        }
    }

    /* renamed from: g */
    private void m3947g() {
        if (getOverScrollMode() == 2) {
            this.f3438d = null;
            this.f3439e = null;
        } else if (this.f3438d == null) {
            Context context = getContext();
            this.f3438d = new EdgeEffectCompat(context);
            this.f3439e = new EdgeEffectCompat(context);
        }
    }

    private float getVerticalScrollFactorCompat() {
        if (this.f3458z == 0.0f) {
            TypedValue typedValue = new TypedValue();
            Context context = getContext();
            if (!context.getTheme().resolveAttribute(16842829, typedValue, true)) {
                throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
            }
            this.f3458z = typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return this.f3458z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo3226a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z) {
        int overScrollMode = getOverScrollMode();
        boolean z2 = computeHorizontalScrollRange() > computeHorizontalScrollExtent();
        boolean z3 = computeVerticalScrollRange() > computeVerticalScrollExtent();
        boolean z4 = overScrollMode == 0 || (overScrollMode == 1 && z2);
        boolean z5 = overScrollMode == 0 || (overScrollMode == 1 && z3);
        int i9 = i3 + i;
        if (!z4) {
            i7 = 0;
        }
        int i10 = i4 + i2;
        if (!z5) {
            i8 = 0;
        }
        int i11 = -i7;
        int i12 = i7 + i5;
        int i13 = -i8;
        int i14 = i8 + i6;
        boolean z6 = false;
        if (i9 > i12) {
            i9 = i12;
            z6 = true;
        } else if (i9 < i11) {
            i9 = i11;
            z6 = true;
        }
        boolean z7 = false;
        if (i10 > i14) {
            i10 = i14;
            z7 = true;
        } else if (i10 < i13) {
            i10 = i13;
            z7 = true;
        }
        if (z7) {
            this.f3437c.springBack(i9, i10, 0, 0, 0, getScrollRange());
        }
        onOverScrolled(i9, i10, z6, z7);
        return z6 || z7;
    }

    public void addView(View view) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view);
    }

    public void addView(View view, int i) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, i);
    }

    public void addView(View view, int i, LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, i, layoutParams);
    }

    public void addView(View view, LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, layoutParams);
    }

    public boolean arrowScroll(int i) {
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i);
        int maxScrollAmount = getMaxScrollAmount();
        if (findNextFocus == null || !m3937a(findNextFocus, maxScrollAmount, getHeight())) {
            int i2 = maxScrollAmount;
            if (i == 33 && getScrollY() < i2) {
                i2 = getScrollY();
            } else if (i == 130 && getChildCount() > 0) {
                int bottom = getChildAt(0).getBottom();
                int scrollY = (getScrollY() + getHeight()) - getPaddingBottom();
                if (bottom - scrollY < maxScrollAmount) {
                    i2 = bottom - scrollY;
                }
            }
            if (i2 == 0) {
                return false;
            }
            m3931a(i == 130 ? i2 : -i2);
        } else {
            findNextFocus.getDrawingRect(this.f3436b);
            offsetDescendantRectToMyCoords(findNextFocus, this.f3436b);
            m3931a(computeScrollDeltaToGetChildRectOnScreen(this.f3436b));
            findNextFocus.requestFocus(i);
        }
        if (findFocus != null && findFocus.isFocused() && m3936a(findFocus)) {
            int descendantFocusability = getDescendantFocusability();
            setDescendantFocusability(131072);
            requestFocus();
            setDescendantFocusability(descendantFocusability);
        }
        return true;
    }

    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }

    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }

    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }

    public void computeScroll() {
        boolean z = true;
        if (this.f3437c.computeScrollOffset()) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.f3437c.getCurrX();
            int currY = this.f3437c.getCurrY();
            if (scrollX != currX || scrollY != currY) {
                int scrollRange = getScrollRange();
                int overScrollMode = getOverScrollMode();
                if (overScrollMode != 0 && (overScrollMode != 1 || scrollRange <= 0)) {
                    z = false;
                }
                mo3226a(currX - scrollX, currY - scrollY, scrollX, scrollY, 0, scrollRange, 0, 0, false);
                if (z) {
                    m3947g();
                    if (currY <= 0 && scrollY > 0) {
                        this.f3438d.onAbsorb((int) this.f3437c.getCurrVelocity());
                    } else if (currY >= scrollRange && scrollY < scrollRange) {
                        this.f3439e.onAbsorb((int) this.f3437c.getCurrVelocity());
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public int computeScrollDeltaToGetChildRectOnScreen(Rect rect) {
        if (getChildCount() == 0) {
            return 0;
        }
        int height = getHeight();
        int scrollY = getScrollY();
        int i = scrollY + height;
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        if (rect.top > 0) {
            scrollY += verticalFadingEdgeLength;
        }
        if (rect.bottom < getChildAt(0).getHeight()) {
            i -= verticalFadingEdgeLength;
        }
        if (rect.bottom > i && rect.top > scrollY) {
            return Math.min(rect.height() > height ? 0 + (rect.top - scrollY) : 0 + (rect.bottom - i), getChildAt(0).getBottom() - i);
        } else if (rect.top >= scrollY || rect.bottom >= i) {
            return 0;
        } else {
            return Math.max(rect.height() > height ? 0 - (i - rect.bottom) : 0 - (scrollY - rect.top), -getScrollY());
        }
    }

    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }

    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    public int computeVerticalScrollRange() {
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        if (getChildCount() == 0) {
            return height;
        }
        int bottom = getChildAt(0).getBottom();
        int scrollY = getScrollY();
        int max = Math.max(0, bottom - height);
        if (scrollY < 0) {
            bottom -= scrollY;
        } else if (scrollY > max) {
            bottom += scrollY - max;
        }
        return bottom;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || executeKeyEvent(keyEvent);
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.f3457y.mo13424a(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.f3457y.mo13423a(f, f2);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.f3457y.mo13427a(i, i2, iArr, iArr2);
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.f3457y.mo13426a(i, i2, i3, i4, iArr);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f3438d != null) {
            int scrollY = getScrollY();
            if (!this.f3438d.isFinished()) {
                int save = canvas.save();
                int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
                canvas.translate((float) getPaddingLeft(), (float) Math.min(0, scrollY));
                this.f3438d.setSize(width, getHeight());
                if (this.f3438d.draw(canvas)) {
                    ViewCompat.m12913d(this);
                }
                canvas.restoreToCount(save);
            }
            if (!this.f3439e.isFinished()) {
                int save2 = canvas.save();
                int width2 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                int height = getHeight();
                canvas.translate((float) ((-width2) + getPaddingLeft()), (float) (Math.max(getScrollRange(), scrollY) + height));
                canvas.rotate(180.0f, (float) width2, 0.0f);
                this.f3439e.setSize(width2, height);
                if (this.f3439e.draw(canvas)) {
                    ViewCompat.m12913d(this);
                }
                canvas.restoreToCount(save2);
            }
        }
    }

    public boolean executeKeyEvent(KeyEvent keyEvent) {
        this.f3436b.setEmpty();
        if (m3942b()) {
            boolean z = false;
            if (keyEvent.getAction() == 0) {
                switch (keyEvent.getKeyCode()) {
                    case 19:
                        if (keyEvent.isAltPressed()) {
                            z = fullScroll(33);
                            break;
                        } else {
                            z = arrowScroll(33);
                            break;
                        }
                    case 20:
                        if (keyEvent.isAltPressed()) {
                            z = fullScroll(130);
                            break;
                        } else {
                            z = arrowScroll(130);
                            break;
                        }
                    case 62:
                        pageScroll(keyEvent.isShiftPressed() ? 33 : 130);
                        break;
                }
            }
            return z;
        } else if (!isFocused() || keyEvent.getKeyCode() == 4) {
            return false;
        } else {
            View findFocus = findFocus();
            if (findFocus == this) {
                findFocus = null;
            }
            View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, 130);
            return (findNextFocus == null || findNextFocus == this || !findNextFocus.requestFocus(130)) ? false : true;
        }
    }

    public void fling(int i) {
        if (getChildCount() > 0) {
            int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
            int i2 = i;
            this.f3437c.fling(getScrollX(), getScrollY(), 0, i2, 0, 0, 0, Math.max(0, getChildAt(0).getHeight() - height), 0, height / 2);
            ViewCompat.m12913d(this);
        }
    }

    public boolean fullScroll(int i) {
        boolean z = i == 130;
        int height = getHeight();
        this.f3436b.top = 0;
        this.f3436b.bottom = height;
        if (z) {
            int childCount = getChildCount();
            if (childCount > 0) {
                this.f3436b.bottom = getChildAt(childCount - 1).getBottom() + getPaddingBottom();
                this.f3436b.top = this.f3436b.bottom - height;
            }
        }
        return m3934a(i, this.f3436b.top, this.f3436b.bottom);
    }

    /* access modifiers changed from: protected */
    public float getBottomFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int bottom = (getChildAt(0).getBottom() - getScrollY()) - (getHeight() - getPaddingBottom());
        if (bottom < verticalFadingEdgeLength) {
            return ((float) bottom) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    public int getMaxScrollAmount() {
        return (int) (0.5f * ((float) getHeight()));
    }

    public int getNestedScrollAxes() {
        return this.f3456x.mo13430a();
    }

    /* access modifiers changed from: 0000 */
    public int getScrollRange() {
        if (getChildCount() > 0) {
            return Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public float getTopFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int scrollY = getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return ((float) scrollY) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    public boolean hasNestedScrollingParent() {
        return this.f3457y.mo13428b();
    }

    public boolean isFillViewport() {
        return this.f3446l;
    }

    public boolean isNestedScrollingEnabled() {
        return this.f3457y.mo13422a();
    }

    public boolean isSmoothScrollingEnabled() {
        return this.f3447m;
    }

    /* access modifiers changed from: protected */
    public void measureChild(View view, int i, int i2) {
        view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), view.getLayoutParams().width), MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* access modifiers changed from: protected */
    public void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
        view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width), MeasureSpec.makeMeasureSpec(marginLayoutParams.topMargin + marginLayoutParams.bottomMargin, 0));
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f3442h = false;
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if ((motionEvent.getSource() & 2) != 0) {
            switch (motionEvent.getAction()) {
                case 8:
                    if (!this.f3444j) {
                        float e = MotionEventCompat.m12838e(motionEvent, 9);
                        if (e != 0.0f) {
                            int verticalScrollFactorCompat = (int) (getVerticalScrollFactorCompat() * e);
                            int scrollRange = getScrollRange();
                            int scrollY = getScrollY();
                            int i = scrollY - verticalScrollFactorCompat;
                            if (i < 0) {
                                i = 0;
                            } else if (i > scrollRange) {
                                i = scrollRange;
                            }
                            if (i != scrollY) {
                                super.scrollTo(getScrollX(), i);
                                return true;
                            }
                        }
                    }
                    break;
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        int action = motionEvent.getAction();
        if (action == 2 && this.f3444j) {
            return true;
        }
        switch (action & 255) {
            case 0:
                int y = (int) motionEvent.getY();
                if (m3933a((int) motionEvent.getX(), y)) {
                    this.f3440f = y;
                    this.f3451q = motionEvent.getPointerId(0);
                    m3943c();
                    this.f3445k.addMovement(motionEvent);
                    this.f3437c.computeScrollOffset();
                    if (!this.f3437c.isFinished()) {
                        z = true;
                    }
                    this.f3444j = z;
                    startNestedScroll(2);
                    break;
                } else {
                    this.f3444j = false;
                    m3945e();
                    break;
                }
            case 1:
            case 3:
                this.f3444j = false;
                this.f3451q = -1;
                m3945e();
                if (this.f3437c.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    ViewCompat.m12913d(this);
                }
                stopNestedScroll();
                break;
            case 2:
                int i = this.f3451q;
                if (i != -1) {
                    int findPointerIndex = motionEvent.findPointerIndex(i);
                    if (findPointerIndex != -1) {
                        int y2 = (int) motionEvent.getY(findPointerIndex);
                        if (Math.abs(y2 - this.f3440f) > this.f3448n && (getNestedScrollAxes() & 2) == 0) {
                            this.f3444j = true;
                            this.f3440f = y2;
                            m3944d();
                            this.f3445k.addMovement(motionEvent);
                            this.f3454t = 0;
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                                break;
                            }
                        }
                    } else {
                        Log.e("NestedScrollView", "Invalid pointerId=" + i + " in onInterceptTouchEvent");
                        break;
                    }
                }
                break;
            case 6:
                m3932a(motionEvent);
                break;
        }
        return this.f3444j;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f3441g = false;
        if (this.f3443i != null && m3938a(this.f3443i, (View) this)) {
            m3941b(this.f3443i);
        }
        this.f3443i = null;
        if (!this.f3442h) {
            if (this.f3455u != null) {
                scrollTo(getScrollX(), this.f3455u.scrollPosition);
                this.f3455u = null;
            }
            int max = Math.max(0, (getChildCount() > 0 ? getChildAt(0).getMeasuredHeight() : 0) - (((i4 - i2) - getPaddingBottom()) - getPaddingTop()));
            if (getScrollY() > max) {
                scrollTo(getScrollX(), max);
            } else if (getScrollY() < 0) {
                scrollTo(getScrollX(), 0);
            }
        }
        scrollTo(getScrollX(), getScrollY());
        this.f3442h = true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f3446l && MeasureSpec.getMode(i2) != 0 && getChildCount() > 0) {
            View childAt = getChildAt(0);
            int measuredHeight = getMeasuredHeight();
            if (childAt.getMeasuredHeight() < measuredHeight) {
                childAt.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), ((FrameLayout.LayoutParams) childAt.getLayoutParams()).width), MeasureSpec.makeMeasureSpec((measuredHeight - getPaddingTop()) - getPaddingBottom(), 1073741824));
            }
        }
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (z) {
            return false;
        }
        m3940b((int) f2);
        return true;
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return dispatchNestedPreFling(f, f2);
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        dispatchNestedPreScroll(i, i2, iArr, null);
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        int scrollY = getScrollY();
        scrollBy(0, i4);
        int scrollY2 = getScrollY() - scrollY;
        dispatchNestedScroll(0, scrollY2, 0, i4 - scrollY2, null);
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.f3456x.mo13432a(view, view2, i);
        startNestedScroll(2);
    }

    /* access modifiers changed from: protected */
    public void onOverScrolled(int i, int i2, boolean z, boolean z2) {
        super.scrollTo(i, i2);
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        if (i == 2) {
            i = 130;
        } else if (i == 1) {
            i = 33;
        }
        View findNextFocusFromRect = rect == null ? FocusFinder.getInstance().findNextFocus(this, null, i) : FocusFinder.getInstance().findNextFocusFromRect(this, rect, i);
        if (findNextFocusFromRect != null && !m3936a(findNextFocusFromRect)) {
            return findNextFocusFromRect.requestFocus(i, rect);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.f3455u = savedState;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.scrollPosition = getScrollY();
        return savedState;
    }

    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (this.f3434A != null) {
            this.f3434A.onScrollChange(this, i, i2, i3, i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        View findFocus = findFocus();
        if (findFocus != null && this != findFocus && m3937a(findFocus, 0, i4)) {
            findFocus.getDrawingRect(this.f3436b);
            offsetDescendantRectToMyCoords(findFocus, this.f3436b);
            m3931a(computeScrollDeltaToGetChildRectOnScreen(this.f3436b));
        }
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        return (i & 2) != 0;
    }

    public void onStopNestedScroll(View view) {
        this.f3456x.mo13431a(view);
        stopNestedScroll();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        m3944d();
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        int a = MotionEventCompat.m12830a(motionEvent);
        if (a == 0) {
            this.f3454t = 0;
        }
        obtain.offsetLocation(0.0f, (float) this.f3454t);
        switch (a) {
            case 0:
                if (getChildCount() != 0) {
                    boolean z = !this.f3437c.isFinished();
                    this.f3444j = z;
                    if (z) {
                        ViewParent parent = getParent();
                        if (parent != null) {
                            parent.requestDisallowInterceptTouchEvent(true);
                        }
                    }
                    if (!this.f3437c.isFinished()) {
                        this.f3437c.abortAnimation();
                    }
                    this.f3440f = (int) motionEvent.getY();
                    this.f3451q = motionEvent.getPointerId(0);
                    startNestedScroll(2);
                    break;
                } else {
                    return false;
                }
            case 1:
                if (this.f3444j) {
                    VelocityTracker velocityTracker = this.f3445k;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.f3450p);
                    int b = (int) VelocityTrackerCompat.m12858b(velocityTracker, this.f3451q);
                    if (Math.abs(b) > this.f3449o) {
                        m3940b(-b);
                    } else if (this.f3437c.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                        ViewCompat.m12913d(this);
                    }
                }
                this.f3451q = -1;
                m3946f();
                break;
            case 2:
                int findPointerIndex = motionEvent.findPointerIndex(this.f3451q);
                if (findPointerIndex != -1) {
                    int y = (int) motionEvent.getY(findPointerIndex);
                    int i = this.f3440f - y;
                    if (dispatchNestedPreScroll(0, i, this.f3453s, this.f3452r)) {
                        i -= this.f3453s[1];
                        obtain.offsetLocation(0.0f, (float) this.f3452r[1]);
                        this.f3454t += this.f3452r[1];
                    }
                    if (!this.f3444j && Math.abs(i) > this.f3448n) {
                        ViewParent parent2 = getParent();
                        if (parent2 != null) {
                            parent2.requestDisallowInterceptTouchEvent(true);
                        }
                        this.f3444j = true;
                        i = i > 0 ? i - this.f3448n : i + this.f3448n;
                    }
                    if (this.f3444j) {
                        this.f3440f = y - this.f3452r[1];
                        int scrollY = getScrollY();
                        int scrollRange = getScrollRange();
                        int overScrollMode = getOverScrollMode();
                        boolean z2 = overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0);
                        if (mo3226a(0, i, 0, getScrollY(), 0, scrollRange, 0, 0, true) && !hasNestedScrollingParent()) {
                            this.f3445k.clear();
                        }
                        int scrollY2 = getScrollY() - scrollY;
                        if (!dispatchNestedScroll(0, scrollY2, 0, i - scrollY2, this.f3452r)) {
                            if (z2) {
                                m3947g();
                                int i2 = scrollY + i;
                                if (i2 < 0) {
                                    this.f3438d.onPull(((float) i) / ((float) getHeight()), motionEvent.getX(findPointerIndex) / ((float) getWidth()));
                                    if (!this.f3439e.isFinished()) {
                                        this.f3439e.onRelease();
                                    }
                                } else if (i2 > scrollRange) {
                                    this.f3439e.onPull(((float) i) / ((float) getHeight()), 1.0f - (motionEvent.getX(findPointerIndex) / ((float) getWidth())));
                                    if (!this.f3438d.isFinished()) {
                                        this.f3438d.onRelease();
                                    }
                                }
                                if (this.f3438d != null && (!this.f3438d.isFinished() || !this.f3439e.isFinished())) {
                                    ViewCompat.m12913d(this);
                                    break;
                                }
                            }
                        } else {
                            this.f3440f -= this.f3452r[1];
                            obtain.offsetLocation(0.0f, (float) this.f3452r[1]);
                            this.f3454t += this.f3452r[1];
                            break;
                        }
                    }
                } else {
                    Log.e("NestedScrollView", "Invalid pointerId=" + this.f3451q + " in onTouchEvent");
                    break;
                }
                break;
            case 3:
                if (this.f3444j && getChildCount() > 0 && this.f3437c.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    ViewCompat.m12913d(this);
                }
                this.f3451q = -1;
                m3946f();
                break;
            case 5:
                int b2 = MotionEventCompat.m12832b(motionEvent);
                this.f3440f = (int) motionEvent.getY(b2);
                this.f3451q = motionEvent.getPointerId(b2);
                break;
            case 6:
                m3932a(motionEvent);
                this.f3440f = (int) motionEvent.getY(motionEvent.findPointerIndex(this.f3451q));
                break;
        }
        if (this.f3445k != null) {
            this.f3445k.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }

    public boolean pageScroll(int i) {
        boolean z = i == 130;
        int height = getHeight();
        if (z) {
            this.f3436b.top = getScrollY() + height;
            int childCount = getChildCount();
            if (childCount > 0) {
                View childAt = getChildAt(childCount - 1);
                if (this.f3436b.top + height > childAt.getBottom()) {
                    this.f3436b.top = childAt.getBottom() - height;
                }
            }
        } else {
            this.f3436b.top = getScrollY() - height;
            if (this.f3436b.top < 0) {
                this.f3436b.top = 0;
            }
        }
        this.f3436b.bottom = this.f3436b.top + height;
        return m3934a(i, this.f3436b.top, this.f3436b.bottom);
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.f3441g) {
            m3941b(view2);
        } else {
            this.f3443i = view2;
        }
        super.requestChildFocus(view, view2);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return m3935a(rect, z);
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (z) {
            m3945e();
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public void requestLayout() {
        this.f3441g = true;
        super.requestLayout();
    }

    public void scrollTo(int i, int i2) {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            int b = m3939b(i, (getWidth() - getPaddingRight()) - getPaddingLeft(), childAt.getWidth());
            int b2 = m3939b(i2, (getHeight() - getPaddingBottom()) - getPaddingTop(), childAt.getHeight());
            if (b != getScrollX() || b2 != getScrollY()) {
                super.scrollTo(b, b2);
            }
        }
    }

    public void setFillViewport(boolean z) {
        if (z != this.f3446l) {
            this.f3446l = z;
            requestLayout();
        }
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.f3457y.mo13421a(z);
    }

    public void setOnScrollChangeListener(OnScrollChangeListener onScrollChangeListener) {
        this.f3434A = onScrollChangeListener;
    }

    public void setSmoothScrollingEnabled(boolean z) {
        this.f3447m = z;
    }

    public boolean shouldDelayChildPressedState() {
        return true;
    }

    public final void smoothScrollBy(int i, int i2) {
        if (getChildCount() != 0) {
            if (AnimationUtils.currentAnimationTimeMillis() - this.f3435a > 250) {
                int max = Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
                int scrollY = getScrollY();
                this.f3437c.startScroll(getScrollX(), scrollY, 0, Math.max(0, Math.min(scrollY + i2, max)) - scrollY);
                ViewCompat.m12913d(this);
            } else {
                if (!this.f3437c.isFinished()) {
                    this.f3437c.abortAnimation();
                }
                scrollBy(i, i2);
            }
            this.f3435a = AnimationUtils.currentAnimationTimeMillis();
        }
    }

    public final void smoothScrollTo(int i, int i2) {
        smoothScrollBy(i - getScrollX(), i2 - getScrollY());
    }

    public boolean startNestedScroll(int i) {
        return this.f3457y.mo13425a(i);
    }

    public void stopNestedScroll() {
        this.f3457y.mo13429c();
    }
}
