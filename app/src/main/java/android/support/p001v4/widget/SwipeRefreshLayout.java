package android.support.p001v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;

/* renamed from: android.support.v4.widget.SwipeRefreshLayout */
public class SwipeRefreshLayout extends ViewGroup implements NestedScrollingChild, NestedScrollingParent {

    /* renamed from: B */
    private static final int[] f3513B = {16842766};
    public static final int DEFAULT = 1;
    public static final int LARGE = 0;

    /* renamed from: k */
    private static final String f3514k = SwipeRefreshLayout.class.getSimpleName();

    /* renamed from: A */
    private final DecelerateInterpolator f3515A;

    /* renamed from: C */
    private int f3516C;

    /* renamed from: D */
    private Animation f3517D;

    /* renamed from: E */
    private Animation f3518E;

    /* renamed from: F */
    private Animation f3519F;

    /* renamed from: G */
    private Animation f3520G;

    /* renamed from: H */
    private Animation f3521H;

    /* renamed from: I */
    private int f3522I;

    /* renamed from: J */
    private OnChildScrollUpCallback f3523J;

    /* renamed from: K */
    private AnimationListener f3524K;

    /* renamed from: L */
    private final Animation f3525L;

    /* renamed from: M */
    private final Animation f3526M;

    /* renamed from: a */
    OnRefreshListener f3527a;

    /* renamed from: b */
    boolean f3528b;

    /* renamed from: c */
    int f3529c;

    /* renamed from: d */
    boolean f3530d;

    /* renamed from: e */
    CircleImageView f3531e;

    /* renamed from: f */
    float f3532f;

    /* renamed from: g */
    int f3533g;

    /* renamed from: h */
    MaterialProgressDrawable f3534h;

    /* renamed from: i */
    boolean f3535i;

    /* renamed from: j */
    boolean f3536j;

    /* renamed from: l */
    private View f3537l;

    /* renamed from: m */
    private int f3538m;
    protected int mFrom;
    protected int mOriginalOffsetTop;

    /* renamed from: n */
    private float f3539n;

    /* renamed from: o */
    private float f3540o;

    /* renamed from: p */
    private final NestedScrollingParentHelper f3541p;

    /* renamed from: q */
    private final NestedScrollingChildHelper f3542q;

    /* renamed from: r */
    private final int[] f3543r;

    /* renamed from: s */
    private final int[] f3544s;

    /* renamed from: t */
    private boolean f3545t;

    /* renamed from: u */
    private int f3546u;

    /* renamed from: v */
    private float f3547v;

    /* renamed from: w */
    private float f3548w;

    /* renamed from: x */
    private boolean f3549x;

    /* renamed from: y */
    private int f3550y;

    /* renamed from: z */
    private boolean f3551z;

    /* renamed from: android.support.v4.widget.SwipeRefreshLayout$OnChildScrollUpCallback */
    public interface OnChildScrollUpCallback {
        boolean canChildScrollUp(SwipeRefreshLayout swipeRefreshLayout, View view);
    }

    /* renamed from: android.support.v4.widget.SwipeRefreshLayout$OnRefreshListener */
    public interface OnRefreshListener {
        void onRefresh();
    }

    public SwipeRefreshLayout(Context context) {
        this(context, null);
    }

    public SwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3528b = false;
        this.f3539n = -1.0f;
        this.f3543r = new int[2];
        this.f3544s = new int[2];
        this.f3550y = -1;
        this.f3516C = -1;
        this.f3524K = new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (SwipeRefreshLayout.this.f3528b) {
                    SwipeRefreshLayout.this.f3534h.setAlpha(255);
                    SwipeRefreshLayout.this.f3534h.start();
                    if (SwipeRefreshLayout.this.f3535i && SwipeRefreshLayout.this.f3527a != null) {
                        SwipeRefreshLayout.this.f3527a.onRefresh();
                    }
                    SwipeRefreshLayout.this.f3529c = SwipeRefreshLayout.this.f3531e.getTop();
                    return;
                }
                SwipeRefreshLayout.this.mo3412a();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        };
        this.f3525L = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.mo3414a((SwipeRefreshLayout.this.mFrom + ((int) (((float) ((!SwipeRefreshLayout.this.f3536j ? SwipeRefreshLayout.this.f3533g - Math.abs(SwipeRefreshLayout.this.mOriginalOffsetTop) : SwipeRefreshLayout.this.f3533g) - SwipeRefreshLayout.this.mFrom)) * f))) - SwipeRefreshLayout.this.f3531e.getTop(), false);
                SwipeRefreshLayout.this.f3534h.setArrowScale(1.0f - f);
            }
        };
        this.f3526M = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.mo3413a(f);
            }
        };
        this.f3538m = ViewConfiguration.get(context).getScaledTouchSlop();
        this.f3546u = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.f3515A = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.f3522I = (int) (40.0f * displayMetrics.density);
        m3978b();
        ViewCompat.m12900a((ViewGroup) this, true);
        this.f3533g = (int) (64.0f * displayMetrics.density);
        this.f3539n = (float) this.f3533g;
        this.f3541p = new NestedScrollingParentHelper(this);
        this.f3542q = new NestedScrollingChildHelper(this);
        setNestedScrollingEnabled(true);
        int i = -this.f3522I;
        this.f3529c = i;
        this.mOriginalOffsetTop = i;
        mo3413a(1.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f3513B);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    /* renamed from: a */
    private Animation m3973a(final int i, final int i2) {
        if (this.f3530d && m3984c()) {
            return null;
        }
        C05194 r0 = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.f3534h.setAlpha((int) (((float) i) + (((float) (i2 - i)) * f)));
            }
        };
        r0.setDuration(300);
        this.f3531e.setAnimationListener(null);
        this.f3531e.clearAnimation();
        this.f3531e.startAnimation(r0);
        return r0;
    }

    /* renamed from: a */
    private void m3974a(int i, AnimationListener animationListener) {
        this.mFrom = i;
        this.f3525L.reset();
        this.f3525L.setDuration(200);
        this.f3525L.setInterpolator(this.f3515A);
        if (animationListener != null) {
            this.f3531e.setAnimationListener(animationListener);
        }
        this.f3531e.clearAnimation();
        this.f3531e.startAnimation(this.f3525L);
    }

    /* renamed from: a */
    private void m3975a(MotionEvent motionEvent) {
        int b = MotionEventCompat.m12832b(motionEvent);
        if (motionEvent.getPointerId(b) == this.f3550y) {
            this.f3550y = motionEvent.getPointerId(b == 0 ? 1 : 0);
        }
    }

    /* renamed from: a */
    private void m3976a(boolean z, boolean z2) {
        if (this.f3528b != z) {
            this.f3535i = z2;
            m3988f();
            this.f3528b = z;
            if (this.f3528b) {
                m3974a(this.f3529c, this.f3524K);
            } else {
                mo3415a(this.f3524K);
            }
        }
    }

    /* renamed from: a */
    private boolean m3977a(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    /* renamed from: b */
    private void m3978b() {
        this.f3531e = new CircleImageView(getContext(), -328966);
        this.f3534h = new MaterialProgressDrawable(getContext(), this);
        this.f3534h.setBackgroundColor(-328966);
        this.f3531e.setImageDrawable(this.f3534h);
        this.f3531e.setVisibility(8);
        addView(this.f3531e);
    }

    /* renamed from: b */
    private void m3979b(float f) {
        this.f3534h.showArrow(true);
        float min = Math.min(1.0f, Math.abs(f / this.f3539n));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f) - this.f3539n;
        float f2 = this.f3536j ? (float) (this.f3533g - this.mOriginalOffsetTop) : (float) this.f3533g;
        float max2 = Math.max(0.0f, Math.min(abs, 2.0f * f2) / f2);
        float pow = ((float) (((double) (max2 / 4.0f)) - Math.pow((double) (max2 / 4.0f), 2.0d))) * 2.0f;
        int i = this.mOriginalOffsetTop + ((int) ((f2 * min) + (f2 * pow * 2.0f)));
        if (this.f3531e.getVisibility() != 0) {
            this.f3531e.setVisibility(0);
        }
        if (!this.f3530d) {
            ViewCompat.m12914d((View) this.f3531e, 1.0f);
            ViewCompat.m12917e((View) this.f3531e, 1.0f);
        }
        if (this.f3530d) {
            setAnimationProgress(Math.min(1.0f, f / this.f3539n));
        }
        if (f < this.f3539n) {
            if (this.f3534h.getAlpha() > 76) {
                if (!m3977a(this.f3519F)) {
                    m3985d();
                }
            }
        } else if (this.f3534h.getAlpha() < 255) {
            if (!m3977a(this.f3520G)) {
                m3987e();
            }
        }
        this.f3534h.setStartEndTrim(0.0f, Math.min(0.8f, max * 0.8f));
        this.f3534h.setArrowScale(Math.min(1.0f, max));
        this.f3534h.setProgressRotation((-0.25f + (0.4f * max) + (2.0f * pow)) * 0.5f);
        mo3414a(i - this.f3529c, true);
    }

    /* renamed from: b */
    private void m3980b(int i, AnimationListener animationListener) {
        if (this.f3530d) {
            m3983c(i, animationListener);
            return;
        }
        this.mFrom = i;
        this.f3526M.reset();
        this.f3526M.setDuration(200);
        this.f3526M.setInterpolator(this.f3515A);
        if (animationListener != null) {
            this.f3531e.setAnimationListener(animationListener);
        }
        this.f3531e.clearAnimation();
        this.f3531e.startAnimation(this.f3526M);
    }

    /* renamed from: b */
    private void m3981b(AnimationListener animationListener) {
        this.f3531e.setVisibility(0);
        if (VERSION.SDK_INT >= 11) {
            this.f3534h.setAlpha(255);
        }
        this.f3517D = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(f);
            }
        };
        this.f3517D.setDuration((long) this.f3546u);
        if (animationListener != null) {
            this.f3531e.setAnimationListener(animationListener);
        }
        this.f3531e.clearAnimation();
        this.f3531e.startAnimation(this.f3517D);
    }

    /* renamed from: c */
    private void m3982c(float f) {
        if (f > this.f3539n) {
            m3976a(true, true);
            return;
        }
        this.f3528b = false;
        this.f3534h.setStartEndTrim(0.0f, 0.0f);
        C05205 r0 = null;
        if (!this.f3530d) {
            r0 = new AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    if (!SwipeRefreshLayout.this.f3530d) {
                        SwipeRefreshLayout.this.mo3415a((AnimationListener) null);
                    }
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            };
        }
        m3980b(this.f3529c, r0);
        this.f3534h.showArrow(false);
    }

    /* renamed from: c */
    private void m3983c(int i, AnimationListener animationListener) {
        this.mFrom = i;
        if (m3984c()) {
            this.f3532f = (float) this.f3534h.getAlpha();
        } else {
            this.f3532f = ViewCompat.m12935t(this.f3531e);
        }
        this.f3521H = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(SwipeRefreshLayout.this.f3532f + ((-SwipeRefreshLayout.this.f3532f) * f));
                SwipeRefreshLayout.this.mo3413a(f);
            }
        };
        this.f3521H.setDuration(150);
        if (animationListener != null) {
            this.f3531e.setAnimationListener(animationListener);
        }
        this.f3531e.clearAnimation();
        this.f3531e.startAnimation(this.f3521H);
    }

    /* renamed from: c */
    private boolean m3984c() {
        return VERSION.SDK_INT < 11;
    }

    /* renamed from: d */
    private void m3985d() {
        this.f3519F = m3973a(this.f3534h.getAlpha(), 76);
    }

    /* renamed from: d */
    private void m3986d(float f) {
        if (f - this.f3548w > ((float) this.f3538m) && !this.f3549x) {
            this.f3547v = this.f3548w + ((float) this.f3538m);
            this.f3549x = true;
            this.f3534h.setAlpha(76);
        }
    }

    /* renamed from: e */
    private void m3987e() {
        this.f3520G = m3973a(this.f3534h.getAlpha(), 255);
    }

    /* renamed from: f */
    private void m3988f() {
        if (this.f3537l == null) {
            for (int i = 0; i < getChildCount(); i++) {
                View childAt = getChildAt(i);
                if (!childAt.equals(this.f3531e)) {
                    this.f3537l = childAt;
                    return;
                }
            }
        }
    }

    private void setColorViewAlpha(int i) {
        this.f3531e.getBackground().setAlpha(i);
        this.f3534h.setAlpha(i);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3412a() {
        this.f3531e.clearAnimation();
        this.f3534h.stop();
        this.f3531e.setVisibility(8);
        setColorViewAlpha(255);
        if (this.f3530d) {
            setAnimationProgress(0.0f);
        } else {
            mo3414a(this.mOriginalOffsetTop - this.f3529c, true);
        }
        this.f3529c = this.f3531e.getTop();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3413a(float f) {
        mo3414a((this.mFrom + ((int) (((float) (this.mOriginalOffsetTop - this.mFrom)) * f))) - this.f3531e.getTop(), false);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3414a(int i, boolean z) {
        this.f3531e.bringToFront();
        ViewCompat.m12918e((View) this.f3531e, i);
        this.f3529c = this.f3531e.getTop();
        if (z && VERSION.SDK_INT < 11) {
            invalidate();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3415a(AnimationListener animationListener) {
        this.f3518E = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(1.0f - f);
            }
        };
        this.f3518E.setDuration(150);
        this.f3531e.setAnimationListener(animationListener);
        this.f3531e.clearAnimation();
        this.f3531e.startAnimation(this.f3518E);
    }

    public boolean canChildScrollUp() {
        boolean z = false;
        if (this.f3523J != null) {
            return this.f3523J.canChildScrollUp(this, this.f3537l);
        }
        if (VERSION.SDK_INT >= 14) {
            return ViewCompat.m12908b(this.f3537l, -1);
        }
        if (this.f3537l instanceof AbsListView) {
            AbsListView absListView = (AbsListView) this.f3537l;
            return absListView.getChildCount() > 0 && (absListView.getFirstVisiblePosition() > 0 || absListView.getChildAt(0).getTop() < absListView.getPaddingTop());
        }
        if (ViewCompat.m12908b(this.f3537l, -1) || this.f3537l.getScrollY() > 0) {
            z = true;
        }
        return z;
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.f3542q.mo13424a(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.f3542q.mo13423a(f, f2);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.f3542q.mo13427a(i, i2, iArr, iArr2);
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.f3542q.mo13426a(i, i2, i3, i4, iArr);
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        return this.f3516C < 0 ? i2 : i2 == i + -1 ? this.f3516C : i2 >= this.f3516C ? i2 + 1 : i2;
    }

    public int getNestedScrollAxes() {
        return this.f3541p.mo13430a();
    }

    public int getProgressCircleDiameter() {
        return this.f3522I;
    }

    public int getProgressViewEndOffset() {
        return this.f3533g;
    }

    public int getProgressViewStartOffset() {
        return this.mOriginalOffsetTop;
    }

    public boolean hasNestedScrollingParent() {
        return this.f3542q.mo13428b();
    }

    public boolean isNestedScrollingEnabled() {
        return this.f3542q.mo13422a();
    }

    public boolean isRefreshing() {
        return this.f3528b;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mo3412a();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        m3988f();
        int a = MotionEventCompat.m12830a(motionEvent);
        if (this.f3551z && a == 0) {
            this.f3551z = false;
        }
        if (!isEnabled() || this.f3551z || canChildScrollUp() || this.f3528b || this.f3545t) {
            return false;
        }
        switch (a) {
            case 0:
                mo3414a(this.mOriginalOffsetTop - this.f3531e.getTop(), true);
                this.f3550y = motionEvent.getPointerId(0);
                this.f3549x = false;
                int findPointerIndex = motionEvent.findPointerIndex(this.f3550y);
                if (findPointerIndex >= 0) {
                    this.f3548w = motionEvent.getY(findPointerIndex);
                    break;
                } else {
                    return false;
                }
            case 1:
            case 3:
                this.f3549x = false;
                this.f3550y = -1;
                break;
            case 2:
                if (this.f3550y == -1) {
                    Log.e(f3514k, "Got ACTION_MOVE event but don't have an active pointer id.");
                    return false;
                }
                int findPointerIndex2 = motionEvent.findPointerIndex(this.f3550y);
                if (findPointerIndex2 >= 0) {
                    m3986d(motionEvent.getY(findPointerIndex2));
                    break;
                } else {
                    return false;
                }
            case 6:
                m3975a(motionEvent);
                break;
        }
        return this.f3549x;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.f3537l == null) {
                m3988f();
            }
            if (this.f3537l != null) {
                View view = this.f3537l;
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, paddingLeft + ((measuredWidth - getPaddingLeft()) - getPaddingRight()), paddingTop + ((measuredHeight - getPaddingTop()) - getPaddingBottom()));
                int measuredWidth2 = this.f3531e.getMeasuredWidth();
                this.f3531e.layout((measuredWidth / 2) - (measuredWidth2 / 2), this.f3529c, (measuredWidth / 2) + (measuredWidth2 / 2), this.f3529c + this.f3531e.getMeasuredHeight());
            }
        }
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f3537l == null) {
            m3988f();
        }
        if (this.f3537l != null) {
            this.f3537l.measure(MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.f3531e.measure(MeasureSpec.makeMeasureSpec(this.f3522I, 1073741824), MeasureSpec.makeMeasureSpec(this.f3522I, 1073741824));
            this.f3516C = -1;
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                if (getChildAt(i3) == this.f3531e) {
                    this.f3516C = i3;
                    return;
                }
            }
        }
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        return dispatchNestedFling(f, f2, z);
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return dispatchNestedPreFling(f, f2);
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        if (i2 > 0 && this.f3540o > 0.0f) {
            if (((float) i2) > this.f3540o) {
                iArr[1] = i2 - ((int) this.f3540o);
                this.f3540o = 0.0f;
            } else {
                this.f3540o -= (float) i2;
                iArr[1] = i2;
            }
            m3979b(this.f3540o);
        }
        if (this.f3536j && i2 > 0 && this.f3540o == 0.0f && Math.abs(i2 - iArr[1]) > 0) {
            this.f3531e.setVisibility(8);
        }
        int[] iArr2 = this.f3543r;
        if (dispatchNestedPreScroll(i - iArr[0], i2 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr[1] + iArr2[1];
        }
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        dispatchNestedScroll(i, i2, i3, i4, this.f3544s);
        int i5 = i4 + this.f3544s[1];
        if (i5 < 0 && !canChildScrollUp()) {
            this.f3540o += (float) Math.abs(i5);
            m3979b(this.f3540o);
        }
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.f3541p.mo13432a(view, view2, i);
        startNestedScroll(i & 2);
        this.f3540o = 0.0f;
        this.f3545t = true;
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        return isEnabled() && !this.f3551z && !this.f3528b && (i & 2) != 0;
    }

    public void onStopNestedScroll(View view) {
        this.f3541p.mo13431a(view);
        this.f3545t = false;
        if (this.f3540o > 0.0f) {
            m3982c(this.f3540o);
            this.f3540o = 0.0f;
        }
        stopNestedScroll();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a = MotionEventCompat.m12830a(motionEvent);
        if (this.f3551z && a == 0) {
            this.f3551z = false;
        }
        if (!isEnabled() || this.f3551z || canChildScrollUp() || this.f3528b || this.f3545t) {
            return false;
        }
        switch (a) {
            case 0:
                this.f3550y = motionEvent.getPointerId(0);
                this.f3549x = false;
                break;
            case 1:
                int findPointerIndex = motionEvent.findPointerIndex(this.f3550y);
                if (findPointerIndex < 0) {
                    Log.e(f3514k, "Got ACTION_UP event but don't have an active pointer id.");
                    return false;
                }
                if (this.f3549x) {
                    float y = (motionEvent.getY(findPointerIndex) - this.f3547v) * 0.5f;
                    this.f3549x = false;
                    m3982c(y);
                }
                this.f3550y = -1;
                return false;
            case 2:
                int findPointerIndex2 = motionEvent.findPointerIndex(this.f3550y);
                if (findPointerIndex2 < 0) {
                    Log.e(f3514k, "Got ACTION_MOVE event but have an invalid active pointer id.");
                    return false;
                }
                float y2 = motionEvent.getY(findPointerIndex2);
                m3986d(y2);
                if (this.f3549x) {
                    float f = (y2 - this.f3547v) * 0.5f;
                    if (f > 0.0f) {
                        m3979b(f);
                        break;
                    } else {
                        return false;
                    }
                }
                break;
            case 3:
                return false;
            case 5:
                int b = MotionEventCompat.m12832b(motionEvent);
                if (b >= 0) {
                    this.f3550y = motionEvent.getPointerId(b);
                    break;
                } else {
                    Log.e(f3514k, "Got ACTION_POINTER_DOWN event but have an invalid action index.");
                    return false;
                }
            case 6:
                m3975a(motionEvent);
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (VERSION.SDK_INT < 21 && (this.f3537l instanceof AbsListView)) {
            return;
        }
        if (this.f3537l == null || ViewCompat.m12875D(this.f3537l)) {
            super.requestDisallowInterceptTouchEvent(z);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setAnimationProgress(float f) {
        if (m3984c()) {
            setColorViewAlpha((int) (255.0f * f));
            return;
        }
        ViewCompat.m12914d((View) this.f3531e, f);
        ViewCompat.m12917e((View) this.f3531e, f);
    }

    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    public void setColorSchemeColors(int... iArr) {
        m3988f();
        this.f3534h.setColorSchemeColors(iArr);
    }

    public void setColorSchemeResources(int... iArr) {
        Context context = getContext();
        int[] iArr2 = new int[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr2[i] = ContextCompat.getColor(context, iArr[i]);
        }
        setColorSchemeColors(iArr2);
    }

    public void setDistanceToTriggerSync(int i) {
        this.f3539n = (float) i;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (!z) {
            mo3412a();
        }
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.f3542q.mo13421a(z);
    }

    public void setOnChildScrollUpCallback(OnChildScrollUpCallback onChildScrollUpCallback) {
        this.f3523J = onChildScrollUpCallback;
    }

    public void setOnRefreshListener(OnRefreshListener onRefreshListener) {
        this.f3527a = onRefreshListener;
    }

    @Deprecated
    public void setProgressBackgroundColor(int i) {
        setProgressBackgroundColorSchemeResource(i);
    }

    public void setProgressBackgroundColorSchemeColor(int i) {
        this.f3531e.setBackgroundColor(i);
        this.f3534h.setBackgroundColor(i);
    }

    public void setProgressBackgroundColorSchemeResource(int i) {
        setProgressBackgroundColorSchemeColor(ContextCompat.getColor(getContext(), i));
    }

    public void setProgressViewEndTarget(boolean z, int i) {
        this.f3533g = i;
        this.f3530d = z;
        this.f3531e.invalidate();
    }

    public void setProgressViewOffset(boolean z, int i, int i2) {
        this.f3530d = z;
        this.mOriginalOffsetTop = i;
        this.f3533g = i2;
        this.f3536j = true;
        mo3412a();
        this.f3528b = false;
    }

    public void setRefreshing(boolean z) {
        if (!z || this.f3528b == z) {
            m3976a(z, false);
            return;
        }
        this.f3528b = z;
        mo3414a((!this.f3536j ? this.f3533g + this.mOriginalOffsetTop : this.f3533g) - this.f3529c, true);
        this.f3535i = false;
        m3981b(this.f3524K);
    }

    public void setSize(int i) {
        if (i == 0 || i == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i == 0) {
                this.f3522I = (int) (56.0f * displayMetrics.density);
            } else {
                this.f3522I = (int) (40.0f * displayMetrics.density);
            }
            this.f3531e.setImageDrawable(null);
            this.f3534h.updateSizes(i);
            this.f3531e.setImageDrawable(this.f3534h);
        }
    }

    public boolean startNestedScroll(int i) {
        return this.f3542q.mo13425a(i);
    }

    public void stopNestedScroll() {
        this.f3542q.mo13429c();
    }
}
