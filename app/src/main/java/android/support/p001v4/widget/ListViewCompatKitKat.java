package android.support.p001v4.widget;

import android.widget.ListView;

/* renamed from: android.support.v4.widget.ListViewCompatKitKat */
class ListViewCompatKitKat {
    ListViewCompatKitKat() {
    }

    /* renamed from: a */
    static void m3918a(ListView listView, int i) {
        listView.scrollListBy(i);
    }
}
