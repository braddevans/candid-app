package android.support.p001v4.widget;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.TextView;
import java.lang.reflect.Field;

/* renamed from: android.support.v4.widget.TextViewCompatGingerbread */
class TextViewCompatGingerbread {

    /* renamed from: a */
    private static Field f3563a;

    /* renamed from: b */
    private static boolean f3564b;

    /* renamed from: c */
    private static Field f3565c;

    /* renamed from: d */
    private static boolean f3566d;

    /* renamed from: e */
    private static Field f3567e;

    /* renamed from: f */
    private static boolean f3568f;

    /* renamed from: g */
    private static Field f3569g;

    /* renamed from: h */
    private static boolean f3570h;

    TextViewCompatGingerbread() {
    }

    /* renamed from: a */
    static int m3993a(TextView textView) {
        if (!f3566d) {
            f3565c = m3995a("mMaxMode");
            f3566d = true;
        }
        if (f3565c != null && m3994a(f3565c, textView) == 1) {
            if (!f3564b) {
                f3563a = m3995a("mMaximum");
                f3564b = true;
            }
            if (f3563a != null) {
                return m3994a(f3563a, textView);
            }
        }
        return -1;
    }

    /* renamed from: a */
    private static int m3994a(Field field, TextView textView) {
        try {
            return field.getInt(textView);
        } catch (IllegalAccessException e) {
            Log.d("TextViewCompatGingerbread", "Could not retrieve value of " + field.getName() + " field.");
            return -1;
        }
    }

    /* renamed from: a */
    private static Field m3995a(String str) {
        Field field = null;
        try {
            field = TextView.class.getDeclaredField(str);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e) {
            Log.e("TextViewCompatGingerbread", "Could not retrieve " + str + " field.");
            return field;
        }
    }

    /* renamed from: a */
    static void m3996a(TextView textView, int i) {
        textView.setTextAppearance(textView.getContext(), i);
    }

    /* renamed from: b */
    static int m3997b(TextView textView) {
        if (!f3570h) {
            f3569g = m3995a("mMinMode");
            f3570h = true;
        }
        if (f3569g != null && m3994a(f3569g, textView) == 1) {
            if (!f3568f) {
                f3567e = m3995a("mMinimum");
                f3568f = true;
            }
            if (f3567e != null) {
                return m3994a(f3567e, textView);
            }
        }
        return -1;
    }

    /* renamed from: c */
    static Drawable[] m3998c(TextView textView) {
        return textView.getCompoundDrawables();
    }
}
