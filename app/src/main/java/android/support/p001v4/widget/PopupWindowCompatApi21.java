package android.support.p001v4.widget;

import android.util.Log;
import android.widget.PopupWindow;
import java.lang.reflect.Field;

/* renamed from: android.support.v4.widget.PopupWindowCompatApi21 */
class PopupWindowCompatApi21 {

    /* renamed from: a */
    private static Field f3465a;

    static {
        try {
            f3465a = PopupWindow.class.getDeclaredField("mOverlapAnchor");
            f3465a.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e);
        }
    }

    PopupWindowCompatApi21() {
    }

    /* renamed from: a */
    static void m3949a(PopupWindow popupWindow, boolean z) {
        if (f3465a != null) {
            try {
                f3465a.set(popupWindow, Boolean.valueOf(z));
            } catch (IllegalAccessException e) {
                Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e);
            }
        }
    }

    /* renamed from: a */
    static boolean m3950a(PopupWindow popupWindow) {
        if (f3465a != null) {
            try {
                return ((Boolean) f3465a.get(popupWindow)).booleanValue();
            } catch (IllegalAccessException e) {
                Log.i("PopupWindowCompatApi21", "Could not get overlap anchor field in PopupWindow", e);
            }
        }
        return false;
    }
}
