package android.support.p001v4.widget;

import android.database.Cursor;
import android.widget.Filter;
import android.widget.Filter.FilterResults;

/* renamed from: android.support.v4.widget.CursorFilter */
class CursorFilter extends Filter {

    /* renamed from: a */
    CursorFilterClient f3310a;

    /* renamed from: android.support.v4.widget.CursorFilter$CursorFilterClient */
    interface CursorFilterClient {
        void changeCursor(Cursor cursor);

        CharSequence convertToString(Cursor cursor);

        Cursor getCursor();

        Cursor runQueryOnBackgroundThread(CharSequence charSequence);
    }

    CursorFilter(CursorFilterClient cursorFilterClient) {
        this.f3310a = cursorFilterClient;
    }

    public CharSequence convertResultToString(Object obj) {
        return this.f3310a.convertToString((Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public FilterResults performFiltering(CharSequence charSequence) {
        Cursor runQueryOnBackgroundThread = this.f3310a.runQueryOnBackgroundThread(charSequence);
        FilterResults filterResults = new FilterResults();
        if (runQueryOnBackgroundThread != null) {
            filterResults.count = runQueryOnBackgroundThread.getCount();
            filterResults.values = runQueryOnBackgroundThread;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    /* access modifiers changed from: protected */
    public void publishResults(CharSequence charSequence, FilterResults filterResults) {
        Cursor cursor = this.f3310a.getCursor();
        if (filterResults.values != null && filterResults.values != cursor) {
            this.f3310a.changeCursor((Cursor) filterResults.values);
        }
    }
}
