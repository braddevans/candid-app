package android.support.p001v4.widget;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import java.util.Arrays;

/* renamed from: android.support.v4.widget.ViewDragHelper */
public class ViewDragHelper {
    public static final int DIRECTION_ALL = 3;
    public static final int DIRECTION_HORIZONTAL = 1;
    public static final int DIRECTION_VERTICAL = 2;
    public static final int EDGE_ALL = 15;
    public static final int EDGE_BOTTOM = 8;
    public static final int EDGE_LEFT = 1;
    public static final int EDGE_RIGHT = 2;
    public static final int EDGE_TOP = 4;
    public static final int INVALID_POINTER = -1;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;

    /* renamed from: v */
    private static final Interpolator f3571v = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };

    /* renamed from: a */
    private int f3572a;

    /* renamed from: b */
    private int f3573b;

    /* renamed from: c */
    private int f3574c = -1;

    /* renamed from: d */
    private float[] f3575d;

    /* renamed from: e */
    private float[] f3576e;

    /* renamed from: f */
    private float[] f3577f;

    /* renamed from: g */
    private float[] f3578g;

    /* renamed from: h */
    private int[] f3579h;

    /* renamed from: i */
    private int[] f3580i;

    /* renamed from: j */
    private int[] f3581j;

    /* renamed from: k */
    private int f3582k;

    /* renamed from: l */
    private VelocityTracker f3583l;

    /* renamed from: m */
    private float f3584m;

    /* renamed from: n */
    private float f3585n;

    /* renamed from: o */
    private int f3586o;

    /* renamed from: p */
    private int f3587p;

    /* renamed from: q */
    private ScrollerCompat f3588q;

    /* renamed from: r */
    private final Callback f3589r;

    /* renamed from: s */
    private View f3590s;

    /* renamed from: t */
    private boolean f3591t;

    /* renamed from: u */
    private final ViewGroup f3592u;

    /* renamed from: w */
    private final Runnable f3593w = new Runnable() {
        public void run() {
            ViewDragHelper.this.mo3476a(0);
        }
    };

    /* renamed from: android.support.v4.widget.ViewDragHelper$Callback */
    public static abstract class Callback {
        public int clampViewPositionHorizontal(View view, int i, int i2) {
            return 0;
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return 0;
        }

        public int getOrderedChildIndex(int i) {
            return i;
        }

        public int getViewHorizontalDragRange(View view) {
            return 0;
        }

        public int getViewVerticalDragRange(View view) {
            return 0;
        }

        public void onEdgeDragStarted(int i, int i2) {
        }

        public boolean onEdgeLock(int i) {
            return false;
        }

        public void onEdgeTouched(int i, int i2) {
        }

        public void onViewCaptured(View view, int i) {
        }

        public void onViewDragStateChanged(int i) {
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
        }

        public void onViewReleased(View view, float f, float f2) {
        }

        public abstract boolean tryCaptureView(View view, int i);
    }

    private ViewDragHelper(Context context, ViewGroup viewGroup, Callback callback) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (callback == null) {
            throw new IllegalArgumentException("Callback may not be null");
        } else {
            this.f3592u = viewGroup;
            this.f3589r = callback;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.f3586o = (int) ((20.0f * context.getResources().getDisplayMetrics().density) + 0.5f);
            this.f3573b = viewConfiguration.getScaledTouchSlop();
            this.f3584m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.f3585n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.f3588q = ScrollerCompat.create(context, f3571v);
        }
    }

    /* renamed from: a */
    private float m4002a(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    /* renamed from: a */
    private float m4003a(float f, float f2, float f3) {
        float abs = Math.abs(f);
        if (abs < f2) {
            return 0.0f;
        }
        return abs > f3 ? f <= 0.0f ? -f3 : f3 : f;
    }

    /* renamed from: a */
    private int m4004a(int i, int i2) {
        int i3 = 0;
        if (i < this.f3592u.getLeft() + this.f3586o) {
            i3 = 0 | 1;
        }
        if (i2 < this.f3592u.getTop() + this.f3586o) {
            i3 |= 4;
        }
        if (i > this.f3592u.getRight() - this.f3586o) {
            i3 |= 2;
        }
        return i2 > this.f3592u.getBottom() - this.f3586o ? i3 | 8 : i3;
    }

    /* renamed from: a */
    private int m4005a(int i, int i2, int i3) {
        if (i == 0) {
            return 0;
        }
        int width = this.f3592u.getWidth();
        int i4 = width / 2;
        float a = ((float) i4) + (((float) i4) * m4002a(Math.min(1.0f, ((float) Math.abs(i)) / ((float) width))));
        int abs = Math.abs(i2);
        return Math.min(abs > 0 ? Math.round(1000.0f * Math.abs(a / ((float) abs))) * 4 : (int) (((((float) Math.abs(i)) / ((float) i3)) + 1.0f) * 256.0f), 600);
    }

    /* renamed from: a */
    private int m4006a(View view, int i, int i2, int i3, int i4) {
        int b = m4014b(i3, (int) this.f3585n, (int) this.f3584m);
        int b2 = m4014b(i4, (int) this.f3585n, (int) this.f3584m);
        int abs = Math.abs(i);
        int abs2 = Math.abs(i2);
        int abs3 = Math.abs(b);
        int abs4 = Math.abs(b2);
        int i5 = abs3 + abs4;
        int i6 = abs + abs2;
        return (int) ((((float) m4005a(i, b, this.f3589r.getViewHorizontalDragRange(view))) * (b != 0 ? ((float) abs3) / ((float) i5) : ((float) abs) / ((float) i6))) + (((float) m4005a(i2, b2, this.f3589r.getViewVerticalDragRange(view))) * (b2 != 0 ? ((float) abs4) / ((float) i5) : ((float) abs2) / ((float) i6))));
    }

    /* renamed from: a */
    private void m4007a() {
        if (this.f3575d != null) {
            Arrays.fill(this.f3575d, 0.0f);
            Arrays.fill(this.f3576e, 0.0f);
            Arrays.fill(this.f3577f, 0.0f);
            Arrays.fill(this.f3578g, 0.0f);
            Arrays.fill(this.f3579h, 0);
            Arrays.fill(this.f3580i, 0);
            Arrays.fill(this.f3581j, 0);
            this.f3582k = 0;
        }
    }

    /* renamed from: a */
    private void m4008a(float f, float f2) {
        this.f3591t = true;
        this.f3589r.onViewReleased(this.f3590s, f, f2);
        this.f3591t = false;
        if (this.f3572a == 1) {
            mo3476a(0);
        }
    }

    /* renamed from: a */
    private void m4009a(float f, float f2, int i) {
        m4019c(i);
        float[] fArr = this.f3575d;
        this.f3577f[i] = f;
        fArr[i] = f;
        float[] fArr2 = this.f3576e;
        this.f3578g[i] = f2;
        fArr2[i] = f2;
        this.f3579h[i] = m4004a((int) f, (int) f2);
        this.f3582k |= 1 << i;
    }

    /* renamed from: a */
    private void m4010a(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i = 0; i < pointerCount; i++) {
            int pointerId = motionEvent.getPointerId(i);
            if (m4020d(pointerId)) {
                float x = motionEvent.getX(i);
                float y = motionEvent.getY(i);
                this.f3577f[pointerId] = x;
                this.f3578g[pointerId] = y;
            }
        }
    }

    /* renamed from: a */
    private boolean m4011a(float f, float f2, int i, int i2) {
        float abs = Math.abs(f);
        float abs2 = Math.abs(f2);
        if ((this.f3579h[i] & i2) != i2 || (this.f3587p & i2) == 0 || (this.f3581j[i] & i2) == i2 || (this.f3580i[i] & i2) == i2) {
            return false;
        }
        if (abs <= ((float) this.f3573b) && abs2 <= ((float) this.f3573b)) {
            return false;
        }
        if (abs >= 0.5f * abs2 || !this.f3589r.onEdgeLock(i2)) {
            return (this.f3580i[i] & i2) == 0 && abs > ((float) this.f3573b);
        }
        int[] iArr = this.f3581j;
        iArr[i] = iArr[i] | i2;
        return false;
    }

    /* renamed from: a */
    private boolean m4012a(int i, int i2, int i3, int i4) {
        int left = this.f3590s.getLeft();
        int top = this.f3590s.getTop();
        int i5 = i - left;
        int i6 = i2 - top;
        if (i5 == 0 && i6 == 0) {
            this.f3588q.abortAnimation();
            mo3476a(0);
            return false;
        }
        this.f3588q.startScroll(left, top, i5, i6, m4006a(this.f3590s, i5, i6, i3, i4));
        mo3476a(2);
        return true;
    }

    /* renamed from: a */
    private boolean m4013a(View view, float f, float f2) {
        if (view == null) {
            return false;
        }
        boolean z = this.f3589r.getViewHorizontalDragRange(view) > 0;
        boolean z2 = this.f3589r.getViewVerticalDragRange(view) > 0;
        if (z && z2) {
            return (f * f) + (f2 * f2) > ((float) (this.f3573b * this.f3573b));
        }
        if (z) {
            return Math.abs(f) > ((float) this.f3573b);
        }
        if (z2) {
            return Math.abs(f2) > ((float) this.f3573b);
        }
        return false;
    }

    /* renamed from: b */
    private int m4014b(int i, int i2, int i3) {
        int abs = Math.abs(i);
        if (abs < i2) {
            return 0;
        }
        return abs > i3 ? i <= 0 ? -i3 : i3 : i;
    }

    /* renamed from: b */
    private void m4015b() {
        this.f3583l.computeCurrentVelocity(1000, this.f3584m);
        m4008a(m4003a(VelocityTrackerCompat.m12857a(this.f3583l, this.f3574c), this.f3585n, this.f3584m), m4003a(VelocityTrackerCompat.m12858b(this.f3583l, this.f3574c), this.f3585n, this.f3584m));
    }

    /* renamed from: b */
    private void m4016b(float f, float f2, int i) {
        int i2 = 0;
        if (m4011a(f, f2, i, 1)) {
            i2 = 0 | 1;
        }
        if (m4011a(f2, f, i, 4)) {
            i2 |= 4;
        }
        if (m4011a(f, f2, i, 2)) {
            i2 |= 2;
        }
        if (m4011a(f2, f, i, 8)) {
            i2 |= 8;
        }
        if (i2 != 0) {
            int[] iArr = this.f3580i;
            iArr[i] = iArr[i] | i2;
            this.f3589r.onEdgeDragStarted(i2, i);
        }
    }

    /* renamed from: b */
    private void m4017b(int i) {
        if (this.f3575d != null && isPointerDown(i)) {
            this.f3575d[i] = 0.0f;
            this.f3576e[i] = 0.0f;
            this.f3577f[i] = 0.0f;
            this.f3578g[i] = 0.0f;
            this.f3579h[i] = 0;
            this.f3580i[i] = 0;
            this.f3581j[i] = 0;
            this.f3582k &= (1 << i) ^ -1;
        }
    }

    /* renamed from: b */
    private void m4018b(int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        int left = this.f3590s.getLeft();
        int top = this.f3590s.getTop();
        if (i3 != 0) {
            i5 = this.f3589r.clampViewPositionHorizontal(this.f3590s, i, i3);
            ViewCompat.m12921f(this.f3590s, i5 - left);
        }
        if (i4 != 0) {
            i6 = this.f3589r.clampViewPositionVertical(this.f3590s, i2, i4);
            ViewCompat.m12918e(this.f3590s, i6 - top);
        }
        if (i3 != 0 || i4 != 0) {
            this.f3589r.onViewPositionChanged(this.f3590s, i5, i6, i5 - left, i6 - top);
        }
    }

    /* renamed from: c */
    private void m4019c(int i) {
        if (this.f3575d == null || this.f3575d.length <= i) {
            float[] fArr = new float[(i + 1)];
            float[] fArr2 = new float[(i + 1)];
            float[] fArr3 = new float[(i + 1)];
            float[] fArr4 = new float[(i + 1)];
            int[] iArr = new int[(i + 1)];
            int[] iArr2 = new int[(i + 1)];
            int[] iArr3 = new int[(i + 1)];
            if (this.f3575d != null) {
                System.arraycopy(this.f3575d, 0, fArr, 0, this.f3575d.length);
                System.arraycopy(this.f3576e, 0, fArr2, 0, this.f3576e.length);
                System.arraycopy(this.f3577f, 0, fArr3, 0, this.f3577f.length);
                System.arraycopy(this.f3578g, 0, fArr4, 0, this.f3578g.length);
                System.arraycopy(this.f3579h, 0, iArr, 0, this.f3579h.length);
                System.arraycopy(this.f3580i, 0, iArr2, 0, this.f3580i.length);
                System.arraycopy(this.f3581j, 0, iArr3, 0, this.f3581j.length);
            }
            this.f3575d = fArr;
            this.f3576e = fArr2;
            this.f3577f = fArr3;
            this.f3578g = fArr4;
            this.f3579h = iArr;
            this.f3580i = iArr2;
            this.f3581j = iArr3;
        }
    }

    public static ViewDragHelper create(ViewGroup viewGroup, float f, Callback callback) {
        ViewDragHelper create = create(viewGroup, callback);
        create.f3573b = (int) (((float) create.f3573b) * (1.0f / f));
        return create;
    }

    public static ViewDragHelper create(ViewGroup viewGroup, Callback callback) {
        return new ViewDragHelper(viewGroup.getContext(), viewGroup, callback);
    }

    /* renamed from: d */
    private boolean m4020d(int i) {
        if (isPointerDown(i)) {
            return true;
        }
        Log.e("ViewDragHelper", "Ignoring pointerId=" + i + " because ACTION_DOWN was not received " + "for this pointer before ACTION_MOVE. It likely happened because " + " ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3476a(int i) {
        this.f3592u.removeCallbacks(this.f3593w);
        if (this.f3572a != i) {
            this.f3572a = i;
            this.f3589r.onViewDragStateChanged(i);
            if (this.f3572a == 0) {
                this.f3590s = null;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo3477a(View view, int i) {
        if (view == this.f3590s && this.f3574c == i) {
            return true;
        }
        if (view == null || !this.f3589r.tryCaptureView(view, i)) {
            return false;
        }
        this.f3574c = i;
        captureChildView(view, i);
        return true;
    }

    public void abort() {
        cancel();
        if (this.f3572a == 2) {
            int currX = this.f3588q.getCurrX();
            int currY = this.f3588q.getCurrY();
            this.f3588q.abortAnimation();
            int currX2 = this.f3588q.getCurrX();
            int currY2 = this.f3588q.getCurrY();
            this.f3589r.onViewPositionChanged(this.f3590s, currX2, currY2, currX2 - currX, currY2 - currY);
        }
        mo3476a(0);
    }

    /* access modifiers changed from: protected */
    public boolean canScroll(View view, boolean z, int i, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (canScroll(childAt, true, i, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z && (ViewCompat.m12901a(view, -i) || ViewCompat.m12908b(view, -i2));
    }

    public void cancel() {
        this.f3574c = -1;
        m4007a();
        if (this.f3583l != null) {
            this.f3583l.recycle();
            this.f3583l = null;
        }
    }

    public void captureChildView(View view, int i) {
        if (view.getParent() != this.f3592u) {
            throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.f3592u + ")");
        }
        this.f3590s = view;
        this.f3574c = i;
        this.f3589r.onViewCaptured(view, i);
        mo3476a(1);
    }

    public boolean checkTouchSlop(int i) {
        int length = this.f3575d.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (checkTouchSlop(i, i2)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkTouchSlop(int i, int i2) {
        if (!isPointerDown(i2)) {
            return false;
        }
        boolean z = (i & 1) == 1;
        boolean z2 = (i & 2) == 2;
        float f = this.f3577f[i2] - this.f3575d[i2];
        float f2 = this.f3578g[i2] - this.f3576e[i2];
        if (z && z2) {
            return (f * f) + (f2 * f2) > ((float) (this.f3573b * this.f3573b));
        }
        if (z) {
            return Math.abs(f) > ((float) this.f3573b);
        }
        if (z2) {
            return Math.abs(f2) > ((float) this.f3573b);
        }
        return false;
    }

    public boolean continueSettling(boolean z) {
        if (this.f3572a == 2) {
            boolean computeScrollOffset = this.f3588q.computeScrollOffset();
            int currX = this.f3588q.getCurrX();
            int currY = this.f3588q.getCurrY();
            int left = currX - this.f3590s.getLeft();
            int top = currY - this.f3590s.getTop();
            if (left != 0) {
                ViewCompat.m12921f(this.f3590s, left);
            }
            if (top != 0) {
                ViewCompat.m12918e(this.f3590s, top);
            }
            if (!(left == 0 && top == 0)) {
                this.f3589r.onViewPositionChanged(this.f3590s, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.f3588q.getFinalX() && currY == this.f3588q.getFinalY()) {
                this.f3588q.abortAnimation();
                computeScrollOffset = false;
            }
            if (!computeScrollOffset) {
                if (z) {
                    this.f3592u.post(this.f3593w);
                } else {
                    mo3476a(0);
                }
            }
        }
        return this.f3572a == 2;
    }

    public View findTopChildUnder(int i, int i2) {
        for (int childCount = this.f3592u.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.f3592u.getChildAt(this.f3589r.getOrderedChildIndex(childCount));
            if (i >= childAt.getLeft() && i < childAt.getRight() && i2 >= childAt.getTop() && i2 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    public void flingCapturedView(int i, int i2, int i3, int i4) {
        if (!this.f3591t) {
            throw new IllegalStateException("Cannot flingCapturedView outside of a call to Callback#onViewReleased");
        }
        this.f3588q.fling(this.f3590s.getLeft(), this.f3590s.getTop(), (int) VelocityTrackerCompat.m12857a(this.f3583l, this.f3574c), (int) VelocityTrackerCompat.m12858b(this.f3583l, this.f3574c), i, i3, i2, i4);
        mo3476a(2);
    }

    public int getActivePointerId() {
        return this.f3574c;
    }

    public View getCapturedView() {
        return this.f3590s;
    }

    public int getEdgeSize() {
        return this.f3586o;
    }

    public float getMinVelocity() {
        return this.f3585n;
    }

    public int getTouchSlop() {
        return this.f3573b;
    }

    public int getViewDragState() {
        return this.f3572a;
    }

    public boolean isCapturedViewUnder(int i, int i2) {
        return isViewUnder(this.f3590s, i, i2);
    }

    public boolean isEdgeTouched(int i) {
        int length = this.f3579h.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (isEdgeTouched(i, i2)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEdgeTouched(int i, int i2) {
        return isPointerDown(i2) && (this.f3579h[i2] & i) != 0;
    }

    public boolean isPointerDown(int i) {
        return (this.f3582k & (1 << i)) != 0;
    }

    public boolean isViewUnder(View view, int i, int i2) {
        return view != null && i >= view.getLeft() && i < view.getRight() && i2 >= view.getTop() && i2 < view.getBottom();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0250, code lost:
        r17 = r22.getX(r8);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processTouchEvent(android.view.MotionEvent r22) {
        /*
            r21 = this;
            int r3 = p000.MotionEventCompat.m12830a(r22)
            int r4 = p000.MotionEventCompat.m12832b(r22)
            if (r3 != 0) goto L_0x000d
            r21.cancel()
        L_0x000d:
            r0 = r21
            android.view.VelocityTracker r0 = r0.f3583l
            r19 = r0
            if (r19 != 0) goto L_0x001f
            android.view.VelocityTracker r19 = android.view.VelocityTracker.obtain()
            r0 = r19
            r1 = r21
            r1.f3583l = r0
        L_0x001f:
            r0 = r21
            android.view.VelocityTracker r0 = r0.f3583l
            r19 = r0
            r0 = r19
            r1 = r22
            r0.addMovement(r1)
            switch(r3) {
                case 0: goto L_0x0030;
                case 1: goto L_0x02a0;
                case 2: goto L_0x011a;
                case 3: goto L_0x02b6;
                case 4: goto L_0x002f;
                case 5: goto L_0x008e;
                case 6: goto L_0x0217;
                default: goto L_0x002f;
            }
        L_0x002f:
            return
        L_0x0030:
            float r17 = r22.getX()
            float r18 = r22.getY()
            r19 = 0
            r0 = r22
            r1 = r19
            int r15 = r0.getPointerId(r1)
            r0 = r17
            int r0 = (int) r0
            r19 = r0
            r0 = r18
            int r0 = (int) r0
            r20 = r0
            r0 = r21
            r1 = r19
            r2 = r20
            android.view.View r16 = r0.findTopChildUnder(r1, r2)
            r0 = r21
            r1 = r17
            r2 = r18
            r0.m4009a(r1, r2, r15)
            r0 = r21
            r1 = r16
            r0.mo3477a(r1, r15)
            r0 = r21
            int[] r0 = r0.f3579h
            r19 = r0
            r7 = r19[r15]
            r0 = r21
            int r0 = r0.f3587p
            r19 = r0
            r19 = r19 & r7
            if (r19 == 0) goto L_0x002f
            r0 = r21
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r19 = r0
            r0 = r21
            int r0 = r0.f3587p
            r20 = r0
            r20 = r20 & r7
            r0 = r19
            r1 = r20
            r0.onEdgeTouched(r1, r15)
            goto L_0x002f
        L_0x008e:
            r0 = r22
            int r15 = r0.getPointerId(r4)
            r0 = r22
            float r17 = r0.getX(r4)
            r0 = r22
            float r18 = r0.getY(r4)
            r0 = r21
            r1 = r17
            r2 = r18
            r0.m4009a(r1, r2, r15)
            r0 = r21
            int r0 = r0.f3572a
            r19 = r0
            if (r19 != 0) goto L_0x00f5
            r0 = r17
            int r0 = (int) r0
            r19 = r0
            r0 = r18
            int r0 = (int) r0
            r20 = r0
            r0 = r21
            r1 = r19
            r2 = r20
            android.view.View r16 = r0.findTopChildUnder(r1, r2)
            r0 = r21
            r1 = r16
            r0.mo3477a(r1, r15)
            r0 = r21
            int[] r0 = r0.f3579h
            r19 = r0
            r7 = r19[r15]
            r0 = r21
            int r0 = r0.f3587p
            r19 = r0
            r19 = r19 & r7
            if (r19 == 0) goto L_0x002f
            r0 = r21
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r19 = r0
            r0 = r21
            int r0 = r0.f3587p
            r20 = r0
            r20 = r20 & r7
            r0 = r19
            r1 = r20
            r0.onEdgeTouched(r1, r15)
            goto L_0x002f
        L_0x00f5:
            r0 = r17
            int r0 = (int) r0
            r19 = r0
            r0 = r18
            int r0 = (int) r0
            r20 = r0
            r0 = r21
            r1 = r19
            r2 = r20
            boolean r19 = r0.isCapturedViewUnder(r1, r2)
            if (r19 == 0) goto L_0x002f
            r0 = r21
            android.view.View r0 = r0.f3590s
            r19 = r0
            r0 = r21
            r1 = r19
            r0.mo3477a(r1, r15)
            goto L_0x002f
        L_0x011a:
            r0 = r21
            int r0 = r0.f3572a
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x019e
            r0 = r21
            int r0 = r0.f3574c
            r19 = r0
            r0 = r21
            r1 = r19
            boolean r19 = r0.m4020d(r1)
            if (r19 == 0) goto L_0x002f
            r0 = r21
            int r0 = r0.f3574c
            r19 = r0
            r0 = r22
            r1 = r19
            int r12 = r0.findPointerIndex(r1)
            r0 = r22
            float r17 = r0.getX(r12)
            r0 = r22
            float r18 = r0.getY(r12)
            r0 = r21
            float[] r0 = r0.f3577f
            r19 = r0
            r0 = r21
            int r0 = r0.f3574c
            r20 = r0
            r19 = r19[r20]
            float r19 = r17 - r19
            r0 = r19
            int r10 = (int) r0
            r0 = r21
            float[] r0 = r0.f3578g
            r19 = r0
            r0 = r21
            int r0 = r0.f3574c
            r20 = r0
            r19 = r19[r20]
            float r19 = r18 - r19
            r0 = r19
            int r11 = (int) r0
            r0 = r21
            android.view.View r0 = r0.f3590s
            r19 = r0
            int r19 = r19.getLeft()
            int r19 = r19 + r10
            r0 = r21
            android.view.View r0 = r0.f3590s
            r20 = r0
            int r20 = r20.getTop()
            int r20 = r20 + r11
            r0 = r21
            r1 = r19
            r2 = r20
            r0.m4018b(r1, r2, r10, r11)
            r21.m4010a(r22)
            goto L_0x002f
        L_0x019e:
            int r14 = r22.getPointerCount()
            r8 = 0
        L_0x01a3:
            if (r8 >= r14) goto L_0x01e9
            r0 = r22
            int r15 = r0.getPointerId(r8)
            r0 = r21
            boolean r19 = r0.m4020d(r15)
            if (r19 != 0) goto L_0x01b6
        L_0x01b3:
            int r8 = r8 + 1
            goto L_0x01a3
        L_0x01b6:
            r0 = r22
            float r17 = r0.getX(r8)
            r0 = r22
            float r18 = r0.getY(r8)
            r0 = r21
            float[] r0 = r0.f3575d
            r19 = r0
            r19 = r19[r15]
            float r5 = r17 - r19
            r0 = r21
            float[] r0 = r0.f3576e
            r19 = r0
            r19 = r19[r15]
            float r6 = r18 - r19
            r0 = r21
            r0.m4016b(r5, r6, r15)
            r0 = r21
            int r0 = r0.f3572a
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x01ee
        L_0x01e9:
            r21.m4010a(r22)
            goto L_0x002f
        L_0x01ee:
            r0 = r17
            int r0 = (int) r0
            r19 = r0
            r0 = r18
            int r0 = (int) r0
            r20 = r0
            r0 = r21
            r1 = r19
            r2 = r20
            android.view.View r16 = r0.findTopChildUnder(r1, r2)
            r0 = r21
            r1 = r16
            boolean r19 = r0.m4013a(r1, r5, r6)
            if (r19 == 0) goto L_0x01b3
            r0 = r21
            r1 = r16
            boolean r19 = r0.mo3477a(r1, r15)
            if (r19 == 0) goto L_0x01b3
            goto L_0x01e9
        L_0x0217:
            r0 = r22
            int r15 = r0.getPointerId(r4)
            r0 = r21
            int r0 = r0.f3572a
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x0299
            r0 = r21
            int r0 = r0.f3574c
            r19 = r0
            r0 = r19
            if (r15 != r0) goto L_0x0299
            r13 = -1
            int r14 = r22.getPointerCount()
            r8 = 0
        L_0x023b:
            if (r8 >= r14) goto L_0x0290
            r0 = r22
            int r9 = r0.getPointerId(r8)
            r0 = r21
            int r0 = r0.f3574c
            r19 = r0
            r0 = r19
            if (r9 != r0) goto L_0x0250
        L_0x024d:
            int r8 = r8 + 1
            goto L_0x023b
        L_0x0250:
            r0 = r22
            float r17 = r0.getX(r8)
            r0 = r22
            float r18 = r0.getY(r8)
            r0 = r17
            int r0 = (int) r0
            r19 = r0
            r0 = r18
            int r0 = (int) r0
            r20 = r0
            r0 = r21
            r1 = r19
            r2 = r20
            android.view.View r19 = r0.findTopChildUnder(r1, r2)
            r0 = r21
            android.view.View r0 = r0.f3590s
            r20 = r0
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x024d
            r0 = r21
            android.view.View r0 = r0.f3590s
            r19 = r0
            r0 = r21
            r1 = r19
            boolean r19 = r0.mo3477a(r1, r9)
            if (r19 == 0) goto L_0x024d
            r0 = r21
            int r13 = r0.f3574c
        L_0x0290:
            r19 = -1
            r0 = r19
            if (r13 != r0) goto L_0x0299
            r21.m4015b()
        L_0x0299:
            r0 = r21
            r0.m4017b(r15)
            goto L_0x002f
        L_0x02a0:
            r0 = r21
            int r0 = r0.f3572a
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x02b1
            r21.m4015b()
        L_0x02b1:
            r21.cancel()
            goto L_0x002f
        L_0x02b6:
            r0 = r21
            int r0 = r0.f3572a
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x02d1
            r19 = 0
            r20 = 0
            r0 = r21
            r1 = r19
            r2 = r20
            r0.m4008a(r1, r2)
        L_0x02d1:
            r21.cancel()
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.p001v4.widget.ViewDragHelper.processTouchEvent(android.view.MotionEvent):void");
    }

    public void setEdgeTrackingEnabled(int i) {
        this.f3587p = i;
    }

    public void setMinVelocity(float f) {
        this.f3585n = f;
    }

    public boolean settleCapturedViewAt(int i, int i2) {
        if (this.f3591t) {
            return m4012a(i, i2, (int) VelocityTrackerCompat.m12857a(this.f3583l, this.f3574c), (int) VelocityTrackerCompat.m12858b(this.f3583l, this.f3574c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0213, code lost:
        if (r11 != r13) goto L_0x0222;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldInterceptTouchEvent(android.view.MotionEvent r27) {
        /*
            r26 = this;
            int r4 = p000.MotionEventCompat.m12830a(r27)
            int r5 = p000.MotionEventCompat.m12832b(r27)
            if (r4 != 0) goto L_0x000d
            r26.cancel()
        L_0x000d:
            r0 = r26
            android.view.VelocityTracker r0 = r0.f3583l
            r24 = r0
            if (r24 != 0) goto L_0x001f
            android.view.VelocityTracker r24 = android.view.VelocityTracker.obtain()
            r0 = r24
            r1 = r26
            r1.f3583l = r0
        L_0x001f:
            r0 = r26
            android.view.VelocityTracker r0 = r0.f3583l
            r24 = r0
            r0 = r24
            r1 = r27
            r0.addMovement(r1)
            switch(r4) {
                case 0: goto L_0x0040;
                case 1: goto L_0x0255;
                case 2: goto L_0x0148;
                case 3: goto L_0x0255;
                case 4: goto L_0x002f;
                case 5: goto L_0x00bf;
                case 6: goto L_0x0246;
                default: goto L_0x002f;
            }
        L_0x002f:
            r0 = r26
            int r0 = r0.f3572a
            r24 = r0
            r25 = 1
            r0 = r24
            r1 = r25
            if (r0 != r1) goto L_0x025a
            r24 = 1
        L_0x003f:
            return r24
        L_0x0040:
            float r22 = r27.getX()
            float r23 = r27.getY()
            r24 = 0
            r0 = r27
            r1 = r24
            int r17 = r0.getPointerId(r1)
            r0 = r26
            r1 = r22
            r2 = r23
            r3 = r17
            r0.m4009a(r1, r2, r3)
            r0 = r22
            int r0 = (int) r0
            r24 = r0
            r0 = r23
            int r0 = (int) r0
            r25 = r0
            r0 = r26
            r1 = r24
            r2 = r25
            android.view.View r20 = r0.findTopChildUnder(r1, r2)
            r0 = r26
            android.view.View r0 = r0.f3590s
            r24 = r0
            r0 = r20
            r1 = r24
            if (r0 != r1) goto L_0x0094
            r0 = r26
            int r0 = r0.f3572a
            r24 = r0
            r25 = 2
            r0 = r24
            r1 = r25
            if (r0 != r1) goto L_0x0094
            r0 = r26
            r1 = r20
            r2 = r17
            r0.mo3477a(r1, r2)
        L_0x0094:
            r0 = r26
            int[] r0 = r0.f3579h
            r24 = r0
            r8 = r24[r17]
            r0 = r26
            int r0 = r0.f3587p
            r24 = r0
            r24 = r24 & r8
            if (r24 == 0) goto L_0x002f
            r0 = r26
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r24 = r0
            r0 = r26
            int r0 = r0.f3587p
            r25 = r0
            r25 = r25 & r8
            r0 = r24
            r1 = r25
            r2 = r17
            r0.onEdgeTouched(r1, r2)
            goto L_0x002f
        L_0x00bf:
            r0 = r27
            int r17 = r0.getPointerId(r5)
            r0 = r27
            float r22 = r0.getX(r5)
            r0 = r27
            float r23 = r0.getY(r5)
            r0 = r26
            r1 = r22
            r2 = r23
            r3 = r17
            r0.m4009a(r1, r2, r3)
            r0 = r26
            int r0 = r0.f3572a
            r24 = r0
            if (r24 != 0) goto L_0x010f
            r0 = r26
            int[] r0 = r0.f3579h
            r24 = r0
            r8 = r24[r17]
            r0 = r26
            int r0 = r0.f3587p
            r24 = r0
            r24 = r24 & r8
            if (r24 == 0) goto L_0x002f
            r0 = r26
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r24 = r0
            r0 = r26
            int r0 = r0.f3587p
            r25 = r0
            r25 = r25 & r8
            r0 = r24
            r1 = r25
            r2 = r17
            r0.onEdgeTouched(r1, r2)
            goto L_0x002f
        L_0x010f:
            r0 = r26
            int r0 = r0.f3572a
            r24 = r0
            r25 = 2
            r0 = r24
            r1 = r25
            if (r0 != r1) goto L_0x002f
            r0 = r22
            int r0 = (int) r0
            r24 = r0
            r0 = r23
            int r0 = (int) r0
            r25 = r0
            r0 = r26
            r1 = r24
            r2 = r25
            android.view.View r20 = r0.findTopChildUnder(r1, r2)
            r0 = r26
            android.view.View r0 = r0.f3590s
            r24 = r0
            r0 = r20
            r1 = r24
            if (r0 != r1) goto L_0x002f
            r0 = r26
            r1 = r20
            r2 = r17
            r0.mo3477a(r1, r2)
            goto L_0x002f
        L_0x0148:
            r0 = r26
            float[] r0 = r0.f3575d
            r24 = r0
            if (r24 == 0) goto L_0x002f
            r0 = r26
            float[] r0 = r0.f3576e
            r24 = r0
            if (r24 == 0) goto L_0x002f
            int r16 = r27.getPointerCount()
            r10 = 0
        L_0x015d:
            r0 = r16
            if (r10 >= r0) goto L_0x021b
            r0 = r27
            int r17 = r0.getPointerId(r10)
            r0 = r26
            r1 = r17
            boolean r24 = r0.m4020d(r1)
            if (r24 != 0) goto L_0x0174
        L_0x0171:
            int r10 = r10 + 1
            goto L_0x015d
        L_0x0174:
            r0 = r27
            float r22 = r0.getX(r10)
            r0 = r27
            float r23 = r0.getY(r10)
            r0 = r26
            float[] r0 = r0.f3575d
            r24 = r0
            r24 = r24[r17]
            float r6 = r22 - r24
            r0 = r26
            float[] r0 = r0.f3576e
            r24 = r0
            r24 = r24[r17]
            float r7 = r23 - r24
            r0 = r22
            int r0 = (int) r0
            r24 = r0
            r0 = r23
            int r0 = (int) r0
            r25 = r0
            r0 = r26
            r1 = r24
            r2 = r25
            android.view.View r20 = r0.findTopChildUnder(r1, r2)
            if (r20 == 0) goto L_0x0220
            r0 = r26
            r1 = r20
            boolean r24 = r0.m4013a(r1, r6, r7)
            if (r24 == 0) goto L_0x0220
            r15 = 1
        L_0x01b5:
            if (r15 == 0) goto L_0x0222
            int r13 = r20.getLeft()
            int r0 = (int) r6
            r24 = r0
            int r18 = r13 + r24
            r0 = r26
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r24 = r0
            int r0 = (int) r6
            r25 = r0
            r0 = r24
            r1 = r20
            r2 = r18
            r3 = r25
            int r11 = r0.clampViewPositionHorizontal(r1, r2, r3)
            int r14 = r20.getTop()
            int r0 = (int) r7
            r24 = r0
            int r19 = r14 + r24
            r0 = r26
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r24 = r0
            int r0 = (int) r7
            r25 = r0
            r0 = r24
            r1 = r20
            r2 = r19
            r3 = r25
            int r12 = r0.clampViewPositionVertical(r1, r2, r3)
            r0 = r26
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r24 = r0
            r0 = r24
            r1 = r20
            int r9 = r0.getViewHorizontalDragRange(r1)
            r0 = r26
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.f3589r
            r24 = r0
            r0 = r24
            r1 = r20
            int r21 = r0.getViewVerticalDragRange(r1)
            if (r9 == 0) goto L_0x0215
            if (r9 <= 0) goto L_0x0222
            if (r11 != r13) goto L_0x0222
        L_0x0215:
            if (r21 == 0) goto L_0x021b
            if (r21 <= 0) goto L_0x0222
            if (r12 != r14) goto L_0x0222
        L_0x021b:
            r26.m4010a(r27)
            goto L_0x002f
        L_0x0220:
            r15 = 0
            goto L_0x01b5
        L_0x0222:
            r0 = r26
            r1 = r17
            r0.m4016b(r6, r7, r1)
            r0 = r26
            int r0 = r0.f3572a
            r24 = r0
            r25 = 1
            r0 = r24
            r1 = r25
            if (r0 == r1) goto L_0x021b
            if (r15 == 0) goto L_0x0171
            r0 = r26
            r1 = r20
            r2 = r17
            boolean r24 = r0.mo3477a(r1, r2)
            if (r24 == 0) goto L_0x0171
            goto L_0x021b
        L_0x0246:
            r0 = r27
            int r17 = r0.getPointerId(r5)
            r0 = r26
            r1 = r17
            r0.m4017b(r1)
            goto L_0x002f
        L_0x0255:
            r26.cancel()
            goto L_0x002f
        L_0x025a:
            r24 = 0
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.p001v4.widget.ViewDragHelper.shouldInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    public boolean smoothSlideViewTo(View view, int i, int i2) {
        this.f3590s = view;
        this.f3574c = -1;
        boolean a = m4012a(i, i2, 0, 0);
        if (!a && this.f3572a == 0 && this.f3590s != null) {
            this.f3590s = null;
        }
        return a;
    }
}
