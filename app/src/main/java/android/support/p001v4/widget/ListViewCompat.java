package android.support.p001v4.widget;

import android.os.Build.VERSION;
import android.widget.ListView;

/* renamed from: android.support.v4.widget.ListViewCompat */
public final class ListViewCompat {
    private ListViewCompat() {
    }

    public static void scrollListBy(ListView listView, int i) {
        if (VERSION.SDK_INT >= 19) {
            ListViewCompatKitKat.m3918a(listView, i);
        } else {
            ListViewCompatGingerbread.m3917a(listView, i);
        }
    }
}
