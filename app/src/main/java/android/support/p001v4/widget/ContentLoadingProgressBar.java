package android.support.p001v4.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/* renamed from: android.support.v4.widget.ContentLoadingProgressBar */
public class ContentLoadingProgressBar extends ProgressBar {

    /* renamed from: a */
    long f3300a;

    /* renamed from: b */
    boolean f3301b;

    /* renamed from: c */
    boolean f3302c;

    /* renamed from: d */
    boolean f3303d;

    /* renamed from: e */
    private final Runnable f3304e;

    /* renamed from: f */
    private final Runnable f3305f;

    public ContentLoadingProgressBar(Context context) {
        this(context, null);
    }

    public ContentLoadingProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        this.f3300a = -1;
        this.f3301b = false;
        this.f3302c = false;
        this.f3303d = false;
        this.f3304e = new Runnable() {
            public void run() {
                ContentLoadingProgressBar.this.f3301b = false;
                ContentLoadingProgressBar.this.f3300a = -1;
                ContentLoadingProgressBar.this.setVisibility(8);
            }
        };
        this.f3305f = new Runnable() {
            public void run() {
                ContentLoadingProgressBar.this.f3302c = false;
                if (!ContentLoadingProgressBar.this.f3303d) {
                    ContentLoadingProgressBar.this.f3300a = System.currentTimeMillis();
                    ContentLoadingProgressBar.this.setVisibility(0);
                }
            }
        };
    }

    /* renamed from: a */
    private void m3852a() {
        removeCallbacks(this.f3304e);
        removeCallbacks(this.f3305f);
    }

    public void hide() {
        this.f3303d = true;
        removeCallbacks(this.f3305f);
        long currentTimeMillis = System.currentTimeMillis() - this.f3300a;
        if (currentTimeMillis >= 500 || this.f3300a == -1) {
            setVisibility(8);
        } else if (!this.f3301b) {
            postDelayed(this.f3304e, 500 - currentTimeMillis);
            this.f3301b = true;
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        m3852a();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m3852a();
    }

    public void show() {
        this.f3300a = -1;
        this.f3303d = false;
        removeCallbacks(this.f3304e);
        if (!this.f3302c) {
            postDelayed(this.f3305f, 500);
            this.f3302c = true;
        }
    }
}
