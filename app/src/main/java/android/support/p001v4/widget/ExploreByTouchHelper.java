package android.support.p001v4.widget;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.p001v4.app.NotificationCompat;
import android.support.p001v4.widget.FocusStrategy.BoundsAdapter;
import android.support.p001v4.widget.FocusStrategy.CollectionAdapter;
import android.support.p003v7.widget.RecyclerView.ItemAnimator;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.widget.ExploreByTouchHelper */
public abstract class ExploreByTouchHelper extends AccessibilityDelegateCompat {
    public static final int HOST_ID = -1;
    public static final int INVALID_ID = Integer.MIN_VALUE;

    /* renamed from: a */
    private static final Rect f3369a = new Rect(ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, Integer.MIN_VALUE, Integer.MIN_VALUE);

    /* renamed from: l */
    private static final BoundsAdapter<AccessibilityNodeInfoCompat> f3370l = new BoundsAdapter<AccessibilityNodeInfoCompat>() {
        public void obtainBounds(AccessibilityNodeInfoCompat ggVar, Rect rect) {
            ggVar.mo13586a(rect);
        }
    };

    /* renamed from: m */
    private static final CollectionAdapter<SparseArrayCompat<AccessibilityNodeInfoCompat>, AccessibilityNodeInfoCompat> f3371m = new CollectionAdapter<SparseArrayCompat<AccessibilityNodeInfoCompat>, AccessibilityNodeInfoCompat>() {
        public AccessibilityNodeInfoCompat get(SparseArrayCompat<AccessibilityNodeInfoCompat> duVar, int i) {
            return (AccessibilityNodeInfoCompat) duVar.mo13330f(i);
        }

        public int size(SparseArrayCompat<AccessibilityNodeInfoCompat> duVar) {
            return duVar.mo13322b();
        }
    };

    /* renamed from: b */
    private final Rect f3372b = new Rect();

    /* renamed from: c */
    private final Rect f3373c = new Rect();

    /* renamed from: d */
    private final Rect f3374d = new Rect();

    /* renamed from: e */
    private final int[] f3375e = new int[2];

    /* renamed from: f */
    private final AccessibilityManager f3376f;

    /* renamed from: g */
    private final View f3377g;

    /* renamed from: h */
    private MyNodeProvider f3378h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public int f3379i = Integer.MIN_VALUE;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public int f3380j = Integer.MIN_VALUE;

    /* renamed from: k */
    private int f3381k = Integer.MIN_VALUE;

    /* renamed from: android.support.v4.widget.ExploreByTouchHelper$MyNodeProvider */
    class MyNodeProvider extends AccessibilityNodeProviderCompat {
        MyNodeProvider() {
        }

        public AccessibilityNodeInfoCompat createAccessibilityNodeInfo(int i) {
            return AccessibilityNodeInfoCompat.m13414a(ExploreByTouchHelper.this.mo3133a(i));
        }

        public AccessibilityNodeInfoCompat findFocus(int i) {
            int b = i == 2 ? ExploreByTouchHelper.this.f3379i : ExploreByTouchHelper.this.f3380j;
            if (b == Integer.MIN_VALUE) {
                return null;
            }
            return createAccessibilityNodeInfo(b);
        }

        public boolean performAction(int i, int i2, Bundle bundle) {
            return ExploreByTouchHelper.this.mo3134a(i, i2, bundle);
        }
    }

    public ExploreByTouchHelper(View view) {
        if (view == null) {
            throw new IllegalArgumentException("View may not be null");
        }
        this.f3377g = view;
        this.f3376f = (AccessibilityManager) view.getContext().getSystemService("accessibility");
        view.setFocusable(true);
        if (ViewCompat.m12916e(view) == 0) {
            ViewCompat.m12910c(view, 1);
        }
    }

    /* renamed from: a */
    private static Rect m3884a(View view, int i, Rect rect) {
        int width = view.getWidth();
        int height = view.getHeight();
        switch (i) {
            case 17:
                rect.set(width, 0, width, height);
                break;
            case 33:
                rect.set(0, height, width, height);
                break;
            case 66:
                rect.set(-1, 0, -1, height);
                break;
            case 130:
                rect.set(0, -1, width, -1);
                break;
            default:
                throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return rect;
    }

    /* renamed from: a */
    private AccessibilityEvent m3885a(int i, int i2) {
        switch (i) {
            case -1:
                return m3898d(i2);
            default:
                return m3892b(i, i2);
        }
    }

    /* renamed from: a */
    private SparseArrayCompat<AccessibilityNodeInfoCompat> m3886a() {
        ArrayList arrayList = new ArrayList();
        getVisibleVirtualViews(arrayList);
        SparseArrayCompat<AccessibilityNodeInfoCompat> duVar = new SparseArrayCompat<>();
        for (int i = 0; i < arrayList.size(); i++) {
            duVar.mo13324b(i, m3899e(i));
        }
        return duVar;
    }

    /* renamed from: a */
    private void m3887a(int i, Rect rect) {
        mo3133a(i).mo13586a(rect);
    }

    /* renamed from: a */
    private boolean m3888a(int i, Bundle bundle) {
        return ViewCompat.m12902a(this.f3377g, i, bundle);
    }

    /* renamed from: a */
    private boolean m3889a(Rect rect) {
        if (rect == null || rect.isEmpty() || this.f3377g.getWindowVisibility() != 0) {
            return false;
        }
        ViewParent parent = this.f3377g.getParent();
        while (parent instanceof View) {
            View view = (View) parent;
            if (ViewCompat.m12919f(view) <= 0.0f || view.getVisibility() != 0) {
                return false;
            }
            parent = view.getParent();
        }
        if (parent == null || !this.f3377g.getLocalVisibleRect(this.f3374d)) {
            return false;
        }
        return rect.intersect(this.f3374d);
    }

    /* renamed from: b */
    private static int m3890b(int i) {
        switch (i) {
            case 19:
                return 33;
            case 21:
                return 17;
            case 22:
                return 66;
            default:
                return 130;
        }
    }

    /* renamed from: b */
    private AccessibilityEvent m3892b(int i, int i2) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        AccessibilityRecordCompat a = AccessibilityEventCompat.m13397a(obtain);
        AccessibilityNodeInfoCompat a2 = mo3133a(i);
        a.mo13721a().add(a2.mo13634t());
        a.mo13727b(a2.mo13636u());
        a.mo13732d(a2.mo13631q());
        a.mo13730c(a2.mo13630p());
        a.mo13728b(a2.mo13629o());
        a.mo13725a(a2.mo13617g());
        onPopulateEventForVirtualView(i, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            a.mo13724a(a2.mo13633s());
            a.mo13723a(this.f3377g, i);
            obtain.setPackageName(this.f3377g.getContext().getPackageName());
            return obtain;
        }
        throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    /* renamed from: b */
    private boolean m3893b() {
        return this.f3380j != Integer.MIN_VALUE && onPerformActionForVirtualView(this.f3380j, 16, null);
    }

    /* renamed from: b */
    private boolean m3894b(int i, int i2, Bundle bundle) {
        switch (i2) {
            case 1:
                return requestKeyboardFocusForVirtualView(i);
            case 2:
                return clearKeyboardFocusForVirtualView(i);
            case 64:
                return m3900f(i);
            case NotificationCompat.FLAG_HIGH_PRIORITY /*128*/:
                return m3901g(i);
            default:
                return onPerformActionForVirtualView(i, i2, bundle);
        }
    }

    /* renamed from: b */
    private boolean m3895b(int i, Rect rect) {
        AccessibilityNodeInfoCompat ggVar;
        SparseArrayCompat a = m3886a();
        int i2 = this.f3380j;
        AccessibilityNodeInfoCompat ggVar2 = i2 == Integer.MIN_VALUE ? null : (AccessibilityNodeInfoCompat) a.mo13320a(i2);
        switch (i) {
            case 1:
            case 2:
                ggVar = (AccessibilityNodeInfoCompat) FocusStrategy.findNextFocusInRelativeDirection(a, f3371m, f3370l, ggVar2, i, ViewCompat.m12923h(this.f3377g) == 1, false);
                break;
            case 17:
            case 33:
            case 66:
            case 130:
                Rect rect2 = new Rect();
                if (this.f3380j != Integer.MIN_VALUE) {
                    m3887a(this.f3380j, rect2);
                } else if (rect != null) {
                    rect2.set(rect);
                } else {
                    m3884a(this.f3377g, i, rect2);
                }
                ggVar = (AccessibilityNodeInfoCompat) FocusStrategy.findNextFocusInAbsoluteDirection(a, f3371m, f3370l, ggVar2, rect2, i);
                break;
            default:
                throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return requestKeyboardFocusForVirtualView(ggVar == null ? Integer.MIN_VALUE : a.mo13329e(a.mo13318a(ggVar)));
    }

    /* renamed from: c */
    private AccessibilityNodeInfoCompat m3896c() {
        AccessibilityNodeInfoCompat a = AccessibilityNodeInfoCompat.m13413a(this.f3377g);
        ViewCompat.m12896a(this.f3377g, a);
        ArrayList arrayList = new ArrayList();
        getVisibleVirtualViews(arrayList);
        if (a.mo13598c() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                a.mo13594b(this.f3377g, ((Integer) arrayList.get(i)).intValue());
            }
            return a;
        }
        throw new RuntimeException("Views cannot have both real and virtual children");
    }

    /* renamed from: c */
    private void m3897c(int i) {
        if (this.f3381k != i) {
            int i2 = this.f3381k;
            this.f3381k = i;
            sendEventForVirtualView(i, NotificationCompat.FLAG_HIGH_PRIORITY);
            sendEventForVirtualView(i2, NotificationCompat.FLAG_LOCAL_ONLY);
        }
    }

    /* renamed from: d */
    private AccessibilityEvent m3898d(int i) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
        ViewCompat.m12893a(this.f3377g, obtain);
        return obtain;
    }

    /* renamed from: e */
    private AccessibilityNodeInfoCompat m3899e(int i) {
        AccessibilityNodeInfoCompat b = AccessibilityNodeInfoCompat.m13416b();
        b.mo13618h(true);
        b.mo13589a(true);
        b.mo13595b((CharSequence) "android.view.View");
        b.mo13592b(f3369a);
        b.mo13605d(f3369a);
        onPopulateNodeForVirtualView(i, b);
        if (b.mo13634t() == null && b.mo13636u() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        b.mo13586a(this.f3373c);
        if (this.f3373c.equals(f3369a)) {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
        int d = b.mo13604d();
        if ((d & 64) != 0) {
            throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        } else if ((d & NotificationCompat.FLAG_HIGH_PRIORITY) != 0) {
            throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        } else {
            b.mo13588a((CharSequence) this.f3377g.getContext().getPackageName());
            b.mo13587a(this.f3377g, i);
            b.mo13606d(this.f3377g);
            if (this.f3379i == i) {
                b.mo13608d(true);
                b.mo13585a((int) NotificationCompat.FLAG_HIGH_PRIORITY);
            } else {
                b.mo13608d(false);
                b.mo13585a(64);
            }
            boolean z = this.f3380j == i;
            if (z) {
                b.mo13585a(2);
            } else if (b.mo13619h()) {
                b.mo13585a(1);
            }
            b.mo13597b(z);
            if (m3889a(this.f3373c)) {
                b.mo13603c(true);
                b.mo13592b(this.f3373c);
            }
            b.mo13599c(this.f3372b);
            if (this.f3372b.equals(f3369a)) {
                this.f3377g.getLocationOnScreen(this.f3375e);
                b.mo13586a(this.f3372b);
                this.f3372b.offset(this.f3375e[0] - this.f3377g.getScrollX(), this.f3375e[1] - this.f3377g.getScrollY());
                b.mo13605d(this.f3372b);
            }
            return b;
        }
    }

    /* renamed from: f */
    private boolean m3900f(int i) {
        if (!this.f3376f.isEnabled() || !AccessibilityManagerCompat.m13408a(this.f3376f) || this.f3379i == i) {
            return false;
        }
        if (this.f3379i != Integer.MIN_VALUE) {
            m3901g(this.f3379i);
        }
        this.f3379i = i;
        this.f3377g.invalidate();
        sendEventForVirtualView(i, 32768);
        return true;
    }

    /* renamed from: g */
    private boolean m3901g(int i) {
        if (this.f3379i != i) {
            return false;
        }
        this.f3379i = Integer.MIN_VALUE;
        this.f3377g.invalidate();
        sendEventForVirtualView(i, 65536);
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public AccessibilityNodeInfoCompat mo3133a(int i) {
        return i == -1 ? m3896c() : m3899e(i);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo3134a(int i, int i2, Bundle bundle) {
        switch (i) {
            case -1:
                return m3888a(i2, bundle);
            default:
                return m3894b(i, i2, bundle);
        }
    }

    public final boolean clearKeyboardFocusForVirtualView(int i) {
        if (this.f3380j != i) {
            return false;
        }
        this.f3380j = Integer.MIN_VALUE;
        onVirtualViewKeyboardFocusChanged(i, false);
        sendEventForVirtualView(i, 8);
        return true;
    }

    public final boolean dispatchHoverEvent(MotionEvent motionEvent) {
        boolean z = true;
        if (!this.f3376f.isEnabled() || !AccessibilityManagerCompat.m13408a(this.f3376f)) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 7:
            case 9:
                int virtualViewAt = getVirtualViewAt(motionEvent.getX(), motionEvent.getY());
                m3897c(virtualViewAt);
                if (virtualViewAt == Integer.MIN_VALUE) {
                    z = false;
                }
                return z;
            case 10:
                if (this.f3379i == Integer.MIN_VALUE) {
                    return false;
                }
                m3897c(Integer.MIN_VALUE);
                return true;
            default:
                return false;
        }
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        boolean z = false;
        if (keyEvent.getAction() == 1) {
            return false;
        }
        int keyCode = keyEvent.getKeyCode();
        switch (keyCode) {
            case 19:
            case 20:
            case 21:
            case 22:
                if (!KeyEventCompat.m12730a(keyEvent)) {
                    return false;
                }
                int b = m3890b(keyCode);
                int repeatCount = keyEvent.getRepeatCount() + 1;
                for (int i = 0; i < repeatCount && m3895b(b, (Rect) null); i++) {
                    z = true;
                }
                return z;
            case 23:
            case 66:
                if (!KeyEventCompat.m12730a(keyEvent) || keyEvent.getRepeatCount() != 0) {
                    return false;
                }
                m3893b();
                return true;
            case 61:
                if (KeyEventCompat.m12730a(keyEvent)) {
                    return m3895b(2, (Rect) null);
                }
                if (KeyEventCompat.m12731a(keyEvent, 1)) {
                    return m3895b(1, (Rect) null);
                }
                return false;
            default:
                return false;
        }
    }

    public final int getAccessibilityFocusedVirtualViewId() {
        return this.f3379i;
    }

    public AccessibilityNodeProviderCompat getAccessibilityNodeProvider(View view) {
        if (this.f3378h == null) {
            this.f3378h = new MyNodeProvider();
        }
        return this.f3378h;
    }

    @Deprecated
    public int getFocusedVirtualView() {
        return getAccessibilityFocusedVirtualViewId();
    }

    public final int getKeyboardFocusedVirtualViewId() {
        return this.f3380j;
    }

    /* access modifiers changed from: protected */
    public abstract int getVirtualViewAt(float f, float f2);

    /* access modifiers changed from: protected */
    public abstract void getVisibleVirtualViews(List<Integer> list);

    public final void invalidateRoot() {
        invalidateVirtualView(-1, 1);
    }

    public final void invalidateVirtualView(int i) {
        invalidateVirtualView(i, 0);
    }

    public final void invalidateVirtualView(int i, int i2) {
        if (i != Integer.MIN_VALUE && this.f3376f.isEnabled()) {
            ViewParent parent = this.f3377g.getParent();
            if (parent != null) {
                AccessibilityEvent a = m3885a(i, (int) ItemAnimator.FLAG_MOVED);
                AccessibilityEventCompat.m13398a(a, i2);
                ViewParentCompat.m13266a(parent, this.f3377g, a);
            }
        }
    }

    public final void onFocusChanged(boolean z, int i, Rect rect) {
        if (this.f3380j != Integer.MIN_VALUE) {
            clearKeyboardFocusForVirtualView(this.f3380j);
        }
        if (z) {
            m3895b(i, rect);
        }
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        onPopulateEventForHost(accessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat ggVar) {
        super.onInitializeAccessibilityNodeInfo(view, ggVar);
        onPopulateNodeForHost(ggVar);
    }

    /* access modifiers changed from: protected */
    public abstract boolean onPerformActionForVirtualView(int i, int i2, Bundle bundle);

    /* access modifiers changed from: protected */
    public void onPopulateEventForHost(AccessibilityEvent accessibilityEvent) {
    }

    /* access modifiers changed from: protected */
    public void onPopulateEventForVirtualView(int i, AccessibilityEvent accessibilityEvent) {
    }

    /* access modifiers changed from: protected */
    public void onPopulateNodeForHost(AccessibilityNodeInfoCompat ggVar) {
    }

    /* access modifiers changed from: protected */
    public abstract void onPopulateNodeForVirtualView(int i, AccessibilityNodeInfoCompat ggVar);

    /* access modifiers changed from: protected */
    public void onVirtualViewKeyboardFocusChanged(int i, boolean z) {
    }

    public final boolean requestKeyboardFocusForVirtualView(int i) {
        if ((!this.f3377g.isFocused() && !this.f3377g.requestFocus()) || this.f3380j == i) {
            return false;
        }
        if (this.f3380j != Integer.MIN_VALUE) {
            clearKeyboardFocusForVirtualView(this.f3380j);
        }
        this.f3380j = i;
        onVirtualViewKeyboardFocusChanged(i, true);
        sendEventForVirtualView(i, 8);
        return true;
    }

    public final boolean sendEventForVirtualView(int i, int i2) {
        if (i == Integer.MIN_VALUE || !this.f3376f.isEnabled()) {
            return false;
        }
        ViewParent parent = this.f3377g.getParent();
        if (parent == null) {
            return false;
        }
        return ViewParentCompat.m13266a(parent, this.f3377g, m3885a(i, i2));
    }
}
