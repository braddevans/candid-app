package android.support.p001v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.support.p001v4.view.AbsSavedState;
import android.support.p001v4.widget.ViewDragHelper.Callback;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.widget.DrawerLayout */
public class DrawerLayout extends ViewGroup implements DrawerLayoutImpl {
    public static final int LOCK_MODE_LOCKED_CLOSED = 1;
    public static final int LOCK_MODE_LOCKED_OPEN = 2;
    public static final int LOCK_MODE_UNDEFINED = 3;
    public static final int LOCK_MODE_UNLOCKED = 0;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;

    /* renamed from: a */
    static final int[] f3311a = {16842931};

    /* renamed from: b */
    static final boolean f3312b = (VERSION.SDK_INT >= 19);

    /* renamed from: c */
    static final DrawerLayoutCompatImpl f3313c;

    /* renamed from: d */
    private static final boolean f3314d;

    /* renamed from: A */
    private float f3315A;

    /* renamed from: B */
    private Drawable f3316B;

    /* renamed from: C */
    private Drawable f3317C;

    /* renamed from: D */
    private Drawable f3318D;

    /* renamed from: E */
    private CharSequence f3319E;

    /* renamed from: F */
    private CharSequence f3320F;

    /* renamed from: G */
    private Object f3321G;

    /* renamed from: H */
    private boolean f3322H;

    /* renamed from: I */
    private Drawable f3323I;

    /* renamed from: J */
    private Drawable f3324J;

    /* renamed from: K */
    private Drawable f3325K;

    /* renamed from: L */
    private Drawable f3326L;

    /* renamed from: M */
    private final ArrayList<View> f3327M;

    /* renamed from: e */
    private final ChildAccessibilityDelegate f3328e;

    /* renamed from: f */
    private float f3329f;

    /* renamed from: g */
    private int f3330g;

    /* renamed from: h */
    private int f3331h;

    /* renamed from: i */
    private float f3332i;

    /* renamed from: j */
    private Paint f3333j;

    /* renamed from: k */
    private final ViewDragHelper f3334k;

    /* renamed from: l */
    private final ViewDragHelper f3335l;

    /* renamed from: m */
    private final ViewDragCallback f3336m;

    /* renamed from: n */
    private final ViewDragCallback f3337n;

    /* renamed from: o */
    private int f3338o;

    /* renamed from: p */
    private boolean f3339p;

    /* renamed from: q */
    private boolean f3340q;

    /* renamed from: r */
    private int f3341r;

    /* renamed from: s */
    private int f3342s;

    /* renamed from: t */
    private int f3343t;

    /* renamed from: u */
    private int f3344u;

    /* renamed from: v */
    private boolean f3345v;

    /* renamed from: w */
    private boolean f3346w;

    /* renamed from: x */
    private DrawerListener f3347x;

    /* renamed from: y */
    private List<DrawerListener> f3348y;

    /* renamed from: z */
    private float f3349z;

    /* renamed from: android.support.v4.widget.DrawerLayout$AccessibilityDelegate */
    class AccessibilityDelegate extends AccessibilityDelegateCompat {

        /* renamed from: b */
        private final Rect f3351b = new Rect();

        AccessibilityDelegate() {
        }

        /* renamed from: a */
        private void m3879a(AccessibilityNodeInfoCompat ggVar, ViewGroup viewGroup) {
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (DrawerLayout.m3860g(childAt)) {
                    ggVar.mo13600c(childAt);
                }
            }
        }

        /* renamed from: a */
        private void m3880a(AccessibilityNodeInfoCompat ggVar, AccessibilityNodeInfoCompat ggVar2) {
            Rect rect = this.f3351b;
            ggVar2.mo13586a(rect);
            ggVar.mo13592b(rect);
            ggVar2.mo13599c(rect);
            ggVar.mo13605d(rect);
            ggVar.mo13603c(ggVar2.mo13624j());
            ggVar.mo13588a(ggVar2.mo13632r());
            ggVar.mo13595b(ggVar2.mo13633s());
            ggVar.mo13607d(ggVar2.mo13636u());
            ggVar.mo13618h(ggVar2.mo13629o());
            ggVar.mo13614f(ggVar2.mo13627m());
            ggVar.mo13589a(ggVar2.mo13619h());
            ggVar.mo13597b(ggVar2.mo13622i());
            ggVar.mo13608d(ggVar2.mo13625k());
            ggVar.mo13612e(ggVar2.mo13626l());
            ggVar.mo13616g(ggVar2.mo13628n());
            ggVar.mo13585a(ggVar2.mo13604d());
        }

        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            if (accessibilityEvent.getEventType() != 32) {
                return super.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
            }
            List text = accessibilityEvent.getText();
            View b = DrawerLayout.this.mo3040b();
            if (b != null) {
                CharSequence drawerTitle = DrawerLayout.this.getDrawerTitle(DrawerLayout.this.mo3053d(b));
                if (drawerTitle != null) {
                    text.add(drawerTitle);
                }
            }
            return true;
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(DrawerLayout.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat ggVar) {
            if (DrawerLayout.f3312b) {
                super.onInitializeAccessibilityNodeInfo(view, ggVar);
            } else {
                AccessibilityNodeInfoCompat a = AccessibilityNodeInfoCompat.m13414a(ggVar);
                super.onInitializeAccessibilityNodeInfo(view, a);
                ggVar.mo13593b(view);
                ViewParent i = ViewCompat.m12924i(view);
                if (i instanceof View) {
                    ggVar.mo13606d((View) i);
                }
                m3880a(ggVar, a);
                a.mo13637v();
                m3879a(ggVar, (ViewGroup) view);
            }
            ggVar.mo13595b((CharSequence) DrawerLayout.class.getName());
            ggVar.mo13589a(false);
            ggVar.mo13597b(false);
            ggVar.mo13590a(C2404a.f10038a);
            ggVar.mo13590a(C2404a.f10039b);
        }

        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            if (DrawerLayout.f3312b || DrawerLayout.m3860g(view)) {
                return super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
            }
            return false;
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$ChildAccessibilityDelegate */
    final class ChildAccessibilityDelegate extends AccessibilityDelegateCompat {
        ChildAccessibilityDelegate() {
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat ggVar) {
            super.onInitializeAccessibilityNodeInfo(view, ggVar);
            if (!DrawerLayout.m3860g(view)) {
                ggVar.mo13606d((View) null);
            }
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$DrawerLayoutCompatImpl */
    interface DrawerLayoutCompatImpl {
        void applyMarginInsets(MarginLayoutParams marginLayoutParams, Object obj, int i);

        void configureApplyInsets(View view);

        void dispatchChildInsets(View view, Object obj, int i);

        Drawable getDefaultStatusBarBackground(Context context);

        int getTopInset(Object obj);
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$DrawerLayoutCompatImplApi21 */
    static class DrawerLayoutCompatImplApi21 implements DrawerLayoutCompatImpl {
        DrawerLayoutCompatImplApi21() {
        }

        public void applyMarginInsets(MarginLayoutParams marginLayoutParams, Object obj, int i) {
            DrawerLayoutCompatApi21.applyMarginInsets(marginLayoutParams, obj, i);
        }

        public void configureApplyInsets(View view) {
            DrawerLayoutCompatApi21.configureApplyInsets(view);
        }

        public void dispatchChildInsets(View view, Object obj, int i) {
            DrawerLayoutCompatApi21.dispatchChildInsets(view, obj, i);
        }

        public Drawable getDefaultStatusBarBackground(Context context) {
            return DrawerLayoutCompatApi21.getDefaultStatusBarBackground(context);
        }

        public int getTopInset(Object obj) {
            return DrawerLayoutCompatApi21.getTopInset(obj);
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$DrawerLayoutCompatImplBase */
    static class DrawerLayoutCompatImplBase implements DrawerLayoutCompatImpl {
        DrawerLayoutCompatImplBase() {
        }

        public void applyMarginInsets(MarginLayoutParams marginLayoutParams, Object obj, int i) {
        }

        public void configureApplyInsets(View view) {
        }

        public void dispatchChildInsets(View view, Object obj, int i) {
        }

        public Drawable getDefaultStatusBarBackground(Context context) {
            return null;
        }

        public int getTopInset(Object obj) {
            return 0;
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$DrawerListener */
    public interface DrawerListener {
        void onDrawerClosed(View view);

        void onDrawerOpened(View view);

        void onDrawerSlide(View view, float f);

        void onDrawerStateChanged(int i);
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$LayoutParams */
    public static class LayoutParams extends MarginLayoutParams {

        /* renamed from: a */
        float f3353a;

        /* renamed from: b */
        boolean f3354b;

        /* renamed from: c */
        int f3355c;
        public int gravity;

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.gravity = 0;
        }

        public LayoutParams(int i, int i2, int i3) {
            this(i, i2);
            this.gravity = i3;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.gravity = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.f3311a);
            this.gravity = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = 0;
            this.gravity = layoutParams.gravity;
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = 0;
        }

        public LayoutParams(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.gravity = 0;
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$SavedState */
    public static class SavedState extends AbsSavedState {
        public static final Creator<SavedState> CREATOR = ParcelableCompat.m12486a(new ParcelableCompatCreatorCallbacks<SavedState>() {
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a */
        int f3356a = 0;

        /* renamed from: b */
        int f3357b;

        /* renamed from: c */
        int f3358c;

        /* renamed from: d */
        int f3359d;

        /* renamed from: e */
        int f3360e;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f3356a = parcel.readInt();
            this.f3357b = parcel.readInt();
            this.f3358c = parcel.readInt();
            this.f3359d = parcel.readInt();
            this.f3360e = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f3356a);
            parcel.writeInt(this.f3357b);
            parcel.writeInt(this.f3358c);
            parcel.writeInt(this.f3359d);
            parcel.writeInt(this.f3360e);
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$SimpleDrawerListener */
    public static abstract class SimpleDrawerListener implements DrawerListener {
        public void onDrawerClosed(View view) {
        }

        public void onDrawerOpened(View view) {
        }

        public void onDrawerSlide(View view, float f) {
        }

        public void onDrawerStateChanged(int i) {
        }
    }

    /* renamed from: android.support.v4.widget.DrawerLayout$ViewDragCallback */
    class ViewDragCallback extends Callback {

        /* renamed from: b */
        private final int f3362b;

        /* renamed from: c */
        private ViewDragHelper f3363c;

        /* renamed from: d */
        private final Runnable f3364d = new Runnable() {
            public void run() {
                ViewDragCallback.this.mo3108a();
            }
        };

        ViewDragCallback(int i) {
            this.f3362b = i;
        }

        /* renamed from: b */
        private void m3881b() {
            int i = 3;
            if (this.f3362b == 3) {
                i = 5;
            }
            View a = DrawerLayout.this.mo3031a(i);
            if (a != null) {
                DrawerLayout.this.closeDrawer(a);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo3108a() {
            View a;
            int width;
            int i = 0;
            int edgeSize = this.f3363c.getEdgeSize();
            boolean z = this.f3362b == 3;
            if (z) {
                a = DrawerLayout.this.mo3031a(3);
                if (a != null) {
                    i = -a.getWidth();
                }
                width = i + edgeSize;
            } else {
                a = DrawerLayout.this.mo3031a(5);
                width = DrawerLayout.this.getWidth() - edgeSize;
            }
            if (a == null) {
                return;
            }
            if (((z && a.getLeft() < width) || (!z && a.getLeft() > width)) && DrawerLayout.this.getDrawerLockMode(a) == 0) {
                LayoutParams layoutParams = (LayoutParams) a.getLayoutParams();
                this.f3363c.smoothSlideViewTo(a, width, a.getTop());
                layoutParams.f3354b = true;
                DrawerLayout.this.invalidate();
                m3881b();
                DrawerLayout.this.mo3044c();
            }
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            if (DrawerLayout.this.mo3036a(view, 3)) {
                return Math.max(-view.getWidth(), Math.min(i, 0));
            }
            int width = DrawerLayout.this.getWidth();
            return Math.max(width - view.getWidth(), Math.min(i, width));
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return view.getTop();
        }

        public int getViewHorizontalDragRange(View view) {
            if (DrawerLayout.this.mo3056f(view)) {
                return view.getWidth();
            }
            return 0;
        }

        public void onEdgeDragStarted(int i, int i2) {
            View a = (i & 1) == 1 ? DrawerLayout.this.mo3031a(3) : DrawerLayout.this.mo3031a(5);
            if (a != null && DrawerLayout.this.getDrawerLockMode(a) == 0) {
                this.f3363c.captureChildView(a, i2);
            }
        }

        public boolean onEdgeLock(int i) {
            return false;
        }

        public void onEdgeTouched(int i, int i2) {
            DrawerLayout.this.postDelayed(this.f3364d, 160);
        }

        public void onViewCaptured(View view, int i) {
            ((LayoutParams) view.getLayoutParams()).f3354b = false;
            m3881b();
        }

        public void onViewDragStateChanged(int i) {
            DrawerLayout.this.mo3032a(this.f3362b, i, this.f3363c.getCapturedView());
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            int width = view.getWidth();
            float width2 = DrawerLayout.this.mo3036a(view, 3) ? ((float) (width + i)) / ((float) width) : ((float) (DrawerLayout.this.getWidth() - i)) / ((float) width);
            DrawerLayout.this.mo3042b(view, width2);
            view.setVisibility(width2 == 0.0f ? 4 : 0);
            DrawerLayout.this.invalidate();
        }

        public void onViewReleased(View view, float f, float f2) {
            int i;
            float c = DrawerLayout.this.mo3043c(view);
            int width = view.getWidth();
            if (DrawerLayout.this.mo3036a(view, 3)) {
                i = (f > 0.0f || (f == 0.0f && c > 0.5f)) ? 0 : -width;
            } else {
                int width2 = DrawerLayout.this.getWidth();
                i = (f < 0.0f || (f == 0.0f && c > 0.5f)) ? width2 - width : width2;
            }
            this.f3363c.settleCapturedViewAt(i, view.getTop());
            DrawerLayout.this.invalidate();
        }

        public void removeCallbacks() {
            DrawerLayout.this.removeCallbacks(this.f3364d);
        }

        public void setDragger(ViewDragHelper viewDragHelper) {
            this.f3363c = viewDragHelper;
        }

        public boolean tryCaptureView(View view, int i) {
            return DrawerLayout.this.mo3056f(view) && DrawerLayout.this.mo3036a(view, this.f3362b) && DrawerLayout.this.getDrawerLockMode(view) == 0;
        }
    }

    static {
        boolean z = true;
        if (VERSION.SDK_INT < 21) {
            z = false;
        }
        f3314d = z;
        if (VERSION.SDK_INT >= 21) {
            f3313c = new DrawerLayoutCompatImplApi21();
        } else {
            f3313c = new DrawerLayoutCompatImplBase();
        }
    }

    public DrawerLayout(Context context) {
        this(context, null);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3328e = new ChildAccessibilityDelegate();
        this.f3331h = -1728053248;
        this.f3333j = new Paint();
        this.f3340q = true;
        this.f3341r = 3;
        this.f3342s = 3;
        this.f3343t = 3;
        this.f3344u = 3;
        this.f3323I = null;
        this.f3324J = null;
        this.f3325K = null;
        this.f3326L = null;
        setDescendantFocusability(262144);
        float f = getResources().getDisplayMetrics().density;
        this.f3330g = (int) ((64.0f * f) + 0.5f);
        float f2 = 400.0f * f;
        this.f3336m = new ViewDragCallback(3);
        this.f3337n = new ViewDragCallback(5);
        this.f3334k = ViewDragHelper.create(this, 1.0f, this.f3336m);
        this.f3334k.setEdgeTrackingEnabled(1);
        this.f3334k.setMinVelocity(f2);
        this.f3336m.setDragger(this.f3334k);
        this.f3335l = ViewDragHelper.create(this, 1.0f, this.f3337n);
        this.f3335l.setEdgeTrackingEnabled(2);
        this.f3335l.setMinVelocity(f2);
        this.f3337n.setDragger(this.f3335l);
        setFocusableInTouchMode(true);
        ViewCompat.m12910c((View) this, 1);
        ViewCompat.m12894a((View) this, (AccessibilityDelegateCompat) new AccessibilityDelegate());
        ViewGroupCompat.m13255a(this, false);
        if (ViewCompat.m12939x(this)) {
            f3313c.configureApplyInsets(this);
            this.f3316B = f3313c.getDefaultStatusBarBackground(context);
        }
        this.f3329f = 10.0f * f;
        this.f3327M = new ArrayList<>();
    }

    /* renamed from: a */
    private void m3853a(View view, boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((z || mo3056f(childAt)) && (!z || childAt != view)) {
                ViewCompat.m12910c(childAt, 4);
            } else {
                ViewCompat.m12910c(childAt, 1);
            }
        }
    }

    /* renamed from: a */
    private boolean m3854a(Drawable drawable, int i) {
        if (drawable == null || !DrawableCompat.m8891b(drawable)) {
            return false;
        }
        DrawableCompat.m8892b(drawable, i);
        return true;
    }

    /* renamed from: b */
    static String m3855b(int i) {
        return (i & 3) == 3 ? "LEFT" : (i & 5) == 5 ? "RIGHT" : Integer.toHexString(i);
    }

    /* renamed from: d */
    private void m3856d() {
        if (!f3314d) {
            this.f3317C = m3857e();
            this.f3318D = m3858f();
        }
    }

    /* renamed from: e */
    private Drawable m3857e() {
        int h = ViewCompat.m12923h(this);
        if (h == 0) {
            if (this.f3323I != null) {
                m3854a(this.f3323I, h);
                return this.f3323I;
            }
        } else if (this.f3324J != null) {
            m3854a(this.f3324J, h);
            return this.f3324J;
        }
        return this.f3325K;
    }

    /* renamed from: f */
    private Drawable m3858f() {
        int h = ViewCompat.m12923h(this);
        if (h == 0) {
            if (this.f3324J != null) {
                m3854a(this.f3324J, h);
                return this.f3324J;
            }
        } else if (this.f3323I != null) {
            m3854a(this.f3323I, h);
            return this.f3323I;
        }
        return this.f3326L;
    }

    /* renamed from: g */
    private boolean m3859g() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((LayoutParams) getChildAt(i).getLayoutParams()).f3354b) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: g */
    static boolean m3860g(View view) {
        return (ViewCompat.m12916e(view) == 4 || ViewCompat.m12916e(view) == 2) ? false : true;
    }

    /* renamed from: h */
    private boolean m3861h() {
        return mo3040b() != null;
    }

    /* renamed from: h */
    private static boolean m3862h(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public View mo3030a() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((((LayoutParams) childAt.getLayoutParams()).f3355c & 1) == 1) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public View mo3031a(int i) {
        int a = GravityCompat.m12720a(i, ViewCompat.m12923h(this)) & 7;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((mo3053d(childAt) & 7) == a) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3032a(int i, int i2, View view) {
        int viewDragState = this.f3334k.getViewDragState();
        int viewDragState2 = this.f3335l.getViewDragState();
        int i3 = (viewDragState == 1 || viewDragState2 == 1) ? 1 : (viewDragState == 2 || viewDragState2 == 2) ? 2 : 0;
        if (view != null && i2 == 0) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (layoutParams.f3353a == 0.0f) {
                mo3033a(view);
            } else if (layoutParams.f3353a == 1.0f) {
                mo3041b(view);
            }
        }
        if (i3 != this.f3338o) {
            this.f3338o = i3;
            if (this.f3348y != null) {
                for (int size = this.f3348y.size() - 1; size >= 0; size--) {
                    ((DrawerListener) this.f3348y.get(size)).onDrawerStateChanged(i3);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3033a(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if ((layoutParams.f3355c & 1) == 1) {
            layoutParams.f3355c = 0;
            if (this.f3348y != null) {
                for (int size = this.f3348y.size() - 1; size >= 0; size--) {
                    ((DrawerListener) this.f3348y.get(size)).onDrawerClosed(view);
                }
            }
            m3853a(view, false);
            if (hasWindowFocus()) {
                View rootView = getRootView();
                if (rootView != null) {
                    rootView.sendAccessibilityEvent(32);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3034a(View view, float f) {
        if (this.f3348y != null) {
            for (int size = this.f3348y.size() - 1; size >= 0; size--) {
                ((DrawerListener) this.f3348y.get(size)).onDrawerSlide(view, f);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3035a(boolean z) {
        boolean z2 = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (mo3056f(childAt) && (!z || layoutParams.f3354b)) {
                z2 = mo3036a(childAt, 3) ? z2 | this.f3334k.smoothSlideViewTo(childAt, -childAt.getWidth(), childAt.getTop()) : z2 | this.f3335l.smoothSlideViewTo(childAt, getWidth(), childAt.getTop());
                layoutParams.f3354b = false;
            }
        }
        this.f3336m.removeCallbacks();
        this.f3337n.removeCallbacks();
        if (z2) {
            invalidate();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo3036a(View view, int i) {
        return (mo3053d(view) & i) == i;
    }

    public void addDrawerListener(DrawerListener drawerListener) {
        if (drawerListener != null) {
            if (this.f3348y == null) {
                this.f3348y = new ArrayList();
            }
            this.f3348y.add(drawerListener);
        }
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        if (getDescendantFocusability() != 393216) {
            int childCount = getChildCount();
            boolean z = false;
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = getChildAt(i3);
                if (!mo3056f(childAt)) {
                    this.f3327M.add(childAt);
                } else if (isDrawerOpen(childAt)) {
                    z = true;
                    childAt.addFocusables(arrayList, i, i2);
                }
            }
            if (!z) {
                int size = this.f3327M.size();
                for (int i4 = 0; i4 < size; i4++) {
                    View view = (View) this.f3327M.get(i4);
                    if (view.getVisibility() == 0) {
                        view.addFocusables(arrayList, i, i2);
                    }
                }
            }
            this.f3327M.clear();
        }
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (mo3030a() != null || mo3056f(view)) {
            ViewCompat.m12910c(view, 4);
        } else {
            ViewCompat.m12910c(view, 1);
        }
        if (!f3312b) {
            ViewCompat.m12894a(view, (AccessibilityDelegateCompat) this.f3328e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public View mo3040b() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (mo3056f(childAt) && isDrawerVisible(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo3041b(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if ((layoutParams.f3355c & 1) == 0) {
            layoutParams.f3355c = 1;
            if (this.f3348y != null) {
                for (int size = this.f3348y.size() - 1; size >= 0; size--) {
                    ((DrawerListener) this.f3348y.get(size)).onDrawerOpened(view);
                }
            }
            m3853a(view, true);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo3042b(View view, float f) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f != layoutParams.f3353a) {
            layoutParams.f3353a = f;
            mo3034a(view, f);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public float mo3043c(View view) {
        return ((LayoutParams) view.getLayoutParams()).f3353a;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo3044c() {
        if (!this.f3346w) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                getChildAt(i).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.f3346w = true;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo3045c(View view, float f) {
        float c = mo3043c(view);
        int width = view.getWidth();
        int i = ((int) (((float) width) * f)) - ((int) (((float) width) * c));
        if (!mo3036a(view, 3)) {
            i = -i;
        }
        view.offsetLeftAndRight(i);
        mo3042b(view, f);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public void closeDrawer(int i) {
        closeDrawer(i, true);
    }

    public void closeDrawer(int i, boolean z) {
        View a = mo3031a(i);
        if (a == null) {
            throw new IllegalArgumentException("No drawer view found with gravity " + m3855b(i));
        }
        closeDrawer(a, z);
    }

    public void closeDrawer(View view) {
        closeDrawer(view, true);
    }

    public void closeDrawer(View view, boolean z) {
        if (!mo3056f(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (this.f3340q) {
            layoutParams.f3353a = 0.0f;
            layoutParams.f3355c = 0;
        } else if (z) {
            layoutParams.f3355c |= 4;
            if (mo3036a(view, 3)) {
                this.f3334k.smoothSlideViewTo(view, -view.getWidth(), view.getTop());
            } else {
                this.f3335l.smoothSlideViewTo(view, getWidth(), view.getTop());
            }
        } else {
            mo3045c(view, 0.0f);
            mo3032a(layoutParams.gravity, 0, view);
            view.setVisibility(4);
        }
        invalidate();
    }

    public void closeDrawers() {
        mo3035a(false);
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f = 0.0f;
        for (int i = 0; i < childCount; i++) {
            f = Math.max(f, ((LayoutParams) getChildAt(i).getLayoutParams()).f3353a);
        }
        this.f3332i = f;
        if (this.f3334k.continueSettling(true) || this.f3335l.continueSettling(true)) {
            ViewCompat.m12913d(this);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public int mo3053d(View view) {
        return GravityCompat.m12720a(((LayoutParams) view.getLayoutParams()).gravity, ViewCompat.m12923h(this));
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        int height = getHeight();
        boolean e = mo3055e(view);
        int i = 0;
        int width = getWidth();
        int save = canvas.save();
        if (e) {
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                if (childAt != view && childAt.getVisibility() == 0 && m3862h(childAt) && mo3056f(childAt) && childAt.getHeight() >= height) {
                    if (mo3036a(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right > i) {
                            i = right;
                        }
                    } else {
                        int left = childAt.getLeft();
                        if (left < width) {
                            width = left;
                        }
                    }
                }
            }
            canvas.clipRect(i, 0, width, getHeight());
        }
        boolean drawChild = super.drawChild(canvas, view, j);
        canvas.restoreToCount(save);
        if (this.f3332i > 0.0f && e) {
            this.f3333j.setColor((((int) (((float) ((this.f3331h & -16777216) >>> 24)) * this.f3332i)) << 24) | (this.f3331h & 16777215));
            canvas.drawRect((float) i, 0.0f, (float) width, (float) getHeight(), this.f3333j);
        } else if (this.f3317C != null && mo3036a(view, 3)) {
            int intrinsicWidth = this.f3317C.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.f3334k.getEdgeSize()), 1.0f));
            this.f3317C.setBounds(right2, view.getTop(), right2 + intrinsicWidth, view.getBottom());
            this.f3317C.setAlpha((int) (255.0f * max));
            this.f3317C.draw(canvas);
        } else if (this.f3318D != null && mo3036a(view, 5)) {
            int intrinsicWidth2 = this.f3318D.getIntrinsicWidth();
            int left2 = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left2)) / ((float) this.f3335l.getEdgeSize()), 1.0f));
            this.f3318D.setBounds(left2 - intrinsicWidth2, view.getTop(), left2, view.getBottom());
            this.f3318D.setAlpha((int) (255.0f * max2));
            this.f3318D.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public boolean mo3055e(View view) {
        return ((LayoutParams) view.getLayoutParams()).gravity == 0;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public boolean mo3056f(View view) {
        int a = GravityCompat.m12720a(((LayoutParams) view.getLayoutParams()).gravity, ViewCompat.m12923h(view));
        if ((a & 3) != 0) {
            return true;
        }
        return (a & 5) != 0;
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    public android.view.ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams ? new LayoutParams((LayoutParams) layoutParams) : layoutParams instanceof MarginLayoutParams ? new LayoutParams((MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    public float getDrawerElevation() {
        if (f3314d) {
            return this.f3329f;
        }
        return 0.0f;
    }

    public int getDrawerLockMode(int i) {
        int h = ViewCompat.m12923h(this);
        switch (i) {
            case 3:
                if (this.f3341r != 3) {
                    return this.f3341r;
                }
                int i2 = h == 0 ? this.f3343t : this.f3344u;
                if (i2 != 3) {
                    return i2;
                }
                break;
            case 5:
                if (this.f3342s != 3) {
                    return this.f3342s;
                }
                int i3 = h == 0 ? this.f3344u : this.f3343t;
                if (i3 != 3) {
                    return i3;
                }
                break;
            case 8388611:
                if (this.f3343t != 3) {
                    return this.f3343t;
                }
                int i4 = h == 0 ? this.f3341r : this.f3342s;
                if (i4 != 3) {
                    return i4;
                }
                break;
            case 8388613:
                if (this.f3344u != 3) {
                    return this.f3344u;
                }
                int i5 = h == 0 ? this.f3342s : this.f3341r;
                if (i5 != 3) {
                    return i5;
                }
                break;
        }
        return 0;
    }

    public int getDrawerLockMode(View view) {
        if (mo3056f(view)) {
            return getDrawerLockMode(((LayoutParams) view.getLayoutParams()).gravity);
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public CharSequence getDrawerTitle(int i) {
        int a = GravityCompat.m12720a(i, ViewCompat.m12923h(this));
        if (a == 3) {
            return this.f3319E;
        }
        if (a == 5) {
            return this.f3320F;
        }
        return null;
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.f3316B;
    }

    public boolean isDrawerOpen(int i) {
        View a = mo3031a(i);
        if (a != null) {
            return isDrawerOpen(a);
        }
        return false;
    }

    public boolean isDrawerOpen(View view) {
        if (mo3056f(view)) {
            return (((LayoutParams) view.getLayoutParams()).f3355c & 1) == 1;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public boolean isDrawerVisible(int i) {
        View a = mo3031a(i);
        if (a != null) {
            return isDrawerVisible(a);
        }
        return false;
    }

    public boolean isDrawerVisible(View view) {
        if (mo3056f(view)) {
            return ((LayoutParams) view.getLayoutParams()).f3353a > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f3340q = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f3340q = true;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f3322H && this.f3316B != null) {
            int topInset = f3313c.getTopInset(this.f3321G);
            if (topInset > 0) {
                this.f3316B.setBounds(0, 0, getWidth(), topInset);
                this.f3316B.draw(canvas);
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean shouldInterceptTouchEvent = this.f3334k.shouldInterceptTouchEvent(motionEvent) | this.f3335l.shouldInterceptTouchEvent(motionEvent);
        boolean z = false;
        switch (MotionEventCompat.m12830a(motionEvent)) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.f3349z = x;
                this.f3315A = y;
                if (this.f3332i > 0.0f) {
                    View findTopChildUnder = this.f3334k.findTopChildUnder((int) x, (int) y);
                    if (findTopChildUnder != null && mo3055e(findTopChildUnder)) {
                        z = true;
                    }
                }
                this.f3345v = false;
                this.f3346w = false;
                break;
            case 1:
            case 3:
                mo3035a(true);
                this.f3345v = false;
                this.f3346w = false;
                break;
            case 2:
                if (this.f3334k.checkTouchSlop(3)) {
                    this.f3336m.removeCallbacks();
                    this.f3337n.removeCallbacks();
                    break;
                }
                break;
        }
        return shouldInterceptTouchEvent || z || m3859g() || this.f3346w;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !m3861h()) {
            return super.onKeyDown(i, keyEvent);
        }
        keyEvent.startTracking();
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyUp(i, keyEvent);
        }
        View b = mo3040b();
        if (b != null && getDrawerLockMode(b) == 0) {
            closeDrawers();
        }
        return b != null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        float f;
        this.f3339p = true;
        int i6 = i3 - i;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (mo3055e(childAt)) {
                    childAt.layout(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.leftMargin + childAt.getMeasuredWidth(), layoutParams.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (mo3036a(childAt, 3)) {
                        i5 = (-measuredWidth) + ((int) (((float) measuredWidth) * layoutParams.f3353a));
                        f = ((float) (measuredWidth + i5)) / ((float) measuredWidth);
                    } else {
                        i5 = i6 - ((int) (((float) measuredWidth) * layoutParams.f3353a));
                        f = ((float) (i6 - i5)) / ((float) measuredWidth);
                    }
                    boolean z2 = f != layoutParams.f3353a;
                    switch (layoutParams.gravity & 112) {
                        case 16:
                            int i8 = i4 - i2;
                            int i9 = (i8 - measuredHeight) / 2;
                            if (i9 < layoutParams.topMargin) {
                                i9 = layoutParams.topMargin;
                            } else {
                                if (i9 + measuredHeight > i8 - layoutParams.bottomMargin) {
                                    i9 = (i8 - layoutParams.bottomMargin) - measuredHeight;
                                }
                            }
                            childAt.layout(i5, i9, i5 + measuredWidth, i9 + measuredHeight);
                            break;
                        case 80:
                            int i10 = i4 - i2;
                            childAt.layout(i5, (i10 - layoutParams.bottomMargin) - childAt.getMeasuredHeight(), i5 + measuredWidth, i10 - layoutParams.bottomMargin);
                            break;
                        default:
                            childAt.layout(i5, layoutParams.topMargin, i5 + measuredWidth, layoutParams.topMargin + measuredHeight);
                            break;
                    }
                    if (z2) {
                        mo3042b(childAt, f);
                    }
                    int i11 = layoutParams.f3353a > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i11) {
                        childAt.setVisibility(i11);
                    }
                }
            }
        }
        this.f3339p = false;
        this.f3340q = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        if (!(mode == 1073741824 && mode2 == 1073741824)) {
            if (isInEditMode()) {
                if (mode != Integer.MIN_VALUE) {
                    if (mode == 0) {
                        size = 300;
                    }
                }
                if (mode2 != Integer.MIN_VALUE) {
                    if (mode2 == 0) {
                        size2 = 300;
                    }
                }
            } else {
                throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
            }
        }
        setMeasuredDimension(size, size2);
        boolean z = this.f3321G != null && ViewCompat.m12939x(this);
        int h = ViewCompat.m12923h(this);
        boolean z2 = false;
        boolean z3 = false;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (z) {
                    int a = GravityCompat.m12720a(layoutParams.gravity, h);
                    if (ViewCompat.m12939x(childAt)) {
                        f3313c.dispatchChildInsets(childAt, this.f3321G, a);
                    } else {
                        f3313c.applyMarginInsets(layoutParams, this.f3321G, a);
                    }
                }
                if (mo3055e(childAt)) {
                    childAt.measure(MeasureSpec.makeMeasureSpec((size - layoutParams.leftMargin) - layoutParams.rightMargin, 1073741824), MeasureSpec.makeMeasureSpec((size2 - layoutParams.topMargin) - layoutParams.bottomMargin, 1073741824));
                } else if (mo3056f(childAt)) {
                    if (f3314d && ViewCompat.m12936u(childAt) != this.f3329f) {
                        ViewCompat.m12920f(childAt, this.f3329f);
                    }
                    int d = mo3053d(childAt) & 7;
                    boolean z4 = d == 3;
                    if ((!z4 || !z2) && (z4 || !z3)) {
                        if (z4) {
                            z2 = true;
                        } else {
                            z3 = true;
                        }
                        childAt.measure(getChildMeasureSpec(i, this.f3330g + layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width), getChildMeasureSpec(i2, layoutParams.topMargin + layoutParams.bottomMargin, layoutParams.height));
                    } else {
                        throw new IllegalStateException("Child drawer has absolute gravity " + m3855b(d) + " but this " + "DrawerLayout" + " already has a " + "drawer view along that edge");
                    }
                } else {
                    throw new IllegalStateException("Child " + childAt + " at index " + i3 + " does not have a valid layout_gravity - must be Gravity.LEFT, " + "Gravity.RIGHT or Gravity.NO_GRAVITY");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f3356a != 0) {
            View a = mo3031a(savedState.f3356a);
            if (a != null) {
                openDrawer(a);
            }
        }
        if (savedState.f3357b != 3) {
            setDrawerLockMode(savedState.f3357b, 3);
        }
        if (savedState.f3358c != 3) {
            setDrawerLockMode(savedState.f3358c, 5);
        }
        if (savedState.f3359d != 3) {
            setDrawerLockMode(savedState.f3359d, 8388611);
        }
        if (savedState.f3360e != 3) {
            setDrawerLockMode(savedState.f3360e, 8388613);
        }
    }

    public void onRtlPropertiesChanged(int i) {
        m3856d();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        LayoutParams layoutParams;
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            }
            layoutParams = (LayoutParams) getChildAt(i).getLayoutParams();
            boolean z = layoutParams.f3355c == 1;
            boolean z2 = layoutParams.f3355c == 2;
            if (z || z2) {
                savedState.f3356a = layoutParams.gravity;
            } else {
                i++;
            }
        }
        savedState.f3356a = layoutParams.gravity;
        savedState.f3357b = this.f3341r;
        savedState.f3358c = this.f3342s;
        savedState.f3359d = this.f3343t;
        savedState.f3360e = this.f3344u;
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.f3334k.processTouchEvent(motionEvent);
        this.f3335l.processTouchEvent(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.f3349z = x;
                this.f3315A = y;
                this.f3345v = false;
                this.f3346w = false;
                break;
            case 1:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                boolean z = true;
                View findTopChildUnder = this.f3334k.findTopChildUnder((int) x2, (int) y2);
                if (findTopChildUnder != null && mo3055e(findTopChildUnder)) {
                    float f = x2 - this.f3349z;
                    float f2 = y2 - this.f3315A;
                    int touchSlop = this.f3334k.getTouchSlop();
                    if ((f * f) + (f2 * f2) < ((float) (touchSlop * touchSlop))) {
                        View a = mo3030a();
                        if (a != null) {
                            z = getDrawerLockMode(a) == 2;
                        }
                    }
                }
                mo3035a(z);
                this.f3345v = false;
                break;
            case 3:
                mo3035a(true);
                this.f3345v = false;
                this.f3346w = false;
                break;
        }
        return true;
    }

    public void openDrawer(int i) {
        openDrawer(i, true);
    }

    public void openDrawer(int i, boolean z) {
        View a = mo3031a(i);
        if (a == null) {
            throw new IllegalArgumentException("No drawer view found with gravity " + m3855b(i));
        }
        openDrawer(a, z);
    }

    public void openDrawer(View view) {
        openDrawer(view, true);
    }

    public void openDrawer(View view, boolean z) {
        if (!mo3056f(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (this.f3340q) {
            layoutParams.f3353a = 1.0f;
            layoutParams.f3355c = 1;
            m3853a(view, true);
        } else if (z) {
            layoutParams.f3355c |= 2;
            if (mo3036a(view, 3)) {
                this.f3334k.smoothSlideViewTo(view, 0, view.getTop());
            } else {
                this.f3335l.smoothSlideViewTo(view, getWidth() - view.getWidth(), view.getTop());
            }
        } else {
            mo3045c(view, 1.0f);
            mo3032a(layoutParams.gravity, 0, view);
            view.setVisibility(0);
        }
        invalidate();
    }

    public void removeDrawerListener(DrawerListener drawerListener) {
        if (drawerListener != null && this.f3348y != null) {
            this.f3348y.remove(drawerListener);
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        this.f3345v = z;
        if (z) {
            mo3035a(true);
        }
    }

    public void requestLayout() {
        if (!this.f3339p) {
            super.requestLayout();
        }
    }

    public void setChildInsets(Object obj, boolean z) {
        this.f3321G = obj;
        this.f3322H = z;
        setWillNotDraw(!z && getBackground() == null);
        requestLayout();
    }

    public void setDrawerElevation(float f) {
        this.f3329f = f;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (mo3056f(childAt)) {
                ViewCompat.m12920f(childAt, this.f3329f);
            }
        }
    }

    @Deprecated
    public void setDrawerListener(DrawerListener drawerListener) {
        if (this.f3347x != null) {
            removeDrawerListener(this.f3347x);
        }
        if (drawerListener != null) {
            addDrawerListener(drawerListener);
        }
        this.f3347x = drawerListener;
    }

    public void setDrawerLockMode(int i) {
        setDrawerLockMode(i, 3);
        setDrawerLockMode(i, 5);
    }

    public void setDrawerLockMode(int i, int i2) {
        int a = GravityCompat.m12720a(i2, ViewCompat.m12923h(this));
        switch (i2) {
            case 3:
                this.f3341r = i;
                break;
            case 5:
                this.f3342s = i;
                break;
            case 8388611:
                this.f3343t = i;
                break;
            case 8388613:
                this.f3344u = i;
                break;
        }
        if (i != 0) {
            (a == 3 ? this.f3334k : this.f3335l).cancel();
        }
        switch (i) {
            case 1:
                View a2 = mo3031a(a);
                if (a2 != null) {
                    closeDrawer(a2);
                    return;
                }
                return;
            case 2:
                View a3 = mo3031a(a);
                if (a3 != null) {
                    openDrawer(a3);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void setDrawerLockMode(int i, View view) {
        if (!mo3056f(view)) {
            throw new IllegalArgumentException("View " + view + " is not a " + "drawer with appropriate layout_gravity");
        }
        setDrawerLockMode(i, ((LayoutParams) view.getLayoutParams()).gravity);
    }

    public void setDrawerShadow(int i, int i2) {
        setDrawerShadow(ContextCompat.getDrawable(getContext(), i), i2);
    }

    public void setDrawerShadow(Drawable drawable, int i) {
        if (!f3314d) {
            if ((i & 8388611) == 8388611) {
                this.f3323I = drawable;
            } else if ((i & 8388613) == 8388613) {
                this.f3324J = drawable;
            } else if ((i & 3) == 3) {
                this.f3325K = drawable;
            } else if ((i & 5) == 5) {
                this.f3326L = drawable;
            } else {
                return;
            }
            m3856d();
            invalidate();
        }
    }

    public void setDrawerTitle(int i, CharSequence charSequence) {
        int a = GravityCompat.m12720a(i, ViewCompat.m12923h(this));
        if (a == 3) {
            this.f3319E = charSequence;
        } else if (a == 5) {
            this.f3320F = charSequence;
        }
    }

    public void setScrimColor(int i) {
        this.f3331h = i;
        invalidate();
    }

    public void setStatusBarBackground(int i) {
        this.f3316B = i != 0 ? ContextCompat.getDrawable(getContext(), i) : null;
        invalidate();
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.f3316B = drawable;
        invalidate();
    }

    public void setStatusBarBackgroundColor(int i) {
        this.f3316B = new ColorDrawable(i);
        invalidate();
    }
}
