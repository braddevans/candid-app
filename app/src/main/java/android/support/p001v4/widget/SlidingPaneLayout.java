package android.support.p001v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p001v4.view.AbsSavedState;
import android.support.p001v4.widget.ViewDragHelper.Callback;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/* renamed from: android.support.v4.widget.SlidingPaneLayout */
public class SlidingPaneLayout extends ViewGroup {

    /* renamed from: h */
    static final SlidingPanelLayoutImpl f3479h;

    /* renamed from: a */
    View f3480a;

    /* renamed from: b */
    float f3481b;

    /* renamed from: c */
    int f3482c;

    /* renamed from: d */
    boolean f3483d;

    /* renamed from: e */
    final ViewDragHelper f3484e;

    /* renamed from: f */
    boolean f3485f;

    /* renamed from: g */
    final ArrayList<DisableLayerRunnable> f3486g;

    /* renamed from: i */
    private int f3487i;

    /* renamed from: j */
    private int f3488j;

    /* renamed from: k */
    private Drawable f3489k;

    /* renamed from: l */
    private Drawable f3490l;

    /* renamed from: m */
    private final int f3491m;

    /* renamed from: n */
    private boolean f3492n;

    /* renamed from: o */
    private float f3493o;

    /* renamed from: p */
    private int f3494p;

    /* renamed from: q */
    private float f3495q;

    /* renamed from: r */
    private float f3496r;

    /* renamed from: s */
    private PanelSlideListener f3497s;

    /* renamed from: t */
    private boolean f3498t;

    /* renamed from: u */
    private final Rect f3499u;

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$AccessibilityDelegate */
    class AccessibilityDelegate extends AccessibilityDelegateCompat {

        /* renamed from: b */
        private final Rect f3501b = new Rect();

        AccessibilityDelegate() {
        }

        /* renamed from: a */
        private void m3971a(AccessibilityNodeInfoCompat ggVar, AccessibilityNodeInfoCompat ggVar2) {
            Rect rect = this.f3501b;
            ggVar2.mo13586a(rect);
            ggVar.mo13592b(rect);
            ggVar2.mo13599c(rect);
            ggVar.mo13605d(rect);
            ggVar.mo13603c(ggVar2.mo13624j());
            ggVar.mo13588a(ggVar2.mo13632r());
            ggVar.mo13595b(ggVar2.mo13633s());
            ggVar.mo13607d(ggVar2.mo13636u());
            ggVar.mo13618h(ggVar2.mo13629o());
            ggVar.mo13614f(ggVar2.mo13627m());
            ggVar.mo13589a(ggVar2.mo13619h());
            ggVar.mo13597b(ggVar2.mo13622i());
            ggVar.mo13608d(ggVar2.mo13625k());
            ggVar.mo13612e(ggVar2.mo13626l());
            ggVar.mo13616g(ggVar2.mo13628n());
            ggVar.mo13585a(ggVar2.mo13604d());
            ggVar.mo13591b(ggVar2.mo13609e());
        }

        public boolean filter(View view) {
            return SlidingPaneLayout.this.mo3372f(view);
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(SlidingPaneLayout.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat ggVar) {
            AccessibilityNodeInfoCompat a = AccessibilityNodeInfoCompat.m13414a(ggVar);
            super.onInitializeAccessibilityNodeInfo(view, a);
            m3971a(ggVar, a);
            a.mo13637v();
            ggVar.mo13595b((CharSequence) SlidingPaneLayout.class.getName());
            ggVar.mo13593b(view);
            ViewParent i = ViewCompat.m12924i(view);
            if (i instanceof View) {
                ggVar.mo13606d((View) i);
            }
            int childCount = SlidingPaneLayout.this.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = SlidingPaneLayout.this.getChildAt(i2);
                if (!filter(childAt) && childAt.getVisibility() == 0) {
                    ViewCompat.m12910c(childAt, 1);
                    ggVar.mo13600c(childAt);
                }
            }
        }

        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            if (!filter(view)) {
                return super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
            }
            return false;
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$DisableLayerRunnable */
    class DisableLayerRunnable implements Runnable {

        /* renamed from: a */
        final View f3502a;

        DisableLayerRunnable(View view) {
            this.f3502a = view;
        }

        public void run() {
            if (this.f3502a.getParent() == SlidingPaneLayout.this) {
                ViewCompat.m12888a(this.f3502a, 0, (Paint) null);
                SlidingPaneLayout.this.mo3371e(this.f3502a);
            }
            SlidingPaneLayout.this.f3486g.remove(this);
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$DragHelperCallback */
    class DragHelperCallback extends Callback {
        DragHelperCallback() {
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            LayoutParams layoutParams = (LayoutParams) SlidingPaneLayout.this.f3480a.getLayoutParams();
            if (SlidingPaneLayout.this.mo3361b()) {
                int width = SlidingPaneLayout.this.getWidth() - ((SlidingPaneLayout.this.getPaddingRight() + layoutParams.rightMargin) + SlidingPaneLayout.this.f3480a.getWidth());
                return Math.max(Math.min(i, width), width - SlidingPaneLayout.this.f3482c);
            }
            int paddingLeft = SlidingPaneLayout.this.getPaddingLeft() + layoutParams.leftMargin;
            return Math.min(Math.max(i, paddingLeft), paddingLeft + SlidingPaneLayout.this.f3482c);
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return view.getTop();
        }

        public int getViewHorizontalDragRange(View view) {
            return SlidingPaneLayout.this.f3482c;
        }

        public void onEdgeDragStarted(int i, int i2) {
            SlidingPaneLayout.this.f3484e.captureChildView(SlidingPaneLayout.this.f3480a, i2);
        }

        public void onViewCaptured(View view, int i) {
            SlidingPaneLayout.this.mo3356a();
        }

        public void onViewDragStateChanged(int i) {
            if (SlidingPaneLayout.this.f3484e.getViewDragState() != 0) {
                return;
            }
            if (SlidingPaneLayout.this.f3481b == 0.0f) {
                SlidingPaneLayout.this.mo3368d(SlidingPaneLayout.this.f3480a);
                SlidingPaneLayout.this.mo3362c(SlidingPaneLayout.this.f3480a);
                SlidingPaneLayout.this.f3485f = false;
                return;
            }
            SlidingPaneLayout.this.mo3360b(SlidingPaneLayout.this.f3480a);
            SlidingPaneLayout.this.f3485f = true;
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            SlidingPaneLayout.this.mo3357a(i);
            SlidingPaneLayout.this.invalidate();
        }

        public void onViewReleased(View view, float f, float f2) {
            int paddingLeft;
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (SlidingPaneLayout.this.mo3361b()) {
                int paddingRight = SlidingPaneLayout.this.getPaddingRight() + layoutParams.rightMargin;
                if (f < 0.0f || (f == 0.0f && SlidingPaneLayout.this.f3481b > 0.5f)) {
                    paddingRight += SlidingPaneLayout.this.f3482c;
                }
                paddingLeft = (SlidingPaneLayout.this.getWidth() - paddingRight) - SlidingPaneLayout.this.f3480a.getWidth();
            } else {
                paddingLeft = SlidingPaneLayout.this.getPaddingLeft() + layoutParams.leftMargin;
                if (f > 0.0f || (f == 0.0f && SlidingPaneLayout.this.f3481b > 0.5f)) {
                    paddingLeft += SlidingPaneLayout.this.f3482c;
                }
            }
            SlidingPaneLayout.this.f3484e.settleCapturedViewAt(paddingLeft, view.getTop());
            SlidingPaneLayout.this.invalidate();
        }

        public boolean tryCaptureView(View view, int i) {
            if (SlidingPaneLayout.this.f3483d) {
                return false;
            }
            return ((LayoutParams) view.getLayoutParams()).f3506a;
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$LayoutParams */
    public static class LayoutParams extends MarginLayoutParams {

        /* renamed from: d */
        private static final int[] f3505d = {16843137};

        /* renamed from: a */
        boolean f3506a;

        /* renamed from: b */
        boolean f3507b;

        /* renamed from: c */
        Paint f3508c;
        public float weight = 0.0f;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f3505d);
            this.weight = obtainStyledAttributes.getFloat(0, 0.0f);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(LayoutParams layoutParams) {
            super(layoutParams);
            this.weight = layoutParams.weight;
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$PanelSlideListener */
    public interface PanelSlideListener {
        void onPanelClosed(View view);

        void onPanelOpened(View view);

        void onPanelSlide(View view, float f);
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$SavedState */
    static class SavedState extends AbsSavedState {
        public static final Creator<SavedState> CREATOR = ParcelableCompat.m12486a(new ParcelableCompatCreatorCallbacks<SavedState>() {
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a */
        boolean f3509a;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f3509a = parcel.readInt() != 0;
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f3509a ? 1 : 0);
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$SimplePanelSlideListener */
    public static class SimplePanelSlideListener implements PanelSlideListener {
        public void onPanelClosed(View view) {
        }

        public void onPanelOpened(View view) {
        }

        public void onPanelSlide(View view, float f) {
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImpl */
    interface SlidingPanelLayoutImpl {
        void invalidateChildRegion(SlidingPaneLayout slidingPaneLayout, View view);
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImplBase */
    static class SlidingPanelLayoutImplBase implements SlidingPanelLayoutImpl {
        SlidingPanelLayoutImplBase() {
        }

        public void invalidateChildRegion(SlidingPaneLayout slidingPaneLayout, View view) {
            ViewCompat.m12887a(slidingPaneLayout, view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImplJB */
    static class SlidingPanelLayoutImplJB extends SlidingPanelLayoutImplBase {

        /* renamed from: a */
        private Method f3510a;

        /* renamed from: b */
        private Field f3511b;

        SlidingPanelLayoutImplJB() {
            try {
                this.f3510a = View.class.getDeclaredMethod("getDisplayList", null);
            } catch (NoSuchMethodException e) {
                Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", e);
            }
            try {
                this.f3511b = View.class.getDeclaredField("mRecreateDisplayList");
                this.f3511b.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", e2);
            }
        }

        public void invalidateChildRegion(SlidingPaneLayout slidingPaneLayout, View view) {
            if (this.f3510a == null || this.f3511b == null) {
                view.invalidate();
                return;
            }
            try {
                this.f3511b.setBoolean(view, true);
                this.f3510a.invoke(view, null);
            } catch (Exception e) {
                Log.e("SlidingPaneLayout", "Error refreshing display list state", e);
            }
            super.invalidateChildRegion(slidingPaneLayout, view);
        }
    }

    /* renamed from: android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImplJBMR1 */
    static class SlidingPanelLayoutImplJBMR1 extends SlidingPanelLayoutImplBase {
        SlidingPanelLayoutImplJBMR1() {
        }

        public void invalidateChildRegion(SlidingPaneLayout slidingPaneLayout, View view) {
            ViewCompat.m12890a(view, ((LayoutParams) view.getLayoutParams()).f3508c);
        }
    }

    static {
        int i = VERSION.SDK_INT;
        if (i >= 17) {
            f3479h = new SlidingPanelLayoutImplJBMR1();
        } else if (i >= 16) {
            f3479h = new SlidingPanelLayoutImplJB();
        } else {
            f3479h = new SlidingPanelLayoutImplBase();
        }
    }

    public SlidingPaneLayout(Context context) {
        this(context, null);
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3487i = -858993460;
        this.f3498t = true;
        this.f3499u = new Rect();
        this.f3486g = new ArrayList<>();
        float f = context.getResources().getDisplayMetrics().density;
        this.f3491m = (int) ((32.0f * f) + 0.5f);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        setWillNotDraw(false);
        ViewCompat.m12894a((View) this, (AccessibilityDelegateCompat) new AccessibilityDelegate());
        ViewCompat.m12910c((View) this, 1);
        this.f3484e = ViewDragHelper.create(this, 0.5f, new DragHelperCallback());
        this.f3484e.setMinVelocity(400.0f * f);
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0020  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3956a(float r13) {
        /*
            r12 = this;
            r11 = 1065353216(0x3f800000, float:1.0)
            boolean r4 = r12.mo3361b()
            android.view.View r9 = r12.f3480a
            android.view.ViewGroup$LayoutParams r7 = r9.getLayoutParams()
            android.support.v4.widget.SlidingPaneLayout$LayoutParams r7 = (android.support.p001v4.widget.SlidingPaneLayout.LayoutParams) r7
            boolean r9 = r7.f3507b
            if (r9 == 0) goto L_0x002e
            if (r4 == 0) goto L_0x002b
            int r9 = r7.rightMargin
        L_0x0016:
            if (r9 > 0) goto L_0x002e
            r1 = 1
        L_0x0019:
            int r0 = r12.getChildCount()
            r3 = 0
        L_0x001e:
            if (r3 >= r0) goto L_0x005c
            android.view.View r8 = r12.getChildAt(r3)
            android.view.View r9 = r12.f3480a
            if (r8 != r9) goto L_0x0030
        L_0x0028:
            int r3 = r3 + 1
            goto L_0x001e
        L_0x002b:
            int r9 = r7.leftMargin
            goto L_0x0016
        L_0x002e:
            r1 = 0
            goto L_0x0019
        L_0x0030:
            float r9 = r12.f3493o
            float r9 = r11 - r9
            int r10 = r12.f3494p
            float r10 = (float) r10
            float r9 = r9 * r10
            int r6 = (int) r9
            r12.f3493o = r13
            float r9 = r11 - r13
            int r10 = r12.f3494p
            float r10 = (float) r10
            float r9 = r9 * r10
            int r5 = (int) r9
            int r2 = r6 - r5
            if (r4 == 0) goto L_0x0047
            int r2 = -r2
        L_0x0047:
            r8.offsetLeftAndRight(r2)
            if (r1 == 0) goto L_0x0028
            if (r4 == 0) goto L_0x0057
            float r9 = r12.f3493o
            float r9 = r9 - r11
        L_0x0051:
            int r10 = r12.f3488j
            r12.m3957a(r8, r9, r10)
            goto L_0x0028
        L_0x0057:
            float r9 = r12.f3493o
            float r9 = r11 - r9
            goto L_0x0051
        L_0x005c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.p001v4.widget.SlidingPaneLayout.m3956a(float):void");
    }

    /* renamed from: a */
    private void m3957a(View view, float f, int i) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f > 0.0f && i != 0) {
            int i2 = (((int) (((float) ((-16777216 & i) >>> 24)) * f)) << 24) | (16777215 & i);
            if (layoutParams.f3508c == null) {
                layoutParams.f3508c = new Paint();
            }
            layoutParams.f3508c.setColorFilter(new PorterDuffColorFilter(i2, Mode.SRC_OVER));
            if (ViewCompat.m12922g(view) != 2) {
                ViewCompat.m12888a(view, 2, layoutParams.f3508c);
            }
            mo3371e(view);
        } else if (ViewCompat.m12922g(view) != 0) {
            if (layoutParams.f3508c != null) {
                layoutParams.f3508c.setColorFilter(null);
            }
            DisableLayerRunnable disableLayerRunnable = new DisableLayerRunnable(view);
            this.f3486g.add(disableLayerRunnable);
            ViewCompat.m12897a((View) this, (Runnable) disableLayerRunnable);
        }
    }

    /* renamed from: a */
    private boolean m3958a(View view, int i) {
        if (!this.f3498t && !mo3359a(0.0f, i)) {
            return false;
        }
        this.f3485f = false;
        return true;
    }

    /* renamed from: b */
    private boolean m3959b(View view, int i) {
        if (!this.f3498t && !mo3359a(1.0f, i)) {
            return false;
        }
        this.f3485f = true;
        return true;
    }

    /* renamed from: g */
    private static boolean m3960g(View view) {
        if (view.isOpaque()) {
            return true;
        }
        if (VERSION.SDK_INT >= 18) {
            return false;
        }
        Drawable background = view.getBackground();
        if (background != null) {
            return background.getOpacity() == -1;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3356a() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3357a(int i) {
        if (this.f3480a == null) {
            this.f3481b = 0.0f;
            return;
        }
        boolean b = mo3361b();
        LayoutParams layoutParams = (LayoutParams) this.f3480a.getLayoutParams();
        this.f3481b = ((float) ((b ? (getWidth() - i) - this.f3480a.getWidth() : i) - ((b ? getPaddingRight() : getPaddingLeft()) + (b ? layoutParams.rightMargin : layoutParams.leftMargin)))) / ((float) this.f3482c);
        if (this.f3494p != 0) {
            m3956a(this.f3481b);
        }
        if (layoutParams.f3507b) {
            m3957a(this.f3480a, this.f3481b, this.f3487i);
        }
        mo3358a(this.f3480a);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3358a(View view) {
        if (this.f3497s != null) {
            this.f3497s.onPanelSlide(view, this.f3481b);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo3359a(float f, int i) {
        int paddingLeft;
        if (!this.f3492n) {
            return false;
        }
        LayoutParams layoutParams = (LayoutParams) this.f3480a.getLayoutParams();
        if (mo3361b()) {
            paddingLeft = (int) (((float) getWidth()) - ((((float) (getPaddingRight() + layoutParams.rightMargin)) + (((float) this.f3482c) * f)) + ((float) this.f3480a.getWidth())));
        } else {
            paddingLeft = (int) (((float) (getPaddingLeft() + layoutParams.leftMargin)) + (((float) this.f3482c) * f));
        }
        if (!this.f3484e.smoothSlideViewTo(this.f3480a, paddingLeft, this.f3480a.getTop())) {
            return false;
        }
        mo3356a();
        ViewCompat.m12913d(this);
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo3360b(View view) {
        if (this.f3497s != null) {
            this.f3497s.onPanelOpened(view);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public boolean mo3361b() {
        return ViewCompat.m12923h(this) == 1;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo3362c(View view) {
        if (this.f3497s != null) {
            this.f3497s.onPanelClosed(view);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: protected */
    public boolean canScroll(View view, boolean z, int i, int i2, int i3) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i2 + scrollX >= childAt.getLeft() && i2 + scrollX < childAt.getRight() && i3 + scrollY >= childAt.getTop() && i3 + scrollY < childAt.getBottom()) {
                    if (canScroll(childAt, true, i, (i2 + scrollX) - childAt.getLeft(), (i3 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (z) {
            if (!mo3361b()) {
                i = -i;
            }
            if (ViewCompat.m12901a(view, i)) {
                return true;
            }
        }
        return false;
    }

    @Deprecated
    public boolean canSlide() {
        return this.f3492n;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public boolean closePane() {
        return m3958a(this.f3480a, 0);
    }

    public void computeScroll() {
        if (!this.f3484e.continueSettling(true)) {
            return;
        }
        if (!this.f3492n) {
            this.f3484e.abort();
        } else {
            ViewCompat.m12913d(this);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public void mo3368d(View view) {
        int i;
        int i2;
        int i3;
        int i4;
        boolean b = mo3361b();
        int paddingLeft = b ? getWidth() - getPaddingRight() : getPaddingLeft();
        int width = b ? getPaddingLeft() : getWidth() - getPaddingRight();
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        if (view == null || !m3960g(view)) {
            i = 0;
            i2 = 0;
            i3 = 0;
            i4 = 0;
        } else {
            i4 = view.getLeft();
            i3 = view.getRight();
            i2 = view.getTop();
            i = view.getBottom();
        }
        int i5 = 0;
        int childCount = getChildCount();
        while (i5 < childCount) {
            View childAt = getChildAt(i5);
            if (childAt != view) {
                if (childAt.getVisibility() != 8) {
                    childAt.setVisibility((Math.max(b ? width : paddingLeft, childAt.getLeft()) < i4 || Math.max(paddingTop, childAt.getTop()) < i2 || Math.min(b ? paddingLeft : width, childAt.getRight()) > i3 || Math.min(height, childAt.getBottom()) > i) ? 0 : 4);
                }
                i5++;
            } else {
                return;
            }
        }
    }

    public void draw(Canvas canvas) {
        int left;
        int i;
        super.draw(canvas);
        Drawable drawable = mo3361b() ? this.f3490l : this.f3489k;
        View view = getChildCount() > 1 ? getChildAt(1) : null;
        if (view != null && drawable != null) {
            int top = view.getTop();
            int bottom = view.getBottom();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (mo3361b()) {
                i = view.getRight();
                left = i + intrinsicWidth;
            } else {
                left = view.getLeft();
                i = left - intrinsicWidth;
            }
            drawable.setBounds(i, top, left, bottom);
            drawable.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        boolean drawChild;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.f3492n && !layoutParams.f3506a && this.f3480a != null) {
            canvas.getClipBounds(this.f3499u);
            if (mo3361b()) {
                this.f3499u.left = Math.max(this.f3499u.left, this.f3480a.getRight());
            } else {
                this.f3499u.right = Math.min(this.f3499u.right, this.f3480a.getLeft());
            }
            canvas.clipRect(this.f3499u);
        }
        if (VERSION.SDK_INT >= 11) {
            drawChild = super.drawChild(canvas, view, j);
        } else if (!layoutParams.f3507b || this.f3481b <= 0.0f) {
            if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
            drawChild = super.drawChild(canvas, view, j);
        } else {
            if (!view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(true);
            }
            Bitmap drawingCache = view.getDrawingCache();
            if (drawingCache != null) {
                canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), layoutParams.f3508c);
                drawChild = false;
            } else {
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
                drawChild = super.drawChild(canvas, view, j);
            }
        }
        canvas.restoreToCount(save);
        return drawChild;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public void mo3371e(View view) {
        f3479h.invalidateChildRegion(this, view);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public boolean mo3372f(View view) {
        if (view == null) {
            return false;
        }
        return this.f3492n && ((LayoutParams) view.getLayoutParams()).f3507b && this.f3481b > 0.0f;
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    public android.view.ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof MarginLayoutParams ? new LayoutParams((MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    public int getCoveredFadeColor() {
        return this.f3488j;
    }

    public int getParallaxDistance() {
        return this.f3494p;
    }

    public int getSliderFadeColor() {
        return this.f3487i;
    }

    public boolean isOpen() {
        return !this.f3492n || this.f3481b == 1.0f;
    }

    public boolean isSlideable() {
        return this.f3492n;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f3498t = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f3498t = true;
        int size = this.f3486g.size();
        for (int i = 0; i < size; i++) {
            ((DisableLayerRunnable) this.f3486g.get(i)).run();
        }
        this.f3486g.clear();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int a = MotionEventCompat.m12830a(motionEvent);
        if (!this.f3492n && a == 0 && getChildCount() > 1) {
            View childAt = getChildAt(1);
            if (childAt != null) {
                this.f3485f = !this.f3484e.isViewUnder(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
            }
        }
        if (!this.f3492n || (this.f3483d && a != 0)) {
            this.f3484e.cancel();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (a == 3 || a == 1) {
            this.f3484e.cancel();
            return false;
        } else {
            boolean z = false;
            switch (a) {
                case 0:
                    this.f3483d = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.f3495q = x;
                    this.f3496r = y;
                    if (this.f3484e.isViewUnder(this.f3480a, (int) x, (int) y) && mo3372f(this.f3480a)) {
                        z = true;
                        break;
                    }
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.f3495q);
                    float abs2 = Math.abs(y2 - this.f3496r);
                    if (abs > ((float) this.f3484e.getTouchSlop()) && abs2 > abs) {
                        this.f3484e.cancel();
                        this.f3483d = true;
                        return false;
                    }
            }
            return this.f3484e.shouldInterceptTouchEvent(motionEvent) || z;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        boolean b = mo3361b();
        if (b) {
            this.f3484e.setEdgeTrackingEnabled(2);
        } else {
            this.f3484e.setEdgeTrackingEnabled(1);
        }
        int i7 = i3 - i;
        int paddingLeft = b ? getPaddingRight() : getPaddingLeft();
        int paddingRight = b ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        int i8 = paddingLeft;
        int i9 = i8;
        if (this.f3498t) {
            this.f3481b = (!this.f3492n || !this.f3485f) ? 0.0f : 1.0f;
        }
        for (int i10 = 0; i10 < childCount; i10++) {
            View childAt = getChildAt(i10);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int i11 = 0;
                if (layoutParams.f3506a) {
                    int min = (Math.min(i9, (i7 - paddingRight) - this.f3491m) - i8) - (layoutParams.leftMargin + layoutParams.rightMargin);
                    this.f3482c = min;
                    int i12 = b ? layoutParams.rightMargin : layoutParams.leftMargin;
                    layoutParams.f3507b = ((i8 + i12) + min) + (measuredWidth / 2) > i7 - paddingRight;
                    int i13 = (int) (((float) min) * this.f3481b);
                    i8 += i13 + i12;
                    this.f3481b = ((float) i13) / ((float) this.f3482c);
                } else if (!this.f3492n || this.f3494p == 0) {
                    i8 = i9;
                } else {
                    i11 = (int) ((1.0f - this.f3481b) * ((float) this.f3494p));
                    i8 = i9;
                }
                if (b) {
                    i6 = (i7 - i8) + i11;
                    i5 = i6 - measuredWidth;
                } else {
                    i5 = i8 - i11;
                    i6 = i5 + measuredWidth;
                }
                childAt.layout(i5, paddingTop, i6, paddingTop + childAt.getMeasuredHeight());
                i9 += childAt.getWidth();
            }
        }
        if (this.f3498t) {
            if (this.f3492n) {
                if (this.f3494p != 0) {
                    m3956a(this.f3481b);
                }
                if (((LayoutParams) this.f3480a.getLayoutParams()).f3507b) {
                    m3957a(this.f3480a, this.f3481b, this.f3487i);
                }
            } else {
                for (int i14 = 0; i14 < childCount; i14++) {
                    m3957a(getChildAt(i14), 0.0f, this.f3487i);
                }
            }
            mo3368d(this.f3480a);
        }
        this.f3498t = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (mode != 1073741824) {
            if (!isInEditMode()) {
                throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
            } else if (mode != Integer.MIN_VALUE) {
                if (mode == 0) {
                    size = 300;
                }
            }
        } else if (mode2 == 0) {
            if (!isInEditMode()) {
                throw new IllegalStateException("Height must not be UNSPECIFIED");
            } else if (mode2 == 0) {
                mode2 = Integer.MIN_VALUE;
                size2 = 300;
            }
        }
        int i3 = 0;
        int i4 = -1;
        switch (mode2) {
            case Integer.MIN_VALUE:
                i4 = (size2 - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                i4 = (size2 - getPaddingTop()) - getPaddingBottom();
                i3 = i4;
                break;
        }
        float f = 0.0f;
        boolean z = false;
        int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
        int i5 = paddingLeft;
        int childCount = getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.f3480a = null;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (childAt.getVisibility() == 8) {
                layoutParams.f3507b = false;
            } else {
                if (layoutParams.weight > 0.0f) {
                    f += layoutParams.weight;
                    if (layoutParams.width == 0) {
                    }
                }
                int i7 = layoutParams.leftMargin + layoutParams.rightMargin;
                int makeMeasureSpec = layoutParams.width == -2 ? MeasureSpec.makeMeasureSpec(paddingLeft - i7, Integer.MIN_VALUE) : layoutParams.width == -1 ? MeasureSpec.makeMeasureSpec(paddingLeft - i7, 1073741824) : MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824);
                int makeMeasureSpec2 = layoutParams.height == -2 ? MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE) : layoutParams.height == -1 ? MeasureSpec.makeMeasureSpec(i4, 1073741824) : MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
                childAt.measure(makeMeasureSpec, makeMeasureSpec2);
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (mode2 == Integer.MIN_VALUE && measuredHeight > i3) {
                    i3 = Math.min(measuredHeight, i4);
                }
                i5 -= measuredWidth;
                boolean z2 = i5 < 0;
                layoutParams.f3506a = z2;
                z |= z2;
                if (layoutParams.f3506a) {
                    this.f3480a = childAt;
                }
            }
        }
        if (z || f > 0.0f) {
            int i8 = paddingLeft - this.f3491m;
            for (int i9 = 0; i9 < childCount; i9++) {
                View childAt2 = getChildAt(i9);
                if (childAt2.getVisibility() != 8) {
                    LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8) {
                        boolean z3 = layoutParams2.width == 0 && layoutParams2.weight > 0.0f;
                        int measuredWidth2 = z3 ? 0 : childAt2.getMeasuredWidth();
                        if (!z || childAt2 == this.f3480a) {
                            if (layoutParams2.weight > 0.0f) {
                                int makeMeasureSpec3 = layoutParams2.width == 0 ? layoutParams2.height == -2 ? MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE) : layoutParams2.height == -1 ? MeasureSpec.makeMeasureSpec(i4, 1073741824) : MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824) : MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                if (z) {
                                    int i10 = paddingLeft - (layoutParams2.leftMargin + layoutParams2.rightMargin);
                                    int makeMeasureSpec4 = MeasureSpec.makeMeasureSpec(i10, 1073741824);
                                    if (measuredWidth2 != i10) {
                                        childAt2.measure(makeMeasureSpec4, makeMeasureSpec3);
                                    }
                                } else {
                                    childAt2.measure(MeasureSpec.makeMeasureSpec(measuredWidth2 + ((int) ((layoutParams2.weight * ((float) Math.max(0, i5))) / f)), 1073741824), makeMeasureSpec3);
                                }
                            }
                        } else if (layoutParams2.width < 0 && (measuredWidth2 > i8 || layoutParams2.weight > 0.0f)) {
                            int makeMeasureSpec5 = z3 ? layoutParams2.height == -2 ? MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE) : layoutParams2.height == -1 ? MeasureSpec.makeMeasureSpec(i4, 1073741824) : MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824) : MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                            childAt2.measure(MeasureSpec.makeMeasureSpec(i8, 1073741824), makeMeasureSpec5);
                        }
                    }
                }
            }
        }
        setMeasuredDimension(size, getPaddingTop() + i3 + getPaddingBottom());
        this.f3492n = z;
        if (this.f3484e.getViewDragState() != 0 && !z) {
            this.f3484e.abort();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f3509a) {
            openPane();
        } else {
            closePane();
        }
        this.f3485f = savedState.f3509a;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f3509a = isSlideable() ? isOpen() : this.f3485f;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            this.f3498t = true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f3492n) {
            return super.onTouchEvent(motionEvent);
        }
        this.f3484e.processTouchEvent(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.f3495q = x;
                this.f3496r = y;
                return true;
            case 1:
                if (!mo3372f(this.f3480a)) {
                    return true;
                }
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                float f = x2 - this.f3495q;
                float f2 = y2 - this.f3496r;
                int touchSlop = this.f3484e.getTouchSlop();
                if ((f * f) + (f2 * f2) >= ((float) (touchSlop * touchSlop)) || !this.f3484e.isViewUnder(this.f3480a, (int) x2, (int) y2)) {
                    return true;
                }
                m3958a(this.f3480a, 0);
                return true;
            default:
                return true;
        }
    }

    public boolean openPane() {
        return m3959b(this.f3480a, 0);
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.f3492n) {
            this.f3485f = view == this.f3480a;
        }
    }

    public void setCoveredFadeColor(int i) {
        this.f3488j = i;
    }

    public void setPanelSlideListener(PanelSlideListener panelSlideListener) {
        this.f3497s = panelSlideListener;
    }

    public void setParallaxDistance(int i) {
        this.f3494p = i;
        requestLayout();
    }

    @Deprecated
    public void setShadowDrawable(Drawable drawable) {
        setShadowDrawableLeft(drawable);
    }

    public void setShadowDrawableLeft(Drawable drawable) {
        this.f3489k = drawable;
    }

    public void setShadowDrawableRight(Drawable drawable) {
        this.f3490l = drawable;
    }

    @Deprecated
    public void setShadowResource(int i) {
        setShadowDrawable(getResources().getDrawable(i));
    }

    public void setShadowResourceLeft(int i) {
        setShadowDrawableLeft(ContextCompat.getDrawable(getContext(), i));
    }

    public void setShadowResourceRight(int i) {
        setShadowDrawableRight(ContextCompat.getDrawable(getContext(), i));
    }

    public void setSliderFadeColor(int i) {
        this.f3487i = i;
    }

    @Deprecated
    public void smoothSlideClosed() {
        closePane();
    }

    @Deprecated
    public void smoothSlideOpen() {
        openPane();
    }
}
