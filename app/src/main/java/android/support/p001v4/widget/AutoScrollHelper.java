package android.support.p001v4.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

/* renamed from: android.support.v4.widget.AutoScrollHelper */
public abstract class AutoScrollHelper implements OnTouchListener {
    public static final int EDGE_TYPE_INSIDE = 0;
    public static final int EDGE_TYPE_INSIDE_EXTEND = 1;
    public static final int EDGE_TYPE_OUTSIDE = 2;
    public static final float NO_MAX = Float.MAX_VALUE;
    public static final float NO_MIN = 0.0f;
    public static final float RELATIVE_UNSPECIFIED = 0.0f;

    /* renamed from: r */
    private static final int f3262r = ViewConfiguration.getTapTimeout();

    /* renamed from: a */
    final ClampedScroller f3263a = new ClampedScroller();

    /* renamed from: b */
    final View f3264b;

    /* renamed from: c */
    boolean f3265c;

    /* renamed from: d */
    boolean f3266d;

    /* renamed from: e */
    boolean f3267e;

    /* renamed from: f */
    private final Interpolator f3268f = new AccelerateInterpolator();

    /* renamed from: g */
    private Runnable f3269g;

    /* renamed from: h */
    private float[] f3270h = {0.0f, 0.0f};

    /* renamed from: i */
    private float[] f3271i = {Float.MAX_VALUE, Float.MAX_VALUE};

    /* renamed from: j */
    private int f3272j;

    /* renamed from: k */
    private int f3273k;

    /* renamed from: l */
    private float[] f3274l = {0.0f, 0.0f};

    /* renamed from: m */
    private float[] f3275m = {0.0f, 0.0f};

    /* renamed from: n */
    private float[] f3276n = {Float.MAX_VALUE, Float.MAX_VALUE};

    /* renamed from: o */
    private boolean f3277o;

    /* renamed from: p */
    private boolean f3278p;

    /* renamed from: q */
    private boolean f3279q;

    /* renamed from: android.support.v4.widget.AutoScrollHelper$ClampedScroller */
    static class ClampedScroller {

        /* renamed from: a */
        private int f3280a;

        /* renamed from: b */
        private int f3281b;

        /* renamed from: c */
        private float f3282c;

        /* renamed from: d */
        private float f3283d;

        /* renamed from: e */
        private long f3284e = Long.MIN_VALUE;

        /* renamed from: f */
        private long f3285f = 0;

        /* renamed from: g */
        private int f3286g = 0;

        /* renamed from: h */
        private int f3287h = 0;

        /* renamed from: i */
        private long f3288i = -1;

        /* renamed from: j */
        private float f3289j;

        /* renamed from: k */
        private int f3290k;

        ClampedScroller() {
        }

        /* renamed from: a */
        private float m3838a(float f) {
            return (-4.0f * f * f) + (4.0f * f);
        }

        /* renamed from: a */
        private float m3839a(long j) {
            if (j < this.f3284e) {
                return 0.0f;
            }
            if (this.f3288i < 0 || j < this.f3288i) {
                return AutoScrollHelper.m3830a(((float) (j - this.f3284e)) / ((float) this.f3280a), 0.0f, 1.0f) * 0.5f;
            }
            long j2 = j - this.f3288i;
            return (AutoScrollHelper.m3830a(((float) j2) / ((float) this.f3290k), 0.0f, 1.0f) * this.f3289j) + (1.0f - this.f3289j);
        }

        public void computeScrollDelta() {
            if (this.f3285f == 0) {
                throw new RuntimeException("Cannot compute scroll delta before calling start()");
            }
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            float a = m3838a(m3839a(currentAnimationTimeMillis));
            long j = currentAnimationTimeMillis - this.f3285f;
            this.f3285f = currentAnimationTimeMillis;
            this.f3286g = (int) (((float) j) * a * this.f3282c);
            this.f3287h = (int) (((float) j) * a * this.f3283d);
        }

        public int getDeltaX() {
            return this.f3286g;
        }

        public int getDeltaY() {
            return this.f3287h;
        }

        public int getHorizontalDirection() {
            return (int) (this.f3282c / Math.abs(this.f3282c));
        }

        public int getVerticalDirection() {
            return (int) (this.f3283d / Math.abs(this.f3283d));
        }

        public boolean isFinished() {
            return this.f3288i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.f3288i + ((long) this.f3290k);
        }

        public void requestStop() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.f3290k = AutoScrollHelper.m3833a((int) (currentAnimationTimeMillis - this.f3284e), 0, this.f3281b);
            this.f3289j = m3839a(currentAnimationTimeMillis);
            this.f3288i = currentAnimationTimeMillis;
        }

        public void setRampDownDuration(int i) {
            this.f3281b = i;
        }

        public void setRampUpDuration(int i) {
            this.f3280a = i;
        }

        public void setTargetVelocity(float f, float f2) {
            this.f3282c = f;
            this.f3283d = f2;
        }

        public void start() {
            this.f3284e = AnimationUtils.currentAnimationTimeMillis();
            this.f3288i = -1;
            this.f3285f = this.f3284e;
            this.f3289j = 0.5f;
            this.f3286g = 0;
            this.f3287h = 0;
        }
    }

    /* renamed from: android.support.v4.widget.AutoScrollHelper$ScrollAnimationRunnable */
    class ScrollAnimationRunnable implements Runnable {
        ScrollAnimationRunnable() {
        }

        public void run() {
            if (AutoScrollHelper.this.f3267e) {
                if (AutoScrollHelper.this.f3265c) {
                    AutoScrollHelper.this.f3265c = false;
                    AutoScrollHelper.this.f3263a.start();
                }
                ClampedScroller clampedScroller = AutoScrollHelper.this.f3263a;
                if (clampedScroller.isFinished() || !AutoScrollHelper.this.mo2953a()) {
                    AutoScrollHelper.this.f3267e = false;
                    return;
                }
                if (AutoScrollHelper.this.f3266d) {
                    AutoScrollHelper.this.f3266d = false;
                    AutoScrollHelper.this.mo2954b();
                }
                clampedScroller.computeScrollDelta();
                AutoScrollHelper.this.scrollTargetBy(clampedScroller.getDeltaX(), clampedScroller.getDeltaY());
                ViewCompat.m12897a(AutoScrollHelper.this.f3264b, (Runnable) this);
            }
        }
    }

    public AutoScrollHelper(View view) {
        this.f3264b = view;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i = (int) ((1575.0f * displayMetrics.density) + 0.5f);
        int i2 = (int) ((315.0f * displayMetrics.density) + 0.5f);
        setMaximumVelocity((float) i, (float) i);
        setMinimumVelocity((float) i2, (float) i2);
        setEdgeType(1);
        setMaximumEdges(Float.MAX_VALUE, Float.MAX_VALUE);
        setRelativeEdges(0.2f, 0.2f);
        setRelativeVelocity(1.0f, 1.0f);
        setActivationDelay(f3262r);
        setRampUpDuration(500);
        setRampDownDuration(500);
    }

    /* renamed from: a */
    private float m3829a(float f, float f2) {
        if (f2 == 0.0f) {
            return 0.0f;
        }
        switch (this.f3272j) {
            case 0:
            case 1:
                if (f < f2) {
                    return f >= 0.0f ? 1.0f - (f / f2) : (!this.f3267e || this.f3272j != 1) ? 0.0f : 1.0f;
                }
                return 0.0f;
            case 2:
                if (f < 0.0f) {
                    return f / (-f2);
                }
                return 0.0f;
            default:
                return 0.0f;
        }
    }

    /* renamed from: a */
    static float m3830a(float f, float f2, float f3) {
        return f > f3 ? f3 : f < f2 ? f2 : f;
    }

    /* renamed from: a */
    private float m3831a(float f, float f2, float f3, float f4) {
        float interpolation;
        float a = m3830a(f * f2, 0.0f, f3);
        float a2 = m3829a(f2 - f4, a) - m3829a(f4, a);
        if (a2 < 0.0f) {
            interpolation = -this.f3268f.getInterpolation(-a2);
        } else if (a2 <= 0.0f) {
            return 0.0f;
        } else {
            interpolation = this.f3268f.getInterpolation(a2);
        }
        return m3830a(interpolation, -1.0f, 1.0f);
    }

    /* renamed from: a */
    private float m3832a(int i, float f, float f2, float f3) {
        float a = m3831a(this.f3270h[i], f2, this.f3271i[i], f);
        if (a == 0.0f) {
            return 0.0f;
        }
        float f4 = this.f3274l[i];
        float f5 = this.f3275m[i];
        float f6 = this.f3276n[i];
        float f7 = f4 * f3;
        return a > 0.0f ? m3830a(a * f7, f5, f6) : -m3830a((-a) * f7, f5, f6);
    }

    /* renamed from: a */
    static int m3833a(int i, int i2, int i3) {
        return i > i3 ? i3 : i < i2 ? i2 : i;
    }

    /* renamed from: c */
    private void m3834c() {
        if (this.f3269g == null) {
            this.f3269g = new ScrollAnimationRunnable();
        }
        this.f3267e = true;
        this.f3265c = true;
        if (this.f3277o || this.f3273k <= 0) {
            this.f3269g.run();
        } else {
            ViewCompat.m12898a(this.f3264b, this.f3269g, (long) this.f3273k);
        }
        this.f3277o = true;
    }

    /* renamed from: d */
    private void m3835d() {
        if (this.f3265c) {
            this.f3267e = false;
        } else {
            this.f3263a.requestStop();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo2953a() {
        ClampedScroller clampedScroller = this.f3263a;
        int verticalDirection = clampedScroller.getVerticalDirection();
        int horizontalDirection = clampedScroller.getHorizontalDirection();
        return (verticalDirection != 0 && canTargetScrollVertically(verticalDirection)) || (horizontalDirection != 0 && canTargetScrollHorizontally(horizontalDirection));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo2954b() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        this.f3264b.onTouchEvent(obtain);
        obtain.recycle();
    }

    public abstract boolean canTargetScrollHorizontally(int i);

    public abstract boolean canTargetScrollVertically(int i);

    public boolean isEnabled() {
        return this.f3278p;
    }

    public boolean isExclusive() {
        return this.f3279q;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z = true;
        if (!this.f3278p) {
            return false;
        }
        switch (MotionEventCompat.m12830a(motionEvent)) {
            case 0:
                this.f3266d = true;
                this.f3277o = false;
                break;
            case 1:
            case 3:
                m3835d();
                break;
            case 2:
                break;
        }
        this.f3263a.setTargetVelocity(m3832a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.f3264b.getWidth()), m3832a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.f3264b.getHeight()));
        if (!this.f3267e && mo2953a()) {
            m3834c();
        }
        if (!this.f3279q || !this.f3267e) {
            z = false;
        }
        return z;
    }

    public abstract void scrollTargetBy(int i, int i2);

    public AutoScrollHelper setActivationDelay(int i) {
        this.f3273k = i;
        return this;
    }

    public AutoScrollHelper setEdgeType(int i) {
        this.f3272j = i;
        return this;
    }

    public AutoScrollHelper setEnabled(boolean z) {
        if (this.f3278p && !z) {
            m3835d();
        }
        this.f3278p = z;
        return this;
    }

    public AutoScrollHelper setExclusive(boolean z) {
        this.f3279q = z;
        return this;
    }

    public AutoScrollHelper setMaximumEdges(float f, float f2) {
        this.f3271i[0] = f;
        this.f3271i[1] = f2;
        return this;
    }

    public AutoScrollHelper setMaximumVelocity(float f, float f2) {
        this.f3276n[0] = f / 1000.0f;
        this.f3276n[1] = f2 / 1000.0f;
        return this;
    }

    public AutoScrollHelper setMinimumVelocity(float f, float f2) {
        this.f3275m[0] = f / 1000.0f;
        this.f3275m[1] = f2 / 1000.0f;
        return this;
    }

    public AutoScrollHelper setRampDownDuration(int i) {
        this.f3263a.setRampDownDuration(i);
        return this;
    }

    public AutoScrollHelper setRampUpDuration(int i) {
        this.f3263a.setRampUpDuration(i);
        return this;
    }

    public AutoScrollHelper setRelativeEdges(float f, float f2) {
        this.f3270h[0] = f;
        this.f3270h[1] = f2;
        return this;
    }

    public AutoScrollHelper setRelativeVelocity(float f, float f2) {
        this.f3274l[0] = f / 1000.0f;
        this.f3274l[1] = f2 / 1000.0f;
        return this;
    }
}
