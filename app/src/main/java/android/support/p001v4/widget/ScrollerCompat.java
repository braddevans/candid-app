package android.support.p001v4.widget;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.animation.Interpolator;
import android.widget.OverScroller;

/* renamed from: android.support.v4.widget.ScrollerCompat */
public final class ScrollerCompat {

    /* renamed from: a */
    OverScroller f3466a;

    /* renamed from: b */
    private final boolean f3467b;

    ScrollerCompat(boolean z, Context context, Interpolator interpolator) {
        this.f3467b = z;
        this.f3466a = interpolator != null ? new OverScroller(context, interpolator) : new OverScroller(context);
    }

    public static ScrollerCompat create(Context context) {
        return create(context, null);
    }

    public static ScrollerCompat create(Context context, Interpolator interpolator) {
        return new ScrollerCompat(VERSION.SDK_INT >= 14, context, interpolator);
    }

    public void abortAnimation() {
        this.f3466a.abortAnimation();
    }

    public boolean computeScrollOffset() {
        return this.f3466a.computeScrollOffset();
    }

    public void fling(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f3466a.fling(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void fling(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f3466a.fling(i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
    }

    public float getCurrVelocity() {
        if (this.f3467b) {
            return ScrollerCompatIcs.getCurrVelocity(this.f3466a);
        }
        return 0.0f;
    }

    public int getCurrX() {
        return this.f3466a.getCurrX();
    }

    public int getCurrY() {
        return this.f3466a.getCurrY();
    }

    public int getFinalX() {
        return this.f3466a.getFinalX();
    }

    public int getFinalY() {
        return this.f3466a.getFinalY();
    }

    public boolean isFinished() {
        return this.f3466a.isFinished();
    }

    public boolean isOverScrolled() {
        return this.f3466a.isOverScrolled();
    }

    public void notifyHorizontalEdgeReached(int i, int i2, int i3) {
        this.f3466a.notifyHorizontalEdgeReached(i, i2, i3);
    }

    public void notifyVerticalEdgeReached(int i, int i2, int i3) {
        this.f3466a.notifyVerticalEdgeReached(i, i2, i3);
    }

    public boolean springBack(int i, int i2, int i3, int i4, int i5, int i6) {
        return this.f3466a.springBack(i, i2, i3, i4, i5, i6);
    }

    public void startScroll(int i, int i2, int i3, int i4) {
        this.f3466a.startScroll(i, i2, i3, i4);
    }

    public void startScroll(int i, int i2, int i3, int i4, int i5) {
        this.f3466a.startScroll(i, i2, i3, i4, i5);
    }
}
