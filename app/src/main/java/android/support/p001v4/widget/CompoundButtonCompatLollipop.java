package android.support.p001v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.widget.CompoundButton;

/* renamed from: android.support.v4.widget.CompoundButtonCompatLollipop */
class CompoundButtonCompatLollipop {
    CompoundButtonCompatLollipop() {
    }

    /* renamed from: a */
    static ColorStateList m3848a(CompoundButton compoundButton) {
        return compoundButton.getButtonTintList();
    }

    /* renamed from: a */
    static void m3849a(CompoundButton compoundButton, ColorStateList colorStateList) {
        compoundButton.setButtonTintList(colorStateList);
    }

    /* renamed from: a */
    static void m3850a(CompoundButton compoundButton, Mode mode) {
        compoundButton.setButtonTintMode(mode);
    }

    /* renamed from: b */
    static Mode m3851b(CompoundButton compoundButton) {
        return compoundButton.getButtonTintMode();
    }
}
