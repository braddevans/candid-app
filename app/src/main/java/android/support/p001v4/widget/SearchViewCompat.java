package android.support.p001v4.widget;

import android.content.ComponentName;
import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;

/* renamed from: android.support.v4.widget.SearchViewCompat */
public final class SearchViewCompat {

    /* renamed from: a */
    private static final SearchViewCompatImpl f3468a;

    /* renamed from: android.support.v4.widget.SearchViewCompat$OnCloseListener */
    public interface OnCloseListener {
        boolean onClose();
    }

    @Deprecated
    /* renamed from: android.support.v4.widget.SearchViewCompat$OnCloseListenerCompat */
    public static abstract class OnCloseListenerCompat implements OnCloseListener {
        public boolean onClose() {
            return false;
        }
    }

    /* renamed from: android.support.v4.widget.SearchViewCompat$OnQueryTextListener */
    public interface OnQueryTextListener {
        boolean onQueryTextChange(String str);

        boolean onQueryTextSubmit(String str);
    }

    @Deprecated
    /* renamed from: android.support.v4.widget.SearchViewCompat$OnQueryTextListenerCompat */
    public static abstract class OnQueryTextListenerCompat implements OnQueryTextListener {
        public boolean onQueryTextChange(String str) {
            return false;
        }

        public boolean onQueryTextSubmit(String str) {
            return false;
        }
    }

    /* renamed from: android.support.v4.widget.SearchViewCompat$SearchViewCompatHoneycombImpl */
    static class SearchViewCompatHoneycombImpl extends SearchViewCompatStubImpl {
        SearchViewCompatHoneycombImpl() {
        }

        /* access modifiers changed from: protected */
        public void checkIfLegalArg(View view) {
            SearchViewCompatHoneycomb.checkIfLegalArg(view);
        }

        public CharSequence getQuery(View view) {
            checkIfLegalArg(view);
            return SearchViewCompatHoneycomb.getQuery(view);
        }

        public boolean isIconified(View view) {
            checkIfLegalArg(view);
            return SearchViewCompatHoneycomb.isIconified(view);
        }

        public boolean isQueryRefinementEnabled(View view) {
            checkIfLegalArg(view);
            return SearchViewCompatHoneycomb.isQueryRefinementEnabled(view);
        }

        public boolean isSubmitButtonEnabled(View view) {
            checkIfLegalArg(view);
            return SearchViewCompatHoneycomb.isSubmitButtonEnabled(view);
        }

        public Object newOnCloseListener(final OnCloseListener onCloseListener) {
            return SearchViewCompatHoneycomb.newOnCloseListener(new OnCloseListenerCompatBridge() {
                public boolean onClose() {
                    return onCloseListener.onClose();
                }
            });
        }

        public Object newOnQueryTextListener(final OnQueryTextListener onQueryTextListener) {
            return SearchViewCompatHoneycomb.newOnQueryTextListener(new OnQueryTextListenerCompatBridge() {
                public boolean onQueryTextChange(String str) {
                    return onQueryTextListener.onQueryTextChange(str);
                }

                public boolean onQueryTextSubmit(String str) {
                    return onQueryTextListener.onQueryTextSubmit(str);
                }
            });
        }

        public View newSearchView(Context context) {
            return SearchViewCompatHoneycomb.newSearchView(context);
        }

        public void setIconified(View view, boolean z) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setIconified(view, z);
        }

        public void setMaxWidth(View view, int i) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setMaxWidth(view, i);
        }

        public void setOnCloseListener(View view, OnCloseListener onCloseListener) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setOnCloseListener(view, newOnCloseListener(onCloseListener));
        }

        public void setOnQueryTextListener(View view, OnQueryTextListener onQueryTextListener) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setOnQueryTextListener(view, newOnQueryTextListener(onQueryTextListener));
        }

        public void setQuery(View view, CharSequence charSequence, boolean z) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setQuery(view, charSequence, z);
        }

        public void setQueryHint(View view, CharSequence charSequence) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setQueryHint(view, charSequence);
        }

        public void setQueryRefinementEnabled(View view, boolean z) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setQueryRefinementEnabled(view, z);
        }

        public void setSearchableInfo(View view, ComponentName componentName) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setSearchableInfo(view, componentName);
        }

        public void setSubmitButtonEnabled(View view, boolean z) {
            checkIfLegalArg(view);
            SearchViewCompatHoneycomb.setSubmitButtonEnabled(view, z);
        }
    }

    /* renamed from: android.support.v4.widget.SearchViewCompat$SearchViewCompatIcsImpl */
    static class SearchViewCompatIcsImpl extends SearchViewCompatHoneycombImpl {
        SearchViewCompatIcsImpl() {
        }

        public View newSearchView(Context context) {
            return SearchViewCompatIcs.newSearchView(context);
        }

        public void setImeOptions(View view, int i) {
            checkIfLegalArg(view);
            SearchViewCompatIcs.setImeOptions(view, i);
        }

        public void setInputType(View view, int i) {
            checkIfLegalArg(view);
            SearchViewCompatIcs.setInputType(view, i);
        }
    }

    /* renamed from: android.support.v4.widget.SearchViewCompat$SearchViewCompatImpl */
    interface SearchViewCompatImpl {
        CharSequence getQuery(View view);

        boolean isIconified(View view);

        boolean isQueryRefinementEnabled(View view);

        boolean isSubmitButtonEnabled(View view);

        Object newOnCloseListener(OnCloseListener onCloseListener);

        Object newOnQueryTextListener(OnQueryTextListener onQueryTextListener);

        View newSearchView(Context context);

        void setIconified(View view, boolean z);

        void setImeOptions(View view, int i);

        void setInputType(View view, int i);

        void setMaxWidth(View view, int i);

        void setOnCloseListener(View view, OnCloseListener onCloseListener);

        void setOnQueryTextListener(View view, OnQueryTextListener onQueryTextListener);

        void setQuery(View view, CharSequence charSequence, boolean z);

        void setQueryHint(View view, CharSequence charSequence);

        void setQueryRefinementEnabled(View view, boolean z);

        void setSearchableInfo(View view, ComponentName componentName);

        void setSubmitButtonEnabled(View view, boolean z);
    }

    /* renamed from: android.support.v4.widget.SearchViewCompat$SearchViewCompatStubImpl */
    static class SearchViewCompatStubImpl implements SearchViewCompatImpl {
        SearchViewCompatStubImpl() {
        }

        public CharSequence getQuery(View view) {
            return null;
        }

        public boolean isIconified(View view) {
            return true;
        }

        public boolean isQueryRefinementEnabled(View view) {
            return false;
        }

        public boolean isSubmitButtonEnabled(View view) {
            return false;
        }

        public Object newOnCloseListener(OnCloseListener onCloseListener) {
            return null;
        }

        public Object newOnQueryTextListener(OnQueryTextListener onQueryTextListener) {
            return null;
        }

        public View newSearchView(Context context) {
            return null;
        }

        public void setIconified(View view, boolean z) {
        }

        public void setImeOptions(View view, int i) {
        }

        public void setInputType(View view, int i) {
        }

        public void setMaxWidth(View view, int i) {
        }

        public void setOnCloseListener(View view, OnCloseListener onCloseListener) {
        }

        public void setOnQueryTextListener(View view, OnQueryTextListener onQueryTextListener) {
        }

        public void setQuery(View view, CharSequence charSequence, boolean z) {
        }

        public void setQueryHint(View view, CharSequence charSequence) {
        }

        public void setQueryRefinementEnabled(View view, boolean z) {
        }

        public void setSearchableInfo(View view, ComponentName componentName) {
        }

        public void setSubmitButtonEnabled(View view, boolean z) {
        }
    }

    static {
        if (VERSION.SDK_INT >= 14) {
            f3468a = new SearchViewCompatIcsImpl();
        } else if (VERSION.SDK_INT >= 11) {
            f3468a = new SearchViewCompatHoneycombImpl();
        } else {
            f3468a = new SearchViewCompatStubImpl();
        }
    }

    public static CharSequence getQuery(View view) {
        return f3468a.getQuery(view);
    }

    public static boolean isIconified(View view) {
        return f3468a.isIconified(view);
    }

    public static boolean isQueryRefinementEnabled(View view) {
        return f3468a.isQueryRefinementEnabled(view);
    }

    public static boolean isSubmitButtonEnabled(View view) {
        return f3468a.isSubmitButtonEnabled(view);
    }

    public static View newSearchView(Context context) {
        return f3468a.newSearchView(context);
    }

    public static void setIconified(View view, boolean z) {
        f3468a.setIconified(view, z);
    }

    public static void setImeOptions(View view, int i) {
        f3468a.setImeOptions(view, i);
    }

    public static void setInputType(View view, int i) {
        f3468a.setInputType(view, i);
    }

    public static void setMaxWidth(View view, int i) {
        f3468a.setMaxWidth(view, i);
    }

    public static void setOnCloseListener(View view, OnCloseListener onCloseListener) {
        f3468a.setOnCloseListener(view, onCloseListener);
    }

    public static void setOnQueryTextListener(View view, OnQueryTextListener onQueryTextListener) {
        f3468a.setOnQueryTextListener(view, onQueryTextListener);
    }

    public static void setQuery(View view, CharSequence charSequence, boolean z) {
        f3468a.setQuery(view, charSequence, z);
    }

    public static void setQueryHint(View view, CharSequence charSequence) {
        f3468a.setQueryHint(view, charSequence);
    }

    public static void setQueryRefinementEnabled(View view, boolean z) {
        f3468a.setQueryRefinementEnabled(view, z);
    }

    public static void setSearchableInfo(View view, ComponentName componentName) {
        f3468a.setSearchableInfo(view, componentName);
    }

    public static void setSubmitButtonEnabled(View view, boolean z) {
        f3468a.setSubmitButtonEnabled(view, z);
    }
}
