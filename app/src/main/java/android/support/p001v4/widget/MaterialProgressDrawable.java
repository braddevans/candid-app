package android.support.p001v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

/* renamed from: android.support.v4.widget.MaterialProgressDrawable */
class MaterialProgressDrawable extends Drawable implements Animatable {

    /* renamed from: a */
    static final Interpolator f3389a = new FastOutSlowInInterpolator();

    /* renamed from: d */
    private static final Interpolator f3390d = new LinearInterpolator();

    /* renamed from: e */
    private static final int[] f3391e = {-16777216};

    /* renamed from: b */
    float f3392b;

    /* renamed from: c */
    boolean f3393c;

    /* renamed from: f */
    private final ArrayList<Animation> f3394f = new ArrayList<>();

    /* renamed from: g */
    private final Ring f3395g;

    /* renamed from: h */
    private float f3396h;

    /* renamed from: i */
    private Resources f3397i;

    /* renamed from: j */
    private View f3398j;

    /* renamed from: k */
    private Animation f3399k;

    /* renamed from: l */
    private double f3400l;

    /* renamed from: m */
    private double f3401m;

    /* renamed from: n */
    private final Callback f3402n = new Callback() {
        public void invalidateDrawable(Drawable drawable) {
            MaterialProgressDrawable.this.invalidateSelf();
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            MaterialProgressDrawable.this.scheduleSelf(runnable, j);
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            MaterialProgressDrawable.this.unscheduleSelf(runnable);
        }
    };

    @Retention(RetentionPolicy.SOURCE)
    /* renamed from: android.support.v4.widget.MaterialProgressDrawable$ProgressDrawableSize */
    public @interface ProgressDrawableSize {
    }

    /* renamed from: android.support.v4.widget.MaterialProgressDrawable$Ring */
    static class Ring {

        /* renamed from: a */
        private final RectF f3408a = new RectF();

        /* renamed from: b */
        private final Paint f3409b = new Paint();

        /* renamed from: c */
        private final Paint f3410c = new Paint();

        /* renamed from: d */
        private final Callback f3411d;

        /* renamed from: e */
        private float f3412e = 0.0f;

        /* renamed from: f */
        private float f3413f = 0.0f;

        /* renamed from: g */
        private float f3414g = 0.0f;

        /* renamed from: h */
        private float f3415h = 5.0f;

        /* renamed from: i */
        private float f3416i = 2.5f;

        /* renamed from: j */
        private int[] f3417j;

        /* renamed from: k */
        private int f3418k;

        /* renamed from: l */
        private float f3419l;

        /* renamed from: m */
        private float f3420m;

        /* renamed from: n */
        private float f3421n;

        /* renamed from: o */
        private boolean f3422o;

        /* renamed from: p */
        private Path f3423p;

        /* renamed from: q */
        private float f3424q;

        /* renamed from: r */
        private double f3425r;

        /* renamed from: s */
        private int f3426s;

        /* renamed from: t */
        private int f3427t;

        /* renamed from: u */
        private int f3428u;

        /* renamed from: v */
        private final Paint f3429v = new Paint(1);

        /* renamed from: w */
        private int f3430w;

        /* renamed from: x */
        private int f3431x;

        Ring(Callback callback) {
            this.f3411d = callback;
            this.f3409b.setStrokeCap(Cap.SQUARE);
            this.f3409b.setAntiAlias(true);
            this.f3409b.setStyle(Style.STROKE);
            this.f3410c.setStyle(Style.FILL);
            this.f3410c.setAntiAlias(true);
        }

        /* renamed from: a */
        private int m3926a() {
            return (this.f3418k + 1) % this.f3417j.length;
        }

        /* renamed from: a */
        private void m3927a(Canvas canvas, float f, float f2, Rect rect) {
            if (this.f3422o) {
                if (this.f3423p == null) {
                    this.f3423p = new Path();
                    this.f3423p.setFillType(FillType.EVEN_ODD);
                } else {
                    this.f3423p.reset();
                }
                float f3 = ((float) (((int) this.f3416i) / 2)) * this.f3424q;
                float cos = (float) ((this.f3425r * Math.cos(0.0d)) + ((double) rect.exactCenterX()));
                float sin = (float) ((this.f3425r * Math.sin(0.0d)) + ((double) rect.exactCenterY()));
                this.f3423p.moveTo(0.0f, 0.0f);
                this.f3423p.lineTo(((float) this.f3426s) * this.f3424q, 0.0f);
                this.f3423p.lineTo((((float) this.f3426s) * this.f3424q) / 2.0f, ((float) this.f3427t) * this.f3424q);
                this.f3423p.offset(cos - f3, sin);
                this.f3423p.close();
                this.f3410c.setColor(this.f3431x);
                canvas.rotate((f + f2) - 5.0f, rect.exactCenterX(), rect.exactCenterY());
                canvas.drawPath(this.f3423p, this.f3410c);
            }
        }

        /* renamed from: b */
        private void m3928b() {
            this.f3411d.invalidateDrawable(null);
        }

        public void draw(Canvas canvas, Rect rect) {
            RectF rectF = this.f3408a;
            rectF.set(rect);
            rectF.inset(this.f3416i, this.f3416i);
            float f = (this.f3412e + this.f3414g) * 360.0f;
            float f2 = ((this.f3413f + this.f3414g) * 360.0f) - f;
            this.f3409b.setColor(this.f3431x);
            canvas.drawArc(rectF, f, f2, false, this.f3409b);
            m3927a(canvas, f, f2, rect);
            if (this.f3428u < 255) {
                this.f3429v.setColor(this.f3430w);
                this.f3429v.setAlpha(255 - this.f3428u);
                canvas.drawCircle(rect.exactCenterX(), rect.exactCenterY(), (float) (rect.width() / 2), this.f3429v);
            }
        }

        public int getAlpha() {
            return this.f3428u;
        }

        public double getCenterRadius() {
            return this.f3425r;
        }

        public float getEndTrim() {
            return this.f3413f;
        }

        public float getInsets() {
            return this.f3416i;
        }

        public int getNextColor() {
            return this.f3417j[m3926a()];
        }

        public float getRotation() {
            return this.f3414g;
        }

        public float getStartTrim() {
            return this.f3412e;
        }

        public int getStartingColor() {
            return this.f3417j[this.f3418k];
        }

        public float getStartingEndTrim() {
            return this.f3420m;
        }

        public float getStartingRotation() {
            return this.f3421n;
        }

        public float getStartingStartTrim() {
            return this.f3419l;
        }

        public float getStrokeWidth() {
            return this.f3415h;
        }

        public void goToNextColor() {
            setColorIndex(m3926a());
        }

        public void resetOriginals() {
            this.f3419l = 0.0f;
            this.f3420m = 0.0f;
            this.f3421n = 0.0f;
            setStartTrim(0.0f);
            setEndTrim(0.0f);
            setRotation(0.0f);
        }

        public void setAlpha(int i) {
            this.f3428u = i;
        }

        public void setArrowDimensions(float f, float f2) {
            this.f3426s = (int) f;
            this.f3427t = (int) f2;
        }

        public void setArrowScale(float f) {
            if (f != this.f3424q) {
                this.f3424q = f;
                m3928b();
            }
        }

        public void setBackgroundColor(int i) {
            this.f3430w = i;
        }

        public void setCenterRadius(double d) {
            this.f3425r = d;
        }

        public void setColor(int i) {
            this.f3431x = i;
        }

        public void setColorFilter(ColorFilter colorFilter) {
            this.f3409b.setColorFilter(colorFilter);
            m3928b();
        }

        public void setColorIndex(int i) {
            this.f3418k = i;
            this.f3431x = this.f3417j[this.f3418k];
        }

        public void setColors(int[] iArr) {
            this.f3417j = iArr;
            setColorIndex(0);
        }

        public void setEndTrim(float f) {
            this.f3413f = f;
            m3928b();
        }

        public void setInsets(int i, int i2) {
            float min = (float) Math.min(i, i2);
            this.f3416i = (this.f3425r <= 0.0d || min < 0.0f) ? (float) Math.ceil((double) (this.f3415h / 2.0f)) : (float) (((double) (min / 2.0f)) - this.f3425r);
        }

        public void setRotation(float f) {
            this.f3414g = f;
            m3928b();
        }

        public void setShowArrow(boolean z) {
            if (this.f3422o != z) {
                this.f3422o = z;
                m3928b();
            }
        }

        public void setStartTrim(float f) {
            this.f3412e = f;
            m3928b();
        }

        public void setStrokeWidth(float f) {
            this.f3415h = f;
            this.f3409b.setStrokeWidth(f);
            m3928b();
        }

        public void storeOriginals() {
            this.f3419l = this.f3412e;
            this.f3420m = this.f3413f;
            this.f3421n = this.f3414g;
        }
    }

    MaterialProgressDrawable(Context context, View view) {
        this.f3398j = view;
        this.f3397i = context.getResources();
        this.f3395g = new Ring(this.f3402n);
        this.f3395g.setColors(f3391e);
        updateSizes(1);
        m3920a();
    }

    /* renamed from: a */
    private int m3919a(float f, int i, int i2) {
        int intValue = Integer.valueOf(i).intValue();
        int i3 = (intValue >> 24) & 255;
        int i4 = (intValue >> 16) & 255;
        int i5 = (intValue >> 8) & 255;
        int i6 = intValue & 255;
        int intValue2 = Integer.valueOf(i2).intValue();
        return ((((int) (((float) (((intValue2 >> 24) & 255) - i3)) * f)) + i3) << 24) | ((((int) (((float) (((intValue2 >> 16) & 255) - i4)) * f)) + i4) << 16) | ((((int) (((float) (((intValue2 >> 8) & 255) - i5)) * f)) + i5) << 8) | (((int) (((float) ((intValue2 & 255) - i6)) * f)) + i6);
    }

    /* renamed from: a */
    private void m3920a() {
        final Ring ring = this.f3395g;
        C05071 r0 = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                if (MaterialProgressDrawable.this.f3393c) {
                    MaterialProgressDrawable.this.mo3170b(f, ring);
                    return;
                }
                float a = MaterialProgressDrawable.this.mo3167a(ring);
                float startingEndTrim = ring.getStartingEndTrim();
                float startingStartTrim = ring.getStartingStartTrim();
                float startingRotation = ring.getStartingRotation();
                MaterialProgressDrawable.this.mo3169a(f, ring);
                if (f <= 0.5f) {
                    ring.setStartTrim(startingStartTrim + ((0.8f - a) * MaterialProgressDrawable.f3389a.getInterpolation(f / 0.5f)));
                }
                if (f > 0.5f) {
                    ring.setEndTrim(startingEndTrim + (MaterialProgressDrawable.f3389a.getInterpolation((f - 0.5f) / 0.5f) * (0.8f - a)));
                }
                ring.setRotation(startingRotation + (0.25f * f));
                MaterialProgressDrawable.this.mo3168a((216.0f * f) + (1080.0f * (MaterialProgressDrawable.this.f3392b / 5.0f)));
            }
        };
        r0.setRepeatCount(-1);
        r0.setRepeatMode(1);
        r0.setInterpolator(f3390d);
        r0.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
                ring.storeOriginals();
                ring.goToNextColor();
                ring.setStartTrim(ring.getEndTrim());
                if (MaterialProgressDrawable.this.f3393c) {
                    MaterialProgressDrawable.this.f3393c = false;
                    animation.setDuration(1332);
                    ring.setShowArrow(false);
                    return;
                }
                MaterialProgressDrawable.this.f3392b = (MaterialProgressDrawable.this.f3392b + 1.0f) % 5.0f;
            }

            public void onAnimationStart(Animation animation) {
                MaterialProgressDrawable.this.f3392b = 0.0f;
            }
        });
        this.f3399k = r0;
    }

    /* renamed from: a */
    private void m3921a(double d, double d2, double d3, double d4, float f, float f2) {
        Ring ring = this.f3395g;
        float f3 = this.f3397i.getDisplayMetrics().density;
        this.f3400l = ((double) f3) * d;
        this.f3401m = ((double) f3) * d2;
        ring.setStrokeWidth(((float) d4) * f3);
        ring.setCenterRadius(((double) f3) * d3);
        ring.setColorIndex(0);
        ring.setArrowDimensions(f * f3, f2 * f3);
        ring.setInsets((int) this.f3400l, (int) this.f3401m);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public float mo3167a(Ring ring) {
        return (float) Math.toRadians(((double) ring.getStrokeWidth()) / (6.283185307179586d * ring.getCenterRadius()));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3168a(float f) {
        this.f3396h = f;
        invalidateSelf();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo3169a(float f, Ring ring) {
        if (f > 0.75f) {
            ring.setColor(m3919a((f - 0.75f) / 0.25f, ring.getStartingColor(), ring.getNextColor()));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo3170b(float f, Ring ring) {
        mo3169a(f, ring);
        float floor = (float) (Math.floor((double) (ring.getStartingRotation() / 0.8f)) + 1.0d);
        ring.setStartTrim(ring.getStartingStartTrim() + (((ring.getStartingEndTrim() - mo3167a(ring)) - ring.getStartingStartTrim()) * f));
        ring.setEndTrim(ring.getStartingEndTrim());
        ring.setRotation(ring.getStartingRotation() + ((floor - ring.getStartingRotation()) * f));
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        int save = canvas.save();
        canvas.rotate(this.f3396h, bounds.exactCenterX(), bounds.exactCenterY());
        this.f3395g.draw(canvas, bounds);
        canvas.restoreToCount(save);
    }

    public int getAlpha() {
        return this.f3395g.getAlpha();
    }

    public int getIntrinsicHeight() {
        return (int) this.f3401m;
    }

    public int getIntrinsicWidth() {
        return (int) this.f3400l;
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        ArrayList<Animation> arrayList = this.f3394f;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Animation animation = (Animation) arrayList.get(i);
            if (animation.hasStarted() && !animation.hasEnded()) {
                return true;
            }
        }
        return false;
    }

    public void setAlpha(int i) {
        this.f3395g.setAlpha(i);
    }

    public void setArrowScale(float f) {
        this.f3395g.setArrowScale(f);
    }

    public void setBackgroundColor(int i) {
        this.f3395g.setBackgroundColor(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f3395g.setColorFilter(colorFilter);
    }

    public void setColorSchemeColors(int... iArr) {
        this.f3395g.setColors(iArr);
        this.f3395g.setColorIndex(0);
    }

    public void setProgressRotation(float f) {
        this.f3395g.setRotation(f);
    }

    public void setStartEndTrim(float f, float f2) {
        this.f3395g.setStartTrim(f);
        this.f3395g.setEndTrim(f2);
    }

    public void showArrow(boolean z) {
        this.f3395g.setShowArrow(z);
    }

    public void start() {
        this.f3399k.reset();
        this.f3395g.storeOriginals();
        if (this.f3395g.getEndTrim() != this.f3395g.getStartTrim()) {
            this.f3393c = true;
            this.f3399k.setDuration(666);
            this.f3398j.startAnimation(this.f3399k);
            return;
        }
        this.f3395g.setColorIndex(0);
        this.f3395g.resetOriginals();
        this.f3399k.setDuration(1332);
        this.f3398j.startAnimation(this.f3399k);
    }

    public void stop() {
        this.f3398j.clearAnimation();
        mo3168a(0.0f);
        this.f3395g.setShowArrow(false);
        this.f3395g.setColorIndex(0);
        this.f3395g.resetOriginals();
    }

    public void updateSizes(int i) {
        if (i == 0) {
            m3921a(56.0d, 56.0d, 12.5d, 3.0d, 12.0f, 6.0f);
        } else {
            m3921a(40.0d, 40.0d, 8.75d, 2.5d, 10.0f, 5.0f);
        }
    }
}
