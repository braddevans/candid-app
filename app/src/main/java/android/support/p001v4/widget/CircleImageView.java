package android.support.p001v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build.VERSION;
import android.view.View;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

/* renamed from: android.support.v4.widget.CircleImageView */
class CircleImageView extends ImageView {

    /* renamed from: a */
    int f3292a;

    /* renamed from: b */
    private AnimationListener f3293b;

    /* renamed from: android.support.v4.widget.CircleImageView$OvalShadow */
    class OvalShadow extends OvalShape {

        /* renamed from: b */
        private RadialGradient f3295b;

        /* renamed from: c */
        private Paint f3296c = new Paint();

        OvalShadow(int i) {
            CircleImageView.this.f3292a = i;
            m3841a((int) rect().width());
        }

        /* renamed from: a */
        private void m3841a(int i) {
            this.f3295b = new RadialGradient((float) (i / 2), (float) (i / 2), (float) CircleImageView.this.f3292a, new int[]{1023410176, 0}, null, TileMode.CLAMP);
            this.f3296c.setShader(this.f3295b);
        }

        public void draw(Canvas canvas, Paint paint) {
            int width = CircleImageView.this.getWidth();
            int height = CircleImageView.this.getHeight();
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.f3296c);
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) ((width / 2) - CircleImageView.this.f3292a), paint);
        }

        /* access modifiers changed from: protected */
        public void onResize(float f, float f2) {
            super.onResize(f, f2);
            m3841a((int) f);
        }
    }

    CircleImageView(Context context, int i) {
        ShapeDrawable shapeDrawable;
        super(context);
        float f = getContext().getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f);
        int i3 = (int) (0.0f * f);
        this.f3292a = (int) (3.5f * f);
        if (m3840a()) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            ViewCompat.m12920f((View) this, 4.0f * f);
        } else {
            shapeDrawable = new ShapeDrawable(new OvalShadow(this.f3292a));
            ViewCompat.m12888a((View) this, 1, shapeDrawable.getPaint());
            shapeDrawable.getPaint().setShadowLayer((float) this.f3292a, (float) i3, (float) i2, 503316480);
            int i4 = this.f3292a;
            setPadding(i4, i4, i4, i4);
        }
        shapeDrawable.getPaint().setColor(i);
        ViewCompat.m12892a((View) this, (Drawable) shapeDrawable);
    }

    /* renamed from: a */
    private boolean m3840a() {
        return VERSION.SDK_INT >= 21;
    }

    public void onAnimationEnd() {
        super.onAnimationEnd();
        if (this.f3293b != null) {
            this.f3293b.onAnimationEnd(getAnimation());
        }
    }

    public void onAnimationStart() {
        super.onAnimationStart();
        if (this.f3293b != null) {
            this.f3293b.onAnimationStart(getAnimation());
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!m3840a()) {
            setMeasuredDimension(getMeasuredWidth() + (this.f3292a * 2), getMeasuredHeight() + (this.f3292a * 2));
        }
    }

    public void setAnimationListener(AnimationListener animationListener) {
        this.f3293b = animationListener;
    }

    public void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }

    public void setBackgroundColorRes(int i) {
        setBackgroundColor(ContextCompat.getColor(getContext(), i));
    }
}
