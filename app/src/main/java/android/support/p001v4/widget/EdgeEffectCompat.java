package android.support.p001v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build.VERSION;

/* renamed from: android.support.v4.widget.EdgeEffectCompat */
public final class EdgeEffectCompat {

    /* renamed from: b */
    private static final EdgeEffectImpl f3367b;

    /* renamed from: a */
    private Object f3368a;

    /* renamed from: android.support.v4.widget.EdgeEffectCompat$BaseEdgeEffectImpl */
    static class BaseEdgeEffectImpl implements EdgeEffectImpl {
        BaseEdgeEffectImpl() {
        }

        public boolean draw(Object obj, Canvas canvas) {
            return false;
        }

        public void finish(Object obj) {
        }

        public boolean isFinished(Object obj) {
            return true;
        }

        public Object newEdgeEffect(Context context) {
            return null;
        }

        public boolean onAbsorb(Object obj, int i) {
            return false;
        }

        public boolean onPull(Object obj, float f) {
            return false;
        }

        public boolean onPull(Object obj, float f, float f2) {
            return false;
        }

        public boolean onRelease(Object obj) {
            return false;
        }

        public void setSize(Object obj, int i, int i2) {
        }
    }

    /* renamed from: android.support.v4.widget.EdgeEffectCompat$EdgeEffectIcsImpl */
    static class EdgeEffectIcsImpl implements EdgeEffectImpl {
        EdgeEffectIcsImpl() {
        }

        public boolean draw(Object obj, Canvas canvas) {
            return EdgeEffectCompatIcs.draw(obj, canvas);
        }

        public void finish(Object obj) {
            EdgeEffectCompatIcs.finish(obj);
        }

        public boolean isFinished(Object obj) {
            return EdgeEffectCompatIcs.isFinished(obj);
        }

        public Object newEdgeEffect(Context context) {
            return EdgeEffectCompatIcs.newEdgeEffect(context);
        }

        public boolean onAbsorb(Object obj, int i) {
            return EdgeEffectCompatIcs.onAbsorb(obj, i);
        }

        public boolean onPull(Object obj, float f) {
            return EdgeEffectCompatIcs.onPull(obj, f);
        }

        public boolean onPull(Object obj, float f, float f2) {
            return EdgeEffectCompatIcs.onPull(obj, f);
        }

        public boolean onRelease(Object obj) {
            return EdgeEffectCompatIcs.onRelease(obj);
        }

        public void setSize(Object obj, int i, int i2) {
            EdgeEffectCompatIcs.setSize(obj, i, i2);
        }
    }

    /* renamed from: android.support.v4.widget.EdgeEffectCompat$EdgeEffectImpl */
    interface EdgeEffectImpl {
        boolean draw(Object obj, Canvas canvas);

        void finish(Object obj);

        boolean isFinished(Object obj);

        Object newEdgeEffect(Context context);

        boolean onAbsorb(Object obj, int i);

        boolean onPull(Object obj, float f);

        boolean onPull(Object obj, float f, float f2);

        boolean onRelease(Object obj);

        void setSize(Object obj, int i, int i2);
    }

    /* renamed from: android.support.v4.widget.EdgeEffectCompat$EdgeEffectLollipopImpl */
    static class EdgeEffectLollipopImpl extends EdgeEffectIcsImpl {
        EdgeEffectLollipopImpl() {
        }

        public boolean onPull(Object obj, float f, float f2) {
            return EdgeEffectCompatLollipop.onPull(obj, f, f2);
        }
    }

    static {
        if (VERSION.SDK_INT >= 21) {
            f3367b = new EdgeEffectLollipopImpl();
        } else if (VERSION.SDK_INT >= 14) {
            f3367b = new EdgeEffectIcsImpl();
        } else {
            f3367b = new BaseEdgeEffectImpl();
        }
    }

    public EdgeEffectCompat(Context context) {
        this.f3368a = f3367b.newEdgeEffect(context);
    }

    public boolean draw(Canvas canvas) {
        return f3367b.draw(this.f3368a, canvas);
    }

    public void finish() {
        f3367b.finish(this.f3368a);
    }

    public boolean isFinished() {
        return f3367b.isFinished(this.f3368a);
    }

    public boolean onAbsorb(int i) {
        return f3367b.onAbsorb(this.f3368a, i);
    }

    @Deprecated
    public boolean onPull(float f) {
        return f3367b.onPull(this.f3368a, f);
    }

    public boolean onPull(float f, float f2) {
        return f3367b.onPull(this.f3368a, f, f2);
    }

    public boolean onRelease() {
        return f3367b.onRelease(this.f3368a);
    }

    public void setSize(int i, int i2) {
        f3367b.setSize(this.f3368a, i, i2);
    }
}
