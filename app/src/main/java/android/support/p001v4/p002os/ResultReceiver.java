package android.support.p001v4.p002os;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

/* renamed from: android.support.v4.os.ResultReceiver */
public class ResultReceiver implements Parcelable {
    public static final Creator<ResultReceiver> CREATOR = new Creator<ResultReceiver>() {
        /* renamed from: a */
        public ResultReceiver createFromParcel(Parcel parcel) {
            return new ResultReceiver(parcel);
        }

        /* renamed from: a */
        public ResultReceiver[] newArray(int i) {
            return new ResultReceiver[i];
        }
    };

    /* renamed from: a */
    final boolean f3138a = false;

    /* renamed from: b */
    final Handler f3139b = null;

    /* renamed from: c */
    IResultReceiver f3140c;

    /* renamed from: android.support.v4.os.ResultReceiver$a */
    class C0478a extends C2260a {
        C0478a() {
        }

        /* renamed from: a */
        public void mo2836a(int i, Bundle bundle) {
            if (ResultReceiver.this.f3139b != null) {
                ResultReceiver.this.f3139b.post(new C0479b(i, bundle));
            } else {
                ResultReceiver.this.mo2829b(i, bundle);
            }
        }
    }

    /* renamed from: android.support.v4.os.ResultReceiver$b */
    class C0479b implements Runnable {

        /* renamed from: a */
        final int f3142a;

        /* renamed from: b */
        final Bundle f3143b;

        C0479b(int i, Bundle bundle) {
            this.f3142a = i;
            this.f3143b = bundle;
        }

        public void run() {
            ResultReceiver.this.mo2829b(this.f3142a, this.f3143b);
        }
    }

    ResultReceiver(Parcel parcel) {
        this.f3140c = C2260a.m12484a(parcel.readStrongBinder());
    }

    /* renamed from: a */
    public void mo2828a(int i, Bundle bundle) {
        if (this.f3138a) {
            if (this.f3139b != null) {
                this.f3139b.post(new C0479b(i, bundle));
            } else {
                mo2829b(i, bundle);
            }
        } else if (this.f3140c != null) {
            try {
                this.f3140c.mo2836a(i, bundle);
            } catch (RemoteException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo2829b(int i, Bundle bundle) {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.f3140c == null) {
                this.f3140c = new C0478a();
            }
            parcel.writeStrongBinder(this.f3140c.asBinder());
        }
    }
}
