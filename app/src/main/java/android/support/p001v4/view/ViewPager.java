package android.support.p001v4.view;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.p001v4.app.FragmentTransaction;
import android.support.p001v4.widget.EdgeEffectCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: android.support.v4.view.ViewPager */
public class ViewPager extends ViewGroup {

    /* renamed from: a */
    static final int[] f3182a = {16842931};

    /* renamed from: aj */
    private static final C0500h f3183aj = new C0500h();

    /* renamed from: e */
    private static final Comparator<C0494b> f3184e = new Comparator<C0494b>() {
        /* renamed from: a */
        public int compare(C0494b bVar, C0494b bVar2) {
            return bVar.f3256b - bVar2.f3256b;
        }
    };

    /* renamed from: f */
    private static final Interpolator f3185f = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };

    /* renamed from: A */
    private int f3186A = 1;

    /* renamed from: B */
    private boolean f3187B;

    /* renamed from: C */
    private boolean f3188C;

    /* renamed from: D */
    private int f3189D;

    /* renamed from: E */
    private int f3190E;

    /* renamed from: F */
    private int f3191F;

    /* renamed from: G */
    private float f3192G;

    /* renamed from: H */
    private float f3193H;

    /* renamed from: I */
    private float f3194I;

    /* renamed from: J */
    private float f3195J;

    /* renamed from: K */
    private int f3196K = -1;

    /* renamed from: L */
    private VelocityTracker f3197L;

    /* renamed from: M */
    private int f3198M;

    /* renamed from: N */
    private int f3199N;

    /* renamed from: O */
    private int f3200O;

    /* renamed from: P */
    private int f3201P;

    /* renamed from: Q */
    private boolean f3202Q;

    /* renamed from: R */
    private EdgeEffectCompat f3203R;

    /* renamed from: S */
    private EdgeEffectCompat f3204S;

    /* renamed from: T */
    private boolean f3205T = true;

    /* renamed from: U */
    private boolean f3206U = false;

    /* renamed from: V */
    private boolean f3207V;

    /* renamed from: W */
    private int f3208W;

    /* renamed from: aa */
    private List<C0497e> f3209aa;

    /* renamed from: ab */
    private C0497e f3210ab;

    /* renamed from: ac */
    private C0497e f3211ac;

    /* renamed from: ad */
    private List<C0496d> f3212ad;

    /* renamed from: ae */
    private C0498f f3213ae;

    /* renamed from: af */
    private int f3214af;

    /* renamed from: ag */
    private Method f3215ag;

    /* renamed from: ah */
    private int f3216ah;

    /* renamed from: ai */
    private ArrayList<View> f3217ai;

    /* renamed from: ak */
    private final Runnable f3218ak = new Runnable() {
        public void run() {
            ViewPager.this.setScrollState(0);
            ViewPager.this.mo2896c();
        }
    };

    /* renamed from: al */
    private int f3219al = 0;

    /* renamed from: b */
    PagerAdapter f3220b;

    /* renamed from: c */
    int f3221c;

    /* renamed from: d */
    private int f3222d;

    /* renamed from: g */
    private final ArrayList<C0494b> f3223g = new ArrayList<>();

    /* renamed from: h */
    private final C0494b f3224h = new C0494b();

    /* renamed from: i */
    private final Rect f3225i = new Rect();

    /* renamed from: j */
    private int f3226j = -1;

    /* renamed from: k */
    private Parcelable f3227k = null;

    /* renamed from: l */
    private ClassLoader f3228l = null;

    /* renamed from: m */
    private Scroller f3229m;

    /* renamed from: n */
    private boolean f3230n;

    /* renamed from: o */
    private C0499g f3231o;

    /* renamed from: p */
    private int f3232p;

    /* renamed from: q */
    private Drawable f3233q;

    /* renamed from: r */
    private int f3234r;

    /* renamed from: s */
    private int f3235s;

    /* renamed from: t */
    private float f3236t = -3.4028235E38f;

    /* renamed from: u */
    private float f3237u = Float.MAX_VALUE;

    /* renamed from: v */
    private int f3238v;

    /* renamed from: w */
    private int f3239w;

    /* renamed from: x */
    private boolean f3240x;

    /* renamed from: y */
    private boolean f3241y;

    /* renamed from: z */
    private boolean f3242z;

    /* renamed from: android.support.v4.view.ViewPager$LayoutParams */
    public static class LayoutParams extends android.view.ViewGroup.LayoutParams {

        /* renamed from: a */
        public boolean f3246a;

        /* renamed from: b */
        public int f3247b;

        /* renamed from: c */
        float f3248c = 0.0f;

        /* renamed from: d */
        boolean f3249d;

        /* renamed from: e */
        int f3250e;

        /* renamed from: f */
        int f3251f;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.f3182a);
            this.f3247b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: android.support.v4.view.ViewPager$SavedState */
    public static class SavedState extends AbsSavedState {
        public static final Creator<SavedState> CREATOR = ParcelableCompat.m12486a(new ParcelableCompatCreatorCallbacks<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a */
        int f3252a;

        /* renamed from: b */
        Parcelable f3253b;

        /* renamed from: c */
        ClassLoader f3254c;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            if (classLoader == null) {
                classLoader = getClass().getClassLoader();
            }
            this.f3252a = parcel.readInt();
            this.f3253b = parcel.readParcelable(classLoader);
            this.f3254c = classLoader;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.f3252a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f3252a);
            parcel.writeParcelable(this.f3253b, i);
        }
    }

    @Inherited
    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    /* renamed from: android.support.v4.view.ViewPager$a */
    public @interface C0493a {
    }

    /* renamed from: android.support.v4.view.ViewPager$b */
    static class C0494b {

        /* renamed from: a */
        Object f3255a;

        /* renamed from: b */
        int f3256b;

        /* renamed from: c */
        boolean f3257c;

        /* renamed from: d */
        float f3258d;

        /* renamed from: e */
        float f3259e;

        C0494b() {
        }
    }

    /* renamed from: android.support.v4.view.ViewPager$c */
    class C0495c extends AccessibilityDelegateCompat {
        C0495c() {
        }

        /* renamed from: a */
        private boolean m3825a() {
            return ViewPager.this.f3220b != null && ViewPager.this.f3220b.getCount() > 1;
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(ViewPager.class.getName());
            AccessibilityRecordCompat a = AccessibilityEventCompat.m13397a(accessibilityEvent);
            a.mo13732d(m3825a());
            if (accessibilityEvent.getEventType() == 4096 && ViewPager.this.f3220b != null) {
                a.mo13722a(ViewPager.this.f3220b.getCount());
                a.mo13726b(ViewPager.this.f3221c);
                a.mo13729c(ViewPager.this.f3221c);
            }
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat ggVar) {
            super.onInitializeAccessibilityNodeInfo(view, ggVar);
            ggVar.mo13595b((CharSequence) ViewPager.class.getName());
            ggVar.mo13621i(m3825a());
            if (ViewPager.this.canScrollHorizontally(1)) {
                ggVar.mo13585a(4096);
            }
            if (ViewPager.this.canScrollHorizontally(-1)) {
                ggVar.mo13585a((int) FragmentTransaction.TRANSIT_EXIT_MASK);
            }
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            if (super.performAccessibilityAction(view, i, bundle)) {
                return true;
            }
            switch (i) {
                case 4096:
                    if (!ViewPager.this.canScrollHorizontally(1)) {
                        return false;
                    }
                    ViewPager.this.setCurrentItem(ViewPager.this.f3221c + 1);
                    return true;
                case FragmentTransaction.TRANSIT_EXIT_MASK /*8192*/:
                    if (!ViewPager.this.canScrollHorizontally(-1)) {
                        return false;
                    }
                    ViewPager.this.setCurrentItem(ViewPager.this.f3221c - 1);
                    return true;
                default:
                    return false;
            }
        }
    }

    /* renamed from: android.support.v4.view.ViewPager$d */
    public interface C0496d {
        /* renamed from: a */
        void mo2871a(ViewPager viewPager, PagerAdapter evVar, PagerAdapter evVar2);
    }

    /* renamed from: android.support.v4.view.ViewPager$e */
    public interface C0497e {
        void onPageScrollStateChanged(int i);

        void onPageScrolled(int i, float f, int i2);

        void onPageSelected(int i);
    }

    /* renamed from: android.support.v4.view.ViewPager$f */
    public interface C0498f {
        /* renamed from: a */
        void mo2948a(View view, float f);
    }

    /* renamed from: android.support.v4.view.ViewPager$g */
    class C0499g extends DataSetObserver {
        C0499g() {
        }

        public void onChanged() {
            ViewPager.this.mo2892b();
        }

        public void onInvalidated() {
            ViewPager.this.mo2892b();
        }
    }

    /* renamed from: android.support.v4.view.ViewPager$h */
    static class C0500h implements Comparator<View> {
        C0500h() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
            return layoutParams.f3246a != layoutParams2.f3246a ? layoutParams.f3246a ? 1 : -1 : layoutParams.f3250e - layoutParams2.f3250e;
        }
    }

    public ViewPager(Context context) {
        super(context);
        mo2877a();
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo2877a();
    }

    /* renamed from: a */
    private int m3778a(int i, float f, int i2, int i3) {
        int i4;
        if (Math.abs(i3) <= this.f3200O || Math.abs(i2) <= this.f3198M) {
            i4 = i + ((int) (f + (i >= this.f3221c ? 0.4f : 0.6f)));
        } else {
            i4 = i2 > 0 ? i : i + 1;
        }
        if (this.f3223g.size() <= 0) {
            return i4;
        }
        return Math.max(((C0494b) this.f3223g.get(0)).f3256b, Math.min(i4, ((C0494b) this.f3223g.get(this.f3223g.size() - 1)).f3256b));
    }

    /* renamed from: a */
    private Rect m3779a(Rect rect, View view) {
        if (rect == null) {
            rect = new Rect();
        }
        if (view == null) {
            rect.set(0, 0, 0, 0);
        } else {
            rect.left = view.getLeft();
            rect.right = view.getRight();
            rect.top = view.getTop();
            rect.bottom = view.getBottom();
            ViewParent parent = view.getParent();
            while ((parent instanceof ViewGroup) && parent != this) {
                ViewGroup viewGroup = (ViewGroup) parent;
                rect.left += viewGroup.getLeft();
                rect.right += viewGroup.getRight();
                rect.top += viewGroup.getTop();
                rect.bottom += viewGroup.getBottom();
                parent = viewGroup.getParent();
            }
        }
        return rect;
    }

    /* renamed from: a */
    private void m3780a(int i, int i2, int i3, int i4) {
        if (i2 <= 0 || this.f3223g.isEmpty()) {
            C0494b b = mo2890b(this.f3221c);
            int paddingLeft = (int) (((float) ((i - getPaddingLeft()) - getPaddingRight())) * (b != null ? Math.min(b.f3259e, this.f3237u) : 0.0f));
            if (paddingLeft != getScrollX()) {
                m3784a(false);
                scrollTo(paddingLeft, getScrollY());
            }
        } else if (!this.f3229m.isFinished()) {
            this.f3229m.setFinalX(getCurrentItem() * getClientWidth());
        } else {
            scrollTo((int) (((float) (((i - getPaddingLeft()) - getPaddingRight()) + i3)) * (((float) getScrollX()) / ((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4)))), getScrollY());
        }
    }

    /* renamed from: a */
    private void m3781a(int i, boolean z, int i2, boolean z2) {
        C0494b b = mo2890b(i);
        int i3 = 0;
        if (b != null) {
            i3 = (int) (((float) getClientWidth()) * Math.max(this.f3236t, Math.min(b.f3259e, this.f3237u)));
        }
        if (z) {
            mo2880a(i3, 0, i2);
            if (z2) {
                m3792e(i);
                return;
            }
            return;
        }
        if (z2) {
            m3792e(i);
        }
        m3784a(false);
        scrollTo(i3, 0);
        m3791d(i3);
    }

    /* renamed from: a */
    private void m3782a(C0494b bVar, int i, C0494b bVar2) {
        C0494b bVar3;
        C0494b bVar4;
        int count = this.f3220b.getCount();
        int clientWidth = getClientWidth();
        float f = clientWidth > 0 ? ((float) this.f3232p) / ((float) clientWidth) : 0.0f;
        if (bVar2 != null) {
            int i2 = bVar2.f3256b;
            if (i2 < bVar.f3256b) {
                int i3 = 0;
                float f2 = bVar2.f3259e + bVar2.f3258d + f;
                int i4 = i2 + 1;
                while (i4 <= bVar.f3256b && i3 < this.f3223g.size()) {
                    Object obj = this.f3223g.get(i3);
                    while (true) {
                        bVar4 = (C0494b) obj;
                        if (i4 > bVar4.f3256b && i3 < this.f3223g.size() - 1) {
                            i3++;
                            obj = this.f3223g.get(i3);
                        }
                    }
                    while (i4 < bVar4.f3256b) {
                        f2 += this.f3220b.getPageWidth(i4) + f;
                        i4++;
                    }
                    bVar4.f3259e = f2;
                    f2 += bVar4.f3258d + f;
                    i4++;
                }
            } else if (i2 > bVar.f3256b) {
                int size = this.f3223g.size() - 1;
                float f3 = bVar2.f3259e;
                int i5 = i2 - 1;
                while (i5 >= bVar.f3256b && size >= 0) {
                    Object obj2 = this.f3223g.get(size);
                    while (true) {
                        bVar3 = (C0494b) obj2;
                        if (i5 < bVar3.f3256b && size > 0) {
                            size--;
                            obj2 = this.f3223g.get(size);
                        }
                    }
                    while (i5 > bVar3.f3256b) {
                        f3 -= this.f3220b.getPageWidth(i5) + f;
                        i5--;
                    }
                    f3 -= bVar3.f3258d + f;
                    bVar3.f3259e = f3;
                    i5--;
                }
            }
        }
        int size2 = this.f3223g.size();
        float f4 = bVar.f3259e;
        int i6 = bVar.f3256b - 1;
        this.f3236t = bVar.f3256b == 0 ? bVar.f3259e : -3.4028235E38f;
        this.f3237u = bVar.f3256b == count + -1 ? (bVar.f3259e + bVar.f3258d) - 1.0f : Float.MAX_VALUE;
        int i7 = i - 1;
        while (i7 >= 0) {
            C0494b bVar5 = (C0494b) this.f3223g.get(i7);
            while (i6 > bVar5.f3256b) {
                f4 -= this.f3220b.getPageWidth(i6) + f;
                i6--;
            }
            f4 -= bVar5.f3258d + f;
            bVar5.f3259e = f4;
            if (bVar5.f3256b == 0) {
                this.f3236t = f4;
            }
            i7--;
            i6--;
        }
        float f5 = bVar.f3259e + bVar.f3258d + f;
        int i8 = bVar.f3256b + 1;
        int i9 = i + 1;
        while (i9 < size2) {
            C0494b bVar6 = (C0494b) this.f3223g.get(i9);
            while (i8 < bVar6.f3256b) {
                f5 += this.f3220b.getPageWidth(i8) + f;
                i8++;
            }
            if (bVar6.f3256b == count - 1) {
                this.f3237u = (bVar6.f3258d + f5) - 1.0f;
            }
            bVar6.f3259e = f5;
            f5 += bVar6.f3258d + f;
            i9++;
            i8++;
        }
        this.f3206U = false;
    }

    /* renamed from: a */
    private void m3783a(MotionEvent motionEvent) {
        int b = MotionEventCompat.m12832b(motionEvent);
        if (motionEvent.getPointerId(b) == this.f3196K) {
            int i = b == 0 ? 1 : 0;
            this.f3192G = motionEvent.getX(i);
            this.f3196K = motionEvent.getPointerId(i);
            if (this.f3197L != null) {
                this.f3197L.clear();
            }
        }
    }

    /* renamed from: a */
    private void m3784a(boolean z) {
        boolean z2 = true;
        boolean z3 = this.f3219al == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            if (this.f3229m.isFinished()) {
                z2 = false;
            }
            if (z2) {
                this.f3229m.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.f3229m.getCurrX();
                int currY = this.f3229m.getCurrY();
                if (!(scrollX == currX && scrollY == currY)) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        m3791d(currX);
                    }
                }
            }
        }
        this.f3242z = false;
        for (int i = 0; i < this.f3223g.size(); i++) {
            C0494b bVar = (C0494b) this.f3223g.get(i);
            if (bVar.f3257c) {
                z3 = true;
                bVar.f3257c = false;
            }
        }
        if (!z3) {
            return;
        }
        if (z) {
            ViewCompat.m12897a((View) this, this.f3218ak);
        } else {
            this.f3218ak.run();
        }
    }

    /* renamed from: a */
    private boolean m3785a(float f, float f2) {
        return (f < ((float) this.f3190E) && f2 > 0.0f) || (f > ((float) (getWidth() - this.f3190E)) && f2 < 0.0f);
    }

    /* renamed from: b */
    private void m3786b(int i, float f, int i2) {
        if (this.f3210ab != null) {
            this.f3210ab.onPageScrolled(i, f, i2);
        }
        if (this.f3209aa != null) {
            int size = this.f3209aa.size();
            for (int i3 = 0; i3 < size; i3++) {
                C0497e eVar = (C0497e) this.f3209aa.get(i3);
                if (eVar != null) {
                    eVar.onPageScrolled(i, f, i2);
                }
            }
        }
        if (this.f3211ac != null) {
            this.f3211ac.onPageScrolled(i, f, i2);
        }
    }

    /* renamed from: b */
    private void m3787b(boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewCompat.m12888a(getChildAt(i), z ? this.f3214af : 0, (Paint) null);
        }
    }

    /* renamed from: b */
    private boolean m3788b(float f) {
        boolean z = false;
        float f2 = this.f3192G - f;
        this.f3192G = f;
        float scrollX = ((float) getScrollX()) + f2;
        int clientWidth = getClientWidth();
        float f3 = ((float) clientWidth) * this.f3236t;
        float f4 = ((float) clientWidth) * this.f3237u;
        boolean z2 = true;
        boolean z3 = true;
        C0494b bVar = (C0494b) this.f3223g.get(0);
        C0494b bVar2 = (C0494b) this.f3223g.get(this.f3223g.size() - 1);
        if (bVar.f3256b != 0) {
            z2 = false;
            f3 = bVar.f3259e * ((float) clientWidth);
        }
        if (bVar2.f3256b != this.f3220b.getCount() - 1) {
            z3 = false;
            f4 = bVar2.f3259e * ((float) clientWidth);
        }
        if (scrollX < f3) {
            if (z2) {
                z = this.f3203R.onPull(Math.abs(f3 - scrollX) / ((float) clientWidth));
            }
            scrollX = f3;
        } else if (scrollX > f4) {
            if (z3) {
                z = this.f3204S.onPull(Math.abs(scrollX - f4) / ((float) clientWidth));
            }
            scrollX = f4;
        }
        this.f3192G += scrollX - ((float) ((int) scrollX));
        scrollTo((int) scrollX, getScrollY());
        m3791d((int) scrollX);
        return z;
    }

    /* renamed from: c */
    private void m3789c(boolean z) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z);
        }
    }

    /* renamed from: c */
    private static boolean m3790c(View view) {
        return view.getClass().getAnnotation(C0493a.class) != null;
    }

    /* renamed from: d */
    private boolean m3791d(int i) {
        if (this.f3223g.size() != 0) {
            C0494b i2 = m3797i();
            int clientWidth = getClientWidth();
            int i3 = clientWidth + this.f3232p;
            float f = ((float) this.f3232p) / ((float) clientWidth);
            int i4 = i2.f3256b;
            float f2 = ((((float) i) / ((float) clientWidth)) - i2.f3259e) / (i2.f3258d + f);
            int i5 = (int) (((float) i3) * f2);
            this.f3207V = false;
            mo2879a(i4, f2, i5);
            if (this.f3207V) {
                return true;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        } else if (this.f3205T) {
            return false;
        } else {
            this.f3207V = false;
            mo2879a(0, 0.0f, 0);
            if (this.f3207V) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
    }

    /* renamed from: e */
    private void m3792e(int i) {
        if (this.f3210ab != null) {
            this.f3210ab.onPageSelected(i);
        }
        if (this.f3209aa != null) {
            int size = this.f3209aa.size();
            for (int i2 = 0; i2 < size; i2++) {
                C0497e eVar = (C0497e) this.f3209aa.get(i2);
                if (eVar != null) {
                    eVar.onPageSelected(i);
                }
            }
        }
        if (this.f3211ac != null) {
            this.f3211ac.onPageSelected(i);
        }
    }

    /* renamed from: f */
    private void m3793f() {
        int i = 0;
        while (i < getChildCount()) {
            if (!((LayoutParams) getChildAt(i).getLayoutParams()).f3246a) {
                removeViewAt(i);
                i--;
            }
            i++;
        }
    }

    /* renamed from: f */
    private void m3794f(int i) {
        if (this.f3210ab != null) {
            this.f3210ab.onPageScrollStateChanged(i);
        }
        if (this.f3209aa != null) {
            int size = this.f3209aa.size();
            for (int i2 = 0; i2 < size; i2++) {
                C0497e eVar = (C0497e) this.f3209aa.get(i2);
                if (eVar != null) {
                    eVar.onPageScrollStateChanged(i);
                }
            }
        }
        if (this.f3211ac != null) {
            this.f3211ac.onPageScrollStateChanged(i);
        }
    }

    /* renamed from: g */
    private void m3795g() {
        if (this.f3216ah != 0) {
            if (this.f3217ai == null) {
                this.f3217ai = new ArrayList<>();
            } else {
                this.f3217ai.clear();
            }
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                this.f3217ai.add(getChildAt(i));
            }
            Collections.sort(this.f3217ai, f3183aj);
        }
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    /* renamed from: h */
    private boolean m3796h() {
        this.f3196K = -1;
        m3798j();
        return this.f3203R.onRelease() | this.f3204S.onRelease();
    }

    /* renamed from: i */
    private C0494b m3797i() {
        float f = 0.0f;
        int clientWidth = getClientWidth();
        float f2 = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        if (clientWidth > 0) {
            f = ((float) this.f3232p) / ((float) clientWidth);
        }
        int i = -1;
        float f3 = 0.0f;
        float f4 = 0.0f;
        boolean z = true;
        C0494b bVar = null;
        int i2 = 0;
        while (i2 < this.f3223g.size()) {
            C0494b bVar2 = (C0494b) this.f3223g.get(i2);
            if (!z && bVar2.f3256b != i + 1) {
                bVar2 = this.f3224h;
                bVar2.f3259e = f3 + f4 + f;
                bVar2.f3256b = i + 1;
                bVar2.f3258d = this.f3220b.getPageWidth(bVar2.f3256b);
                i2--;
            }
            float f5 = bVar2.f3259e;
            float f6 = f5;
            float f7 = bVar2.f3258d + f5 + f;
            if (!z && f2 < f6) {
                return bVar;
            }
            if (f2 < f7 || i2 == this.f3223g.size() - 1) {
                return bVar2;
            }
            z = false;
            i = bVar2.f3256b;
            f3 = f5;
            f4 = bVar2.f3258d;
            bVar = bVar2;
            i2++;
        }
        return bVar;
    }

    /* renamed from: j */
    private void m3798j() {
        this.f3187B = false;
        this.f3188C = false;
        if (this.f3197L != null) {
            this.f3197L.recycle();
            this.f3197L = null;
        }
    }

    private void setScrollingCacheEnabled(boolean z) {
        if (this.f3241y != z) {
            this.f3241y = z;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public float mo2874a(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C0494b mo2875a(int i, int i2) {
        C0494b bVar = new C0494b();
        bVar.f3256b = i;
        bVar.f3255a = this.f3220b.instantiateItem((ViewGroup) this, i);
        bVar.f3258d = this.f3220b.getPageWidth(i);
        if (i2 < 0 || i2 >= this.f3223g.size()) {
            this.f3223g.add(bVar);
        } else {
            this.f3223g.add(i2, bVar);
        }
        return bVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public C0494b mo2876a(View view) {
        for (int i = 0; i < this.f3223g.size(); i++) {
            C0494b bVar = (C0494b) this.f3223g.get(i);
            if (this.f3220b.isViewFromObject(view, bVar.f3255a)) {
                return bVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2877a() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.f3229m = new Scroller(context, f3185f);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f = context.getResources().getDisplayMetrics().density;
        this.f3191F = viewConfiguration.getScaledPagingTouchSlop();
        this.f3198M = (int) (400.0f * f);
        this.f3199N = viewConfiguration.getScaledMaximumFlingVelocity();
        this.f3203R = new EdgeEffectCompat(context);
        this.f3204S = new EdgeEffectCompat(context);
        this.f3200O = (int) (25.0f * f);
        this.f3201P = (int) (2.0f * f);
        this.f3189D = (int) (16.0f * f);
        ViewCompat.m12894a((View) this, (AccessibilityDelegateCompat) new C0495c());
        if (ViewCompat.m12916e(this) == 0) {
            ViewCompat.m12910c((View) this, 1);
        }
        ViewCompat.m12895a((View) this, (OnApplyWindowInsetsListener) new OnApplyWindowInsetsListener() {

            /* renamed from: b */
            private final Rect f3245b = new Rect();

            public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat fzVar) {
                WindowInsetsCompat a = ViewCompat.m12884a(view, fzVar);
                if (a.mo13570e()) {
                    return a;
                }
                Rect rect = this.f3245b;
                rect.left = a.mo13565a();
                rect.top = a.mo13567b();
                rect.right = a.mo13568c();
                rect.bottom = a.mo13569d();
                int childCount = ViewPager.this.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    WindowInsetsCompat b = ViewCompat.m12903b(ViewPager.this.getChildAt(i), a);
                    rect.left = Math.min(b.mo13565a(), rect.left);
                    rect.top = Math.min(b.mo13567b(), rect.top);
                    rect.right = Math.min(b.mo13568c(), rect.right);
                    rect.bottom = Math.min(b.mo13569d(), rect.bottom);
                }
                return a.mo13566a(rect.left, rect.top, rect.right, rect.bottom);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2878a(int i) {
        String hexString;
        C0494b bVar = null;
        if (this.f3221c != i) {
            bVar = mo2890b(this.f3221c);
            this.f3221c = i;
        }
        if (this.f3220b == null) {
            m3795g();
        } else if (this.f3242z) {
            m3795g();
        } else if (getWindowToken() != null) {
            this.f3220b.startUpdate((ViewGroup) this);
            int i2 = this.f3186A;
            int max = Math.max(0, this.f3221c - i2);
            int count = this.f3220b.getCount();
            int min = Math.min(count - 1, this.f3221c + i2);
            if (count != this.f3222d) {
                try {
                    hexString = getResources().getResourceName(getId());
                } catch (NotFoundException e) {
                    hexString = Integer.toHexString(getId());
                }
                throw new IllegalStateException("The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: " + this.f3222d + ", found: " + count + " Pager id: " + hexString + " Pager class: " + getClass() + " Problematic adapter: " + this.f3220b.getClass());
            }
            C0494b bVar2 = null;
            int i3 = 0;
            while (true) {
                if (i3 >= this.f3223g.size()) {
                    break;
                }
                C0494b bVar3 = (C0494b) this.f3223g.get(i3);
                if (bVar3.f3256b >= this.f3221c) {
                    if (bVar3.f3256b == this.f3221c) {
                        bVar2 = bVar3;
                    }
                } else {
                    i3++;
                }
            }
            if (bVar2 == null && count > 0) {
                bVar2 = mo2875a(this.f3221c, i3);
            }
            if (bVar2 != null) {
                float f = 0.0f;
                int i4 = i3 - 1;
                C0494b bVar4 = i4 >= 0 ? (C0494b) this.f3223g.get(i4) : null;
                int clientWidth = getClientWidth();
                float paddingLeft = clientWidth <= 0 ? 0.0f : (2.0f - bVar2.f3258d) + (((float) getPaddingLeft()) / ((float) clientWidth));
                for (int i5 = this.f3221c - 1; i5 >= 0; i5--) {
                    if (f < paddingLeft || i5 >= max) {
                        if (bVar4 == null || i5 != bVar4.f3256b) {
                            f += mo2875a(i5, i4 + 1).f3258d;
                            i3++;
                            bVar4 = i4 >= 0 ? (C0494b) this.f3223g.get(i4) : null;
                        } else {
                            f += bVar4.f3258d;
                            i4--;
                            bVar4 = i4 >= 0 ? (C0494b) this.f3223g.get(i4) : null;
                        }
                    } else if (bVar4 == null) {
                        break;
                    } else {
                        if (i5 == bVar4.f3256b && !bVar4.f3257c) {
                            this.f3223g.remove(i4);
                            this.f3220b.destroyItem((ViewGroup) this, i5, bVar4.f3255a);
                            i4--;
                            i3--;
                            bVar4 = i4 >= 0 ? (C0494b) this.f3223g.get(i4) : null;
                        }
                    }
                }
                float f2 = bVar2.f3258d;
                int i6 = i3 + 1;
                if (f2 < 2.0f) {
                    C0494b bVar5 = i6 < this.f3223g.size() ? (C0494b) this.f3223g.get(i6) : null;
                    float paddingRight = clientWidth <= 0 ? 0.0f : (((float) getPaddingRight()) / ((float) clientWidth)) + 2.0f;
                    for (int i7 = this.f3221c + 1; i7 < count; i7++) {
                        if (f2 < paddingRight || i7 <= min) {
                            if (bVar5 == null || i7 != bVar5.f3256b) {
                                i6++;
                                f2 += mo2875a(i7, i6).f3258d;
                                bVar5 = i6 < this.f3223g.size() ? (C0494b) this.f3223g.get(i6) : null;
                            } else {
                                f2 += bVar5.f3258d;
                                i6++;
                                bVar5 = i6 < this.f3223g.size() ? (C0494b) this.f3223g.get(i6) : null;
                            }
                        } else if (bVar5 == null) {
                            break;
                        } else {
                            if (i7 == bVar5.f3256b && !bVar5.f3257c) {
                                this.f3223g.remove(i6);
                                this.f3220b.destroyItem((ViewGroup) this, i7, bVar5.f3255a);
                                bVar5 = i6 < this.f3223g.size() ? (C0494b) this.f3223g.get(i6) : null;
                            }
                        }
                    }
                }
                m3782a(bVar2, i3, bVar);
            }
            this.f3220b.setPrimaryItem((ViewGroup) this, this.f3221c, bVar2 != null ? bVar2.f3255a : null);
            this.f3220b.finishUpdate((ViewGroup) this);
            int childCount = getChildCount();
            for (int i8 = 0; i8 < childCount; i8++) {
                View childAt = getChildAt(i8);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                layoutParams.f3251f = i8;
                if (!layoutParams.f3246a && layoutParams.f3248c == 0.0f) {
                    C0494b a = mo2876a(childAt);
                    if (a != null) {
                        layoutParams.f3248c = a.f3258d;
                        layoutParams.f3250e = a.f3256b;
                    }
                }
            }
            m3795g();
            if (hasFocus()) {
                View findFocus = findFocus();
                C0494b bVar6 = findFocus != null ? mo2891b(findFocus) : null;
                if (bVar6 == null || bVar6.f3256b != this.f3221c) {
                    int i9 = 0;
                    while (i9 < getChildCount()) {
                        View childAt2 = getChildAt(i9);
                        C0494b a2 = mo2876a(childAt2);
                        if (a2 == null || a2.f3256b != this.f3221c || !childAt2.requestFocus(2)) {
                            i9++;
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2879a(int i, float f, int i2) {
        int measuredWidth;
        if (this.f3208W > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.f3246a) {
                    switch (layoutParams.f3247b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 3:
                            measuredWidth = paddingLeft;
                            paddingLeft += childAt.getWidth();
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            break;
                        default:
                            measuredWidth = paddingLeft;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
            }
        }
        m3786b(i, f, i2);
        if (this.f3213ae != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i4 = 0; i4 < childCount2; i4++) {
                View childAt2 = getChildAt(i4);
                if (!((LayoutParams) childAt2.getLayoutParams()).f3246a) {
                    this.f3213ae.mo2948a(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.f3207V = true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2880a(int i, int i2, int i3) {
        int scrollX;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        if (this.f3229m != null && !this.f3229m.isFinished()) {
            scrollX = this.f3230n ? this.f3229m.getCurrX() : this.f3229m.getStartX();
            this.f3229m.abortAnimation();
            setScrollingCacheEnabled(false);
        } else {
            scrollX = getScrollX();
        }
        int scrollY = getScrollY();
        int i4 = i - scrollX;
        int i5 = i2 - scrollY;
        if (i4 == 0 && i5 == 0) {
            m3784a(false);
            mo2896c();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i6 = clientWidth / 2;
        float a = ((float) i6) + (((float) i6) * mo2874a(Math.min(1.0f, (1.0f * ((float) Math.abs(i4))) / ((float) clientWidth))));
        int abs = Math.abs(i3);
        int min = Math.min(abs > 0 ? Math.round(1000.0f * Math.abs(a / ((float) abs))) * 4 : (int) ((1.0f + (((float) Math.abs(i4)) / (((float) this.f3232p) + (((float) clientWidth) * this.f3220b.getPageWidth(this.f3221c))))) * 100.0f), 600);
        this.f3230n = false;
        this.f3229m.startScroll(scrollX, scrollY, i4, i5, min);
        ViewCompat.m12913d(this);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2881a(int i, boolean z, boolean z2) {
        mo2882a(i, z, z2, 0);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2882a(int i, boolean z, boolean z2, int i2) {
        boolean z3 = true;
        if (this.f3220b == null || this.f3220b.getCount() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z2 || this.f3221c != i || this.f3223g.size() == 0) {
            if (i < 0) {
                i = 0;
            } else if (i >= this.f3220b.getCount()) {
                i = this.f3220b.getCount() - 1;
            }
            int i3 = this.f3186A;
            if (i > this.f3221c + i3 || i < this.f3221c - i3) {
                for (int i4 = 0; i4 < this.f3223g.size(); i4++) {
                    ((C0494b) this.f3223g.get(i4)).f3257c = true;
                }
            }
            if (this.f3221c == i) {
                z3 = false;
            }
            if (this.f3205T) {
                this.f3221c = i;
                if (z3) {
                    m3792e(i);
                }
                requestLayout();
                return;
            }
            mo2878a(i);
            m3781a(i, z, i2, z3);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    /* renamed from: a */
    public void mo2883a(C0496d dVar) {
        if (this.f3212ad == null) {
            this.f3212ad = new ArrayList();
        }
        this.f3212ad.add(dVar);
    }

    /* renamed from: a */
    public void mo2884a(C0497e eVar) {
        if (this.f3209aa == null) {
            this.f3209aa = new ArrayList();
        }
        this.f3209aa.add(eVar);
    }

    /* renamed from: a */
    public boolean mo2885a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return mo2897c(17);
            case 22:
                return mo2897c(66);
            case 61:
                if (VERSION.SDK_INT < 11) {
                    return false;
                }
                if (KeyEventCompat.m12730a(keyEvent)) {
                    return mo2897c(2);
                }
                if (KeyEventCompat.m12731a(keyEvent, 1)) {
                    return mo2897c(1);
                }
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo2886a(View view, boolean z, int i, int i2, int i3) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i2 + scrollX >= childAt.getLeft() && i2 + scrollX < childAt.getRight() && i3 + scrollY >= childAt.getTop() && i3 + scrollY < childAt.getBottom()) {
                    if (mo2886a(childAt, true, i, (i2 + scrollX) - childAt.getLeft(), (i3 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z && ViewCompat.m12901a(view, -i);
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                if (childAt.getVisibility() == 0) {
                    C0494b a = mo2876a(childAt);
                    if (a != null && a.f3256b == this.f3221c) {
                        childAt.addFocusables(arrayList, i, i2);
                    }
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i2 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0) {
                C0494b a = mo2876a(childAt);
                if (a != null && a.f3256b == this.f3221c) {
                    childAt.addTouchables(arrayList);
                }
            }
        }
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutParams) {
        if (!checkLayoutParams(layoutParams)) {
            layoutParams = generateLayoutParams(layoutParams);
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        layoutParams2.f3246a |= m3790c(view);
        if (!this.f3240x) {
            super.addView(view, i, layoutParams);
        } else if (layoutParams2 == null || !layoutParams2.f3246a) {
            layoutParams2.f3249d = true;
            addViewInLayout(view, i, layoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public C0494b mo2890b(int i) {
        for (int i2 = 0; i2 < this.f3223g.size(); i2++) {
            C0494b bVar = (C0494b) this.f3223g.get(i2);
            if (bVar.f3256b == i) {
                return bVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public C0494b mo2891b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return mo2876a(view);
            }
            if (parent != null && (parent instanceof View)) {
                view = (View) parent;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public void mo2892b() {
        int count = this.f3220b.getCount();
        this.f3222d = count;
        boolean z = this.f3223g.size() < (this.f3186A * 2) + 1 && this.f3223g.size() < count;
        int i = this.f3221c;
        boolean z2 = false;
        int i2 = 0;
        while (i2 < this.f3223g.size()) {
            C0494b bVar = (C0494b) this.f3223g.get(i2);
            int itemPosition = this.f3220b.getItemPosition(bVar.f3255a);
            if (itemPosition != -1) {
                if (itemPosition == -2) {
                    this.f3223g.remove(i2);
                    i2--;
                    if (!z2) {
                        this.f3220b.startUpdate((ViewGroup) this);
                        z2 = true;
                    }
                    this.f3220b.destroyItem((ViewGroup) this, bVar.f3256b, bVar.f3255a);
                    z = true;
                    if (this.f3221c == bVar.f3256b) {
                        i = Math.max(0, Math.min(this.f3221c, count - 1));
                        z = true;
                    }
                } else if (bVar.f3256b != itemPosition) {
                    if (bVar.f3256b == this.f3221c) {
                        i = itemPosition;
                    }
                    bVar.f3256b = itemPosition;
                    z = true;
                }
            }
            i2++;
        }
        if (z2) {
            this.f3220b.finishUpdate((ViewGroup) this);
        }
        Collections.sort(this.f3223g, f3184e);
        if (z) {
            int childCount = getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i3).getLayoutParams();
                if (!layoutParams.f3246a) {
                    layoutParams.f3248c = 0.0f;
                }
            }
            mo2881a(i, false, true);
            requestLayout();
        }
    }

    /* renamed from: b */
    public void mo2893b(C0496d dVar) {
        if (this.f3212ad != null) {
            this.f3212ad.remove(dVar);
        }
    }

    /* renamed from: b */
    public void mo2894b(C0497e eVar) {
        if (this.f3209aa != null) {
            this.f3209aa.remove(eVar);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public C0497e mo2895c(C0497e eVar) {
        C0497e eVar2 = this.f3211ac;
        this.f3211ac = eVar;
        return eVar2;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: c */
    public void mo2896c() {
        mo2878a(this.f3221c);
    }

    /* renamed from: c */
    public boolean mo2897c(int i) {
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        } else if (findFocus != null) {
            boolean z = false;
            ViewParent parent = findFocus.getParent();
            while (true) {
                if (!(parent instanceof ViewGroup)) {
                    break;
                } else if (parent == this) {
                    z = true;
                    break;
                } else {
                    parent = parent.getParent();
                }
            }
            if (!z) {
                StringBuilder sb = new StringBuilder();
                sb.append(findFocus.getClass().getSimpleName());
                for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                    sb.append(" => ").append(parent2.getClass().getSimpleName());
                }
                Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                findFocus = null;
            }
        }
        boolean z2 = false;
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i);
        if (findNextFocus == null || findNextFocus == findFocus) {
            if (i == 17 || i == 1) {
                z2 = mo2901d();
            } else if (i == 66 || i == 2) {
                z2 = mo2906e();
            }
        } else if (i == 17) {
            z2 = (findFocus == null || m3779a(this.f3225i, findNextFocus).left < m3779a(this.f3225i, findFocus).left) ? findNextFocus.requestFocus() : mo2901d();
        } else if (i == 66) {
            z2 = (findFocus == null || m3779a(this.f3225i, findNextFocus).left > m3779a(this.f3225i, findFocus).left) ? findNextFocus.requestFocus() : mo2906e();
        }
        if (z2) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i));
        }
        return z2;
    }

    public boolean canScrollHorizontally(int i) {
        boolean z = true;
        if (this.f3220b == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.f3236t))) {
                z = false;
            }
            return z;
        } else if (i <= 0) {
            return false;
        } else {
            if (scrollX >= ((int) (((float) clientWidth) * this.f3237u))) {
                z = false;
            }
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        this.f3230n = true;
        if (this.f3229m.isFinished() || !this.f3229m.computeScrollOffset()) {
            m3784a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.f3229m.getCurrX();
        int currY = this.f3229m.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!m3791d(currX)) {
                this.f3229m.abortAnimation();
                scrollTo(0, currY);
            }
        }
        ViewCompat.m12913d(this);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public boolean mo2901d() {
        if (this.f3221c <= 0) {
            return false;
        }
        setCurrentItem(this.f3221c - 1, true);
        return true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || mo2885a(keyEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0) {
                C0494b a = mo2876a(childAt);
                if (a != null && a.f3256b == this.f3221c && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z = false;
        int overScrollMode = getOverScrollMode();
        if (overScrollMode == 0 || (overScrollMode == 1 && this.f3220b != null && this.f3220b.getCount() > 1)) {
            if (!this.f3203R.isFinished()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.f3236t * ((float) width));
                this.f3203R.setSize(height, width);
                z = false | this.f3203R.draw(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.f3204S.isFinished()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.f3237u + 1.0f)) * ((float) width2));
                this.f3204S.setSize(height2, width2);
                z |= this.f3204S.draw(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.f3203R.finish();
            this.f3204S.finish();
        }
        if (z) {
            ViewCompat.m12913d(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f3233q;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public boolean mo2906e() {
        if (this.f3220b == null || this.f3221c >= this.f3220b.getCount() - 1) {
            return false;
        }
        setCurrentItem(this.f3221c + 1, true);
        return true;
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    public android.view.ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public PagerAdapter getAdapter() {
        return this.f3220b;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        return ((LayoutParams) ((View) this.f3217ai.get(this.f3216ah == 2 ? (i - 1) - i2 : i2)).getLayoutParams()).f3251f;
    }

    public int getCurrentItem() {
        return this.f3221c;
    }

    public int getOffscreenPageLimit() {
        return this.f3186A;
    }

    public int getPageMargin() {
        return this.f3232p;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f3205T = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.f3218ak);
        if (this.f3229m != null && !this.f3229m.isFinished()) {
            this.f3229m.abortAnimation();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f;
        super.onDraw(canvas);
        if (this.f3232p > 0 && this.f3233q != null && this.f3223g.size() > 0 && this.f3220b != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f2 = ((float) this.f3232p) / ((float) width);
            int i = 0;
            C0494b bVar = (C0494b) this.f3223g.get(0);
            float f3 = bVar.f3259e;
            int size = this.f3223g.size();
            int i2 = bVar.f3256b;
            int i3 = ((C0494b) this.f3223g.get(size - 1)).f3256b;
            int i4 = i2;
            while (i4 < i3) {
                while (i4 > bVar.f3256b && i < size) {
                    i++;
                    bVar = (C0494b) this.f3223g.get(i);
                }
                if (i4 == bVar.f3256b) {
                    f = (bVar.f3259e + bVar.f3258d) * ((float) width);
                    f3 = bVar.f3259e + bVar.f3258d + f2;
                } else {
                    float pageWidth = this.f3220b.getPageWidth(i4);
                    f = (f3 + pageWidth) * ((float) width);
                    f3 += pageWidth + f2;
                }
                if (((float) this.f3232p) + f > ((float) scrollX)) {
                    this.f3233q.setBounds(Math.round(f), this.f3234r, Math.round(((float) this.f3232p) + f), this.f3235s);
                    this.f3233q.draw(canvas);
                }
                if (f <= ((float) (scrollX + width))) {
                    i4++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            m3796h();
            return false;
        }
        if (action != 0) {
            if (this.f3187B) {
                return true;
            }
            if (this.f3188C) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.f3194I = x;
                this.f3192G = x;
                float y = motionEvent.getY();
                this.f3195J = y;
                this.f3193H = y;
                this.f3196K = motionEvent.getPointerId(0);
                this.f3188C = false;
                this.f3230n = true;
                this.f3229m.computeScrollOffset();
                if (this.f3219al == 2 && Math.abs(this.f3229m.getFinalX() - this.f3229m.getCurrX()) > this.f3201P) {
                    this.f3229m.abortAnimation();
                    this.f3242z = false;
                    mo2896c();
                    this.f3187B = true;
                    m3789c(true);
                    setScrollState(1);
                    break;
                } else {
                    m3784a(false);
                    this.f3187B = false;
                    break;
                }
            case 2:
                int i = this.f3196K;
                if (i != -1) {
                    int findPointerIndex = motionEvent.findPointerIndex(i);
                    float x2 = motionEvent.getX(findPointerIndex);
                    float f = x2 - this.f3192G;
                    float abs = Math.abs(f);
                    float y2 = motionEvent.getY(findPointerIndex);
                    float abs2 = Math.abs(y2 - this.f3195J);
                    if (f != 0.0f && !m3785a(this.f3192G, f)) {
                        if (mo2886a(this, false, (int) f, (int) x2, (int) y2)) {
                            this.f3192G = x2;
                            this.f3193H = y2;
                            this.f3188C = true;
                            return false;
                        }
                    }
                    if (abs > ((float) this.f3191F) && 0.5f * abs > abs2) {
                        this.f3187B = true;
                        m3789c(true);
                        setScrollState(1);
                        this.f3192G = f > 0.0f ? this.f3194I + ((float) this.f3191F) : this.f3194I - ((float) this.f3191F);
                        this.f3193H = y2;
                        setScrollingCacheEnabled(true);
                    } else if (abs2 > ((float) this.f3191F)) {
                        this.f3188C = true;
                    }
                    if (this.f3187B && m3788b(x2)) {
                        ViewCompat.m12913d(this);
                        break;
                    }
                }
                break;
            case 6:
                m3783a(motionEvent);
                break;
        }
        if (this.f3197L == null) {
            this.f3197L = VelocityTracker.obtain();
        }
        this.f3197L.addMovement(motionEvent);
        return this.f3187B;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth;
        int measuredHeight;
        int childCount = getChildCount();
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i7 = 0;
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.f3246a) {
                    int i9 = layoutParams.f3247b & 112;
                    switch (layoutParams.f3247b & 7) {
                        case 1:
                            measuredWidth = Math.max((i5 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 3:
                            measuredWidth = paddingLeft;
                            paddingLeft += childAt.getMeasuredWidth();
                            break;
                        case 5:
                            measuredWidth = (i5 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            break;
                        default:
                            measuredWidth = paddingLeft;
                            break;
                    }
                    switch (i9) {
                        case 16:
                            measuredHeight = Math.max((i6 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            break;
                        case 48:
                            measuredHeight = paddingTop;
                            paddingTop += childAt.getMeasuredHeight();
                            break;
                        case 80:
                            measuredHeight = (i6 - paddingBottom) - childAt.getMeasuredHeight();
                            paddingBottom += childAt.getMeasuredHeight();
                            break;
                        default:
                            measuredHeight = paddingTop;
                            break;
                    }
                    int i10 = measuredWidth + scrollX;
                    childAt.layout(i10, measuredHeight, childAt.getMeasuredWidth() + i10, childAt.getMeasuredHeight() + measuredHeight);
                    i7++;
                }
            }
        }
        int i11 = (i5 - paddingLeft) - paddingRight;
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt2 = getChildAt(i12);
            if (childAt2.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (!layoutParams2.f3246a) {
                    C0494b a = mo2876a(childAt2);
                    if (a != null) {
                        int i13 = paddingLeft + ((int) (((float) i11) * a.f3259e));
                        int i14 = paddingTop;
                        if (layoutParams2.f3249d) {
                            layoutParams2.f3249d = false;
                            childAt2.measure(MeasureSpec.makeMeasureSpec((int) (((float) i11) * layoutParams2.f3248c), 1073741824), MeasureSpec.makeMeasureSpec((i6 - paddingTop) - paddingBottom, 1073741824));
                        }
                        childAt2.layout(i13, i14, childAt2.getMeasuredWidth() + i13, childAt2.getMeasuredHeight() + i14);
                    }
                }
            }
        }
        this.f3234r = paddingTop;
        this.f3235s = i6 - paddingBottom;
        this.f3208W = i7;
        if (this.f3205T) {
            m3781a(this.f3221c, false, 0, false);
        }
        this.f3205T = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(getDefaultSize(0, i), getDefaultSize(0, i2));
        int measuredWidth = getMeasuredWidth();
        this.f3190E = Math.min(measuredWidth / 10, this.f3189D);
        int paddingLeft = (measuredWidth - getPaddingLeft()) - getPaddingRight();
        int measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams != null && layoutParams.f3246a) {
                    int i4 = layoutParams.f3247b & 7;
                    int i5 = layoutParams.f3247b & 112;
                    int i6 = Integer.MIN_VALUE;
                    int i7 = Integer.MIN_VALUE;
                    boolean z = i5 == 48 || i5 == 80;
                    boolean z2 = i4 == 3 || i4 == 5;
                    if (z) {
                        i6 = 1073741824;
                    } else if (z2) {
                        i7 = 1073741824;
                    }
                    int i8 = paddingLeft;
                    int i9 = measuredHeight;
                    if (layoutParams.width != -2) {
                        i6 = 1073741824;
                        if (layoutParams.width != -1) {
                            i8 = layoutParams.width;
                        }
                    }
                    if (layoutParams.height != -2) {
                        i7 = 1073741824;
                        if (layoutParams.height != -1) {
                            i9 = layoutParams.height;
                        }
                    }
                    childAt.measure(MeasureSpec.makeMeasureSpec(i8, i6), MeasureSpec.makeMeasureSpec(i9, i7));
                    if (z) {
                        measuredHeight -= childAt.getMeasuredHeight();
                    } else if (z2) {
                        paddingLeft -= childAt.getMeasuredWidth();
                    }
                }
            }
        }
        this.f3238v = MeasureSpec.makeMeasureSpec(paddingLeft, 1073741824);
        this.f3239w = MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824);
        this.f3240x = true;
        mo2896c();
        this.f3240x = false;
        int childCount2 = getChildCount();
        for (int i10 = 0; i10 < childCount2; i10++) {
            View childAt2 = getChildAt(i10);
            if (childAt2.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (layoutParams2 == null || !layoutParams2.f3246a) {
                    childAt2.measure(MeasureSpec.makeMeasureSpec((int) (((float) paddingLeft) * layoutParams2.f3248c), 1073741824), this.f3239w);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2;
        int i3;
        int i4;
        int childCount = getChildCount();
        if ((i & 2) != 0) {
            i2 = 0;
            i3 = 1;
            i4 = childCount;
        } else {
            i2 = childCount - 1;
            i3 = -1;
            i4 = -1;
        }
        for (int i5 = i2; i5 != i4; i5 += i3) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() == 0) {
                C0494b a = mo2876a(childAt);
                if (a != null && a.f3256b == this.f3221c && childAt.requestFocus(i, rect)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.f3220b != null) {
            this.f3220b.restoreState(savedState.f3253b, savedState.f3254c);
            mo2881a(savedState.f3252a, false, true);
            return;
        }
        this.f3226j = savedState.f3252a;
        this.f3227k = savedState.f3253b;
        this.f3228l = savedState.f3254c;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f3252a = this.f3221c;
        if (this.f3220b != null) {
            savedState.f3253b = this.f3220b.saveState();
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            m3780a(i, i3, this.f3232p, this.f3232p);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f3202Q) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.f3220b == null || this.f3220b.getCount() == 0) {
            return false;
        }
        if (this.f3197L == null) {
            this.f3197L = VelocityTracker.obtain();
        }
        this.f3197L.addMovement(motionEvent);
        boolean z = false;
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.f3229m.abortAnimation();
                this.f3242z = false;
                mo2896c();
                float x = motionEvent.getX();
                this.f3194I = x;
                this.f3192G = x;
                float y = motionEvent.getY();
                this.f3195J = y;
                this.f3193H = y;
                this.f3196K = motionEvent.getPointerId(0);
                break;
            case 1:
                if (this.f3187B) {
                    VelocityTracker velocityTracker = this.f3197L;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.f3199N);
                    int a = (int) VelocityTrackerCompat.m12857a(velocityTracker, this.f3196K);
                    this.f3242z = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    C0494b i = m3797i();
                    mo2882a(m3778a(i.f3256b, ((((float) scrollX) / ((float) clientWidth)) - i.f3259e) / (i.f3258d + (((float) this.f3232p) / ((float) clientWidth))), a, (int) (motionEvent.getX(motionEvent.findPointerIndex(this.f3196K)) - this.f3194I)), true, true, a);
                    z = m3796h();
                    break;
                }
                break;
            case 2:
                if (!this.f3187B) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.f3196K);
                    if (findPointerIndex == -1) {
                        z = m3796h();
                        break;
                    } else {
                        float x2 = motionEvent.getX(findPointerIndex);
                        float abs = Math.abs(x2 - this.f3192G);
                        float y2 = motionEvent.getY(findPointerIndex);
                        float abs2 = Math.abs(y2 - this.f3193H);
                        if (abs > ((float) this.f3191F) && abs > abs2) {
                            this.f3187B = true;
                            m3789c(true);
                            this.f3192G = x2 - this.f3194I > 0.0f ? this.f3194I + ((float) this.f3191F) : this.f3194I - ((float) this.f3191F);
                            this.f3193H = y2;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                }
                if (this.f3187B) {
                    z = false | m3788b(motionEvent.getX(motionEvent.findPointerIndex(this.f3196K)));
                    break;
                }
                break;
            case 3:
                if (this.f3187B) {
                    m3781a(this.f3221c, true, 0, false);
                    z = m3796h();
                    break;
                }
                break;
            case 5:
                int b = MotionEventCompat.m12832b(motionEvent);
                this.f3192G = motionEvent.getX(b);
                this.f3196K = motionEvent.getPointerId(b);
                break;
            case 6:
                m3783a(motionEvent);
                this.f3192G = motionEvent.getX(motionEvent.findPointerIndex(this.f3196K));
                break;
        }
        if (z) {
            ViewCompat.m12913d(this);
        }
        return true;
    }

    public void removeView(View view) {
        if (this.f3240x) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    public void setAdapter(PagerAdapter evVar) {
        if (this.f3220b != null) {
            this.f3220b.mo13433a(null);
            this.f3220b.startUpdate((ViewGroup) this);
            for (int i = 0; i < this.f3223g.size(); i++) {
                C0494b bVar = (C0494b) this.f3223g.get(i);
                this.f3220b.destroyItem((ViewGroup) this, bVar.f3256b, bVar.f3255a);
            }
            this.f3220b.finishUpdate((ViewGroup) this);
            this.f3223g.clear();
            m3793f();
            this.f3221c = 0;
            scrollTo(0, 0);
        }
        PagerAdapter evVar2 = this.f3220b;
        this.f3220b = evVar;
        this.f3222d = 0;
        if (this.f3220b != null) {
            if (this.f3231o == null) {
                this.f3231o = new C0499g();
            }
            this.f3220b.mo13433a(this.f3231o);
            this.f3242z = false;
            boolean z = this.f3205T;
            this.f3205T = true;
            this.f3222d = this.f3220b.getCount();
            if (this.f3226j >= 0) {
                this.f3220b.restoreState(this.f3227k, this.f3228l);
                mo2881a(this.f3226j, false, true);
                this.f3226j = -1;
                this.f3227k = null;
                this.f3228l = null;
            } else if (!z) {
                mo2896c();
            } else {
                requestLayout();
            }
        }
        if (this.f3212ad != null && !this.f3212ad.isEmpty()) {
            int size = this.f3212ad.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((C0496d) this.f3212ad.get(i2)).mo2871a(this, evVar2, evVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void setChildrenDrawingOrderEnabledCompat(boolean z) {
        if (VERSION.SDK_INT >= 7) {
            if (this.f3215ag == null) {
                try {
                    this.f3215ag = ViewGroup.class.getDeclaredMethod("setChildrenDrawingOrderEnabled", new Class[]{Boolean.TYPE});
                } catch (NoSuchMethodException e) {
                    Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e);
                }
            }
            try {
                this.f3215ag.invoke(this, new Object[]{Boolean.valueOf(z)});
            } catch (Exception e2) {
                Log.e("ViewPager", "Error changing children drawing order", e2);
            }
        }
    }

    public void setCurrentItem(int i) {
        this.f3242z = false;
        mo2881a(i, !this.f3205T, false);
    }

    public void setCurrentItem(int i, boolean z) {
        this.f3242z = false;
        mo2881a(i, z, false);
    }

    public void setOffscreenPageLimit(int i) {
        if (i < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i + " too small; defaulting to " + 1);
            i = 1;
        }
        if (i != this.f3186A) {
            this.f3186A = i;
            mo2896c();
        }
    }

    @Deprecated
    public void setOnPageChangeListener(C0497e eVar) {
        this.f3210ab = eVar;
    }

    public void setPageMargin(int i) {
        int i2 = this.f3232p;
        this.f3232p = i;
        int width = getWidth();
        m3780a(width, width, i, i2);
        requestLayout();
    }

    public void setPageMarginDrawable(int i) {
        setPageMarginDrawable(ContextCompat.getDrawable(getContext(), i));
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.f3233q = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    public void setPageTransformer(boolean z, C0498f fVar) {
        setPageTransformer(z, fVar, 2);
    }

    public void setPageTransformer(boolean z, C0498f fVar, int i) {
        int i2 = 1;
        if (VERSION.SDK_INT >= 11) {
            boolean z2 = fVar != null;
            boolean z3 = z2 != (this.f3213ae != null);
            this.f3213ae = fVar;
            setChildrenDrawingOrderEnabledCompat(z2);
            if (z2) {
                if (z) {
                    i2 = 2;
                }
                this.f3216ah = i2;
                this.f3214af = i;
            } else {
                this.f3216ah = 0;
            }
            if (z3) {
                mo2896c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void setScrollState(int i) {
        if (this.f3219al != i) {
            this.f3219al = i;
            if (this.f3213ae != null) {
                m3787b(i != 0);
            }
            m3794f(i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f3233q;
    }
}
