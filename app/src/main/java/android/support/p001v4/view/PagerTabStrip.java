package android.support.p001v4.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;

/* renamed from: android.support.v4.view.PagerTabStrip */
public class PagerTabStrip extends PagerTitleStrip {

    /* renamed from: g */
    private int f3145g;

    /* renamed from: h */
    private int f3146h;

    /* renamed from: i */
    private int f3147i;

    /* renamed from: j */
    private int f3148j;

    /* renamed from: k */
    private int f3149k;

    /* renamed from: l */
    private int f3150l;

    /* renamed from: m */
    private final Paint f3151m;

    /* renamed from: n */
    private final Rect f3152n;

    /* renamed from: o */
    private int f3153o;

    /* renamed from: p */
    private boolean f3154p;

    /* renamed from: q */
    private boolean f3155q;

    /* renamed from: r */
    private int f3156r;

    /* renamed from: s */
    private boolean f3157s;

    /* renamed from: t */
    private float f3158t;

    /* renamed from: u */
    private float f3159u;

    /* renamed from: v */
    private int f3160v;

    public PagerTabStrip(Context context) {
        this(context, null);
    }

    public PagerTabStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3151m = new Paint();
        this.f3152n = new Rect();
        this.f3153o = 255;
        this.f3154p = false;
        this.f3155q = false;
        this.f3145g = this.f3171f;
        this.f3151m.setColor(this.f3145g);
        float f = context.getResources().getDisplayMetrics().density;
        this.f3146h = (int) ((3.0f * f) + 0.5f);
        this.f3147i = (int) ((6.0f * f) + 0.5f);
        this.f3148j = (int) (64.0f * f);
        this.f3150l = (int) ((16.0f * f) + 0.5f);
        this.f3156r = (int) ((1.0f * f) + 0.5f);
        this.f3149k = (int) ((32.0f * f) + 0.5f);
        this.f3160v = ViewConfiguration.get(context).getScaledTouchSlop();
        setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        setTextSpacing(getTextSpacing());
        setWillNotDraw(false);
        this.f3167b.setFocusable(true);
        this.f3167b.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PagerTabStrip.this.f3166a.setCurrentItem(PagerTabStrip.this.f3166a.getCurrentItem() - 1);
            }
        });
        this.f3169d.setFocusable(true);
        this.f3169d.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PagerTabStrip.this.f3166a.setCurrentItem(PagerTabStrip.this.f3166a.getCurrentItem() + 1);
            }
        });
        if (getBackground() == null) {
            this.f3154p = true;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2843a(int i, float f, boolean z) {
        Rect rect = this.f3152n;
        int height = getHeight();
        int i2 = height - this.f3146h;
        rect.set(this.f3168c.getLeft() - this.f3150l, i2, this.f3168c.getRight() + this.f3150l, height);
        super.mo2843a(i, f, z);
        this.f3153o = (int) (Math.abs(f - 0.5f) * 2.0f * 255.0f);
        rect.union(this.f3168c.getLeft() - this.f3150l, i2, this.f3168c.getRight() + this.f3150l, height);
        invalidate(rect);
    }

    public boolean getDrawFullUnderline() {
        return this.f3154p;
    }

    /* access modifiers changed from: 0000 */
    public int getMinHeight() {
        return Math.max(super.getMinHeight(), this.f3149k);
    }

    public int getTabIndicatorColor() {
        return this.f3145g;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = getHeight();
        int i = height;
        int left = this.f3168c.getLeft() - this.f3150l;
        int right = this.f3168c.getRight() + this.f3150l;
        int i2 = i - this.f3146h;
        this.f3151m.setColor((this.f3153o << 24) | (this.f3145g & 16777215));
        canvas.drawRect((float) left, (float) i2, (float) right, (float) i, this.f3151m);
        if (this.f3154p) {
            this.f3151m.setColor(-16777216 | (this.f3145g & 16777215));
            canvas.drawRect((float) getPaddingLeft(), (float) (height - this.f3156r), (float) (getWidth() - getPaddingRight()), (float) height, this.f3151m);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0 && this.f3157s) {
            return false;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                this.f3158t = x;
                this.f3159u = y;
                this.f3157s = false;
                break;
            case 1:
                if (x >= ((float) (this.f3168c.getLeft() - this.f3150l))) {
                    if (x > ((float) (this.f3168c.getRight() + this.f3150l))) {
                        this.f3166a.setCurrentItem(this.f3166a.getCurrentItem() + 1);
                        break;
                    }
                } else {
                    this.f3166a.setCurrentItem(this.f3166a.getCurrentItem() - 1);
                    break;
                }
                break;
            case 2:
                if (Math.abs(x - this.f3158t) > ((float) this.f3160v) || Math.abs(y - this.f3159u) > ((float) this.f3160v)) {
                    this.f3157s = true;
                    break;
                }
        }
        return true;
    }

    public void setBackgroundColor(int i) {
        super.setBackgroundColor(i);
        if (!this.f3155q) {
            this.f3154p = (-16777216 & i) == 0;
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (!this.f3155q) {
            this.f3154p = drawable == null;
        }
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (!this.f3155q) {
            this.f3154p = i == 0;
        }
    }

    public void setDrawFullUnderline(boolean z) {
        this.f3154p = z;
        this.f3155q = true;
        invalidate();
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        if (i4 < this.f3147i) {
            i4 = this.f3147i;
        }
        super.setPadding(i, i2, i3, i4);
    }

    public void setTabIndicatorColor(int i) {
        this.f3145g = i;
        this.f3151m.setColor(this.f3145g);
        invalidate();
    }

    public void setTabIndicatorColorResource(int i) {
        setTabIndicatorColor(ContextCompat.getColor(getContext(), i));
    }

    public void setTextSpacing(int i) {
        if (i < this.f3148j) {
            i = this.f3148j;
        }
        super.setTextSpacing(i);
    }
}
