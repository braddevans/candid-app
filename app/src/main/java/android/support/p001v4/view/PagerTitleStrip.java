package android.support.p001v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.p001v4.view.ViewPager.C0493a;
import android.support.p001v4.view.ViewPager.C0496d;
import android.support.p001v4.view.ViewPager.C0497e;
import android.support.p001v4.widget.TextViewCompat;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;
import java.lang.ref.WeakReference;

@C0493a
/* renamed from: android.support.v4.view.PagerTitleStrip */
public class PagerTitleStrip extends ViewGroup {

    /* renamed from: n */
    private static final int[] f3163n = {16842804, 16842901, 16842904, 16842927};

    /* renamed from: o */
    private static final int[] f3164o = {16843660};

    /* renamed from: q */
    private static final C0485b f3165q;

    /* renamed from: a */
    ViewPager f3166a;

    /* renamed from: b */
    TextView f3167b;

    /* renamed from: c */
    TextView f3168c;

    /* renamed from: d */
    TextView f3169d;

    /* renamed from: e */
    float f3170e;

    /* renamed from: f */
    int f3171f;

    /* renamed from: g */
    private int f3172g;

    /* renamed from: h */
    private int f3173h;

    /* renamed from: i */
    private int f3174i;

    /* renamed from: j */
    private boolean f3175j;

    /* renamed from: k */
    private boolean f3176k;

    /* renamed from: l */
    private final C0484a f3177l;

    /* renamed from: m */
    private WeakReference<PagerAdapter> f3178m;

    /* renamed from: p */
    private int f3179p;

    /* renamed from: android.support.v4.view.PagerTitleStrip$a */
    class C0484a extends DataSetObserver implements C0496d, C0497e {

        /* renamed from: b */
        private int f3181b;

        C0484a() {
        }

        /* renamed from: a */
        public void mo2871a(ViewPager viewPager, PagerAdapter evVar, PagerAdapter evVar2) {
            PagerTitleStrip.this.mo2860a(evVar, evVar2);
        }

        public void onChanged() {
            float f = 0.0f;
            PagerTitleStrip.this.mo2859a(PagerTitleStrip.this.f3166a.getCurrentItem(), PagerTitleStrip.this.f3166a.getAdapter());
            if (PagerTitleStrip.this.f3170e >= 0.0f) {
                f = PagerTitleStrip.this.f3170e;
            }
            PagerTitleStrip.this.mo2843a(PagerTitleStrip.this.f3166a.getCurrentItem(), f, true);
        }

        public void onPageScrollStateChanged(int i) {
            this.f3181b = i;
        }

        public void onPageScrolled(int i, float f, int i2) {
            if (f > 0.5f) {
                i++;
            }
            PagerTitleStrip.this.mo2843a(i, f, false);
        }

        public void onPageSelected(int i) {
            float f = 0.0f;
            if (this.f3181b == 0) {
                PagerTitleStrip.this.mo2859a(PagerTitleStrip.this.f3166a.getCurrentItem(), PagerTitleStrip.this.f3166a.getAdapter());
                if (PagerTitleStrip.this.f3170e >= 0.0f) {
                    f = PagerTitleStrip.this.f3170e;
                }
                PagerTitleStrip.this.mo2843a(PagerTitleStrip.this.f3166a.getCurrentItem(), f, true);
            }
        }
    }

    /* renamed from: android.support.v4.view.PagerTitleStrip$b */
    interface C0485b {
        /* renamed from: a */
        void mo2873a(TextView textView);
    }

    /* renamed from: android.support.v4.view.PagerTitleStrip$c */
    static class C0486c implements C0485b {
        C0486c() {
        }

        /* renamed from: a */
        public void mo2873a(TextView textView) {
            textView.setSingleLine();
        }
    }

    /* renamed from: android.support.v4.view.PagerTitleStrip$d */
    static class C0487d implements C0485b {
        C0487d() {
        }

        /* renamed from: a */
        public void mo2873a(TextView textView) {
            PagerTitleStripIcs.m12856a(textView);
        }
    }

    static {
        if (VERSION.SDK_INT >= 14) {
            f3165q = new C0487d();
        } else {
            f3165q = new C0486c();
        }
    }

    public PagerTitleStrip(Context context) {
        this(context, null);
    }

    public PagerTitleStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3172g = -1;
        this.f3170e = -1.0f;
        this.f3177l = new C0484a();
        TextView textView = new TextView(context);
        this.f3167b = textView;
        addView(textView);
        TextView textView2 = new TextView(context);
        this.f3168c = textView2;
        addView(textView2);
        TextView textView3 = new TextView(context);
        this.f3169d = textView3;
        addView(textView3);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f3163n);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        if (resourceId != 0) {
            TextViewCompat.setTextAppearance(this.f3167b, resourceId);
            TextViewCompat.setTextAppearance(this.f3168c, resourceId);
            TextViewCompat.setTextAppearance(this.f3169d, resourceId);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, 0);
        if (dimensionPixelSize != 0) {
            setTextSize(0, (float) dimensionPixelSize);
        }
        if (obtainStyledAttributes.hasValue(2)) {
            int color = obtainStyledAttributes.getColor(2, 0);
            this.f3167b.setTextColor(color);
            this.f3168c.setTextColor(color);
            this.f3169d.setTextColor(color);
        }
        this.f3174i = obtainStyledAttributes.getInteger(3, 80);
        obtainStyledAttributes.recycle();
        this.f3171f = this.f3168c.getTextColors().getDefaultColor();
        setNonPrimaryAlpha(0.6f);
        this.f3167b.setEllipsize(TruncateAt.END);
        this.f3168c.setEllipsize(TruncateAt.END);
        this.f3169d.setEllipsize(TruncateAt.END);
        boolean z = false;
        if (resourceId != 0) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(resourceId, f3164o);
            z = obtainStyledAttributes2.getBoolean(0, false);
            obtainStyledAttributes2.recycle();
        }
        if (z) {
            setSingleLineAllCaps(this.f3167b);
            setSingleLineAllCaps(this.f3168c);
            setSingleLineAllCaps(this.f3169d);
        } else {
            this.f3167b.setSingleLine();
            this.f3168c.setSingleLine();
            this.f3169d.setSingleLine();
        }
        this.f3173h = (int) (16.0f * context.getResources().getDisplayMetrics().density);
    }

    private static void setSingleLineAllCaps(TextView textView) {
        f3165q.mo2873a(textView);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2843a(int i, float f, boolean z) {
        int i2;
        int i3;
        int i4;
        if (i != this.f3172g) {
            mo2859a(i, this.f3166a.getAdapter());
        } else if (!z && f == this.f3170e) {
            return;
        }
        this.f3176k = true;
        int measuredWidth = this.f3167b.getMeasuredWidth();
        int measuredWidth2 = this.f3168c.getMeasuredWidth();
        int measuredWidth3 = this.f3169d.getMeasuredWidth();
        int i5 = measuredWidth2 / 2;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i6 = paddingRight + i5;
        int i7 = (width - (paddingLeft + i5)) - i6;
        float f2 = f + 0.5f;
        if (f2 > 1.0f) {
            f2 -= 1.0f;
        }
        int i8 = ((width - i6) - ((int) (((float) i7) * f2))) - (measuredWidth2 / 2);
        int i9 = i8 + measuredWidth2;
        int baseline = this.f3167b.getBaseline();
        int baseline2 = this.f3168c.getBaseline();
        int baseline3 = this.f3169d.getBaseline();
        int max = Math.max(Math.max(baseline, baseline2), baseline3);
        int i10 = max - baseline;
        int i11 = max - baseline2;
        int i12 = max - baseline3;
        int measuredHeight = i12 + this.f3169d.getMeasuredHeight();
        int max2 = Math.max(Math.max(i10 + this.f3167b.getMeasuredHeight(), i11 + this.f3168c.getMeasuredHeight()), measuredHeight);
        switch (this.f3174i & 112) {
            case 16:
                int i13 = (((height - paddingTop) - paddingBottom) - max2) / 2;
                i2 = i13 + i10;
                i3 = i13 + i11;
                i4 = i13 + i12;
                break;
            case 80:
                int i14 = (height - paddingBottom) - max2;
                i2 = i14 + i10;
                i3 = i14 + i11;
                i4 = i14 + i12;
                break;
            default:
                i2 = paddingTop + i10;
                i3 = paddingTop + i11;
                i4 = paddingTop + i12;
                break;
        }
        this.f3168c.layout(i8, i3, i9, this.f3168c.getMeasuredHeight() + i3);
        int min = Math.min(paddingLeft, (i8 - this.f3173h) - measuredWidth);
        this.f3167b.layout(min, i2, min + measuredWidth, this.f3167b.getMeasuredHeight() + i2);
        int max3 = Math.max((width - paddingRight) - measuredWidth3, this.f3173h + i9);
        this.f3169d.layout(max3, i4, max3 + measuredWidth3, this.f3169d.getMeasuredHeight() + i4);
        this.f3170e = f;
        this.f3176k = false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2859a(int i, PagerAdapter evVar) {
        int i2 = evVar != null ? evVar.getCount() : 0;
        this.f3175j = true;
        CharSequence charSequence = null;
        if (i >= 1 && evVar != null) {
            charSequence = evVar.getPageTitle(i - 1);
        }
        this.f3167b.setText(charSequence);
        this.f3168c.setText((evVar == null || i >= i2) ? null : evVar.getPageTitle(i));
        CharSequence charSequence2 = null;
        if (i + 1 < i2 && evVar != null) {
            charSequence2 = evVar.getPageTitle(i + 1);
        }
        this.f3169d.setText(charSequence2);
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(Math.max(0, (int) (((float) ((getWidth() - getPaddingLeft()) - getPaddingRight())) * 0.8f)), Integer.MIN_VALUE);
        int makeMeasureSpec2 = MeasureSpec.makeMeasureSpec(Math.max(0, (getHeight() - getPaddingTop()) - getPaddingBottom()), Integer.MIN_VALUE);
        this.f3167b.measure(makeMeasureSpec, makeMeasureSpec2);
        this.f3168c.measure(makeMeasureSpec, makeMeasureSpec2);
        this.f3169d.measure(makeMeasureSpec, makeMeasureSpec2);
        this.f3172g = i;
        if (!this.f3176k) {
            mo2843a(i, this.f3170e, false);
        }
        this.f3175j = false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2860a(PagerAdapter evVar, PagerAdapter evVar2) {
        if (evVar != null) {
            evVar.unregisterDataSetObserver(this.f3177l);
            this.f3178m = null;
        }
        if (evVar2 != null) {
            evVar2.registerDataSetObserver(this.f3177l);
            this.f3178m = new WeakReference<>(evVar2);
        }
        if (this.f3166a != null) {
            this.f3172g = -1;
            this.f3170e = -1.0f;
            mo2859a(this.f3166a.getCurrentItem(), evVar2);
            requestLayout();
        }
    }

    /* access modifiers changed from: 0000 */
    public int getMinHeight() {
        Drawable background = getBackground();
        if (background != null) {
            return background.getIntrinsicHeight();
        }
        return 0;
    }

    public int getTextSpacing() {
        return this.f3173h;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (!(parent instanceof ViewPager)) {
            throw new IllegalStateException("PagerTitleStrip must be a direct child of a ViewPager.");
        }
        ViewPager viewPager = (ViewPager) parent;
        PagerAdapter adapter = viewPager.getAdapter();
        viewPager.mo2895c((C0497e) this.f3177l);
        viewPager.mo2883a((C0496d) this.f3177l);
        this.f3166a = viewPager;
        mo2860a(this.f3178m != null ? (PagerAdapter) this.f3178m.get() : null, adapter);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f3166a != null) {
            mo2860a(this.f3166a.getAdapter(), (PagerAdapter) null);
            this.f3166a.mo2895c((C0497e) null);
            this.f3166a.mo2893b((C0496d) this.f3177l);
            this.f3166a = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        float f = 0.0f;
        if (this.f3166a != null) {
            if (this.f3170e >= 0.0f) {
                f = this.f3170e;
            }
            mo2843a(this.f3172g, f, true);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int max;
        if (MeasureSpec.getMode(i) != 1073741824) {
            throw new IllegalStateException("Must measure with an exact width");
        }
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int childMeasureSpec = getChildMeasureSpec(i2, paddingTop, -2);
        int size = MeasureSpec.getSize(i);
        int childMeasureSpec2 = getChildMeasureSpec(i, (int) (((float) size) * 0.2f), -2);
        this.f3167b.measure(childMeasureSpec2, childMeasureSpec);
        this.f3168c.measure(childMeasureSpec2, childMeasureSpec);
        this.f3169d.measure(childMeasureSpec2, childMeasureSpec);
        if (MeasureSpec.getMode(i2) == 1073741824) {
            max = MeasureSpec.getSize(i2);
        } else {
            max = Math.max(getMinHeight(), this.f3168c.getMeasuredHeight() + paddingTop);
        }
        setMeasuredDimension(size, ViewCompat.m12882a(max, i2, ViewCompat.m12927l(this.f3168c) << 16));
    }

    public void requestLayout() {
        if (!this.f3175j) {
            super.requestLayout();
        }
    }

    public void setGravity(int i) {
        this.f3174i = i;
        requestLayout();
    }

    public void setNonPrimaryAlpha(float f) {
        this.f3179p = ((int) (255.0f * f)) & 255;
        int i = (this.f3179p << 24) | (this.f3171f & 16777215);
        this.f3167b.setTextColor(i);
        this.f3169d.setTextColor(i);
    }

    public void setTextColor(int i) {
        this.f3171f = i;
        this.f3168c.setTextColor(i);
        int i2 = (this.f3179p << 24) | (this.f3171f & 16777215);
        this.f3167b.setTextColor(i2);
        this.f3169d.setTextColor(i2);
    }

    public void setTextSize(int i, float f) {
        this.f3167b.setTextSize(i, f);
        this.f3168c.setTextSize(i, f);
        this.f3169d.setTextSize(i, f);
    }

    public void setTextSpacing(int i) {
        this.f3173h = i;
        requestLayout();
    }
}
