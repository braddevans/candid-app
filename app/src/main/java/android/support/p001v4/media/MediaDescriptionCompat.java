package android.support.p001v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

/* renamed from: android.support.v4.media.MediaDescriptionCompat */
public final class MediaDescriptionCompat implements Parcelable {
    public static final Creator<MediaDescriptionCompat> CREATOR = new Creator<MediaDescriptionCompat>() {
        /* renamed from: a */
        public MediaDescriptionCompat createFromParcel(Parcel parcel) {
            return VERSION.SDK_INT < 21 ? new MediaDescriptionCompat(parcel) : MediaDescriptionCompat.m3746a(MediaDescriptionCompatApi21.m8994a(parcel));
        }

        /* renamed from: a */
        public MediaDescriptionCompat[] newArray(int i) {
            return new MediaDescriptionCompat[i];
        }
    };

    /* renamed from: a */
    private final String f3120a;

    /* renamed from: b */
    private final CharSequence f3121b;

    /* renamed from: c */
    private final CharSequence f3122c;

    /* renamed from: d */
    private final CharSequence f3123d;

    /* renamed from: e */
    private final Bitmap f3124e;

    /* renamed from: f */
    private final Uri f3125f;

    /* renamed from: g */
    private final Bundle f3126g;

    /* renamed from: h */
    private final Uri f3127h;

    /* renamed from: i */
    private Object f3128i;

    /* renamed from: android.support.v4.media.MediaDescriptionCompat$a */
    public static final class C0475a {

        /* renamed from: a */
        private String f3129a;

        /* renamed from: b */
        private CharSequence f3130b;

        /* renamed from: c */
        private CharSequence f3131c;

        /* renamed from: d */
        private CharSequence f3132d;

        /* renamed from: e */
        private Bitmap f3133e;

        /* renamed from: f */
        private Uri f3134f;

        /* renamed from: g */
        private Bundle f3135g;

        /* renamed from: h */
        private Uri f3136h;

        /* renamed from: a */
        public C0475a mo2809a(Bitmap bitmap) {
            this.f3133e = bitmap;
            return this;
        }

        /* renamed from: a */
        public C0475a mo2810a(Uri uri) {
            this.f3134f = uri;
            return this;
        }

        /* renamed from: a */
        public C0475a mo2811a(Bundle bundle) {
            this.f3135g = bundle;
            return this;
        }

        /* renamed from: a */
        public C0475a mo2812a(CharSequence charSequence) {
            this.f3130b = charSequence;
            return this;
        }

        /* renamed from: a */
        public C0475a mo2813a(String str) {
            this.f3129a = str;
            return this;
        }

        /* renamed from: a */
        public MediaDescriptionCompat mo2814a() {
            return new MediaDescriptionCompat(this.f3129a, this.f3130b, this.f3131c, this.f3132d, this.f3133e, this.f3134f, this.f3135g, this.f3136h);
        }

        /* renamed from: b */
        public C0475a mo2815b(Uri uri) {
            this.f3136h = uri;
            return this;
        }

        /* renamed from: b */
        public C0475a mo2816b(CharSequence charSequence) {
            this.f3131c = charSequence;
            return this;
        }

        /* renamed from: c */
        public C0475a mo2817c(CharSequence charSequence) {
            this.f3132d = charSequence;
            return this;
        }
    }

    MediaDescriptionCompat(Parcel parcel) {
        this.f3120a = parcel.readString();
        this.f3121b = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f3122c = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f3123d = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f3124e = (Bitmap) parcel.readParcelable(null);
        this.f3125f = (Uri) parcel.readParcelable(null);
        this.f3126g = parcel.readBundle();
        this.f3127h = (Uri) parcel.readParcelable(null);
    }

    MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, Uri uri2) {
        this.f3120a = str;
        this.f3121b = charSequence;
        this.f3122c = charSequence2;
        this.f3123d = charSequence3;
        this.f3124e = bitmap;
        this.f3125f = uri;
        this.f3126g = bundle;
        this.f3127h = uri2;
    }

    /* renamed from: a */
    public static MediaDescriptionCompat m3746a(Object obj) {
        if (obj == null || VERSION.SDK_INT < 21) {
            return null;
        }
        C0475a aVar = new C0475a();
        aVar.mo2813a(MediaDescriptionCompatApi21.m8995a(obj));
        aVar.mo2812a(MediaDescriptionCompatApi21.m8997b(obj));
        aVar.mo2816b(MediaDescriptionCompatApi21.m8998c(obj));
        aVar.mo2817c(MediaDescriptionCompatApi21.m8999d(obj));
        aVar.mo2809a(MediaDescriptionCompatApi21.m9000e(obj));
        aVar.mo2810a(MediaDescriptionCompatApi21.m9001f(obj));
        Bundle g = MediaDescriptionCompatApi21.m9002g(obj);
        Uri uri = g == null ? null : (Uri) g.getParcelable("android.support.v4.media.description.MEDIA_URI");
        if (uri != null) {
            if (!g.containsKey("android.support.v4.media.description.NULL_BUNDLE_FLAG") || g.size() != 2) {
                g.remove("android.support.v4.media.description.MEDIA_URI");
                g.remove("android.support.v4.media.description.NULL_BUNDLE_FLAG");
            } else {
                g = null;
            }
        }
        aVar.mo2811a(g);
        if (uri != null) {
            aVar.mo2815b(uri);
        } else if (VERSION.SDK_INT >= 23) {
            aVar.mo2815b(MediaDescriptionCompatApi23.m12475h(obj));
        }
        MediaDescriptionCompat a = aVar.mo2814a();
        a.f3128i = obj;
        return a;
    }

    /* renamed from: a */
    public Object mo2801a() {
        if (this.f3128i != null || VERSION.SDK_INT < 21) {
            return this.f3128i;
        }
        Object a = C1267a.m9003a();
        C1267a.m9009a(a, this.f3120a);
        C1267a.m9008a(a, this.f3121b);
        C1267a.m9010b(a, this.f3122c);
        C1267a.m9011c(a, this.f3123d);
        C1267a.m9005a(a, this.f3124e);
        C1267a.m9006a(a, this.f3125f);
        Bundle bundle = this.f3126g;
        if (VERSION.SDK_INT < 23 && this.f3127h != null) {
            if (bundle == null) {
                bundle = new Bundle();
                bundle.putBoolean("android.support.v4.media.description.NULL_BUNDLE_FLAG", true);
            }
            bundle.putParcelable("android.support.v4.media.description.MEDIA_URI", this.f3127h);
        }
        C1267a.m9007a(a, bundle);
        if (VERSION.SDK_INT >= 23) {
            C2258a.m12476b(a, this.f3127h);
        }
        this.f3128i = C1267a.m9004a(a);
        return this.f3128i;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return this.f3121b + ", " + this.f3122c + ", " + this.f3123d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (VERSION.SDK_INT < 21) {
            parcel.writeString(this.f3120a);
            TextUtils.writeToParcel(this.f3121b, parcel, i);
            TextUtils.writeToParcel(this.f3122c, parcel, i);
            TextUtils.writeToParcel(this.f3123d, parcel, i);
            parcel.writeParcelable(this.f3124e, i);
            parcel.writeParcelable(this.f3125f, i);
            parcel.writeBundle(this.f3126g);
            parcel.writeParcelable(this.f3127h, i);
            return;
        }
        MediaDescriptionCompatApi21.m8996a(mo2801a(), parcel, i);
    }
}
