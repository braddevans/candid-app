package android.support.p001v4.media.session;

import android.os.Build.VERSION;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* renamed from: android.support.v4.media.session.MediaSessionCompat */
public class MediaSessionCompat {

    /* renamed from: android.support.v4.media.session.MediaSessionCompat$Token */
    public static final class Token implements Parcelable {
        public static final Creator<Token> CREATOR = new Creator<Token>() {
            /* renamed from: a */
            public Token createFromParcel(Parcel parcel) {
                return new Token(VERSION.SDK_INT >= 21 ? parcel.readParcelable(null) : parcel.readStrongBinder());
            }

            /* renamed from: a */
            public Token[] newArray(int i) {
                return new Token[i];
            }
        };

        /* renamed from: a */
        private final Object f3137a;

        Token(Object obj) {
            this.f3137a = obj;
        }

        /* renamed from: a */
        public static Token m3759a(Object obj) {
            if (obj == null || VERSION.SDK_INT < 21) {
                return null;
            }
            return new Token(MediaSessionCompatApi21.m12479a(obj));
        }

        /* renamed from: a */
        public Object mo2819a() {
            return this.f3137a;
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Token)) {
                return false;
            }
            Token token = (Token) obj;
            if (this.f3137a == null) {
                return token.f3137a == null;
            }
            if (token.f3137a == null) {
                return false;
            }
            return this.f3137a.equals(token.f3137a);
        }

        public int hashCode() {
            if (this.f3137a == null) {
                return 0;
            }
            return this.f3137a.hashCode();
        }

        public void writeToParcel(Parcel parcel, int i) {
            if (VERSION.SDK_INT >= 21) {
                parcel.writeParcelable((Parcelable) this.f3137a, i);
            } else {
                parcel.writeStrongBinder((IBinder) this.f3137a);
            }
        }
    }
}
