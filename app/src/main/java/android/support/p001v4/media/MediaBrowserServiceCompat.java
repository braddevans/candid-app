package android.support.p001v4.media;

import android.app.Service;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.p001v4.app.BundleCompat;
import android.support.p001v4.media.MediaBrowserCompat.MediaItem;
import android.support.p001v4.media.session.MediaSessionCompat.Token;
import android.support.p001v4.p002os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: android.support.v4.media.MediaBrowserServiceCompat */
public abstract class MediaBrowserServiceCompat extends Service {

    /* renamed from: a */
    static final boolean f3067a = Log.isLoggable("MBServiceCompat", 3);

    /* renamed from: b */
    final ArrayMap<IBinder, C0461b> f3068b = new ArrayMap<>();

    /* renamed from: c */
    C0461b f3069c;

    /* renamed from: d */
    final C0473g f3070d = new C0473g();

    /* renamed from: e */
    Token f3071e;

    /* renamed from: android.support.v4.media.MediaBrowserServiceCompat$a */
    public static final class C0460a {

        /* renamed from: a */
        private final String f3078a;

        /* renamed from: b */
        private final Bundle f3079b;

        /* renamed from: a */
        public String mo2775a() {
            return this.f3078a;
        }

        /* renamed from: b */
        public Bundle mo2776b() {
            return this.f3079b;
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserServiceCompat$b */
    class C0461b {

        /* renamed from: a */
        String f3080a;

        /* renamed from: b */
        Bundle f3081b;

        /* renamed from: c */
        C0471e f3082c;

        /* renamed from: d */
        C0460a f3083d;

        /* renamed from: e */
        HashMap<String, List<Pair<IBinder, Bundle>>> f3084e = new HashMap<>();

        C0461b() {
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserServiceCompat$c */
    public static class C0462c<T> {

        /* renamed from: a */
        private Object f3086a;

        /* renamed from: b */
        private boolean f3087b;

        /* renamed from: c */
        private boolean f3088c;

        /* renamed from: d */
        private int f3089d;

        C0462c(Object obj) {
            this.f3086a = obj;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo2777a(int i) {
            this.f3089d = i;
        }

        /* renamed from: a */
        public void mo2778a(T t) {
            if (this.f3088c) {
                throw new IllegalStateException("sendResult() called twice for: " + this.f3086a);
            }
            this.f3088c = true;
            mo2772a(t, this.f3089d);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo2772a(T t, int i) {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo2779a() {
            return this.f3087b || this.f3088c;
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserServiceCompat$d */
    class C0463d {
        C0463d() {
        }

        /* renamed from: a */
        public void mo2780a(final C0471e eVar) {
            MediaBrowserServiceCompat.this.f3070d.mo2798a(new Runnable() {
                public void run() {
                    if (((C0461b) MediaBrowserServiceCompat.this.f3068b.remove(eVar.mo2794a())) != null) {
                    }
                }
            });
        }

        /* renamed from: a */
        public void mo2781a(final C0471e eVar, final Bundle bundle) {
            MediaBrowserServiceCompat.this.f3070d.mo2798a(new Runnable() {
                public void run() {
                    IBinder a = eVar.mo2794a();
                    MediaBrowserServiceCompat.this.f3068b.remove(a);
                    C0461b bVar = new C0461b();
                    bVar.f3082c = eVar;
                    bVar.f3081b = bundle;
                    MediaBrowserServiceCompat.this.f3068b.put(a, bVar);
                }
            });
        }

        /* renamed from: a */
        public void mo2782a(String str, int i, Bundle bundle, C0471e eVar) {
            if (!MediaBrowserServiceCompat.this.mo2769a(str, i)) {
                throw new IllegalArgumentException("Package/uid mismatch: uid=" + i + " package=" + str);
            }
            final C0471e eVar2 = eVar;
            final String str2 = str;
            final Bundle bundle2 = bundle;
            final int i2 = i;
            MediaBrowserServiceCompat.this.f3070d.mo2798a(new Runnable() {
                public void run() {
                    IBinder a = eVar2.mo2794a();
                    MediaBrowserServiceCompat.this.f3068b.remove(a);
                    C0461b bVar = new C0461b();
                    bVar.f3080a = str2;
                    bVar.f3081b = bundle2;
                    bVar.f3082c = eVar2;
                    bVar.f3083d = MediaBrowserServiceCompat.this.mo2762a(str2, i2, bundle2);
                    if (bVar.f3083d == null) {
                        Log.i("MBServiceCompat", "No root for client " + str2 + " from service " + getClass().getName());
                        try {
                            eVar2.mo2797b();
                        } catch (RemoteException e) {
                            Log.w("MBServiceCompat", "Calling onConnectFailed() failed. Ignoring. pkg=" + str2);
                        }
                    } else {
                        try {
                            MediaBrowserServiceCompat.this.f3068b.put(a, bVar);
                            if (MediaBrowserServiceCompat.this.f3071e != null) {
                                eVar2.mo2795a(bVar.f3083d.mo2775a(), MediaBrowserServiceCompat.this.f3071e, bVar.f3083d.mo2776b());
                            }
                        } catch (RemoteException e2) {
                            Log.w("MBServiceCompat", "Calling onConnect() failed. Dropping client. pkg=" + str2);
                            MediaBrowserServiceCompat.this.f3068b.remove(a);
                        }
                    }
                }
            });
        }

        /* renamed from: a */
        public void mo2783a(String str, IBinder iBinder, Bundle bundle, C0471e eVar) {
            final C0471e eVar2 = eVar;
            final String str2 = str;
            final IBinder iBinder2 = iBinder;
            final Bundle bundle2 = bundle;
            MediaBrowserServiceCompat.this.f3070d.mo2798a(new Runnable() {
                public void run() {
                    C0461b bVar = (C0461b) MediaBrowserServiceCompat.this.f3068b.get(eVar2.mo2794a());
                    if (bVar == null) {
                        Log.w("MBServiceCompat", "addSubscription for callback that isn't registered id=" + str2);
                    } else {
                        MediaBrowserServiceCompat.this.mo2765a(str2, bVar, iBinder2, bundle2);
                    }
                }
            });
        }

        /* renamed from: a */
        public void mo2784a(final String str, final IBinder iBinder, final C0471e eVar) {
            MediaBrowserServiceCompat.this.f3070d.mo2798a(new Runnable() {
                public void run() {
                    C0461b bVar = (C0461b) MediaBrowserServiceCompat.this.f3068b.get(eVar.mo2794a());
                    if (bVar == null) {
                        Log.w("MBServiceCompat", "removeSubscription for callback that isn't registered id=" + str);
                    } else if (!MediaBrowserServiceCompat.this.mo2770a(str, bVar, iBinder)) {
                        Log.w("MBServiceCompat", "removeSubscription called for " + str + " which is not subscribed");
                    }
                }
            });
        }

        /* renamed from: a */
        public void mo2785a(final String str, final ResultReceiver resultReceiver, final C0471e eVar) {
            if (!TextUtils.isEmpty(str) && resultReceiver != null) {
                MediaBrowserServiceCompat.this.f3070d.mo2798a(new Runnable() {
                    public void run() {
                        C0461b bVar = (C0461b) MediaBrowserServiceCompat.this.f3068b.get(eVar.mo2794a());
                        if (bVar == null) {
                            Log.w("MBServiceCompat", "getMediaItem for callback that isn't registered id=" + str);
                        } else {
                            MediaBrowserServiceCompat.this.mo2766a(str, bVar, resultReceiver);
                        }
                    }
                });
            }
        }

        /* renamed from: b */
        public void mo2786b(final C0471e eVar) {
            MediaBrowserServiceCompat.this.f3070d.mo2798a(new Runnable() {
                public void run() {
                    MediaBrowserServiceCompat.this.f3068b.remove(eVar.mo2794a());
                }
            });
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserServiceCompat$e */
    interface C0471e {
        /* renamed from: a */
        IBinder mo2794a();

        /* renamed from: a */
        void mo2795a(String str, Token token, Bundle bundle) throws RemoteException;

        /* renamed from: a */
        void mo2796a(String str, List<MediaItem> list, Bundle bundle) throws RemoteException;

        /* renamed from: b */
        void mo2797b() throws RemoteException;
    }

    /* renamed from: android.support.v4.media.MediaBrowserServiceCompat$f */
    class C0472f implements C0471e {

        /* renamed from: a */
        final Messenger f3116a;

        C0472f(Messenger messenger) {
            this.f3116a = messenger;
        }

        /* renamed from: a */
        private void m3740a(int i, Bundle bundle) throws RemoteException {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 1;
            obtain.setData(bundle);
            this.f3116a.send(obtain);
        }

        /* renamed from: a */
        public IBinder mo2794a() {
            return this.f3116a.getBinder();
        }

        /* renamed from: a */
        public void mo2795a(String str, Token token, Bundle bundle) throws RemoteException {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putInt("extra_service_version", 1);
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bundle2.putParcelable("data_media_session_token", token);
            bundle2.putBundle("data_root_hints", bundle);
            m3740a(1, bundle2);
        }

        /* renamed from: a */
        public void mo2796a(String str, List<MediaItem> list, Bundle bundle) throws RemoteException {
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bundle2.putBundle("data_options", bundle);
            if (list != null) {
                bundle2.putParcelableArrayList("data_media_item_list", list instanceof ArrayList ? (ArrayList) list : new ArrayList(list));
            }
            m3740a(3, bundle2);
        }

        /* renamed from: b */
        public void mo2797b() throws RemoteException {
            m3740a(2, null);
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserServiceCompat$g */
    final class C0473g extends Handler {

        /* renamed from: b */
        private final C0463d f3119b = new C0463d();

        C0473g() {
        }

        /* renamed from: a */
        public void mo2798a(Runnable runnable) {
            if (Thread.currentThread() == getLooper().getThread()) {
                runnable.run();
            } else {
                post(runnable);
            }
        }

        public void handleMessage(Message message) {
            Bundle data = message.getData();
            switch (message.what) {
                case 1:
                    this.f3119b.mo2782a(data.getString("data_package_name"), data.getInt("data_calling_uid"), data.getBundle("data_root_hints"), (C0471e) new C0472f(message.replyTo));
                    return;
                case 2:
                    this.f3119b.mo2780a(new C0472f(message.replyTo));
                    return;
                case 3:
                    this.f3119b.mo2783a(data.getString("data_media_item_id"), BundleCompat.getBinder(data, "data_callback_token"), data.getBundle("data_options"), (C0471e) new C0472f(message.replyTo));
                    return;
                case 4:
                    this.f3119b.mo2784a(data.getString("data_media_item_id"), BundleCompat.getBinder(data, "data_callback_token"), (C0471e) new C0472f(message.replyTo));
                    return;
                case 5:
                    this.f3119b.mo2785a(data.getString("data_media_item_id"), (ResultReceiver) data.getParcelable("data_result_receiver"), (C0471e) new C0472f(message.replyTo));
                    return;
                case 6:
                    this.f3119b.mo2781a(new C0472f(message.replyTo), data.getBundle("data_root_hints"));
                    return;
                case 7:
                    this.f3119b.mo2786b(new C0472f(message.replyTo));
                    return;
                default:
                    Log.w("MBServiceCompat", "Unhandled message: " + message + "\n  Service version: " + 1 + "\n  Client version: " + message.arg1);
                    return;
            }
        }

        public boolean sendMessageAtTime(Message message, long j) {
            Bundle data = message.getData();
            data.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", Binder.getCallingUid());
            return super.sendMessageAtTime(message, j);
        }
    }

    /* renamed from: a */
    public abstract C0460a mo2762a(String str, int i, Bundle bundle);

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public List<MediaItem> mo2763a(List<MediaItem> list, Bundle bundle) {
        if (list == null) {
            return null;
        }
        int i = bundle.getInt("android.media.browse.extra.PAGE", -1);
        int i2 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (i == -1 && i2 == -1) {
            return list;
        }
        int i3 = i2 * i;
        int i4 = i3 + i2;
        if (i < 0 || i2 < 1 || i3 >= list.size()) {
            return Collections.EMPTY_LIST;
        }
        if (i4 > list.size()) {
            i4 = list.size();
        }
        return list.subList(i3, i4);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2764a(String str, C0461b bVar, Bundle bundle) {
        final C0461b bVar2 = bVar;
        final String str2 = str;
        final Bundle bundle2 = bundle;
        C04581 r0 = new C0462c<List<MediaItem>>(str) {
            /* access modifiers changed from: 0000 */
            /* renamed from: a */
            public void mo2772a(List<MediaItem> list, int i) {
                if (MediaBrowserServiceCompat.this.f3068b.get(bVar2.f3082c.mo2794a()) == bVar2) {
                    try {
                        bVar2.f3082c.mo2796a(str2, (i & 1) != 0 ? MediaBrowserServiceCompat.this.mo2763a(list, bundle2) : list, bundle2);
                    } catch (RemoteException e) {
                        Log.w("MBServiceCompat", "Calling onLoadChildren() failed for id=" + str2 + " package=" + bVar2.f3080a);
                    }
                } else if (MediaBrowserServiceCompat.f3067a) {
                    Log.d("MBServiceCompat", "Not sending onLoadChildren result for connection that has been disconnected. pkg=" + bVar2.f3080a + " id=" + str2);
                }
            }
        };
        this.f3069c = bVar;
        if (bundle == null) {
            mo2767a(str, (C0462c<List<MediaItem>>) r0);
        } else {
            mo2768a(str, (C0462c<List<MediaItem>>) r0, bundle);
        }
        this.f3069c = null;
        if (!r0.mo2779a()) {
            throw new IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + bVar.f3080a + " id=" + str);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2765a(String str, C0461b bVar, IBinder iBinder, Bundle bundle) {
        List<Pair> list = (List) bVar.f3084e.get(str);
        if (list == null) {
            list = new ArrayList<>();
        }
        for (Pair drVar : list) {
            if (iBinder == drVar.f9919a && MediaBrowserCompatUtils.m8993a(bundle, (Bundle) drVar.f9920b)) {
                return;
            }
        }
        list.add(new Pair(iBinder, bundle));
        bVar.f3084e.put(str, list);
        mo2764a(str, bVar, bundle);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2766a(String str, C0461b bVar, final ResultReceiver resultReceiver) {
        C04592 r0 = new C0462c<MediaItem>(str) {
            /* access modifiers changed from: 0000 */
            /* renamed from: a */
            public void mo2772a(MediaItem mediaItem, int i) {
                if ((i & 2) != 0) {
                    resultReceiver.mo2828a(-1, null);
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putParcelable("media_item", mediaItem);
                resultReceiver.mo2828a(0, bundle);
            }
        };
        this.f3069c = bVar;
        mo2771b(str, r0);
        this.f3069c = null;
        if (!r0.mo2779a()) {
            throw new IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + str);
        }
    }

    /* renamed from: a */
    public abstract void mo2767a(String str, C0462c<List<MediaItem>> cVar);

    /* renamed from: a */
    public void mo2768a(String str, C0462c<List<MediaItem>> cVar, Bundle bundle) {
        cVar.mo2777a(1);
        mo2767a(str, cVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo2769a(String str, int i) {
        if (str == null) {
            return false;
        }
        for (String equals : getPackageManager().getPackagesForUid(i)) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo2770a(String str, C0461b bVar, IBinder iBinder) {
        if (iBinder == null) {
            return bVar.f3084e.remove(str) != null;
        }
        boolean z = false;
        List list = (List) bVar.f3084e.get(str);
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                if (iBinder == ((Pair) it.next()).f9919a) {
                    z = true;
                    it.remove();
                }
            }
            if (list.size() == 0) {
                bVar.f3084e.remove(str);
            }
        }
        return z;
    }

    /* renamed from: b */
    public void mo2771b(String str, C0462c<MediaItem> cVar) {
        cVar.mo2777a(2);
        cVar.mo2778a(null);
    }
}
