package android.support.p001v4.content;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.util.SparseArray;

/* renamed from: android.support.v4.content.WakefulBroadcastReceiver */
public abstract class WakefulBroadcastReceiver extends BroadcastReceiver {

    /* renamed from: a */
    private static final SparseArray<WakeLock> f3062a = new SparseArray<>();

    /* renamed from: b */
    private static int f3063b = 1;

    /* renamed from: a */
    public static boolean m3705a(Intent intent) {
        int intExtra = intent.getIntExtra("android.support.content.wakelockid", 0);
        if (intExtra == 0) {
            return false;
        }
        synchronized (f3062a) {
            WakeLock wakeLock = (WakeLock) f3062a.get(intExtra);
            if (wakeLock != null) {
                wakeLock.release();
                f3062a.remove(intExtra);
                return true;
            }
            Log.w("WakefulBroadcastReceiver", "No active wake lock id #" + intExtra);
            return true;
        }
    }

    /* renamed from: a_ */
    public static ComponentName m3706a_(Context context, Intent intent) {
        ComponentName startService;
        synchronized (f3062a) {
            int i = f3063b;
            f3063b++;
            if (f3063b <= 0) {
                f3063b = 1;
            }
            intent.putExtra("android.support.content.wakelockid", i);
            startService = context.startService(intent);
            if (startService == null) {
                startService = null;
            } else {
                WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "wake:" + startService.flattenToShortString());
                newWakeLock.setReferenceCounted(false);
                newWakeLock.acquire(60000);
                f3062a.put(i, newWakeLock);
            }
        }
        return startService;
    }
}
